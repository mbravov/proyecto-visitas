﻿using Agrobanco.SSGV.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SSGV.BLL
{
    public class AsignacionBLL
    {

        private readonly AsignacionesDTO oAsignacion = new AsignacionesDTO();

        public int AnularAsignaciones()
        {
            return oAsignacion.AnulacionPorAntiguedad();
        }

    }
}

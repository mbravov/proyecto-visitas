﻿using System;
using System.Configuration;

namespace AGROBANCO.SSGV.Comun
{
    public static class ApplicationKeys
    {
        #region INFORMACION TIPO DE LISTAS
        public static string TipoDocumento => ConfigurationManager.AppSettings["LISTA_TIPO_DOCUMENTO"];
        public static string TipoUsuario => ConfigurationManager.AppSettings["LISTA_TIPO_USUARIO"];
        #endregion

        #region INFORMACION PARA CORREO ELECTRONICO
        public static string NombreApp => ConfigurationManager.AppSettings["NOMBRE_APP"];
        public static string NombreEmpresa => ConfigurationManager.AppSettings["NOMBRE_EMPRESA"];
        public static string AsuntoOlvidoContrasenia => ConfigurationManager.AppSettings["ASUNTO_OLVIDO_CONTRASENIA"];
        #endregion


        #region TIEMPO DE BLOQUEO DE USUARIO EN EL LOGIN
        public static string TiempoBloqueoUser => ConfigurationManager.AppSettings["TIME_BLOQUEO_USER"];
        public static int NroIntentosBloq => Convert.ToInt32(ConfigurationManager.AppSettings["NRO_INTENTOS_BLOQ_LOGIN"]);
        #endregion

        #region VARIABLES DE JWT
        public static string JWTSecretKey => ConfigurationManager.AppSettings["JWT_SECRET_KEY"];
        public static string JWTAudienceToken => ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
        public static string JWTIssuerToken => ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
        public static string JWTExpireMinutes => ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"];
        #endregion

        #region VARIABLES DE ACTIVE DIRECTORY
        public static bool ADActive => ConfigurationManager.AppSettings["AD_ACTIVE"] == null ? false : Convert.ToBoolean(ConfigurationManager.AppSettings["AD_ACTIVE"]);
        /* public static string ADServer => ConfigurationManager.AppSettings["AD_SERVER"];
         public static string ADUserAdmin => ConfigurationManager.AppSettings["AD_USER_ADMIN"];
         public static string ADPasswordAdmin => ConfigurationManager.AppSettings["AD_PASSWORD_ADMIN"];
         public static string ADContainer => ConfigurationManager.AppSettings["AD_CONTAINER"];
         public static string ADDomain => ConfigurationManager.AppSettings["AD_DOMAIN"]; //VALIDAR USO*/
        public static string ADPasswordDefault => ConfigurationManager.AppSettings["AD_PASSWORD_DEFAULT"]; //VALIDAR USO
        #endregion

        #region --------------PARAMETROS DB2-------------
        //BD
        public static bool Db2_Active => ConfigurationManager.AppSettings["DB2_ACTIVE"] == null ? false : Convert.ToBoolean(ConfigurationManager.AppSettings["DB2_ACTIVE"]);
        public static string Db2_Server => ConfigurationManager.AppSettings["DB2_SERVER"];
        public static string Db2_DataBase => ConfigurationManager.AppSettings["DB2_DATABASE"];
        public static string Db2_Usuario => ConfigurationManager.AppSettings["DB2_USUARIO"];
        public static string Db2_Password => ConfigurationManager.AppSettings["DB2_PASSWORD"];

        //LIBRERIA USADA
        public static string Db2_Library => ConfigurationManager.AppSettings["DB2_LIBRARY"];

        //ENCRIPTACION
        public static string Db2ApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:Db2ApplicationNameEnableEncrip"];
        public static string Db2RegeditFolder => ConfigurationManager.AppSettings["config:Db2ApplicationName"];
        public static string ADApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:ADApplicationNameEnableEncrip"];
        public static string ADRegeditFolder => ConfigurationManager.AppSettings["config:ADApplicationName"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];

        //ESCHEMA
        public static string Db2Esquema => ConfigurationManager.AppSettings["config:Db2Eschema"];
        #endregion

        #region CONFIGURACION DE RUTAS DE LOGS
        public static string DB2rutaLogsError => ConfigurationManager.AppSettings["DB2rutaLogsError"];
        public static string APIrutaLogsError => ConfigurationManager.AppSettings["APIrutaLogsError"];
        #endregion


    }
}
﻿using AGROBANCO.SSGV.Comun;
using AGROBANCO.SSGV.DAL.DataDb2;
using IBM.Data.DB2.iSeries;
using System;
using System.Data;

namespace Agrobanco.SSGV.DAL
{
    public class AsignacionesDTO
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public AsignacionesDTO()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }
        
        public int AnulacionPorAntiguedad()
        {
            int iActualizados = 0;
            
            try
            {
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_ROWUPDATED", int.MaxValue, direction: ParameterDirection.Output);
                
                _db2DataContext.RunStoredProcedure(library + ".UP_SGVASIM03_ANULACION_POR_ANTIGUEDAD", out int affectedRows, lDataParam);
                
                iActualizados = Convert.ToInt32(lDataParam[0].Value);

                return iActualizados;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

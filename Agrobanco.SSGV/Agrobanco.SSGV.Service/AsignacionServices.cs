﻿using Agrobanco.SSGV.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SSGV.Service
{
    partial class AsignacionServices : ServiceBase
    {
        bool bFlag = false;
        public AsignacionServices()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer1.Start(); // TODO: agregar código aquí para iniciar el servicio.
        }

        protected override void OnStop()
        {
            timer1.Stop(); // TODO: agregar código aquí para realizar cualquier anulación necesaria para detener el servicio.
        }

        private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimeSpan t2 = DateTime.Now.TimeOfDay;
            int iRegistrosAnulados = 0;
            AsignacionBLL oAsignaciones = new AsignacionBLL();
            EventLog.WriteEntry("hora SGVVISITAS" + t2.Hours);
            if (bFlag) return;
            string horaInicio = ConfigurationManager.AppSettings["horaInicio"];
            string horaFin = ConfigurationManager.AppSettings["horaFin"];
            TimeSpan t = DateTime.Now.TimeOfDay;
            try
            {
                if (t.Hours >= Convert.ToInt32(horaInicio) && t.Hours <= Convert.ToInt32(horaFin))
                {
                    EventLog.WriteEntry("Iniciando proceso de anulación automática de asignaciones sin visita", EventLogEntryType.Information);
                    iRegistrosAnulados = oAsignaciones.AnularAsignaciones();
                    EventLog.WriteEntry("Se anularon " + iRegistrosAnulados.ToString() + " asignaciones automáticamente.", EventLogEntryType.Information);
                    bFlag = true;
                    EventLog.WriteEntry("Finalizando el proceso de anulación automática de asignaciones sin visita", EventLogEntryType.Information);
                }
            }
            catch (Exception oEx)
            {
                EventLog.WriteEntry(oEx.Message, EventLogEntryType.Error);
            }

            bFlag = false;
        }
    }
}

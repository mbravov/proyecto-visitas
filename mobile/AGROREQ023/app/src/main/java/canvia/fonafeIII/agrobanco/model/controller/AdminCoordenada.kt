package canvia.fonafeIII.agrobanco.model.controller

import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada
import java.lang.Exception

class AdminCoordenada {
    fun getCoordenadas(id_visita: String): ArrayList<Coordenada>?{
        var coordenadas = arrayListOf<Coordenada>()
        val whereArgs = arrayOf(id_visita)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_COORDENADA).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_COORDENADA,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA, whereArgs, null, null, null
                )
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst()
                    do {
                        val coordenada = Coordenada("","")
                        coordenada.id_visita =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_id_visita))
                        coordenada.numero_orden =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_numero_orden))
                        coordenada.latitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_latitud))
                        coordenada.longitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_longitud))
                        coordenada.altitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_altitud))
                        coordenada.precision =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_precision))
                        coordenada.usuario_crea =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_usuario_crea))
                        coordenada.fecha_crea =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_fecha_crea))
                        coordenada.usuario_modifica =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_usuario_modifica))
                        coordenada.fecha_modifica =
                            cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_fecha_modifica))
                        coordenadas.add(coordenada)
                    } while (cursor.moveToNext())
                }
                db.close()
            }else{
                Log.e("getCoordenadas: ","No existe -Coordenadas")
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return coordenadas
    }

    fun getCoordenada(id_visita: String, numero_orden: String): Coordenada?{
        var coordenada: Coordenada? = null

        val whereArgs = arrayOf(id_visita,numero_orden)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_COORDENADA).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_COORDENADA,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA_NORDEN, whereArgs, null, null, null
                )

                if (cursor.getCount() === 1) {
                    cursor.moveToFirst()
                    coordenada = Coordenada("","")
                    coordenada.id_visita = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_id_visita))
                    coordenada.numero_orden = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_numero_orden))
                    coordenada.latitud = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_latitud))
                    coordenada.longitud = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_longitud))
                    coordenada.altitud = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_altitud))
                    coordenada.precision = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_precision))
                    coordenada.usuario_crea = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_usuario_crea))
                    coordenada.fecha_crea = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_fecha_crea))
                    coordenada.usuario_modifica = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_usuario_modifica))
                    coordenada.fecha_modifica = cursor.getString(cursor.getColumnIndex(Contract.Coordenada.coordenada_fecha_modifica))
                }
                db.close()
            }else{
                Log.e("getCoordenada: ","No existe -Coordenada")
            }
        } finally {
            if (cursor != null) cursor.close()
        }

        return coordenada
    }

    fun deleteCoordenadas(id_visita: String){
        if(getCoordenadas(id_visita)!!.size>0) {
            val whereArgs = arrayOf(id_visita)
            try {
                val db = AppConfiguracion.DB.writableDatabase
                db.delete(
                    AppConfiguracion.TB_COORDENADA,
                    SQLConstantes().WHERE_CLAUSE_ID_VISITA,
                    whereArgs
                );
                db.close()
            } catch (ex: Exception) {
                Log.e("E-deleteCoordenadas: ", "" + ex.toString())
            }
        }
    }

    fun deleteCoordenada(id_visita: String, numero_orden: String){
        val whereArgs = arrayOf(id_visita,numero_orden)
        try {
            val db = AppConfiguracion.DB.writableDatabase
            db.delete(AppConfiguracion.TB_COORDENADA,SQLConstantes().WHERE_CLAUSE_ID_VISITA_NORDEN,whereArgs);
            db.close()
        } catch (ex: Exception) {
            Log.e("E-deleteCoordenada: ", "" + ex.toString())
        }
    }
}
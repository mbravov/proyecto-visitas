package canvia.fonafeIII.agrobanco.model.controller

import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes
import canvia.fonafeIII.agrobanco.model.pojos.EvalArea

class AdminEvalArea {
    fun getEvalArea(id: String): EvalArea?{
        var evalArea: EvalArea? = null

        val whereArgs = arrayOf(id)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_EVALAREA).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_EVALAREA,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA, whereArgs, null, null, null
                )

                if (cursor.getCount() === 1) {
                    cursor.moveToFirst()
                    evalArea = EvalArea("")
                    evalArea.id_visita = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_id_visita))
                 //   evalArea.numero_puntos = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_numero_puntos))
                  //  evalArea.area = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_area))
                  //  evalArea.modo_captura = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_modo_captura))
                   // evalArea.tipo_mapa = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_tipo_mapa))
                    evalArea.usuario_crea = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_usuario_crea))
                    evalArea.fecha_crea = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_fecha_crea))
                    evalArea.usuario_modifica = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_usuario_modifica))
                    evalArea.fecha_modifica = cursor.getString(cursor.getColumnIndex(Contract.EvalArea.evalArea_fecha_modifica))
                }
                db.close()
            }else{
                Log.e("get-SeccionC: ","No existe -C")
            }
        } finally {
            if (cursor != null) cursor.close()
        }


        return evalArea
    }
}
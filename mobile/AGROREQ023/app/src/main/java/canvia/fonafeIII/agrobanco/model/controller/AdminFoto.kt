package canvia.fonafeIII.agrobanco.model.controller

import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes

import canvia.fonafeIII.agrobanco.model.pojos.Foto
import java.lang.Exception

class AdminFoto{


    fun getCoordenadas(id_visita: String): ArrayList<Foto>?{
        var fotos = arrayListOf<Foto>()
        val whereArgs = arrayOf(id_visita)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_FOTO).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_FOTO,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA, whereArgs, null, null, null
                )
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst()
                    do {
                        val foto = Foto("")
                        foto.id_visita =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_id_visita))
                        foto.numero_orden =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_nro_orden))
                        foto.latitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_latitud))
                        foto.longitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_longitud))
                        foto.altitud =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_altitud))
                        foto.precision =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_precision))
                        foto.nombre =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_nombre))
                        foto.ruta =
                            cursor.getString(cursor.getColumnIndex(Contract.Foto.foto_ruta))

                        fotos.add(foto)
                    } while (cursor.moveToNext())
                }
                db.close()
            }else{
                Log.e("getCoordenadas: ","No existe -Coordenadas")
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return fotos
    }

}
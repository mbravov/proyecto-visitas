package canvia.fonafeIII.agrobanco.model.controller

import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes
import canvia.fonafeIII.agrobanco.model.pojos.Visita

class AdminVisita {
    fun getVisitas(id_usuario: String): ArrayList<Visita>?{
        var visitas = arrayListOf<Visita>()
        val whereArgs = arrayOf(id_usuario)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_VISITA).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_VISITA,
                    null, SQLConstantes().WHERE_CLAUSE_ID_USUARIO, whereArgs, null, null, null
                )
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst()
                    do {
                        val visita = Visita("")
                        visita.id_visita =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_id_visita))
                     //   visita.cod_asignacion =
                      //      cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_cod_asignacion))
                        visita.id_usuario =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_id_usuario))
                        visita.dni_cliente =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_dni_cliente))
                        visita.primer_nombre_cliente =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_primer_nombre_cliente))
                        visita.segundo_nombre_cliente =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_segundo_nombre_cliente))
                        visita.apellido_paterno_cliente =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_apellido_paterno_cliente))
                        visita.apellido_materno_cliente =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_apellido_materno_cliente))
                        visita.nombre_predio =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_nombre_predio))
                        visita.tipo_visita =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_tipo_visita))
                        visita.codigo_agencia =0
                            //cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_codigo_agencia))
                        visita.fecha_visita =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_visita))

                        visita.usuario_crea =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_usuario_crea))
                        visita.fecha_crea =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_crea))
                        visita.usuario_modifica =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_usuario_modifica))
                        visita.fecha_modifica =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_modifica))
                        visita.acceso =
                            cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_acceso))
                        visitas.add(visita)
                    } while (cursor.moveToNext())
                }
                db.close()
            }else{
                Log.e("getVisitas: ","No existe -Visita")
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return visitas
    }

    fun getVisita(id: String): Visita?{
        var visita: Visita? = null

        val whereArgs = arrayOf(id)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_VISITA).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_VISITA,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA, whereArgs, null, null, null
                )

                if (cursor.getCount() === 1) {
                    cursor.moveToFirst()
                    visita = Visita("")
                    visita.id_visita = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_id_visita))
                 //   visita.cod_asignacion = //cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_cod_asignacion))
                    visita.id_usuario = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_id_usuario))
                    visita.dni_cliente = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_dni_cliente))
                    visita.primer_nombre_cliente = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_primer_nombre_cliente))
                    visita.segundo_nombre_cliente = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_segundo_nombre_cliente))
                    visita.apellido_paterno_cliente = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_apellido_paterno_cliente))
                    visita.apellido_materno_cliente = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_apellido_materno_cliente))
                    visita.nombre_predio = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_nombre_predio))
                    visita.tipo_visita = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_tipo_visita))
                    visita.codigo_agencia =0// cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_codigo_agencia))
                    visita.fecha_visita = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_visita))

                    visita.usuario_crea = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_usuario_crea))
                    visita.fecha_crea = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_crea))
                    visita.usuario_modifica = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_usuario_modifica))
                    visita.fecha_modifica = cursor.getString(cursor.getColumnIndex(Contract.Visita.visita_fecha_modifica))
                }
                db.close()
            }else{
                Log.e("get-AdminVisita: ", "No existe -Visita")
            }
        } finally {
            if (cursor != null) cursor.close()
        }
        return visita
    }
}

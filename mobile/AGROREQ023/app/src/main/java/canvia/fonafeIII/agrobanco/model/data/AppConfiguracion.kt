package canvia.fonafeIII.agrobanco.model.data

import android.app.Application
import android.content.Context
import canvia.fonafeIII.agrobanco.model.pojos.Usuario

class AppConfiguracion: Application() {
    companion object{
        var CONTEXT: Context?=null
        var USUARIO: Usuario? = null
        var MAX_PRECISION = 30
        var DB = InitDb()
        val DB_NAME = "VisitaPruebaDB_1.db"
        val VERSION = 1
        val TB_REGISTROINFORMACION = "TB_REGISTROINFORMACION"
        val TB_VISITA = "TB_VISITA"
        val TB_EVALAREA = "TB_EVALAREA"
        val TB_COORDENADA = "TB_COORDENADA"
        val TB_FOTO = "TB_FOTO"
        val TB_FOTO_COMPLEMENTARIA = "TB_FOTO_COMPLEMENTARIA"
    }

    var user: Usuario? = null
    fun getUsuario(): Usuario? {
        return user
    }

    fun setUsuario(usuario: Usuario) {
        user = usuario
    }

    override fun onCreate() {
        super.onCreate()
        CONTEXT = applicationContext
    }
}
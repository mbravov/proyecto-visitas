package canvia.fonafeIII.agrobanco.model.data

import android.provider.BaseColumns

class Contract {
    class RegistroInformacion: BaseColumns {
        companion object{
            val registro_id_visita = "ID_VISITA"
            val registro_primer_nombre_cliente = "PRIMER_NOMBRE_CLIENTE"
            val registro_segundo_nombre_cliente = "SEGUNDO_NOMBRE_CLIENTE"
            val registro_apellido_paterno_cliente = "APELLIDO_PATERNO_CLIENTE"
            val registro_apellido_materno_cliente = "APELLIDO_MATERNO_CLIENTE"
            val registro_dni_cliente = "DNI_CLIENTE"
            val registro_nombre_predio = "NOMBRE_PREDIO"
            val registro_direccion_predio = "DIRECCION_PREDIO"
            val registro_tipo_actividad = "TIPO_ACTIVIDAD"
            val registro_latitud_inicio = "LATITUD_INICIO"
            val registro_longitud_inicio = "LONGITUD_INICIO"
            val registro_precision_inicio = "PRECISION_INICIO"
            val registro_altitud_inicio = "ALTITUD_INICIO"
            val registro_latitud_final = "LATITUD_FINAL"
            val registro_longitud_final = "LONGITUD_FINAL"
            val registro_precision_final = "PRECISION_FINAL"
            val registro_altitud_final = "ALTITUD_FINAL"
            val registro_usuario_crea = "USUARIO_CREA"
            val registro_fecha_crea = "FECHA_CREA"
            val registro_usuario_modifica = "USUARIO_MODIFICA"
            val registro_fecha_modifica = "FECHA_MODIFICA"
        }
    }

    class Visita: BaseColumns {
        companion object{
            val visita_id_visita = "ID_VISITA"
            val visita_cod_asignacion = "COD_ASIGNACION"
            val visita_id_usuario = "ID_USUARIO"
            val visita_dni_cliente = "DNI_CLIENTE"
            val visita_primer_nombre_cliente = "PRIMER_NOMBRE_CLIENTE"
            val visita_segundo_nombre_cliente = "SEGUNDO_NOMBRE_CLIENTE"
            val visita_apellido_paterno_cliente = "APELLIDO_PATERNO_CLIENTE"
            val visita_apellido_materno_cliente = "APELLIDO_MATERNO_CLIENTE"
            val visita_nombre_predio = "NOMBRE_PREDIO"
            val visita_tipo_visita = "TIPO_VISITA"
            val visita_codigo_agencia = "CODIGO_AGENCIA"
            val visita_fecha_visita = "FECHA_VISITA"
            val visita_estado = "ESTADO"
            val visita_usuario_crea = "USUARIO_CREA"
            val visita_fecha_crea = "FECHA_CREA"
            val visita_usuario_modifica = "USUARIO_MODIFICA"
            val visita_fecha_modifica = "FECHA_MODIFICA"
            val visita_acceso = "ACCESO"
        }
    }

    class EvalArea: BaseColumns {
        companion object {
            val evalArea_id_visita = "ID_VISITA"
            val evalArea_numero_puntos = "NUMERO_PUNTOS"
            val evalArea_area = "AREA"
            val evalArea_modo_captura = "MODO_CAPTURA"
            val evalArea_tipo_mapa = "TIPO_MAPA"
            val evalArea_usuario_crea = "USUARIO_CREA"
            val evalArea_fecha_crea = "FECHA_CREA"
            val evalArea_usuario_modifica = "USUARIO_MODIFICA"
            val evalArea_fecha_modifica = "FECHA_MODIFICA"
        }
    }

    class Coordenada: BaseColumns {
        companion object {
            val coordenada_id_visita = "ID_VISITA"
            val coordenada_numero_orden = "NUMERO_ORDEN"
            val coordenada_latitud = "LATITUD"
            val coordenada_longitud = "LONGITUD"
            val coordenada_altitud = "ALTITUD"
            val coordenada_precision = "PRECISION"
            val coordenada_usuario_crea = "USUARIO_CREA"
            val coordenada_fecha_crea = "FECHA_CREA"
            val coordenada_usuario_modifica = "USUARIO_MODIFICA"
            val coordenada_fecha_modifica = "FECHA_MODIFICA"
        }
    }

    class Foto: BaseColumns {
        companion object {
            val foto_id_visita = "ID_VISITA"
            val foto_nro_orden = "NUMERO_ORDEN"
            val foto_ruta = "RUTA"
            val foto_nombre = "NOMBRE"
            val foto_altitud = "ALTITUD"
            val foto_latitud = "LATITUD"
            val foto_longitud = "LONGITUD"
            val foto_precision = "PRECISION"

        }
    }


}
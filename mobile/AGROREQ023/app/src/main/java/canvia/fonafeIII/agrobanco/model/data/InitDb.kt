package canvia.fonafeIII.agrobanco.model.data

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class InitDb: SQLiteOpenHelper(AppConfiguracion.CONTEXT, AppConfiguracion.DB_NAME, null, AppConfiguracion.VERSION){
    val qryCreaTB_REGISTROINFORMACION = "CREATE TABLE ${AppConfiguracion.TB_REGISTROINFORMACION}(" +
            Contract.RegistroInformacion.registro_id_visita + " TEXT PRIMARY KEY," +
            Contract.RegistroInformacion.registro_primer_nombre_cliente + " TEXT," +
            Contract.RegistroInformacion.registro_segundo_nombre_cliente + " TEXT," +
            Contract.RegistroInformacion.registro_apellido_paterno_cliente + " TEXT," +
            Contract.RegistroInformacion.registro_apellido_materno_cliente + " TEXT," +
            Contract.RegistroInformacion.registro_dni_cliente + " TEXT," +
            Contract.RegistroInformacion.registro_nombre_predio + " TEXT," +
            Contract.RegistroInformacion.registro_direccion_predio + " TEXT," +
            Contract.RegistroInformacion.registro_tipo_actividad + " TEXT," +
            Contract.RegistroInformacion.registro_latitud_inicio + " TEXT," +
            Contract.RegistroInformacion.registro_longitud_inicio + " TEXT," +
            Contract.RegistroInformacion.registro_precision_inicio + " TEXT," +
            Contract.RegistroInformacion.registro_altitud_inicio + " TEXT," +
            Contract.RegistroInformacion.registro_latitud_final + " TEXT," +
            Contract.RegistroInformacion.registro_longitud_final + " TEXT," +
            Contract.RegistroInformacion.registro_precision_final + " TEXT," +
            Contract.RegistroInformacion.registro_altitud_final + " TEXT," +
            Contract.RegistroInformacion.registro_usuario_crea + " TEXT," +
            Contract.RegistroInformacion.registro_fecha_crea + " TEXT," +
            Contract.RegistroInformacion.registro_usuario_modifica + " TEXT," +
            Contract.RegistroInformacion.registro_fecha_modifica + " TEXT)"

    val qryCreaTB_VISITA = "CREATE TABLE ${AppConfiguracion.TB_VISITA}(" +
            Contract.Visita.visita_id_visita + " TEXT PRIMARY KEY," +
            Contract.Visita.visita_cod_asignacion + " TEXT," +
            Contract.Visita.visita_id_usuario + " TEXT," +
            Contract.Visita.visita_dni_cliente + " TEXT," +
            Contract.Visita.visita_primer_nombre_cliente + " TEXT," +
            Contract.Visita.visita_segundo_nombre_cliente + " TEXT," +
            Contract.Visita.visita_apellido_paterno_cliente + " TEXT," +
            Contract.Visita.visita_apellido_materno_cliente + " TEXT," +
            Contract.Visita.visita_nombre_predio + " TEXT," +
            Contract.Visita.visita_tipo_visita + " TEXT," +
            Contract.Visita.visita_codigo_agencia + " TEXT," +
            Contract.Visita.visita_fecha_visita + " TEXT," +
            Contract.Visita.visita_estado + " TEXT," +
            Contract.Visita.visita_usuario_crea + " TEXT," +
            Contract.Visita.visita_fecha_crea + " TEXT," +
            Contract.Visita.visita_usuario_modifica + " TEXT," +
            Contract.Visita.visita_fecha_modifica + " TEXT," +
            Contract.Visita.visita_acceso + " TEXT)"

    val qryCreaTB_EVALAREA = "CREATE TABLE ${AppConfiguracion.TB_EVALAREA}(" +
            Contract.EvalArea.evalArea_id_visita + " TEXT PRIMARY KEY," +
            Contract.EvalArea.evalArea_numero_puntos + " TEXT," +
            Contract.EvalArea.evalArea_area + " TEXT," +
            Contract.EvalArea.evalArea_modo_captura + " TEXT," +
            Contract.EvalArea.evalArea_tipo_mapa + " TEXT," +
            Contract.EvalArea.evalArea_usuario_crea + " TEXT," +
            Contract.EvalArea.evalArea_fecha_crea + " TEXT," +
            Contract.EvalArea.evalArea_usuario_modifica + " TEXT," +
            Contract.EvalArea.evalArea_fecha_modifica + " TEXT)"

    val qryCreaTB_COORDENADA = "CREATE TABLE ${AppConfiguracion.TB_COORDENADA}(" +
            Contract.Coordenada.coordenada_id_visita + " TEXT NOT NULL," +
            Contract.Coordenada.coordenada_numero_orden + " TEXT NOT NULL," +
            Contract.Coordenada.coordenada_latitud + " TEXT," +
            Contract.Coordenada.coordenada_longitud + " TEXT," +
            Contract.Coordenada.coordenada_altitud + " TEXT," +
            Contract.Coordenada.coordenada_precision + " TEXT," +
            Contract.Coordenada.coordenada_usuario_crea + " TEXT," +
            Contract.Coordenada.coordenada_fecha_crea + " TEXT," +
            Contract.Coordenada.coordenada_usuario_modifica + " TEXT," +
            Contract.Coordenada.coordenada_fecha_modifica + " TEXT," +
            "PRIMARY KEY(${Contract.Coordenada.coordenada_id_visita},${Contract.Coordenada.coordenada_numero_orden}))"


    val qryCreaTB_CAMARA =  "CREATE TABLE ${AppConfiguracion.TB_FOTO}(" +
            Contract.Foto.foto_id_visita + " TEXT NOT NULL," +
            Contract.Foto.foto_nro_orden + " TEXT NOT NULL," +
            Contract.Foto.foto_altitud + " TEXT," +
            Contract.Foto.foto_latitud + " TEXT," +
            Contract.Foto.foto_longitud + " TEXT," +
            Contract.Foto.foto_nombre + " TEXT," +
            Contract.Foto.foto_precision + " TEXT," +
            Contract.Foto.foto_ruta + " TEXT)"



    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(qryCreaTB_REGISTROINFORMACION)
        db!!.execSQL(qryCreaTB_VISITA)
        db!!.execSQL(qryCreaTB_EVALAREA)
        db!!.execSQL(qryCreaTB_COORDENADA)
        db!!.execSQL(qryCreaTB_CAMARA)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }
}
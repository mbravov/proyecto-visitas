package canvia.fonafeIII.agrobanco.model.data

class SQLConstantes {
    /* CLAUSULAS WHERE*/
    val WHERE_CLAUSE_ID_VISITA = "ID_VISITA=?"
    val WHERE_CLAUSE_ID_USUARIO = "ID_USUARIO=?"
    val WHERE_CLAUSE_ID_VISITA_NORDEN = "ID_VISITA=? AND NUMERO_ORDEN=?"
}
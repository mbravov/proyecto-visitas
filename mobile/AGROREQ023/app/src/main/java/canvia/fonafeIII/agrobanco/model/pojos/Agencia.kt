package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "TB_AGENCIA")
class Agencia(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "COD_AGENCIA")
        var cod_agencia: Int?=null,
        @ColumnInfo(name = "OFICINA")
        var oficina: String?=null,
        @ColumnInfo(name = "OFICINA_REGIONAL")
        var oficina_regional: String?=null
) {
        override fun toString(): String {
                return "$oficina"
        }
}
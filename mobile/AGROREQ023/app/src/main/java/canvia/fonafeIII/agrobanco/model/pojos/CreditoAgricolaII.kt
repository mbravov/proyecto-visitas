package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_CREDITO_AGRICOLA_II")
class CreditoAgricolaII(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "ESTADO_CAMPO")
    var estado_campo: Int? = null,
    @ColumnInfo(name = "TIPO_RIEGO")
    var tipo_riego: Int? = null,
    @ColumnInfo(name = "DISPONIBILIDAD_AGUA")
    var disponibilidad_agua: Int? = null,
    @ColumnInfo(name = "PENDIENTE")
    var pendiente: Int? = null,
    @ColumnInfo(name = "TIPO_SUELO")
    var tipo_suelo: Int? = null,
    @ColumnInfo(name = "ACCESIBILIDAD")
    var accesibilidad: Int? = null,
    @ColumnInfo(name = "ALTITUD_APROXIMADA")
    var altitud_aproximada: String? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null

) {

}
package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_CREDITO_PECUARIO_I")
class CreditoPecuarioI(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "UNIDAD_FINANCIAR")
    var unidad_financiar: String? = null,
    @ColumnInfo(name = "NUMERO_UNIDADES_FINANCIAR")
    var numero_unidades_financiar: Int? = null,
    @ColumnInfo(name = "NUMERO_UNIDADES_TOTALES")
    var numero_unidades_totales: Int? = null,
    @ColumnInfo(name = "UNIDADES_PRODUCTIVAS")
    var unidades_productivas: Int? = null,
    @ColumnInfo(name = "TIPO_ALIMENTACION")
    var tipo_alimentacion: Int? = null,
    @ColumnInfo(name = "MANEJO_ACTIVIDAD")
    var manejo_actividad: Int? = null,
    @ColumnInfo(name = "FUENTE_AGUA")
    var fuente_agua: Int? = null,
    @ColumnInfo(name = "DISPONIBILIDAD_AGUA")
    var disponibilidad_agua: Int? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null
) {

}
package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_CREDITO_PECUARIO_II")
class CreditoPecuarioII(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "CRIANZA")
    var crianza: String? = null,
    @ColumnInfo(name = "RAZA")
    var raza: String? = null,
    @ColumnInfo(name = "EXPERIENCIA_ANIOS")
    var experiencia_anios: Int? = null,
    @ColumnInfo(name = "TIPO_TECNOLOGIA")
    var tipo_tecnologia: Int? = null,
    @ColumnInfo(name = "MANEJO_CRIANZA")
    var manejo_crianza: Int? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null
) {
}
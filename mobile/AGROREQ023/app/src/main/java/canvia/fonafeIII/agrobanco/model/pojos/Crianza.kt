package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "TB_CRIANZA")
class Crianza(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "COD_CRIANZA")
        var cod_crianza: String,
        @ColumnInfo(name = "CRIANZA")
        var crianza: String?=null
) {
    override fun toString(): String {
        return "$crianza"
    }
}
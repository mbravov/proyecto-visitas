package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "TB_CULTIVO")
class Cultivo(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "COD_CULTIVO")
        var cod_cultivo: String,
        @ColumnInfo(name = "CULTIVO")
        var cultivo: String?=null
) {
    override fun toString(): String {
        return "$cultivo"
    }
}
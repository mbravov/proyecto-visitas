package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_FIRMA")
class Firmas(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "RUTA_FIRMA_TITULAR")
    var ruta_firma_titular: String? = null,
    @ColumnInfo(name = "COMPRIMIDO_FIRMA_TITULAR")
    var comprimido_firma_titular: String? = null,
    @ColumnInfo(name = "RUTA_FIRMA_CONYUGE")
    var ruta_firma_conyuge: String? = null,
    @ColumnInfo(name = "COMPRIMIDO_FIRMA_CONYUGE")
    var comprimido_firma_conyuge: String? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null

    ) {
}
package canvia.fonafeIII.agrobanco.model.pojos

import android.graphics.Bitmap
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import java.io.ByteArrayOutputStream
@Entity(tableName = "TB_FOTO", primaryKeys = arrayOf("ID_VISITA","NUMERO_ORDEN"))
class Imagen(
              @NonNull
              @ColumnInfo(name = "ID_VISITA")
              var id_visita: String,
              @NonNull
             @ColumnInfo(name = "NUMERO_ORDEN")
              var posicion: Int = 0,
             @ColumnInfo(name = "NOMBRE")
             var nombre: String? = null,
             @ColumnInfo(name = "RUTA")
             var ruta: String? = null,
             @ColumnInfo(name = "LATITUD")
             var latitud:String?=null,
             @ColumnInfo(name = "ALTITUD")
             var altitud:String?=null,
             @ColumnInfo(name = "LONGITUD")
             var longitud: String?=null,
             @ColumnInfo(name = "PRECISION")
             var precision: Float = 0.00F) {

    //public var bitmap: Bitmap?=null
    //public var byte= ByteArrayOutputStream().toByteArray()
}
package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_DATOS_PRODUCTOR")
class Productor(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "FECHA_VISITA")
    var fecha_visita: String? = null,
    @ColumnInfo(name = "AGENCIA")
    var agencia: Int? = null,
    @ColumnInfo(name = "OBJETIVO_VISITA")
    var objetivo_visita: String? = null,
    @ColumnInfo(name = "TIPO_PRESENTE")
    var tipo_presente: String? = null,
    @ColumnInfo(name = "ID_TIPO_PRESENTE")
    var id_tipo_presente: Int? = null,
    @ColumnInfo(name = "NOMBRE")
    var nombre: String? = null,
    @ColumnInfo(name = "VINCULO")
    var vinculo: String? = null,
    @ColumnInfo(name = "NUMERO_CELULAR")
    var numero_celular: String? = null,
    @ColumnInfo(name = "TIPO_MERCADO")
    var tipo_mercado: String? = null,
    @ColumnInfo(name = "ID_TIPO_MERCADO")
    var id_tipo_mercado: Int? = null,
    @ColumnInfo(name = "ID_PROTEGIDO")
    var id_protegido: Int? = null,
    @ColumnInfo(name = "PROTEGIDO")
    var protegido: String? = null,
    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null
) {


}
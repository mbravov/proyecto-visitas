package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_RESUMEN")
class Resumen(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "FECHA_PANTALLA_1")
    var fecha_pantalla_1: String? = null,
    @ColumnInfo(name = "FECHA_PANTALLA_2")
    var fecha_pantalla_2: String? = null,
    @ColumnInfo(name = "FECHA_PANTALLA_3")
    var fecha_pantalla_3: String? = null,
    @ColumnInfo(name = "FECHA_PANTALLA_4")
    var fecha_pantalla_4: String? = null,
    @ColumnInfo(name = "FECHA_PANTALLA_5")
    var fecha_pantalla_5: String? = null,
    @ColumnInfo(name = "VISITA_COMPLETA")
    var visita_completa: Int? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null
) {
}
package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "TB_UNIDAD_FINANCIAR")
class UnidadFinanciar(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "COD_UNIDAD_FINANCIAR")
        var cod_unidad_financiar: String,
        @ColumnInfo(name = "UNIDAD_FINANCIAR")
        var unidad_financiar: String?=null
) {
    override fun toString(): String {
        return "$unidad_financiar"
    }
}
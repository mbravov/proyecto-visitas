package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "TB_UNIDAD_PESO")
class UnidadPeso(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "COD_UNIDAD_PESO")
        var cod_unidad_peso: String,
        @ColumnInfo(name = "UNIDAD_PESO")
        var unidad_peso: String?=null
) {
    override fun toString(): String {
        return "$unidad_peso"
    }
}
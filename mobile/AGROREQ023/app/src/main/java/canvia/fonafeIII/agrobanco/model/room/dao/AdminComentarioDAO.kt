package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.Comentario

@Dao
interface AdminComentarioDAO {
    @Query("SELECT * FROM TB_COMENTARIO WHERE ID_VISITA = :id")
    fun selectComentarioItem(id:String): Comentario
    @Update
    fun actualizarComentario(registro: Comentario)
    @Insert
    fun insertarComentario(registro: Comentario)

    @Query("SELECT count(*) FROM TB_COMENTARIO WHERE ID_VISITA =:id")
    fun existeElementoComentario(id: String): Int
}
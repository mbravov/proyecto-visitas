package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.*
import canvia.fonafeIII.agrobanco.model.pojos.*

@Dao
interface AdminCreditoAgricolaIIIDAO {
    @Query("SELECT * FROM TB_CREDITO_AGRICOLA_III WHERE ID_VISITA = :id")
    fun selectCreditoAgricolaIIIItem(id:String): CreditoAgricolaIII
    @Update
    fun actualizarCreditoAgricolaIII( registro: CreditoAgricolaIII)
    @Insert
    fun insertarCreditoAgricolaIII(registro: CreditoAgricolaIII)

    @Query("SELECT count(*) FROM TB_CREDITO_AGRICOLA_III WHERE ID_VISITA =:id")
    fun existeElementoCreditoAgricolaIII(id: String): Int


    @Query("SELECT COD_CULTIVO,CULTIVO FROM TB_CULTIVO")
    fun selecListaCultivo():MutableList<Cultivo>

    @Transaction
    fun actuaizarDataCultivos(users: List<Cultivo>) {
        eliminarCultivos()
        insertarListaCultivos(users)
    }
    @Insert
    fun insertarListaCultivos(users: List<Cultivo>)
    @Query("DELETE FROM TB_CULTIVO")
    fun eliminarCultivos()


    @Query("SELECT COD_UNIDAD_PESO,UNIDAD_PESO FROM TB_UNIDAD_PESO")
    fun selecListaUnidadPeso():MutableList<UnidadPeso>

    @Transaction
    fun actuaizarDataUnidadPesos(users: List<UnidadPeso>) {
        eliminarUnidadPesos()
        insertarListaUnidadPesos(users)
    }
    @Insert
    fun insertarListaUnidadPesos(users: List<UnidadPeso>)
    @Query("DELETE FROM TB_UNIDAD_PESO")
    fun eliminarUnidadPesos()

    @Query("DELETE FROM TB_CREDITO_AGRICOLA_III WHERE ID_VISITA =:id")
    fun eliminarCreditoAgricolaIII(id: String)
}
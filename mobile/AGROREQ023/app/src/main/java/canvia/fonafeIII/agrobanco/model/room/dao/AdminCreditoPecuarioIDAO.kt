package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.*
import canvia.fonafeIII.agrobanco.model.pojos.CreditoPecuarioI
import canvia.fonafeIII.agrobanco.model.pojos.Cultivo
import canvia.fonafeIII.agrobanco.model.pojos.UnidadFinanciar

@Dao
interface AdminCreditoPecuarioIDAO {
    @Query("SELECT * FROM TB_CREDITO_PECUARIO_I WHERE ID_VISITA = :id")
    fun selectCreditoPecuarioIItem(id:String): CreditoPecuarioI
    @Update
    fun actualizarCreditoPecuarioI(registro: CreditoPecuarioI)
    @Insert
    fun insertarCreditoPecuarioI(registro: CreditoPecuarioI)

    @Query("SELECT count(*) FROM TB_CREDITO_PECUARIO_I WHERE ID_VISITA =:id")
    fun existeElementoCreditoPecuarioI(id: String): Int


    @Query("SELECT COD_UNIDAD_FINANCIAR,UNIDAD_FINANCIAR FROM TB_UNIDAD_FINANCIAR")
    fun selecListaUnidadFinanciar():MutableList<UnidadFinanciar>

    @Transaction
    fun actuaizarDataUnidadFinanciars(users: List<UnidadFinanciar>) {
        eliminarUnidadFinanciars()
        insertarListaUnidadFinanciars(users)
    }
    @Insert
    fun insertarListaUnidadFinanciars(users: List<UnidadFinanciar>)
    @Query("DELETE FROM TB_UNIDAD_FINANCIAR")
    fun eliminarUnidadFinanciars()

    @Query("DELETE FROM TB_CREDITO_PECUARIO_I  WHERE ID_VISITA =:id")
    fun eliminarCreditoPecuarioI(id: String)
}
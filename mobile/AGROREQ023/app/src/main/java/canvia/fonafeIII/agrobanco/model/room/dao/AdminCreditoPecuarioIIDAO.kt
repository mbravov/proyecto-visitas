package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.*
import canvia.fonafeIII.agrobanco.model.pojos.CreditoPecuarioII
import canvia.fonafeIII.agrobanco.model.pojos.Crianza
import canvia.fonafeIII.agrobanco.model.pojos.UnidadFinanciar

@Dao
interface AdminCreditoPecuarioIIDAO {
    @Query("SELECT * FROM TB_CREDITO_PECUARIO_II WHERE ID_VISITA = :id")
    fun selectCreditoPecuarioIIItem(id:String): CreditoPecuarioII
    @Update
    fun actualizarCreditoPecuarioII(registro: CreditoPecuarioII)
    @Insert
    fun insertarCreditoPecuarioII(registro: CreditoPecuarioII)

    @Query("SELECT count(*) FROM TB_CREDITO_PECUARIO_II WHERE ID_VISITA =:id")
    fun existeElementoCreditoPecuarioII(id: String): Int


    @Query("SELECT COD_CRIANZA,CRIANZA FROM TB_CRIANZA")
    fun selecListaCrianza():MutableList<Crianza>

    @Transaction
    fun actuaizarDataCrianzas(users: List<Crianza>) {
        eliminarCrianzas()
        insertarListaCrianzas(users)
    }
    @Insert
    fun insertarListaCrianzas(users: List<Crianza>)
    @Query("DELETE FROM TB_CRIANZA")
    fun eliminarCrianzas()

    @Query("DELETE FROM TB_CREDITO_PECUARIO_II  WHERE ID_VISITA =:id")
    fun eliminarCreditoPecuarioII(id: String)
}
package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.Firmas
import kotlinx.coroutines.flow.Flow

@Dao
interface AdminFirmasDAO {
    @Query("SELECT * FROM TB_FIRMA WHERE ID_VISITA = :id LIMIT 1")
    fun selectFirmasItem(id:String): Flow<Firmas?>
    @Query("SELECT * FROM TB_FIRMA WHERE ID_VISITA = :id LIMIT 1")
    fun selectFirmasItem2(id:String): Firmas
    @Update
    fun actualizarFirmas( registro: Firmas)
    @Insert
    fun insertarFirmas(registro: Firmas)

    @Query("SELECT count(*) FROM TB_FIRMA WHERE ID_VISITA =:id")
    fun existeElementoFirmas(id: String): Flow<Int>
}
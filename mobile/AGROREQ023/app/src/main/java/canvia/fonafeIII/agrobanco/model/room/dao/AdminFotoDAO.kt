package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

import canvia.fonafeIII.agrobanco.model.pojos.Imagen

@Dao
interface AdminFotoDAO {


    @Query("SELECT * FROM TB_FOTO WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden")
    fun selecTFotoItem(id:String, nroorden:String): Imagen

    @Query("SELECT * FROM TB_FOTO WHERE RUTA!='' and ID_VISITA in (SELECT t.ID_VISITA FROM TB_VISITA t WHERE Cast ((JulianDay(DateTIME('now')) - JulianDay(datetime(t.FECHA_CREA))) As Integer)>=:diaseliminar)")
    fun selectFotoListaEliminar(diaseliminar:Int?): List<Imagen>?

    @Query("SELECT * FROM TB_FOTO WHERE ID_VISITA =:id")
    fun selectFotoLista(id:String?): List<Imagen>?

    @Query("SELECT RUTA FROM TB_FOTO WHERE ID_VISITA =:id LIMIT 1")
    fun selectTopFotoPrincipal(id:String?): String?


    @Query("DELETE FROM TB_FOTO WHERE ID_VISITA = :id ")
    fun eliminarFotoPorIdVisita(id:String)

    @Query("DELETE FROM TB_FOTO WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden ")
    fun eliminarFotoPorItem(id:String, nroorden: String)






    @Query("SELECT count(*) FROM TB_FOTO WHERE ID_VISITA =:id AND NUMERO_ORDEN=:nroorden")
    fun existeElementoImagen(id: String,nroorden : String): Int


    @Update
    fun actualizarImagen(registro: Imagen)
    @Insert
    fun insertarImagen(registro: Imagen)





}
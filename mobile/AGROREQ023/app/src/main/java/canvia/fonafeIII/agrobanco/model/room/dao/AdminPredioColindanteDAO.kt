package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.PredioColindante

@Dao
interface AdminPredioColindanteDAO {
    @Query("SELECT * FROM TB_PREDIO_COLINDANTE WHERE ID_VISITA = :id")
    fun selectPredioColindanteItem(id:String): PredioColindante
    @Update
    fun actualizarPredioColindante(registro: PredioColindante)
    @Insert
    fun insertarPredioColindante(registro: PredioColindante)

    @Query("SELECT count(*) FROM TB_PREDIO_COLINDANTE WHERE ID_VISITA =:id")
    fun existeElementoPredioColindante(id: String): Int
}
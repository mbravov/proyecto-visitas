package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.*
import canvia.fonafeIII.agrobanco.model.pojos.Agencia
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada

import canvia.fonafeIII.agrobanco.model.pojos.Productor

@Dao
interface AdminProductorDAO {

    @Query("SELECT * FROM TB_DATOS_PRODUCTOR WHERE ID_VISITA = :id")
    fun selectProductorItem(id:String): Productor
    @Update
    fun actualizarProductor( registro: Productor)
    @Insert
    fun insertarProductor(registro: Productor)

    @Query("SELECT count(*) FROM TB_DATOS_PRODUCTOR WHERE ID_VISITA =:id")
    fun existeElementoProductor(id: String): Int


    @Query("SELECT COD_AGENCIA,OFICINA,OFICINA_REGIONAL FROM TB_AGENCIA")
    fun selecListaAgencia():MutableList<Agencia>


    @Transaction
     fun actuaizarDataAgencias(users: List<Agencia>) {
        eliminarAgencias()
        insertarListaAgencias(users)
    }
    @Insert
     fun insertarListaAgencias(users: List<Agencia>)
    @Query("DELETE FROM TB_AGENCIA")
     fun eliminarAgencias()






     @Query("SELECT * FROM TB_COORDENADA WHERE ID_VISITA =:id")
    fun selectCoordenadaLista(id:String?): List<Coordenada>?




}
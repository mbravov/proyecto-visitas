package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.Resumen

@Dao
interface AdminResumenDAO {
    @Query("SELECT * FROM TB_RESUMEN WHERE ID_VISITA = :id")
    fun selectResumenItem(id:String): Resumen
    @Update
    fun actualizarResumen(registro: Resumen)
    @Insert
    fun insertarResumen(registro: Resumen)

    @Query("SELECT count(*) FROM TB_RESUMEN WHERE ID_VISITA =:id")
    fun existeElementoResumen(id: String): Int
}
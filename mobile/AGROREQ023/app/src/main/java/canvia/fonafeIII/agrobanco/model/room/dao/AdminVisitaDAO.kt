package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
import canvia.fonafeIII.agrobanco.model.pojos.Visita

@Dao
interface AdminVisitaDAO {

    @Query("SELECT CODIGO_AGENCIA FROM TB_VISITA WHERE ID_VISITA = :id AND COD_ASIGNACION IS NOT NULL")
    fun selectAgenciaAsignada(id:String): Int?


    @Query("SELECT * FROM TB_VISITA WHERE ID_VISITA = :id")
       fun selectVisitaItem(id:String): Visita

    @Query("UPDATE TB_VISITA SET ESTADO=0 WHERE ID_USUARIO = :id AND COD_ASIGNACION IS NOT NULL")
    fun eliminarVisitasPorUsuario(id:String)

    @Query("UPDATE TB_VISITA SET ESTADO=0 WHERE ID_VISITA = :id")
    fun eliminarVisitasPorIDVisita(id:String)


    @Query("SELECT * FROM TB_VISITA WHERE Cast ((JulianDay(DateTIME('now')) - JulianDay(datetime(FECHA_CREA))) As Integer)>=:diaseliminar")
    fun selectVisitaListaImagenesEliminar(diaseliminar:Int?): List<Visita>?

    @Query("SELECT * FROM TB_VISITA WHERE ID_USUARIO =:id and ESTADO = 1 and Cast ((JulianDay(DateTIME('now')) - JulianDay(datetime(FECHA_CREA))) As Integer)<:diaseliminar")
    fun selectVisitaListaPorUsuario(id:String?, diaseliminar:Int?): List<Visita>?

    @Query("SELECT * FROM TB_VISITA WHERE ID_USUARIO =:id and ESTADO = 1 and INICIADO=1 and Cast ((JulianDay(DateTIME('now')) - JulianDay(datetime(FECHA_CREA))) As Integer)<:diaseliminar")
    fun selectVisitaListaPorUsuarioPendienteEnvio(id:String?, diaseliminar:Int?): List<Visita>?

    @Query("SELECT * FROM TB_VISITA WHERE ID_USUARIO =:id and ESTADO = 1 and COD_ASIGNACION IS NOT NULL and Cast ((JulianDay(DateTIME('now')) - JulianDay(datetime(FECHA_CREA))) As Integer)<:diaseliminar")
    fun selectVisitaListaPorUsuarioProximasVisitas(id:String?, diaseliminar:Int?): List<Visita>?

    @Query("UPDATE TB_VISITA SET ID_USUARIO=:usuario, USUARIO_MODIFICA=:usuario ,ESTADO=:estado, DNI_CLIENTE=:nro_doc, PRIMER_NOMBRE_CLIENTE=:primer_nombre, SEGUNDO_NOMBRE_CLIENTE=:segundo_nombre, APELLIDO_PATERNO_CLIENTE=:ape_pat, APELLIDO_MATERNO_CLIENTE=:ape_mat   WHERE COD_ASIGNACION = :cod_asignacion")
    fun actualizarVisitaDesdeServicio(usuario:String, cod_asignacion:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String,  estado:Int)

    @Query("UPDATE TB_VISITA SET  DNI_CLIENTE=:nro_doc, PRIMER_NOMBRE_CLIENTE=:primer_nombre, SEGUNDO_NOMBRE_CLIENTE=:segundo_nombre, APELLIDO_PATERNO_CLIENTE=:ape_pat, APELLIDO_MATERNO_CLIENTE=:ape_mat   WHERE COD_ASIGNACION = :cod_asignacion")
    fun actualizarEvalRegistroDesdeServicio(cod_asignacion:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String)


    @Query("UPDATE TB_VISITA SET SINCRONIZADO=1  WHERE ID_VISITA = :id_visita")
    fun actualizarDataVisitaSincronizadaOK(id_visita:String)

    @Query("UPDATE TB_VISITA SET RUTAIMAGEN=:ruta  WHERE ID_VISITA = :id_visita")
    fun actualizarFotoPrincipalVisita(ruta:String?, id_visita: String)

    @Query("UPDATE TB_VISITA SET NOMBRE_PREDIO=:nombre_predio , INICIADO=1 WHERE ID_VISITA = :id_visita")
    fun actualizarVisitaDatosDePredio(id_visita:String, nombre_predio:String)

    @Update
     fun actualizarVisita( registro: Visita)

    @Insert
     fun insertarVisita(registro: Visita)


}
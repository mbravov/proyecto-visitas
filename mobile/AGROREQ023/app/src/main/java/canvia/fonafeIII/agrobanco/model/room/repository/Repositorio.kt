package canvia.fonafeIII.agrobanco.model.room.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.asFlow
import androidx.lifecycle.liveData
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion.Companion.USUARIO
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.appdatabase.AppRoomDatabase
import canvia.fonafeIII.agrobanco.model.room.dao.AdminProductorDAO
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Singleton
import canvia.fonafeIII.agrobanco.util.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import java.lang.Exception

class Repositorio(val context: Context) {

    var db: AppRoomDatabase = AppRoomDatabase.getDatabase(context)
    private var adminVisitaDAO = db.AdminVisitaDAO()
    private var adminDAO = db.AdminDAO()
    private var adminEvalRegistroDAO = db.AdminEvalRegistroDAO()
    private var adminCoordenadaDAO = db.AdminCoordenadaDAO()
    private var adminFotoDAO = db.AdminFotoDAO()
    private var adminEvalAreaDAO = db.AdminEvalAreaDAO()
    private var adminFotoComplementaria= db.AdminFotoComplementariaDAO()
    private var adminProductorDAO= db.AdminProductorDAO()
    private var adminCreditoAgricolaIDAO= db.AdminCreditoAgricolaIDAO()
    private var adminCreditoAgricolaIIDAO= db.AdminCreditoAgricolaIIDAO()
    private var adminCreditoAgricolaIIIDAO= db.AdminCreditoAgricolaIIIDAO()
    private var adminCreditoPecuarioIDAO= db.AdminCreditoPecuarioIDAO()
    private var adminCreditoPecuarioIIDAO= db.AdminCreditoPecuarioIIDAO()
    private var adminPredioColindanteDAO= db.AdminPredioColindanteDAO()
    private var adminComentarioDAO= db.AdminComentarioDAO()
    private var adminFirmasDAO= db.AdminFirmasDAO()
    private var adminResumenDAO= db.AdminResumenDAO()


    fun  obtenerAgenciaSeleccionada(id:String ): Int?
    {
        var resultado: Int?
        resultado = adminVisitaDAO.selectAgenciaAsignada(id)

        return resultado

    }


    fun existeElementoRegistroInformacion(id: String): Boolean {
        var resultado: Boolean
        resultado = adminDAO.existeElementoRegistroInformacion(id) > 0

        return resultado
    }

    fun existeElementoVisita(id: String): Boolean {
        var resultado: Boolean
        resultado = adminDAO.existeElementoVisita(id) > 0
        return resultado
    }

    fun existeElementoVisitaPorCodAsignacion(id: String): Boolean {
        var resultado: Boolean
        resultado = adminDAO.existeElementoVisitaPorCodAsignacion(id) > 0
        return resultado
    }

    fun addElementoRegistroInformacion(registro: EvalRegistro) {

        if (existeElementoRegistroInformacion(registro.id_visita.toString())) {
        val us:String=    (context.applicationContext as AppConfiguracion).getUsuario()?.username.toString()
            registro.usuario_modifica = Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminDAO.actualizarRegistroInformacion(registro)
        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminDAO.insertarRegistroInformacion(registro)
        }


    }


    fun addElementoVisita(registro: Visita) {
        if (existeElementoVisita(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminVisitaDAO.actualizarVisita(registro)
        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminVisitaDAO.insertarVisita(registro)
        }


    }
    fun ActualizarVisitaDatosDePredio(id_visita: String,nombrePredio:String){

        adminVisitaDAO.actualizarVisitaDatosDePredio(id_visita,nombrePredio)
    }


    fun ActualizaElementoVisitaDesdeServicio(usuario:String, cod_asignacion:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String,  estado:Int) {

        adminVisitaDAO.actualizarVisitaDesdeServicio(usuario, cod_asignacion,nro_doc,ape_mat,ape_pat,primer_nombre,segundo_nombre,estado)
    }

    fun ActualizaElementoEvalRegistroDesdeServicio(cod_asignacion:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String) {

        adminVisitaDAO.actualizarEvalRegistroDesdeServicio(cod_asignacion,nro_doc,ape_mat,ape_pat,primer_nombre,segundo_nombre)
    }
    fun actualizaDataVisitaSincronizada(id_visita: String){

        adminVisitaDAO.actualizarDataVisitaSincronizadaOK(id_visita)
    }

    fun AgregarElementoVisita(registro: Visita) {
        adminVisitaDAO.insertarVisita(registro)

    }
    fun eliminarVisitaporUsuario(usuario: String) {
        adminVisitaDAO.eliminarVisitasPorUsuario(usuario)

    }

    fun eliminarVisitaporIDVisita(idvisita: String) {
        adminVisitaDAO.eliminarVisitasPorIDVisita(idvisita)
    }

    fun selectEvalRegistroItem(id: String): EvalRegistro? {
      var  resultado = adminEvalRegistroDAO.selectEvalRegistroItem(id)
        return resultado
    }


    fun selectVisitaItem(id: String): Visita {

        var  resultado = adminVisitaDAO.selectVisitaItem(id)
        return resultado
    }

    fun selectVisitaListaImagenesEliminar(): ArrayList<Visita>{

        var  resultado = adminVisitaDAO.selectVisitaListaImagenesEliminar(Utils().getSomeIntvalue("diaseliminar",15,true))
        return ArrayList(resultado)
    }

    fun selectVisitaListaPorUsuario(id: String): ArrayList<Visita>{

        var  resultado = adminVisitaDAO.selectVisitaListaPorUsuario(id,Utils().getSomeIntvalue("diaseliminar",15,true))
        return ArrayList(resultado)
    }

    fun selectVisitaListaPorUsuarioPendienteEnvio(id: String): ArrayList<Visita>{

        var  resultado = adminVisitaDAO.selectVisitaListaPorUsuarioPendienteEnvio(id,Utils().getSomeIntvalue("diaseliminar",15,true))
        return ArrayList(resultado)
    }

    fun selectVisitaListaPorUsuarioProximasVisitas(id: String): ArrayList<Visita>{

        Log.e("VisitaListaPorUsuario","diaseliminar:"+Utils().getSomeIntvalue("diaseliminar",15,true))
        Log.e("VisitaListaPorUsuario","idUsuario:"+id)

        var  resultado = adminVisitaDAO.selectVisitaListaPorUsuarioProximasVisitas(id,Utils().getSomeIntvalue("diaseliminar",15,true))
        return ArrayList(resultado)
    }

////////////////////COORDENADAS



    fun selectCoordenadaLista(id: String): ArrayList<Coordenada>{

        var  resultado = adminCoordenadaDAO.selectCoordenadaLista(id)

        return ArrayList(resultado)
    }



    fun selectCoordenadaItem(id: String, nroorden: String): Coordenada {

        var  resultado = adminCoordenadaDAO.selectCoordenadaItem(id,nroorden)
        return resultado
    }
    fun eliminarCoordenadaPorItem(id: String) {

        var  resultado = adminCoordenadaDAO.eliminarCoordenadaPorIdVisita(id)

    }

    fun existeElementoCoordenada(id: String,nroorden: String): Boolean {
        var resultado: Boolean
        resultado = adminCoordenadaDAO.existeElementoCoodenada(id,nroorden) > 0
        return resultado
    }


    fun addElementoCoordenada(registro: Coordenada) {
        if (existeElementoCoordenada(registro.id_visita.toString(),registro.numero_orden.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCoordenadaDAO.actualizarCoordenada(registro)
        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCoordenadaDAO.insertarCoordenada(registro)

        }


    }

    /////////EVAL_AREA



    fun existeElementoEvalArea(id: String): Boolean {
        var resultado: Boolean
        resultado = adminEvalAreaDAO.existeElementoEvalArea(id) > 0
        return resultado
    }

     fun addElementoEvalArea(registro: EvalArea) {
         if (existeElementoEvalArea(registro.id_visita.toString())) {
             registro.usuario_modifica =Singleton.getUser()
             registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
             adminEvalAreaDAO.actualizarEvalArea(registro)


         } else {
             registro.usuario_crea =Singleton.getUser()
             registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
             adminEvalAreaDAO.insertarEvalArea(registro)

         }
     }
         fun selectEvalAreaItem(id: String): EvalArea {
             var  resultado = adminEvalAreaDAO.selectEvalAreaItem(id)
             return resultado
         }

    fun selectEvalAreaItemSinImagen(id: String): EvalArea {
        var  resultado = adminEvalAreaDAO.selectEvalAreaItem_sinImagen(id)
        return resultado
    }

    fun getEvalAreaCaptura_mapa(id: String): String{
        var  resultado = adminEvalAreaDAO.getEvalAreaCaptura_mapa(id)

        if(resultado==null) resultado = ""

        return resultado
    }

    ///IMAGEN


    fun eliminarFotoPorIdVisita(id:String){

adminFotoDAO.eliminarFotoPorIdVisita(id)
    }

    fun insertarImagen(registro: Imagen){
        adminFotoDAO.insertarImagen(registro)
    }

    fun actualizarImagen(registro: Imagen){
        adminFotoDAO.actualizarImagen(registro)
    }

    fun actualizarImagenPrincipalVisita(ruta: String?, id_visita: String){
        if (ruta != null) {
            adminVisitaDAO.actualizarFotoPrincipalVisita(ruta,id_visita)
        }

    }

    fun selectFotoListaEliminar(): ArrayList<Imagen>{

        var  resultado = adminFotoDAO.selectFotoListaEliminar(Utils().getSomeIntvalue("diaseliminar",15,true))
        return ArrayList(resultado)
    }


    fun selectImagenLista(id: String): ArrayList<Imagen>{

        var  resultado = adminFotoDAO.selectFotoLista(id)

        return ArrayList(resultado)

    }


    fun selectTopFotoPrincipal(id_visita: String): String?{

        var resultado=selectTopFotoPrincipal(id_visita)
        return resultado
    }






/////IMAGEN_COMPLEMENTARIA

    fun eliminarFotoComplementariaPorIdVisita(id:String){

        adminFotoComplementaria.eliminarFotoComplementariaPorIdVisita(id)
    }

    fun insertarImagenComplementaria(registro: ImagenComplemtaria){
        adminFotoComplementaria.insertarImagenComplementaria(registro)
    }

    fun actualizarImagenComplementaria(registro: ImagenComplemtaria){
        adminFotoComplementaria.actualizarImagenComplementaria(registro)
    }

    fun selectImagenComplementariaLista(id: String): ArrayList<ImagenComplemtaria>{

        var  resultado = adminFotoComplementaria.selectFotoComplementariaLista(id)

        return ArrayList(resultado)

    }

    fun selectFotoComplementariaListaEliminar(): ArrayList<ImagenComplemtaria>{

        var  resultado = adminFotoComplementaria.selectFotoComplementariaListaEliminar(Utils().getSomeIntvalue("diaseliminar",15,true))

        return ArrayList(resultado)

    }

/////DATOS_PRODUCTOR


    fun existeElementoProductor(id: String): Boolean {
        var resultado: Boolean
        resultado = adminProductorDAO.existeElementoProductor(id) > 0
        return resultado
    }

    fun addElementoProductor(registro: Productor) {
        if (existeElementoProductor(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminProductorDAO.actualizarProductor(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminProductorDAO.insertarProductor(registro)

        }
    }
    fun selectEvalProductor(id: String): Productor {
        var  resultado = adminProductorDAO.selectProductorItem(id)
        return resultado
    }

    fun selectListaAgencias(): MutableList<Agencia> {
        var  resultado = adminProductorDAO.selecListaAgencia()
        return resultado
    }

    fun actualizarListaAgencias(list: List<Agencia> ) {
         adminProductorDAO.actuaizarDataAgencias(list)

    }

//// CREDITO_AGRICOLA_I



    fun existeElementoCreditoAgricolaI(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIDAO.existeElementoCreditoAgricolaI(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaI(registro: CreditoAgricolaI) {
        if (existeElementoCreditoAgricolaI(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIDAO.actualizarCreditoAgricolaI(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIDAO.insertarCreditoAgricolaI(registro)

        }
    }
    fun selectEvalCreditoAgricolaI(id: String): CreditoAgricolaI {
        var  resultado = adminCreditoAgricolaIDAO.selectCreditoAgricolaIItem(id)
        return resultado
    }

    fun eliminarCreditoAgricolaI(registro: CreditoAgricolaI){
        if (existeElementoCreditoAgricolaI(registro.id_visita.toString())) {
            adminCreditoAgricolaIDAO.eliminarCreditoAgricolaI(registro.id_visita.toString())
        }
    }


//// CREDITO_AGRICOLA_II

    fun existeElementoCreditoAgricolaII(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIIDAO.existeElementoCreditoAgricolaII(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaII(registro: CreditoAgricolaII) {
        if (existeElementoCreditoAgricolaII(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIDAO.actualizarCreditoAgricolaII(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIDAO.insertarCreditoAgricolaII(registro)

        }
    }
    fun selectEvalCreditoAgricolaII(id: String): CreditoAgricolaII {
        var  resultado = adminCreditoAgricolaIIDAO.selectCreditoAgricolaIIItem(id)
        return resultado
    }

    fun eliminarCreditoAgricolaII(registro: CreditoAgricolaII){
        if (existeElementoCreditoAgricolaII(registro.id_visita.toString())) {
            adminCreditoAgricolaIIDAO.eliminarCreditoAgricolaII(registro.id_visita.toString())
        }
    }


//// CREDITO_AGRICOLA_III

    fun existeElementoCreditoAgricolaIII(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIIIDAO.existeElementoCreditoAgricolaIII(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaIII(registro: CreditoAgricolaIII) {
        if (existeElementoCreditoAgricolaIII(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIIDAO.actualizarCreditoAgricolaIII(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIIDAO.insertarCreditoAgricolaIII(registro)

        }
    }
    fun selectEvalCreditoAgricolaIII(id: String): CreditoAgricolaIII {
        var  resultado = adminCreditoAgricolaIIIDAO.selectCreditoAgricolaIIIItem(id)
        return resultado
    }

    fun selectListaCultivos(): MutableList<Cultivo> {
        var  resultado = adminCreditoAgricolaIIIDAO.selecListaCultivo()
        return resultado
    }

    fun actualizarListaCultivos(list: List<Cultivo> ) {
        adminCreditoAgricolaIIIDAO.actuaizarDataCultivos(list)

    }

    fun selectListaUnidadPesos(): MutableList<UnidadPeso> {
        var  resultado = adminCreditoAgricolaIIIDAO.selecListaUnidadPeso()
        return resultado
    }

    fun actualizarListaUnidadPesos(list: List<UnidadPeso> ) {
        adminCreditoAgricolaIIIDAO.actuaizarDataUnidadPesos(list)

    }

    fun eliminarCreditoAgricolaIII(registro: CreditoAgricolaIII){
        if (existeElementoCreditoAgricolaIII(registro.id_visita.toString())) {
            adminCreditoAgricolaIIIDAO.eliminarCreditoAgricolaIII(registro.id_visita.toString())
        }
    }

//// CREDITO_PECUARIO_I

    fun existeElementoCreditoPecuarioI(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoPecuarioIDAO.existeElementoCreditoPecuarioI(id) > 0
        return resultado
    }

    fun addElementoCreditoPecuarioI(registro: CreditoPecuarioI) {
        if (existeElementoCreditoPecuarioI(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoPecuarioIDAO.actualizarCreditoPecuarioI(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoPecuarioIDAO.insertarCreditoPecuarioI(registro)

        }
    }
    fun selectEvalCreditoPecuarioI(id: String): CreditoPecuarioI {
        var  resultado = adminCreditoPecuarioIDAO.selectCreditoPecuarioIItem(id)
        return resultado
    }

    fun selectListaUnidadFinanciars(): MutableList<UnidadFinanciar> {
        var  resultado = adminCreditoPecuarioIDAO.selecListaUnidadFinanciar()
        return resultado
    }

    fun actualizarListaUnidadFinanciars(list: List<UnidadFinanciar> ) {
        adminCreditoPecuarioIDAO.actuaizarDataUnidadFinanciars(list)

    }

    fun eliminarCreditoPecuarioI(registro: CreditoPecuarioI){
        if (existeElementoCreditoPecuarioI(registro.id_visita.toString())) {
            adminCreditoPecuarioIDAO.eliminarCreditoPecuarioI(registro.id_visita.toString())
        }
    }

//// CREDITO_PECUARIO_II

    fun existeElementoCreditoPecuarioII(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoPecuarioIIDAO.existeElementoCreditoPecuarioII(id) > 0
        return resultado
    }

    fun addElementoCreditoPecuarioII(registro: CreditoPecuarioII) {
        if (existeElementoCreditoPecuarioII(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoPecuarioIIDAO.actualizarCreditoPecuarioII(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoPecuarioIIDAO.insertarCreditoPecuarioII(registro)

        }
    }
    fun selectEvalCreditoPecuarioII(id: String): CreditoPecuarioII {
        var  resultado = adminCreditoPecuarioIIDAO.selectCreditoPecuarioIIItem(id)
        return resultado
    }

    fun selectListaCrianzas(): MutableList<Crianza> {
        var  resultado = adminCreditoPecuarioIIDAO.selecListaCrianza()
        return resultado
    }

    fun actualizarListaCrianzas(list: List<Crianza> ) {
        adminCreditoPecuarioIIDAO.actuaizarDataCrianzas(list)

    }

    fun eliminarCreditoPecuarioII(registro: CreditoPecuarioII){
        if (existeElementoCreditoPecuarioII(registro.id_visita.toString())) {
            adminCreditoPecuarioIIDAO.eliminarCreditoPecuarioII(registro.id_visita.toString())
        }
    }

//// PREDIO_COLINDANTE

    fun existeElementoPredioColindante(id: String): Boolean {
        var resultado: Boolean
        resultado = adminPredioColindanteDAO.existeElementoPredioColindante(id) > 0
        return resultado
    }

    fun addElementoPredioColindante(registro: PredioColindante) {
        if (existeElementoPredioColindante(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminPredioColindanteDAO.actualizarPredioColindante(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminPredioColindanteDAO.insertarPredioColindante(registro)

        }
    }
    fun selectEvalPredioColindante(id: String): PredioColindante {
        var  resultado = adminPredioColindanteDAO.selectPredioColindanteItem(id)
        return resultado
    }


//// COMENTARIO

    fun existeElementoComentario(id: String): Boolean {
        var resultado: Boolean
        resultado = adminComentarioDAO.existeElementoComentario(id) > 0
        return resultado
    }

    fun addElementoComentario(registro: Comentario) {
        if (existeElementoComentario(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminComentarioDAO.actualizarComentario(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminComentarioDAO.insertarComentario(registro)

        }
    }
    fun selectEvalComentario(id: String): Comentario {
        var  resultado = adminComentarioDAO.selectComentarioItem(id)
        return resultado
    }

//// FIRMAS

    fun existeElementoFirmas(id: String) = adminFirmasDAO.existeElementoFirmas(id).map {
        Log.i("Cantidad de Items:", it.toString())
        it > 0
    }

    suspend fun addElementoFirmas(registro: Firmas) = flow {
        existeElementoFirmas(registro.id_visita).distinctUntilChanged().collect {
            if (it) {
                registro.usuario_modifica =Singleton.getUser()
                registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
                try {
                    adminFirmasDAO.actualizarFirmas(registro)
                    emit(true)
                } catch (e: Exception) {
                    emit(false)
                }
            } else {
                registro.usuario_crea =Singleton.getUser()
                registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
                try {
                    adminFirmasDAO.insertarFirmas(registro)
                    emit(true)
                } catch (e: Exception) {
                    emit(false)
                }
            }
        }
    }

    fun selectEvalFirmas(id: String) = adminFirmasDAO.selectFirmasItem(id)

    fun selectEvalFirmas2(id: String) = adminFirmasDAO.selectFirmasItem2(id)




//// RESUMEN

    fun existeElementoResumen(id: String): Boolean {
        var resultado: Boolean
        resultado = adminResumenDAO.existeElementoResumen(id) > 0
        return resultado
    }

    fun addElementoResumen(registro: Resumen) {
        if (existeElementoResumen(registro.id_visita.toString())) {
            registro.usuario_modifica =Singleton.getUser()
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminResumenDAO.actualizarResumen(registro)


        } else {
            registro.usuario_crea =Singleton.getUser()
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminResumenDAO.insertarResumen(registro)

        }
    }
    fun selectEvalResumen(id: String): Resumen {
        var  resultado = adminResumenDAO.selectResumenItem(id)
        return resultado
    }
}














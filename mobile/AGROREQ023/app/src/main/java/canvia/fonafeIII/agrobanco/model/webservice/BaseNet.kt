package canvia.fonafeIII.agrobanco.model.webservice

import android.util.Log
import canvia.fonafeIII.agrobanco.model.webservice.configuracion.Configuracion
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

class BaseNet {


      fun <T> createBaseSeguridad(service: Class<T>?): T {

        val gson: Gson = GsonBuilder()
            .setLenient()
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(Configuracion.BASE_SECURITY_URL.toString())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
            .build()
        return retrofit.create(service)
    }

    fun <T> createBaseVisita(service: Class<T>?): T {


        val httpClient = OkHttpClient.Builder()
            .connectTimeout(20,TimeUnit.MINUTES)
            .callTimeout(20,TimeUnit.MINUTES)
            .readTimeout(20,TimeUnit.MINUTES)
            .writeTimeout(20,TimeUnit.MINUTES)

        httpClient.addInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .method(original.method(), original.body())
                    .build()
            chain.proceed(request)
        }

        httpClient.addInterceptor(LoggingInterceptor())

        val client = httpClient.build()


        val gson: Gson = GsonBuilder()
            .setLenient()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()


        val retrofit = Retrofit.Builder()
            .baseUrl(Configuracion.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
            .build()
        return retrofit.create(service)
    }


    class LoggingInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            Log.i("LoggingInterceptor", "inside intercept callback")
            val request = chain.request()
            val t1 = System.nanoTime()
            var requestLog = String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers())
            if (request.method().compareTo("post", ignoreCase = true) == 0) {
                requestLog = """
                
                $requestLog
                ${bodyToString(request)}
                """.trimIndent()
            }
            Log.d("TAG", "request\n$requestLog")
            val response = chain.proceed(request)
            val t2 = System.nanoTime()
            val responseLog = String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6, response.headers())
            val bodyString = response.body()!!.string()
            Log.d("TAG", "response only\n$bodyString")
            Log.d("TAG", "response\n$responseLog\n$bodyString")
            return response.newBuilder()
                    .body(ResponseBody.create(response.body()!!.contentType(), bodyString))
                    .build()
        }

        fun bodyToString(request: Request): String {
            return try {
                val copy = request.newBuilder().build()
                val buffer = Buffer()
                copy.body()!!.writeTo(buffer)
                buffer.readUtf8()
            } catch (e: IOException) {
                "did not work"
            }
        }
    }
}
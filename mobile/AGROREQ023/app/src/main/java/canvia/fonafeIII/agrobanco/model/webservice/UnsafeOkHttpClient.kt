package canvia.fonafeIII.agrobanco.model.webservice

import okhttp3.OkHttpClient
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

class UnsafeOkHttpClient {

    companion object {
        fun getUnsafeOkHttpClient(): OkHttpClient {
            val xTrustManager = object: X509TrustManager {
                override fun checkClientTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {

                }

                override fun checkServerTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {

                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }

            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, arrayOf(xTrustManager), SecureRandom())

            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder().apply {
                sslSocketFactory(sslSocketFactory, xTrustManager)
                hostnameVerifier { _, _ -> true }
                connectTimeout(20, TimeUnit.MINUTES)
                callTimeout(20, TimeUnit.MINUTES)
                readTimeout(20, TimeUnit.MINUTES)
                writeTimeout(20, TimeUnit.MINUTES)
                addInterceptor(BaseNet.LoggingInterceptor())
            }

            return builder.build()
        }
    }

}
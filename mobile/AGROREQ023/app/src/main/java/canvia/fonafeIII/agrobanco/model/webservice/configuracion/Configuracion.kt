package canvia.fonafeIII.agrobanco.model.webservice.configuracion

import canvia.fonafeIII.agrobanco.BuildConfig

object Configuracion {

    const val BASE_URL = BuildConfig.BASE_URL
    const val BASE_SECURITY_URL = BuildConfig.BASE_SECURITY_URL
    const val API_AUTENTIFICATE = BuildConfig.API_AUTENTIFICATE
    const val HEADERS_APP_KEY = BuildConfig.HEADERS_APP_KEY
    const val HEADERS_APP_CODE = BuildConfig.HEADERS_APP_CODE
    const val LOGIN_URL = BuildConfig.LOGIN_URL
    const val API_ACCESO = BuildConfig.API_ACCESO
    const val API_VERSION = BuildConfig.API_VERSION
    const val API_DOWNLOAD_VISITAS = BuildConfig.API_DOWNLOAD_VISITAS
    const val API_UPLOAD = BuildConfig.API_UPLOAD
    const val API_DOWNLOAD_PARAMETROS = BuildConfig.API_DOWNLOAD_PARAMETROS
    const val MENU_ITEM_PROXIMAS_VISITAS = BuildConfig.MENU_ITEM_PROXIMAS_VISITAS
    const val MENU_ITEM_EVALUACION_INICIAL = BuildConfig.MENU_ITEM_EVALUACION_INICIAL
    const val MENU_ITEM_PENDIENTE_ENVIO = BuildConfig.MENU_ITEM_PENDIENTE_ENVIO


    const val API_DOWNLOAD_AGENCIAS = "api/v1/parametro/agencias"
    const val API_DOWNLOAD_CULTIVOS = "api/v1/parametro/cultivos"
    const val API_DOWNLOAD_UNIDADPESOS = "api/v1/parametro/unidadpesos"
    const val API_DOWNLOAD_UNIDADFINANCIAR = "api/v1/parametro/unidadfinanciars"
    const val API_DOWNLOAD_CRIANZAS = "api/v1/parametro/crianzas"
}
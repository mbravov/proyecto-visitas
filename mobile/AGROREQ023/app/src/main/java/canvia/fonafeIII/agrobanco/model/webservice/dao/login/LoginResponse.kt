package canvia.fonafeIII.agrobanco.model.webservice

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(

	@field:SerializedName("vUsuarioWeb")
	val vUsuarioWeb: String? = null,

	@field:SerializedName("eServicioResponse")
	val eServicioResponse: EServicioResponse? = null,

	@field:SerializedName("vUsuarioAD")
	val vUsuarioAD: String? = null,

	@field:SerializedName("vToken")
	val vToken: String? = null,

	@field:SerializedName("vAgenciaAsignada")
	val vAgenciaAsignada: String? = null,

	@field:SerializedName("agencias")
	val agencias: List<AgenciasItem?>? = null,

	@field:SerializedName("vId")
	val vId: String? = null,

	@field:SerializedName("vPerfilDescripcion")
	val vPerfilDescripcion: String? = null,

	@field:SerializedName("vDescripcionAgencia")
	val vDescripcionAgencia: String? = null,

	@field:SerializedName("vCodFuncionario")
	val vCodFuncionario: String? = null,

	@field:SerializedName("vNombre")
	val vNombre: String? = null,

	@field:SerializedName("listaModulo")
	val listaModulo: List<ListaModuloItem?>? = null,

	@field:SerializedName("listUsuarioPerfil")
	val listUsuarioPerfil: List<ListUsuarioPerfilItem?>? = null,

	@field:SerializedName("vEmail")
	val vEmail: String? = null,

	@field:SerializedName("vEstadoAD")
	val vEstadoAD: String? = null,

	@field:SerializedName("vPassword")
	val vPassword: String? = null,

	@field:SerializedName("vCargo")
	val vCargo: String? = null
) : Parcelable

@Parcelize
data class EServicioResponse(

	@field:SerializedName("resultado")
	var resultado: Int? = null,

	@field:SerializedName("mensaje")
	var mensaje: String? = null
) : Parcelable

@Parcelize
data class ListaModuloItem(

	@field:SerializedName("urlOpcion")
	val urlOpcion: String? = null,

	@field:SerializedName("idAplicacion")
	val idAplicacion: String? = null,

	@field:SerializedName("controller")
	val controller: String? = null,

	@field:SerializedName("estadoLogicoOpcion")
	val estadoLogicoOpcion: String? = null,

	@field:SerializedName("tipoIconoCodigo")
	val tipoIconoCodigo: String? = null,

	@field:SerializedName("idRelacion")
	val idRelacion: String? = null,

	@field:SerializedName("rutaFisicaIcono")
	val rutaFisicaIcono: String? = null,

	@field:SerializedName("nombrePerfil")
	val nombrePerfil: String? = null,

	@field:SerializedName("tipoOpcion")
	val tipoOpcion: String? = null,

	@field:SerializedName("nombreAplicacion")
	val nombreAplicacion: String? = null,

	@field:SerializedName("urlAplicacion")
	val urlAplicacion: String? = null,

	@field:SerializedName("descripcionAplicacion")
	val descripcionAplicacion: String? = null,

	@field:SerializedName("correlativoOpcion")
	val correlativoOpcion: String? = null,

	@field:SerializedName("tipoIconoDescripcion")
	val tipoIconoDescripcion: String? = null,

	@field:SerializedName("idOpcion")
	val idOpcion: String? = null,

	@field:SerializedName("estadoOpcion")
	val estadoOpcion: String? = null,

	@field:SerializedName("idPerfil")
	val idPerfil: String? = null,

	@field:SerializedName("nombreOpcion")
	val nombreOpcion: String? = null,

	@field:SerializedName("action")
	val action: String? = null
) : Parcelable


@Parcelize
data class AgenciasItem(

	@field:SerializedName("vCodigo")
	val vCodigo: String? = null,

	@field:SerializedName("descripcion")
	val descripcion: String? = null,

	@field:SerializedName("valor")
	val valor: String? = null,

	@field:SerializedName("nombreRegional")
	val nombreRegional: String? = null
) : Parcelable

@Parcelize
data class ListUsuarioPerfilItem(

	@field:SerializedName("nombresUsuario")
	val nombresUsuario: String? = null,

	@field:SerializedName("idAplicacion")
	val idAplicacion: String? = null,

	@field:SerializedName("idPerfilSelected")
	val idPerfilSelected: String? = null,

	@field:SerializedName("idPerfil")
	val idPerfil: String? = null,

	@field:SerializedName("idUsuario")
	val idUsuario: String? = null,

	@field:SerializedName("estadoPerfil")
	val estadoPerfil: String? = null,

	@field:SerializedName("correlativoPerfil")
	val correlativoPerfil: Int? = null,

	@field:SerializedName("nombrePerfil")
	val nombrePerfil: String? = null,

	@field:SerializedName("usuarioWeb")
	val usuarioWeb: String? = null,

	@field:SerializedName("nombreAplicacion")
	val nombreAplicacion: String? = null,

	@field:SerializedName("urlAplicacion")
	val urlAplicacion: String? = null
) : Parcelable

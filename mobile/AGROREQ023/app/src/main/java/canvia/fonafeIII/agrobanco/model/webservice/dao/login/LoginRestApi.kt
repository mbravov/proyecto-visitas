package canvia.fonafeIII.agrobanco.model.webservice.dao.login

import canvia.fonafeIII.agrobanco.model.webservice.LoginResponse
import canvia.fonafeIII.agrobanco.model.webservice.TokenResponse
import canvia.fonafeIII.agrobanco.model.webservice.configuracion.Configuracion
import retrofit2.Call
import retrofit2.http.*

interface LoginRestApi {

    @POST(Configuracion.API_AUTENTIFICATE.toString())
    fun Login(@Body user: LoginRequest): Call<LoginResponse?>?

    @Headers(
        "Content-Type:application/json",
        Configuracion.HEADERS_APP_KEY.toString(),
        Configuracion.HEADERS_APP_CODE.toString()
    )
    @POST(Configuracion.API_ACCESO.toString())
    fun ObtenerToken(@Body data: TokenRequest?): Call<TokenResponse?>?
}
package canvia.fonafeIII.agrobanco.model.webservice.dao.login

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TokenRequest(

	@field:SerializedName("codigoUsuario")
	val codigoUsuario: String? = null,

	@field:SerializedName("nombreUsuario")
	val nombreUsuario: String? = null,

	@field:SerializedName("numeroDocumento")
	val numeroDocumento: String? = null,

	@field:SerializedName("correoElectronico")
	val correoElectronico: String? = null
) : Parcelable

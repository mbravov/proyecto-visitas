package canvia.fonafeIII.agrobanco.model.webservice

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TokenResponse(

	@field:SerializedName("authAccess")
	val authAccess: AuthAccess? = null
) : Parcelable

@Parcelize
data class AuthAccess(

	@field:SerializedName("codigoUsuario")
	val codigoUsuario: String? = null,

	@field:SerializedName("fechaInicioVigencia")
	val fechaInicioVigencia: String? = null,

	@field:SerializedName("nombreUsuario")
	val nombreUsuario: String? = null,

	@field:SerializedName("numeroDocumento")
	val numeroDocumento: String? = null,

	@field:SerializedName("fechaFinVigencia")
	val fechaFinVigencia: String? = null,

	@field:SerializedName("correoElectronico")
	val correoElectronico: String? = null
) : Parcelable

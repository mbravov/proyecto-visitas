package canvia.fonafeIII.agrobanco.model.webservice.dao.splash

import canvia.fonafeIII.agrobanco.model.webservice.LoginResponse
import canvia.fonafeIII.agrobanco.model.webservice.TokenResponse
import canvia.fonafeIII.agrobanco.model.webservice.configuracion.Configuracion
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.LoginRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.TokenRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface SplashRestApi {



    @POST(Configuracion.API_VERSION.toString())
    fun ValidarVersion(@Body data: VersionRequest?): Call<VersionRequest?>?

}
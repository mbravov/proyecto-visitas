package canvia.fonafeIII.agrobanco.model.webservice.dao.splash

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VersionRequest(

	@field:SerializedName("resultado")
	val resultado: Int? = null,

	@field:SerializedName("version")
	val version: String? = null
) : Parcelable

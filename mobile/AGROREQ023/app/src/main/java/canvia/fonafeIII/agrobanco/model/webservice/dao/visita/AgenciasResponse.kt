package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AgenciasResponse(
	val codigo: Int? = null,
	val oficina: String? = null,
	val oficinaRegional: String? = null
) : Parcelable

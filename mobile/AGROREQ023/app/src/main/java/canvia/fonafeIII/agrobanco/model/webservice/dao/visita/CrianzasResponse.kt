package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CrianzasResponse(
        val codigo: String? = null,
        val crianza: String? = null
) : Parcelable
package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CultivosResponse(
        val codigo: String? = null,
        val cultivo: String? = null
) : Parcelable
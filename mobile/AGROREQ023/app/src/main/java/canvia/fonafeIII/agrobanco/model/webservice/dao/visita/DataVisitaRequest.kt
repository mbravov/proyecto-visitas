package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataVisitaRequest(

	@field:SerializedName("NumDocumentoCliente")
	var numdocumentocliente: String? = "",

	@field:SerializedName("CodigoAgencia")
	var codigoagencia: Int? = -1,

	@field:SerializedName("CodigoAsignacion")
	var codigoasignacion: Int? = -1,

	@field:SerializedName("TipoVisita")
	var tipovisita: Int? = 1,

	@field:SerializedName("UsuarioCreacionVisita")
	var usuariocreacionvisita: String? = "",

	@field:SerializedName("CodigoVisitaMobile")
	var codvisitamobile: String? = "",

	@field:SerializedName("CodigoVisita")
	var codvisita: Int? = 0,

	@field:SerializedName("PrimerNombrecliente")
	var primernombrecliente: String? = "",

	@field:SerializedName("SegundoNombrecliente")
	var segundonombrecliente: String? = "",

	@field:SerializedName("ApellidoPaternoCliente")
	var apellidopaternocliente: String? = "",

	@field:SerializedName("ApellidoMaternoCliente")
	var apellidomaternocliente: String? = "",


	@field:SerializedName("NombrePredio")
	var nombrepredio : String? = "",

	@field:SerializedName("DireccionPredio")
	var direccionpredio: String? = "",

	@field:SerializedName("TipoActividad")
	var tipoactividad: Int? = -1,

	@field:SerializedName("TipoMapa")
	var tipomapa: Int? = -1,

	@field:SerializedName("ModoCaptura")
	var modocaptura: Int? = -1,

	@field:SerializedName("NumPuntosMapa")
	var numpuntosmapa: Int? = -1,

	@field:SerializedName("NumFotosPrincipales")
	var numfotosprincipales: Int? = -1,

	@field:SerializedName("NumFotosSecundarias")
	var numfotossecundarias: Int? = -1,

	@field:SerializedName("AreaTotalPuntos")
	var areatotalpuntos: Double? = -1.0,

	@field:SerializedName("FechaVisita")
	var fechavisita: String? = "2000-01-01T00:00:00",

	@field:SerializedName("CodigoAgenciaAsignada")
	var codigoagenciaasignada: Int? = -1,

	@field:SerializedName("DescObjetivoVisita")
	var descobjetivovisita: String? = "",

	@field:SerializedName("PersonaPresente")
	var presenteenvisita: Int? = -1, //presente en visita

	@field:SerializedName("VinculoFamiliar")
	var vinculofamiliar: String? = "",

	@field:SerializedName("NombreFamiliar")
	var nombrefamiliar: String? = "",

	@field:SerializedName("NumCelFamiliar")
	var numcelfamiliar: Int? = 999999999,
	@field:SerializedName("MercadoAcceso")
	var mercadoacceso: Int? = -1,

	@field:SerializedName("AreaProtegida")
	var areaprotegida: Int? = -1,

	@field:SerializedName("NumParcelas")
	var numparcelas: Int? = -1,

	@field:SerializedName("NumHectareasSembradas")
	var numhectareassembradas: Double? = -1.0,

	@field:SerializedName("UnidadCatastral")
	var unidadcatastral: String? = "",

	@field:SerializedName("NumHectareasFinanciar")
	var numhectareasfinanciar: Double? = -1.0,

	@field:SerializedName("NumHectareasTotales")
	var numhectareastotales: Double? = -1.0,

	@field:SerializedName("RegimenTenencia")
	var regimentenencia: Int? = -1,


	@field:SerializedName("TipoUbicacion")
	var tipoubicacion: Int? = -1,

	@field:SerializedName("EstadoCampo")
	var estadocampo: Int? = -1,

	@field:SerializedName("TipoRiego")
	var tiporiego: Int? = -1,

	@field:SerializedName("DisponibilidadAgua")
	var disponibilidadagua: Int? = -1,

	@field:SerializedName("Pendiente")
	var pendiente: Int? = -1,

	@field:SerializedName("TipoSuelo")
	var tiposuelo: Int? = -1,
	@field:SerializedName("AccesibilidadPredio")
	var accesibilidadpredio: Int? = -1,

	@field:SerializedName("AltitudPredio")
	var altitudpredio: Int? = -1,

	@field:SerializedName("Cultivo")
	var cultivo: String? = "",

	@field:SerializedName("Variedad")
	var variedad: String? = "",


	@field:SerializedName("FechaSiembra")
	var fechasiembra: String? = "2000-01-01T00:00:00", //tipo de dato FEcha

	@field:SerializedName("aniosexp")
	var aniosexp : Int? = -1,

	@field:SerializedName("TipoSiembra")
	var tiposiembra: Int? = -1,

	@field:SerializedName("RendimientoAnterior")
	var rendimientoanterior: Double? = -1.0,
	@field:SerializedName("RendimientoEsperado")
	var rendimientoesperado: Double? = -1.0,
	@field:SerializedName("unidadpesocod")
	var unidadpesocod : String? = "",

	@field:SerializedName("UnidadFinanciar")
	var unidadfinanciar: String? = "",
	@field:SerializedName("unidadfinanciarcod")
	var unidadfinanciarcod:  String? = "",

	@field:SerializedName("NumUnidadesFinanciar")
	var numunidadesfinanciar: Int? = -1,

	@field:SerializedName("NumTotalUnidades")
	var numtotalunidades: Int? = -1,


	@field:SerializedName("UnidadesProductivas")
	var unidadesproductivas: String? = "",

	@field:SerializedName("TipoAlimentacion")
	var tipoalimentacion: Int? = -1,

	@field:SerializedName("TipoManejo")
	var tipomanejo: Int? = -1,


	@field:SerializedName("TipoFuenteAgua")
	var tipofuenteagua: Int? = -1,

	@field:SerializedName("DisponibilidadAguaPecuario")
	var disponibilidadaguapecuario: Int? = -1,

	@field:SerializedName("TipoCrianza")
	var tipocrianza: String? = "",

	@field:SerializedName("Raza")
	var raza: String? = "",  //lo tengo como string

	@field:SerializedName("aniosexppecuario")
	var aniosexppecuario : Int? = -1,

	@field:SerializedName("TipoTecnologia")
	var tipotecnologia: Int? = -1,


	@field:SerializedName("TipoManejoPecuario")
	var tipomanejopecuario: Int? = -1,

	@field:SerializedName("ComentPrediosColindantes")
	var comentpredioscolindantes: String? = "",

	@field:SerializedName("ComentRecomendaciones")
	var comentrecomendaciones: String? = "",

	@field:SerializedName("FirmaTitular")
	var firmatitular: String? = "",

	@field:SerializedName("FirmaConyugue")
	var firmaconyugue: String? = "",


	@field:SerializedName("FechaInicioVisita")
	var fechainiciovisita: String? = "2000-01-01T00:00:00",
	@field:SerializedName("PrecisionInicioVisita")
	var precicionInicioVisita: String? = "",
	@field:SerializedName("LatInicioVisita")
	var latiniciovisita: String? = "",
	@field:SerializedName("LongInicioVisita")
	var longiniciovisita: String? = "",


	@field:SerializedName("FechaFinVisita")
	var fechafinvisita: String? = "2000-01-01T00:00:00",
	@field:SerializedName("PrecisionFinVisita")
	var precicionfinvisita: String? = "",
	@field:SerializedName("latfinvisita")
	var latfinvisita: String? = "",
	@field:SerializedName("longfinvisita")
	var longfinvisita: String? = "",

	@field:SerializedName("ListaCoordenadas")
	var coordenadas: List<CoordenadasItem?>? = null,
	@field:SerializedName("ListaImagenes")
	var imagen: List<ImagenItem?>? = null,

	@field:SerializedName("capturamapa")
	var capturamapa: String? = ""

) : Parcelable

@Parcelize
data class CoordenadasItem(

	@field:SerializedName("Codigo")
	var codigo: Int? = -1,

	@field:SerializedName("CodigoCoordenadaMobile")
	var codigocoordenadamobile: String? = "",

	@field:SerializedName("CodigoVisita")
	var codigovisita: Int? = 0,

	@field:SerializedName("NumOrden")
	var numorden: Int? = -1,

	@field:SerializedName("Precision")
	var presicion: String? = "",

	@field:SerializedName("Longitud")
	var longitud: String? = "",

	@field:SerializedName("Latitud")
	var latitud: String? = "",

	@field:SerializedName("Altitud")
	var altitud: String? = ""

) : Parcelable

@Parcelize
data class ImagenItem(

	@field:SerializedName("Codigo")
	var codigo: Int? = -1,

	@field:SerializedName("CodigoVisita")
	var codigovisita: Int? = 0,

	@field:SerializedName("CodigoFotoMobile")
	var codigofotomobile: String? = "",

	@field:SerializedName("NumOrdenCaptura")
	var numordencaptura: Int? = -1,

	@field:SerializedName("LongCapturaImagen")
	var longcapturaimagen: String? = "",

	@field:SerializedName("LatCapturaImagen")
	var latcapturaimagen: String? = "",

	@field:SerializedName("AltitudCapturaImagen")
	var altitudcapturaimagen: String? = "",

	@field:SerializedName("Precision")
	var presicion: String? = "",

	@field:SerializedName("TipoImagen")
	var tipoimagen: Int? = -1,

	@field:SerializedName("TipoBien")
	var tipobien: Int? = -1,

	@field:SerializedName("ArchivoByte")
	var archivobyte:  String? = "",

	@field:SerializedName("ExtensionArchivo")
	var extensionarchivo: String? = "",

	@field:SerializedName("NombreArchivo")
	var nombrearchivo: String? = ""

) : Parcelable

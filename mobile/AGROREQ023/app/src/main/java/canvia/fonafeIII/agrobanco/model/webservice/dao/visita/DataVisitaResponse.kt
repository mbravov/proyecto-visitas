package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
@Parcelize
data class DataVisitaResponse  (
    @field:SerializedName("exito")
    val exito: Boolean? = null,

    @field:SerializedName("mensaje")
    val mensaje: String? = null

) : Parcelable {
}






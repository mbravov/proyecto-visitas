package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemVisitaResponse(


	@field:SerializedName("apellidoPaterno")
	val apellidoPaterno: String? = null,

	@field:SerializedName("segundoNombre")
	val segundoNombre: String? = null,

	@field:SerializedName("codigo")
	val codigo: Int? = null,

	@field:SerializedName("estado")
	val estado: Int? = null,

	@field:SerializedName("webUser")
	val webUser: String? = null,

	@field:SerializedName("primerNombre")
	val primerNombre: String? = null,

	@field:SerializedName("numeroDocumento")
	val numeroDocumento: String? = null,

	@field:SerializedName("apellidoMaterno")
	val apellidoMaterno: String? = null,
	@field:SerializedName("codAgencia")
	val codAgencia : Int? = null

) : Parcelable


package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParametrosMovilRequest (

        @field:SerializedName("Imei")
        var imei: String? = "",

        @field:SerializedName("Usuario")
        var usuario: String? = ""

) : Parcelable
package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParametrosMovilResponse(

	@field:SerializedName("hectareas")
	val hectareas: Double? = null,

	@field:SerializedName("listaAgencias")
	val listaAgencias: List<ListaAgenciasItem?>? = null,

	@field:SerializedName("listaCultivo")
	val listaCultivo: List<ListaCultivoItem?>? = null,

	@field:SerializedName("listaUnidadFinanciar")
	val listaUnidadFinanciar: List<ListaUnidadFinanciarItem?>? = null,

	@field:SerializedName("precision")
	val precision: Int? = null,

	@field:SerializedName("diasEliminar")
	val diasEliminar: Int? = null,

	@field:SerializedName("listaCrianza")
	val listaCrianza: List<ListaCrianzaItem?>? = null,

	@field:SerializedName("listaMedida")
	val listaMedida: List<ListaMedidaItem?>? = null,

	@field:SerializedName("dispositivo")
	val dispositivo: Int? = null
) : Parcelable

@Parcelize
data class ListaCrianzaItem(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("orden")
	val orden: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null
) : Parcelable

@Parcelize
data class ListaUnidadFinanciarItem(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("orden")
	val orden: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null
) : Parcelable

@Parcelize
data class ListaCultivoItem(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("orden")
	val orden: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null
) : Parcelable

@Parcelize
data class ListaMedidaItem(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("orden")
	val orden: Int? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null
) : Parcelable

@Parcelize
data class ListaAgenciasItem(

	@field:SerializedName("codigo")
	val codigo: Int? = null,

	@field:SerializedName("oficina")
	val oficina: String? = null,

	@field:SerializedName("oficinaRegional")
	val oficinaRegional: String? = null
) : Parcelable

package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import canvia.fonafeIII.agrobanco.model.webservice.ProximaVisitaResponse
import canvia.fonafeIII.agrobanco.model.webservice.TokenResponse
import canvia.fonafeIII.agrobanco.model.webservice.configuracion.Configuracion
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.TokenRequest
import canvia.fonafeIII.agrobanco.util.Utils
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ProximaVisitaRestApi {

  //  Utils().setSomeStringValue("token",token,true)

    @Headers(
        "Content-Type:application/json"
    )
    @POST(Configuracion.API_DOWNLOAD_VISITAS.toString())
    fun ObtenerListaProximasVisitas(@Header("token") token:String, @Body data: ProximaVisitaRquest?): Call<List<ItemVisitaResponse>?>?

    @POST(Configuracion.API_UPLOAD.toString())
    fun SincronizarDatosVisitaporIdVisita(@Header("token") token:String, @Body data: DataVisitaRequest?): Call<DataVisitaResponse>?

    @POST(Configuracion.API_DOWNLOAD_AGENCIAS.toString())
    fun ObtenerListaAgencias(@Header("token") token:String): Call<List<AgenciasResponse>?>?

    @POST(Configuracion.API_DOWNLOAD_CULTIVOS.toString())
    fun ObtenerListaCultivos(@Header("token") token:String): Call<List<CultivosResponse>?>?

    @POST(Configuracion.API_DOWNLOAD_UNIDADPESOS.toString())
    fun ObtenerListaUnidadPesos(@Header("token") token:String): Call<List<UnidadPesosResponse>?>?

    @POST(Configuracion.API_DOWNLOAD_UNIDADFINANCIAR.toString())
    fun ObtenerListaUnidadFinanciars(@Header("token") token:String): Call<List<UnidadFinanciarsResponse>?>?

    @POST(Configuracion.API_DOWNLOAD_CRIANZAS.toString())
    fun ObtenerListaCrianzas(@Header("token") token:String): Call<List<CrianzasResponse>?>?

    @POST(Configuracion.API_DOWNLOAD_PARAMETROS.toString())
    fun ObtenerParametrosMovil(@Header("token") token:String, @Body data: ParametrosMovilRequest?): Call<ParametrosMovilResponse?>?

}
package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProximaVisitaRquest(
	val webuser: String? = null
) : Parcelable

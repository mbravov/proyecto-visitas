package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UnidadFinanciarsResponse(
        val codigo: String? = null,
        val unidad: String? = null
) : Parcelable
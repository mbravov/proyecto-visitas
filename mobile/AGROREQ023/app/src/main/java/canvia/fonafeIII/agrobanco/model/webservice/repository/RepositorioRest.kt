package canvia.fonafeIII.agroba

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import canvia.fonafeIII.agrobanco.BuildConfig
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.model.webservice.*
import canvia.fonafeIII.agrobanco.model.webservice.configuracion.Configuracion
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.LoginRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.LoginRestApi
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.TokenRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.splash.SplashRestApi
import canvia.fonafeIII.agrobanco.model.webservice.dao.splash.VersionRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.visita.*
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Singleton
import canvia.fonafeIII.agrobanco.util.Utils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.util.*

class RepositorioRest() {
    var respuesta_usuario: MutableLiveData<Usuario?>? = null
    var respuesta_token: MutableLiveData<Token?>? = null
    var respuesta_proximavisita: MutableLiveData<ProximasVisitas?>? = null
    var respuesta_sincronizaDataVisita: MutableLiveData<VisitaSincronizada?>? = null
    var respuesta_validarVersion: MutableLiveData<Int?>? = null

    var repo: Repositorio = Repositorio(AppConfiguracion.CONTEXT!!)

    fun obtenerResultadoLogin(usuario: String, clave: String): MutableLiveData<Usuario?> {
        if (respuesta_usuario == null) {
            respuesta_usuario = MutableLiveData<Usuario?>()
        } else {
            val restApi: LoginRestApi = BaseNet().createBaseSeguridad(LoginRestApi::class.java)
            restApi.Login(LoginRequest(usuario, clave, Configuracion.LOGIN_URL.toString()))?.enqueue(object :
                    retrofit2.Callback<LoginResponse?> {
                override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    usuario_ent?.login_ok = 0
                    respuesta_usuario!!.value = usuario_ent
                }

                override fun onResponse(
                        call: Call<LoginResponse?>,
                        response: Response<LoginResponse?>
                ) = if (response.isSuccessful) {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.login_ok = response.body()?.eServicioResponse?.resultado
                    if (usuario_ent?.login_ok == 1) {
                        usuario_ent?.mensaje_resultado =
                                response.body()?.eServicioResponse?.mensaje.toString()
                        obtenerResultadoToken(response.body()?.vUsuarioWeb.toString())
                    } else {
                        usuario_ent?.mensaje_resultado = "Usuario o contraseña Incorrecta"
                    }
///////////cheka esta linea de abajo se repite
                    usuario_ent?.mensaje_resultado =
                            response.body()?.eServicioResponse?.mensaje.toString()
                    usuario_ent?.code_response = response.code()
                    usuario_ent?.cargo = response.body()?.vCargo
                    usuario_ent?.dni = response.body()?.vCodFuncionario
                    usuario_ent?.nombre = response.body()?.vNombre
                    usuario_ent?.username = usuario
                    usuario_ent?.password = clave
                    usuario_ent?.id_usuario = usuario
                    usuario_ent?.webuser = response.body()?.vUsuarioWeb

                    Utils().setSomeBooleanValue("visitas", false,true)
                    Utils().setSomeBooleanValue("evaluacioninicial", false,true)
                    Utils().setSomeBooleanValue("pendienteenvio", false,true)

                    for (i in response.body()?.listaModulo!!) {
                        if (i?.descripcionAplicacion?.toString().equals(Configuracion.MENU_ITEM_PROXIMAS_VISITAS.toString())) {
                            Utils().setSomeBooleanValue("visitas", true,true)
                        }

                        if (i?.descripcionAplicacion?.toString().equals(Configuracion.MENU_ITEM_EVALUACION_INICIAL.toString())) {
                            Utils().setSomeBooleanValue("evaluacioninicial", true,true)
                        }

                        if (i?.descripcionAplicacion?.toString() .equals( Configuracion.MENU_ITEM_PENDIENTE_ENVIO.toString())) {
                            Utils().setSomeBooleanValue("pendienteenvio", true,true)
                        }
                    }
                    respuesta_usuario!!.value = usuario_ent
                } else {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.mensaje_resultado = "Error en la peticion " + response.code()
                    usuario_ent?.login_ok = 0
                    usuario_ent?.code_response = response.code()
                    respuesta_usuario!!.value = usuario_ent
                }
            })
        }
        return respuesta_usuario!!
    }


    fun validarVersion(version: String): MutableLiveData<Int?> {
        if (respuesta_validarVersion == null) {
            respuesta_validarVersion = MutableLiveData<Int?>()
        } else {
            val restApi: SplashRestApi = BaseNet().createBaseVisita(SplashRestApi::class.java)
            restApi.ValidarVersion(VersionRequest(0, version))?.enqueue(object :
                    retrofit2.Callback<VersionRequest?> {
                override fun onFailure(call: Call<VersionRequest?>, t: Throwable) {
                    respuesta_validarVersion!!.value = 0
                }
                override fun onResponse(
                        call: Call<VersionRequest?>,
                        response: Response<VersionRequest?>
                ) = if (response.isSuccessful && response.body() != null) {

                    respuesta_validarVersion!!.value = response.body()!!.resultado
                } else {
                    respuesta_validarVersion!!.value = 0
                }
            })
        }
        return respuesta_validarVersion!!
    }



    fun obtenerResultadoToken(usuarioWeb: String): MutableLiveData<Token?> {
        if (respuesta_token == null) {
            respuesta_token = MutableLiveData<Token?>()
        } else {
            val restApi: LoginRestApi = BaseNet().createBaseVisita(LoginRestApi::class.java)
            restApi.ObtenerToken(TokenRequest(usuarioWeb, usuarioWeb, usuarioWeb, usuarioWeb))
                    ?.enqueue(object :
                            retrofit2.Callback<TokenResponse?> {
                        override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                            var token_ent: Token? = Token()
                            token_ent?.mensaje_resultado = "Ha ocurrido un error: " + t.message
                            token_ent?.token_ok = 0
                            respuesta_token!!.value = token_ent
                        }

                        override fun onResponse(
                                call: Call<TokenResponse?>,
                                response: Response<TokenResponse?>
                        ) = if (response.isSuccessful && response.body() != null) {
                            var token_ent: Token? = Token()
                            token_ent?.code_response = response.code()
                            if (response.headers().get("Authorization") != null) {
                                if (response.headers().get("Authorization").toString() != "") {
                                    val token: String =
                                            response.headers().get("Authorization").toString()
                                    //obtenerParametrosMovil(token)


                                    var data: ParametrosMovilRequest = ParametrosMovilRequest()

                                    data.imei = Utils().getImei(AppConfiguracion.CONTEXT!!)
                                    data.usuario = Singleton.getUser()

                                    Log.e("obtenerParametrosMovil","data.imei: "+data.imei)
                                    Log.e("obtenerParametrosMovil","data.usuario: "+data.usuario)

                                    val restApi: ProximaVisitaRestApi =
                                            BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

                                    restApi.ObtenerParametrosMovil(
                                            token,data
                                    )?.enqueue(object :
                                            retrofit2.Callback<ParametrosMovilResponse?> {
                                        override fun onResponse(call: Call<ParametrosMovilResponse?>, response: Response<ParametrosMovilResponse?>) {

                                            if (response.isSuccessful && response.body() != null) {
                                                var listaragencia: MutableList<Agencia> = mutableListOf()
                                                var listarCrianza: MutableList<Crianza> = mutableListOf()
                                                var listarCultivo: MutableList<Cultivo> = mutableListOf()
                                                var listarUnidadFinanciar: MutableList<UnidadFinanciar> = mutableListOf()
                                                var listarUnidadPeso: MutableList<UnidadPeso> = mutableListOf()

                                                for (agenciaResponse: ListaAgenciasItem? in response.body()!!.listaAgencias!!) {
                                                    var age: Agencia? = Agencia(agenciaResponse?.codigo!!, agenciaResponse.oficina, agenciaResponse.oficinaRegional);
                                                    listaragencia.add(age!!)
                                                }
                                                repo.actualizarListaAgencias(listaragencia)

                                                for (crianzaResponse: ListaCrianzaItem? in response.body()!!.listaCrianza!!) {
                                                    var cri: Crianza? = Crianza(crianzaResponse?.codigo!!,crianzaResponse.nombre);
                                                    listarCrianza.add(cri!!)
                                                }
                                                repo.actualizarListaCrianzas(listarCrianza)

                                                for (cultivosResponse: ListaCultivoItem? in response.body()!!.listaCultivo!!) {
                                                    var cul: Cultivo? = Cultivo(cultivosResponse?.codigo!!,cultivosResponse.nombre);
                                                    listarCultivo.add(cul!!)
                                                }
                                                repo.actualizarListaCultivos(listarCultivo)

                                                for (unidadFinanciarResponse: ListaUnidadFinanciarItem? in response.body()!!.listaUnidadFinanciar!!) {
                                                    var fin: UnidadFinanciar? = UnidadFinanciar(unidadFinanciarResponse?.codigo!!,unidadFinanciarResponse.nombre);
                                                    listarUnidadFinanciar.add(fin!!)
                                                }
                                                repo.actualizarListaUnidadFinanciars(listarUnidadFinanciar)

                                                for (unidadPesoResponse: ListaMedidaItem? in response.body()!!.listaMedida!!) {
                                                    var med: UnidadPeso? = UnidadPeso(unidadPesoResponse?.codigo!!,unidadPesoResponse.nombre);
                                                    listarUnidadPeso.add(med!!)
                                                }
                                                repo.actualizarListaUnidadPesos(listarUnidadPeso)

                                                Utils().setSomeIntValue("diaseliminar", response.body()!!.diasEliminar, true)
                                                Utils().setSomeStringValue("hectarias", response.body()!!.hectareas.toString(), true)

                                                if(response.body()!!.precision!!>21)
                                                    Utils().setSomeIntValue("precision", response.body()!!.precision, true)
                                                else
                                                    Utils().setSomeIntValue("precision", 21, true)
                                                Utils().setSomeStringValue("token", token, false)
                                                token_ent?.token_ok = 1
                                                token_ent?.mensaje_resultado = "Token almacenado"

                                                if(response.body()!!.dispositivo==1)
                                                    token_ent?.imei_ok = response.body()!!.dispositivo
                                                else
                                                    token_ent?.imei_ok = 2

                                                //token_ent?.imei_ok = 1
                                                respuesta_token!!.value = token_ent
                                            }
                                        }


                                        override fun onFailure(call: Call<ParametrosMovilResponse?>, t: Throwable) {
                                            token_ent?.token_ok = 1
                                            token_ent?.mensaje_resultado = "Error en el servicio"
                                            token_ent?.imei_ok = 2
                                            respuesta_token!!.value = token_ent
                                        }
                                    }
                                    )


                                    //Utils().setSomeStringValue("token", token, false)
                                    //token_ent?.token_ok = 1
                                    //token_ent?.mensaje_resultado = "Token almacenado"
                                } else {
                                    token_ent?.token_ok = 0
                                    token_ent?.mensaje_resultado = "Token vacio"
                                    respuesta_token!!.value = token_ent
                                }
                            } else {
                                token_ent?.token_ok = 0
                                token_ent?.mensaje_resultado = "Sin token"
                                respuesta_token!!.value = token_ent
                            }
                            respuesta_token!!.value = token_ent
                        } else {
                            var token_ent: Token? = Token()
                            token_ent?.mensaje_resultado = "Ha ocurrido un error"
                            token_ent?.token_ok = 0
                            token_ent?.code_response = response.code()
                            respuesta_token!!.value = token_ent
                        }
                    })
        }
        return respuesta_token!!
    }


    fun obtenerListaProximasVisitas(usuario: String): MutableLiveData<ProximasVisitas?> {
        if (respuesta_proximavisita == null) {
            respuesta_proximavisita = MutableLiveData<ProximasVisitas?>()
        } else {
            val restApi: ProximaVisitaRestApi =
                    BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

            restApi.ObtenerListaProximasVisitas(
                    Utils().getSomeStringvalue("token", "", false).toString(),
                    ProximaVisitaRquest(Utils().getSomeStringvalue("webuser", "", false))
            )?.enqueue(object :
                    retrofit2.Callback<List<ItemVisitaResponse>?> {
                override fun onFailure(call: Call<List<ItemVisitaResponse>?>, t: Throwable) {
                    var resultado: ProximasVisitas = ProximasVisitas()
                    resultado.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    resultado.respuesta_ok = 0
                    respuesta_proximavisita!!.value = resultado
                }

                override fun onResponse(
                        call: Call<List<ItemVisitaResponse>?>,
                        response: Response<List<ItemVisitaResponse>?>
                ) = if (response.isSuccessful && response.body() != null) {

                    var resultado: ProximasVisitas = ProximasVisitas()

                    try {
                        if (response.body()?.size!! > 0) {

                            var codigo_visita_temporal: Int = 0
                            repo.eliminarVisitaporUsuario(
                                    Utils().getSomeStringvalue(
                                            "username",
                                            "",
                                            false
                                    ).toString()
                            )

                            //Log.e("ProximasVisitas","response.body(): "+response.body()!!)

                            for (visi: ItemVisitaResponse in response.body()!!) {
                                var item_visita: Visita? = Visita("")
                                // item_visita?.id_visita = (codigo_visita_temporal + 1).toString()
                                //  codigo_visita_temporal = codigo_visita_temporal + 1

                                item_visita?.dni_cliente = visi.numeroDocumento ?: ""
                                item_visita?.primer_nombre_cliente = visi.primerNombre ?: ""
                                item_visita?.segundo_nombre_cliente = visi.segundoNombre ?: ""
                                item_visita?.apellido_materno_cliente = visi.apellidoMaterno ?: ""
                                item_visita?.apellido_paterno_cliente = visi.apellidoPaterno ?: ""
                                item_visita?.cod_asignacion = visi.codigo
                                item_visita?.nombre_predio = ""
                                item_visita?.codigo_agencia=visi.codAgencia
                                item_visita?.id_usuario = Singleton.getUser()

                                if (visi.estado == null) {
                                    item_visita?.estado = 1
                                } else {
                                    item_visita?.estado = visi.estado
                                }
                                if (repo.existeElementoVisitaPorCodAsignacion(item_visita?.cod_asignacion.toString())) {
                                    item_visita?.usuario_modifica =
                                            Singleton.getUser()
                                    item_visita?.fecha_modifica = Utils().obtenerFechaHoraActual(
                                            Constants().ZONA_HORARIA
                                    )
                                    item_visita?.acceso = "1"

                                    repo.ActualizaElementoVisitaDesdeServicio(
                                            Singleton.getUser(),
                                            item_visita?.cod_asignacion.toString(),
                                            item_visita?.dni_cliente.toString(),
                                            item_visita?.apellido_paterno_cliente.toString(),
                                            item_visita?.apellido_materno_cliente.toString(),
                                            item_visita?.primer_nombre_cliente.toString(),
                                            item_visita?.segundo_nombre_cliente.toString(),
                                            item_visita?.estado!!
                                    )
                                    repo.ActualizaElementoEvalRegistroDesdeServicio(item_visita?.cod_asignacion.toString(),
                                            item_visita?.dni_cliente.toString(),
                                            item_visita?.apellido_paterno_cliente.toString(),
                                            item_visita?.apellido_materno_cliente.toString(),
                                            item_visita?.primer_nombre_cliente.toString(),
                                            item_visita?.segundo_nombre_cliente.toString())
                                } else {
                                    item_visita?.acceso = "1"
                                    item_visita?.iniciado = 0
                                    item_visita?.id_visita = UUID.randomUUID().toString()
                                    item_visita?.usuario_crea = Singleton.getUser()
                                    item_visita?.fecha_crea = Utils().obtenerFechaHoraActual(
                                            Constants().ZONA_HORARIA
                                    )
                                    repo.AgregarElementoVisita(item_visita!!)

                                    var reg_inf: EvalRegistro? = EvalRegistro("")
                                    reg_inf?.id_visita = item_visita?.id_visita
                                    reg_inf?.dni_cliente = item_visita?.dni_cliente.toString()
                                    reg_inf?.apellido_materno_cliente = item_visita?.apellido_materno_cliente.toString()
                                    reg_inf?.apellido_paterno_cliente = item_visita?.apellido_paterno_cliente.toString()
                                    reg_inf?.primer_nombre_cliente = item_visita?.primer_nombre_cliente.toString()
                                    reg_inf?.segundo_nombre_cliente = item_visita?.segundo_nombre_cliente.toString()
                                    reg_inf?.cod_asignacion = item_visita?.cod_asignacion?.toInt()
                                    repo.addElementoRegistroInformacion(reg_inf!!)
                                }
                            }
                        } else {
                            repo.eliminarVisitaporUsuario(
                                    Utils().getSomeStringvalue(
                                            "username",
                                            "",
                                            false
                                    ).toString()
                            )
                        }
                        resultado.mensaje_resultado = "todo ok"
                        resultado.respuesta_ok = 1
                    } catch (ex: Exception) {
                        resultado.mensaje_resultado = "Ha Ocurrido un error"
                        resultado.respuesta_ok = 0
                    }

                    respuesta_proximavisita!!.value = resultado
                } else {
                    var resultado: ProximasVisitas = ProximasVisitas()

                    resultado.mensaje_resultado =
                            "Ha ocurrido un error: " + response.code().toString()
                    resultado.respuesta_ok = 0
                    respuesta_proximavisita!!.value = resultado
                }
            })
        }
        return respuesta_proximavisita!!
    }

    fun sincronizarDataVisita(
            usuario: String,
            idvisita: String
    ): MutableLiveData<VisitaSincronizada?> {
        if (respuesta_sincronizaDataVisita == null) {
            respuesta_sincronizaDataVisita = MutableLiveData<VisitaSincronizada?>()
        } else {
            val restApi: ProximaVisitaRestApi =
                    BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

            var visita: Visita? = null
            visita = repo.selectVisitaItem(idvisita)

            var evalRegistro: EvalRegistro? = null
            evalRegistro = repo.selectEvalRegistroItem(idvisita)

            var evalArea: EvalArea? = null
            evalArea = repo.selectEvalAreaItemSinImagen(idvisita)

            var coordenada: MutableList<Coordenada> = mutableListOf()
            coordenada = repo.selectCoordenadaLista(idvisita)

            var fotosPrincipales: MutableList<Imagen> = mutableListOf()
            fotosPrincipales = repo.selectImagenLista(idvisita)

            var fotosComplemtarias: MutableList<ImagenComplemtaria> = mutableListOf()
            fotosComplemtarias = repo.selectImagenComplementariaLista(idvisita)

            var productor: Productor? = null
            productor = repo.selectEvalProductor(idvisita)

            var creditoAgricolaI: CreditoAgricolaI? = null
            creditoAgricolaI = repo.selectEvalCreditoAgricolaI(idvisita)

            var creditoAgricolaII: CreditoAgricolaII? = null
            creditoAgricolaII = repo.selectEvalCreditoAgricolaII(idvisita)

            var creditoAgricolaIII: CreditoAgricolaIII? = null
            creditoAgricolaIII = repo.selectEvalCreditoAgricolaIII(idvisita)

            var creditoPecuarioI: CreditoPecuarioI? = null
            creditoPecuarioI = repo.selectEvalCreditoPecuarioI(idvisita)

            var creditoPecuarioII: CreditoPecuarioII? = null
            creditoPecuarioII = repo.selectEvalCreditoPecuarioII(idvisita)

            var predioColindante: PredioColindante? = null
            predioColindante = repo.selectEvalPredioColindante(idvisita)

            var comentario: Comentario? = null
            comentario = repo.selectEvalComentario(idvisita)

            var firmas: Firmas = repo.selectEvalFirmas2(idvisita)

            var data: DataVisitaRequest = DataVisitaRequest()

            if(evalRegistro!=null){
                if(evalRegistro.dni_cliente!=null) data.numdocumentocliente= evalRegistro.dni_cliente
                if(evalRegistro.cod_asignacion!=null) data.codigoasignacion=evalRegistro.cod_asignacion
                if(usuario!=null) data.usuariocreacionvisita=usuario
                if(evalRegistro.primer_nombre_cliente!=null) data.primernombrecliente = evalRegistro.primer_nombre_cliente
                if(evalRegistro.segundo_nombre_cliente!=null) data.segundonombrecliente= evalRegistro.segundo_nombre_cliente
                if(evalRegistro.apellido_paterno_cliente!=null) data.apellidopaternocliente = evalRegistro.apellido_paterno_cliente
                if(evalRegistro.apellido_materno_cliente!=null) data.apellidomaternocliente= evalRegistro.apellido_materno_cliente
                if(evalRegistro.nombre_predio!=null) data.nombrepredio = evalRegistro.nombre_predio
                if(evalRegistro.direccion_predio!=null) data.direccionpredio= evalRegistro.direccion_predio
                if(evalRegistro.tipo_actividad!=null) data.tipoactividad = evalRegistro.tipo_actividad
                if(evalRegistro.latitud_inicio!=null) data.latiniciovisita = evalRegistro.latitud_inicio
                if(evalRegistro.latitud_final!=null) data.latfinvisita = evalRegistro.latitud_final
                if(evalRegistro.precision_inicio!=null) data.precicionInicioVisita= evalRegistro.precision_inicio
                if(evalRegistro.precision_final!=null) data.precicionfinvisita= evalRegistro.precision_final
                if(evalRegistro.longitud_inicio!=null) data.longiniciovisita= evalRegistro.longitud_inicio
                if(evalRegistro.longitud_final!=null) data.longfinvisita = evalRegistro.longitud_final
                if(evalRegistro.fecha_crea!=null) data.fechainiciovisita = evalRegistro.fecha_crea.toString().replace(" ","T")
                if(evalRegistro.fecha_modifica!=null) data.fechafinvisita = evalRegistro.fecha_modifica.toString().replace(" ","T") //se toma esta fecha, dado que al momento de dar la conformidad del Resumen, se actualiza la tabla EvalRegistro
            }

            if(visita!=null){
                if(visita.id_visita!=null) data.codvisitamobile = visita.id_visita
                if(visita.fecha_visita!=null) data.fechavisita = visita.fecha_visita.toString().replace(" ","T")
            }

            if(evalArea!= null){
                if(evalArea.tipo_mapa!=null) data.tipomapa=evalArea.tipo_mapa
                if(evalArea.modo_captura!=null) data.modocaptura= evalArea.modo_captura
                if(evalArea.numero_puntos!=null) data.numpuntosmapa = evalArea.numero_puntos
                if(evalArea.area!=null) data.areatotalpuntos=evalArea.area
                data.capturamapa = repo.getEvalAreaCaptura_mapa(idvisita)
            }

            if(productor!=null){
                if(productor.objetivo_visita!=null) data.descobjetivovisita= productor.objetivo_visita
                if(productor.id_tipo_presente!=null) data.presenteenvisita = productor.id_tipo_presente
                if(productor.nombre!=null) data.nombrefamiliar = productor.nombre
                if(productor.vinculo!=null) data.vinculofamiliar= productor.vinculo
                if(productor.numero_celular==null || productor.numero_celular.equals("")){
                    data.numcelfamiliar= 0
                }else{
                    data.numcelfamiliar= productor.numero_celular?.toInt()
                }
                if(productor.agencia!=null) data.codigoagenciaasignada= productor.agencia
                if(productor.agencia!=null) data.codigoagencia= productor.agencia
                if(productor.id_tipo_mercado!=null) data.mercadoacceso= productor.id_tipo_mercado?.toInt()
                if(productor.id_protegido!=null) data.areaprotegida = productor.id_protegido?.toInt()
            }

            if(creditoAgricolaI!=null){
                if(creditoAgricolaI.has_financiar!=null) data.numhectareasfinanciar= creditoAgricolaI.has_financiar?.toDouble()
                if(creditoAgricolaI.has_sembradas!=null) data.numhectareassembradas= creditoAgricolaI.has_sembradas
                if(creditoAgricolaI.has_totales!=null) data.numhectareastotales=creditoAgricolaI.has_totales
                if(creditoAgricolaI.nro_parcelas!=null) data.numparcelas= creditoAgricolaI.nro_parcelas
                if(creditoAgricolaI.regimen_tenencia!=null) data.regimentenencia=  creditoAgricolaI.regimen_tenencia
                if(creditoAgricolaI.ubicacion!=null) data.tipoubicacion=creditoAgricolaI.ubicacion
                if(creditoAgricolaI.unidad_catastral!=null) data.unidadcatastral = creditoAgricolaI.unidad_catastral
            }

            if(creditoAgricolaII !=null){
                if(creditoAgricolaII.altitud_aproximada!=null) data.altitudpredio =creditoAgricolaII.altitud_aproximada?.toInt()
                if(creditoAgricolaII.disponibilidad_agua!=null) data.disponibilidadagua =creditoAgricolaII.disponibilidad_agua
                if(creditoAgricolaII.estado_campo!=null) data.estadocampo= creditoAgricolaII.estado_campo
                if(creditoAgricolaII.pendiente!=null) data.pendiente= creditoAgricolaII.pendiente
                if(creditoAgricolaII.tipo_riego!=null) data.tiporiego= creditoAgricolaII.tipo_riego
                if(creditoAgricolaII.tipo_suelo!=null) data.tiposuelo= creditoAgricolaII.tipo_suelo
                if(creditoAgricolaII.accesibilidad!=null) data.accesibilidadpredio= creditoAgricolaII.accesibilidad
            }

            if(creditoAgricolaIII !=null){
                if(creditoAgricolaIII.experiencia_anios!=null) data.aniosexp= creditoAgricolaIII.experiencia_anios
                if(creditoAgricolaIII.fecha_siembra!=null) data.fechasiembra= Utils().cambiarFormatoFechaSiembra(creditoAgricolaIII.fecha_siembra)
                if(creditoAgricolaIII.producto_cultivo!=null) data.cultivo = creditoAgricolaIII.producto_cultivo
                if(creditoAgricolaIII.rendimiento_anterior!=null) data.rendimientoanterior=  creditoAgricolaIII.rendimiento_anterior?.toDouble()
                if(creditoAgricolaIII.rendimiento_esperado!=null) data.rendimientoesperado = creditoAgricolaIII.rendimiento_esperado?.toDouble()
                if(creditoAgricolaIII.tipo_semilla!=null) data.tiposiembra=  creditoAgricolaIII.tipo_semilla
                if(creditoAgricolaIII.variedad!=null) data.variedad=   creditoAgricolaIII.variedad
                if(creditoAgricolaIII.rendimiento_unidad_medida!=null) data.unidadpesocod=   creditoAgricolaIII.rendimiento_unidad_medida
            }

            if(creditoPecuarioI !=null){
                if(creditoPecuarioI.disponibilidad_agua!=null) data.disponibilidadaguapecuario= creditoPecuarioI.disponibilidad_agua
                if(creditoPecuarioI.fuente_agua!=null) data.tipofuenteagua= creditoPecuarioI.fuente_agua
                if(creditoPecuarioI.manejo_actividad!=null) data.tipomanejo =creditoPecuarioI.manejo_actividad
                if(creditoPecuarioI.numero_unidades_financiar!=null) data.numunidadesfinanciar= creditoPecuarioI.numero_unidades_financiar
                if(creditoPecuarioI.numero_unidades_totales!=null) data.numtotalunidades= creditoPecuarioI.numero_unidades_totales
                if(creditoPecuarioI.tipo_alimentacion!=null) data.tipoalimentacion= creditoPecuarioI.tipo_alimentacion
                if(creditoPecuarioI.unidad_financiar!=null) data.unidadfinanciar= creditoPecuarioI.unidad_financiar
                if(creditoPecuarioI.unidad_financiar!=null) data.unidadfinanciarcod= creditoPecuarioI.unidad_financiar

                if(creditoPecuarioI.unidades_productivas!=null) data.unidadesproductivas= creditoPecuarioI.unidades_productivas.toString()
            }

            if (creditoPecuarioII != null) {
                if(creditoPecuarioII.crianza!=null) data.tipocrianza = creditoPecuarioII.crianza
                if(creditoPecuarioII.experiencia_anios!=null) data.aniosexppecuario = creditoPecuarioII.experiencia_anios
                if(creditoPecuarioII.manejo_crianza!=null) data.tipomanejopecuario = creditoPecuarioII.manejo_crianza
                if(creditoPecuarioII.raza!=null) data.raza = creditoPecuarioII.raza
                if(creditoPecuarioII.tipo_tecnologia!=null) data.tipotecnologia = creditoPecuarioII.tipo_tecnologia
            }

            if (predioColindante != null) {
                if(predioColindante.comentario_predios_colindantes!=null) data.comentpredioscolindantes = predioColindante.comentario_predios_colindantes
            }

            if (comentario != null) {
                if(comentario.comentario_recomendaciones!=null) data.comentrecomendaciones = comentario.comentario_recomendaciones
            }

            if(coordenada !=null){
                if(coordenada.size>0){
                    var listcoordenadas:MutableList<CoordenadasItem> = mutableListOf()
                    for ( x: Coordenada in coordenada ){
                        var item: CoordenadasItem = CoordenadasItem()
                        if(x.numero_orden!=null) item.codigo = x.numero_orden.toInt()
                        if(x.id_visita!=null) item.codigocoordenadamobile = x.id_visita
                        if(x.numero_orden!=null) item.numorden = x.numero_orden.toInt()
                        if(x.precision!=null) item.presicion = x.precision
                        if(x.longitud!=null) item.longitud = x.longitud
                        if(x.latitud!=null) item.latitud = x.latitud
                        if(x.altitud!=null) item.altitud= x.altitud

                        listcoordenadas.add(item)
                    }
                    data.coordenadas = listcoordenadas
                }
            }

            if(firmas!=null){
                if(firmas.comprimido_firma_titular!=null){
                    data.firmatitular = firmas.comprimido_firma_titular.toString()
                }
                if(firmas.comprimido_firma_conyuge!=null){
                    data.firmaconyugue = firmas.comprimido_firma_conyuge.toString()
                }
            }

            var listFotos: MutableList<ImagenItem> = mutableListOf()

            var numFotos = 0
            if (fotosPrincipales != null) {
                if (fotosPrincipales.size > 0) {
                    for (x: Imagen in fotosPrincipales) {
                        var item: ImagenItem = ImagenItem()

                        if(x.posicion!=null) item.codigo = x.posicion
                        if(x.posicion!=null) item.codigofotomobile =x.posicion.toString()
                        if(x.posicion!=null) item.numordencaptura= x.posicion
                        if(x.longitud!=null) item.longcapturaimagen= x.longitud
                        if(x.latitud!=null) item.latcapturaimagen= x.latitud
                        if(x.altitud!=null) item.altitudcapturaimagen = x.altitud
                        if(x.precision!=null) item.presicion= x.precision.toString()
                        item.tipoimagen = 1 //  1 es las principales y 2 las complementarias
                        item.archivobyte = convertiraString64(x.ruta.toString())
                        item.extensionarchivo = "jpg"

                        numFotos++
                        item.nombrearchivo = "imagen $numFotos"
                        listFotos.add(item)
                    }
                    data.numfotosprincipales = numFotos
                    data.imagen = listFotos
                }
            }

            numFotos = 0
            if (fotosComplemtarias != null) {
                if (fotosComplemtarias.size > 0) {
                    for (x: ImagenComplemtaria in fotosComplemtarias) {
                        var item: ImagenItem = ImagenItem()

                        if(x.posicion!=null) item.codigo = x.posicion
                        if(x.posicion!=null) item.codigofotomobile =x.posicion.toString()
                        if(x.posicion!=null) item.numordencaptura= x.posicion
                        if(x.longitud!=null) item.longcapturaimagen= x.longitud
                        if(x.latitud!=null) item.latcapturaimagen= x.latitud
                        if(x.altitud!=null) item.altitudcapturaimagen = x.altitud
                        if(x.precision!=null) item.presicion = x.precision.toString()
                        item.tipoimagen = 2 // 1 es las principales y 2 las complementarias
                        item.tipobien = x.id_tipo
                        item.archivobyte = convertiraString64(x.ruta.toString())
                        item.extensionarchivo = "jpg"

                        numFotos++
                        item.nombrearchivo = "comple-imagen $numFotos"
                        listFotos.add(item)
                    }
                    data.numfotossecundarias = numFotos
                    data.imagen = listFotos
                }
            }

            if (data.latiniciovisita == null || data.latiniciovisita!!.isEmpty()) {
                data.latiniciovisita = data.latfinvisita
            }

            if (data.longiniciovisita == null || data.longiniciovisita!!.isEmpty()) {
                data.longiniciovisita = data.longfinvisita
            }

            //Log.e("data: ",""+data)
            val gson = Gson();
            val jsonString = gson.toJson(data);
            Log.e("SINCRONIZAR VISITA", jsonString)

            restApi.SincronizarDatosVisitaporIdVisita(
                    Utils().getSomeStringvalue("token", "", false).toString(),
                    data
            )?.enqueue(object :
                    retrofit2.Callback<DataVisitaResponse?> {
                override fun onFailure(call: Call<DataVisitaResponse?>, t: Throwable) {
                    var resultado: VisitaSincronizada = VisitaSincronizada()
                    resultado.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    resultado.respuesta_ok = 0
                    respuesta_sincronizaDataVisita!!.value = resultado
                }

                override fun onResponse(
                        call: Call<DataVisitaResponse?>,
                        response: Response<DataVisitaResponse?>
                ) = if (response.isSuccessful && response.body() != null) {

//response.body().exito==true

                    var resultado: VisitaSincronizada = VisitaSincronizada()

                    try {
                        repo.actualizaDataVisitaSincronizada(idvisita)
                        resultado.mensaje_resultado = "todo ok"
                        resultado.respuesta_ok = 1
                    } catch (ex: Exception) {
                        resultado.mensaje_resultado = "Ha Ocurrido un error"
                        resultado.respuesta_ok = 0
                    }

                    respuesta_sincronizaDataVisita!!.value = resultado
                } else {
                    var resultado: VisitaSincronizada = VisitaSincronizada()

                    resultado.mensaje_resultado =
                            "Ha ocurrido un error: " + response.code().toString()
                    resultado.respuesta_ok = 0
                    respuesta_sincronizaDataVisita!!.value = resultado
                }
            })
        }
        return respuesta_sincronizaDataVisita!!
    }

    private fun convertiraString64(ruta: String): String {
        val bitmap = BitmapFactory.decodeFile(ruta)
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val fotoEnBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT)
        return fotoEnBase64
    }


    fun obtenerListaAgencias(tok:String) {

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerListaAgencias(
                tok
        )?.enqueue(object :
                retrofit2.Callback<List<AgenciasResponse>?> {
            override fun onResponse(call: Call<List<AgenciasResponse>?>, response: Response<List<AgenciasResponse>?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaresultado: MutableList<Agencia> = mutableListOf()
                    if (response.body()!!.isNotEmpty()) {
                        for (agenciaResponse: AgenciasResponse in response.body()!!) {
                            var age: Agencia? = Agencia(agenciaResponse.codigo!!, agenciaResponse.oficina, agenciaResponse.oficinaRegional);
                            listaresultado.add(age!!)
                        }
                        repo.actualizarListaAgencias(listaresultado)
                    }
                }
            }

            override fun onFailure(call: Call<List<AgenciasResponse>?>, t: Throwable) {

            }
        }
        )
    }



    fun obtenerParametrosMovil(tok:String) {

        var data: ParametrosMovilRequest = ParametrosMovilRequest()

        data.imei = Utils().getImei(AppConfiguracion.CONTEXT!!)
        data.usuario = Singleton.getUser()

        Log.e("obtenerParametrosMovil","data.imei: "+data.imei)
        Log.e("obtenerParametrosMovil","data.usuario: "+data.usuario)

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerParametrosMovil(
                tok,data
        )?.enqueue(object :
                retrofit2.Callback<ParametrosMovilResponse?> {
            override fun onResponse(call: Call<ParametrosMovilResponse?>, response: Response<ParametrosMovilResponse?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaragencia: MutableList<Agencia> = mutableListOf()
                    var listarCrianza: MutableList<Crianza> = mutableListOf()
                    var listarCultivo: MutableList<Cultivo> = mutableListOf()
                    var listarUnidadFinanciar: MutableList<UnidadFinanciar> = mutableListOf()
                    var listarUnidadPeso: MutableList<UnidadPeso> = mutableListOf()

                    for (agenciaResponse: ListaAgenciasItem? in response.body()!!.listaAgencias!!) {
                        var age: Agencia? = Agencia(agenciaResponse?.codigo!!, agenciaResponse.oficina, agenciaResponse.oficinaRegional);
                        listaragencia.add(age!!)
                    }
                    repo.actualizarListaAgencias(listaragencia)

                    for (crianzaResponse: ListaCrianzaItem? in response.body()!!.listaCrianza!!) {
                        var cri: Crianza? = Crianza(crianzaResponse?.codigo!!,crianzaResponse.nombre);
                        listarCrianza.add(cri!!)
                    }
                    repo.actualizarListaCrianzas(listarCrianza)

                    for (cultivosResponse: ListaCultivoItem? in response.body()!!.listaCultivo!!) {
                        var cul: Cultivo? = Cultivo(cultivosResponse?.codigo!!,cultivosResponse.nombre);
                        listarCultivo.add(cul!!)
                    }
                    repo.actualizarListaCultivos(listarCultivo)

                    for (unidadFinanciarResponse: ListaUnidadFinanciarItem? in response.body()!!.listaUnidadFinanciar!!) {
                        var fin: UnidadFinanciar? = UnidadFinanciar(unidadFinanciarResponse?.codigo!!,unidadFinanciarResponse.nombre);
                        listarUnidadFinanciar.add(fin!!)
                    }
                    repo.actualizarListaUnidadFinanciars(listarUnidadFinanciar)

                    for (unidadPesoResponse: ListaMedidaItem? in response.body()!!.listaMedida!!) {
                        var med: UnidadPeso? = UnidadPeso(unidadPesoResponse?.codigo!!,unidadPesoResponse.nombre);
                        listarUnidadPeso.add(med!!)
                    }
                    repo.actualizarListaUnidadPesos(listarUnidadPeso)

                    Utils().setSomeIntValue("diaseliminar", response.body()!!.diasEliminar, true)
                    Utils().setSomeStringValue("hectarias", response.body()!!.hectareas.toString(), true)

                    Utils().setSomeIntValue("precision", 20, true)

                    if(response.body()!!.precision!!>20)
                        Utils().setSomeIntValue("precision", response.body()!!.precision, true)
                    else
                        Utils().setSomeIntValue("precision", 20, true)

                    Log.e("RepositirioRest","precision: "+Utils().getSomeIntvalue("precision",30,true)!!)
                }
            }


            override fun onFailure(call: Call<ParametrosMovilResponse?>, t: Throwable) {
            }
        }
        )
    }





    fun obtenerListaCultivos(tok:String) {

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerListaCultivos(
                tok
        )?.enqueue(object :
                retrofit2.Callback<List<CultivosResponse>?> {
            override fun onResponse(call: Call<List<CultivosResponse>?>, response: Response<List<CultivosResponse>?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaresultado: MutableList<Cultivo> = mutableListOf()
                    if (response.body()!!.isNotEmpty()) {
                        for (CultivoResponse: CultivosResponse in response.body()!!) {
                            var Cultivo: Cultivo? = Cultivo(CultivoResponse.codigo!!, CultivoResponse.cultivo);
                            listaresultado.add(Cultivo!!)
                        }
                        repo.actualizarListaCultivos(listaresultado)
                    }
                }
            }

            override fun onFailure(call: Call<List<CultivosResponse>?>, t: Throwable) {

            }
        }
        )
    }

    fun obtenerListaUnidadPesos(tok:String) {

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerListaUnidadPesos(
                tok
        )?.enqueue(object :
                retrofit2.Callback<List<UnidadPesosResponse>?> {
            override fun onResponse(call: Call<List<UnidadPesosResponse>?>, response: Response<List<UnidadPesosResponse>?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaresultado: MutableList<UnidadPeso> = mutableListOf()
                    if (response.body()!!.isNotEmpty()) {
                        for (unidadPesoResponse: UnidadPesosResponse in response.body()!!) {
                            var unidadPeso: UnidadPeso? = UnidadPeso(unidadPesoResponse.codigo!!, unidadPesoResponse.unidad);
                            listaresultado.add(unidadPeso!!)
                        }
                        repo.actualizarListaUnidadPesos(listaresultado)
                    }
                }
            }

            override fun onFailure(call: Call<List<UnidadPesosResponse>?>, t: Throwable) {

            }
        }
        )
    }

    fun obtenerListaUnidadFinanciars(tok:String) {

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerListaUnidadFinanciars(
                tok
        )?.enqueue(object :
                retrofit2.Callback<List<UnidadFinanciarsResponse>?> {
            override fun onResponse(call: Call<List<UnidadFinanciarsResponse>?>, response: Response<List<UnidadFinanciarsResponse>?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaresultado: MutableList<UnidadFinanciar> = mutableListOf()
                    if (response.body()!!.isNotEmpty()) {
                        for (unidadFinanciarResponse: UnidadFinanciarsResponse in response.body()!!) {
                            var unidadFinanciar: UnidadFinanciar? = UnidadFinanciar(unidadFinanciarResponse.codigo!!, unidadFinanciarResponse.unidad);
                            listaresultado.add(unidadFinanciar!!)
                        }
                        repo.actualizarListaUnidadFinanciars(listaresultado)
                    }
                }
            }

            override fun onFailure(call: Call<List<UnidadFinanciarsResponse>?>, t: Throwable) {

            }
        }
        )
    }

    fun obtenerListaCrianzas(tok:String) {

        val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

        restApi.ObtenerListaCrianzas(
                tok
        )?.enqueue(object :
                retrofit2.Callback<List<CrianzasResponse>?> {
            override fun onResponse(call: Call<List<CrianzasResponse>?>, response: Response<List<CrianzasResponse>?>) {

                if (response.isSuccessful && response.body() != null) {
                    var listaresultado: MutableList<Crianza> = mutableListOf()
                    if (response.body()!!.isNotEmpty()) {
                        for (crianzaResponse: CrianzasResponse in response.body()!!) {
                            var crianza: Crianza? = Crianza(crianzaResponse.codigo!!, crianzaResponse.crianza);
                            listaresultado.add(crianza!!)
                        }
                        repo.actualizarListaCrianzas(listaresultado)
                    }
                }
            }

            override fun onFailure(call: Call<List<CrianzasResponse>?>, t: Throwable) {

            }
        }
        )
    }

    init {

    }
}
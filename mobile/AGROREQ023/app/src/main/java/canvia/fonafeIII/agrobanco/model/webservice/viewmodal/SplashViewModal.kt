package canvia.fonafeIII.agrobanco.model.webservice.viewmodal

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import canvia.fonafeIII.agroba.RepositorioRest
import canvia.fonafeIII.agrobanco.model.pojos.Token
import canvia.fonafeIII.agrobanco.model.pojos.Usuario

class SplashViewModal : ViewModel() {


    private var splashRepository: RepositorioRest

    var respuestasplash: MutableLiveData<Int?>? = null

    fun splash(version: String?) {
        splashRepository?.validarVersion(version!!)
    }

    fun getVersionResponseLiveData(version: String?): MutableLiveData<Int?>? {
        if (respuestasplash == null) {
            respuestasplash = MutableLiveData<Int?>()
        }
        respuestasplash = splashRepository.validarVersion(version!!)
        return respuestasplash
    }

    init {
        splashRepository = RepositorioRest()
       respuestasplash = MutableLiveData<Int?>()



    }



}
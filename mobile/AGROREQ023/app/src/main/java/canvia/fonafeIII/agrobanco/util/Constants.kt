package canvia.fonafeIII.agrobanco.util

class Constants {
    val ESTADO_CAMPO = "ESTADO_CAMPO"
    val EVALUACION_PENDIENTE = 0
    val EN_PROCESO = 1


    val ACCESO_SOLO_INICIAL = 1
    val ACCESO_TODOS_MENOS_INICIAL = 2
    val ACCESO_SIN_PENDIENTES = 6
    val LOG_TAG = "AGROBANCO_LOG";
    val UPDATE_VISITAS= "Lista de visitas actualizada";

    /* Fragmento Visita*/
    val PROXIMAS_VISITAS = 1
    val PENDIENTES_ENVIO = 2

    /* Tipo Evaluacion*/
    val TIPO_EVALUACION = "TIPO_EVALUACION"
    val EVALUACION_INICIAL = 1
    val EVALUACION_SEGUIMIENTO = 2
    val EVALUACION_RIESGO = 3
    val EVALUACION_FINAL = 4

    val PREDIO = "predio"

    /*utilitarios*/
    val ZONA_HORARIA = "America/Lima"
}
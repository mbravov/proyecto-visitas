package canvia.fonafeIII.agrobanco.util

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.view.util.CustomTextView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesUtil
import java.text.SimpleDateFormat
import java.util.*


class Utils {



    private val PREFS_NAME = "prefs_name"
    private  val ENCRYPTED_PREFS_NAME = "encrypted_$PREFS_NAME"

    private val sharedPrefs by lazy {
        AppConfiguracion.CONTEXT?.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE)
    }

    private val encryptedSharedPrefs by lazy {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        EncryptedSharedPreferences.create(
            ENCRYPTED_PREFS_NAME,
            masterKeyAlias,
            AppConfiguracion.CONTEXT!!,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }



    val APP_SETTING_FILES = "APP_SETTINGS"

    fun cleanPreferences() {
        //SharedPreferences shred = MyApp.getContext().getSharedPreferences(APP_SETTING_FILES,MyApp.getContext().MODE_PRIVATE);
        //shred.edit().clear();
        val editor = getSharedPreferences()?.edit()
        editor?.clear()
        editor?.commit()
    }

    private fun getSharedPreferences(): SharedPreferences? {
        return AppConfiguracion.CONTEXT?.getSharedPreferences(
            APP_SETTING_FILES,
           Context.MODE_PRIVATE
        )
    }



    fun setSomeIntValue(datalabel: String?, value: Int?,encrypted: Boolean = false) {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        val editor = prefs?.edit()
        editor?.putInt(datalabel, value!!)
        editor?.commit()
    }

    fun setSomeStringValue(datalabel: String?, value: String?,encrypted: Boolean = false) {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        val editor = prefs?.edit()
        editor?.putString(datalabel, value)
        editor?.commit()
    }

    fun setSomeBooleanValue(datalabel: String?, value: Boolean,encrypted: Boolean = false) {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        val editor = prefs?.edit()
        editor?.putBoolean(datalabel, value)
        editor?.commit()
    }



    fun getSomeIntvalue(label: String?,default: Int? = null, encrypted: Boolean = false): Int? {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        return prefs?.getInt(label, default!!)
    }
    fun getSomeStringvalue(label: String?,default: String? = null, encrypted: Boolean = false): String? {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        return prefs?.getString(label, default)
    }

    fun getSomeBooleanvalue(label: String?,default: Boolean = false, encrypted: Boolean = false): Boolean? {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        return prefs?.getBoolean(label, default)
    }
    fun checkDataBase():Boolean{
        return true
    }

    fun crearckDataBase(){

    }

    fun mostrarMensajeError(titulo: String?, mensaje: String?, context: Context?) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(mensaje)
            .setCancelable(false)
            .setPositiveButton("OK", null)
        builder.show()
    }
    fun mostrarMensajeErrorPersonaliza(
        titulo: String?,
        mensaje: String?,
        context: Context?,
        boton: String?
    ) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(mensaje)
            .setCancelable(false)
            .setPositiveButton(boton, null)
        builder.show()
    }


    fun compruebaConexion(context: Context): Boolean {
        var connected = false
        val connec = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val redes = connec.allNetworkInfo
        for (i in redes.indices) {
            if (redes[i].state == NetworkInfo.State.CONNECTED) {
                connected = true
            }
        }
        return connected
    }

    fun mostrarToast(texto: String?, context: Context?) {
        Toast.makeText(context, texto, Toast.LENGTH_LONG).show()
    }

    fun obtenerFechaConFormato(
        formato: String?,
        zonaHoraria: String?
    ): String? {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.getTime()
        val sdf: SimpleDateFormat
        sdf = SimpleDateFormat(formato)
        sdf.setTimeZone(TimeZone.getTimeZone(zonaHoraria))
        return sdf.format(date)
    }

    fun obtenerHoraActual(zonaHoraria: String?): String? {
        val formato = "HH:mm:ss"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }

    fun obtenerFechaActual(zonaHoraria: String?): String? {
        val formato = "yyyy-MM-dd"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }
    fun obtenerFechaActual2(zonaHoraria: String?): String? {
        val formato = "dd/MM/yyy"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }
    fun obtenerFechaHoraActual(zonaHoraria: String?): String? {
        return obtenerFechaActual(zonaHoraria) + " " + obtenerHoraActual(zonaHoraria)
    }

    //21/01/2020 -> 2020-01-21T00:00:00
    fun cambiarFormatoFechaSiembra(fecha: String?): String?{
        var fecha_formato = "2000-01-01T00:00:00"

        if(fecha!=null && fecha.length==10)
            fecha_formato = fecha!!.substring(6,10) + "-" + fecha!!.substring(3,5) + "-" + fecha!!.substring(0,2) + "T00:00:00"

        return fecha_formato
    }

    //2020-01-21 00:00:00 -> 21/01/2020
    fun cambiarFormatoFechaResumen(fecha: String?): String?{
        var fecha_formato = "01/01/2000"

        if(fecha!=null && fecha.length==19)
            fecha_formato = fecha!!.substring(8,10) + "/" + fecha!!.substring(5,7) + "/" + fecha!!.substring(0,4)

        return fecha_formato
    }

    fun aumentarEspacioBlanco(customTextView: CustomTextView, longitud: Int){
        var diferencia = longitud-customTextView.prefixText.length

        diferencia = (diferencia * 1.8).toInt()

        for (i in 1..diferencia) {
            customTextView.prefixText = customTextView.prefixText + " "
        }
    }

    fun getImei(context: Context): String?{
        var imei = ""

        val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.READ_PHONE_STATE), 1)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
            imei = try {
                telephonyManager.imei
            } catch (e: Exception) {
                e.printStackTrace()
                Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            }
        } else {
            imei =telephonyManager.deviceId
        }

        Log.e("Utils","getImei: "+imei)

        return imei
    }

    fun checkGooglePlayServices(context: Context): Boolean {
        /*
        val status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)

        Log.e("Utils","GooglePlayServicesUtil-status: "+status)

        return if (status == 0) {//ConnectionResult.SUCCESS
            Log.e(TAG, GooglePlayServicesUtil.getErrorString(status))

            mostrarMensajeError("Play Service", "No tiene instalado Play Service \ndebe instalarlo!!!", context)
            false
        } else {
            Log.i(TAG, GooglePlayServicesUtil.getErrorString(status))
            // google play services is updated.
            //your code goes here...
            true
        }
*/


        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(context)

        Log.e("Utils","GooglePlayServicesUtil-status: "+status)

        //if (status != ConnectionResult.SUCCESS) {
        //    mostrarMensajeError("Play Service", "No tiene instalado Play Service \ndebe instalarlo!!!", context)
        //    return false
        //}
        return true

        /*
        val statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)
        if ((statusCode == ConnectionResult.SUCCESS)
                && (GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE >= -1)) {
            Log.e("Utils","GooglePlayServicesUtil-status-true: "+statusCode)
            return true;
        } else {
            Log.e("Utils","GooglePlayServicesUtil-status-false: "+statusCode)
            return false;
        }
         */
    }

}
package canvia.fonafeIII.agrobanco.view.activities

import android.Manifest
import android.app.Notification
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.view.fragments.evaluacion.*
import com.google.android.gms.maps.model.LatLng

class EvaluacionActivity : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var actionBar: ActionBar? = null

    var posicion: Int = 1
    var cambio_fragment = true


    var eval1RegistroFragment: Eval1RegistroFragment? = null
    var eval2CapAreaFragment: Eval2CapAreaFragment? = null
    var eval3CapFotosFragment: Eval3CapFotosFragment? = null
    var eval4CapFotosCompFragment: Eval4CapFotosCompFragment? = null
    var eval5RegistroProductorFragment: Eval5RegistroProductorFragment? = null
    var eval6CreditoAgricolaIFragment:Eval6CreditoAgricolaIFragment? = null
    var eval7CreditoAgricola2Fragment: Eval7CreditoAgricola2Fragment? = null
    var eval8CreditoAgricola3Fragment: Eval8CreditoAgricola3Fragment? = null
    var eval9CreditoPecuarioFragment: Eval9CreditoPecuarioFragment? = null
    var eval10CreditoPecuario2Fragment: Eval10CreditoPecuario2Fragment? = null
    var eval11PrediosColindantesFragment: Eval11PrediosColindantesFragment? = null
    var eval12ComentariosFragment: Eval12ComentariosFragment? = null
    var eval13FirmasFragment: Eval13FirmasFragment? = null
    var eval14ResumenFragment: Eval14ResumenFragment? = null
    var eval15Resumen2Fragment: Eval15Resumen2Fragment? = null
    var eval16Resumen3Fragment: Eval16Resumen3Fragment? = null
    var eval17Resumen4Fragment: Eval17Resumen4Fragment? = null
    var eval18Resumen5Fragment: Eval18Resumen5Fragment? = null
    var latLng_markers = mutableListOf<LatLng>()

    var id_visita: String?=null
    var username: String? = null
    var origen_fragmento: Int? = null

    var entrar=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evaluacion)

        username = intent.extras!!.getString("username")
        id_visita = intent.extras!!.getString("id_visita")
        origen_fragmento = intent.extras!!.getInt("origen_fragmento")

        Log.e("EvaluacionActivity","username: "+username)
        Log.e("EvaluacionActivity","id_visita: "+id_visita)
        Log.e("EvaluacionActivity","origen_fragmento: "+origen_fragmento)

        toolbar = findViewById(R.id.toolbar) as Toolbar

        setSupportActionBar(toolbar)

        actionBar = supportActionBar

        //actionBar!!.setDisplayHomeAsUpEnabled(true)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.FOREGROUND_SERVICE), 1)
        }

        eval1RegistroFragment = Eval1RegistroFragment()
        eval2CapAreaFragment = Eval2CapAreaFragment()
        eval3CapFotosFragment = Eval3CapFotosFragment()
        eval4CapFotosCompFragment = Eval4CapFotosCompFragment()
        eval5RegistroProductorFragment = Eval5RegistroProductorFragment()
        eval6CreditoAgricolaIFragment = Eval6CreditoAgricolaIFragment()
        eval7CreditoAgricola2Fragment = Eval7CreditoAgricola2Fragment()
        eval8CreditoAgricola3Fragment = Eval8CreditoAgricola3Fragment()
        eval9CreditoPecuarioFragment = Eval9CreditoPecuarioFragment()
        eval10CreditoPecuario2Fragment = Eval10CreditoPecuario2Fragment()
        eval11PrediosColindantesFragment = Eval11PrediosColindantesFragment()
        eval12ComentariosFragment = Eval12ComentariosFragment()
        eval13FirmasFragment = Eval13FirmasFragment()
        eval14ResumenFragment = Eval14ResumenFragment()
        eval15Resumen2Fragment = Eval15Resumen2Fragment()
        eval16Resumen3Fragment = Eval16Resumen3Fragment()
        eval17Resumen4Fragment = Eval17Resumen4Fragment()
        eval18Resumen5Fragment = Eval18Resumen5Fragment()

        makeCurrentFragment(eval1RegistroFragment!!)
        cambiarTitulo("Evaluación Inicial")
    }

    fun cambiarTitulo(titulo: String?) {
        if (supportActionBar != null) {
            supportActionBar!!.title = titulo
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            if(cambio_fragment) {
                replace(R.id.fragmentContainer, fragment)
                commit()
            }
        }

    private fun makeCurrentFragment() = supportFragmentManager.beginTransaction().apply {
        if(cambio_fragment) {
            replace(R.id.fragmentContainer, eval2CapAreaFragment!!)
            commit()
        }
    }

    private fun  devolverFragment(): Fragment {
        Log.e(" devolverFragment: ", ""+posicion)
        return when (posicion) {
            1 -> return eval1RegistroFragment!!
            2 -> return eval2CapAreaFragment!!
            //2 -> return eval1RegistroFragment!!
            3 -> return eval3CapFotosFragment!!
            //3 -> return eval1RegistroFragment!!
            4 -> return eval4CapFotosCompFragment!!
            //4 -> return eval5RegistroProductorFragment!!
            5 -> return eval5RegistroProductorFragment!!
            6 -> return eval6CreditoAgricolaIFragment!!
            7 -> return eval7CreditoAgricola2Fragment!!
            8 -> return eval8CreditoAgricola3Fragment!!
            9 -> return eval9CreditoPecuarioFragment!!
            10 -> return eval10CreditoPecuario2Fragment!!
            11 -> return eval11PrediosColindantesFragment!!
            12 -> return eval12ComentariosFragment!!
            13 -> return eval13FirmasFragment!!
            14 -> return eval14ResumenFragment!!
            15 -> return eval15Resumen2Fragment!!
            16 -> return eval16Resumen3Fragment!!
            17 -> return eval17Resumen4Fragment!!
            //18 -> return eval18Resumen5Fragment!!
            else -> eval1RegistroFragment!!
        }
    }

    private fun  retroceder(): Fragment {
        Log.e("retroceder:", " 1")
        cambio_fragment = false
        if(posicion>1){
            posicion = posicion - 1
            cambio_fragment = true
        }
        Log.e("retroceder:", " 2")
        return devolverFragment()
    }

    private fun avanzar(): Fragment {
        Log.e("avanzar:", " 1")
        cambio_fragment = false
        if(posicion<18){
            posicion = posicion + 1
            cambio_fragment = true
            Log.e("avanzar:", " 2")
        }
        return devolverFragment()
    }

    fun avanzar_fragmento(){
        makeCurrentFragment(avanzar())
    }

    fun ir_fragmento(pos: Int){
        posicion = pos
        cambio_fragment = true
        makeCurrentFragment(devolverFragment())
    }

    fun retroceder_fragmento(){
        makeCurrentFragment(retroceder())
    }

    fun finalizar(){
        stopDisconnectTimer()
        val intent = Intent(applicationContext, VisitaActivity::class.java)
        intent.putExtra("username", ""+username)
        intent.putExtra("origen", ""+origen_fragmento)
        this.startActivity(intent)
        finish()
    }

    fun salirActivityEvaluacion() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Está seguro que desea volver al menu Principal?")
            .setTitle("Aviso")
            .setCancelable(false)
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })
            .setPositiveButton("Sí",
                DialogInterface.OnClickListener { dialog, id ->

           //         supportFragmentManager.beginTransaction().remove(devolverFragment())

                    finalizar()
                })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    override fun onBackPressed() {
        salirActivityEvaluacion()
    }

    fun terminoTiempoSesion() {
        if(entrar) {
            entrar = false
            val builder = AlertDialog.Builder(this)
            builder.setMessage("¿Desa continuar en la aplicación?")
                .setTitle("Se agoto el tiempo de Actividad")
                .setCancelable(false)
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        entrar = true
                        stopDisconnectTimer()
                        irActivityLogin()
                    })
                .setPositiveButton("Sí",
                    DialogInterface.OnClickListener { dialog, id ->
                        entrar = true
                        //ejecutarTarea()
                        resetDisconnectTimer()
                        dialog.cancel()
                    })
            val alert: AlertDialog = builder.create()
            alert.show()
        }
    }

    fun aplicaionPausada() {
        stopDisconnectTimer()
        entrar = false
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Desea cerrar la aplicación?")
            .setTitle("Aplicación Pausada")
            .setCancelable(false)
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialog, id ->
                    resetDisconnectTimer()
                    entrar = true
                    dialog.cancel()
                })
            .setPositiveButton("Sí",
                DialogInterface.OnClickListener { dialog, id ->
                    stopDisconnectTimer()
                    finish()
                })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    fun irActivityLogin(){
        stopDisconnectTimer()
        val intent = Intent(applicationContext, LoginActivity::class.java)
        this.startActivity(intent)
        this.finish()
    }

    val DISCONNECT_TIMEOUT: Long = 300000 // 5 min = 5 * 60 * 1000 ms

    private val disconnectHandler: Handler = object : Handler() {
        fun handleMessage(msg: Notification.MessagingStyle.Message?) {}
    }

    private val disconnectCallback = Runnable {
        Log.e("disconnectCallback","cesar")
        terminoTiempoSesion()
    }

    fun resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback)
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT)
    }

    fun stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback)
    }

    override fun onUserInteraction() {
        Log.e("onUserInteraction","cesar")
        resetDisconnectTimer()
    }

    override fun onResume() {
        super.onResume()
        resetDisconnectTimer()
    }

    override fun onStop() {
        Log.e("onStop Evaluacion","cesar")
        Log.d("liberalocation","stopactivity")
        Log.e("seguimiento:","onstopactivity")
        super.onStop()
        stopDisconnectTimer()
    }
    var pausar = false
    /*
    override fun onPause() {
        Log.e("onPause","Evaluacion: "+pausar)
        super.onPause()
        if(pausar) {
            aplicaionPausada()
        }
    }
     */

    override fun onPostResume() {
        pausar = true
        Log.e("onResume Evaluacion","cesar: "+pausar)
        Log.e("seguimiento:","onresumeactivity")
        super.onPostResume()
    }

    override fun onDestroy() {
        Log.e("seguimiento:","ondestroyactivity")
        super.onDestroy()
    }

}

package canvia.fonafeIII.agrobanco.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Imagen
import canvia.fonafeIII.agrobanco.view.contracts.FotosContract

class FotoAdapter(listado: MutableList<Imagen>, view: FotosContract.View) : RecyclerView.Adapter<FotoAdapter.ViewHolder>() {
    private val mListado: MutableList<Imagen>
    private val mView: FotosContract.View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_imagen, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imagen: Imagen = mListado.get(position)
        val posicion: Int = imagen.posicion
        val nombre = "imagen $posicion.jpg"
        holder.tvNombre!!.text = nombre
        holder.setItemActionListener(object : ItemActionListener {
            override fun onItemClick(position: Int) {
                val i: Imagen = mListado.get(position)
                mView.visualizarImagen(i)
            }

            override fun onDeleteClick(position: Int) {
                val i: Imagen = mListado.get(position)
                mView.eliminarImagen(i)
            }
        })
    }

    override fun getItemCount(): Int {
        return mListado.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNombre: TextView? = null
        var ivRemover: ImageView? = null
        private var itemActionListener: ItemActionListener? = null
        fun setItemActionListener(itemActionListener: ItemActionListener?) {
            this.itemActionListener = itemActionListener
        }

        init {
            tvNombre = itemView.findViewById<TextView>(R.id.tvNombres)
            tvNombre!!.setOnClickListener(View.OnClickListener {
                if (itemActionListener != null) {
                    itemActionListener!!.onItemClick(adapterPosition)
                }
            })

            ivRemover = itemView.findViewById<ImageView>(R.id.ivRemover)
            ivRemover!!.setOnClickListener(View.OnClickListener {
                if (itemActionListener != null) {
                    itemActionListener!!.onDeleteClick(adapterPosition)
                }
            })

        }
    }

    interface ItemActionListener {
        fun onItemClick(position: Int)
        fun onDeleteClick(position: Int)
    }

    init {
        mListado = listado
        mView = view
    }
}
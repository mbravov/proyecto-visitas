package canvia.fonafeIII.agrobanco.view.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Imagen
import canvia.fonafeIII.agrobanco.view.util.base.BaseRecyclerAdapter

class FotoResumenAdapter: BaseRecyclerAdapter<Imagen>() {

    override fun getLayout(): Int = R.layout.layout_foto_resumen

    override fun onBindViewHold(position: Int, itemView: View) {
        val image: AppCompatImageView = itemView.findViewById(R.id.image)
        list[position].ruta?.let {
            val bitmap = BitmapFactory.decodeFile(it)
            image.setImageBitmap(bitmap)
        }
    }

    override fun onClick(itemView: View, position: Int, data: Imagen) {

    }
}
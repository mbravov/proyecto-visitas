package canvia.fonafeIII.agrobanco.view.adapter

import android.util.Log
import android.util.SparseBooleanArray
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.view.contracts.PendientesEnvioContract
import com.bumptech.glide.Glide
import java.util.*


class PendientesEnvioAdapter(listadoVisitas: List<Visita>?, view: PendientesEnvioContract.View?): RecyclerView.Adapter<PendientesEnvioAdapter.ViewHolder>() {
    private var mListadoVisitas: List<Visita>? = null
    private var mSelectedItems: SparseBooleanArray? = null
    private var mView: PendientesEnvioContract.View? = null

    init {
        this.mListadoVisitas = listadoVisitas
        this.mView = view
        this.mSelectedItems = SparseBooleanArray()
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvPredio: TextView? = null
        var tvUsuario: TextView? = null
        var tvFecha: TextView? = null
        var ivSincronizado: ImageView? = null
        var ivFinalizado: ImageView? = null
        var ivCompletado: ImageView? = null

        var ivPredio: ImageView? = null
        var tvTipoEvaluacion: TextView? = null
        //var item_predio: LinearLayout? = null
        private var itemActionListener: ItemActionListener? = null

        fun setItemActionListener(itemActionListener: ItemActionListener?) {
            this.itemActionListener = itemActionListener
        }

        fun onItemClick() {
            if (itemActionListener != null) {
                itemActionListener!!.onItemClick(adapterPosition)
            }
        }

        fun onItemLongClick(v: View): Boolean {
            if (itemActionListener != null) {
                v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                itemActionListener!!.onItemLongClick(v, adapterPosition)
            }
            return true
        }

        init {
            tvPredio = itemView.findViewById<TextView>(R.id.tvPredio)
            tvUsuario = itemView.findViewById(R.id.tvUsuario)
            tvFecha = itemView.findViewById(R.id.tvFecha)
            ivSincronizado = itemView.findViewById(R.id.ivSincronizado)
            ivCompletado = itemView.findViewById(R.id.ivCompletado)

            ivFinalizado = itemView.findViewById(R.id.ivFinalizado)
            ivPredio = itemView.findViewById(R.id.ivPredio)
            tvTipoEvaluacion = itemView.findViewById(R.id.tvTipoEvaluacion)

            ivFinalizado!!.setOnClickListener(View.OnClickListener {
                if (itemActionListener != null) {
                    itemActionListener!!.onItemClick(adapterPosition)
                }
            })

            ivSincronizado!!.setOnClickListener(View.OnClickListener{

                if(itemActionListener!=null){

                    itemActionListener!!.onItemClickSincronizar(adapterPosition)

                }



            })


        }
    }


    interface ItemActionListener {
        fun onItemClick(posicion: Int)
        fun onItemClickSincronizar(posicion: Int)
        fun onItemLongClick(view: View?, posicion: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_visita, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.mListadoVisitas!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val visita: Visita = mListadoVisitas!!.get(position)
        var fecha_registro = ""

        if(visita.fecha_crea!=null)
            fecha_registro = visita.fecha_crea!!

                    //Cambiando estado de la vista

        //Cambiando estado de la vista
        holder.itemView.isActivated = mSelectedItems!![position, false]

        if (visita!=null && visita.fecha_crea!=null && !visita.fecha_crea.equals("")) {
            holder.tvFecha!!.setText(visita.fecha_crea)
        }

//        holder.tvTipoEvaluacion.setText(predio.);

//        holder.tvTipoEvaluacion.setText(predio.);
        holder.tvPredio!!.setText(visita.nombre_predio)
        holder.tvUsuario!!.setText(visita.getNombres())

        var tipo: String
        tipo = "EVALUACION INICIAL"

        if(visita.tipo_visita!=null){
            Log.e("onBindViewHolder","visita.tipo_visita: "+visita.tipo_visita!!)
            tipo =when (visita.tipo_visita!!.toInt()) {
                Constants().EVALUACION_SEGUIMIENTO -> "EVALUACIÓN SEGUIMIENTO"
                Constants().EVALUACION_RIESGO -> "EVALUACIÓN RIESGO CREDITICIO"
                Constants().EVALUACION_FINAL -> "EVALUACIÓN FINAL"
                else -> "EVALUACION INICIAL"
            }
        }

        holder.tvTipoEvaluacion!!.text = tipo

        if (visita!!.finalizado) {
            holder.ivSincronizado!!.visibility = View.VISIBLE
            holder.ivFinalizado!!.visibility = View.GONE
        } else {
            holder.ivSincronizado!!.visibility = View.GONE
            holder.ivFinalizado!!.visibility = View.VISIBLE
        }

        if (visita!!.sincronizado) {
            holder.ivCompletado!!.visibility=View.VISIBLE
            holder.ivSincronizado!!.visibility = View.GONE
            holder.ivFinalizado!!.visibility =View.GONE
        } else {
            holder.ivCompletado!!.visibility=View.GONE
        }

        if (visita.rutaImagen != null) {

            Glide.with(holder.itemView).load(visita!!.rutaImagen)
                .into(holder.ivPredio!!)

        } else {
            holder.ivPredio!!.setImageResource(R.mipmap.ic_insert_photo_black_24dp)
        }

        holder.setItemActionListener(object : ItemActionListener {
            override fun onItemClick(posicion: Int) {
                mView!!.seleccionarVisita(mListadoVisitas!!.get(posicion), posicion)
            }

            override fun onItemClickSincronizar(posicion: Int) {
            mView!!.sincronizarDatos(mListadoVisitas!!.get(posicion),posicion)

            }

            override fun onItemLongClick(view: View?, posicion: Int) {
                view!!.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                mView!!.seleccionLargVisita(posicion)
            }
        })
    }

    fun toggleSelection(pos: Int) {
        if (mSelectedItems!![pos, false]) {
            mSelectedItems!!.delete(pos)
        } else {
            mSelectedItems!!.put(pos, true)
        }
        notifyItemChanged(pos)
    }

    fun clearSelections() {
        mSelectedItems!!.clear()
        notifyDataSetChanged()
    }

    fun getSelectedItemCount(): Int {
        return mSelectedItems!!.size()
    }

    fun getSelectedVisitas(): List<Visita>? {
        val listado: MutableList<Visita> = ArrayList<Visita>()
        for (i in 0 until mSelectedItems!!.size()) {
            listado.add(mListadoVisitas!!.get(mSelectedItems!!.keyAt(i)))
        }
        return listado
    }
}
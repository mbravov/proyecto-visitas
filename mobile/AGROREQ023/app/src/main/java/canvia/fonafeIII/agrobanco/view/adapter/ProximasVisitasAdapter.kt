package canvia.fonafeIII.agrobanco.view.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.util.Constants


class ProximasVisitasAdapter(var visitas: ArrayList<Visita>) : RecyclerView.Adapter<ProximasVisitasAdapter.ViewHolder>() {
    private var mVisitas: ArrayList<Visita>? = visitas
    private var mListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.row_prox_visita, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mVisitas!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val visita = mVisitas!![position]

        holder.tvNombre.text = visita.getNombres()
        holder.tvApellido.text = visita.getApellidos()
        holder.tvDocumento.text = visita.dni_cliente
        holder.tvPredioUUID.text = visita.nombre_predio

        when {
            visita.finalizado ->
                holder.ivAcceso.setImageResource(R.mipmap.ic_label_green_500_24dp)
            visita.iniciado!!.toInt() == Constants().EN_PROCESO ->
                holder.ivAcceso.setImageResource(R.drawable.ic_bandera_proxima_visita_ambar)
            else->holder.ivAcceso.setImageResource(R.drawable.ic_bandera_proximas_visitas)
        }

        holder.itemView.setOnClickListener {
            if (!visita.finalizado) {
                mListener?.onItemClick(visita)
            }
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNombre: TextView
        var tvDocumento: TextView
        var tvPredioUUID: TextView
        var ivAcceso: ImageView
        var tvApellido: TextView

        init {
            tvNombre = itemView.findViewById(R.id.tvNombres)
            tvDocumento = itemView.findViewById(R.id.tvDocumento)
            tvPredioUUID = itemView.findViewById(R.id.tvPredioUUID)
            ivAcceso = itemView.findViewById(R.id.ivAcceso)
            tvApellido = itemView.findViewById(R.id.tvApellidos)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(visita: Visita)
    }

    fun OnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }
}
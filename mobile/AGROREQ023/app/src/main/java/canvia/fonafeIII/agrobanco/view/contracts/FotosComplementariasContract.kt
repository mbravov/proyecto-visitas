package canvia.fonafeIII.agrobanco.view.contracts

import canvia.fonafeIII.agrobanco.model.pojos.Imagen
import canvia.fonafeIII.agrobanco.model.pojos.ImagenComplemtaria

interface FotosComplementariasContract {

    interface View  {
        fun abrirCamara(nombreArchivo: String?)
        fun mostrarListado(listado: MutableList<ImagenComplemtaria>)
        fun visualizarImagen(imagen: ImagenComplemtaria?)
        fun eliminarImagen(imagen: ImagenComplemtaria?)
        fun mostrarMensajeError(texto: String?)
    }
}
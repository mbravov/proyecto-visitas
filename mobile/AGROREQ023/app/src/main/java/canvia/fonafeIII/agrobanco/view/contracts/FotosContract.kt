package canvia.fonafeIII.agrobanco.view.contracts

import canvia.fonafeIII.agrobanco.model.pojos.Imagen

interface FotosContract {
    interface View  {
        fun abrirCamara(nombreArchivo: String?)
        fun mostrarListado(listado: MutableList<Imagen>)
        fun visualizarImagen(imagen: Imagen?)
        fun eliminarImagen(imagen: Imagen?)
        fun mostrarMensajeError(texto: String?)
    }
}
package canvia.fonafeIII.agrobanco.view.contracts

import android.content.Context
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import java.io.File

interface PendientesEnvioContract {
    interface View {
        fun mostrarListado(listadoVisitas: List<Visita?>?)
        fun seleccionarVisita(visita: Visita?, posicion: Int)
        fun sincronizarDatos(visita: Visita?, posicion: Int)

        fun seleccionLargVisita(posicion: Int)
        fun mostrarMensajeListadoVacio()
        fun mostrarError(texto: String?)
        fun mostrarError(titulo: String?, texto: String?)
        fun mostrarMensajeFinal(texto: String?)
        fun redireccionarLogin()
        fun alternarProgress(activado: Boolean)
        fun cambiarTextoSincronizacion(texto: String?)
        fun dirImages(): File?

        fun obtenerContext(): Context?
    }
}
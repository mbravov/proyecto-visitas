package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoPecuarioII
import canvia.fonafeIII.agrobanco.model.pojos.Crianza
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity


/**
 * CUS026: Registrar y editar datos de créditos pecuarios paso 2: Datos de la crianza
 */
class Eval10CreditoPecuario2Fragment : Fragment() {
    var spCrianza: Spinner?=null
    var etRaza: EditText?=null
    var etExperiencia: EditText?=null
    var rgTecnologia: RadioGroup?=null
    var rb_T_Medio: RadioButton?=null
    var rgManejo: RadioGroup?=null
    var rb_M_Facil: RadioButton?=null

    var cod_crianza_seleccionado: String?=null
    var lista: MutableList<Crianza> = mutableListOf()

    var creditoPecuarioII: CreditoPecuarioII? = null
    lateinit var repo: Repositorio
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval10_credito_pecuario2, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        etRaza = rootView.findViewById(R.id.etRaza)
        etExperiencia = rootView.findViewById(R.id.etExperiencia)
        rgTecnologia = rootView.findViewById(R.id.rgTecnologia)
        rb_T_Medio = rootView.findViewById(R.id.rb_T_Medio)
        rgManejo = rootView.findViewById(R.id.rgManejo)
        rb_M_Facil = rootView.findViewById(R.id.rb_M_Facil)


        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }
        })



        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lista =repo.selectListaCrianzas()

        spCrianza = view.findViewById(R.id.spCrianza)

        if(lista==null || lista.size==0){
            insertarCrianza()
        }

        val adapter: ArrayAdapter<Crianza> = ArrayAdapter<Crianza>(requireActivity(), android.R.layout.simple_list_item_1, lista.toTypedArray())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spCrianza!!.adapter = adapter


        spCrianza!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val crian:Crianza= parent?.getItemAtPosition(position) as Crianza
                cod_crianza_seleccionado=crian.cod_crianza
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        inicio()
    }

    fun llenarVariables() {
        creditoPecuarioII!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        creditoPecuarioII!!.crianza = cod_crianza_seleccionado
        if(etRaza!!.getText().trim().length>0  ) {
            creditoPecuarioII!!.raza = etRaza!!.text.toString().trim()
        }
        if(etExperiencia!!.getText().trim().length>0  ) {
            creditoPecuarioII!!.experiencia_anios = etExperiencia!!.text.toString().trim().toInt()
        }else creditoPecuarioII!!.experiencia_anios = null
        creditoPecuarioII!!.tipo_tecnologia =
            rgTecnologia!!.indexOfChild(rgTecnologia!!.findViewById(rgTecnologia!!.getCheckedRadioButtonId()))
        creditoPecuarioII!!.manejo_crianza =
            rgManejo!!.indexOfChild(rgManejo!!.findViewById(rgManejo!!.getCheckedRadioButtonId()))

        Log.e("Pecuarios2","llenarVariables-tipo_tecnologia: "+creditoPecuarioII!!.tipo_tecnologia)
        Log.e("Pecuarios2","llenarVariables-manejo_crianza: "+creditoPecuarioII!!.manejo_crianza)
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if(creditoPecuarioII!!.crianza==null){
            (spCrianza!!.selectedView as TextView).error = "Seleccione un tipo de Crianza"
            correcto = false
        }

        if (creditoPecuarioII!!.raza==null || creditoPecuarioII!!.raza.equals("")) {
            etRaza!!.error = "Ingresar la Raza"
            correcto = false
        }

        if (creditoPecuarioII!!.experiencia_anios==null) {
            etExperiencia!!.error = "Ingresar Experiencia (años)"
            correcto = false
        }else if(creditoPecuarioII!!.experiencia_anios!!<0){
            etExperiencia!!.error = "Experiencia (años), no puede ser negativo"
            correcto = false
        }

        if (creditoPecuarioII!!.tipo_tecnologia == -1 || creditoPecuarioII!!.tipo_tecnologia == null) {
            rb_T_Medio!!.error = "Seleccione un Tipo de Tecnología"
            correcto = false
        } else {
            rb_T_Medio!!.error = null
        }

        if (creditoPecuarioII!!.manejo_crianza == -1 || creditoPecuarioII!!.manejo_crianza == null) {
            rb_M_Facil!!.error = "Seleccione un elemento de Manejo"
            correcto = false
        } else {
            rb_M_Facil!!.error = null
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoCreditoPecuarioII(creditoPecuarioII!!)
        }

        return guardado
    }

    private fun inicio() {
        creditoPecuarioII = CreditoPecuarioII("")
        if (repo.existeElementoCreditoPecuarioII((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoPecuarioII = repo.selectEvalCreditoPecuarioII((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (creditoPecuarioII != null) {
            if (creditoPecuarioII!!.crianza != null)
                spCrianza!!.setSelection(calcularIndex(lista, creditoPecuarioII!!.crianza!!))
            if (creditoPecuarioII!!.raza != null) {
                etRaza!!.setText(creditoPecuarioII!!.raza.toString())
            }
            if (creditoPecuarioII!!.experiencia_anios != null) {
                etExperiencia!!.setText(creditoPecuarioII!!.experiencia_anios.toString())
            }
            if (creditoPecuarioII!!.tipo_tecnologia != null && creditoPecuarioII!!.tipo_tecnologia!=-1) {
                (rgTecnologia!!.getChildAt(creditoPecuarioII!!.tipo_tecnologia!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoPecuarioII!!.manejo_crianza != null && creditoPecuarioII!!.manejo_crianza!=-1) {
                (rgManejo!!.getChildAt(creditoPecuarioII!!.manejo_crianza!!.toInt()) as RadioButton).isChecked = true
            }
        }
    }

    fun insertarCrianza(){
        var posicion = 0
        lista = mutableListOf()

        for(item in resources.getStringArray(R.array.crianza)){
            val crianza = Crianza(posicion.toString(),item)
            lista.add(crianza)
            posicion++
        }

        repo.actualizarListaCrianzas(lista)
    }

    fun calcularIndex(lista: List<Crianza>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:Crianza  in lista){
            if(item.cod_crianza.equals(valor)){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }
}
package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.PredioColindante
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants


/**
 * CUS027: Registrar y editar información de predios colindantes
 */
class Eval11PrediosColindantesFragment : Fragment() {
    var etInfoPrediosColindantes: EditText?=null

    var predioColindante: PredioColindante? = null
    lateinit var repo: Repositorio
    var guardado = false

    var tipo_actividad = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval11_predios_colindantes, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        etInfoPrediosColindantes = rootView.findViewById(R.id.etInfoPrediosColindantes)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                if(tipo_actividad == 2)
                    (context as EvaluacionActivity).retroceder_fragmento()
                else
                    (context as EvaluacionActivity).ir_fragmento(Constants().EVAL8_CREDITO_AGRICOLA3)
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }
        })

        inicio()

        return rootView
    }

    fun llenarVariables() {
        predioColindante!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        if(etInfoPrediosColindantes!!.getText().trim().length>0  ) {
            predioColindante!!.comentario_predios_colindantes = etInfoPrediosColindantes!!.text.toString().trim()
        }
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if (predioColindante!!.comentario_predios_colindantes==null) {
            etInfoPrediosColindantes!!.error = "Ingresar el Comentario sobre los predios Colindantes"
            correcto = false
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoPredioColindante(predioColindante!!)
        }

        return guardado
    }

    private fun inicio() {
        if (repo.existeElementoRegistroInformacion((context as EvaluacionActivity)!!.id_visita!! )) {
            val evalRegistro =  repo.selectEvalRegistroItem((context as EvaluacionActivity)!!.id_visita!!)
            if (evalRegistro != null) {
                if (evalRegistro!!.tipo_actividad != null )
                    tipo_actividad = evalRegistro!!.tipo_actividad!!
            }
        }

        predioColindante = PredioColindante("")
        if (repo.existeElementoPredioColindante((context as EvaluacionActivity)!!.id_visita!!)) {
            predioColindante = repo.selectEvalPredioColindante((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (predioColindante != null) {
            if (predioColindante!!.comentario_predios_colindantes != null) {
                etInfoPrediosColindantes!!.setText(predioColindante!!.comentario_predios_colindantes.toString())
            }
        }
    }
}
package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Comentario
import canvia.fonafeIII.agrobanco.model.pojos.PredioColindante
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity

/**
 * CUS028: Registrar y editar comentarios
 */
class Eval12ComentariosFragment : Fragment() {
    var etInfoComentarios: EditText?=null

    var comentario: Comentario? = null
    lateinit var repo: Repositorio
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval12_comentarios, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        etInfoComentarios = rootView.findViewById(R.id.etInfoComentarios)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }
        })

        inicio()

        return rootView
    }

    fun llenarVariables() {
        comentario!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        if(etInfoComentarios!!.getText().trim().length>0  ) {
            comentario!!.comentario_recomendaciones = etInfoComentarios!!.text.toString().trim()
        }
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if (comentario!!.comentario_recomendaciones==null) {
            etInfoComentarios!!.error = "Ingresar el Comentario"
            correcto = false
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoComentario(comentario!!)
        }

        return guardado
    }

    private fun inicio() {
        comentario = Comentario("")
        if (repo.existeElementoComentario((context as EvaluacionActivity)!!.id_visita!!)) {
            comentario = repo.selectEvalComentario((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (comentario != null) {
            if (comentario!!.comentario_recomendaciones != null) {
                etInfoComentarios!!.setText(comentario!!.comentario_recomendaciones.toString())
            }
        }
    }
}
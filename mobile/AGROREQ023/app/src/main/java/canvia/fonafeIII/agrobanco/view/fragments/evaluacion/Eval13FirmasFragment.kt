package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Firmas
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.*
import com.google.gson.Gson
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import java.security.Permission

/**
 * CUS029: Registrar y editar firmas
 */
class Eval13FirmasFragment : Fragment() {

    var guardado = false
    private lateinit var hostCanvasView: MyCanvasView
    private lateinit var conyugeCanvasView: MyCanvasView
    private lateinit var clearHost: ImageButton
    private lateinit var clearConyuge: ImageButton
    private lateinit var layout1: LinearLayout
    private lateinit var layout2: LinearLayout
    private lateinit var regresar: TextView
    private lateinit var siguiente: TextView

    private var job: Job? = null

    private var repository: Repositorio? = null

    private val requested = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val REQUEST_PERMISSION_CODE = 120

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = Repositorio(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_eval13_firmas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        regresar = view.findViewById(R.id.tvRegresar)
        siguiente = view.findViewById(R.id.tvAvanzar)
        hostCanvasView = view.findViewById(R.id.signHost)
        conyugeCanvasView = view.findViewById(R.id.signConyuge)
        clearHost = view.findViewById(R.id.btnClearHost)
        clearConyuge = view.findViewById(R.id.btnClearConyuge)

        layout1 = view.findViewById(R.id.linear1)
        layout2 = view.findViewById(R.id.linear2)

        setupViews()
        setupListeners()

        searchStored()

    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    requested[0]
                ) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(
                    requireContext(),
                    requested[1]
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    requested,
                    REQUEST_PERMISSION_CODE
                )
            } else {
                guardar()
            }
        } else {
            guardar()
        }

    }

    private fun searchStored() {
        (requireActivity() as EvaluacionActivity).apply {
            job = GlobalScope.launch {
                repository?.selectEvalFirmas(id_visita ?: "")?.collect {
                    withContext(Dispatchers.Main) {
                        it?.comprimido_firma_titular?.base64toBitmap()?.let { bitmap ->
                            hostCanvasView.setPreviousBitmap(bitmap)
                        }
                        it?.comprimido_firma_conyuge?.base64toBitmap()?.let { bitmap ->
                            conyugeCanvasView.setPreviousBitmap(bitmap)
                        }
                    }
                }
            }

        }
    }

    private fun setupViews() {
        layout1.setShapeWithBorder(
                Color.WHITE,
                resources.getDimension(R.dimen.border_normal),
                Color.BLACK,
                resources.getDimension(R.dimen.stroke_normal).toInt()
        )
        layout2.setShapeWithBorder(
                Color.WHITE,
                resources.getDimension(R.dimen.border_normal),
                Color.BLACK,
                resources.getDimension(R.dimen.stroke_normal).toInt()
        )
    }

    private var isNext = true

    private fun setupListeners() {
        regresar.setOnClickListener {
            requestPermission()
            isNext = false
        }

        siguiente.setOnClickListener {
            requestPermission()
            isNext = true
        }

        clearHost.setOnClickListener {
            hostCanvasView.clearCanvas()
        }

        clearConyuge.setOnClickListener {
            conyugeCanvasView.clearCanvas()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                guardar()
            } else {
                Utils().mostrarMensajeError( "Permisos", "Debe aceptar los permisos para continuar", requireActivity())
            }
        }
    }

    private fun validate(): Boolean {
        if (!hostCanvasView.isDrawValid) {
            Utils().mostrarMensajeError( "No se firmó", "Debe registrar su firma para continuar", requireActivity())
            return false
        }
        return true
    }

    private fun guardar() {
        if (!validate()) return

        (requireActivity() as EvaluacionActivity).apply {
            val firma = Firmas(id_visita ?: "").apply {
                comprimido_firma_titular = hostCanvasView.mExtraBitmap.bitmapToBase64()
                if (conyugeCanvasView.isDrawValid) {
                    comprimido_firma_conyuge = conyugeCanvasView.mExtraBitmap.bitmapToBase64()
                }
            }
            job = GlobalScope.launch {
                repository?.addElementoFirmas(firma)?.distinctUntilChanged()?.collect {
                    if (it) {
                        Log.i("Firmas", Gson().toJson(firma))
                        if (isNext) avanzar_fragmento() else retroceder_fragmento()
                    } else {
                        Utils().mostrarMensajeError("", "No se pudo guardar la informacion", requireContext())
                    }
                }

            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        repository = null
        job?.cancel()
    }

    override fun onDetach() {
        super.onDetach()
        repository = null
        job?.cancel()
    }

}
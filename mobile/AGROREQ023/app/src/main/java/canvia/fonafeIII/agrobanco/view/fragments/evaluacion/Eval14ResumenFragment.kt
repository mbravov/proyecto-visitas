package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.adapter.FotoResumenAdapter
import canvia.fonafeIII.agrobanco.view.util.base.BaseFragment
import canvia.fonafeIII.agrobanco.view.util.setHtmlText
import kotlinx.coroutines.*

/**
 * CUS030: Ver resumen - Pantalla 1
 */
class Eval14ResumenFragment : Fragment() {

    private lateinit var txtPredioName: TextView
    private lateinit var txtPredioArea: TextView
    private lateinit var txtUserFirstName: TextView
    private lateinit var txtUserSecondName: TextView
    private lateinit var txtUserLastName: TextView
    private lateinit var txtUserSecondLastName: TextView
    private lateinit var txtUserDNI: TextView
    private lateinit var rvPredioImages: RecyclerView
    private lateinit var rvPredioImages_comp: RecyclerView
    private lateinit var tvRegresar: TextView
    private lateinit var tvAvanzar: TextView

    private lateinit var repositorio: Repositorio
    private var id_visita = ""
    private var job: Job? = null
    private val adapter = FotoResumenAdapter()
    private val adapter_comp = FotoResumenAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(requireContext()).inflate(R.layout.fragment_eval14_resumen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        repositorio = Repositorio(requireContext())
        id_visita = (requireActivity() as? EvaluacionActivity)?.id_visita ?: ""

        tvRegresar = view.findViewById<TextView>(R.id.tvRegresar)
        tvAvanzar = view.findViewById<TextView>(R.id.tvAvanzar)
        txtPredioName = view.findViewById(R.id.txtPredioName)
        txtPredioArea = view.findViewById(R.id.txtPredioArea)
        txtUserFirstName = view.findViewById(R.id.txtUserFirstName)
        txtUserSecondName = view.findViewById(R.id.txtUserSecondName)
        txtUserLastName = view.findViewById(R.id.txtUserLastName)
        txtUserSecondLastName = view.findViewById(R.id.txtUserSecondLastName)
        txtUserDNI = view.findViewById(R.id.txtUserDNI)
        rvPredioImages = view.findViewById(R.id.rvPredioImages)
        rvPredioImages_comp = view.findViewById(R.id.rvPredioImages_comp)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        rvPredioImages.layoutManager = GridLayoutManager(requireContext(), 2)
        rvPredioImages_comp.layoutManager = GridLayoutManager(requireContext(), 2)
        rvPredioImages.adapter = adapter
        rvPredioImages_comp.adapter = adapter_comp
    }

    private fun fillUserData(registro: EvalRegistro) {
        txtPredioName.setHtmlText(getString(R.string.lbl_predio_name, registro.nombre_predio))
        txtUserFirstName.setHtmlText(getString(R.string.lbl_user_first_name, registro.primer_nombre_cliente))
        txtUserSecondName.setHtmlText(getString(R.string.lbl_user_second_name, registro.segundo_nombre_cliente))
        txtUserLastName.setHtmlText(getString(R.string.lbl_user_last_name, registro.apellido_paterno_cliente))
        txtUserSecondLastName.setHtmlText(getString(R.string.lbl_user_second_last_name, registro.apellido_materno_cliente))
        txtUserDNI.setHtmlText(getString(R.string.lbl_user_dni, registro.dni_cliente))
    }

    private fun fillArea(evalArea: EvalArea) {
        var area = ""
        if(evalArea!=null && evalArea.area!=null)
            area = evalArea.area.toString()
        txtPredioArea.setHtmlText(getString(R.string.lbl_predio_area, area))
    }

    private fun guardar() {
        runBlocking {
            val resumen = if (repositorio.existeElementoResumen(id_visita)) {
                repositorio.selectEvalResumen(id_visita)
            } else {
                Resumen(id_visita)
            }
            resumen.fecha_pantalla_1 = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            repositorio.addElementoResumen(resumen)
        }
    }

    private var data: EvalRegistro? = null

    private fun setupListeners() {
        tvRegresar.setOnClickListener {
            guardar()
            (context as EvaluacionActivity).retroceder_fragmento()
        }

        tvAvanzar.setOnClickListener {
            guardar()
            (context as EvaluacionActivity).avanzar_fragmento()
        }

        job = GlobalScope.launch {
            data = repositorio.selectEvalRegistroItem(id_visita)
            val coordinate = repositorio.selectEvalAreaItemSinImagen(id_visita)
            val pictures = repositorio.selectImagenLista(id_visita)
            val pictures2 = ArrayList<Imagen>()
            val pictures_comp = repositorio.selectImagenComplementariaLista(id_visita)
            withContext(Dispatchers.Main) {
                data?.let { fillUserData(it) }
                fillArea(coordinate)
                //pictures.addAll(pictures_comp.map { it.mapToImage() })
                adapter.list = pictures
                pictures2.addAll(pictures_comp.map { it.mapToImage() })
                adapter_comp.list = pictures2
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        job?.cancel()
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

}
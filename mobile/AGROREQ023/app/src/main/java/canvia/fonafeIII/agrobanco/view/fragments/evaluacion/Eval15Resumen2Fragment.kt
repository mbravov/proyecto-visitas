package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Agencia
import canvia.fonafeIII.agrobanco.model.pojos.Productor
import canvia.fonafeIII.agrobanco.model.pojos.Resumen
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants
import canvia.fonafeIII.agrobanco.view.util.setHtmlText
import kotlinx.coroutines.*

/**
 * CUS030: Ver resumen - Pantalla 2
 */
class Eval15Resumen2Fragment : Fragment() {

    private lateinit var txtDate: TextView
    private lateinit var txtAgency: TextView
    private lateinit var txtObjective: TextView
    private lateinit var txtPresent: TextView
    private lateinit var txtProducerPhone: TextView
    private lateinit var layout_family: LinearLayout
    private lateinit var txt_family_relation: TextView
    private lateinit var txt_family_name: TextView
    private lateinit var tvRegresar: TextView
    private lateinit var tvAvanzar: TextView

    private lateinit var repositorio: Repositorio
    private var id_visita = ""
    private var job: Job? = null
    private var acitvidad = 1 // Agricola por defecto

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_eval15_resumen2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        repositorio = Repositorio(requireContext())
        id_visita = (requireActivity() as? EvaluacionActivity)?.id_visita ?: ""

        tvRegresar = view.findViewById<TextView>(R.id.tvRegresar)
        tvAvanzar = view.findViewById<TextView>(R.id.tvAvanzar)
        txtDate = view.findViewById(R.id.txtDate)
        txtAgency = view.findViewById(R.id.txtAgency)
        txtObjective = view.findViewById(R.id.txtObjective)
        txtPresent = view.findViewById(R.id.txtPresent)
        txtProducerPhone = view.findViewById(R.id.txtProducerPhone)
        layout_family = view.findViewById(R.id.layout_family)
        txt_family_relation = view.findViewById(R.id.txt_family_relation)
        txt_family_name = view.findViewById(R.id.txt_family_name)
        setupViews()
        setupListeners()
    }

    private fun setupViews() {

    }

    private fun guardar() {
        runBlocking {
            val resumen = if (repositorio.existeElementoResumen(id_visita)) {
                repositorio.selectEvalResumen(id_visita)
            } else {
                Resumen(id_visita)
            }
            resumen.fecha_pantalla_2 = Utils().obtenerFechaHoraActual(canvia.fonafeIII.agrobanco.util.Constants().ZONA_HORARIA)
            repositorio.addElementoResumen(resumen)
        }
    }

    private fun setupListeners() {
        tvRegresar.setOnClickListener {
            guardar()
            (context as EvaluacionActivity).retroceder_fragmento()
        }
        tvAvanzar.setOnClickListener {
            guardar()
            if(acitvidad == 1) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }else {
                (context as EvaluacionActivity).ir_fragmento(Constants().EVAL17_RESUMEN_4)
            }
        }

        job = GlobalScope.launch {
            val producer = repositorio.selectEvalProductor(id_visita)
            val registro = repositorio.selectEvalRegistroItem(id_visita)
            withContext(Dispatchers.Main) {
                fillProducerData(producer)
                acitvidad = registro?.tipo_actividad ?: 1
            }
        }
    }

    private fun fillProducerData(productor: Productor) {
        txtDate.setHtmlText(getString(R.string.lbl_date, Utils().cambiarFormatoFechaResumen(productor.fecha_crea)))

        val lista = repositorio.selectListaAgencias()
        val index_agencia = calcularIndexAgencia(lista,productor.agencia!!)
        val agencia = lista.get(index_agencia)
        txtAgency .setHtmlText(getString(R.string.lbl_agency, agencia.oficina))

        txtObjective.setHtmlText(getString(R.string.lbl_visit_objective, productor.objetivo_visita))
        txtPresent.text = productor.tipo_presente ?: "Productor"
        txtProducerPhone.text = productor.numero_celular
        layout_family.isVisible = (productor.vinculo != null && productor.vinculo.toString().isNotEmpty())
        txt_family_relation.setHtmlText(getString(R.string.txtVinculoFamiliar, productor.vinculo))
        txt_family_name.setHtmlText(getString(R.string.txtNombreFamiliar, productor.nombre))
    }

    override fun onDetach() {
        super.onDetach()
        job?.cancel()
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    fun calcularIndexAgencia(lista: List<Agencia>, valor: Int): Int{
        var index=0
        var resultado_index=0
        for(item_age: Agencia in lista)
        {
            if(item_age.cod_agencia==valor){
                resultado_index=index
            }
            index++
        }

        return  resultado_index
    }

}
package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.compose.ui.unit.Constraints
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants
import canvia.fonafeIII.agrobanco.view.util.CustomTextView
import canvia.fonafeIII.agrobanco.view.util.base.BaseFragment
import kotlinx.coroutines.*

/**
 * CUS030: Ver resumen - Pantalla 3
 */
class Eval16Resumen3Fragment : BaseFragment() {

    private lateinit var txtParcela: CustomTextView
    private lateinit var txtUniCatastral: CustomTextView
    private lateinit var txtHas: CustomTextView
    private lateinit var txtHasSembrada: CustomTextView
    private lateinit var txtHasFinanciar: CustomTextView
    private lateinit var txtRegConduccion: CustomTextView
    private lateinit var txtUbicExp: CustomTextView
    private lateinit var txtEstadoCampo: CustomTextView
    private lateinit var txtTipoRiego: CustomTextView
    private lateinit var txtDispoAgua: CustomTextView
    private lateinit var txtPending: CustomTextView
    private lateinit var txtTipoSuelo: CustomTextView
    private lateinit var txtAccesibilidad: CustomTextView
    private lateinit var txtAltitud: CustomTextView
    private lateinit var txtCultivo: CustomTextView
    private lateinit var txtVariedad: CustomTextView
    private lateinit var txtSiembraFecha: CustomTextView
    private lateinit var txtAñoExp: CustomTextView
    private lateinit var txtTipoSemilla: CustomTextView
    private lateinit var txtRendimientoAnt: CustomTextView
    private lateinit var txtRendimientoEsp: CustomTextView
    private lateinit var txtUnidadMedida: CustomTextView
    private lateinit var txtPrediosColindantes: TextView
    private lateinit var txtComentario: TextView
    private lateinit var tvRegresar: TextView
    private lateinit var tvAvanzar: TextView

    override fun getLayout(): Int = R.layout.fragment_eval16_resumen3

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        repositorio = Repositorio(requireContext())
        id_visita = (requireActivity() as? EvaluacionActivity)?.id_visita ?: ""

        txtParcela = view.findViewById(R.id.txtParcela)
        txtUniCatastral = view.findViewById(R.id.txtUniCatastral)
        txtHas = view.findViewById(R.id.txtHas)
        txtHasSembrada = view.findViewById(R.id.txtHasSembrada)
        txtHasFinanciar = view.findViewById(R.id.txtHasFinanciar)
        txtRegConduccion = view.findViewById(R.id.txtRegConduccion)
        txtUbicExp = view.findViewById(R.id.txtUbicExp)
        txtEstadoCampo = view.findViewById(R.id.txtEstadoCampo)
        txtTipoRiego = view.findViewById(R.id.txtTipoRiego)
        txtDispoAgua = view.findViewById(R.id.txtDispoAgua)
        txtPending = view.findViewById(R.id.txtPending)
        txtTipoSuelo = view.findViewById(R.id.txtTipoSuelo)
        txtAccesibilidad = view.findViewById(R.id.txtAccesibilidad)
        txtAltitud = view.findViewById(R.id.txtAltitud)
        txtCultivo = view.findViewById(R.id.txtCultivo)
        txtVariedad = view.findViewById(R.id.txtVariedad)
        txtSiembraFecha = view.findViewById(R.id.txtSiembraFecha)
        txtAñoExp = view.findViewById(R.id.txtAñoExp)
        txtTipoSemilla = view.findViewById(R.id.txtTipoSemilla)
        txtRendimientoAnt = view.findViewById(R.id.txtRendimientoAnt)
        txtRendimientoEsp = view.findViewById(R.id.txtRendimientoEsp)
        txtUnidadMedida = view.findViewById(R.id.txtUnidadMedida)
        txtPrediosColindantes = view.findViewById(R.id.txtPrediosColindantes)
        txtComentario = view.findViewById(R.id.txtComentario)
        tvRegresar = view.findViewById(R.id.tvRegresar)
        tvAvanzar = view.findViewById(R.id.tvAvanzar)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {

    }

    private fun guardar(visita: Int? = null) {
        runBlocking {
            val resumen = if (repositorio.existeElementoResumen(id_visita)) {
                repositorio.selectEvalResumen(id_visita)
            } else {
                Resumen(id_visita)
            }
            resumen.fecha_pantalla_3 = Utils().obtenerFechaHoraActual(canvia.fonafeIII.agrobanco.util.Constants().ZONA_HORARIA)
            visita?.let { resumen.visita_completa = it }
            repositorio.addElementoResumen(resumen)

            data?.apply {
                latitud_final = coordenda.latitud
                longitud_final = coordenda.longitud
                altitud_final = coordenda.altitud
                precision_final = coordenda.precision
            }
            data?.let { repositorio.addElementoRegistroInformacion(it) }

            d_visita?.apply {
                finalizado = true
            }
            d_visita?.let { repositorio.addElementoVisita(it) }
        }
    }

    private var data: EvalRegistro? = null
    private var d_visita: Visita? = null

    private fun setupListeners() {
        tvRegresar.setOnClickListener {
            (context as EvaluacionActivity).retroceder_fragmento()
        }

        tvAvanzar.setOnClickListener{
            val dialog = AlertDialog.Builder(requireActivity())
                .setTitle("Confirmar")
                .setMessage("Está seguro que la información mostrada es la correcta?")
                .setCancelable(false)
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                .setPositiveButton("Sí",
                    DialogInterface.OnClickListener { dialog, id ->
                        finalizar()
                    }).create()
            dialog.show()
        }

        job = GlobalScope.launch {
            d_visita = repositorio.selectVisitaItem(id_visita)
            data = repositorio.selectEvalRegistroItem(id_visita)
            val data1 = repositorio.selectEvalCreditoAgricolaI(id_visita)
            val data2 = repositorio.selectEvalCreditoAgricolaII(id_visita)
            val data3 = repositorio.selectEvalCreditoAgricolaIII(id_visita)
            val predioColindante = repositorio.selectEvalPredioColindante(id_visita)
            val comentario = repositorio.selectEvalComentario(id_visita)
            withContext(Dispatchers.Main) {
                fillUI(data1, data2, data3, predioColindante, comentario)
            }
        }
    }

    private fun finalizar() {
        gpsLocation()
        guardar(1)
        (requireActivity() as EvaluacionActivity).finalizar()
    }

    private fun fillUI(
            credito1: CreditoAgricolaI,
            credito2: CreditoAgricolaII,
            credito3: CreditoAgricolaIII,
            prediColindate: PredioColindante,
            comentario: Comentario
    ) {
        var longitud = 0

        if(txtParcela.prefixText.length>longitud) longitud = txtParcela.prefixText.length
        if(txtUniCatastral.prefixText.length>longitud) longitud = txtUniCatastral.prefixText.length
        if(txtHas.prefixText.length>longitud) longitud = txtHas.prefixText.length
        if(txtHasSembrada.prefixText.length>longitud) longitud = txtHasSembrada.prefixText.length
        if(txtHasFinanciar.prefixText.length>longitud) longitud = txtHasFinanciar.prefixText.length
        if(txtRegConduccion.prefixText.length>longitud) longitud = txtRegConduccion.prefixText.length
        if(txtUbicExp.prefixText.length>longitud) longitud = txtUbicExp.prefixText.length
        if(txtEstadoCampo.prefixText.length>longitud) longitud = txtEstadoCampo.prefixText.length
        if(txtTipoRiego.prefixText.length>longitud) longitud = txtTipoRiego.prefixText.length
        if(txtDispoAgua.prefixText.length>longitud) longitud = txtDispoAgua.prefixText.length
        if(txtPending.prefixText.length>longitud) longitud = txtPending.prefixText.length
        if(txtTipoSuelo.prefixText.length>longitud) longitud = txtTipoSuelo.prefixText.length
        if(txtAccesibilidad.prefixText.length>longitud) longitud = txtAccesibilidad.prefixText.length
        if(txtAltitud.prefixText.length>longitud) longitud = txtAltitud.prefixText.length
        if(txtCultivo.prefixText.length>longitud) longitud = txtCultivo.prefixText.length
        if(txtVariedad.prefixText.length>longitud) longitud = txtVariedad.prefixText.length
        if(txtSiembraFecha.prefixText.length>longitud) longitud = txtSiembraFecha.prefixText.length
        if(txtAñoExp.prefixText.length>longitud) longitud = txtAñoExp.prefixText.length
        if(txtTipoSemilla.prefixText.length>longitud) longitud = txtTipoSemilla.prefixText.length
        if(txtRendimientoAnt.prefixText.length>longitud) longitud = txtRendimientoAnt.prefixText.length
        if(txtRendimientoEsp.prefixText.length>longitud) longitud = txtRendimientoEsp.prefixText.length
        if(txtUnidadMedida.prefixText.length>longitud) longitud = txtUnidadMedida.prefixText.length

        longitud = longitud +3

        Utils().aumentarEspacioBlanco(txtParcela,longitud)
        Utils().aumentarEspacioBlanco(txtUniCatastral,longitud)
        Utils().aumentarEspacioBlanco(txtHas,longitud)
        Utils().aumentarEspacioBlanco(txtHasSembrada,longitud)
        Utils().aumentarEspacioBlanco(txtHasFinanciar,longitud)
        Utils().aumentarEspacioBlanco(txtRegConduccion,longitud)
        Utils().aumentarEspacioBlanco(txtUbicExp,longitud)
        Utils().aumentarEspacioBlanco(txtEstadoCampo,longitud)
        Utils().aumentarEspacioBlanco(txtTipoRiego,longitud)
        Utils().aumentarEspacioBlanco(txtDispoAgua,longitud)
        Utils().aumentarEspacioBlanco(txtPending,longitud)
        Utils().aumentarEspacioBlanco(txtTipoSuelo,longitud)
        Utils().aumentarEspacioBlanco(txtAccesibilidad,longitud)
        Utils().aumentarEspacioBlanco(txtAltitud,longitud)
        Utils().aumentarEspacioBlanco(txtCultivo,longitud)
        Utils().aumentarEspacioBlanco(txtVariedad,longitud)
        Utils().aumentarEspacioBlanco(txtSiembraFecha,longitud)
        Utils().aumentarEspacioBlanco(txtAñoExp,longitud)
        Utils().aumentarEspacioBlanco(txtTipoSemilla,longitud)
        Utils().aumentarEspacioBlanco(txtRendimientoAnt,longitud)
        Utils().aumentarEspacioBlanco(txtRendimientoEsp,longitud)
        Utils().aumentarEspacioBlanco(txtUnidadMedida,longitud)

        txtParcela.mainText = credito1.nro_parcelas?.toString() ?: ""
        txtParcela.update(true)

        txtUniCatastral.mainText = credito1.unidad_catastral
        txtUniCatastral.update(false)

        txtHas.mainText = credito1.has_totales?.toString() ?: ""
        txtHas.update(true)

        txtHasSembrada.mainText = credito1.has_sembradas?.toString() ?: ""
        txtHasSembrada.update(true)

        txtHasFinanciar.mainText = credito1.has_financiar?.toString() ?: ""
        txtHasFinanciar.update(true)

        txtRegConduccion.mainText = credito1.regimen_tenencia?.let {
            resources.getStringArray(R.array.rg_regimenConduccion)[it]
        } ?: ""
        txtRegConduccion.update(false)

        txtUbicExp.mainText = credito1.ubicacion?.let {
            resources.getStringArray(R.array.rg_ubicacionRiesgo)[it]
        } ?: ""
        txtUbicExp.update(false)

        txtEstadoCampo.mainText = credito2.estado_campo?.let {
            resources.getStringArray(R.array.rg_estadoCampo)[it]
        } ?: ""
        txtEstadoCampo.update(false)

        txtTipoRiego.mainText = credito2.tipo_riego?.let {
            resources.getStringArray(R.array.rg_tipoRiego)[it]
        } ?: ""
        txtTipoRiego.update(false)

        txtDispoAgua.mainText = credito2.disponibilidad_agua?.let {
            resources.getStringArray(R.array.dispoAgua1)[it]
        } ?: ""
        txtDispoAgua.update(false)

        txtPending.mainText = credito2.pendiente?.let {
            resources.getStringArray(R.array.rg_pendiente)[it]
        } ?: ""
        txtPending.update(false)

        txtTipoSuelo.mainText = credito2.tipo_suelo?.let {
            resources.getStringArray(R.array.rg_tipoSuelo)[it]
        } ?: ""
        txtTipoSuelo.update(false)

        txtAccesibilidad.mainText = credito2.accesibilidad?.let {
            resources.getStringArray(R.array.rg_accesibilidadPredio)[it]
        } ?: ""
        txtAccesibilidad.update(false)

        txtAltitud.mainText = (credito2.altitud_aproximada?.toString() + " " + resources.getString(R.string.tv_eval7_alt)) ?: ""
        txtAltitud.update(true)

        val lista_cultivo = repositorio.selectListaCultivos()
        val index_cultivo = calcularIndex_cultivo(lista_cultivo,credito3.producto_cultivo!!)
        val cultivo = lista_cultivo.get(index_cultivo)

        txtCultivo.mainText = cultivo.cultivo?.toString() ?: ""
        txtCultivo.update(false)

        txtVariedad.mainText = credito3.variedad
        txtVariedad.update(false)

        txtSiembraFecha.mainText = credito3.fecha_siembra
        txtSiembraFecha.update(true)

        txtAñoExp.mainText = credito3.experiencia_anios?.toString() ?: ""
        txtAñoExp.update(true)

        txtTipoSemilla.mainText = credito3.tipo_semilla?.let {
            resources.getStringArray(R.array.rg_tipoSemilla)[it]
        } ?: ""
        txtTipoSemilla.update(false)

        txtRendimientoAnt.mainText = credito3.rendimiento_anterior?.toString() ?: ""
        txtRendimientoAnt.update(true)

        txtRendimientoEsp.mainText = credito3.rendimiento_esperado?.toString() ?: ""
        txtRendimientoEsp.update(true)

        val lista_peso = repositorio.selectListaUnidadPesos()
        val index_peso = calcularIndex_peso(lista_peso,credito3.rendimiento_unidad_medida!!)
        val peso = lista_peso.get(index_peso)

        txtUnidadMedida.mainText =  peso.unidad_peso?.toString() ?: ""
        txtUnidadMedida.update(false)

        txtPrediosColindantes.text = prediColindate.comentario_predios_colindantes

        txtComentario.text = comentario.comentario_recomendaciones
    }

    override fun onDetach() {
        super.onDetach()
        job?.cancel()
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    fun calcularIndex_cultivo(lista: List<Cultivo>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:Cultivo  in lista){
            if(item.cod_cultivo.equals(valor)){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }

    fun calcularIndex_peso(lista: List<UnidadPeso>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:UnidadPeso  in lista){
            if(item.cod_unidad_peso.equals(valor) ){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }
}
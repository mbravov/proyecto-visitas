package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants
import canvia.fonafeIII.agrobanco.view.util.CustomTextView
import canvia.fonafeIII.agrobanco.view.util.base.BaseFragment
import kotlinx.coroutines.*

/**
 * CUS030: Ver resumen - Pantalla 4
 */
class Eval17Resumen4Fragment : BaseFragment() {

    private lateinit var txtUniFinanciar: CustomTextView
    private lateinit var txtNumUniFinanciar: CustomTextView
    private lateinit var txtNumUniProductiva: CustomTextView
    private lateinit var txtNumUniTotal: CustomTextView
    private lateinit var txtTipoAlimentacion: CustomTextView
    private lateinit var txtManejo: CustomTextView
    private lateinit var txtFuenteAgua: CustomTextView
    private lateinit var txtDispoAgua: CustomTextView
    private lateinit var txtCrianza: CustomTextView
    private lateinit var txtRaza: CustomTextView
    private lateinit var txtExpAños: CustomTextView
    private lateinit var txtTipoTecnologia: CustomTextView
    private lateinit var txtManejoCrianza: CustomTextView
    private lateinit var txtPrediosColindantes: TextView
    private lateinit var txtComentario: TextView
    private lateinit var tvRegresar: TextView
    private lateinit var tvAvanzar: TextView

    override fun getLayout(): Int = R.layout.fragment_eval17_resumen4

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        repositorio = Repositorio(requireContext())
        id_visita = (requireActivity() as? EvaluacionActivity)?.id_visita ?: ""

        txtUniFinanciar = view.findViewById(R.id.txtUniFinanciar)
        txtNumUniFinanciar = view.findViewById(R.id.txtNumUniFinanciar)
        txtNumUniProductiva = view.findViewById(R.id.txtNumUniProductiva)
        txtNumUniTotal = view.findViewById(R.id.txtNumUniTotal)
        txtTipoAlimentacion = view.findViewById(R.id.txtTipoAlimentacion)
        txtManejo = view.findViewById(R.id.txtManejo)
        txtFuenteAgua = view.findViewById(R.id.txtFuenteAgua)
        txtDispoAgua = view.findViewById(R.id.txtDispoAgua)
        txtCrianza = view.findViewById(R.id.txtCrianza)
        txtRaza = view.findViewById(R.id.txtRaza)
        txtExpAños = view.findViewById(R.id.txtExpAños)
        txtTipoTecnologia = view.findViewById(R.id.txtTipoTecnologia)
        txtManejoCrianza = view.findViewById(R.id.txtManejoCrianza)
        txtPrediosColindantes = view.findViewById(R.id.txtPrediosColindantes)
        txtComentario = view.findViewById(R.id.txtComentario)
        tvRegresar = view.findViewById(R.id.tvRegresar)
        tvAvanzar = view.findViewById(R.id.tvAvanzar)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {

    }

    private fun guardar(visita: Int? = null) {
        runBlocking {
            val resumen = if (repositorio.existeElementoResumen(id_visita)) {
                repositorio.selectEvalResumen(id_visita)
            } else {
                Resumen(id_visita)
            }
            resumen.fecha_pantalla_4 = Utils().obtenerFechaHoraActual(canvia.fonafeIII.agrobanco.util.Constants().ZONA_HORARIA)
            visita?.let { resumen.visita_completa = it }
            repositorio.addElementoResumen(resumen)

            data?.apply {
                latitud_final = coordenda.latitud
                longitud_final = coordenda.longitud
                altitud_final = coordenda.altitud
                precision_final = coordenda.precision
            }
            data?.let { repositorio.addElementoRegistroInformacion(it) }

            d_visita?.apply {
                finalizado = true
            }
            d_visita?.let { repositorio.addElementoVisita(it) }
        }
    }

    private var data: EvalRegistro? = null
    private var d_visita: Visita? = null

    private fun setupListeners() {
        tvRegresar.setOnClickListener {
            (context as EvaluacionActivity).ir_fragmento(Constants().EVAL15_RESUMEN_2)
        }

        tvAvanzar.setOnClickListener{
            val dialog = AlertDialog.Builder(requireActivity())
                .setTitle("Confirmar")
                .setMessage("Está seguro que la información mostrada es la correcta?")
                .setCancelable(false)
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                .setPositiveButton("Sí",
                    DialogInterface.OnClickListener { dialog, id ->
                        finalizar()
                    }).create()
            dialog.show()
        }

        job = GlobalScope.launch {
            d_visita = repositorio.selectVisitaItem(id_visita)
            data = repositorio.selectEvalRegistroItem(id_visita)
            val data1 = repositorio.selectEvalCreditoPecuarioI(id_visita)
            val data2 = repositorio.selectEvalCreditoPecuarioII(id_visita)
            val predioColindante = repositorio.selectEvalPredioColindante(id_visita)
            val comentario = repositorio.selectEvalComentario(id_visita)
            withContext(Dispatchers.Main) {
                fillUI(data1, data2, predioColindante, comentario)
            }
        }
    }

    private fun finalizar() {
        gpsLocation()
        guardar(1)
        (requireActivity() as EvaluacionActivity).finalizar()
    }

    private fun fillUI(pecuario1: CreditoPecuarioI, pecuario2: CreditoPecuarioII, prediColindate: PredioColindante, comentario: Comentario) {
        var longitud = 0

        if(txtUniFinanciar.prefixText.length>longitud) longitud = txtUniFinanciar.prefixText.length
        if(txtNumUniFinanciar.prefixText.length>longitud) longitud = txtNumUniFinanciar.prefixText.length
        if(txtNumUniProductiva.prefixText.length>longitud) longitud = txtNumUniProductiva.prefixText.length
        if(txtNumUniTotal.prefixText.length>longitud) longitud = txtNumUniTotal.prefixText.length
        if(txtTipoAlimentacion.prefixText.length>longitud) longitud = txtTipoAlimentacion.prefixText.length
        if(txtManejo.prefixText.length>longitud) longitud = txtManejo.prefixText.length
        if(txtFuenteAgua.prefixText.length>longitud) longitud = txtFuenteAgua.prefixText.length
        if(txtDispoAgua.prefixText.length>longitud) longitud = txtDispoAgua.prefixText.length
        if(txtCrianza.prefixText.length>longitud) longitud = txtCrianza.prefixText.length
        if(txtRaza.prefixText.length>longitud) longitud = txtRaza.prefixText.length
        if(txtExpAños.prefixText.length>longitud) longitud = txtExpAños.prefixText.length
        if(txtTipoTecnologia.prefixText.length>longitud) longitud = txtTipoTecnologia.prefixText.length
        if(txtManejoCrianza.prefixText.length>longitud) longitud = txtManejoCrianza.prefixText.length

        longitud = longitud +3

        Utils().aumentarEspacioBlanco(txtUniFinanciar,longitud)
        Utils().aumentarEspacioBlanco(txtNumUniFinanciar,longitud)
        Utils().aumentarEspacioBlanco(txtNumUniProductiva,longitud)
        Utils().aumentarEspacioBlanco(txtNumUniTotal,longitud)
        Utils().aumentarEspacioBlanco(txtTipoAlimentacion,longitud)
        Utils().aumentarEspacioBlanco(txtManejo,longitud)
        Utils().aumentarEspacioBlanco(txtFuenteAgua,longitud)
        Utils().aumentarEspacioBlanco(txtDispoAgua,longitud)
        Utils().aumentarEspacioBlanco(txtCrianza,longitud)
        Utils().aumentarEspacioBlanco(txtRaza,longitud)
        Utils().aumentarEspacioBlanco(txtExpAños,longitud)
        Utils().aumentarEspacioBlanco(txtTipoTecnologia,longitud)
        Utils().aumentarEspacioBlanco(txtManejoCrianza,longitud)

        val lista_unidadFinanciar = repositorio.selectListaUnidadFinanciars()
        Log.e("fillUI","lista_unidadFinanciar.sixe: "+lista_unidadFinanciar.size)
        val index_unidadFinanciar = calcularIndex_unidadFinanciar(lista_unidadFinanciar,pecuario1.unidad_financiar!!)
        Log.e("fillUI","index_unidadFinanciar: "+index_unidadFinanciar)
        val unidadFinanciar = lista_unidadFinanciar.get(index_unidadFinanciar)

        txtUniFinanciar.mainText = unidadFinanciar.unidad_financiar?.toString() ?: ""
        txtUniFinanciar.update(false)

        txtNumUniFinanciar.mainText = pecuario1.numero_unidades_financiar?.toString() ?: ""
        txtNumUniFinanciar.update(true)

        txtNumUniProductiva.mainText = pecuario1.unidades_productivas?.toString() ?: ""
        txtNumUniProductiva.update(true)

        txtNumUniTotal.mainText = pecuario1.numero_unidades_totales?.toString() ?: ""
        txtNumUniTotal.update(true)

        txtTipoAlimentacion.mainText = pecuario1.tipo_alimentacion?.let {
            resources.getStringArray(R.array.rg_tipoAlimentacion)[it]
        } ?: ""
        txtTipoAlimentacion.update(false)

        txtManejo.mainText = pecuario1.manejo_actividad?.let {
            resources.getStringArray(R.array.rg_Manejo)[it]
        } ?: ""
        txtManejo.update(false)

        txtFuenteAgua.mainText = pecuario1.fuente_agua?.let {
            resources.getStringArray(R.array.rg_fuenteAgua)[it]
        } ?: ""
        txtFuenteAgua.update(false)

        txtDispoAgua.mainText = pecuario1.disponibilidad_agua?.let {
            resources.getStringArray(R.array.rg_DispoAgua2)[it]
        } ?: ""
        txtDispoAgua.update(false)

        val lista_crianza = repositorio.selectListaCrianzas()
        val index_crianza = calcularIndex_crianza(lista_crianza,pecuario2.crianza!!)
        val crianza = lista_crianza.get(index_crianza)

        txtCrianza.mainText =  crianza.crianza?.toString() ?: ""
        txtCrianza.update(false)

        txtRaza.mainText = pecuario2.raza
        txtRaza.update(false)

        txtExpAños.mainText = pecuario2.experiencia_anios?.toString() ?: ""
        txtExpAños.update(true)

        txtTipoTecnologia.mainText = pecuario2.tipo_tecnologia?.let {
            resources.getStringArray(R.array.rg_tipoTecnologia)[it]
        } ?: ""
        txtTipoTecnologia.update(false)

        txtManejoCrianza.mainText = pecuario2.manejo_crianza?.let {
            resources.getStringArray(R.array.rg_manejo2)[it]
        } ?: ""
        txtManejoCrianza.update(false)

        txtPrediosColindantes.text = prediColindate.comentario_predios_colindantes
        txtComentario.text = comentario.comentario_recomendaciones
    }

    override fun onDetach() {
        super.onDetach()
        job?.cancel()
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    fun calcularIndex_unidadFinanciar(lista: List<UnidadFinanciar>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:UnidadFinanciar  in lista){
            if(item.cod_unidad_financiar.equals(valor) ){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }

    fun calcularIndex_crianza(lista: List<Crianza>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:Crianza  in lista){
            if(item.cod_crianza.equals(valor)){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }
}
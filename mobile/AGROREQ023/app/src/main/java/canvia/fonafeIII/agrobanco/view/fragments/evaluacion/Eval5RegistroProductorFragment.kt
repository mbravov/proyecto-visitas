package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import com.google.android.material.textfield.TextInputLayout


class Eval5RegistroProductorFragment : Fragment() {
    var productor: Productor? = null
    var tvFechaReporte: TextView? = null
    var etObjetivo: EditText? = null
    var etVinculo: EditText? = null
    var etNombre: EditText? = null
    var tvVinculo: TextInputLayout? = null
    var tvNombre: TextInputLayout? = null


    var etCelular: EditText? = null
    var rgMercados: RadioGroup? = null
    var rbLocal: RadioButton? = null
    var rbRegional: RadioButton? = null
    var rbProvincial: RadioButton? = null
    var rbDistrital: RadioButton? = null
    var rgArea: RadioGroup? = null
    var rbSi: RadioButton? = null
    var rbNo: RadioButton? = null

    var spAgencia: Spinner?=null

    var rgPresente: RadioGroup? = null
    var rbProductor: RadioButton? = null
    var rbFamiliar: RadioButton? = null

    var cod_agencia_seleccionada: Int?=null

    var guardado = false
    var lista: MutableList<Agencia> = mutableListOf()
    lateinit var repo: Repositorio

    var tipo_actividad = 1

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =
            inflater.inflate(R.layout.fragment_eval5_registro_datos_proveedor, container, false)
        repo = Repositorio((context as EvaluacionActivity))


      //  spAgencia = rootView.findViewById(R.id.spAgencia)


        etObjetivo = rootView.findViewById<EditText>(R.id.etobjetivo)
        etVinculo = rootView.findViewById<EditText>(R.id.etvinculo)
        etNombre = rootView.findViewById<EditText>(R.id.etnombre)
        tvVinculo = rootView.findViewById<TextInputLayout>(R.id.tvVinculo)
        tvNombre = rootView.findViewById<TextInputLayout>(R.id.tvNombre)


        etCelular = rootView.findViewById<EditText>(R.id.etcelular)
        tvFechaReporte = rootView.findViewById<TextView>(R.id.tvFechaReporte)

        rgMercados = rootView.findViewById<RadioGroup>(R.id.rgMercados)
        rbLocal = rootView.findViewById<RadioButton>(R.id.rbLocal)
        rbRegional = rootView.findViewById<RadioButton>(R.id.rbLocal)
        rbProvincial = rootView.findViewById<RadioButton>(R.id.rbProvincial)
        rbDistrital = rootView.findViewById<RadioButton>(R.id.rbDistrital)

        rgArea = rootView.findViewById<RadioGroup>(R.id.rgArea)
        rbSi = rootView.findViewById<RadioButton>(R.id.rbSi)
        rbNo = rootView.findViewById<RadioButton>(R.id.rbNo)

        rgPresente = rootView.findViewById<RadioGroup>(R.id.rgPresente)
        rbProductor = rootView.findViewById<RadioButton>(R.id.rbProductor)
        rbFamiliar = rootView.findViewById<RadioButton>(R.id.rbFamiliar)


        rgPresente!!.setOnCheckedChangeListener(
                RadioGroup.OnCheckedChangeListener { group, checkedId ->


                    if (checkedId == R.id.rbProductor) {

                        tvVinculo!!.visibility = View.GONE
                        tvNombre!!.visibility = View.GONE

                    } else {

                        tvVinculo!!.visibility = View.VISIBLE
                        tvNombre!!.visibility = View.VISIBLE

                    }


                })



        tvFechaReporte!!.setText(Utils().obtenerFechaActual2(Constants().ZONA_HORARIA))
        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            //handler.removeCallbacks(runnable!!)
            //guardar()
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                if (tipo_actividad == 1) {
                    limpiarActividadPecuaria()
                    (context as EvaluacionActivity).avanzar_fragmento()
                } else {
                    limpiarActividadAgricola()
                    (context as EvaluacionActivity).ir_fragmento(canvia.fonafeIII.agrobanco.view.util.Constants().EVAL9_CREDITO_PECUARIO)
                }
            }
        })

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lista =repo.selectListaAgencias()

        if(lista==null || lista.size==0){
            insertarAgencias()
        }

        Log.e("selectListaAgencias()",""+lista.size)

        spAgencia= view.findViewById(R.id.spAgencia)

        val adapter: ArrayAdapter<Agencia> = ArrayAdapter<Agencia>(requireActivity(), android.R.layout.simple_list_item_1, lista.toTypedArray())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spAgencia!!.adapter = adapter


        spAgencia!!.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val age:Agencia= parent?.getItemAtPosition(position) as Agencia
                cod_agencia_seleccionada=age.cod_agencia
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
        inicio()
    }

    override fun onResume() {
        super.onResume()
        guardado = false
    }

    private fun inicio() {
        productor = Productor("")
        if (repo.existeElementoProductor((context as EvaluacionActivity)!!.id_visita!!)) {
            productor = repo.selectEvalProductor((context as EvaluacionActivity)!!.id_visita!!)

        }





        if (productor != null) {
            Log.e("Eval5RegistroProductor", "Existe")
           if (productor!!.agencia != null) {
                spAgencia!!.setSelection(calcularIndex(lista, productor!!.agencia!!))
              // spAgencia!!.setSelection(0)
           }

            if (productor!!.vinculo != null) {
                etVinculo!!.setText(productor!!.vinculo)
            }
            if (productor!!.nombre != null) {
                etNombre!!.setText(productor!!.nombre)
            }
            if (productor!!.numero_celular != null) {
                etCelular!!.setText(productor!!.numero_celular)
            }
            if (productor!!.objetivo_visita != null) {
                etObjetivo!!.setText(productor!!.objetivo_visita)
            }

            if (productor!!.id_tipo_mercado != null && productor!!.id_tipo_mercado!=-1) {
                (rgMercados!!.getChildAt(productor!!.id_tipo_mercado!!.toInt()) as RadioButton).isChecked =
                    true
            }

            if (productor!!.id_protegido != null  && productor!!.id_protegido!=-1) {
                (rgArea!!.getChildAt(productor!!.id_protegido!!.toInt()) as RadioButton).isChecked =
                    true
            }

            if (productor!!.id_tipo_presente != null && productor!!.id_tipo_presente!=-1) {
                (rgPresente!!.getChildAt(productor!!.id_tipo_presente!!.toInt()) as RadioButton).isChecked =
                    true
                if(productor!!.id_tipo_presente!!.toInt()==2){
                    tvVinculo!!.visibility = View.VISIBLE
                    tvNombre!!.visibility = View.VISIBLE
                } else {
                    tvVinculo!!.visibility = View.GONE
                    tvNombre!!.visibility = View.GONE
                    etVinculo!!.setText("")
                    etNombre!!.setText("")
                }
            }else if(productor!!.id_tipo_presente == null){
                tvVinculo!!.visibility = View.GONE
                tvNombre!!.visibility = View.GONE
                etVinculo!!.setText("")
                etNombre!!.setText("")
            }

        } else {
            Log.e("Eval5RegistroProductor", "No Existe")
            spAgencia!!.setSelection(0)
            (rgPresente!!.getChildAt(1) as RadioButton).isChecked = true
            etVinculo!!.setText("")
            etNombre!!.setText("")
            etCelular!!.setText("")
            etObjetivo!!.setText("")
            tvVinculo!!.visibility = View.GONE
            tvNombre!!.visibility = View.GONE
        }
        val codage:Int?= repo.obtenerAgenciaSeleccionada((context as EvaluacionActivity)!!.id_visita!!)
        if(codage==null){
            spAgencia?.isEnabled=true

        }else
        { spAgencia!!.setSelection(calcularIndex(lista, codage))
            spAgencia?.isEnabled=false

        }

    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {

            if (productor!!.id_tipo_presente == 1) {
                productor!!.vinculo=""
                productor!!.nombre=""

            }
            repo.addElementoProductor(productor!!)
        }

        return guardado

    }


    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if(productor!!.agencia==null){
            (spAgencia!!.selectedView as TextView).error = "Seleccione una Agencia"
            correcto = false
        }


        if (productor!!.objetivo_visita.equals("")) {
            etObjetivo!!.error = "Complete el Objetivo"
            correcto = false
        }
        if (productor!!.numero_celular!!.length > 0) {

            if (productor!!.numero_celular!!.length != 9) {
                etCelular!!.error = "El Nro de celular debe tener 9 digitos"
                correcto = false
            }


        }

        if (productor!!.id_tipo_mercado == -1 || productor!!.id_tipo_mercado == null) {

            rbDistrital!!.error = "Seleccione Tipo de Mercado"

            correcto = false
        } else {
            rbDistrital!!.error = null

        }

        if (productor!!.id_protegido == -1 || productor!!.id_protegido == null) {

            rbNo!!.error = "Seleccione si es área protegida"
            correcto = false
        } else {

            rbNo!!.error = null
        }
        if (productor!!.id_tipo_presente == -1 || productor!!.id_tipo_presente == null) {
            rbFamiliar!!.error = "Seleccione si es Familiar"

            correcto = false
        } else {

            rbFamiliar!!.error = null

            if (productor!!.id_tipo_presente == 2) {
                if (productor!!.vinculo.equals("")) {
                    etVinculo!!.error = "Complete el Vinculo"
                    correcto = false
                }

                if (productor!!.nombre.equals("")) {
                    etNombre!!.error = "Complete el Nombre"
                    correcto = false
                }
            } else {

                etVinculo!!.error = null
                etNombre!!.error = null

            }

        }

        return correcto
    }


    fun  llenarVariables() {


        productor!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        productor!!.numero_celular = etCelular!!.getText().toString().trim()
        productor!!.agencia = cod_agencia_seleccionada
        productor!!.vinculo = etVinculo!!.getText().toString().trim()
        productor!!.nombre = etNombre!!.getText().toString().trim()
        productor!!.objetivo_visita = etObjetivo!!.getText().toString().trim()

        productor!!.id_tipo_mercado =
            rgMercados!!.indexOfChild(rgMercados!!.findViewById(rgMercados!!.getCheckedRadioButtonId()))



        productor!!.id_protegido =
            rgArea!!.indexOfChild(rgArea!!.findViewById(rgArea!!.getCheckedRadioButtonId()))



        productor!!.id_tipo_presente =
            rgPresente!!.indexOfChild(rgPresente!!.findViewById(rgPresente!!.getCheckedRadioButtonId()))

        if (productor!!.id_tipo_presente!! == 1) {

            tvVinculo!!.visibility = View.GONE
            tvNombre!!.visibility = View.GONE

        } else {

            tvVinculo!!.visibility = View.VISIBLE
            tvNombre!!.visibility = View.VISIBLE

        }

        if (repo.existeElementoRegistroInformacion((context as EvaluacionActivity)!!.id_visita!! )) {
            val evalRegistro =  repo.selectEvalRegistroItem((context as EvaluacionActivity)!!.id_visita!!)
            if (evalRegistro != null) {
                if (evalRegistro!!.tipo_actividad != null && evalRegistro!!.tipo_actividad != -1)
                    tipo_actividad = evalRegistro!!.tipo_actividad!!.toInt()
            }
        }
    }

    fun calcularIndex(lista: List<Agencia>, valor: Int): Int{
        var index=0
        var resultado_index=0
        for(item_age:Agencia  in lista)
        {
            if(item_age.cod_agencia==valor){
                resultado_index=index
            }
            index++
        }

        return  resultado_index
    }

    fun limpiarActividadAgricola(){
        val creditoAgricolaI = CreditoAgricolaI("")
        creditoAgricolaI.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        repo.eliminarCreditoAgricolaI(creditoAgricolaI)

        val creditoAgricolaII = CreditoAgricolaII("")
        creditoAgricolaII.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        repo.eliminarCreditoAgricolaII(creditoAgricolaII)

        val creditoAgricolaIII = CreditoAgricolaIII("")
        creditoAgricolaIII.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        repo.eliminarCreditoAgricolaIII(creditoAgricolaIII)
    }

    fun limpiarActividadPecuaria(){
        val creditoPecuarioI = CreditoPecuarioI("")
        creditoPecuarioI.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        repo.eliminarCreditoPecuarioI(creditoPecuarioI)

        val creditoPecuarioII = CreditoPecuarioII("")
        creditoPecuarioII.id_visita = (context as EvaluacionActivity)!!.id_visita!!
        repo.eliminarCreditoPecuarioII(creditoPecuarioII)
    }

    fun insertarAgencias(){
        var posicion = 0
        lista = mutableListOf()

        for(item in resources.getStringArray(R.array.agencia)){
            val peso = Agencia(posicion,item)
            lista.add(peso)
            posicion++
        }

        repo.actualizarListaAgencias(lista)
    }
}
package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaI
import canvia.fonafeIII.agrobanco.model.pojos.Productor
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity

/**
 * CUS022: Registrar y editar datos de créditos agrícolas, datos del terreno 1 (Pantalla 1)
 */
class Eval6CreditoAgricolaIFragment : Fragment() {
    var creditoAgricolaI: CreditoAgricolaI? = null
    var etNroParcelas: EditText? = null
    var etHasSembradas: EditText? = null
    var etHasFinanciar: EditText? = null
    var etUnidadCatastral: EditText? = null
    var etHasTotales: EditText? = null
var rbMixto: RadioButton?=null
    var rgRegimen: RadioGroup? =null
    var rgUbicacion: RadioGroup? = null

   var rbSinExposicion: RadioButton?=null

    var guardado = false

    lateinit var repo: Repositorio
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =
            inflater.inflate(R.layout.fragment_eval6_creditos_agricolas_i, container, false)
        repo = Repositorio((context as EvaluacionActivity))
        rbSinExposicion=  rootView.findViewById<RadioButton>(R.id.rbSinExposicionI)
        rbMixto=  rootView.findViewById<RadioButton>(R.id.rbMixtoI)
        etNroParcelas = rootView.findViewById<EditText>(R.id.etNroParcelas)
        etHasSembradas = rootView.findViewById<EditText>(R.id.etHasSembradas)
        etHasFinanciar = rootView.findViewById<EditText>(R.id.etHasFinanciar)
        etHasTotales = rootView.findViewById<EditText>(R.id.etHasTotales)
        etUnidadCatastral = rootView.findViewById<EditText>(R.id.etUnidadCatastral)

        rgRegimen = rootView.findViewById<RadioGroup>(R.id.rgRegimenI)
        rgUbicacion = rootView.findViewById<RadioGroup>(R.id.rgUbicacionI)
        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            //handler.removeCallbacks(runnable!!)
            //guardar()
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {

            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }


        })
        inicio()
        return rootView
    }
    private fun inicio() {
        creditoAgricolaI = CreditoAgricolaI("")
        if (repo.existeElementoCreditoAgricolaI((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoAgricolaI = repo.selectEvalCreditoAgricolaI((context as EvaluacionActivity)!!.id_visita!!)

        }

        if (creditoAgricolaI != null) {

            if (creditoAgricolaI!!.has_financiar != null) {
                etHasFinanciar!!.setText(creditoAgricolaI!!.has_financiar.toString())
            }

            if (creditoAgricolaI!!.has_sembradas != null) {
                etHasSembradas!!.setText(creditoAgricolaI!!.has_sembradas.toString())
            }
            if (creditoAgricolaI!!.has_totales != null) {
                etHasTotales!!.setText(creditoAgricolaI!!.has_totales.toString())
            }

            if (creditoAgricolaI!!.nro_parcelas != null) {
                etNroParcelas!!.setText(creditoAgricolaI!!.nro_parcelas.toString())
            }
            if (creditoAgricolaI!!.unidad_catastral != null) {
                etUnidadCatastral!!.setText(creditoAgricolaI!!.unidad_catastral.toString())
            }





            if (creditoAgricolaI!!.regimen_tenencia != null && creditoAgricolaI!!.regimen_tenencia!=-1) {
                (rgRegimen!!.getChildAt(creditoAgricolaI!!.regimen_tenencia!!.toInt()) as RadioButton).isChecked =
                    true
            }

            if (creditoAgricolaI!!.ubicacion != null && creditoAgricolaI!!.ubicacion!=-1) {
                (rgUbicacion!!.getChildAt(creditoAgricolaI!!.ubicacion!!.toInt()) as RadioButton).isChecked =
                    true
            }



        } else {
            etHasFinanciar!!.setText("")
            etHasSembradas!!.setText("")
            etHasTotales!!.setText("")
            etNroParcelas!!.setText("")
            etUnidadCatastral!!.setText("")

        }


    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {


            repo.addElementoCreditoAgricolaI(creditoAgricolaI!!)


        }

        return guardado

    }




    fun llenarVariables() {


        creditoAgricolaI!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        if(etHasTotales!!.getText().trim().length>0  ) {
            creditoAgricolaI!!.has_totales = etHasTotales!!.text.toString().toDouble()
        }
        else{
            creditoAgricolaI!!.has_totales =null
        }

        if(etHasFinanciar!!.getText().trim().length>0  ) {
            creditoAgricolaI!!.has_financiar = etHasFinanciar!!.text.toString().toDouble()
        }
        else{
            creditoAgricolaI!!.has_financiar =null
        }

        if(etHasSembradas!!.getText().trim().length>0  ) {
            creditoAgricolaI!!.has_sembradas = etHasSembradas!!.text.toString().toDouble()
        }
        else{
            creditoAgricolaI!!.has_sembradas =null
        }

        if(etNroParcelas!!.getText().trim().length>0  ) {
            creditoAgricolaI!!.nro_parcelas = etNroParcelas!!.text.toString().toInt()
        }
        else{
            creditoAgricolaI!!.nro_parcelas =null
        }

        creditoAgricolaI!!.unidad_catastral= etUnidadCatastral!!.text.toString()

        creditoAgricolaI!!.regimen_tenencia =
            rgRegimen!!.indexOfChild(rgRegimen!!.findViewById(rgRegimen!!.getCheckedRadioButtonId()))

        creditoAgricolaI!!.ubicacion =
            rgUbicacion!!.indexOfChild(rgUbicacion!!.findViewById(rgUbicacion!!.getCheckedRadioButtonId()))



    }


    fun validar(): Boolean {
        var correcto = true

        llenarVariables()
        if (creditoAgricolaI!!.nro_parcelas==null) {
            etNroParcelas!!.error = "Ingresar Nro. Parcelas"
            correcto = false
        }
        if (creditoAgricolaI!!.has_sembradas==null) {
            etHasSembradas!!.error = "Ingresar Has. sembradas"
            correcto = false
        }
        if (creditoAgricolaI!!.unidad_catastral.equals("")) {
            etUnidadCatastral!!.error = "Ingresar unidad catastral"
            correcto = false
        }
        if (creditoAgricolaI!!.has_financiar==null) {
            etHasFinanciar!!.error = "Ingresar Has. a financiar"
            correcto = false
        }
        if (creditoAgricolaI!!.has_totales==null) {
            etHasTotales!!.error = "Ingresar Has. Totales"
            correcto = false
        }
        if(creditoAgricolaI!!.has_totales!=null && creditoAgricolaI!!.has_sembradas!=null) {
            if (creditoAgricolaI!!.has_totales!! < creditoAgricolaI!!.has_sembradas!!) {
                etHasTotales!!.error = "Has. Totales debe ser mayor o igual que Has. Sembradas"
                correcto = false
            }
        }
        if(creditoAgricolaI!!.has_totales!=null && creditoAgricolaI!!.has_financiar!=null) {
            if (creditoAgricolaI!!.has_totales!! < creditoAgricolaI!!.has_financiar!!) {
                etHasTotales!!.error = "Has. Totales debe ser mayor o igual que Has. a Financiar"
                correcto = false
            }
        }
        if (creditoAgricolaI!!.regimen_tenencia == -1 || creditoAgricolaI!!.regimen_tenencia == null) {

            rbMixto!!.error = "Seleccione un régimen"

            correcto = false
        } else {
            rbMixto!!.error = null

        }

        if (creditoAgricolaI!!.ubicacion == -1 || creditoAgricolaI!!.ubicacion == null) {

            rbSinExposicion!!.error = "Seleccione una Ubicación"

            correcto = false
        } else {
            rbSinExposicion!!.error = null

        }

        return correcto

    }












    }
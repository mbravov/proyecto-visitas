package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaI
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaII
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity


/**
 * CUS023: Registrar y editar datos de créditos agrícolas, datos del terreno 2 (Pantalla 2)
 */
class Eval7CreditoAgricola2Fragment : Fragment() {
    var rgEstadoCampo: RadioGroup?=null
    var rb_EC_Sembrado: RadioButton?=null
    var rgTipoRiego: RadioGroup?=null
    var rb_TR_Tecnificado: RadioButton?=null
    var rgDispAgua: RadioGroup?=null
    var rb_DA_Permanente: RadioButton?=null
    var rgPendiente: RadioGroup?=null
    var rb_P_Plana: RadioButton?=null
    var rgTipoSuelo: RadioGroup?=null
    var rb_TS_Arenoso: RadioButton?=null
    var rgAccesibilidadPredio: RadioGroup?=null
    var rb_AP_Facil: RadioButton?=null
    var etAltitudAprox: EditText? = null

    var creditoAgricolaII: CreditoAgricolaII? = null
    lateinit var repo: Repositorio
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_eval7_credito_agricola2, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        rgEstadoCampo=  rootView.findViewById<RadioGroup>(R.id.rgEstadoCampo)
        rb_EC_Sembrado=  rootView.findViewById<RadioButton>(R.id.rb_EC_Sembrado)
        rgTipoRiego=  rootView.findViewById<RadioGroup>(R.id.rgTipoRiego)
        rb_TR_Tecnificado=  rootView.findViewById<RadioButton>(R.id.rb_TR_Tecnificado)
        rgDispAgua=  rootView.findViewById<RadioGroup>(R.id.rgDispAgua)
        rb_DA_Permanente=  rootView.findViewById<RadioButton>(R.id.rb_DA_Permanente)
        rgPendiente=  rootView.findViewById<RadioGroup>(R.id.rgPendiente)
        rb_P_Plana=  rootView.findViewById<RadioButton>(R.id.rb_P_Plana)
        rgTipoSuelo=  rootView.findViewById<RadioGroup>(R.id.rgTipoSuelo)
        rb_TS_Arenoso=  rootView.findViewById<RadioButton>(R.id.rb_TS_Arenoso)
        rgAccesibilidadPredio=  rootView.findViewById<RadioGroup>(R.id.rgAccesibilidadPredio)
        rb_AP_Facil=  rootView.findViewById<RadioButton>(R.id.rb_AP_Facil)
        etAltitudAprox = rootView.findViewById<EditText>(R.id.etAltitudAprox)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }
        })

        inicio()

        return rootView
    }

    fun llenarVariables() {
        creditoAgricolaII!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        creditoAgricolaII!!.estado_campo =
            rgEstadoCampo!!.indexOfChild(rgEstadoCampo!!.findViewById(rgEstadoCampo!!.getCheckedRadioButtonId()))
        creditoAgricolaII!!.tipo_riego =
            rgTipoRiego!!.indexOfChild(rgTipoRiego!!.findViewById(rgTipoRiego!!.getCheckedRadioButtonId()))
        creditoAgricolaII!!.disponibilidad_agua =
            rgDispAgua!!.indexOfChild(rgDispAgua!!.findViewById(rgDispAgua!!.getCheckedRadioButtonId()))
        creditoAgricolaII!!.pendiente =
            rgPendiente!!.indexOfChild(rgPendiente!!.findViewById(rgPendiente!!.getCheckedRadioButtonId()))
        creditoAgricolaII!!.tipo_suelo =
            rgTipoSuelo!!.indexOfChild(rgTipoSuelo!!.findViewById(rgTipoSuelo!!.getCheckedRadioButtonId()))
        creditoAgricolaII!!.accesibilidad =
            rgAccesibilidadPredio!!.indexOfChild(rgAccesibilidadPredio!!.findViewById(rgAccesibilidadPredio!!.getCheckedRadioButtonId()))
        if(etAltitudAprox!!.getText().trim().length>0  ) {
            creditoAgricolaII!!.altitud_aproximada = etAltitudAprox!!.text.toString()
        }
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if (creditoAgricolaII!!.estado_campo == -1 || creditoAgricolaII!!.estado_campo == null) {
            rb_EC_Sembrado!!.error = "Seleccione un Estado de Campo"
            correcto = false
        } else {
            rb_EC_Sembrado!!.error = null
        }
        if (creditoAgricolaII!!.tipo_riego == -1 || creditoAgricolaII!!.tipo_riego == null) {
            rb_TR_Tecnificado!!.error = "Seleccione un Tipo de Riego"
            correcto = false
        } else {
            rb_TR_Tecnificado!!.error = null
        }
        if (creditoAgricolaII!!.disponibilidad_agua == -1 || creditoAgricolaII!!.disponibilidad_agua == null) {
            rb_DA_Permanente!!.error = "Seleccione un elemento de Disponibilidad de Agua"
            correcto = false
        } else {
            rb_DA_Permanente!!.error = null
        }
        if (creditoAgricolaII!!.pendiente == -1 || creditoAgricolaII!!.pendiente == null) {
            rb_P_Plana!!.error = "Seleccione un elemento de Pendiente"
            correcto = false
        } else {
            rb_P_Plana!!.error = null
        }
        if (creditoAgricolaII!!.tipo_suelo == -1 || creditoAgricolaII!!.tipo_suelo == null) {
            rb_TS_Arenoso!!.error = "Seleccione un Tipo de Suelo"
            correcto = false
        } else {
            rb_TS_Arenoso!!.error = null
        }
        if (creditoAgricolaII!!.accesibilidad == -1 || creditoAgricolaII!!.accesibilidad == null) {
            rb_AP_Facil!!.error = "Seleccione un elemento de Accesibildad al Predio"
            correcto = false
        } else {
            rb_AP_Facil!!.error = null
        }
        if (creditoAgricolaII!!.altitud_aproximada==null) {
            etAltitudAprox!!.error = "Ingresar Altitud aproximada"
            correcto = false
        }else if(creditoAgricolaII!!.altitud_aproximada!!.trim().toInt()<0){
            etAltitudAprox!!.error = "Altitud aproximada, no puede ser negativo"
            correcto = false
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoCreditoAgricolaII(creditoAgricolaII!!)
        }

        return guardado
    }

    private fun inicio() {
        creditoAgricolaII = CreditoAgricolaII("")
        if (repo.existeElementoCreditoAgricolaII((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoAgricolaII = repo.selectEvalCreditoAgricolaII((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (creditoAgricolaII != null) {
            if (creditoAgricolaII!!.estado_campo != null && creditoAgricolaII!!.estado_campo!=-1) {
                (rgEstadoCampo!!.getChildAt(creditoAgricolaII!!.estado_campo!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.tipo_riego != null && creditoAgricolaII!!.tipo_riego!=-1) {
                (rgTipoRiego!!.getChildAt(creditoAgricolaII!!.tipo_riego!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.disponibilidad_agua != null && creditoAgricolaII!!.disponibilidad_agua!=-1) {
                (rgDispAgua!!.getChildAt(creditoAgricolaII!!.disponibilidad_agua!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.pendiente != null && creditoAgricolaII!!.pendiente!=-1) {
                (rgPendiente!!.getChildAt(creditoAgricolaII!!.pendiente!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.tipo_suelo != null && creditoAgricolaII!!.tipo_suelo!=-1) {
                (rgTipoSuelo!!.getChildAt(creditoAgricolaII!!.tipo_suelo!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.accesibilidad != null && creditoAgricolaII!!.accesibilidad!=-1) {
                (rgAccesibilidadPredio!!.getChildAt(creditoAgricolaII!!.accesibilidad!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaII!!.altitud_aproximada != null) {
                etAltitudAprox!!.setText(creditoAgricolaII!!.altitud_aproximada.toString())
            }
        }
    }
}
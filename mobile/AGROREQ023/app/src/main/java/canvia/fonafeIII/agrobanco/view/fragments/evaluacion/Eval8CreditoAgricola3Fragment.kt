package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaIII
import canvia.fonafeIII.agrobanco.model.pojos.Cultivo
import canvia.fonafeIII.agrobanco.model.pojos.UnidadPeso
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants
import com.google.android.material.textfield.TextInputLayout
import java.text.SimpleDateFormat
import java.util.*


/**
 * CUS024: Registrar y editar datos de créditos agrícolas, datos del cultivo (Pantalla 3)
 */
class Eval8CreditoAgricola3Fragment : Fragment() {
    var spCultivo: Spinner?=null
    var etVariedad: EditText?=null
    var etFechaSiembra: EditText?=null
    var etExperiencia: EditText?=null
    var rgTipoSemilla: RadioGroup?=null
    var rb_TS_Certificada: RadioButton?=null
    var etCampAnterior: EditText?=null
    var etEsperado: EditText?=null
    var spPeso: Spinner?=null

    var cod_cultivo_seleccionado: String?=null
    var cod_peso_seleccionado: String?=null

    var creditoAgricolaIII: CreditoAgricolaIII? = null
    var lista_cultivo: MutableList<Cultivo> = mutableListOf()
    var lista_peso: MutableList<UnidadPeso> = mutableListOf()
    lateinit var repo: Repositorio
    var guardado = false

    var c = Calendar.getInstance()

    var mes: Int = c.get(Calendar.MONTH)
    var dia: Int = c.get(Calendar.DAY_OF_MONTH)
    var anio: Int = c.get(Calendar.YEAR)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_eval8_credito_agricola3, container, false)

        repo = Repositorio((context as EvaluacionActivity))


        etVariedad = rootView.findViewById(R.id.etVariedad)
        etFechaSiembra = rootView.findViewById(R.id.etFechaSiembra)
        etExperiencia = rootView.findViewById(R.id.etExperiencia)
        rgTipoSemilla = rootView.findViewById(R.id.rgTipoSemilla)
        rb_TS_Certificada = rootView.findViewById(R.id.rb_TS_Certificada)
        etCampAnterior = rootView.findViewById(R.id.etCampAnterior)
        etEsperado = rootView.findViewById(R.id.etEsperado)


        val ibCalendar = rootView.findViewById<ImageButton>(R.id.ibCalendar)

        ibCalendar.setOnClickListener(View.OnClickListener {
            mostrarCalendario()
        })

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).ir_fragmento(Constants().EVAL11_PREDIOS_COLINDANTES)
            }
        })

        etFechaSiembra!!.isEnabled = false

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lista_cultivo =repo.selectListaCultivos()
        lista_peso =repo.selectListaUnidadPesos()

        spCultivo = view.findViewById(R.id.spCultivo)
        spPeso = view.findViewById(R.id.spPeso)

        if(lista_cultivo==null || lista_cultivo.size==0){
            insertarCultivos()
        }

        if(lista_peso==null || lista_peso.size==0){
            insertarUnidadPesos()
        }

        val adapter_cultivo: ArrayAdapter<Cultivo> = ArrayAdapter<Cultivo>(requireActivity(), android.R.layout.simple_list_item_1, lista_cultivo.toTypedArray())
        adapter_cultivo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spCultivo!!.adapter = adapter_cultivo

        spCultivo!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val cult:Cultivo= parent?.getItemAtPosition(position) as Cultivo
                cod_cultivo_seleccionado=cult.cod_cultivo
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        val adapter_peso: ArrayAdapter<UnidadPeso> = ArrayAdapter<UnidadPeso>(requireActivity(), android.R.layout.simple_list_item_1, lista_peso.toTypedArray())
        adapter_peso.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spPeso!!.adapter = adapter_peso


        spPeso!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val pes:UnidadPeso= parent?.getItemAtPosition(position) as UnidadPeso
                cod_peso_seleccionado=pes.cod_unidad_peso
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        inicio()
    }

    fun mostrarCalendario() {
        val listener =
            OnDateSetListener { view, year, month, dayOfMonth ->
                val mesActual = month + 1
                val diaFormateado =
                    if (dayOfMonth < 10) Constants().CERO.toString() + dayOfMonth.toString() else dayOfMonth.toString()
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                val mesFormateado =
                    if (mesActual < 10) Constants().CERO.toString() + mesActual.toString() else mesActual.toString()
                //Muestro la fecha con el formato deseado
                etFechaSiembra!!.setText(diaFormateado + Constants().BARRA.toString() + mesFormateado + Constants().BARRA + year)

                /*
                val fechaactual = Date(System.currentTimeMillis())
                val fechaInicio = "$year-$mesFormateado-$diaFormateado"
                val date = SimpleDateFormat("yyyy-MM-dd")
                val fechaInicioDate = date.parse(fechaInicio)
                if(fechaInicioDate.after(fechaactual)){
                    tiFechaSiembra!!.getEditText()!!.setText("")
                    tiFechaSiembra!!.error = "Fecha ingresada es mayor a la fecha actual"
                }else
                    tiFechaSiembra!!.error = null
                 */
            }
        val picker = DatePickerDialog((context as EvaluacionActivity), 0, listener, anio, mes, dia)
        picker.show()
    }

    fun llenarVariables() {
        creditoAgricolaIII!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        creditoAgricolaIII!!.producto_cultivo = cod_cultivo_seleccionado
        if(etVariedad!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.variedad = etVariedad!!.text.toString().trim()
        }else creditoAgricolaIII!!.variedad = ""
        if(etFechaSiembra!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.fecha_siembra = etFechaSiembra!!.text.toString().trim()
        }else creditoAgricolaIII!!.fecha_siembra = ""
        if(etExperiencia!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.experiencia_anios = etExperiencia!!.text.toString().trim().toInt()
        }
        creditoAgricolaIII!!.tipo_semilla =
            rgTipoSemilla!!.indexOfChild(rgTipoSemilla!!.findViewById(rgTipoSemilla!!.getCheckedRadioButtonId()))
        if(etCampAnterior!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.rendimiento_anterior = etCampAnterior!!.text.toString().trim().toInt()
        }
        if(etEsperado!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.rendimiento_esperado = etEsperado!!.text.toString().trim().toInt()
        }
        creditoAgricolaIII!!.rendimiento_unidad_medida = cod_peso_seleccionado
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if(creditoAgricolaIII!!.producto_cultivo==null){
            (spCultivo!!.selectedView as TextView).error = "Seleccione un tipo de Cultivo"
            correcto = false
        }

        if (creditoAgricolaIII!!.variedad==null || creditoAgricolaIII!!.variedad!!.trim().equals("")) {
            etVariedad!!.error = "Ingresar la variedad del tipo de cultivo"
            correcto = false
        }

        if (creditoAgricolaIII!!.fecha_siembra==null || creditoAgricolaIII!!.fecha_siembra!!.trim().equals("")) {
            etFechaSiembra!!.error = "Ingresar la fecha de la siembra"
            correcto = false
        }else{
            etFechaSiembra!!.error = null
        }

        if (creditoAgricolaIII!!.experiencia_anios==null) {
            etExperiencia!!.error = "Ingresar Experiencia (años)"
            correcto = false
        }else if(creditoAgricolaIII!!.experiencia_anios!!<0){
            etExperiencia!!.error = "Experiencia (años), no puede ser negativo"
            correcto = false
        }

        if (creditoAgricolaIII!!.tipo_semilla == -1 || creditoAgricolaIII!!.tipo_semilla == null) {
            rb_TS_Certificada!!.error = "Seleccione un Tipo de Semilla"
            correcto = false
        } else {
            rb_TS_Certificada!!.error = null
        }

        if (creditoAgricolaIII!!.rendimiento_anterior==null) {
            etCampAnterior!!.error = "Ingresar Rendimiento campaña anterior"
            correcto = false
        }

        if (creditoAgricolaIII!!.rendimiento_esperado==null) {
            etEsperado!!.error = "Ingresar Rendimiento esperado"
            correcto = false
        }

        if(creditoAgricolaIII!!.rendimiento_unidad_medida==null){
            (spPeso!!.selectedView as TextView).error = "Seleccione un tipo de unidad de medida"
            correcto = false
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoCreditoAgricolaIII(creditoAgricolaIII!!)
        }

        return guardado
    }

    private fun inicio() {
        creditoAgricolaIII = CreditoAgricolaIII("")
        if (repo.existeElementoCreditoAgricolaIII((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoAgricolaIII = repo.selectEvalCreditoAgricolaIII((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (creditoAgricolaIII != null) {
            if (creditoAgricolaIII!!.producto_cultivo != null)
                spCultivo!!.setSelection(calcularIndex_cultivo(lista_cultivo, creditoAgricolaIII!!.producto_cultivo!!))
            if (creditoAgricolaIII!!.variedad != null) {
                etVariedad!!.setText(creditoAgricolaIII!!.variedad.toString())
            }
            if (creditoAgricolaIII!!.fecha_siembra != null) {
                etFechaSiembra!!.setText(creditoAgricolaIII!!.fecha_siembra.toString())
            }
            if (creditoAgricolaIII!!.experiencia_anios != null) {
                etExperiencia!!.setText(creditoAgricolaIII!!.experiencia_anios.toString())
            }
            if (creditoAgricolaIII!!.tipo_semilla != null && creditoAgricolaIII!!.tipo_semilla!=-1) {
                (rgTipoSemilla!!.getChildAt(creditoAgricolaIII!!.tipo_semilla!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaIII!!.rendimiento_anterior != null) {
                etCampAnterior!!.setText(creditoAgricolaIII!!.rendimiento_anterior.toString())
            }
            if (creditoAgricolaIII!!.rendimiento_esperado != null) {
                etEsperado!!.setText(creditoAgricolaIII!!.rendimiento_esperado.toString())
            }
            if (creditoAgricolaIII!!.rendimiento_unidad_medida != null)
                spPeso!!.setSelection(calcularIndex_peso(lista_peso, creditoAgricolaIII!!.rendimiento_unidad_medida!!))
        }
    }

    fun insertarCultivos(){
        var posicion = 0
        lista_cultivo = mutableListOf()

        for(item in resources.getStringArray(R.array.cultivo)){
            val cultivo = Cultivo(posicion.toString(),item)
            lista_cultivo.add(cultivo)
            posicion++
        }

        repo.actualizarListaCultivos(lista_cultivo)
    }

    fun insertarUnidadPesos(){
        var posicion = 0
        lista_peso = mutableListOf()

        for(item in resources.getStringArray(R.array.peso)){
            val peso = UnidadPeso(posicion.toString(),item)
            lista_peso.add(peso)
            posicion++
        }

        repo.actualizarListaUnidadPesos(lista_peso)
    }

    fun calcularIndex_cultivo(lista: List<Cultivo>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:Cultivo  in lista){
            if(item.cod_cultivo.equals(valor)){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }

    fun calcularIndex_peso(lista: List<UnidadPeso>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:UnidadPeso  in lista){
            if(item.cod_unidad_peso.equals(valor)){
                resultado_index=index
            }
            index++
        }
        return  resultado_index
    }
}
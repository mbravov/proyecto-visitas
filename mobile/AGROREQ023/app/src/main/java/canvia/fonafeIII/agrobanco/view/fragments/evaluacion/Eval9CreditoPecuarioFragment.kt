package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoPecuarioI
import canvia.fonafeIII.agrobanco.model.pojos.UnidadFinanciar
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants


/**
 * CUS025: Registrar y editar datos de créditos pecuarios paso 1: Datos de la Actividad
 */
class Eval9CreditoPecuarioFragment : Fragment() {
    var spUnidadFinanciar: Spinner?=null
    var etNroUnidadFinanciar: EditText?=null
    var etUnidadesTotales: EditText?=null
    var etUnidadesProductivas: EditText?=null
    var rgTipoAlimentacion: RadioGroup?=null
    var rb_TA_PastosNaturales: RadioButton?=null
    var rgManejo: RadioGroup?=null
    var rb_M_Extensiva: RadioButton?=null
    var rgFuenteAgua: RadioGroup?=null
    var rb_FA_CanalRiego: RadioButton?=null
    var rgDisponibilidadAgua: RadioGroup?=null
    var rb_DA_Permanente: RadioButton?=null

    var cod_unidad_seleccionado: String?=null
    var lista: MutableList<UnidadFinanciar> = mutableListOf()

    var creditoPecuarioI: CreditoPecuarioI? = null
    lateinit var repo: Repositorio
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval9_credito_pecuario, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        etNroUnidadFinanciar = rootView.findViewById(R.id.etNroUnidadFinanciar)
        etUnidadesTotales = rootView.findViewById(R.id.etUnidadesTotales)
        etUnidadesProductivas = rootView.findViewById(R.id.etUnidadesProductivas)
        rgTipoAlimentacion = rootView.findViewById(R.id.rgTipoAlimentacion)
        rb_TA_PastosNaturales = rootView.findViewById(R.id.rb_TA_PastosNaturales)
        rgManejo = rootView.findViewById(R.id.rgManejo)
        rb_M_Extensiva = rootView.findViewById(R.id.rb_M_Extensiva)
        rgFuenteAgua = rootView.findViewById(R.id.rgFuenteAgua)
        rb_FA_CanalRiego = rootView.findViewById(R.id.rb_FA_CanalRiego)
        rgDisponibilidadAgua = rootView.findViewById(R.id.rgDisponibilidadAgua)
        rb_DA_Permanente = rootView.findViewById(R.id.rb_DA_Permanente)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if(guardar())
            (context as EvaluacionActivity).ir_fragmento(Constants().EVAL5_CAP_REG_PROD)
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if(guardar())
            (context as EvaluacionActivity).avanzar_fragmento()
        })

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lista =repo.selectListaUnidadFinanciars()

        spUnidadFinanciar = view.findViewById(R.id.spUnidadFinanciar)

        if(lista==null || lista.size==0){
            insertarUnidadFinanciar()
        }

        val adapter: ArrayAdapter<UnidadFinanciar> = ArrayAdapter<UnidadFinanciar>(requireActivity(), android.R.layout.simple_list_item_1, lista.toTypedArray())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spUnidadFinanciar!!.adapter = adapter

        spUnidadFinanciar!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val unidad:UnidadFinanciar= parent?.getItemAtPosition(position) as UnidadFinanciar
                cod_unidad_seleccionado = unidad.cod_unidad_financiar
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        inicio()
    }

    fun llenarVariables() {
        creditoPecuarioI!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        creditoPecuarioI!!.unidad_financiar = cod_unidad_seleccionado
        if(etNroUnidadFinanciar!!.getText().trim().length>0  ) {
            creditoPecuarioI!!.numero_unidades_financiar = etNroUnidadFinanciar!!.text.toString().trim().toInt()
        }
        if(etUnidadesTotales!!.getText().trim().length>0  ) {
            creditoPecuarioI!!.numero_unidades_totales = etUnidadesTotales!!.text.toString().trim().toInt()
        }
        if(etUnidadesProductivas!!.getText().trim().length>0  ) {
            creditoPecuarioI!!.unidades_productivas = etUnidadesProductivas!!.text.toString().trim().toInt()
        }
        creditoPecuarioI!!.tipo_alimentacion =
            rgTipoAlimentacion!!.indexOfChild(rgTipoAlimentacion!!.findViewById(rgTipoAlimentacion!!.getCheckedRadioButtonId()))
        creditoPecuarioI!!.manejo_actividad =
            rgManejo!!.indexOfChild(rgManejo!!.findViewById(rgManejo!!.getCheckedRadioButtonId()))
        creditoPecuarioI!!.fuente_agua =
            rgFuenteAgua!!.indexOfChild(rgFuenteAgua!!.findViewById(rgFuenteAgua!!.getCheckedRadioButtonId()))
        creditoPecuarioI!!.disponibilidad_agua =
            rgDisponibilidadAgua!!.indexOfChild(rgDisponibilidadAgua!!.findViewById(rgDisponibilidadAgua!!.getCheckedRadioButtonId()))
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()


        if(creditoPecuarioI!!.unidad_financiar==null){
            (spUnidadFinanciar!!.selectedView as TextView).error = "Seleccione una unidad a financiar"
            correcto = false
        }
        if (creditoPecuarioI!!.numero_unidades_financiar==null) {
            etNroUnidadFinanciar!!.error = "Ingresar Número de unidades a financiar"
            correcto = false
        }else if(creditoPecuarioI!!.numero_unidades_financiar!!<0){
            etNroUnidadFinanciar!!.error = "Número de unidades a financiar, no puede ser negativo"
            correcto = false
        }
        if (creditoPecuarioI!!.numero_unidades_totales==null) {
            etUnidadesTotales!!.error = "Ingresar Número de unidades totales"
            correcto = false
        }else if(creditoPecuarioI!!.numero_unidades_totales!!<0){
            etUnidadesTotales!!.error = "Número de unidades totales, no puede ser negativo"
            correcto = false
        }
        if(creditoPecuarioI!!.numero_unidades_financiar!=null && creditoPecuarioI!!.numero_unidades_totales!=null) {
            if (creditoPecuarioI!!.numero_unidades_totales!! < creditoPecuarioI!!.numero_unidades_financiar!!) {
                etUnidadesTotales!!.error = "Número de unidades Totales debe ser mayor o igual que Número de unidades a Financiar"
                correcto = false
            }
        }
        if (creditoPecuarioI!!.unidades_productivas==null) {
            etUnidadesProductivas!!.error = "Ingresar Número de unidades totales"
            correcto = false
        }else if(creditoPecuarioI!!.unidades_productivas!!<0){
            etUnidadesProductivas!!.error = "Número de unidades totales, no puede ser negativo"
            correcto = false
        }
        if (creditoPecuarioI!!.tipo_alimentacion == -1 || creditoPecuarioI!!.tipo_alimentacion == null) {
            rb_TA_PastosNaturales!!.error = "Seleccione un Tipo de alimentación"
            correcto = false
        } else {
            rb_TA_PastosNaturales!!.error = null
        }
        if (creditoPecuarioI!!.manejo_actividad == -1 || creditoPecuarioI!!.manejo_actividad == null) {
            rb_M_Extensiva!!.error = "Seleccione un elemento de Manejo"
            correcto = false
        } else {
            rb_M_Extensiva!!.error = null
        }
        if (creditoPecuarioI!!.fuente_agua == -1 || creditoPecuarioI!!.fuente_agua == null) {
            rb_FA_CanalRiego!!.error = "Seleccione un elemento de Fuente de agua"
            correcto = false
        } else {
            rb_FA_CanalRiego!!.error = null
        }
        if (creditoPecuarioI!!.disponibilidad_agua == -1 || creditoPecuarioI!!.disponibilidad_agua == null) {
            rb_DA_Permanente!!.error = "Seleccione un elemento de Disponibilidad de agua"
            correcto = false
        } else {
            rb_DA_Permanente!!.error = null
        }
        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()

        if (guardado) {
            repo.addElementoCreditoPecuarioI(creditoPecuarioI!!)
        }

        return guardado
    }

    private fun inicio() {
        creditoPecuarioI = CreditoPecuarioI("")
        if (repo.existeElementoCreditoPecuarioI((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoPecuarioI = repo.selectEvalCreditoPecuarioI((context as EvaluacionActivity)!!.id_visita!!)
        }

        if(creditoPecuarioI!=null){
            if (creditoPecuarioI!!.unidad_financiar != null)
                spUnidadFinanciar!!.setSelection(calcularIndex(lista, creditoPecuarioI!!.unidad_financiar!!))
            if (creditoPecuarioI!!.numero_unidades_financiar != null) {
                etNroUnidadFinanciar!!.setText(creditoPecuarioI!!.numero_unidades_financiar.toString())
            }
            if (creditoPecuarioI!!.numero_unidades_totales != null) {
                etUnidadesTotales!!.setText(creditoPecuarioI!!.numero_unidades_totales.toString())
            }
            if (creditoPecuarioI!!.unidades_productivas != null) {
                etUnidadesProductivas!!.setText(creditoPecuarioI!!.unidades_productivas.toString())
            }
            if (creditoPecuarioI!!.tipo_alimentacion != null && creditoPecuarioI!!.tipo_alimentacion!=-1) {
                (rgTipoAlimentacion!!.getChildAt(creditoPecuarioI!!.tipo_alimentacion!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoPecuarioI!!.manejo_actividad != null && creditoPecuarioI!!.manejo_actividad!=-1) {
                (rgManejo!!.getChildAt(creditoPecuarioI!!.manejo_actividad!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoPecuarioI!!.fuente_agua != null && creditoPecuarioI!!.fuente_agua!=-1) {
                (rgFuenteAgua!!.getChildAt(creditoPecuarioI!!.fuente_agua!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoPecuarioI!!.disponibilidad_agua != null && creditoPecuarioI!!.disponibilidad_agua!=-1) {
                (rgDisponibilidadAgua!!.getChildAt(creditoPecuarioI!!.disponibilidad_agua!!.toInt()) as RadioButton).isChecked = true
            }
        }
    }

    fun insertarUnidadFinanciar(){
        var posicion = 0
        lista = mutableListOf()

        for(item in resources.getStringArray(R.array.unidadFinanciar)){
            val unidadFinanciar = UnidadFinanciar(posicion.toString(),item)
            lista.add(unidadFinanciar)
            posicion++
        }

        repo.actualizarListaUnidadFinanciars(lista)
    }

    fun calcularIndex(lista: List<UnidadFinanciar>, valor: String): Int{
        var index=0
        var resultado_index=0
        for(item:UnidadFinanciar  in lista){
            if(item.cod_unidad_financiar.equals(valor)){
                resultado_index=index
            }
            index++
        }

        return  resultado_index
    }
}
package canvia.fonafeIII.agrobanco.view.fragments.visita

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agroba.RepositorioRest
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.controller.AdminVisita
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.Usuario
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.model.webservice.viewmodal.VisitaViewModal
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Singleton
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.activities.VisitaActivity
import canvia.fonafeIII.agrobanco.view.adapter.PendientesEnvioAdapter
import canvia.fonafeIII.agrobanco.view.adapter.ProximasVisitasAdapter
import canvia.fonafeIII.agrobanco.view.contracts.FragmentListener
import canvia.fonafeIII.agrobanco.view.contracts.PendientesEnvioContract
import com.google.android.material.snackbar.Snackbar
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 * Use the [PendientesEnvioFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PendientesEnvioFragment : Fragment(), PendientesEnvioContract.View{
    val adminVisita = AdminVisita()
lateinit var repo:Repositorio


    var rvPredios: RecyclerView? = null
    var tvMensaje: TextView? = null
    var lnCarga: LinearLayout? = null
    var tvSincronizacion: TextView? = null

    var mAdapter: PendientesEnvioAdapter? = null
    var mListadoVisitas: List<Visita>? = null

    var mCallback: FragmentListener.View? = null
    var viewModel: VisitaViewModal? = null
    fun newInstance(): PendientesEnvioFragment? {
        val args = Bundle()
        val fragment: PendientesEnvioFragment =
            PendientesEnvioFragment()
        fragment.setArguments(args)
        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(VisitaViewModal::class.java)
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_pendientes_envio, container, false)
        repo= Repositorio((context as VisitaActivity))
        rvPredios = rootView.findViewById(R.id.rvPredios)
        tvMensaje = rootView.findViewById(R.id.tvMensaje)
        lnCarga = rootView.findViewById(R.id.lnCarga)
        tvSincronizacion = rootView.findViewById(R.id.tvSincronizacion)

        val itemDecoration = DividerItemDecoration(rvPredios!!.getContext(), DividerItemDecoration.VERTICAL)
        rvPredios!!.layoutManager = LinearLayoutManager(rvPredios!!.context)
        rvPredios!!.addItemDecoration(itemDecoration)
        setHasOptionsMenu(true)

        (context as VisitaActivity).getSupportActionBar()!!.setTitle("Pendientes de Envio");

        viewModel!!.sincronizaDataVisita()!!
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    visit ->
                if (visit?.respuesta_ok == 1) {


                    val iduser: Usuario? = (context?.applicationContext as AppConfiguracion).getUsuario()
                    //  mListadoVisitas = repo.selectVisitaListaPorUsuario(iduser?.id_usuario.toString())
                    mListadoVisitas = repo.selectVisitaListaPorUsuarioPendienteEnvio(Singleton.getUser())


                    if(mListadoVisitas!=null)
                        Log.e("onViewCreated","mListadoVisitas.s"+mListadoVisitas!!.size)
                    if(mListadoVisitas!!.size>0)
                        Log.e("onViewCreated","getNombres(): "+mListadoVisitas!!.get(0).getNombres())

                    mostrarListado(mListadoVisitas)

                    Utils().mostrarToast("Se sincronizo Visita", requireContext())
                    alternarProgress(false)
                } else {
                    alternarProgress(false)
                    Utils().mostrarToast(visit?.mensaje_resultado.toString(), requireContext())
                  //  mostrarPanel(false)
                    // panel!!.setVisibility(View.GONE);
                }

            })





        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       val iduser: Usuario? = (context?.applicationContext as AppConfiguracion).getUsuario()
      //  mListadoVisitas = repo.selectVisitaListaPorUsuario(iduser?.id_usuario.toString())
        mListadoVisitas = repo.selectVisitaListaPorUsuarioPendienteEnvio(Singleton.getUser())

        if(mListadoVisitas!=null)
            Log.e("onViewCreated","mListadoVisitas.s"+mListadoVisitas!!.size)
        if(mListadoVisitas!!.size>0)
            Log.e("onViewCreated","getNombres(): "+mListadoVisitas!!.get(0).getNombres())

        mostrarListado(mListadoVisitas)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        mCallback = if (context is FragmentListener.View) {
            context as FragmentListener.View
        } else {
            throw IllegalStateException("Eval1Registro Fragment - Evaluacion Activity no implementa FragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pendientes_envio, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_ayuda -> {
                val view: View =
                    LayoutInflater.from(context).inflate(R.layout.pendientes_envio_dialog, null)
                val dialog = Dialog(requireContext())
                dialog.setContentView(view)
                if (dialog.window != null) {
                    dialog.window!!.setLayout(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }
                dialog.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun mostrarListado(listadoVisitas: List<Visita?>?) {
        if (tvMensaje!!.visibility == View.VISIBLE) {
            tvMensaje!!.visibility = View.GONE
        }
        if (mAdapter == null) {
            mListadoVisitas = ArrayList<Visita>(listadoVisitas)!!
            mAdapter = PendientesEnvioAdapter(mListadoVisitas, this)
        } else {
            mListadoVisitas = ArrayList<Visita>()
            mListadoVisitas = ArrayList<Visita>(listadoVisitas)!!
            mAdapter = PendientesEnvioAdapter(mListadoVisitas, this)
        }
        rvPredios!!.adapter = mAdapter
    }

    override fun seleccionarVisita(visita: Visita?, posicion: Int) {
        if(visita!=null && visita.id_visita!=null){
            (context as VisitaActivity).irEvaluacionActivity(visita.id_visita!!,Constants().PENDIENTES_ENVIO)
        }
    }

    override fun sincronizarDatos(visita: Visita?, posicion: Int) {
        if (Utils().compruebaConexion(requireContext())) {
            alternarProgress(true)
            viewModel?.enviarDataVisita(Singleton.getUser(),
                visita?.id_visita.toString()
            )
        }else{

            Utils().mostrarToast("Sin conexion a internet",requireContext())
        }
    }

    override fun seleccionLargVisita(posicion: Int) {
    }

    fun alternarSeleccion(pos: Int) {
        mAdapter!!.toggleSelection(pos)
        val count = mAdapter!!.getSelectedItemCount()
        mCallback!!.actulizarActionMode(count)
    }

    fun limpiarSeleccion() {
        mAdapter!!.clearSelections()
    }

    override fun mostrarMensajeListadoVacio() {
        tvMensaje!!.visibility = View.VISIBLE
    }

    override fun mostrarError(texto: String?) {
        if (view == null) {
            Log.e(
                Constants().LOG_TAG,
                "PendientesEnvioFragment - mostrarError() - getView() null"
            )
            return
        }

        val snackbar = Snackbar.make(requireView(), texto!!, Snackbar.LENGTH_LONG)
        snackbar.setAction(
            "OK"
        ) { v: View? -> snackbar.dismiss() }
        snackbar.show()
    }

    override fun mostrarError(titulo: String?, texto: String?) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(texto)
            .setPositiveButton("OK", null)
        builder.show()
    }

    override fun mostrarMensajeFinal(texto: String?) {
        val builder = AlertDialog.Builder(context)
            .setTitle("Sincronización")
            .setMessage(texto)
            .setPositiveButton("OK", null)
            .setOnDismissListener { dialog: DialogInterface? ->
                mCallback!!.cambiarFragment(newInstance())
            }
        builder.show()
    }

    override fun redireccionarLogin() {

    }

    override fun alternarProgress(activado: Boolean) {
        lnCarga!!.visibility = if (activado) View.VISIBLE else View.GONE
    }

    override fun cambiarTextoSincronizacion(texto: String?) {
        tvSincronizacion!!.text = texto
    }

    override fun dirImages(): File? {
        return requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    }


    override fun obtenerContext(): Context? {
        return context
    }

    override fun onDestroyView() {

        super.onDestroyView()
    }
}
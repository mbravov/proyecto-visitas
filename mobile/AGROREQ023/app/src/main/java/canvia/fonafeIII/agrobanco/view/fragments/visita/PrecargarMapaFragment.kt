package canvia.fonafeIII.agrobanco.view.fragments.visita

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.activities.VisitaActivity
import canvia.fonafeIII.agrobanco.view.util.WeakLocationCallback
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [PrecargarMapaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PrecargarMapaFragment : Fragment(), OnMapReadyCallback {
    var latLng_gps: LatLng? = null
    var mMap: GoogleMap? = null
    var spMapaSC: Spinner? = null
    var etDireccion: EditText? = null
    var marker_buscar: Marker? = null
    var marker_pos: Marker? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationRequest: LocationRequest? = null
    private val TIEMPO: Long? = 2000

    var mapView: SupportMapFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_precargar_mapa, container, false)

        mapView = childFragmentManager.findFragmentById(R.id.fr_descMapa) as SupportMapFragment?

        mapView!!.getMapAsync(this)

        spMapaSC = rootView.findViewById<Spinner>(R.id.spMapaSC)

        ArrayAdapter.createFromResource(
            (context as VisitaActivity),
            R.array.mapas,
            R.layout.map_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.map_spinner_item)
            spMapaSC!!.adapter = adapter
        }

        spMapaSC!!.setSelection(0)

        spMapaSC!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                cambiarTipoMapa(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        etDireccion = rootView.findViewById<EditText>(R.id.etDireccion)

        etDireccion!!.setOnKeyListener(View.OnKeyListener { view, keyCode, keyEvent ->
            if (keyEvent.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                ocultarTeclado(etDireccion!!)
                view.requestFocus()
                return@OnKeyListener true
            }
            false
        })

        val button = rootView.findViewById<Button>(R.id.btnBuscar)
        button?.setOnClickListener()
        {
            if (!etDireccion!!.text.toString().equals("")) {
                buscarDireccion()
            }
        }

        val btnGPSfixed = rootView.findViewById<FloatingActionButton>(R.id.btnGPSfixed)
        btnGPSfixed?.setOnClickListener()
        {
            if(latLng_gps!=null) {
                gpsLocation()
                actualizarPosicion()
            }else latLng_gpsIsNULL()
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(
            Objects.requireNonNull(context as VisitaActivity)
        )

        if (ActivityCompat.checkSelfPermission(
                        (context as VisitaActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                    (context as VisitaActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
        }

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(2000) // 2 seconds interval
        mLocationRequest!!.setFastestInterval(2000)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)

        val client: SettingsClient = LocationServices.getSettingsClient(context as VisitaActivity)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->


        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        context as VisitaActivity,
                        //          this@VisitaActivity,
                        1
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest!!,
            mLocationCallback!!,
            Looper.getMainLooper()
        )


        (context as VisitaActivity).cambiarTitulo(getString(R.string.item_precargarmapa))


        return rootView
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        latLng_gpsIsNULL()

        if(latLng_gps!=null){
            gpsLocation()
            actualizarPosicion()
            spMapaSC!!.setSelection(2)
        }

    }

    private fun cambiarTipoMapa(tipo: Int) {

        when (tipo) {
            0 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_NORMAL) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
            }
            1 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_SATELLITE) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
            }
            2 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_HYBRID) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            }
        }

    }

    fun ocultarTeclado(view: View) {
        val mgr: InputMethodManager =
                (context as VisitaActivity).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        mgr.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun buscarDireccion() {
        var geo: Geocoder? = Geocoder(context as VisitaActivity)
        var maxResultados = 6
        var adress = mutableListOf<Address>()
        adress = geo!!.getFromLocationName(etDireccion!!.text.toString(), maxResultados)
        //LatLng latLng = new LatLng(adress.get(0).getLatitude(), adress.get(0).getLongitude());
        Log.e("buscarDireccion: ", "" + adress)
        if (adress != null && adress.size > 0) {
            val latLng = LatLng(adress.get(0).getLatitude(), adress.get(0).getLongitude())
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f))
            if (marker_buscar != null) {
                marker_buscar!!.remove()
            }
            marker_buscar = mMap!!.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(adress.get(0).getAddressLine(0))
            )
        } else {
            Toast.makeText(
                (context as VisitaActivity),
                "No se encontro la dirección!!!", Toast.LENGTH_LONG
            ).show()
        }
        ocultarTeclado(etDireccion!!)
    }

    fun actualizarPosicion() {
         gpsLocation()

        if (marker_pos != null) {
            marker_pos!!.remove()
        }
        marker_pos = mMap!!.addMarker(
            MarkerOptions()
                .position(latLng_gps!!)
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location))
        )
        // mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_gps, 17f))

        //  etDireccion!!.setText("")
    }

    private fun gpsLocation() {
        if (ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                (context as VisitaActivity), arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 1
            )
        }

        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(
                context as VisitaActivity
            ) { location: Location? ->
                if (location != null) {
                    val latLng = LatLng(location.latitude, location.longitude)
                    //                  if(latLng_gps!!.latitude!=latLng.latitude || latLng_gps!!.longitude!=latLng.longitude) {
                    latLng_gps = latLng
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_gps, 17f))
                    etDireccion!!.setText("")
                    if (marker_buscar != null) {
                        marker_buscar!!.remove()
                    }
                    //                 }
                }
            }
    }

    var hacerzoom = true
    private val mLocationCallback: LocationCallback = WeakLocationCallback(object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            locationResult ?: return


            latLng_gps =
                LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)

            actualizarPosicion()
            if (hacerzoom) {

                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_gps, 17f))
                hacerzoom = false
            }

        }
    })

    fun getLastKnownLocation(context: Context) {
        val locationManager: LocationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_DENIED || ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                (context as VisitaActivity),
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 1
            )
        }

        val providers: List<String> = locationManager.getProviders(true)

        var location: Location? = null
        for (i in providers.size - 1 downTo 0) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            location = locationManager.getLastKnownLocation(providers[i])
            if (location != null)
                break
        }
        if (location != null) {
            latLng_gps = LatLng(location.getLatitude(), location.getLongitude())
        }

        Log.e("getLastKnownLocation:", "" + latLng_gps)
    }

    override fun onResume() {


        super.onResume()
        if (ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (context as VisitaActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )


    }

    override fun onPause() {
        mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
        Log.d("liberalocation","pause")

        super.onPause()

    }


    fun latLng_gpsIsNULL(){
        var contador = 0

        if(latLng_gps==null) {
            while(contador<100 && latLng_gps==null){
                getLastKnownLocation(context as VisitaActivity)
                contador++
            }
        }
    }

}
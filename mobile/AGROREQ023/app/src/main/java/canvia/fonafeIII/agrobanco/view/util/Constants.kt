package canvia.fonafeIII.agrobanco.view.util

class Constants {
    /* Imagenes */
    val FORMATO_IMAGEN = ".jpg"
    val CAMERA_INTENT_CODE = 1888
    val FORMATO_FECHA_CAPTURAS = "yyyyMMdd_HHmmss"
    val EVAL4_CAP_FOTOS_COMP = 4
    val EVAL5_CAP_REG_PROD = 5
    val EVAL8_CREDITO_AGRICOLA3 = 8
    val EVAL9_CREDITO_PECUARIO = 9
    val EVAL11_PREDIOS_COLINDANTES = 11
    val EVAL15_RESUMEN_2 = 15
    val EVAL17_RESUMEN_4 = 17
    val EVAL1_INIT = 1

    val CERO = "0"
    val BARRA= "/"
    val FORMATO_FECHA_SLASH = "dd/MM/yyyy"
}
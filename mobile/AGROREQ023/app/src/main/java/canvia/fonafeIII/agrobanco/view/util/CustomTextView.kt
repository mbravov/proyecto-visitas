package canvia.fonafeIII.agrobanco.view.util

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView
import canvia.fonafeIII.agrobanco.R

class CustomTextView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    private val rightTextView: AppCompatTextView = AppCompatTextView(context)
    private val leftTextView: AppCompatTextView = AppCompatTextView(context)

    var prefixText: String = ""
    var mainText: String? = ""

    var prefixColorText = 1
    var mainColorText = 1

    init {

        val array = getContext().theme.obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0
        )

        prefixText = array.getString(R.styleable.CustomTextView_prefixText) ?: ""
        mainText = array.getString(R.styleable.CustomTextView_mainText) ?: ""
        prefixColorText = array.getColor(R.styleable.CustomTextView_prefixColor, Color.BLACK)
        mainColorText = array.getColor(R.styleable.CustomTextView_mainColor, Color.BLACK)

        array.recycle()
        setup()
    }

    private fun setup() {
        orientation = HORIZONTAL
        setPadding(
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt()
        )

        leftTextView.apply {
            text = prefixText
            setTextColor(prefixColorText)
            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
            typeface = Typeface.DEFAULT_BOLD
            setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.texto_small)
            )
        }

        rightTextView.apply {
            text = mainText
            setTextColor(mainColorText)
            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
            setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.texto_small)
            )
        }

        addView(leftTextView, LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT
        ))

        addView(rightTextView, LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT,
                1f
        ))
    }

    public fun update(derecha: Boolean) {
        orientation = HORIZONTAL
        setPadding(
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt(),
                resources.getDimension(R.dimen.padding_custom_text_view).toInt()
        )

        leftTextView.apply {
            text = prefixText
            setTextColor(prefixColorText)
            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
            typeface = Typeface.DEFAULT_BOLD
            setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.texto_small)
            )
        }

        if(derecha){
            rightTextView.apply {
                text = mainText
                setTextColor(mainColorText)
                textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                setTextSize(
                        TypedValue.COMPLEX_UNIT_PX,
                        resources.getDimension(R.dimen.texto_small)
                )
            }
        }else{
            rightTextView.apply {
                text = mainText
                setTextColor(mainColorText)
                textAlignment = View.TEXT_ALIGNMENT_VIEW_START
                setTextSize(
                        TypedValue.COMPLEX_UNIT_PX,
                        resources.getDimension(R.dimen.texto_small)
                )
            }
        }
    }

}
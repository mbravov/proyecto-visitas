package canvia.fonafeIII.agrobanco.view.util

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.text.HtmlCompat
import canvia.fonafeIII.agrobanco.BuildConfig
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

fun View.setShapeWithBorder(
        color: Int,
        corner: Float,
        colorBorder: Int = Color.TRANSPARENT,
        strokeWidth: Int = 0
) {

    val shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    shape.setColor(color)
    shape.setStroke(strokeWidth, colorBorder)
    shape.cornerRadius = corner
    this.background = shape

}

/**
 * @param cornerArray has the next structure:
 * [
 *  cornersTopLeftRadius, cornersTopLeftRadius,
 *  cornersTopRightRadius, cornersTopRightRadius,
 *  cornersBottomRightRadius, cornersBottomRightRadius,
 *  cornersBottomLeftRadius, cornersBottomLeftRadius
 * ]
 */

fun View.setShapeWithHalfBorder(
        color: Int,
        colorBorder: Int,
        cornerArray: FloatArray,
        strokeWidth: Int = 1
) {

    val shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    shape.setColor(color)
    shape.setStroke(strokeWidth, colorBorder)
    shape.cornerRadii = cornerArray
    this.background = shape

}

fun Bitmap.bitmapToBase64(): String {
    val byteArrayOutputStream = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
    val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(byteArray, Base64.DEFAULT)
}

fun String.base64toBitmap(): Bitmap? {
    val decodedString = Base64.decode(this, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
}

fun TextView.setHtmlText(string: String) {
    text = HtmlCompat.fromHtml(string, HtmlCompat.FROM_HTML_MODE_COMPACT)
}

fun AppCompatActivity.saveImage(image: Bitmap, name: String): String {
    //ByteArrayOutputStream().apply {
        //bitmap.compress(Bitmap.CompressFormat.PNG, 100, this)
        //fileName = UUID.nameUUIDFromBytes(toByteArray()).toString().replace("-", "")
    //}

    val imageFile = File("${getExternalFilesDir(Environment.DIRECTORY_PICTURES)}/$name.png")

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val contentValues = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, name)
            put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis())
            put(MediaStore.Images.Media.MIME_TYPE, "image/png")
            put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
        }

        return contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)?.let {uri ->
            contentResolver.openOutputStream(uri).use {
                Log.e("document path", uri.path ?: "")
                image.compress(Bitmap.CompressFormat.PNG, 100, it)
                it?.close()
                "${uri.encodedPath ?: ""}/$name"
            }
        } ?: ""
    } else {
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        if (!storageDir.exists()) storageDir.mkdir()
        Log.e("document path", storageDir?.path ?: "")
        val file = File(storageDir, "$name.png")

        return try {
            val fos = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.close()
            fos.close()
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //    //FileProvider.getUriForFile(this,
            //    //    BuildConfig.APPLICATION_ID+".FileProvider",
            //    //    file).path ?: ""
            //} else
            //    Uri.fromFile(file).path ?: ""
            file.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e("main", "error $e")
            ""
        }
    }
}

fun View.convertViewToBitmap(): Bitmap {
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    background?.let {
        it.draw(canvas)
    } ?: run {
        canvas.drawColor(Color.WHITE)
    }
    draw(canvas)
    return bitmap
}
package canvia.fonafeIII.agrobanco.view.util

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import kotlin.math.abs


class MyCanvasView(context: Context, attrs: AttributeSet): View(context, attrs) {

    companion object {
        const val TOUCH_TOLERANCE = 4f
    }

    private var mPaint: Paint
    private var mPath: Path
    var mDrawColor = 0
    var mBackgroundColor = 0
    private lateinit var mExtraCanvas: Canvas
    lateinit var mExtraBitmap: Bitmap
    var isDrawValid = false

    private var mX = 0f
    private var mY = 0f

    var canvasStrokeWidth = 8f

    init {
        mBackgroundColor = ResourcesCompat.getColor(resources, android.R.color.white, null)
        mDrawColor = ResourcesCompat.getColor(resources, android.R.color.black, null)
        // Holds the path we are currently drawing.
        mPath = Path()

        // Set up the paint with which to draw.
        mPaint = Paint().apply {
            color = mDrawColor
            // Smoothes out edges of what is drawn without affecting shape.
            isAntiAlias = true
            // Dithering affects how colors with higher-precision device
            // than the are down-sampled.
            isDither = true
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = canvasStrokeWidth
        }

    }

    fun setPreviousBitmap(bitmap: Bitmap) {
        if (::mExtraBitmap.isInitialized) {
            mExtraCanvas.drawBitmap(bitmap, Matrix(), null)
            isDrawValid = true
            invalidate()
        }
    }

    fun clearCanvas() {
        isDrawValid = false
        mExtraCanvas.drawColor(mBackgroundColor)
        mPath.reset()
        invalidate()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if  (event.action == MotionEvent.ACTION_DOWN) {
            parent?.requestDisallowInterceptTouchEvent(true)
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN->{
                touchStart(x, y)
            }
            MotionEvent.ACTION_MOVE->{
                touchMove(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP->{
                touchUp()
            }
            else->{}
        }

        return true
    }

    private fun touchStart(x: Float, y: Float) {
        mPath.moveTo(x, y)
        mX = x
        mY = y
    }

    private fun touchMove(x: Float, y: Float) {
        val dx = abs(x - mX)
        val dy = abs(y - mY)

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            // QuadTo() adds a quadratic bezier from the last point,
            // approaching control point (x1,y1), and ending at (x2,y2).
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
            mX = x
            mY = y
            // Save the path in the extra bitmap,
            // which we access through its canvas.
            mExtraCanvas.drawPath(mPath, mPaint)
            isDrawValid = true
        }
    }

    private fun touchUp() {
        mPath.reset()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mExtraBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        mExtraCanvas = Canvas(mExtraBitmap)

        mExtraCanvas.drawColor(mBackgroundColor)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawBitmap(mExtraBitmap, 0f, 0f, null)

    }

}

package canvia.fonafeIII.agrobanco.view.util

class TipoModulo {
    val MODULO_I = "M1_Prox_Visita"
    val MODULO_II = "M2_Eval_Inicial"
    val MODULO_III = "M3_Pend_Envio"
}
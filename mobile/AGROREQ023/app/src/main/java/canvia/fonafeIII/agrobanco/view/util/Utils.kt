package canvia.fonafeIII.agrobanco.view.util

import android.R.attr.bitmap
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class Utils {
    fun esFechaValida(fecha: String?): Boolean {
        val dateFormat =
            SimpleDateFormat(Constants().FORMATO_FECHA_SLASH, Locale.getDefault())
        dateFormat.isLenient = false
        try {
            val date = dateFormat.parse(fecha)
            println(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    companion object {
        private fun loadFile(file: File): ByteArray? {
            val fis = FileInputStream(file)

            if (file.length() > Integer.MAX_VALUE) {
                return null
            }

            val bytes = ByteArray(file.length().toInt())

            var offset = 0
            var numRead = 0
            while (offset < bytes.size
                    && fis.read(bytes, offset, bytes.size - offset).also { numRead = it } >= 0) {
                offset += numRead
            }

            if (offset < bytes.size) {
                throw IOException("Could not completely read file " + file.name)
            }

            fis.close()
            return bytes
        }

        @Throws(IOException::class)
        fun encodeFileToBase64Binary(file: File): String? {
            //val file = File(fileName)
            val bytes = loadFile(file)
            val encoded: ByteArray = Base64.encode(bytes, Base64.DEFAULT)
            return String(encoded)
        }
    }
}
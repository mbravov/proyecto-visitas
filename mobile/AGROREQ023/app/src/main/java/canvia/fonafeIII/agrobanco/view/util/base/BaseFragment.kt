package canvia.fonafeIII.agrobanco.view.util.base

import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.adapter.FotoResumenAdapter
import canvia.fonafeIII.agrobanco.view.util.WeakLocationCallback
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.Job

abstract class BaseFragment: Fragment() {

    var coordenda = Coordenada("","")
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationRequest: LocationRequest? = null

    private lateinit var mLocationCallback: LocationCallback
    var guardar_coordendas_iniciales = true

    protected lateinit var repositorio: Repositorio
    protected var id_visita = ""

    protected var job: Job? = null

    abstract fun getLayout(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(requireContext()).inflate(getLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermissions()

        repositorio = Repositorio(requireContext())
        id_visita = (requireActivity() as? EvaluacionActivity)?.id_visita ?: ""

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(4000) // 2 seconds interval
        mLocationRequest!!.setFastestInterval(2000)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        mLocationCallback = WeakLocationCallback(object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult ?: return
                coordenda.latitud = locationResult.lastLocation.latitude.toString()
                coordenda.longitud = locationResult.lastLocation.longitude.toString()
                coordenda.altitud = locationResult.lastLocation.altitude.toString()
                coordenda.precision = locationResult.lastLocation.accuracy.toString()

                Log.d("seguimiento","locationResult")
            }
        })


        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        val client: SettingsClient = LocationServices.getSettingsClient(context as EvaluacionActivity)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(
                        (context as EvaluacionActivity),
                        1
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions((requireContext() as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.FOREGROUND_SERVICE), 1)
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(
            requireActivity()
        )
    }

    protected fun gpsLocation() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((requireContext() as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }
        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(
                (requireContext() as EvaluacionActivity)
            ) { location: Location? ->
                if (location != null) {
                    if(!coordenda.precision.equals(location.accuracy.toString())) {
                        coordenda.latitud = location.latitude.toString()
                        coordenda.longitud = location.longitude.toString()
                        coordenda.altitud = location.altitude.toString()
                        coordenda.precision = location.accuracy.toString()
                    }
                } else {
                    Toast.makeText(context,
                        "No se pudo obtener su posición", Toast.LENGTH_LONG).show()
                }
            }
    }

    override fun onResume() {
        super.onResume()

        if (ActivityCompat.checkSelfPermission(
                (requireContext() as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (requireContext() as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {


            return
        }
        mFusedLocationClient?.requestLocationUpdates(mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper())
    }

}
package canvia.fonafeIII.agrobanco.view.util.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T>: RecyclerView.Adapter<BaseRecyclerAdapter.MyViewHolder>() {

    var itemClickListener: ItemClickListener<T>? = null
    var list: List<T> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    abstract fun getLayout(): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(getLayout(), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        onBindViewHold(position, holder.itemView)
        holder.itemView.setOnClickListener { onClick(holder.itemView, position, list[position]) }
    }

    abstract fun onBindViewHold(position: Int, itemView: View)

    abstract fun onClick(itemView: View, position: Int, data: T)

    override fun getItemCount(): Int = list.size

    inline fun setOnClickListener(crossinline body: (View, Int, T) -> Unit = { _, _, _ ->}) {
        itemClickListener = object: ItemClickListener<T> {
            override fun onItemClick(view: View, position: Int, data: T) {
                body(view, position, data)
            }
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}

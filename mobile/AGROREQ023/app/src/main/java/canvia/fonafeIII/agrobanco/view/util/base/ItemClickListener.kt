package canvia.fonafeIII.agrobanco.view.util.base

import android.view.View

interface ItemClickListener<T> {
    fun onItemClick(view: View, position: Int, data: T)
}

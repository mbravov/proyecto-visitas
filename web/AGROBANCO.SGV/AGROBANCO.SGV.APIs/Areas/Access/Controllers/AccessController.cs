﻿using AGROBANCO.SGV.APIs.Areas.Access.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AGROBANCO.SGV.APIs.Areas.Access.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/acceso")]
    [ApiController]
    public class AccessController : ControllerBase
    {
        private readonly IAccessControl _accesscontrol;

        public AccessController()
        {
            _accesscontrol = new AccessControl();
        }

        #region Access 

        /// <summary>
        /// Método para autenticar al usuario y generar token de sesión
        /// </summary>
        /// <param name="appkey">AppKey de aplicación</param>
        /// <param name="appcode">AppCode de aplicación</param>
        /// <param name="access">Objeto con correo, nombre y codigo del usuario</param>
        /// <response code="200">Ok - Retorna objeto con los datos del token</response> 
        /// <response code="400">Bad Request - Solicitud Errada o contenido incorrecto</response> 
        /// <response code="500">Server Error - Errores no controlados</response>  
        /// <response code="502">Bad Gateway - Servidor remoto sin conexión o no disponible</response>  
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(AuthAccessResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ProducesResponseType(502, Type = typeof(ResponseError))]
        [ValidateAppHeadersRequest]
        public object GenerarToken(
            [Required][FromHeader(Name = "X-AppKey")] string appkey,
            [Required][FromHeader(Name = "X-AppCode")] string appcode,
            [Required][FromBody] AuthAccessRequest access)
        {
            try
            {
                
                string token = "";

               

                var authAccessdto = ApiModelControlMapper.Mapper.Map<AccessDto>(access);
            

                AccessDto result = _accesscontrol.generateToken(authAccessdto,
                    appkey,
                    appcode,
                    ref token
                );

                var accessresponsedata = ApiModelControlMapper.Mapper.Map<AuthAccessResponseData>(result);

                var response = new AuthAccessResponse
                {
                    AuthAccess = accessresponsedata
                };

                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", "Bearer " + token);

                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion





    }
}
﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/asignacion")]
    [ApiController]
    public class AsignacionController : ControllerBase
    {
        private readonly IAsignacionControl _asignacionControl;
        // GET: HomeController

        public AsignacionController()
        {
            _asignacionControl = new AsignacionControl();
        }

        #region api

        /// <summary>
        /// Método para listar todos los clientes
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <response code="200">Ok - Lista de Empresas del proyecto según Requisitor</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("clientes")]
        [ProducesResponseType(200, Type = typeof(ClientResponse<List<ClienteDTO>>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public object Clientes([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                var clientes = _asignacionControl.obtenerClientes();

                var response = new ClientResponse<List<ClienteDTO>>
                {
                    Data = clientes,
                    Mensaje = "Listado de clientes",
                    Exito = true
                };

                return StatusCode(200, response);

            } catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Método para registrar asignar profesionales tecnicos con clientes
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <response code="200">Ok - True</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("asignacionProfesional")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public object AsignacionProfesional([Required][FromHeader(Name = "Token")] string token, [FromBody] AsignacionProfesionalRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<List<EProfesionalAsignacionDTO>>(request.Asignaciones);
                bool resultado = _asignacionControl.AsignarProfesionalClientes(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Método para registrar asignar profesional técnico con un cliente
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <response code="200">Ok - True</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("asignacionIndividual")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public object AsignacionIndividual([Required][FromHeader(Name = "Token")] string token, [FromBody] AsignacionProfesionalModel request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<EProfesionalAsignacionDTO>(request);
                bool resultado = _asignacionControl.AsignacionIndividual(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        /// <summary>
        /// Método para registrar asignar funcionario con visitas sin asignacion
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <response code="200">Ok - True</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("asignacionGrupal")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public object AsignacionGrupal([Required][FromHeader(Name = "Token")] string token, [FromBody] AsignacionProfesionalRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<List<EProfesionalAsignacionDTO>>(request.Asignaciones);
                bool resultado = _asignacionControl.AsignacionGrupal(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("sinasignar")]
        [ProducesResponseType(200, Type = typeof(List<EVisitaSinAsignacionDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]

        public object BuscarVisitaSinAsignar([Required][FromHeader(Name = "Token")] string token, [FromBody] VisitaSinAsignarRequest request)
        {
            try
            {

                var listaResultado = _asignacionControl.ObtenerVisitasSinAsignacion(request.NroDoc.ToString().Trim());

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
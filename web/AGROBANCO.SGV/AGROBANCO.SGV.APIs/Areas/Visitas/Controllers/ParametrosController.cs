﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/parametro")]
    [ApiController]
    public class ParametrosController : ControllerBase
    {
        private readonly IParametrosControl _parametrosControl;

        public ParametrosController()
        {
            _parametrosControl = new ParametrosControl();
        }


        [HttpPost("agencias")]
        [ProducesResponseType(200, Type = typeof(List<EDescargaAgenciaDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Agencias([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
            
                var listaResultado = _parametrosControl.DescargarAgencias();

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
              throw ex;
            }
        }


        [HttpPost("funcionarios")]
        [ProducesResponseType(200, Type = typeof(List<EDescargaFuncionarioDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Funcionarios([Required][FromHeader(Name = "Token")] string token, Parametro parametro)
        {
            try
            {

                var listaResultado = _parametrosControl.DescargarFuncionarios(parametro.CodAgencia);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("ObtenerParametros")]
        [ProducesResponseType(200, Type = typeof(List<ParametroDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ObtenerParametros([Required][FromHeader(Name = "Token")] string token,
                                        [FromBody] Parametro parametro)
        {
            try
            {

                var listaResultado = _parametrosControl.ObtenerParametros(parametro.CodGrupo);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("movil")]
        [ProducesResponseType(200, Type = typeof(EDescargaMovilDTO))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ParametrosMovil([Required][FromHeader(Name = "Token")] string token, [FromBody] MovilRequest movilRequest)
        {
            try
            {

                var listaResultado = _parametrosControl.DescargaParametrosMovil(movilRequest.Usuario.ToString(), movilRequest.Imei.ToString());

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("asociaciones")]
        [ProducesResponseType(200, Type = typeof(List<EAsociacionDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Asociaciones([Required][FromHeader(Name = "Token")] string token, [FromBody] Asociacion parametro)
        {
            try
            {
                var listaResultado = _parametrosControl.ObtenerAsociaciones(parametro.Codigo);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("integrantesAsociaciones")]
        [ProducesResponseType(200, Type = typeof(List<EAsociacionIntegranteDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object IntegrantesAsociaciones([Required][FromHeader(Name = "Token")] string token, [FromBody] Asociacion parametro)
        {
            try
            {
                var listaResultado = _parametrosControl.ObtenerAsociacionIntegrantes(parametro.Codigo);
                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("version")]
        [ProducesResponseType(200, Type = typeof(ValidarVersion))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
       
        public object ValidarVersion([FromBody] ValidarVersion valVersion)
        {
            try
            {
                ValidarVersion val = new ValidarVersion();


                int resultado = _parametrosControl.ObtenerValidacionVersion(valVersion.Version.Trim());
                val.resultado = resultado;
                val.Version = valVersion.Version.Trim();
                return StatusCode(200, val);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("listaIMEI")]
        [ProducesResponseType(200, Type = typeof(List<IMEIDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ListarIMEI(
        [Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                var lsfinalidaddto = _parametrosControl.ObtenerIMEI();

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("ResetearIMEI")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ResetearIMEI(
        [Required][FromHeader(Name = "Token")] string token, [FromBody] ValidarIMEI imei)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                int resultado = _parametrosControl.ResetearIMEI(imei.Usuario);
                response.Exito = resultado == 1?true:false;
                response.Mensaje = resultado.ToString();
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        [HttpPost("listarCultivos")]
        [ProducesResponseType(200, Type = typeof(List<CultivoDatoDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ListarCultivos(
        [Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                return StatusCode(200, _parametrosControl.ObtenerCultivos());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        [HttpPost("listarCreditos")]
        [ProducesResponseType(200, Type = typeof(List<string>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ListarCreditos(
        [Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                return StatusCode(200, _parametrosControl.ObtenerEstadosCreditos());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

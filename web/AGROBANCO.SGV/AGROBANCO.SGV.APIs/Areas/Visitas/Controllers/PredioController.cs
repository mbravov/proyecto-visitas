﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/predio")]
    [ApiController]
    public class PredioController : Controller
    {
        private readonly IPredioControl _prediocontrol;
 
        public PredioController()
        {
            _prediocontrol = new PredioControl();
        }

        [HttpPost("lista")]
        [ProducesResponseType(200, Type = typeof(PredioListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Lista(
               [Required][FromHeader(Name = "Token")] string token,
               [FromBody] PredioFiltrosRequest request)
        {
            try
            {
                var filtros = new PredioModel
                {
                    CodigoAgencia = Convert.ToInt32(request.codigoagencia),
                    CodigoFuncionario = request.codigofuncionario,
                    Cultivo = Convert.ToString(request.cultivo),
                    Campania = Convert.ToInt32(request.campania),
                    CodigoAsociacion = Convert.ToInt32(request.codigoAsociacion),
                    CodigoEstadoCredido = request.codigoEstadoCredido
                };

                var oprediodto = ApiModelControlMapper.Mapper.Map<PredioDTO>(filtros);
                var lsfinalidaddto = _prediocontrol.ObtenerListaPredio(oprediodto);

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/sincronizacion")]
    [ApiController]
    public class SincronizacionController : ControllerBase
    {
        private readonly ISincronizacionControl _sincronizacionControl;

        public SincronizacionController()
        {
            _sincronizacionControl = new SincronizacionControl();
        }

        [HttpPost("descarga")]
        [ProducesResponseType(200, Type = typeof(List<DescargaAsignacionDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Descarga([Required][FromHeader(Name = "Token")] string token, [FromBody] SincronizacionRequest request)
        {
            try
            {
                var filtroDTO = ApiModelControlMapper.Mapper.Map<DescargaAsignacionDTO>(request);

                var listaResultado = _sincronizacionControl.DescargarAsignaciones(filtroDTO);

                return StatusCode(200, listaResultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

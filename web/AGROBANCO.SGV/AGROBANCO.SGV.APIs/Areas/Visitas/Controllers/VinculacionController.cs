﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{

    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/vinculacion")]
    [ApiController]
    public class VinculacionController : ControllerBase
    {
        private readonly IVinculacionControl _vinculacionControl;

        public VinculacionController()
        {
            _vinculacionControl = new VinculacionControl();
        }

        [HttpPost("registrar")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Registrar([FromBody] VinculacionRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var Resultado = _vinculacionControl.RegistrarTokenVinculacion(request.IdToken, request.Aplicacion);
                response.Exito = Resultado;
                response.Mensaje = "OK";
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("obtener")]
        [ProducesResponseType(200, Type = typeof(ETokenVinculacionDTO))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Obtener([FromBody] VinculacionRequest request)
        {
            try
            {
            
                var Resultado = _vinculacionControl.ObtenerTokenVinculacion(request.IdToken);

                return StatusCode(200, Resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("actualizar")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Actualizar([FromBody] VinculacionRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var Resultado = _vinculacionControl.ActualizarTokenVinculacion(request.IdToken, request.Estado);
                response.Exito = Resultado;
                response.Mensaje = "OK";
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

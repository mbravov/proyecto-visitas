﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Comun.Laserfiche;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementacion;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/visita")]
    [ApiController]
    public class VisitaController : Controller
    {
        private readonly IVisitaControl _visitacontrol;
        private readonly ICargaVisitaControl _cargavisitacontrol;
        private readonly IVisorVisitaControl _visorVisitaControl;

        public VisitaController()
        {
            _visitacontrol = new VisitaControl();
            _cargavisitacontrol = new CargaVisitaControl();
            _visorVisitaControl = new VisorVisitaControl();
        }

        #region API

        /// <summary>
        /// Método para listar las visitas
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <param name="request">Filtros para obtener lista de visitas</param>
        /// <response code="200">Ok - Lista de Empresas del proyecto según Requisitor</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("lista")]
        [ProducesResponseType(200, Type = typeof(VisitaListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Lista(
                [Required][FromHeader(Name = "Token")] string token,
                [FromBody] VisitaFiltrosRequest request)
        {
            try
            {
                var filtros = new VisitaModel
                {
                    CodigoAgencia = Convert.ToInt32(request.codigoagencia),
                    CodigoFuncionario = Convert.ToString(request.codigofuncionario),
                    TipoVisita = Convert.ToInt32(request.tipovisita),
                    Estado = Convert.ToInt32(request.estadoinforme),
                    Vinculado = Convert.ToInt32(request.vinculado)
                };

                var ovisitadto = ApiModelControlMapper.Mapper.Map<VisitaDTO>(filtros);
                var lsfinalidaddto = _visitacontrol.ObtenerListaVisita(ovisitadto);

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("cargavisita")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object CarobtenerimagenesListagarVisita([Required][FromHeader(Name = "Token")] string token, [FromBody] CargaVisitaModel paramCargaVisitaModel)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<ECargaVisitaDTO>(paramCargaVisitaModel);

                bool resultado = _cargavisitacontrol.RegistrarCargaVisita(objDTO);
                response.Exito = resultado;
                if (resultado == false)
                {
                    response.Mensaje = "Falla en el servicio, intente denuevo";
                }
                else {
                    response.Mensaje = "OK";
                }

               

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        [HttpPost("visorvisita")]
        [ProducesResponseType(200, Type = typeof(EVisorVisitaDTO))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ObtenerVisorVisita([Required][FromHeader(Name = "Token")] string token,
                                        [FromBody] VisorVisitaRequest codvisita)
        {
            try
            {

                var resultado = _visorVisitaControl.ObtenerDatosVisita(codvisita.CodVisita);

                return StatusCode(200, resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("reasignar")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ReasignarVisita([Required][FromHeader(Name = "Token")] string token, [FromBody] VisitaUpdateRequest paramUpdateRequest)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                bool resultado = _visitacontrol.ReasignarVisitaPT(
                    paramUpdateRequest.codigovisita, 
                    paramUpdateRequest.codigoprofesional
                );

                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpPost("reasignar_masivo")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ReasignarMasivo([Required][FromHeader(Name = "Token")] string token, [FromBody] MasivoUpdateRequest paramUpdateRequest)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                bool resultado = _visitacontrol.ReasignarMasivo(
                    paramUpdateRequest.codigoanterior,
                    paramUpdateRequest.codigonuevo
                );

                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost("anulacion")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Anular([Required][FromHeader(Name = "Token")] string token, [FromBody] AnulacionRequest paramUpdateRequest)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                bool resultado = _visitacontrol.Anulacion(
                    paramUpdateRequest.codigoasignacion
                );

                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost("aprobacion")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Aprobacion([Required][FromHeader(Name = "Token")] string token, [FromBody] AprobacionRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();

                bool resultado = _visitacontrol.Aprobacion(
                    request.codigovisita,
                    request.estadovisita,
                    request.comentario
                );

                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost("listavincularvisita")]
        [ProducesResponseType(200, Type = typeof(VisitaListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ListaVincularVisita(
        [Required][FromHeader(Name = "Token")] string token, [FromBody] ListaVisitaRequest request)
        {
            try
            {
                var lsfinalidaddto = _visitacontrol.ObtenerListaVincularVisita(request.documento);

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

                


        #endregion


        [HttpPost("obtenerimagenesLista")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ObtenerImagenesLista([Required][FromHeader(Name = "Token")] string token, ImagenModel request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();

            var bytes = laserfiche.ConsultarDocumentoLista(request.coddocumentolf);
            return StatusCode(200, bytes);
        }

        [HttpPost("obtenerimagenesListaMapa")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ObtenerImagenesListaMapa([Required][FromHeader(Name = "Token")] string token, ImagenModel request)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();

            var bytes = laserfiche.ConsultarDocumentoLista(request.coddocumentolf);
            ServiceResponse<Documento> response = new ServiceResponse<Documento>();
            response.DataList = bytes;
            response.posicionLista = request.posicionLista;
            return StatusCode(200, response);
        }

        [HttpGet("obtenerimagenes")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public FileResult ObtenerImagenes(int coddocumentolf)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();

            var bytes = laserfiche.ConsultarDocumento(coddocumentolf);
            byte[] imageBytes = Convert.FromBase64String(bytes);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);
            return File(ms2.GetBuffer(), "image/jpeg");
        }

        [HttpPost("vincularvisitasolicitud")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object VincularVisitaSolicitudCredito([Required][FromHeader(Name = "Token")] string token,[FromBody] VisitaVinculacionCreditoRequest request)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var Resultado = _visitacontrol.AsignarSolicitudCredito(request.codigovisita, request.codigosolicitudcredito);
                response.Exito = Resultado;
                response.Mensaje = "OK";
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

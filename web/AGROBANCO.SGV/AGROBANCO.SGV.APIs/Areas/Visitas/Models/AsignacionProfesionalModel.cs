﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class AsignacionProfesionalModel
    {
        [JsonPropertyName("tipodocumento")]
        public int TipoDocumento { get; set; }

        [JsonPropertyName("numerodocumento")]
        public string NumDocumento { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public string CodigoFuncionario { get; set; }

        [JsonPropertyName("codigoprofesional")]
        public int CodigoProfesional { get; set; }

        [JsonPropertyName("estado")]
        public int Estado { get; set; }

        [JsonPropertyName("codigousuario")]
        public string CodigoUsuario { get; set; }

        [JsonPropertyName("primernombre")]
        public string? PrimerNombre { get; set; }

        [JsonPropertyName("segundonombre")]
        public string? SegundoNombre { get; set; }

        [JsonPropertyName("apellidopaterno")]
        public string? ApellidoPaterno { get; set; }

        [JsonPropertyName("apellidomaterno")]
        public string? ApellidoMaterno { get; set; }

        [JsonPropertyName("usuarioweb")]
        public string UsuarioWeb { get; set; }
    }

    public class AsignacionProfesionalRequest
    {
        [JsonPropertyName("lista")]
        public List<AsignacionProfesionalModel> Asignaciones { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class CargaVisitaModel
    {
        //[JsonPropertyName("codigo")]
        //public int Codigo { get; set; }

        //[JsonPropertyName("codigosolicitudcredito")]
        //public string CodigoSolicitudCredito { get; set; }

        [JsonPropertyName("numdocumentocliente")]
        public string NumDocumentoCliente { get; set; }

        //[JsonPropertyName("ultimofiltro")]
        //public int UltimoFiltro { get; set; }

        //[JsonPropertyName("fechaultimofiltro")]
        //public DateTime? FechaUltimoFiltro { get; set; }

        [JsonPropertyName("codigoagencia")]
        public int CodigoAgencia { get; set; }

        [JsonPropertyName("codigoasignacion")]
        public int CodigoAsignacion { get; set; }

        //[JsonPropertyName("fechaasignacion")]
        //public DateTime? FechaAsignacion { get; set; }

        //[JsonPropertyName("codigofuncionario")]
        //public int CodigoFuncionario { get; set; }

        //[JsonPropertyName("codigoprofesionaltecnico")]
        //public int CodigoProfesionalTecnico { get; set; }

        [JsonPropertyName("tipovisita")]
        public int TipoVisita { get; set; }

        //[JsonPropertyName("estadovisita")]
        //public int EstadoVisita { get; set; }

        //[JsonPropertyName("fechaaprobacion")]
        //public DateTime? FechaAprobacion { get; set; }

        //[JsonPropertyName("usuarioaprobacion")]
        //public string UsuarioAprobacion { get; set; }

        [JsonPropertyName("usuariocreacionvisita")]
        public string UsuarioCreacionVisita { get; set; }

        [JsonPropertyName("CodigoVisitaMobile")]
        public string CodigoVisitaMobile { get; set; }

        [JsonPropertyName("codigovisita")]
        public int CodigoVisita { get; set; }

        [JsonPropertyName("primernombrecliente")]
        public string PrimerNombrecliente { get; set; }

        [JsonPropertyName("segundonombrecliente")]
        public string SegundoNombrecliente { get; set; }

        [JsonPropertyName("apellidopaternocliente")]
        public string ApellidoPaternoCliente { get; set; }

        [JsonPropertyName("apellidomaternocliente")]
        public string ApellidoMaternoCliente { get; set; }

        [JsonPropertyName("nombrepredio")]
        public string NombrePredio { get; set; }

        [JsonPropertyName("direccionpredio")]
        public string DireccionPredio { get; set; }

        [JsonPropertyName("tipoactividad")]
        public int TipoActividad { get; set; }

        [JsonPropertyName("tipomapa")]
        public int TipoMapa { get; set; }

        [JsonPropertyName("modocaptura")]
        public int ModoCaptura { get; set; }

        [JsonPropertyName("numpuntosmapa")]
        public int NumPuntosMapa { get; set; }

        [JsonPropertyName("numfotosprincipales")]
        public int NumFotosPrincipales { get; set; }

        [JsonPropertyName("numfotossecundarias")]
        public int NumFotosSecundarias { get; set; }

        [JsonPropertyName("areatotalpuntos")]
        public decimal AreaTotalPuntos { get; set; }

        [JsonPropertyName("fechavisita")]
        public DateTime? FechaVisita { get; set; }

        [JsonPropertyName("codigoagenciaasignada")]
        public int CodigoAgenciaAsignada { get; set; }

        //[JsonPropertyName("codagencia")]
        //public int CodAgencia { get; set; }

        [JsonPropertyName("descobjetivovisita")]
        public string DescObjetivoVisita { get; set; }

        [JsonPropertyName("personapresente")]//[JsonPropertyName("productofamiliar")]
        public int PersonaPresente { get; set; }

        [JsonPropertyName("vinculofamiliar")]
        public string VinculoFamiliar { get; set; }

        [JsonPropertyName("nombrefamiliar")]
        public string NombreFamiliar { get; set; }

        [JsonPropertyName("numcelfamiliar")]
        public int NumCelFamiliar { get; set; }

        [JsonPropertyName("mercadoacceso")]
        public int MercadoAcceso { get; set; }

        //[JsonPropertyName("tipoacceso")]
        //public int TipoAcceso { get; set; }
        [JsonPropertyName("areaprotegida")]
        public int AreaProtegida { get; set; }

        //[JsonPropertyName("tiporespuesta")]
        //public int TipoRespuesta { get; set; }

        [JsonPropertyName("numparcelas")]
        public int NumParcelas { get; set; }

        [JsonPropertyName("numhectareassembradas")]
        public decimal NumHectareasSembradas { get; set; }

        [JsonPropertyName("unidadcatastral")]
        public string UnidadCatastral { get; set; }

        [JsonPropertyName("numhectareasfinanciar")]
        public decimal NumHectareasFinanciar { get; set; }

        [JsonPropertyName("numhectareastotales")]
        public decimal NumHectareasTotales { get; set; }

        [JsonPropertyName("regimentenencia")]
        public int RegimenTenencia { get; set; }

        [JsonPropertyName("tipoubicacion")]
        public int TipoUbicacion { get; set; }

        [JsonPropertyName("estadocampo")]
        public int EstadoCampo { get; set; }

        [JsonPropertyName("tiporiego")]
        public int TipoRiego { get; set; }

        [JsonPropertyName("disponibilidadagua")]
        public int DisponibilidadAgua { get; set; }

        [JsonPropertyName("pendiente")]
        public int Pendiente { get; set; }

        [JsonPropertyName("tiposuelo")]
        public int TipoSuelo { get; set; }

        [JsonPropertyName("accesibilidadpredio")]
        public int AccesibilidadPredio { get; set; }

        [JsonPropertyName("altitudpredio")]
        public decimal AltitudPredio { get; set; }

        [JsonPropertyName("cultivo")]
        public string Cultivo { get; set; }

        [JsonPropertyName("variedad")]
        public string Variedad { get; set; }

        [JsonPropertyName("fechasiembra")]
        public DateTime FechaSiembra { get; set; }

        [JsonPropertyName("tiposiembra")]
        public int TipoSiembra { get; set; }

        [JsonPropertyName("rendimientoanterior")]
        public decimal RendimientoAnterior { get; set; }

        [JsonPropertyName("rendimientoesperado")]
        public decimal RendimientoEsperado { get; set; }

        [JsonPropertyName("unidadfinanciar")]
        public string UnidadFinanciar { get; set; }

        [JsonPropertyName("numunidadesfinanciar")]
        public int NumUnidadesFinanciar { get; set; }

        [JsonPropertyName("numtotalunidades")]
        public int NumTotalUnidades { get; set; }

        [JsonPropertyName("unidadesproductivas")]
        public string UnidadesProductivas { get; set; }

        [JsonPropertyName("tipoalimentacion")]
        public int TipoAlimentacion { get; set; }

        [JsonPropertyName("tipomanejo")]
        public int TipoManejo { get; set; }

        [JsonPropertyName("tipofuenteagua")]
        public int TipoFuenteAgua { get; set; }

        [JsonPropertyName("disponibilidadaguapecuario")]
        public int DisponibilidadAguaPecuario { get; set; }

        [JsonPropertyName("tipocrianza")]
        public string TipoCrianza { get; set; }

        [JsonPropertyName("raza")]
        public string Raza { get; set; }

        [JsonPropertyName("aniosexp")]
        public int AniosExp { get; set; }

        [JsonPropertyName("tipotecnologia")]
        public int TipoTecnologia { get; set; }

        [JsonPropertyName("tipomanejopecuario")]
        public int TipoManejoPecuario { get; set; }

        [JsonPropertyName("comentpredioscolindantes")]
        public string ComentPrediosColindantes { get; set; }

        [JsonPropertyName("comentrecomendaciones")]
        public string ComentRecomendaciones { get; set; }

        [JsonPropertyName("firmatitular")]
        public string FirmaTitular { get; set; }

        [JsonPropertyName("firmaconyugue")]
        public string FirmaConyugue { get; set; }

        [JsonPropertyName("fechainiciovisita")]
        public DateTime FechaInicioVisita { get; set; }
        
        [JsonPropertyName("precisioniniciovisita")]
        public string PrecisionInicioVisita { get; set; }

        [JsonPropertyName("latiniciovisita")]
        public string LatInicioVisita { get; set; }

        [JsonPropertyName("longiniciovisita")]
        public string LongInicioVisita { get; set; }

        [JsonPropertyName("fechafinvisita")]
        public DateTime FechaFinVisita { get; set; }

        [JsonPropertyName("precisionfinvisita")]
        public string PrecisionFinVisita { get; set; }

        [JsonPropertyName("latfinvisita")]
        public string LatFinVisita { get; set; }

        [JsonPropertyName("longfinvisita")]
        public string LongFinVisita { get; set; }

        //[JsonPropertyName("coordenadas")]
        //public CoordenadasModel Coordenadas { get; set; }

        //[JsonPropertyName("imagenes")]
        //public ImagenesModel Imagenes { get; set; }

        [JsonPropertyName("listacoordenadas")]
        public List<CoordenadaModel> ListaCoordenadas { get; set; }

        [JsonPropertyName("listaimagenes")]
        public List<ImagenModel> ListaImagenes { get; set; }

        [JsonPropertyName("capturamapa")]
        public string CapturaMapa { get; set; }



        [JsonPropertyName("unidadfinanciarcod")]
        public string UnidadFinanciarCod { get; set; }

        [JsonPropertyName("aniosexppecuario")]
        public int AniosExpPecuario { get; set; }

        [JsonPropertyName("unidadpesocod")]
        public string UnidadPesoCod { get; set; }

    }

    public class CargaVisitaListResponse
    {
        [JsonPropertyName("cargavisitas")]
        public List<CargaVisitaModel> CargaVisitas { get; set; }
    }

    public class CargaVisitaFiltrosRequest
    {    
        [JsonPropertyName("pagina")]
        public int? pagina { get; set; }
    }

}

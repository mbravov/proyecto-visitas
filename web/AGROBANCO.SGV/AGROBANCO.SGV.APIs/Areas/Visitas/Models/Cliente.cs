﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class ClienteModel
    {
        [JsonPropertyName("codigo")]
        public int? Codigo { get; set; }

        [JsonPropertyName("tipodocumento")]
        public int? TipoDocumento { get; set; }

        [JsonPropertyName("numerodocumento")]
        public int? Numdocumento { get; set; }

        [JsonPropertyName("primernombre")]
        public string PrimerNombre { get; set; }

        [JsonPropertyName("segundonombre")]
        public string SegundoNombre { get; set; }

        [JsonPropertyName("apellidopaterno")]
        public string ApellidoPaterno { get; set; }

        [JsonPropertyName("apellidomaterno")]
        public string ApellidoMaterno { get; set; }

        [JsonPropertyName("nombrecompleto")]
        public string NombreCompleto { get; set; }

        [JsonPropertyName("codigoasosiacion")]
        public int? CodigoAsosiacion { get; set; }

        [JsonPropertyName("ultimofiltro")]
        public int? UltimoFiltro { get; set; }

        [JsonPropertyName("estado")]
        public int? Estado { get; set; }

        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }

        [JsonPropertyName("usuarioactualizacion")]
        public string UsuarioActualizacion { get; set; }
    }

    public class ClienteListResponse
    {
        [JsonPropertyName("clientes")]
        public List<ClienteModel> Clientes { get; set; }
    }

    public class ClienteFiltrosRequest
    {       
        [JsonPropertyName("pagina")]
        public int Pagina { get; set; }

        [JsonPropertyName("pagesize")]
        public int PageSize { get; set; }
    }


}

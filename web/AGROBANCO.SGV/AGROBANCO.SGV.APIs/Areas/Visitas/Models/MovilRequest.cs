﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class MovilRequest
    {

        [JsonPropertyName("imei")]
        public string Imei { get; set; }

        [JsonPropertyName("usuario")]
        public string Usuario { get; set; }
    }
}

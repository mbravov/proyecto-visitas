﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class Parametro
    {
        [JsonPropertyName("codgrupo")]
        public int CodGrupo { get; set; }

        [JsonPropertyName("codagencia")]
        public int CodAgencia { get; set; }
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class PredioModel
    {
        [JsonPropertyName("codigoagencia")]
        public int? CodigoAgencia { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public string CodigoFuncionario { get; set; }

        [JsonPropertyName("cultivo")]
        public string? Cultivo { get; set; }

        [JsonPropertyName("campania")]
        public int? Campania { get; set; }

        [JsonPropertyName("codigoAsociacion")]
        public int? CodigoAsociacion { get; set; }

        [JsonPropertyName("codigoEstadoCredido")]
        public string? CodigoEstadoCredido { get; set; }

        [JsonPropertyName("nombrePredio")]
        public string? NombrePredio { get; set; }

        [JsonPropertyName("direccionPredio")]
        public string? DireccionPredio { get; set; }

        [JsonPropertyName("primerNombre")]
        public string? PrimerNombre { get; set; }

        [JsonPropertyName("segundoNombre")]
        public string? SegundoNombre { get; set; }

        [JsonPropertyName("apellidoPaterno")]
        public string? ApellidoPaterno { get; set; }

        [JsonPropertyName("apellidoMaterno")]
        public string? ApellidoMaterno { get; set; }

        [JsonPropertyName("actividad")]
        public int? Actividad { get; set; }

        [JsonPropertyName("comentario")]
        public string? Comentario { get; set; }

        [JsonPropertyName("codigo")]
        public int? codigo { get; set; }
        [JsonPropertyName("longitud")]
        public string longitud { get; set; }
        [JsonPropertyName("latitud")]
        public string latitud { get; set; }
        [JsonPropertyName("fecha_visita")]
        public DateTime? fecha_visita { get; set; }
        [JsonPropertyName("imagen")]
        public List<EImagenesDatos> ListaImagenes { get; set; }
        public List<ECoordenadasDatos> ListaCoordenadas { get; set; }
    }
    public class PredioListResponse
    {
        [JsonPropertyName("predios")]
        public List<PredioModel> Predios { get; set; }
    }

    public class PredioFiltrosRequest
    {
        [JsonPropertyName("codigoagencia")]
        public int? codigoagencia { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public string codigofuncionario { get; set; }

        [JsonPropertyName("cultivo")]
        public string? cultivo { get; set; }

        [JsonPropertyName("campania")]
        public int? campania { get; set; }

        [JsonPropertyName("codigoAsociacion")]
        public int? codigoAsociacion { get; set; }

        [JsonPropertyName("codigoEstadoCredido")]
        public string? codigoEstadoCredido { get; set; }

        //[JsonPropertyName("TipoMapa")]
        //public string? TipoMapa { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class ServiceResponse
    {
        public bool Exito { get; set; }

        public string Mensaje { get; set; }
    }
}

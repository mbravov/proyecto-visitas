﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class SincronizacionRequest
    {
        [JsonPropertyName("webuser")]
        public string WebUser { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class ValidarIMEI
    {
        [JsonPropertyName("usuario")]
        public string Usuario { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class VinculacionRequest
    {
    
        [JsonPropertyName("idtoken")]
        public string IdToken { get; set; }

        [JsonPropertyName("estado")]
        public int Estado { get; set; }

        [JsonPropertyName("aplicacion")]
        public string Aplicacion { get; set; }

    }


}

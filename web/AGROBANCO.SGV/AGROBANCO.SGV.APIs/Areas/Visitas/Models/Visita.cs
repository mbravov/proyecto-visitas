﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class VisitaModel
    {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }

        [JsonPropertyName("codigosolicitudcredito")]
        public string CodigoSolicitudCredito { get; set; }

        [JsonPropertyName("numdocumentocliente")]
        public string NumDocumentoCliente { get; set; }

        [JsonPropertyName("fechaultimofiltro")]
        public DateTime? FechaUltimoFiltro { get; set; }

        [JsonPropertyName("fechaultimofiltroobtenido")]
        public DateTime? FechaUltimoFiltroObtenido { get; set; }

        [JsonPropertyName("codigoagencia")]
        public int CodigoAgencia { get; set; }

        [JsonPropertyName("codigoasignacion")]
        public int CodigoAsignacion { get; set; }

        [JsonPropertyName("fechaasignacion")]
        public DateTime? FechaAsignacion { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public string CodigoFuncionario { get; set; }

        [JsonPropertyName("nombrefuncionario")]
        public string NombreFuncionario { get; set; }

        [JsonPropertyName("codigoprofesionaltecnico")]
        public int CodigoProfesionalTecnico { get; set; }

        [JsonPropertyName("tipovisita")]
        public int TipoVisita { get; set; }

        [JsonPropertyName("estado")]
        public int? Estado { get; set; }

        [JsonPropertyName("vinculado")]
        public int? Vinculado { get; set; }

        [JsonPropertyName("fecharegistro")]
        public DateTime? FechaRegistro { get; set; }

        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }

        [JsonPropertyName("usuarioactualizacion")]
        public string UsuarioActualizacion { get; set; }

        [JsonPropertyName("asignacion")]
        public AsignacionModel EAsignacion { get; set; }
    }

    public class VisitaListResponse
    {
        [JsonPropertyName("visitas")]
        public List<VisitaModel> Visitas { get; set; }
    }

    public class VisitaFiltrosRequest
    {
        [JsonPropertyName("codigoagencia")]
        public int? codigoagencia { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public string? codigofuncionario { get; set; }

        [JsonPropertyName("tipovisita")]
        public int? tipovisita { get; set; }

        [JsonPropertyName("estadoinforme")]
        public int? estadoinforme { get; set; }

        [JsonPropertyName("pagina")]
        public int? pagina { get; set; }

        [JsonPropertyName("vinculado")]
        public int? vinculado { get; set; }
    }

    public class VisitaVinculacionCreditoRequest
    {
        [JsonPropertyName("codigovisita")]
        public int codigovisita { get; set; }

        [JsonPropertyName("solicitudcredito")]
        public string codigosolicitudcredito { get; set; }
    }

    public class VisitaUpdateRequest
    {
        [JsonPropertyName("codigovisita")]
        public int codigovisita { get; set; }

        [JsonPropertyName("codigoprofesional")]
        public int codigoprofesional { get; set; }
    }

    public class MasivoUpdateRequest
    {
        [JsonPropertyName("codigoanterior")]
        public int codigoanterior { get; set; }

        [JsonPropertyName("codigonuevo")]
        public int codigonuevo { get; set; }
    }

    public class AnulacionRequest
    {
        [JsonPropertyName("codigoasignacion")]
        public int codigoasignacion { get; set; }
    }

    public class AprobacionRequest
    {
        [JsonPropertyName("codigovisita")]
        public int codigovisita { get; set; }

        [JsonPropertyName("comentario")]
        public string comentario { get; set; }

        [JsonPropertyName("estadovisita")]
        public int estadovisita { get; set; }
    }
    public class DocumentoLaserficheRequest
    {
        [JsonPropertyName("coddocumentolf")]
        public int coddocumentolf { get; set; }
    }

    public class ListaVisitaRequest
    {
        [JsonPropertyName("documento")]
        public string documento { get; set; }
    }

}

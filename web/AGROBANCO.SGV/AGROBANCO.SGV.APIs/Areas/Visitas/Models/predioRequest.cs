﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class predioRequest
    {
        public int? CodigoAgencia { get; set; }
        public int? CodigoAsociacion { get; set; }
        public string? CodigoFuncionario { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string Cultivo { get; set; }
        public int? TipoFiltro { get; set; }
    }


}

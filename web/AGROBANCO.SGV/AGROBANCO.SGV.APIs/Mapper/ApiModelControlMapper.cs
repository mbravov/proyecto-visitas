﻿using AGROBANCO.SGV.APIs.Areas.Access.Models;
using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.Control.Dto;
using AutoMapper;
using System;

namespace AGROBANCO.SGV.APIs.Mapper
{
    public static class ApiModelControlMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;

    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Generate Toke

            CreateMap<AuthAccessRequest, AccessDto>();
            CreateMap<AccessDto, AuthAccessRequest>();

            CreateMap<AuthAccessResponseData, AccessDto>();
            CreateMap<AccessDto, AuthAccessResponseData>();

            #endregion

            #region Visitas

            CreateMap<ProfesionalTecnicoModel, ProfesionalTecnicoDTO>();
            CreateMap<ProfesionalTecnicoDTO, ProfesionalTecnicoModel>();
            CreateMap<SincronizacionRequest, DescargaAsignacionDTO>().ReverseMap();
            CreateMap<VinculacionRequest, ETokenVinculacionDTO>().ReverseMap();


            CreateMap<VisitaModel, VisitaDTO>();
            CreateMap<VisitaDTO, VisitaModel>();

            CreateMap<ClienteModel, ClienteDTO>();
            CreateMap<ClienteDTO, ClienteModel>();

            CreateMap<CargaVisitaModel, ECargaVisitaDTO>();
            CreateMap<ECargaVisitaDTO, CargaVisitaModel>();

            CreateMap<CoordenadaModel, ECoordenadasDTO>();
            CreateMap<ECoordenadasDTO, CoordenadaModel>();

            CreateMap<ImagenModel, EImagenesDTO>();
            CreateMap<EImagenesDTO, ImagenModel>();

            #endregion

            #region Asignaciones

            CreateMap<AsignacionProfesionalModel, EProfesionalAsignacionDTO>();
            CreateMap<EProfesionalAsignacionDTO, AsignacionProfesionalModel>();

            #endregion

            #region Predio
            CreateMap<PredioModel, PredioDTO>();
            CreateMap<PredioDTO, PredioModel>();
            #endregion
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class CultivoDatos
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class EAsignacionDatos
    {
        public int? Codigo { get; set; }

        public string NumDocumentoCliente { get; set; }

        public string? CodigoFuncionario { get; set; }
        public string? NombreFuncionario { get; set; }

        public int? CodigoProfesionalTecnico { get; set; }

        public int? Estao { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public EClienteDatos ECliente { get; set; }
        public EProfesionalTecnicoDatos EProfesionalTecnico { get; set; }
    }
}

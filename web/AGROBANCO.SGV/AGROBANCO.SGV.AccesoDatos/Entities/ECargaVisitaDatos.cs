﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class ECargaVisitaDatos
    {
        public int Codigo { get; set; }
        public string CodigoSolicitudCredito { get; set; }
        public string NumDocumentoCliente { get; set; }
        //public int UltimoFiltro { get; set; }
        //public DateTime? FechaUltimoFiltro { get; set; }
        public int CodigoAgencia { get; set; }
        public int CodigoAsignacion { get; set; }
        //public DateTime? FechaAsignacion { get; set; }
        //public int CodigoFuncionario { get; set; }
        //public int CodigoProfesionalTecnico { get; set; }
        public int TipoVisita { get; set; }
        public int EstadoVisita { get; set; }
        //public DateTime? FechaAprobacion { get; set; }
        //public string UsuarioAprobacion { get; set; }
        public string UsuarioCreacionVisita { get; set; }

        public string CodigoVisitaMobile { get; set; }
        public int CodVisita { get; set; }
        public string PrimerNombrecliente { get; set; }
        public string SegundoNombrecliente { get; set; }
        public string ApellidoPaternoCliente { get; set; }
        public string ApellidoMaternoCliente { get; set; }
        public string NombrePredio { get; set; }
        public string DireccionPredio { get; set; }
        public int TipoActividad { get; set; }
        public int TipoMapa { get; set; }
        public int ModoCaptura { get; set; }
        public int NumPuntosMapa { get; set; }
        public int NumFotosPrincipales { get; set; }
        public int NumFotosSecundarias { get; set; }
        public decimal AreaTotalPuntos { get; set; }
        public DateTime? FechaVisita { get; set; }
        public int CodigoAgenciaAsignada { get; set; }
        //public int CodAgencia { get; set; }
        public string DescObjetivoVisita { get; set; }
        public int PersonaPresente { get; set; }
        public string VinculoFamiliar { get; set; }
        public string NombreFamiliar { get; set; }
        public int NumCelFamiliar { get; set; }
        public int MercadoAcceso { get; set; }
        public int AreaProtegida { get; set; }
        public int NumParcelas { get; set; }
        public decimal NumHectareasSembradas { get; set; }
        public string UnidadCatastral { get; set; }
        public decimal NumHectareasFinanciar { get; set; }
        public decimal NumHectareasTotales { get; set; }
        public int RegimenTenencia { get; set; }
        public int TipoUbicacion { get; set; }
        public int EstadoCampo { get; set; }
        public int TipoRiego { get; set; }
        public int DisponibilidadAgua { get; set; }
        public int Pendiente { get; set; }
        public int TipoSuelo { get; set; }
        public int AccesibilidadPredio { get; set; }
        public decimal AltitudPredio { get; set; }
        public string Cultivo { get; set; }
        public string Variedad { get; set; }
        public DateTime FechaSiembra { get; set; }
        public int TipoSiembra { get; set; }
        public decimal RendimientoAnterior { get; set; }
        public decimal RendimientoEsperado { get; set; }
        public string UnidadFinanciar { get; set; }
        public int NumUnidadesFinanciar { get; set; }
        public int NumTotalUnidades { get; set; }
        public string UnidadesProductivas { get; set; }
        public int TipoAlimentacion { get; set; }
        public int TipoManejo { get; set; }
        public int TipoFuenteAgua { get; set; }
        public int DisponibilidadAguaPecuario { get; set; }
        public string TipoCrianza { get; set; }
        public string Raza { get; set; }
        public int AniosExp { get; set; }
        public int TipoTecnologia { get; set; }
        public int TipoManejoPecuario { get; set; }
        public string ComentPrediosColindantes { get; set; }
        public string ComentRecomendaciones { get; set; }
        public string FirmaTitular { get; set; }
        public string FirmaConyugue { get; set; }
        public DateTime FechaInicioVisita { get; set; }
        public string PrecisionInicioVisita { get; set; }
        public string LatInicioVisita { get; set; }
        public string LongInicioVisita { get; set; }
        public DateTime FechaFinVisita { get; set; }
        public string PrecisionFinVisita { get; set; }
        public string LatFinVisita { get; set; }
        public string LongFinVisita { get; set; }
        public List<ECoordenadasDatos> ListaCoordenadas { get; set; }
        public List<EImagenesDatos> ListaImagenes { get; set; }
        public string CapturaMapa { get; set; }

        public string UnidadFinanciarCod { get; set; }
        public int AniosExpPecuario { get; set; }
        public string UnidadPesoCod { get; set; }



    }
}

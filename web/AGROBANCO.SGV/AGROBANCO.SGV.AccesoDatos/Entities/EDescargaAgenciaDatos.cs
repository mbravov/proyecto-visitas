﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class EDescargaAgenciaDatos
    {
        public int Codigo { get; set; }
        public string Oficina { get; set; }
        public string OficinaRegional { get; set; }
    }
}

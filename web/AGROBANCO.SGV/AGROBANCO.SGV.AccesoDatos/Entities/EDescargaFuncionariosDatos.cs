﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class EDescargaFuncionariosDatos
    {
        public string Usuario { get; set; }
        public string Nombre { get; set; }
    }
}

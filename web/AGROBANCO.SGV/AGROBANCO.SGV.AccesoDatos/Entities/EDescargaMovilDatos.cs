﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
     public class EDescargaMovilDatos
    {
        public int DiasEliminar { get; set; }
        public int Precision { get; set; }
        public decimal Hectareas { get; set; }
        public int Dispositivo { get; set; }

        public List<EDescargaAgenciaDatos> ListaAgencias { get; set; }
        public List<DescargaCultivoDatos> ListaCultivo { get; set; }
        public List<DescargaMedidaDatos> ListaMedida { get; set; }
        public List<DescargaCrianzaDatos> ListaCrianza { get; set; }
        public List<DescargaUnidadFinanciarDatos> ListaUnidadFinanciar { get; set; }

    }

    public class DescargaCultivoDatos {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }

    }
    public class DescargaMedidaDatos {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaCrianzaDatos {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaUnidadFinanciarDatos {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }



}

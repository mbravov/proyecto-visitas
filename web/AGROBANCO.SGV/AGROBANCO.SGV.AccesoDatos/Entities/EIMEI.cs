﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{

    public class EIMEI
    {
        public string IdentificadorMovil { get; set; }
        public string Usuario { get; set; }
    }
}

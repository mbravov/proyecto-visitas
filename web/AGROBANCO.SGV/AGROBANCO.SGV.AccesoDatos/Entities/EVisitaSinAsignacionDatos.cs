﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
   public class EVisitaSinAsignacionDatos
    {
        public int CodigoVisita { get; set; }
        public string NumeroDocumento { get; set; }
        public string NombreCompleto { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string UsuarioCreacion { get; set; }
        public string Cultivo { get; set; }
    }
}

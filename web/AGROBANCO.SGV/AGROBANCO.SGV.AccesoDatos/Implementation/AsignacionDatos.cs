﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class AsignacionDatos: IAsignacionDatos
    {

        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public AsignacionDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public bool AsignacionIndividual(EProfesionalAsignacionDatos element)
        {
            try
            {
                var lDataParam = new iDB2Parameter[11];
                lDataParam[0] = _db2DataContext.CreateParameter("P_ASIDTIP", int.MaxValue, element.TipoDocumento, 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_ASINDCA", 15, element.NumDocumento.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("P_ASIIDFN", 15, element.CodigoFuncionario.ToString());
                lDataParam[3] = _db2DataContext.CreateParameter("P_ASIIDPN", int.MaxValue, element.CodigoProfesional, 0, iDB2DbType.iDB2Integer);
                lDataParam[4] = _db2DataContext.CreateParameter("P_ASISTAN", int.MaxValue, element.Estado, 0, iDB2DbType.iDB2Integer);
                lDataParam[5] = _db2DataContext.CreateParameter("P_ASIUREA", 200, element.CodigoUsuario.ToString());
                lDataParam[6] = _db2DataContext.CreateParameter("P_ASIWUFA", 15, element.UsuarioWeb.ToString());
                lDataParam[7] = _db2DataContext.CreateParameter("P_CLIPNOA", 200, element.PrimerNombre.ToString());
                lDataParam[8] = _db2DataContext.CreateParameter("P_CLISNOA", 200, element.SegundoNombre.ToString());
                lDataParam[9] = _db2DataContext.CreateParameter("P_CLIAPAA", 200, element.ApellidoPaterno.ToString());
                lDataParam[10] = _db2DataContext.CreateParameter("P_CLIAMAA", 200, element.ApellidoMaterno.ToString());
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_ASIGNACION_INDIVIDUAL", out int rowsAffected, lDataParam);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AsignacionGrupal(List<EProfesionalAsignacionDatos> element)
        {
            try
            {
                foreach(EProfesionalAsignacionDatos item in element)
                {
                    var lDataParam = new iDB2Parameter[7];
                    lDataParam[0] = _db2DataContext.CreateParameter("P_ASIDTIP", int.MaxValue, item.TipoDocumento, 0, iDB2DbType.iDB2Integer);
                    lDataParam[1] = _db2DataContext.CreateParameter("P_ASINDCA", 15, item.NumDocumento.ToString());
                    lDataParam[2] = _db2DataContext.CreateParameter("P_ASIIDFN", 15, item.CodigoFuncionario.ToString());
                    lDataParam[3] = _db2DataContext.CreateParameter("P_ASIIDPN", int.MaxValue, item.CodigoProfesional, 0, iDB2DbType.iDB2Integer);
                    lDataParam[4] = _db2DataContext.CreateParameter("P_ASISTAN", int.MaxValue, item.Estado, 0, iDB2DbType.iDB2Integer);
                    lDataParam[5] = _db2DataContext.CreateParameter("P_ASIUREA", 200, item.CodigoUsuario.ToString());
                    lDataParam[6] = _db2DataContext.CreateParameter("P_ASIWUFA", 15, item.UsuarioWeb.ToString());
                    _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VISITA_SIN_ASIGNACIONACION", out int rowsAffected, lDataParam);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AsignarProfesionalClientes(List<EProfesionalAsignacionDatos> lista)
        {
            try
            {

                foreach (EProfesionalAsignacionDatos element in lista)
                {
                    var lDataParam = new iDB2Parameter[11];
                    lDataParam[0] = _db2DataContext.CreateParameter("P_ASIDTIP", int.MaxValue, element.TipoDocumento, 0, iDB2DbType.iDB2Integer);
                    lDataParam[1] = _db2DataContext.CreateParameter("P_ASINDCA", 15, element.NumDocumento.ToString());
                    lDataParam[2] = _db2DataContext.CreateParameter("P_ASIIDFN", 15, element.CodigoFuncionario.ToString());
                    lDataParam[3] = _db2DataContext.CreateParameter("P_ASIIDPN", int.MaxValue, element.CodigoProfesional, 0, iDB2DbType.iDB2Integer);
                    lDataParam[4] = _db2DataContext.CreateParameter("P_ASISTAN", int.MaxValue, element.Estado, 0, iDB2DbType.iDB2Integer);
                    lDataParam[5] = _db2DataContext.CreateParameter("P_ASIUREA", 200, element.CodigoUsuario.ToString());
                    lDataParam[6] = _db2DataContext.CreateParameter("P_CLIPNOM", 100, element.PrimerNombre.ToString());
                    lDataParam[7] = _db2DataContext.CreateParameter("P_CLISNOM", 100, element.SegundoNombre.ToString());
                    lDataParam[8] = _db2DataContext.CreateParameter("P_CLIAPPA", 100, element.ApellidoPaterno.ToString());
                    lDataParam[9] = _db2DataContext.CreateParameter("P_CLIAPMA", 100, element.ApellidoMaterno.ToString());
                    lDataParam[10] = _db2DataContext.CreateParameter("P_ASIWUFA", 15, element.UsuarioWeb.ToString());

                    _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_ASIGNACION", out int rowsAffected, lDataParam);
                }

                return true;

            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EClienteDatos> ObtenerListaClientes()
        {
            try
            {
                List<EClienteDatos> _EObj = new List<EClienteDatos>();

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_CLIENTES", out IDataReader reader, null);

                while (reader.Read())
                {
                    EClienteDatos cliente = new EClienteDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("ID")))
                    {

                        cliente.Codigo = Convert.ToInt32(reader["ID"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_DOC")))
                    {

                        cliente.TipoDocumento = Convert.ToInt32(reader["TIPO_DOC"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("DOCUMENTO")))
                    {
                        cliente.Numdocumento = Convert.ToString(reader["DOCUMENTO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PRIMER_NOMBRE")))
                    {
                        cliente.PrimerNombre = Convert.ToString(reader["PRIMER_NOMBRE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("SEGUNDO_NOMBRE")))
                    {
                        cliente.SegundoNombre = Convert.ToString(reader["SEGUNDO_NOMBRE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_PATERNO")))
                    {
                        cliente.ApellidoPaterno = Convert.ToString(reader["APELLIDO_PATERNO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_MATERNO")))
                    {
                        cliente.ApellidoMaterno = Convert.ToString(reader["APELLIDO_MATERNO"]).Trim();
                    }

                    cliente.NombreCompleto = cliente.PrimerNombre + ' ' + cliente.SegundoNombre + ' ' + cliente.ApellidoPaterno + ' ' + cliente.ApellidoMaterno;
                    
                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_ASOCIACION")))
                    {
                        cliente.CodigoAsosiacion = Convert.ToInt32(reader["CODIGO_ASOCIACION"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ESTADO")))
                    {
                        cliente.Estado = Convert.ToInt32(reader["ESTADO"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ULTIMA_EVALUACION")))
                    {
                        cliente.UltimoFiltro = Convert.ToString(reader["ULTIMA_EVALUACION"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_REGISTRO")))
                    {
                        cliente.UsuarioCreacion = Convert.ToString(reader["FECHA_REGISTRO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_ACTUALIZACION")))
                    {
                        cliente.UsuarioActualizacion = Convert.ToString(reader["FECHA_ACTUALIZACION"]).Trim();
                    }

                    _EObj.Add(cliente);
                    //break;
                }

                return _EObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EVisitaSinAsignacionDatos> ObtenerVisitasSinAsignacion(string nrodocumento)
        {

            try
            {
                List<EVisitaSinAsignacionDatos> _EObj = new List<EVisitaSinAsignacionDatos>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_NRODOC", 15, nrodocumento);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_BUSCAR_VISITAS_SINASIGNACION", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EVisitaSinAsignacionDatos profesionalTecnico = new EVisitaSinAsignacionDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_VISITA")))
                    {
                        profesionalTecnico.CodigoVisita = Convert.ToInt32(reader["CODIGO_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_DOCUMENTO_CLIENTE")))
                    {
                        profesionalTecnico.NumeroDocumento = Convert.ToString(reader["NUMERO_DOCUMENTO_CLIENTE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_COMPLETO")))
                    {
                        profesionalTecnico.NombreCompleto = Convert.ToString(reader["NOMBRE_COMPLETO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PRIMER_NOMBRE_CLIENTE")))
                    {
                        profesionalTecnico.PrimerNombre = Convert.ToString(reader["PRIMER_NOMBRE_CLIENTE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("SEGUNDO_NOMBRE_CLIENTE")))
                    {
                        profesionalTecnico.SegundoNombre = Convert.ToString(reader["SEGUNDO_NOMBRE_CLIENTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_PATERNO_CLIENTE")))
                    {
                        profesionalTecnico.ApellidoPaterno = Convert.ToString(reader["APELLIDO_PATERNO_CLIENTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_MATERNO_CLIENTE")))
                    {
                        profesionalTecnico.ApellidoMaterno = Convert.ToString(reader["APELLIDO_MATERNO_CLIENTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA")))
                    {
                        profesionalTecnico.Fecha = Convert.ToString(reader["FECHA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("HORA")))
                    {
                        profesionalTecnico.Hora = Convert.ToString(reader["HORA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_CREACION_VISITA")))
                    {
                        profesionalTecnico.UsuarioCreacion = Convert.ToString(reader["USUARIO_CREACION_VISITA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CULTIVO_DES")))
                    {
                        profesionalTecnico.Cultivo = Convert.ToString(reader["CULTIVO_DES"]).Trim();
                    }

                    _EObj.Add(profesionalTecnico);
                }

                return _EObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }





        }




    }
}

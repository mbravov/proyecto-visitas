﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using AGROBANCO.SGV.Comun.Laserfiche;
using System.IO;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class CargaVisitaDatos : ICargaVisitaDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;


        public CargaVisitaDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            
            library = KeyDb2.obtenerParametros("Db2Library");
        }


        private    int subirLaserFicher(String arcbytes, String nombre, String extension, String folder)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            Documento doc = new Documento();
            int docRpta;
                doc.ArchivoBytes = arcbytes;
                doc.Nombre = nombre;
                doc.Extension = extension;
                doc.Folder = folder;
                docRpta =   laserfiche.UploadDocumento(doc).CodigoLaserfiche;
            var resultado =  docRpta;
             return resultado;
            
        }


 

        public  bool RegistrarCargaVisita(ECargaVisitaDatos cargavisita)
        {
            List<EImagenesDatos> ListaImagenes = new List<EImagenesDatos>();
            List<ECoordenadasDatos> ListaCoordenadas = new List<ECoordenadasDatos>();
            int codigoVisita = 0;
            LaserficheProxy laserfiche = new LaserficheProxy();


            try
            {

                ImagenesModel imagenesModelRequest = new ImagenesModel();
                ImagenesModel imagenesModelResponse = new ImagenesModel();

                imagenesModelRequest.Folder = cargavisita.NumDocumentoCliente;
                List<ItemImagen> listIma = new List<ItemImagen>();


                if (!string.IsNullOrEmpty(cargavisita.FirmaTitular)) {
                    ItemImagen itemima = new ItemImagen();
                    itemima.ArchivoBytes = cargavisita.FirmaTitular;
                    itemima.Extension = "jpg";
                    itemima.Nombre = "FirmaTitular";
                    itemima.Tipo =10 ;
                    itemima.CodigoImagen = 1;
                    listIma.Add(itemima);
                }

                if (!string.IsNullOrEmpty(cargavisita.FirmaConyugue)) {
                    ItemImagen itemima = new ItemImagen();
                    itemima.ArchivoBytes = cargavisita.FirmaConyugue;
                    itemima.Extension = "jpg";
                    itemima.Nombre = "FirmaConyuge";
                    itemima.Tipo =10 ;
                    itemima.CodigoImagen = 2;
                    listIma.Add(itemima);
                }

                if (!string.IsNullOrEmpty(cargavisita.CapturaMapa)) {

                    ItemImagen itemima = new ItemImagen();
                    itemima.ArchivoBytes = cargavisita.CapturaMapa;
                    itemima.Extension = "jpg";
                    itemima.Nombre = "MapaTerreno";
                    itemima.Tipo = 10;
                    itemima.CodigoImagen = 3;
                    listIma.Add(itemima);

                }

                foreach(var ima in cargavisita.ListaImagenes) {
                    ItemImagen itemima = new ItemImagen();
                    itemima.ArchivoBytes = ima.ArchivoByte;
                    itemima.Extension = "jpg";
                    itemima.Nombre = ima.NombreArchivo;
                    itemima.Tipo = ima.TipoImagen;
                    itemima.CodigoImagen = ima.Codigo;
                    listIma.Add(itemima);
                }
                imagenesModelRequest.ListaImagenes = listIma;

                imagenesModelResponse= laserfiche.UploadListaDocumento(imagenesModelRequest);

                if (imagenesModelResponse.ResultadoOK == false)
                {
                    return false;
                }
                else { 
                
               

                var codigoLFFirmaTitular = 0;
                var codigoLFFirmaConyugue = 0;
                var codigoLFMapaTerreno = 0;
                foreach (var i in imagenesModelResponse.ListaImagenes) {
                    if (i.Tipo == 10) {

                        switch (i.CodigoImagen) {
                            case 1:
                                codigoLFFirmaTitular = i.CodigoLaser;
                                break;
                            case 2:
                                codigoLFFirmaConyugue = i.CodigoLaser;
                                break;
                            case 3: codigoLFMapaTerreno = i.CodigoLaser; break;
                        }
                    }
                 if (i.Tipo == 1 || i.Tipo==2) {
                        foreach (var ima in cargavisita.ListaImagenes)
                        {
                            if (ima.Codigo == i.CodigoImagen && ima.TipoImagen== i.Tipo) {
                                ima.CodigoLaserFiche = i.CodigoLaser;
                                    ima.NombreArchivo = i.Nombre;
                            }
                        }
                    }
              }



                var lDataParam = new iDB2Parameter[78];
                //lDataParam[0] = _db2DataContext.CreateParameter("P_VISISCA", 100, cargavisita.CodigoSolicitudCredito.ToString());
                lDataParam[0] = _db2DataContext.CreateParameter("P_VISNDCA", 15, cargavisita.NumDocumentoCliente.ToString());

                lDataParam[1] = _db2DataContext.CreateParameter("P_VDEAGEN", int.MaxValue, cargavisita.CodigoAgenciaAsignada, 0, iDB2DbType.iDB2Integer);
                //lDataParam[2] = _db2DataContext.CreateParameter("P_VISUFIN", int.MaxValue, cargavisita.UltimoFiltro, 0, iDB2DbType.iDB2Integer);
                //lDataParam[3] = _db2DataContext.CreateParameter("P_VISFFIF", 50, cargavisita.FechaUltimoFiltro.FormatFechaDB2());
                lDataParam[2] = _db2DataContext.CreateParameter("P_VISAGEN", int.MaxValue, cargavisita.CodigoAgenciaAsignada, 0, iDB2DbType.iDB2Integer);
                lDataParam[3] = _db2DataContext.CreateParameter("P_VISASIN", int.MaxValue, cargavisita.CodigoAsignacion, 0, iDB2DbType.iDB2Integer);
                //lDataParam[6] = _db2DataContext.CreateParameter("P_VISFASF", 50, cargavisita.FechaAsignacion.FormatFechaDB2());
                //lDataParam[7] = _db2DataContext.CreateParameter("P_VISIDFN", int.MaxValue, cargavisita.CodigoFuncionario, 0, iDB2DbType.iDB2Integer);
                //lDataParam[8] = _db2DataContext.CreateParameter("P_VISIDPN", int.MaxValue, cargavisita.CodigoProfesionalTecnico, 0, iDB2DbType.iDB2Integer);
                lDataParam[4] = _db2DataContext.CreateParameter("P_VISTIVN", int.MaxValue, cargavisita.TipoVisita, 0, iDB2DbType.iDB2Integer);
                //lDataParam[10] = _db2DataContext.CreateParameter("P_VISSTAN", int.MaxValue, cargavisita.EstadoVisita, 0, iDB2DbType.iDB2Integer);
                //lDataParam[11] = _db2DataContext.CreateParameter("P_VISFAPF", 50, cargavisita.FechaAprobacion.FormatFechaDB2());
                //lDataParam[12] = _db2DataContext.CreateParameter("P_VISUAPA", 100, cargavisita.UsuarioAprobacion.ToString());
                lDataParam[5] = _db2DataContext.CreateParameter("P_VISUREA", 100, cargavisita.UsuarioCreacionVisita);
                lDataParam[6] = _db2DataContext.CreateParameter("P_VISIDMA", 100, cargavisita.CodigoVisitaMobile);
                
                lDataParam[7] = _db2DataContext.CreateParameter("P_VDEPNOA", 200, cargavisita.PrimerNombrecliente.ToString());
                lDataParam[8] = _db2DataContext.CreateParameter("P_VDESNOA", 200, cargavisita.SegundoNombrecliente.ToString());
                lDataParam[9] = _db2DataContext.CreateParameter("P_VDEAPAA", 300, cargavisita.ApellidoPaternoCliente.ToString());
                lDataParam[10] = _db2DataContext.CreateParameter("P_VDEAMAA", 300, cargavisita.ApellidoMaternoCliente.ToString());
                lDataParam[11] = _db2DataContext.CreateParameter("P_VDENOPA", 300, cargavisita.NombrePredio.ToString());
                lDataParam[12] = _db2DataContext.CreateParameter("P_VDEDIPA", 500, cargavisita.DireccionPredio.ToString());
                lDataParam[13] = _db2DataContext.CreateParameter("P_VDETACN", int.MaxValue, cargavisita.TipoActividad, 0, iDB2DbType.iDB2Integer);
                lDataParam[14] = _db2DataContext.CreateParameter("P_VDETMAN", int.MaxValue, cargavisita.TipoMapa, 0, iDB2DbType.iDB2Integer);
                lDataParam[15] = _db2DataContext.CreateParameter("P_VDEMCAN", int.MaxValue, cargavisita.ModoCaptura, 0, iDB2DbType.iDB2Integer);

                lDataParam[16] = _db2DataContext.CreateParameter("P_VDENPGN", int.MaxValue, cargavisita.NumPuntosMapa, 0, iDB2DbType.iDB2Integer);
                lDataParam[17] = _db2DataContext.CreateParameter("P_VDENFPN", int.MaxValue, cargavisita.NumFotosPrincipales, 0, iDB2DbType.iDB2Integer);
                lDataParam[18] = _db2DataContext.CreateParameter("P_VDENFSN", int.MaxValue, cargavisita.NumFotosSecundarias, 0, iDB2DbType.iDB2Integer);
                lDataParam[19] = _db2DataContext.CreateParameter("P_VDEAREN", 13, cargavisita.AreaTotalPuntos, 4, iDB2DbType.iDB2Numeric);
                lDataParam[20] = _db2DataContext.CreateParameter("P_VDEFEVF", 50, cargavisita.FechaVisita.FormatFechaHoraDB2());

                lDataParam[21] = _db2DataContext.CreateParameter("P_VDEOBJA", 300, cargavisita.DescObjetivoVisita.ToString());
                lDataParam[22] = _db2DataContext.CreateParameter("P_VDEPEPN", int.MaxValue, cargavisita.PersonaPresente, 0, iDB2DbType.iDB2Integer);
                lDataParam[23] = _db2DataContext.CreateParameter("P_VDEVINA", 300, cargavisita.VinculoFamiliar.ToString());
                lDataParam[24] = _db2DataContext.CreateParameter("P_VDEFAMA", 300, cargavisita.NombreFamiliar);
                lDataParam[25] = _db2DataContext.CreateParameter("P_VDECELN", int.MaxValue, cargavisita.NumCelFamiliar, 0, iDB2DbType.iDB2Integer);
                lDataParam[26] = _db2DataContext.CreateParameter("P_VDEMAPN", int.MaxValue, cargavisita.MercadoAcceso, 0, iDB2DbType.iDB2Integer);
                lDataParam[27] = _db2DataContext.CreateParameter("P_VDEPRON", int.MaxValue, cargavisita.AreaProtegida, 0, iDB2DbType.iDB2Integer);
                lDataParam[28] = _db2DataContext.CreateParameter("P_VDEPARN", int.MaxValue, cargavisita.NumParcelas, 0, iDB2DbType.iDB2Integer);
                lDataParam[29] = _db2DataContext.CreateParameter("P_VDEHSEN", 20, cargavisita.NumHectareasSembradas.ToString());
                lDataParam[30] = _db2DataContext.CreateParameter("P_VDEMUCA", 100, cargavisita.UnidadCatastral.ToString());

                lDataParam[31] = _db2DataContext.CreateParameter("P_VDEHFIN", 20, cargavisita.NumHectareasFinanciar.ToString());
                lDataParam[32] = _db2DataContext.CreateParameter("P_VDEHTON", 20, cargavisita.NumHectareasTotales.ToString());

                lDataParam[33] = _db2DataContext.CreateParameter("P_VDERTEN", int.MaxValue, cargavisita.RegimenTenencia, 0, iDB2DbType.iDB2Integer);
                lDataParam[34] = _db2DataContext.CreateParameter("P_VDETUBN", int.MaxValue, cargavisita.TipoUbicacion, 0, iDB2DbType.iDB2Integer);
                lDataParam[35] = _db2DataContext.CreateParameter("P_VDESCAN", int.MaxValue, cargavisita.EstadoCampo, 0, iDB2DbType.iDB2Integer);
                lDataParam[36] = _db2DataContext.CreateParameter("P_VDERIEN", int.MaxValue, cargavisita.TipoRiego, 0, iDB2DbType.iDB2Integer);
                lDataParam[37] = _db2DataContext.CreateParameter("P_VDEDAGN", int.MaxValue, cargavisita.DisponibilidadAgua, 0, iDB2DbType.iDB2Integer);
                lDataParam[38] = _db2DataContext.CreateParameter("P_VDEPENN", int.MaxValue, cargavisita.Pendiente, 0, iDB2DbType.iDB2Integer);
                lDataParam[39] = _db2DataContext.CreateParameter("P_VDESUEN", int.MaxValue, cargavisita.TipoSuelo, 0, iDB2DbType.iDB2Integer);
                lDataParam[40] = _db2DataContext.CreateParameter("P_VDEACPN", int.MaxValue, cargavisita.AccesibilidadPredio, 0, iDB2DbType.iDB2Integer);

                lDataParam[41] = _db2DataContext.CreateParameter("P_VDEALTN", 20, cargavisita.AltitudPredio.ToString());

                lDataParam[42] = _db2DataContext.CreateParameter("P_VDECULN", 50, cargavisita.Cultivo.ToString());
                    lDataParam[43] = _db2DataContext.CreateParameter("P_VDEVARA", 100, cargavisita.Variedad.ToString());
                lDataParam[44] = _db2DataContext.CreateParameter("P_VDEFSIF", 50, cargavisita.FechaSiembra.FormatFechaHoraDB2());
                lDataParam[45] = _db2DataContext.CreateParameter("P_VDETSIN", int.MaxValue, cargavisita.TipoSiembra, 0, iDB2DbType.iDB2Integer);
                lDataParam[46] = _db2DataContext.CreateParameter("P_VDEKANN", 20, cargavisita.RendimientoAnterior.ToString());
                lDataParam[47] = _db2DataContext.CreateParameter("P_VDEKACN", 20, cargavisita.RendimientoEsperado.ToString());

                lDataParam[48] = _db2DataContext.CreateParameter("P_VDEUFIA", 100, cargavisita.UnidadFinanciar.ToString());
                lDataParam[49] = _db2DataContext.CreateParameter("P_VDENUFN", int.MaxValue, cargavisita.NumUnidadesFinanciar, 0, iDB2DbType.iDB2Integer);
                lDataParam[50] = _db2DataContext.CreateParameter("P_VDETUFN", int.MaxValue, cargavisita.NumTotalUnidades, 0, iDB2DbType.iDB2Integer);
                lDataParam[51] = _db2DataContext.CreateParameter("P_VDEUPRA", 100, cargavisita.UnidadesProductivas.ToString());
                lDataParam[52] = _db2DataContext.CreateParameter("P_VDEALIN", int.MaxValue, cargavisita.TipoAlimentacion, 0, iDB2DbType.iDB2Integer);
                lDataParam[53] = _db2DataContext.CreateParameter("P_VDEMANN", int.MaxValue, cargavisita.TipoManejo, 0, iDB2DbType.iDB2Integer);
                lDataParam[54] = _db2DataContext.CreateParameter("P_VDEFAGN", int.MaxValue, cargavisita.TipoFuenteAgua, 0, iDB2DbType.iDB2Integer);
                /*P_VDEAGUN - DisponibilidadAgua*/
                lDataParam[55] = _db2DataContext.CreateParameter("P_VDEAGUN", int.MaxValue, cargavisita.DisponibilidadAguaPecuario, 0, iDB2DbType.iDB2Integer);
                lDataParam[56] = _db2DataContext.CreateParameter("P_VDECRIN", 50, cargavisita.TipoCrianza.ToString());
                    lDataParam[57] = _db2DataContext.CreateParameter("P_VDERAZN", 100, cargavisita.Raza.ToString());
                lDataParam[58] = _db2DataContext.CreateParameter("P_VDEAEXN", int.MaxValue, cargavisita.AniosExp, 0, iDB2DbType.iDB2Integer);
                lDataParam[59] = _db2DataContext.CreateParameter("P_VDETECN", int.MaxValue, cargavisita.TipoTecnologia, 0, iDB2DbType.iDB2Integer);
                /*P_VDECMAN - TipoManejo*/
                lDataParam[60] = _db2DataContext.CreateParameter("P_VDECMAN", int.MaxValue, cargavisita.TipoManejoPecuario, 0, iDB2DbType.iDB2Integer);
                lDataParam[61] = _db2DataContext.CreateParameter("P_VDECOLA", 500, cargavisita.ComentPrediosColindantes.ToString());
                lDataParam[62] = _db2DataContext.CreateParameter("P_VDECOMA", 500, cargavisita.ComentRecomendaciones.ToString());
                lDataParam[63] = _db2DataContext.CreateParameter("P_VDEFTIN", int.MaxValue, codigoLFFirmaTitular, 0, iDB2DbType.iDB2Integer);
                lDataParam[64] = _db2DataContext.CreateParameter("P_VDEFCON", int.MaxValue, codigoLFFirmaConyugue, 0, iDB2DbType.iDB2Integer);

                lDataParam[65] = _db2DataContext.CreateParameter("P_VDEFIVF", 50, cargavisita.FechaInicioVisita.FormatFechaHoraDB2());
                lDataParam[66] = _db2DataContext.CreateParameter("P_VDEPIVA", 200, cargavisita.PrecisionInicioVisita.ToString());
                lDataParam[67] = _db2DataContext.CreateParameter("P_VDELAIA", 200, cargavisita.LatInicioVisita.ToString());
                lDataParam[68] = _db2DataContext.CreateParameter("P_VDELOIA", 200, cargavisita.LongInicioVisita.ToString());

                lDataParam[69] = _db2DataContext.CreateParameter("P_VDEFFVF", 50, cargavisita.FechaFinVisita.FormatFechaHoraDB2());
                lDataParam[70] = _db2DataContext.CreateParameter("P_VDEPFVA", 200, cargavisita.PrecisionFinVisita.ToString());
                lDataParam[71] = _db2DataContext.CreateParameter("P_VDELAFA", 200, cargavisita.LatFinVisita.ToString());
                lDataParam[72] = _db2DataContext.CreateParameter("P_VDELOGA", 200, cargavisita.LongFinVisita.ToString());

                lDataParam[73] = _db2DataContext.CreateParameter("P_VDEMAPA", int.MaxValue, codigoLFMapaTerreno, 0, iDB2DbType.iDB2Integer);

                lDataParam[74] = _db2DataContext.CreateParameter("P_VDEUFIN", 50, cargavisita.UnidadFinanciarCod.ToString());
                    lDataParam[75] = _db2DataContext.CreateParameter("P_VDEAEPN", int.MaxValue, cargavisita.AniosExpPecuario, 0, iDB2DbType.iDB2Integer);
                lDataParam[76] = _db2DataContext.CreateParameter("P_VDEUNPN", 50, cargavisita.UnidadPesoCod.ToString());


                    lDataParam[77] = _db2DataContext.CreateParameter("P_VISIDVI", int.MaxValue, direction: ParameterDirection.Output);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_CARGAVISITA", out int affectedRows, lDataParam);

                codigoVisita = Convert.ToInt32(lDataParam[77].Value);
                
                ListaImagenes = cargavisita.ListaImagenes;
                ListaCoordenadas = cargavisita.ListaCoordenadas;
                
                if (ListaImagenes != null && ListaImagenes.Count > 0)
                {
                         for (int i = 0; i < ListaImagenes.Count; i++)
                         {
                             EImagenesDatos imagen = ListaImagenes[i];
                             //int codigoLaserFiche = 0;


                             imagen.CodigoVisita = codigoVisita;
                             imagen.UsuarioCreacion = cargavisita.UsuarioCreacionVisita;
                             var lDataParamImagenes = new iDB2Parameter[12];

                             lDataParamImagenes[0] = _db2DataContext.CreateParameter("P_FOTIDVN", int.MaxValue, imagen.CodigoVisita, 0, iDB2DbType.iDB2Integer);
                             lDataParamImagenes[1] = _db2DataContext.CreateParameter("P_FOTILFN", int.MaxValue, imagen.CodigoLaserFiche, 0, iDB2DbType.iDB2Integer);
                             lDataParamImagenes[2] = _db2DataContext.CreateParameter("P_FOTORDN", int.MaxValue, imagen.NumOrdenCaptura, 0, iDB2DbType.iDB2Integer);
                             lDataParamImagenes[3] = _db2DataContext.CreateParameter("P_FOTNOMA", 200, imagen.NombreArchivo);
                             lDataParamImagenes[4] = _db2DataContext.CreateParameter("P_FOTIDMA", 200, imagen.CodigoFotoMobile);
                             lDataParamImagenes[5] = _db2DataContext.CreateParameter("P_FOTLONA", 200, imagen.LongCapturaImagen.ToString());
                             lDataParamImagenes[6] = _db2DataContext.CreateParameter("P_FOTLATA", 200, imagen.LatCapturaImagen.ToString());
                             lDataParamImagenes[7] = _db2DataContext.CreateParameter("P_FOTALTA", 200, imagen.AltitudCapturaImagen.ToString());
                             lDataParamImagenes[8] = _db2DataContext.CreateParameter("P_FOTPREA", 100, imagen.Precision.ToString());
                             lDataParamImagenes[9] = _db2DataContext.CreateParameter("P_FOTTIPN", int.MaxValue, imagen.TipoImagen, 0, iDB2DbType.iDB2Integer);
                             lDataParamImagenes[10] = _db2DataContext.CreateParameter("P_FOTTCON", int.MaxValue, imagen.TipoBien, 0, iDB2DbType.iDB2Integer);
                             lDataParamImagenes[11] = _db2DataContext.CreateParameter("P_FOTUREA", 100, imagen.UsuarioCreacion);

                             _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_IMAGEN", out int affectedRows1, lDataParamImagenes);
                         }
                        /*
                        Parallel.ForEach(ListaImagenes, word =>
                        {

                            EImagenesDatos imagen = word;
                            //int codigoLaserFiche = 0;


                            imagen.CodigoVisita = codigoVisita;
                            imagen.UsuarioCreacion = cargavisita.UsuarioCreacionVisita;
                            var lDataParamImagenes = new iDB2Parameter[12];

                            lDataParamImagenes[0] = _db2DataContext.CreateParameter("P_FOTIDVN", int.MaxValue, imagen.CodigoVisita, 0, iDB2DbType.iDB2Integer);
                            lDataParamImagenes[1] = _db2DataContext.CreateParameter("P_FOTILFN", int.MaxValue, imagen.CodigoLaserFiche, 0, iDB2DbType.iDB2Integer);
                            lDataParamImagenes[2] = _db2DataContext.CreateParameter("P_FOTORDN", int.MaxValue, imagen.NumOrdenCaptura, 0, iDB2DbType.iDB2Integer);
                            lDataParamImagenes[3] = _db2DataContext.CreateParameter("P_FOTNOMA", 200, imagen.NombreArchivo);
                            lDataParamImagenes[4] = _db2DataContext.CreateParameter("P_FOTIDMA", 200, imagen.CodigoFotoMobile);
                            lDataParamImagenes[5] = _db2DataContext.CreateParameter("P_FOTLONA", 200, imagen.LongCapturaImagen.ToString());
                            lDataParamImagenes[6] = _db2DataContext.CreateParameter("P_FOTLATA", 200, imagen.LatCapturaImagen.ToString());
                            lDataParamImagenes[7] = _db2DataContext.CreateParameter("P_FOTALTA", 200, imagen.AltitudCapturaImagen.ToString());
                            lDataParamImagenes[8] = _db2DataContext.CreateParameter("P_FOTPREA", 100, imagen.Precision.ToString());
                            lDataParamImagenes[9] = _db2DataContext.CreateParameter("P_FOTTIPN", int.MaxValue, imagen.TipoImagen, 0, iDB2DbType.iDB2Integer);
                            lDataParamImagenes[10] = _db2DataContext.CreateParameter("P_FOTTCON", int.MaxValue, imagen.TipoBien, 0, iDB2DbType.iDB2Integer);
                            lDataParamImagenes[11] = _db2DataContext.CreateParameter("P_FOTUREA", 100, imagen.UsuarioCreacion);

                            _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_IMAGEN", out int affectedRows1, lDataParamImagenes);


                        } ); */






                }
                
                
                if (ListaCoordenadas != null && ListaCoordenadas.Count > 0)
                {
                        
                              for (int i = 0; i < ListaCoordenadas.Count; i++)
                          {
                              ECoordenadasDatos coordenadas = ListaCoordenadas[i];
                              var lDataParamCoordenadas = new iDB2Parameter[8];

                              coordenadas.CodigoVisita = codigoVisita;
                              coordenadas.UsuarioCreacion = cargavisita.UsuarioCreacionVisita;

                              lDataParamCoordenadas[0] = _db2DataContext.CreateParameter("P_MAPIDVN", int.MaxValue, coordenadas.CodigoVisita, 0, iDB2DbType.iDB2Integer);
                              lDataParamCoordenadas[1] = _db2DataContext.CreateParameter("P_MAPORDN", int.MaxValue, coordenadas.NumOrden, 0, iDB2DbType.iDB2Integer);
                              lDataParamCoordenadas[2] = _db2DataContext.CreateParameter("P_MAPIDMA", 200, coordenadas.CodigoCoordenadaMobile);
                              lDataParamCoordenadas[3] = _db2DataContext.CreateParameter("P_MAPPREA", 200, coordenadas.Precision.ToString());
                              lDataParamCoordenadas[4] = _db2DataContext.CreateParameter("P_MAPLONA", 200, coordenadas.Longitud.ToString());
                              lDataParamCoordenadas[5] = _db2DataContext.CreateParameter("P_MAPLATA", 200, coordenadas.Latitud.ToString());
                              lDataParamCoordenadas[6] = _db2DataContext.CreateParameter("P_MAPALTA", 200, coordenadas.Altitud.ToString());
                              lDataParamCoordenadas[7] = _db2DataContext.CreateParameter("P_MAPUREA", 200, coordenadas.UsuarioCreacion);

                              _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_COORDENADAS", out int affectedRows2, lDataParamCoordenadas);
                          }
                        
                        /*
                        Parallel.ForEach(ListaCoordenadas, word =>
                        {
                            ECoordenadasDatos coordenadas = word;
                            var lDataParamCoordenadas = new iDB2Parameter[8];

                            coordenadas.CodigoVisita = codigoVisita;
                            coordenadas.UsuarioCreacion = cargavisita.UsuarioCreacionVisita;

                            lDataParamCoordenadas[0] = _db2DataContext.CreateParameter("P_MAPIDVN", int.MaxValue, coordenadas.CodigoVisita, 0, iDB2DbType.iDB2Integer);
                            lDataParamCoordenadas[1] = _db2DataContext.CreateParameter("P_MAPORDN", int.MaxValue, coordenadas.NumOrden, 0, iDB2DbType.iDB2Integer);
                            lDataParamCoordenadas[2] = _db2DataContext.CreateParameter("P_MAPIDMA", 200, coordenadas.CodigoCoordenadaMobile);
                            lDataParamCoordenadas[3] = _db2DataContext.CreateParameter("P_MAPPREA", 200, coordenadas.Precision.ToString());
                            lDataParamCoordenadas[4] = _db2DataContext.CreateParameter("P_MAPLONA", 200, coordenadas.Longitud.ToString());
                            lDataParamCoordenadas[5] = _db2DataContext.CreateParameter("P_MAPLATA", 200, coordenadas.Latitud.ToString());
                            lDataParamCoordenadas[6] = _db2DataContext.CreateParameter("P_MAPALTA", 200, coordenadas.Altitud.ToString());
                            lDataParamCoordenadas[7] = _db2DataContext.CreateParameter("P_MAPUREA", 200, coordenadas.UsuarioCreacion);

                            _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_COORDENADAS", out int affectedRows2, lDataParamCoordenadas);



                        });
                        */

                        }
                
                
                return true;
                }
            }
            catch (Exception ex)
            {                
                 throw ex;
            }
        }

    }
}

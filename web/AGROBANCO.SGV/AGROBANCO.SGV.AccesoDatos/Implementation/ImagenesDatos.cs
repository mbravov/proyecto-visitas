﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class ImagenesDatos : IImagenesDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public ImagenesDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        //public bool RegistrarImagenes(EImagenesDatos imagenes)
        //{
        //    try
        //    {
        //        var lDataParam = new iDB2Parameter[11];

        //        lDataParam[0] = _db2DataContext.CreateParameter("P_FOTIDVN", int.MaxValue, imagenes.codigovisita, 0, iDB2DbType.iDB2Integer);
        //        lDataParam[1] = _db2DataContext.CreateParameter("P_FOTILFN", int.MaxValue, imagenes.codigofotolas, 0, iDB2DbType.iDB2Integer);
        //        lDataParam[2] = _db2DataContext.CreateParameter("P_FOTORDN", int.MaxValue, imagenes.numordencaptura, 0, iDB2DbType.iDB2Integer);
        //        lDataParam[3] = _db2DataContext.CreateParameter("P_FOTLONA", 200, imagenes.longcapturaimagen.ToString());
        //        lDataParam[4] = _db2DataContext.CreateParameter("P_FOTLATA", 200, imagenes.latcapturaimagen.ToString());
        //        lDataParam[5] = _db2DataContext.CreateParameter("P_FOTALTA", 200, imagenes.altitudcapturaimagen.ToString());
        //        lDataParam[6] = _db2DataContext.CreateParameter("P_FOTPREA", 200, imagenes.presicion.ToString());
        //        lDataParam[7] = _db2DataContext.CreateParameter("P_FOTTIPN", int.MaxValue, imagenes.tipoimagen, 0, iDB2DbType.iDB2Integer);
        //        lDataParam[8] = _db2DataContext.CreateParameter("P_FOTTCON", int.MaxValue, imagenes.tipobien, 0, iDB2DbType.iDB2Integer);
        //        lDataParam[9] = _db2DataContext.CreateParameter("P_FOTUREA", 200, imagenes.usuariocreacion.ToString());
        //        lDataParam[10] = _db2DataContext.CreateParameter("P_FOTFREF", 50, imagenes.fechacreacion.FormatFechaDB2());


        //        _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_IMAGEN", out int rowsAffected, lDataParam);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}

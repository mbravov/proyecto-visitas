﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun;
using AGROBANCO.SGV.Comun.Database;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class ParametrosDatos : IParametrosDatos
    {

        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;
        public ParametrosDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }


        public EDescargaMovilDatos DescargaParametrosMovil(String usuario , String imei)
        {
            try
            {
                


                EDescargaMovilDatos descargaMovilDatos = new EDescargaMovilDatos();

                descargaMovilDatos.ListaAgencias = DescargaAgencias();
                descargaMovilDatos.Dispositivo = ObtenerValidacionImei(imei, usuario);



                List<DescargaCultivoDatos> listaCultivo = new List<DescargaCultivoDatos>();
                List<DescargaMedidaDatos> listaMedida = new List<DescargaMedidaDatos>();
                List<DescargaCrianzaDatos> listaCrianza = new List<DescargaCrianzaDatos>();
                List<DescargaUnidadFinanciarDatos> listaUnidadFinanciar = new List<DescargaUnidadFinanciarDatos>();

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_PARAMETROS_MOVIL", out IDataReader reader, null);


                while (reader.Read())
                {
                 

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_GRUPO")))
                    {

                        switch (Convert.ToInt32(reader["CODIGO_GRUPO"]))
                        {
                            case 3:
                                DescargaCultivoDatos item = new DescargaCultivoDatos();

                                if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                                {
                                    item.Codigo = Convert.ToString(reader["VALOR"]).Trim();
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("ORDEN")))
                                {
                                    item.Orden = Convert.ToInt32(reader["ORDEN"]);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PARAMETRO")))
                                {
                                    item.Nombre = Convert.ToString(reader["NOMBRE_PARAMETRO"]).Trim();
                                }
                                listaCultivo.Add(item);
                                break;
                            case 4:
                                DescargaMedidaDatos itemMedida = new DescargaMedidaDatos();

                                if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                                {
                                    itemMedida.Codigo = Convert.ToString(reader["VALOR"]).Trim();
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("ORDEN")))
                                {
                                    itemMedida.Orden = Convert.ToInt32(reader["ORDEN"]);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PARAMETRO")))
                                {
                                    itemMedida.Nombre = Convert.ToString(reader["NOMBRE_PARAMETRO"]).Trim();
                                }
                                listaMedida.Add(itemMedida);
                                break;
                            case 5:
                                DescargaCrianzaDatos itemCrianza = new DescargaCrianzaDatos();

                                if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                                {
                                    itemCrianza.Codigo = Convert.ToString(reader["VALOR"]).Trim();
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("ORDEN")))
                                {
                                    itemCrianza.Orden = Convert.ToInt32(reader["ORDEN"]);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PARAMETRO")))
                                {
                                    itemCrianza.Nombre = Convert.ToString(reader["NOMBRE_PARAMETRO"]).Trim();
                                }
                                listaCrianza.Add(itemCrianza);

                                break;
                            case 6:
                                DescargaUnidadFinanciarDatos itemUnidadFinanciar = new DescargaUnidadFinanciarDatos();

                                if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                                {
                                    itemUnidadFinanciar.Codigo = Convert.ToString(reader["VALOR"]).Trim();
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("ORDEN")))
                                {
                                    itemUnidadFinanciar.Orden = Convert.ToInt32(reader["ORDEN"]);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PARAMETRO")))
                                {
                                    itemUnidadFinanciar.Nombre = Convert.ToString(reader["NOMBRE_PARAMETRO"]).Trim();
                                }
                                listaUnidadFinanciar.Add(itemUnidadFinanciar);
                                break;
                            case 7:

                                if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_INTERNO")))
                                {
                                    if (Convert.ToInt32(reader["CODIGO_INTERNO"]) == 1)
                                    {
                                            descargaMovilDatos.Hectareas = Convert.ToDecimal(reader["VALOR"]);
                                    }

                                    if (Convert.ToInt32(reader["CODIGO_INTERNO"]) == 2)
                                    {
                                        descargaMovilDatos.DiasEliminar = Convert.ToInt32(reader["VALOR"]);
                                    }

                                    if (Convert.ToInt32(reader["CODIGO_INTERNO"]) == 3)
                                    {
                                        descargaMovilDatos.Precision = Convert.ToInt32(reader["VALOR"]);
                                    }


                                }
                                break;
                        }

                    }
                }

                descargaMovilDatos.ListaCultivo = listaCultivo;
                descargaMovilDatos.ListaMedida = listaMedida;
                descargaMovilDatos.ListaCrianza = listaCrianza;
                descargaMovilDatos.ListaUnidadFinanciar = listaUnidadFinanciar;

                return descargaMovilDatos;
            }
            catch (Exception ex)
            {

                throw ex;
            }





        }


        public List<EDescargaAgenciaDatos> DescargaAgencias()
        {
            try
            {
                List<EDescargaAgenciaDatos> listaDescargaAgencias = new List<EDescargaAgenciaDatos>();
         

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_PARAMETRO_AGENCIAS", out IDataReader reader, null);

                while (reader.Read())
                {
                    EDescargaAgenciaDatos descargaAgenciaDatos = new EDescargaAgenciaDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                    {
                        descargaAgenciaDatos.Codigo = Convert.ToInt32(reader["VALOR"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("OFICINA")))
                    {
                        descargaAgenciaDatos.Oficina = Convert.ToString(reader["OFICINA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("OFICINAS_REGIONALES")))
                    {
                        descargaAgenciaDatos.OficinaRegional = Convert.ToString(reader["OFICINAS_REGIONALES"]).Trim();
                    }

                  

                    listaDescargaAgencias.Add(descargaAgenciaDatos);
                }

                return listaDescargaAgencias;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }


        public List<EDescargaFuncionariosDatos> DescargaFuncionarios(int CodAgencia)
        {
            try
            {
                List<EDescargaFuncionariosDatos> listaDescargaFuncionarios = new List<EDescargaFuncionariosDatos>();
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_CODAGENCIA", int.MaxValue, CodAgencia, 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_FUNCIONARIOS", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EDescargaFuncionariosDatos descargaFuncionarios = new EDescargaFuncionariosDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("USRUARIO")))
                    {
                        descargaFuncionarios.Usuario = Convert.ToString(reader["USRUARIO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE")))
                    {
                        descargaFuncionarios.Nombre = Convert.ToString(reader["NOMBRE"]).Trim();
                    }

                    listaDescargaFuncionarios.Add(descargaFuncionarios);
                }

                return listaDescargaFuncionarios;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<EParametroDatos> ObtenerParametros(int CodGrupo)
        {
            try
            {
                List<EParametroDatos> listarParametros = new List<EParametroDatos>();
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_COD_GRUPO", int.MaxValue, CodGrupo, 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_PARAMETROS", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EParametroDatos objParametro = new EParametroDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_PARAMETRO")))
                    {
                        objParametro.CodigoParametro = Convert.ToInt32(reader["CODIGO_PARAMETRO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_GRUPO")))
                    {
                        objParametro.CodigoGrupo = Convert.ToInt32(reader["CODIGO_GRUPO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_INTERNO")))
                    {
                        objParametro.CodigoInterno = Convert.ToInt32(reader["CODIGO_INTERNO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PARAMETRO")))
                    {
                        objParametro.NombreParametro = Convert.ToString(reader["NOMBRE_PARAMETRO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_CORTO")))
                    {
                        objParametro.NombreCorto = Convert.ToString(reader["NOMBRE_CORTO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("DESCRIPCION")))
                    {
                        objParametro.Descripcion = Convert.ToString(reader["DESCRIPCION"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                    {
                        objParametro.Valor = Convert.ToString(reader["VALOR"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ORDEN")))
                    {
                        objParametro.Orden = Convert.ToInt32(reader["ORDEN"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ESTADO")))
                    {
                        objParametro.Estado = Convert.ToInt32(reader["ESTADO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_CREACION")))
                    {
                        objParametro.UsuarioCreacion = Convert.ToString(reader["USUARIO_CREACION"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_CREACION")))
                    {
                        objParametro.FechaCreacion = Convert.ToDateTime(reader["FECHA_CREACION"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_MODIFICACION")))
                    {
                        objParametro.UsuarioModificacion = Convert.ToString(reader["USUARIO_MODIFICACION"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_MODIFICACION")))
                    {
                        objParametro.FechaModificacion = Convert.ToDateTime(reader["FECHA_MODIFICACION"]);
                    }

                    listarParametros.Add(objParametro);
                }

                return listarParametros;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        public List<CultivoDatos> ObtenerCultivos()
        {
            try
            {
                List<CultivoDatos> datos = new List<CultivoDatos>();
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_CULTIVOS", out IDataReader reader, null);

                while (reader.Read())
                {
                    CultivoDatos dato = new CultivoDatos();
                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO")))
                    {
                        dato.Codigo = Convert.ToString(reader["CODIGO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("DESCRIPCION")))
                    {
                        dato.Descripcion = Convert.ToString(reader["DESCRIPCION"]).Trim();
                    }

                    datos.Add(dato);
                }
                return datos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EAsociacion> ObtenerAsociaciones(int CodAgencia)
        {
            try
            {
                List<EAsociacion> listarAsociaciones = new List<EAsociacion>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_CODAGENCIA", int.MaxValue, CodAgencia, 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_ASOCIACIONES", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EAsociacion objParametro = new EAsociacion();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO")))
                    {
                        objParametro.Codigo = Convert.ToInt32(reader["CODIGO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("VALOR")))
                    {
                        objParametro.Valor = Convert.ToString(reader["VALOR"]).Trim();
                    }

                    listarAsociaciones.Add(objParametro);
                }

                return listarAsociaciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EAsociacionIntegrante> ObtenerAsociacionIntegrantes(int CodAsociacion)
        {
            try
            {
                List<EAsociacionIntegrante> listarIntegrantes = new List<EAsociacionIntegrante>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_CODAGENCIA", int.MaxValue, CodAsociacion, 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_INT_ASOCIACION", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EAsociacionIntegrante objParametro = new EAsociacionIntegrante();

                    if (!reader.IsDBNull(reader.GetOrdinal("MPIIDN")))
                    {
                        objParametro.Documento = Convert.ToString(reader["MPIIDN"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("MPINOC")))
                    {
                        objParametro.Nombre = Convert.ToString(reader["MPINOC"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("RESULTADO")))
                    {
                        objParametro.Resultado = Convert.ToInt32(reader["RESULTADO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("MPINOC")))
                    {
                        objParametro.Nombre = Convert.ToString(reader["MPINOC"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_EVALUACION")))
                    {
                        objParametro.Fecha = Convert.ToString(reader["FECHA_EVALUACION"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("HORA_EVALUACION")))
                    {
                        objParametro.Hora = Convert.ToString(reader["HORA_EVALUACION"]).Trim();
                    }

                    listarIntegrantes.Add(objParametro);
                }

                return listarIntegrantes;

            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EIMEI> ObtenerIMEI()
        {
            try
            {
                List<EIMEI> listaIMEI = new List<EIMEI>();

                var lDataParam = new iDB2Parameter[1];
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_IMEI", out IDataReader reader, null);

                while (reader.Read())
                {
                    EIMEI objParametro = new EIMEI();

                    if (!reader.IsDBNull(reader.GetOrdinal("IDENTIFICADORMOVIL")))
                    {
                        objParametro.IdentificadorMovil = Convert.ToString(reader["IDENTIFICADORMOVIL"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO")))
                    {
                        objParametro.Usuario = Convert.ToString(reader["USUARIO"]).Trim();
                    }

                    listaIMEI.Add(objParametro);
                }

                return listaIMEI;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ObtenerValidacionVersion(string Version)
        {
            try
            {
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_VERSION", 15, Version.ToString());
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VALIDAR_VERSION", out IDataReader reader, lDataParam);
                int resultado = 0;
                while (reader.Read())
                {
                    EAsociacion objParametro = new EAsociacion();

                    if (!reader.IsDBNull(reader.GetOrdinal("CANTIDAD")))
                    {
                       resultado= Convert.ToInt32(reader["CANTIDAD"]);
                    }
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public int ObtenerValidacionImei(string Imei, string Usuario )
        {
            try
            {
                var lDataParam = new iDB2Parameter[3];
                lDataParam[0] = _db2DataContext.CreateParameter("P_IMEI", 200, Imei.ToString());
                lDataParam[1] = _db2DataContext.CreateParameter("P_USUARIO", 200, Usuario.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("P_RESULTADO", int.MaxValue, direction: ParameterDirection.Output);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VALIDAR_DISPOSITIVO", out IDataReader reader, lDataParam);
                int resultado = 0;
                resultado = Convert.ToInt32(lDataParam[2].Value);
           
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public int ResetearIMEI(string usuario)
        {
            try
            {
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_USUARIO", 200, usuario.ToString());
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_RESETEAR_IMEI", out IDataReader reader, lDataParam);

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<string> ObtenerEstadosCreditos()
        {
            try
            {
                List<string> resultado = new List<string>();
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_CREDITOS", out IDataReader reader, null);
                while (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("S10SIT")))
                    {
                        string data = Convert.ToString(reader["S10SIT"]).Trim();
                        resultado.Add(data);
                    }
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

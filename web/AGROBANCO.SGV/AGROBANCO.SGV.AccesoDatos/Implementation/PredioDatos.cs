﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun;
using AGROBANCO.SGV.Comun.Database;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class PredioDatos : IPredioDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public PredioDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }
        public List<EPredioDatos> ObtenerListaPredio(EPredioDatos filtro)
        {
            List<EPredioDatos> lstPredio = null;
            EPredioDatos oEPredioDatos = new EPredioDatos();
            try
            {
                var lDataParam = new iDB2Parameter[6];

                lDataParam[0] = _db2DataContext.CreateParameter("P_VISAGEN", int.MaxValue, (filtro.CodigoAgencia.HasValue ? filtro.CodigoAgencia.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_VISIDFN", 100, filtro.CodigoFuncionario.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("P_CLIASON", int.MaxValue, (filtro.CodigoAsociacion.HasValue ? filtro.CodigoAsociacion.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[3] = _db2DataContext.CreateParameter("P_CAMPANIA", int.MaxValue, (filtro.Campania.HasValue ? filtro.Campania.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[4] = _db2DataContext.CreateParameter("P_VDECULN", 50, filtro.Cultivo.ToString());
                lDataParam[5] = _db2DataContext.CreateParameter("P_ESTADO", 10, filtro.CodigoEstadoCredido.ToString());

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_PREDIOS", out IDataReader reader, lDataParam);

                lstPredio = new List<EPredioDatos>();

                while (reader.Read())
                {
                    oEPredioDatos = new EPredioDatos();
                    oEPredioDatos.codigo = reader.IsDBNull(reader.GetOrdinal("CODIGO")) ? 0 : Convert.ToInt32(reader["CODIGO"]);
                    oEPredioDatos.Comentario = reader.IsDBNull(reader.GetOrdinal("COMENTARIO")) ? "" : reader["COMENTARIO"].ToString().Trim();
                    oEPredioDatos.NombrePredio = reader.IsDBNull(reader.GetOrdinal("NOMBRE_PREDIO")) ? "" : reader["NOMBRE_PREDIO"].ToString().Trim();
                    oEPredioDatos.DireccionPredio = reader.IsDBNull(reader.GetOrdinal("DIRECCION_PREDIO")) ? "" : reader["DIRECCION_PREDIO"].ToString().Trim();
                    oEPredioDatos.Actividad = reader.IsDBNull(reader.GetOrdinal("ACTIVIDAD")) ? 0 : Convert.ToInt32(reader["ACTIVIDAD"]);
                    oEPredioDatos.fecha_visita = reader.IsDBNull(reader.GetOrdinal("FEC_VISITA")) ? (DateTime?)null : Convert.ToDateTime(reader["FEC_VISITA"]);

                    oEPredioDatos.ListaCoordenadas = new VisorVisitaDatos().ObtenerDatosVisitaCoordenada(Convert.ToInt32(oEPredioDatos.codigo));
                    oEPredioDatos.ListaImagenes = new VisorVisitaDatos().ObtenerDatosVisitaImagen(Convert.ToInt32(oEPredioDatos.codigo));

                    oEPredioDatos.PrimerNombre = reader.IsDBNull(reader.GetOrdinal("PRIMER_NOMBRE")) ? "" : reader["PRIMER_NOMBRE"].ToString().Trim();
                    oEPredioDatos.SegundoNombre = reader.IsDBNull(reader.GetOrdinal("SEGUNDO_NOMBRE")) ? "" : reader["SEGUNDO_NOMBRE"].ToString().Trim();
                    oEPredioDatos.ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("APELLIDO_PATERNO")) ? "" : reader["APELLIDO_PATERNO"].ToString().Trim();
                    oEPredioDatos.ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("APELLIDO_MATERNO")) ? "" : reader["APELLIDO_MATERNO"].ToString().Trim();

                    lstPredio.Add(oEPredioDatos);
                
                }
                 
                return lstPredio;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class ProfesionalTecnicoDatos : IProfesionalTecnicoDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public ProfesionalTecnicoDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library"); 
        }

     

        public List<EProfesionalTecnicoDatos> ObtenerListaProfesionalTecnico(EProfesionalTecnicoDatos EObj)
        {
            try
            {
                List<EProfesionalTecnicoDatos> _EObj = new List<EProfesionalTecnicoDatos>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_NUMERO_DOCUMENTO", 36, EObj.NumeroDocumento.ToString());
                _db2DataContext.RunStoredProcedure(library + ".UP_PRUEBA_SGV", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EProfesionalTecnicoDatos profesionalTecnico = new EProfesionalTecnicoDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("MUSNOMA")))
                    {
                        
                        profesionalTecnico.PrimerNombre =  reader.IsDBNull(reader.GetOrdinal("MUSNOMA")) ? null : Convert.ToString(reader["MUSNOMA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("MUSALIA")))
                    {
                        profesionalTecnico.ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("MUSALIA")) ? null : Convert.ToString(reader["MUSALIA"]).Trim(); ;
                    }

                    _EObj.Add(profesionalTecnico);
                    //break;
                }

                return _EObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EProfesionalTecnicoDatos> ObtenerListaProfesionalTecnico2(EProfesionalTecnicoDatos filtro)
        {
            try
            {
                List<EProfesionalTecnicoDatos> listaRespuesta = new List<EProfesionalTecnicoDatos>();
                var lDataParam = new iDB2Parameter[3];
                lDataParam[0] = _db2DataContext.CreateParameter("P_NUM_DOCUMENTO", 15, filtro.NumeroDocumento.ToString());
                lDataParam[1] = _db2DataContext.CreateParameter("P_ID_ASOCIACION", int.MaxValue, (filtro.CodigoAsociacion.HasValue ? filtro.CodigoAsociacion.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("P_ID_AGENCIA", int.MaxValue, (filtro.CodigoAgencia.HasValue ? filtro.CodigoAgencia.Value : 0), 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_PROFESIONALTECNICO", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EProfesionalTecnicoDatos profesionalTecnico = new EProfesionalTecnicoDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTIDPN"))) 
                    {
                        profesionalTecnico.Codigo = Convert.ToInt32(reader["PFTIDPN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTTDCN")))
                    {
                        profesionalTecnico.TipoDocumento = Convert.ToInt32(reader["PFTTDCN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTNDCA")))
                    {
                        profesionalTecnico.NumeroDocumento = Convert.ToString(reader["PFTNDCA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTPNOA")))
                    {
                        profesionalTecnico.PrimerNombre = Convert.ToString(reader["PFTPNOA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTSNOA")))
                    {
                        profesionalTecnico.SegundoNombre = Convert.ToString(reader["PFTSNOA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAPAA")))
                    {
                        profesionalTecnico.ApellidoPaterno = Convert.ToString(reader["PFTAPAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAMAA")))
                    {
                        profesionalTecnico.ApellidoMaterno = Convert.ToString(reader["PFTAMAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTNCEA")))
                    {
                        profesionalTecnico.Celular = Convert.ToString(reader["PFTNCEA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTEMAA")))
                    {
                        profesionalTecnico.Email = Convert.ToString(reader["PFTEMAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAGEN")))
                    {
                        profesionalTecnico.CodigoAgencia = Convert.ToInt32(reader["PFTAGEN"]);
                    }
                    
                    if (!reader.IsDBNull(reader.GetOrdinal("PFTSTAN")))
                    {
                        profesionalTecnico.Estado = Convert.ToInt16(reader["PFTSTAN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTWUSA")))
                    {
                        profesionalTecnico.UsuarioWeb = Convert.ToString(reader["PFTWUSA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("BRNNME")))
                    {
                        profesionalTecnico.NombreAgencia = Convert.ToString(reader["BRNNME"]).Trim();
                    }

                    listaRespuesta.Add(profesionalTecnico);
                }

                return listaRespuesta;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        
        }

        public bool RegistrarProfesionalTecnico(EProfesionalTecnicoDatos profesional)
        {
            try
            {
                var lDataParam = new iDB2Parameter[12];
                lDataParam[0] = _db2DataContext.CreateParameter("PFTTDCN", int.MaxValue, profesional.TipoDocumento, 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("PFTNDCA", 15, profesional.NumeroDocumento.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("PFTPNOA", 100, profesional.PrimerNombre.ToString());
                lDataParam[3] = _db2DataContext.CreateParameter("PFTSNOA", 100, profesional.SegundoNombre.ToString());
                lDataParam[4] = _db2DataContext.CreateParameter("PFTAPAA", 100, profesional.ApellidoPaterno.ToString());
                lDataParam[5] = _db2DataContext.CreateParameter("PFTAMAA", 100, profesional.ApellidoMaterno.ToString());
                lDataParam[6] = _db2DataContext.CreateParameter("PFTNCEA", 12, profesional.Celular.ToString());
                lDataParam[7] = _db2DataContext.CreateParameter("PFTEMAA", 200, profesional.Email.ToString());
                lDataParam[8] = _db2DataContext.CreateParameter("PFTAGEN", int.MaxValue, profesional.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                lDataParam[9] = _db2DataContext.CreateParameter("PFTSTAN", int.MaxValue, profesional.Estado, 0, iDB2DbType.iDB2Integer);
                lDataParam[10] = _db2DataContext.CreateParameter("PFTUREA", 200, profesional.UsuarioCreacion.ToString());
                lDataParam[11] = _db2DataContext.CreateParameter("PFTWUSA", 100, profesional.UsuarioWeb.ToString());

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_PROFESIONAL", out int rowsAffected, lDataParam);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarProfesionalTecnico(EProfesionalTecnicoDatos profesional)
        {
            try
            {
                var lDataParam = new iDB2Parameter[13];
                lDataParam[0] = _db2DataContext.CreateParameter("P_PFTIDPN", int.MaxValue, profesional.Codigo, 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_PFTTDCN", int.MaxValue, profesional.TipoDocumento, 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("P_PFTNDCA", 15, profesional.NumeroDocumento.ToString());
                lDataParam[3] = _db2DataContext.CreateParameter("P_PFTPNOA", 100, profesional.PrimerNombre.ToString());
                lDataParam[4] = _db2DataContext.CreateParameter("P_PFTSNOA", 100, profesional.SegundoNombre.ToString());
                lDataParam[5] = _db2DataContext.CreateParameter("P_PFTAPAA", 100, profesional.ApellidoPaterno.ToString());
                lDataParam[6] = _db2DataContext.CreateParameter("P_PFTAMAA", 100, profesional.ApellidoMaterno.ToString());
                lDataParam[7] = _db2DataContext.CreateParameter("P_PFTNCEA", 12, profesional.Celular.ToString());
                lDataParam[8] = _db2DataContext.CreateParameter("P_PFTEMAA", 200, profesional.Email.ToString());
                lDataParam[9] = _db2DataContext.CreateParameter("P_PFTAGEN", int.MaxValue, profesional.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                lDataParam[10] = _db2DataContext.CreateParameter("P_PFTSTAN", int.MaxValue, profesional.Estado, 0, iDB2DbType.iDB2Integer);
                lDataParam[11] = _db2DataContext.CreateParameter("P_PFTUMOA", 200, profesional.UsuarioCreacion.ToString());
                lDataParam[12] = _db2DataContext.CreateParameter("PFTWUSA", 100, profesional.UsuarioWeb.ToString());


                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_ACTUALIZAR_PROFESIONAL", out int rowsAffected, lDataParam);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EProfesionalTecnicoDatos ObtenerProfesionalTecnico(EProfesionalTecnicoDatos profesional)
        {
            try
            {
                EProfesionalTecnicoDatos profesionalTecnico = new EProfesionalTecnicoDatos();
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_PFTIDPN", int.MaxValue, profesional.Codigo, 0, iDB2DbType.iDB2Integer);                

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_OBTENER_PROFESIONALTECNICO", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("PFTIDPN")))
                    {
                        profesionalTecnico.Codigo = Convert.ToInt32(reader["PFTIDPN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTTDCN")))
                    {
                        profesionalTecnico.TipoDocumento = Convert.ToInt32(reader["PFTTDCN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTNDCA")))
                    {
                        profesionalTecnico.NumeroDocumento = Convert.ToString(reader["PFTNDCA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTPNOA")))
                    {
                        profesionalTecnico.PrimerNombre = Convert.ToString(reader["PFTPNOA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTSNOA")))
                    {
                        profesionalTecnico.SegundoNombre = Convert.ToString(reader["PFTSNOA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAPAA")))
                    {
                        profesionalTecnico.ApellidoPaterno = Convert.ToString(reader["PFTAPAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAMAA")))
                    {
                        profesionalTecnico.ApellidoMaterno = Convert.ToString(reader["PFTAMAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTNCEA")))
                    {
                        profesionalTecnico.Celular = Convert.ToString(reader["PFTNCEA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTEMAA")))
                    {
                        profesionalTecnico.Email = Convert.ToString(reader["PFTEMAA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTAGEN")))
                    {
                        profesionalTecnico.CodigoAgencia = Convert.ToInt32(reader["PFTAGEN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTWUSA")))
                    {
                        profesionalTecnico.UsuarioWeb = Convert.ToString(reader["PFTWUSA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PFTSTAN")))
                    {
                        profesionalTecnico.Estado = Convert.ToInt16(reader["PFTSTAN"]);
                    }
                }
                
                return profesionalTecnico;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

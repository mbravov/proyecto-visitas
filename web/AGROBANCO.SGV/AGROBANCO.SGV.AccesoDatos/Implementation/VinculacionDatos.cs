﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun;
using AGROBANCO.SGV.Comun.Database;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class VinculacionDatos : IVinculacion
    {

        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public VinculacionDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public ETokenVinculacion ObtenerTokenVinculacion(string codigoToken)
        {
            ETokenVinculacion token = null;
            try
            {
                var lDataParam = new iDB2Parameter[1];

                lDataParam[0] = _db2DataContext.CreateParameter("P_TKVIDTA", 100, codigoToken.ToString().Trim());

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_OBTENER_TKNVINCULACION", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    token = new ETokenVinculacion();

                    if (!reader.IsDBNull(reader.GetOrdinal("IDTOKEN")))
                    {
                        token.IdToken = reader["IDTOKEN"].ToString().Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ESTADOTOKEN")))
                    {
                        token.Estado = Convert.ToInt32(reader["ESTADOTOKEN"]);
                    }
                    break;
                }

                return token;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo)
        {
            //bool resultOperacion = false;
            try
            {
                var lDataParam = new iDB2Parameter[2];

                lDataParam[0] = _db2DataContext.CreateParameter("P_TKVIDTA", 100, codigoToken);
                lDataParam[1] = _db2DataContext.CreateParameter("P_TKNSTAN", int.MaxValue, estadoNuevo, 0, iDB2DbType.iDB2Integer);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_ACTUALIZAR_TKNVINCULACION", out int rowsAffected, lDataParam);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarTokenVinculacion(string codigotoken, string codigoapp)
        {
            try
            {
                var lDataParam = new iDB2Parameter[2];

                lDataParam[0] = _db2DataContext.CreateParameter("P_TKVIDTA", 100, codigotoken);
                lDataParam[1] = _db2DataContext.CreateParameter("P_TKNAPPA", 50, codigoapp);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_TKNVINCULACION", out int rowsAffected, lDataParam);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class VisitaDatos : IVisitaDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public VisitaDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public List<EVisitaDatos> ObtenerListavisita(EVisitaDatos filtro)
        {
            List<EVisitaDatos> lstvisitas = null;
            try
            {
                var lDataParam = new iDB2Parameter[5];

                lDataParam[0] = _db2DataContext.CreateParameter("P_VISAGEN", int.MaxValue, (filtro.CodigoAgencia.HasValue ? filtro.CodigoAgencia.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_VISIDFN", 100, filtro.CodigoFuncionario.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("P_VISTIVN", int.MaxValue, (filtro.TipoVisita.HasValue ? filtro.TipoVisita.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[3] = _db2DataContext.CreateParameter("P_VISSTAN", int.MaxValue, (filtro.Estado.HasValue ? filtro.Estado.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[4] = _db2DataContext.CreateParameter("P_VINCULADO", int.MaxValue, (filtro.Vinculado.HasValue ? filtro.Vinculado.Value : 0), 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_VISITAS", out IDataReader reader, lDataParam);

                lstvisitas = new List<EVisitaDatos>();

                while (reader.Read())
                {
                    lstvisitas.Add(new EVisitaDatos
                    {
                        Codigo = reader.IsDBNull(reader.GetOrdinal("CODIGO")) ? 0 : Convert.ToInt32(reader["CODIGO"]),
                        CodigoSolicitudCredito = reader.IsDBNull(reader.GetOrdinal("NUM_SOLICITUD")) ? null : reader["NUM_SOLICITUD"].ToString(),
                        TipoVisita = reader.IsDBNull(reader.GetOrdinal("TIPO_VISITA_COD")) ? 0 : Convert.ToInt32(reader["TIPO_VISITA_COD"]),
                        FechaRegistro = reader.IsDBNull(reader.GetOrdinal("FEC_ASIG")) ? (DateTime?)null : Convert.ToDateTime(reader["FEC_ASIG"]),
                        Estado = reader.IsDBNull(reader.GetOrdinal("ESTADO_VISITA")) ? 0 : Convert.ToInt32(reader["ESTADO_VISITA"]),
                        Cultivo = reader.IsDBNull(reader.GetOrdinal("CULTIVO")) ? "" : Convert.ToString(reader["CULTIVO"]),
                        CodigoAsignacion = reader.IsDBNull(reader.GetOrdinal("CODIGO_ASIGNACION")) ? 0 : Convert.ToInt32(reader["CODIGO_ASIGNACION"]),
                        CodigoProfesionalTecnico = reader.IsDBNull(reader.GetOrdinal("CODIGO_PROFESIONAL_TECNICO")) ? 0 : Convert.ToInt32(reader["CODIGO_PROFESIONAL_TECNICO"]),
                        NombreFuncionario = reader.IsDBNull(reader.GetOrdinal("FUNCIONARIO")) ? "" : reader["FUNCIONARIO"].ToString().Trim(),
                        EAsignacion = new EAsignacionDatos
                        {
                            ECliente = new EClienteDatos
                            {
                                Numdocumento = reader.IsDBNull(reader.GetOrdinal("NUM_DOC")) ? null : reader["NUM_DOC"].ToString(),
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("CLIENTE_PRIMERNOM")) ? null : reader["CLIENTE_PRIMERNOM"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("CLIENTE_SEGUNDONOM")) ? null : reader["CLIENTE_SEGUNDONOM"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("CLIENTE_APATERNO")) ? null : reader["CLIENTE_APATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("CLIENTE_AMATERNO")) ? null : reader["CLIENTE_AMATERNO"].ToString().Trim(),
                            },
                            EProfesionalTecnico = new EProfesionalTecnicoDatos
                            {
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("PT_PRIMERNOM")) ? reader["PT2_PRIMERNOM"].ToString().Trim() : reader["PT_PRIMERNOM"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("PT_SEGUNDONOM")) ? reader["PT2_SEGUNDONOM"].ToString().Trim() : reader["PT_SEGUNDONOM"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("PT_APATERNO")) ? reader["PT2_APATERNO"].ToString().Trim() : reader["PT_APATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("PT_AMATERNO")) ? reader["PT2_AMATERNO"].ToString().Trim() : reader["PT_AMATERNO"].ToString().Trim(),
                            },
                            FechaRegistro = reader.IsDBNull(reader.GetOrdinal("FEC_VISITA")) ? (DateTime?)null : Convert.ToDateTime(reader["FEC_VISITA"]),
                            Estao =  reader.IsDBNull(reader.GetOrdinal("ESTADO_ASIGNACION")) ? 0 : Convert.ToInt32(reader["ESTADO_ASIGNACION"]),
                        },
                    });
                }

                return lstvisitas;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public List<EVisitaDatos> ObtenerListaVincularVisita(string documento)
        {
            List<EVisitaDatos> lstvisitas = null;
            try
            {
                var lDataParam = new iDB2Parameter[1];

                lDataParam[0] = _db2DataContext.CreateParameter("P_DOCUMENTO", 15, documento.Trim());
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_VINCULARVISITA", out IDataReader reader, lDataParam);

                lstvisitas = new List<EVisitaDatos>();

                while (reader.Read())
                {
                    lstvisitas.Add(new EVisitaDatos
                    {
                        Codigo = reader.IsDBNull(reader.GetOrdinal("IDENTIFICADOR")) ? 0 : Convert.ToInt32(reader["IDENTIFICADOR"]),
                        Cultivo = reader.IsDBNull(reader.GetOrdinal("CULTIVO")) ? "" : Convert.ToString(reader["CULTIVO"]).Trim(),
                        NumDocumentoCliente = reader.IsDBNull(reader.GetOrdinal("NRODOCUMENTO")) ? "" : Convert.ToString(reader["NRODOCUMENTO"]).Trim(),
                        CodigoSolicitudCredito = reader.IsDBNull(reader.GetOrdinal("SOLCREDITO")) ? "" : Convert.ToString(reader["SOLCREDITO"]).Trim(),
                        FechaAprobacion = reader.IsDBNull(reader.GetOrdinal("FECHAAPROBACION")) ? (DateTime?)null : Convert.ToDateTime(reader["FECHAAPROBACION"]),
                        EAsignacion = new EAsignacionDatos
                        {
                            ECliente = new EClienteDatos
                            {
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("NOMBRE")) ? null : reader["NOMBRE"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("SEGUNDONOMBRE")) ? null : reader["SEGUNDONOMBRE"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("APELLIDOPATERNO")) ? null : reader["APELLIDOPATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("APELLIDOMATERNO")) ? null : reader["APELLIDOMATERNO"].ToString().Trim(),
                            },
                            EProfesionalTecnico = new EProfesionalTecnicoDatos
                            {
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("PT_PRIMERNOM")) ? null : reader["PT_PRIMERNOM"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("PT_SEGUNDONOM")) ? null : reader["PT_SEGUNDONOM"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("PT_APATERNO")) ? null : reader["PT_APATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("PT_AMATERNO")) ? null : reader["PT_AMATERNO"].ToString().Trim(),
                            },
                            CodigoFuncionario = reader.IsDBNull(reader.GetOrdinal("FUNCIONARIO")) ? null : reader["FUNCIONARIO"].ToString().Trim(),
                            NombreFuncionario = reader.IsDBNull(reader.GetOrdinal("NOMBREFUNCIONARIO")) ? null : reader["NOMBREFUNCIONARIO"].ToString().Trim(),
                            FechaAprobacion = reader.IsDBNull(reader.GetOrdinal("FECHAAPROBACION")) ? (DateTime?)null : Convert.ToDateTime(reader["FECHAAPROBACION"].ToString().Trim()),
                        },
                          
                    });
                }

                return lstvisitas;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional)
        {
            try
            {
                var lDataParam = new iDB2Parameter[2];

                lDataParam[0] = _db2DataContext.CreateParameter("P_ASIIDPN", int.MaxValue, (CodigoProfesional), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_ASIIDAN", int.MaxValue, (CodigoVisita), 0, iDB2DbType.iDB2Integer);
                
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REASIGNAR_VISITA_PT", out IDataReader reader, lDataParam);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ReasignarMasivo(int anterior, int nuevo)
        {
            try
            {
                var lDataParam = new iDB2Parameter[2];

                lDataParam[0] = _db2DataContext.CreateParameter("P_ASIIDPN", int.MaxValue, (anterior), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_ASIIDPN_N", int.MaxValue, (nuevo), 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REASIGNAR_MASIVO_PT", out IDataReader reader, lDataParam);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Anulacion(int asignacion)
        {
            try
            {
                var lDataParam = new iDB2Parameter[1];

                lDataParam[0] = _db2DataContext.CreateParameter("P_ASIIDAN", int.MaxValue, (asignacion), 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_ANULAR_ASIGNACION", out IDataReader reader, lDataParam);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Aprobacion(int codigovisita, int estadovisita, string comentario)
        {
            try
            {
                var lDataParam = new iDB2Parameter[3];

                lDataParam[0] = _db2DataContext.CreateParameter("P_VISIDVN", int.MaxValue, (codigovisita), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_VISSTAN", int.MaxValue, (estadovisita), 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("P_VISCOME", 200, comentario);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_APROBACION_VISITA", out IDataReader reader, lDataParam);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool AsignarSolicitudCredito(int codigovisita, string solicitudcredito)
        {
            try
            {
                var lDataParam = new iDB2Parameter[2];

                lDataParam[0] = _db2DataContext.CreateParameter("P_SOLCREDITO", 100, solicitudcredito);
                lDataParam[1] = _db2DataContext.CreateParameter("P_CODVISITA", int.MaxValue, (codigovisita), 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_ASIGNAR_SOLICITUDCREDITO", out IDataReader reader, lDataParam);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

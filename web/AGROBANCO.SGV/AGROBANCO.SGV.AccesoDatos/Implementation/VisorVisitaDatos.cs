﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun.Laserfiche;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class VisorVisitaDatos : IVisorVisitaDatos
    {

        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;
        public VisorVisitaDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public EVisorVisitaDatos ObtenerDatosVisita(int CodVisita)
        {
            try
            {
                LaserficheProxy laserfiche = new LaserficheProxy();
                EVisorVisitaDatos visorVisitaDatos = new EVisorVisitaDatos();
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_COD_VISITA", int.MaxValue, CodVisita, 0, iDB2DbType.iDB2Integer);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VISOR_VISITA", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
         
                    if (!reader.IsDBNull(reader.GetOrdinal("COMENTARIO")))
                    {
                        visorVisitaDatos.Comentario = Convert.ToString(reader["COMENTARIO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_VISITA")))
                    {
                        visorVisitaDatos.CodigoVisita = Convert.ToInt32(reader["CODIGO_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_DOCUMENTO_CLIENTE")))
                    {
                        visorVisitaDatos.NumDocumentoCliente = Convert.ToString(reader["NUMERO_DOCUMENTO_CLIENTE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_AGENCIA_ASIGNADA")))
                    {
                        visorVisitaDatos.CodigoAgenciaAsignada = Convert.ToInt32(reader["CODIGO_AGENCIA_ASIGNADA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_ASIGNACION")))
                    {
                        visorVisitaDatos.CodigoAsignacion = Convert.ToInt32(reader["CODIGO_ASIGNACION"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_PROFESIONAL_TECNICO")))
                    {
                        visorVisitaDatos.CodigoProfesionalTecnico = Convert.ToInt32(reader["CODIGO_PROFESIONAL_TECNICO"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("COD_TIPO_VISITA")))
                    {
                        visorVisitaDatos.TipoVisita = Convert.ToInt32(reader["COD_TIPO_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_CREACION_VISITA")))
                    {
                        visorVisitaDatos.UsuarioCreacionVisita = Convert.ToString(reader["USUARIO_CREACION_VISITA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_REGISTRO_VISITA")))
                    {
                        visorVisitaDatos.FechaRegistroVisita = Convert.ToString(reader["FECHA_REGISTRO_VISITA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PRIMER_NOMBRE_CLIENTE")))
                    {
                        visorVisitaDatos.PrimerNombrecliente = Convert.ToString(reader["PRIMER_NOMBRE_CLIENTE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("SEGUNDO_NOMBRE_CLIENTE")))
                    {
                        visorVisitaDatos.SegundoNombrecliente = Convert.ToString(reader["SEGUNDO_NOMBRE_CLIENTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_PATERNO_CLIENTE")))
                    {
                        visorVisitaDatos.ApellidoPaternoCliente = Convert.ToString(reader["APELLIDO_PATERNO_CLIENTE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("APELLIDO_MATERNO_CLIENTE")))
                    {
                        visorVisitaDatos.ApellidoMaternoCliente = Convert.ToString(reader["APELLIDO_MATERNO_CLIENTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_PREDIO")))
                    {
                        visorVisitaDatos.NombrePredio = Convert.ToString(reader["NOMBRE_PREDIO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("DIRECCION_PREDIO")))
                    {
                        visorVisitaDatos.DireccionPredio = Convert.ToString(reader["DIRECCION_PREDIO"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("COD_TIPO_ACTIVIDAD")))
                    {
                        visorVisitaDatos.TipoActividad = Convert.ToInt32(reader["COD_TIPO_ACTIVIDAD"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("COD_TIPO_MAPA")))
                    {
                        visorVisitaDatos.TipoMapa = Convert.ToInt32(reader["COD_TIPO_MAPA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("MODO_CAPTURA")))
                    {
                        visorVisitaDatos.ModoCaptura = Convert.ToInt32(reader["MODO_CAPTURA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_PUNTOS_MAPA")))
                    {
                        visorVisitaDatos.NumPuntosMapa = Convert.ToInt32(reader["NUMERO_PUNTOS_MAPA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_FOTOS_PRINCIPALES")))
                    {
                        visorVisitaDatos.NumFotosPrincipales = Convert.ToInt32(reader["NUMERO_FOTOS_PRINCIPALES"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_FOTOS_SECUNDARIAS")))
                    {
                        visorVisitaDatos.NumFotosSecundarias = Convert.ToInt32(reader["NUMERO_FOTOS_SECUNDARIAS"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("AREA_TOTAL_PUNTOS")))
                    {
                        visorVisitaDatos.AreaTotalPuntos = Convert.ToDecimal(reader["AREA_TOTAL_PUNTOS"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_VISITA")))
                    {
                        visorVisitaDatos.FechaVisita = Convert.ToDateTime(reader["FECHA_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("DESCRIPCION_OBJETIVO_VISITA")))
                    {
                        visorVisitaDatos.DescObjetivoVisita = Convert.ToString(reader["DESCRIPCION_OBJETIVO_VISITA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PERSONA_PRESENTE")))
                    {
                        visorVisitaDatos.PersonaPresente = Convert.ToInt32(reader["PERSONA_PRESENTE"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("VINCULO_FAMILIAR")))
                    {
                        visorVisitaDatos.VinculoFamiliar = Convert.ToString(reader["VINCULO_FAMILIAR"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_FAMILIAR")))
                    {
                        visorVisitaDatos.NombreFamiliar = Convert.ToString(reader["NOMBRE_FAMILIAR"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_CELULAR_FAMILIAR")))
                    {
                        visorVisitaDatos.NumCelFamiliar = Convert.ToInt32(reader["NUMERO_CELULAR_FAMILIAR"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("MERCADO_ACCESO")))
                    {
                        visorVisitaDatos.MercadoAcceso = Convert.ToInt32(reader["MERCADO_ACCESO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("AREA_PROTEGIDA")))
                    {
                        visorVisitaDatos.AreaProtegida = Convert.ToInt32(reader["AREA_PROTEGIDA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_PARCELAS")))
                    {
                        visorVisitaDatos.NumParcelas = Convert.ToInt32(reader["NUMERO_PARCELAS"]);
                    }


                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_HECTAREAS_SEMBRADAS")))
                    {
                        visorVisitaDatos.NumHectareasSembradas = Convert.ToDecimal(reader["NUMERO_HECTAREAS_SEMBRADAS"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("UNIDAD_CATASTRAL")))
                    {
                        visorVisitaDatos.UnidadCatastral = Convert.ToString(reader["UNIDAD_CATASTRAL"]).Trim();
                    }


                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_HECTAREAS_FINANCIAR")))
                    {
                        visorVisitaDatos.NumHectareasFinanciar = Convert.ToDecimal(reader["NUMERO_HECTAREAS_FINANCIAR"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_HECTAREAS_TOTALES")))
                    {
                        visorVisitaDatos.NumHectareasTotales = Convert.ToDecimal(reader["NUMERO_HECTAREAS_TOTALES"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("REGIMEN_TENENCIA")))
                    {
                        visorVisitaDatos.RegimenTenencia = Convert.ToInt32(reader["REGIMEN_TENENCIA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_UBICACION")))
                    {
                        visorVisitaDatos.TipoUbicacion = Convert.ToInt32(reader["TIPO_UBICACION"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ESTADO_CAMPO")))
                    {
                        visorVisitaDatos.EstadoCampo = Convert.ToInt32(reader["ESTADO_CAMPO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_RIEGO")))
                    {
                        visorVisitaDatos.TipoRiego = Convert.ToInt32(reader["TIPO_RIEGO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("DISPONIBILIDAD_AGUA")))
                    {
                        visorVisitaDatos.DisponibilidadAgua = Convert.ToInt32(reader["DISPONIBILIDAD_AGUA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PENDIENTE")))
                    {
                        visorVisitaDatos.Pendiente = Convert.ToInt32(reader["PENDIENTE"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_SUELO")))
                    {
                        visorVisitaDatos.TipoSuelo = Convert.ToInt32(reader["TIPO_SUELO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ACCESIBILIDAD_PREDIO")))
                    {
                        visorVisitaDatos.AccesibilidadPredio = Convert.ToInt32(reader["ACCESIBILIDAD_PREDIO"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ALTITUD_PREDIO")))
                    {
                        visorVisitaDatos.AltitudPredio = Convert.ToDecimal(reader["ALTITUD_PREDIO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CULTIVO")))
                    {
                        visorVisitaDatos.Cultivo = Convert.ToInt32(reader["CULTIVO"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("VARIEDAD")))
                    {
                        visorVisitaDatos.Variedad = Convert.ToString(reader["VARIEDAD"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_SIEMBRA")))
                    {
                        visorVisitaDatos.FechaSiembra = Convert.ToDateTime(reader["FECHA_SIEMBRA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_SIEMBRA")))
                    {
                        visorVisitaDatos.TipoSiembra = Convert.ToInt32(reader["TIPO_SIEMBRA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("RENDIMIENTO_ANTERIOR")))
                    {
                        visorVisitaDatos.RendimientoAnterior = Convert.ToDecimal(reader["RENDIMIENTO_ANTERIOR"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("RENDIMIENTO_ESPERADO")))
                    {
                        visorVisitaDatos.RendimientoEsperado = Convert.ToDecimal(reader["RENDIMIENTO_ESPERADO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("UNIDAD_FINANCIAR")))
                    {
                        visorVisitaDatos.UnidadFinanciar = Convert.ToString(reader["UNIDAD_FINANCIAR"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_UNIDADES_FINANCIAR")))
                    {
                        visorVisitaDatos.NumUnidadesFinanciar = Convert.ToInt32(reader["NUMERO_UNIDADES_FINANCIAR"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_TOTAL_UNIDADES")))
                    {
                        visorVisitaDatos.NumTotalUnidades = Convert.ToInt32(reader["NUMERO_TOTAL_UNIDADES"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("UNIDADES_PRODUCTIVAS")))
                    {
                        visorVisitaDatos.UnidadesProductivas = Convert.ToString(reader["UNIDADES_PRODUCTIVAS"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_ALIMENTACION")))
                    {
                        visorVisitaDatos.TipoAlimentacion = Convert.ToInt32(reader["TIPO_ALIMENTACION"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_MANEJO")))
                    {
                        visorVisitaDatos.TipoManejo = Convert.ToInt32(reader["TIPO_MANEJO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_FUENTE_AGUA")))
                    {
                        visorVisitaDatos.TipoFuenteAgua = Convert.ToInt32(reader["TIPO_FUENTE_AGUA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("DISPONIBILIDAD_AGUA_PECUARIO")))
                    {
                        visorVisitaDatos.DisponibilidadAguaPecuario = Convert.ToInt32(reader["DISPONIBILIDAD_AGUA_PECUARIO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_CRIANZA")))
                    {
                        visorVisitaDatos.TipoCrianza = Convert.ToInt32(reader["TIPO_CRIANZA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("RAZA")))
                    {
                        visorVisitaDatos.Raza = Convert.ToString(reader["RAZA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ANIOS_EXPERIENCIA")))
                    {
                        visorVisitaDatos.AniosExp = Convert.ToInt32(reader["ANIOS_EXPERIENCIA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_TECNOLOGIA")))
                    {
                        visorVisitaDatos.TipoTecnologia = Convert.ToInt32(reader["TIPO_TECNOLOGIA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_MANEJO_PECUARIO")))
                    {
                        visorVisitaDatos.TipoManejoPecuario = Convert.ToInt32(reader["TIPO_MANEJO_PECUARIO"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("COMENTARIO_PREDIO_COLINDANTE")))
                    {
                        visorVisitaDatos.ComentPrediosColindantes = Convert.ToString(reader["COMENTARIO_PREDIO_COLINDANTE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("COMENTARIO_RECOMENDACION")))
                    {
                        visorVisitaDatos.ComentRecomendaciones = Convert.ToString(reader["COMENTARIO_RECOMENDACION"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FIRMA_TITULAR")))
                    {
                        int codigoLFFirmaTitular = 0;
                        codigoLFFirmaTitular= Convert.ToInt32(reader["FIRMA_TITULAR"]);
                        visorVisitaDatos.FirmaTitular = codigoLFFirmaTitular.ToString();// laserfiche.ConsultarDocumento(codigoLFFirmaTitular);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FIRMA_CONYUGE")))
                    {
                       // visorVisitaDatos.FirmaConyugue = Convert.ToString(reader["FIRMA_CONYUGE"]).Trim();
                        int codigoLFFirmaConyuge = 0;
                        codigoLFFirmaConyuge = Convert.ToInt32(reader["FIRMA_CONYUGE"]);
                        visorVisitaDatos.FirmaConyugue = codigoLFFirmaConyuge.ToString();//laserfiche.ConsultarDocumento(codigoLFFirmaConyuge);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("CAPTURA_MAPA")))
                    {
                        int codigoLFMapa = 0;
                        codigoLFMapa = Convert.ToInt32(reader["CAPTURA_MAPA"]);
                        visorVisitaDatos.CapturaMapa = codigoLFMapa.ToString();//laserfiche.ConsultarDocumento(codigoLFMapa);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_INICIO_VISITA")))
                    {
                        visorVisitaDatos.FechaInicioVisita = Convert.ToDateTime(reader["FECHA_INICIO_VISITA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PRECISION_INICIO_VISITA")))
                    {
                        visorVisitaDatos.PrecisionInicioVisita = Convert.ToString(reader["PRECISION_INICIO_VISITA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LATITUD_INICIO_VISITA")))
                    {
                        visorVisitaDatos.LatInicioVisita = Convert.ToString(reader["LATITUD_INICIO_VISITA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LONGIUD_INICIO_VISITA")))
                    {
                        visorVisitaDatos.LongInicioVisita = Convert.ToString(reader["LONGIUD_INICIO_VISITA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("FECHA_FIN_VISITA")))
                    {
                        visorVisitaDatos.FechaFinVisita = Convert.ToDateTime(reader["FECHA_FIN_VISITA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PRECISION_FIN_VISITA")))
                    {
                        visorVisitaDatos.PrecisionFinVisita = Convert.ToString(reader["PRECISION_FIN_VISITA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LATITID_FIN_VISITA")))
                    {
                        visorVisitaDatos.LatFinVisita = Convert.ToString(reader["LATITID_FIN_VISITA"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("LONGITUD_FIN_VISITA")))
                    {
                        visorVisitaDatos.LongFinVisita = Convert.ToString(reader["LONGITUD_FIN_VISITA"]).Trim();
                    }



                    if (!reader.IsDBNull(reader.GetOrdinal("CULTIVO_DES")))
                    {
                        visorVisitaDatos.CultivoDescripcion = Convert.ToString(reader["CULTIVO_DES"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("UNIDAD_PESO_DES")))
                    {
                        visorVisitaDatos.UnidadPesoDescripcion = Convert.ToString(reader["UNIDAD_PESO_DES"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CRIANZA_DES")))
                    {
                        visorVisitaDatos.CrianzaDescripcion = Convert.ToString(reader["CRIANZA_DES"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("UNIDAD_FINANCIAR_DES")))
                    {
                        visorVisitaDatos.UnidadFinanciarDescripcion = Convert.ToString(reader["UNIDAD_FINANCIAR_DES"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("AGENCIA_DES")))
                    {
                        visorVisitaDatos.AgenciaDescripcion = Convert.ToString(reader["AGENCIA_DES"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ANIOS_EXPERIENCIA_PECUARIO")))
                    {
                        visorVisitaDatos.AniosExperienciaPecuario = Convert.ToInt32(reader["ANIOS_EXPERIENCIA_PECUARIO"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ESTADO_VISITA")))
                    {
                        visorVisitaDatos.EstadoVisita = Convert.ToInt32(reader["ESTADO_VISITA"]);
                    }
                }


                visorVisitaDatos.ListaCoordenadas = ObtenerDatosVisitaCoordenada(CodVisita);
                visorVisitaDatos.ListaImagenes = ObtenerDatosVisitaImagen(CodVisita);



                return visorVisitaDatos;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }



        public List<ECoordenadasDatos> ObtenerDatosVisitaCoordenada(int CodVisita)
        {
            try
            {
                List<ECoordenadasDatos> listaCoordenadas = new List<ECoordenadasDatos>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_COD_VISITA", int.MaxValue, CodVisita, 0, iDB2DbType.iDB2Integer);
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VISOR_VISITA_COORDENADA", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    ECoordenadasDatos coordenadasDatos = new ECoordenadasDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_COORDENADA")))
                    {
                        coordenadasDatos.Codigo = Convert.ToInt32(reader["CODIGO_COORDENADA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_VISITA")))
                    {
                        coordenadasDatos.CodigoVisita = Convert.ToInt32(reader["CODIGO_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_COORDENADA_MOBILE")))
                    {
                        coordenadasDatos.CodigoCoordenadaMobile = Convert.ToString(reader["CODIGO_COORDENADA_MOBILE"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_ORDEN")))
                    {
                        coordenadasDatos.NumOrden = Convert.ToInt32(reader["NUMERO_ORDEN"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PRECISION_COORDENADA")))
                    {
                        coordenadasDatos.Precision = Convert.ToString(reader["PRECISION_COORDENADA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LONGITUD")))
                    {
                        coordenadasDatos.Longitud = Convert.ToString(reader["LONGITUD"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LATITUD")))
                    {
                        coordenadasDatos.Latitud = Convert.ToString(reader["LATITUD"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ALTITUD")))
                    {
                        coordenadasDatos.Altitud = Convert.ToString(reader["ALTITUD"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_CREACION_COORDENADA")))
                    {
                        coordenadasDatos.UsuarioCreacion = Convert.ToString(reader["USUARIO_CREACION_COORDENADA"]).Trim();
                    }
                    listaCoordenadas.Add(coordenadasDatos);
                }

                return listaCoordenadas;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }




        public List<EImagenesDatos> ObtenerDatosVisitaImagen(int CodVisita)
        {
            LaserficheProxy laserfiche = new LaserficheProxy();
            try
            {
                List<EImagenesDatos> listaImagenes = new List<EImagenesDatos>();

                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_COD_VISITA", int.MaxValue, CodVisita, 0, iDB2DbType.iDB2Integer);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_VISOR_VISITA_IMAGEN", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    EImagenesDatos imagenDatos = new EImagenesDatos();

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_IMAGEN")))
                    {
                        imagenDatos.Codigo = Convert.ToInt32(reader["CODIGO_IMAGEN"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_VISITA")))
                    {
                        imagenDatos.CodigoVisita = Convert.ToInt32(reader["CODIGO_VISITA"]);
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("CODIGO_FOTO_MOBILE")))
                    {
                        imagenDatos.CodigoFotoMobile = Convert.ToString(reader["CODIGO_FOTO_MOBILE"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("TAMANO_IMAGEN")))
                    {
                        int codigoArchivoBytes = 0;
                        codigoArchivoBytes = Convert.ToInt32(reader["TAMANO_IMAGEN"]);
                        imagenDatos.ArchivoByte = codigoArchivoBytes.ToString();//laserfiche.ConsultarDocumento(codigoArchivoBytes);

                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("NUMERO_ORDEN_CAPTURA")))
                    {
                        imagenDatos.NumOrdenCaptura = Convert.ToInt32(reader["NUMERO_ORDEN_CAPTURA"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("NOMBRE_ARCHIVO")))
                    {
                        imagenDatos.NombreArchivo = Convert.ToString(reader["NOMBRE_ARCHIVO"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LONGITUD_CAPTURA")))
                    {
                        imagenDatos.LongCapturaImagen = Convert.ToString(reader["LONGITUD_CAPTURA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("LATITUD_CAPTURA")))
                    {
                        imagenDatos.LatCapturaImagen = Convert.ToString(reader["LATITUD_CAPTURA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("ALTITUD_CAPTURA")))
                    {
                        imagenDatos.AltitudCapturaImagen = Convert.ToString(reader["ALTITUD_CAPTURA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("PRECISION_CAPTURA")))
                    {
                        imagenDatos.Precision = Convert.ToString(reader["PRECISION_CAPTURA"]).Trim();
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_IMAGEN")))
                    {
                        imagenDatos.TipoImagen = Convert.ToInt32(reader["TIPO_IMAGEN"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("TIPO_BIEN")))
                    {
                        imagenDatos.TipoBien = Convert.ToInt32(reader["TIPO_BIEN"]);
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("USUARIO_CREACION_IMAGEN")))
                    {
                        imagenDatos.UsuarioCreacion = Convert.ToString(reader["USUARIO_CREACION_IMAGEN"]).Trim();
                    }



                    listaImagenes.Add(imagenDatos);
                }

                return listaImagenes;
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }




    }
}

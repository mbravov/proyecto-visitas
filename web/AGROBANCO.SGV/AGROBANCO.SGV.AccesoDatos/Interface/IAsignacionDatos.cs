﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IAsignacionDatos
    {
        List<EClienteDatos> ObtenerListaClientes();
        bool AsignarProfesionalClientes(List<EProfesionalAsignacionDatos> lista);
        bool AsignacionIndividual(EProfesionalAsignacionDatos element);
        bool AsignacionGrupal(List<EProfesionalAsignacionDatos> element);
        List<EVisitaSinAsignacionDatos> ObtenerVisitasSinAsignacion(String nrodocumento);
    }
}

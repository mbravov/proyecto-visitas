﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IParametrosDatos
    {
        EDescargaMovilDatos DescargaParametrosMovil(String usuario, String imei);

        List<EDescargaAgenciaDatos> DescargaAgencias();
        List<EDescargaFuncionariosDatos> DescargaFuncionarios(int CodAgencia);
        List<EParametroDatos> ObtenerParametros(int CodGrupo);
        List<EAsociacion> ObtenerAsociaciones(int CodAgencia);
        List<EAsociacionIntegrante> ObtenerAsociacionIntegrantes(int CodAsociacion);
        List<CultivoDatos> ObtenerCultivos();
        List<EIMEI> ObtenerIMEI();
        List<String> ObtenerEstadosCreditos();
        int ObtenerValidacionVersion(String Version);
        int ResetearIMEI(String usuario);
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IPredioDatos
    {
        List<EPredioDatos> ObtenerListaPredio(EPredioDatos filtro);
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IVinculacion
    {

        bool RegistrarTokenVinculacion(string codigotoken, string codigoapp);
        ETokenVinculacion ObtenerTokenVinculacion(string codigoToken);
        bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo);
    }
}

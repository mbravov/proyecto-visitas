﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IVisitaDatos
    {
        List<EVisitaDatos> ObtenerListavisita(EVisitaDatos filtro);
        bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional);
        bool ReasignarMasivo(int anterior, int nuevo);
        bool Anulacion(int asignacion);
        bool Aprobacion(int codigovisita, int estadovisita, string comentario);
        bool AsignarSolicitudCredito(int codvisita, string solicitudcredito);
        List<EVisitaDatos> ObtenerListaVincularVisita(string documento);
    }   
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using AGROBANCO.SGV.AccesoDatos.Entities;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
   public interface IVisorVisitaDatos
    {
        EVisorVisitaDatos ObtenerDatosVisita(int CodVisita);

    }
}

﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace AGROBANCO.SGV.Comun
{
    public static class KeyDb2
    {

        public static string obtenerParametros(string keys) {
            var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", false)
                  .Build();

            return configuration[keys];
        }

    }
}

﻿using System;
using System.Linq;
using System.Net;
using System.Web;

namespace AGROBANCO.SGV.Comun
{
    public static class HelpersComun
    {
        private static Random random = new Random();

        public static string GetNewGuid()
        {
            return Guid.NewGuid().ToString().ToUpper();
        }

        public static string RandomString(this int o)
        {

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, o)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string FormatFechaDB2(this DateTime? o)
        {
            try
            {
                return o.Value.ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string FormatFechaDB2(this DateTime o)
        {
            try
            {
                return o.ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string FormatFechaHoraDB2(this DateTime? o)
        {
            try
            {
                return o.Value.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string FormatFechaHoraDB2(this DateTime o)
        {
            try
            {
                return o.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}

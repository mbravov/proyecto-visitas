﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.Comun.Laserfiche
{
    public class Documento
    {
        public int CodigoLaserfiche { get; set; }
        public string ArchivoBytes { get; set; }
        public string Extension { get; set; }
        public string Nombre { get; set; }
        public string NombreDocumento { get; set; }
        public string Folder { get; set; }
        public string Tamanio { get; set; }
        public string ContentType { get; set; }
        public Documento() { }

    }

    public class DocumentoRequest
    {
        public int CodigoLaserfiche { get; set; }
        public string CodigoLaserficheAnidados { get; set; }
    }
   

    public class ServiceResponse<T>
    {
        public int Resultado { get; set; }
        public string Mensaje { get; set; }
        public T Data { get; set; }
        
        public List<T> DataList { get; set; }
        public int posicionLista { get; set; }
    }

    public class ImagenesModel
    {
        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }
        public string Folder { get; set; }
        public List<ItemImagen> ListaImagenes;
    }

    public class ItemImagen
    {
        public int CodigoImagen { get; set; }
        public int Tipo { get; set; }
        public string Extension { get; set; }
        public string Nombre { get; set; }

        public string ArchivoBytes { get; set; }
        public int CodigoLaser { get; set; }

        public bool ResultadoOK { get; set; }
        public String ResultadoMensaje { get; set; }



    }
}

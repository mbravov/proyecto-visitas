﻿using AGROBANCO.SGV.Comun.API;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Text.Json;

namespace AGROBANCO.SGV.Comun.Laserfiche
{
    public class LaserficheProxy
    {
        public string ConsultaArchivoSimple(Documento document)
        {
            string resultado = string.Empty;
            decimal peso;
            
            //using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(KeyDb2.obtenerParametros("LaserFicheRepository")))
            //{
            //    var stream = laserFicheDocumentAccess.GetDocument(codigoSustento, out fileName, out extension, out peso);
            //    resultado = HelperIT.MemoryStreamToBase64(stream);

            //    laserFicheDocumentAccess.Dispose();
            //}

            return resultado;
        }
        public String ConsultarDocumento(Int32 codigoLaser)
        {

            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserfiche = codigoLaser;
            ServicePointManager.ServerCertificateValidationCallback = new
            RemoteCertificateValidationCallback
            (
               delegate { return true; }
            );
            //int ldDocumentId = -1;
            Documento docRespuesta = new Documento();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();
                    
            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/get", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta.ArchivoBytes;
        }
        public List<Documento> ConsultarDocumentoLista(string codigoLaserAnidado) {
                
            DocumentoRequest documento = new DocumentoRequest();
            documento.CodigoLaserficheAnidados = codigoLaserAnidado;
            ServicePointManager.ServerCertificateValidationCallback = new
               RemoteCertificateValidationCallback
               (
                  delegate { return true; }
               );
            //int ldDocumentId = -1;
            List<Documento> docRespuesta = new List<Documento>();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();

            servicioResponse.DataList = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                
                StringContent sc = new StringContent(JsonConvert.SerializeObject(documento), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/getListImage", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.DataList;
                }

            }

            return docRespuesta;
        }
    
        public Documento UploadDocumento(Documento document)
        {
            //int ldDocumentId = -1;
            Documento docRespuesta = new Documento();
            ServiceResponse<Documento> servicioResponse = new ServiceResponse<Documento>();
            ServicePointManager.ServerCertificateValidationCallback = new
               RemoteCertificateValidationCallback
               (
                  delegate { return true; }
               );
            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");
               // var json = JsonConvert.SerializeObject(document)


                response = client.PostAsync("api/file/save", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<Documento>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }

        public ImagenesModel UploadListaDocumento(ImagenesModel document)
        {
            //int ldDocumentId = -1;
            ImagenesModel docRespuesta = new ImagenesModel();
            ServiceResponse<ImagenesModel> servicioResponse = new ServiceResponse<ImagenesModel>();
            ServicePointManager.ServerCertificateValidationCallback = new
       RemoteCertificateValidationCallback
       (
          delegate { return true; }
       );
            servicioResponse.Data = docRespuesta;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApplicationKeys.APILaserfiche);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(document), Encoding.UTF8, "application/json");

                response = client.PostAsync("api/file/savelist", sc).Result;

                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync();
                    servicioResponse = JsonConvert.DeserializeObject<ServiceResponse<ImagenesModel>>(message.Result);

                    docRespuesta = servicioResponse.Data;
                }

            }

            return docRespuesta;
        }
    }
}

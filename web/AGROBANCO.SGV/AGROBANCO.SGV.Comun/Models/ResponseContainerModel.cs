﻿using System;

namespace AGROBANCO.SGV.Comun.Models
{
    public class ResponseContainerModel
    {
        public ResponseContainerModel() { }

        public string Token { get; set; }
        public DateTime FechaInicioVigencia { get; set; }
        public DateTime FechaFinVigencia { get; set; }
    }
}

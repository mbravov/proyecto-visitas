﻿using AGROBANCO.SGV.Control.Security;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System;
using AGROBANCO.SGV.Control.Constants;

namespace AGROBANCO.SGV.Control.Base
{
    public class BaseControl
    {
        private readonly ITokenControl _tokencontrol;

        public BaseControl()
        {
            _tokencontrol = new TokenControl();
        }



        #region token
        public void ValidarTokenSesion(string token)
        {
            try
            {
                var paramkeytoken = ObtenerValorParametro(ConstantesParametros.TokenClave);
                _tokencontrol.IsTokenJWTValid(paramkeytoken, token);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string ObtenerValorClaimToken(string token, string tipoclaim)
        {
            try
            {
                token = token.Contains("Bearer ") ? token.Replace("Bearer ", "") : token;

                var paramkeytoken = ObtenerValorParametro(ConstantesParametros.TokenClave);

                var valorclaim = _tokencontrol.GetClaimValueByToken(paramkeytoken, tipoclaim, token);

                return valorclaim;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion



        #region parametros

        public string ObtenerValorParametro(string parametro)
        {
            var parametros = ObtenerParametros();

            var valorParametro = parametros.Find(x => x.llave == parametro) == null ? "" : parametros.Find(x => x.llave == parametro).valor;
            return valorParametro;
        }

        private List<ParametrosDto> ObtenerParametros()
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", false)
                   .Build();

                 var responseParametros = new List<ParametrosDto> {
                                                        new ParametrosDto {llave = "TokenClave" , valor = configuration["TokenClave"] },
                                                        new ParametrosDto {llave = "TokenMinutos" , valor = configuration["TokenMinutos"] },
                                                        new ParametrosDto {llave = "AppKey" , valor = configuration["AppKey"] },
                                                        new ParametrosDto {llave = "AppCode" , valor = configuration["AppCode"] },
                };

                return responseParametros;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}

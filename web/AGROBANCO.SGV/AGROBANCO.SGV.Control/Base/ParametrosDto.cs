﻿using System.Collections.Generic;

namespace AGROBANCO.SGV.Control.Base
{
    public class ParametrosDto
    {
        public string llave { get; set; }
        public string valor { get; set; }
    }

    public class ListaParametrosDto
    {
        public List<ParametrosDto> parametros { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class EAsociacionDTO
    {
        public int Codigo { get; set; }
        public string Valor { get; set; }
    }

    public class EAsociacionIntegranteDTO
    {
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public int Resultado { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
    }
}

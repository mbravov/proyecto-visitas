﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class ClienteDTO
    {
        public int? Codigo { get; set; }
        public int? TipoDocumento { get; set; }
        public string Numdocumento { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NombreCompleto { get; set; }
        public int? CodigoAsosiacion { get; set; }
        public string UltimoFiltro { get; set; }
        public int? Estado { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
    }

    public class ClientResponse<T>
    {
        public string Mensaje { get; set; }
        public T Data { get; set; }
        public bool Exito { get; set; }
    }
}

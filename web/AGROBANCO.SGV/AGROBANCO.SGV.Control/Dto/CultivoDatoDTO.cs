﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class CultivoDatoDTO
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}

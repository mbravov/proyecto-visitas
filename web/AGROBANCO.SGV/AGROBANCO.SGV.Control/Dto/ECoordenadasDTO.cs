﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class ECoordenadasDTO
    {
        public int Codigo { get; set; }
        public string CodigoCoordenadaMobile { get; set; }
        public int CodigoVisita { get; set; }
        public int NumOrden { get; set; }
        public string Precision { get; set; }
        public string Longitud { get; set; }
        public string Latitud { get; set; }
        public string Altitud { get; set; }
        //public String usuariocreacion { get; set; }
        //public DateTime? fechacreacion { get; set; }
    }
}

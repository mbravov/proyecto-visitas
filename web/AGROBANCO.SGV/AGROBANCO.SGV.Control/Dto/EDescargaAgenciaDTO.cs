﻿using System;
using System.Collections.Generic;
using System.Text;


namespace AGROBANCO.SGV.Control.Dto
{
    public class EDescargaAgenciaDTO
    {

        public int Codigo { get; set; }
        public string Oficina { get; set; }
        public string OficinaRegional { get; set; }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;


namespace AGROBANCO.SGV.Control.Dto
{
    public class EDescargaFuncionarioDTO
    {
        public string Usuario { get; set; }
        public string Nombre { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class EProfesionalAsignacionDTO
    {
		public int TipoDocumento { get; set; }
		public string NumDocumento { get; set; }
		public string CodigoFuncionario { get; set; }
		public int CodigoProfesional { get; set; }
		public int Estado { get; set; }
		public string CodigoUsuario { get; set; }
		public string? PrimerNombre { get; set; }
		public string? SegundoNombre { get; set; }
		public string? ApellidoPaterno { get; set; }
		public string? ApellidoMaterno { get; set; }
		public string UsuarioWeb { get; set; }
	}
}

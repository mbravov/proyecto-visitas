﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
   public class ETokenVinculacionDTO
    {

        public string IdToken { get; set; }
        public int Estado { get; set; }
        public string Aplicacion { get; set; }


    }
}

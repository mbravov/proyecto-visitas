﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class IMEIDTO
    {
        public string IdentificadorMovil { get; set; }
        public string Usuario { get; set; }
    }
}

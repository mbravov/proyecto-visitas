﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class PredioDTO
    {
        public int? CodigoAgencia { get; set; }
        public int? CodigoAsociacion { get; set; }
        public string? CodigoFuncionario { get; set; }
        public int? Campania { get; set; }
        public string Cultivo { get; set; }
        public string CodigoEstadoCredido { get; set; }
         
        public int? TipoFiltro { get; set; }
        public string? PrimerNombre { get; set; }
        public string? SegundoNombre { get; set; }
        public string? ApellidoPaterno { get; set; }
        public string? ApellidoMaterno { get; set; }
        public string? NombrePredio { get; set; }
        public string? DireccionPredio { get; set; }
        public int? Actividad { get; set; }
        public string? Comentario { get; set; }



        public List<PredioDTO> Lista { get; set; }
        public int TotalRegistros { get; set; }


        public int? codigo { get; set; }
        public string longitud { get; set; }
        public string latitud { get; set; }
        public DateTime? fecha_visita { get; set; }
        public List<EImagenesDatos> ListaImagenes { get; set; }
        public List<ECoordenadasDatos> ListaCoordenadas { get; set; }
    }
}

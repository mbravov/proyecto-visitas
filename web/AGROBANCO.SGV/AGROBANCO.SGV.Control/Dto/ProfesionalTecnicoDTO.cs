﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class ProfesionalTecnicoDTO
    {
   
        public int Codigo { get; set; }

        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
       
        public string PrimerNombre { get; set; }
        
        public string SegundoNombre { get; set; }
        
        public string ApellidoPaterno { get; set; }
       
        public string ApellidoMaterno { get; set; }
      
        public string Celular { get; set; }
       
        public string Email { get; set; }
        
        public int? CodigoAgencia { get; set; }
        public int? CodigoAsociacion { get; set; }

        public short? Estado { get; set; }
        public string UsuarioWeb { get; set; }
        public string NombreAgencia { get; set; }

        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }

        public List<AsignacionAsociacionesDTO> AsignacionAsociaciones { get; set; }
    }

    public class AsignacionAsociacionesDTO
    {
       
        public int Codigo { get; set; }
       
        public int CodigoAsociacion { get; set; }
   
        public int CodigoProfesionalTecnico { get; set; }
      
        public short? Estado { get; set; }
      
        public string UsuarioCreacion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class VisitaDTO
    {
        public int? Codigo { get; set; }
        public string CodigoSolicitudCredito { get; set; }
        public string NumDocumentoCliente { get; set; }
        public DateTime? FechaUltimoFiltro { get; set; }
        public DateTime? FechaUltimoFiltroObtenido { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public int? CodigoAgencia { get; set; }
        public int? CodigoAsignacion { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public string? CodigoFuncionario { get; set; }
        public string? NombreFuncionario { get; set; }
        public int? CodigoProfesionalTecnico { get; set; }
        public int? TipoVisita { get; set; }
        public int? Estado { get; set; }
        public int? Vinculado { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public List<VisitaDTO> Lista { get; set; }
        public int TotalRegistros { get; set; }

        //ASIGNACION
        public int? Asignacion_Estao { get; set; }
        public DateTime? Asignacion_FechaRegistro { get; set; }

        //CLIENTE
        public string ClientePrimerNombre { get; set; }
        public string ClienteSegundoNombre { get; set; }
        public string ClienteApellidoPaterno { get; set; }
        public string ClienteApellidoMaterno { get; set; }

        //PROFESIONAL TECNICO
        public string TecnicoPrimerNombre { get; set; }
        public string TecnicoSegundoNombre { get; set; }
        public string TecnicoApellidoPaterno { get; set; }
        public string TecnicoApellidoMaterno { get; set; }


        public AsignacionDTO EAsignacion { get; set; }
        //Detalle
        public string Cultivo { get; set; }

    }
}

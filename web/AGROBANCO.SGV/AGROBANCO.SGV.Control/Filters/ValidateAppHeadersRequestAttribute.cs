﻿using AGROBANCO.SGV.Control.Constants;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace AGROBANCO.SGV.Control.Filters
{
    public class ValidateAppHeadersRequestAttribute : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
           
            string appkey = context.HttpContext.Request.Headers["X-AppKey"];
            string appcode = context.HttpContext.Request.Headers["X-AppCode"];

            if (string.IsNullOrEmpty(appkey))
            {
                var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_PARAMETRO_CABECERA_REQUIREDO_CODIGO, "Cabecera X-AppKey es requerido.");
                throw new Exception(mensaje);
            }

            if (string.IsNullOrEmpty(appcode))
            {
                var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_PARAMETRO_CABECERA_REQUIREDO_CODIGO, "Cabecera X-AppCode es requerido.");
                throw new Exception(mensaje);
            }

        }
    }
}

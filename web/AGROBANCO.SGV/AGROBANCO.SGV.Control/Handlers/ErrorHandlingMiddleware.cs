﻿using AGROBANCO.SGV.Comun;
using AGROBANCO.SGV.Control.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.Control.Handlers
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {

            try { context.Request.EnableBuffering(); } catch { }

            try
            {
                await next(context);
            }
            catch (Exception ex)
            {

                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var exceptioncontent = ex.Message;
            var arrexcepcion = exceptioncontent.Split("|");
            string exceptionCodigo;
            string exceptionMensaje;

            if (exceptioncontent.Contains(ConstantesToken.ExpiradoCodigoJWT))
            {
                exceptionCodigo = ConstantesError.ERROR_TOKEN_EXPIRADO_CODIGO;
                exceptionMensaje = exceptioncontent;
            }
            else
            {
                exceptionCodigo = arrexcepcion.Length == 1 ? ConstantesError.ERROR_NO_CONTROLADO_CODIGO : arrexcepcion[0];
                exceptionMensaje = arrexcepcion.Length == 1 ? ex.Message : arrexcepcion[1];
            }

            string mensaje = exceptionMensaje;

            ErroresControl.ManejarErrores(exceptionCodigo, out HttpStatusCode httpstatuscode, out string titulo);

            var result = JsonConvert.SerializeObject(new RespuestaError
            {
                error = new RespuestaErrorDetalle
                {
                    codigo = exceptionCodigo,
                    mensaje = mensaje,
                    titulo = titulo
                }
            });

            var mensajeexcepcion = string.Format("Mensaje: {0} , Detalle: {1}", ex.Message, ex.ToString());

            var resultexcepcioncompleta = JsonConvert.SerializeObject(new RespuestaError
            {
                error = new RespuestaErrorDetalle
                {
                    codigo = exceptionCodigo,
                    mensaje = mensajeexcepcion,
                    titulo = titulo
                }
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)httpstatuscode;

            dynamic jsonrquest = null;

            if (context.Request.Body.CanSeek)
            {
                using (var body = new StreamReader(context.Request.Body))
                {
                    body.BaseStream.Seek(0, SeekOrigin.Begin);
                    var requestBody = body.ReadToEnd();
                    jsonrquest = JsonConvert.DeserializeObject(requestBody);
                }
            }



            var request = new
            {
                headers = context.Request.Headers,
                body = jsonrquest
            };

            GrabarLogError(context.Request.Path, JsonConvert.SerializeObject(request), resultexcepcioncompleta);

            return context.Response.WriteAsync(result);
        }

        private static void GrabarLogError(string api, string request, string response)
        {
            //AQUI GRABAMOS LOS LOGS DE ERROR

            GenerarLog objLOG = new GenerarLog();

            string detalleError = "Conexion DB2" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    api + "\n" +
                    request + "\n" +
                    response + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
            objLOG.GenerarArchivoLog(detalleError);
        }

    }
}

﻿using AGROBANCO.SGV.Comun.Models;
using AGROBANCO.SGV.Control.Base;
using AGROBANCO.SGV.Control.Constants;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Security;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class AccessControl : BaseControl, IAccessControl
    {
        private readonly ITokenControl _tokencontrol;

        public string TokenSesion { get; set; }

        public AccessControl()
        {
            _tokencontrol = new TokenControl();
        }

        public AccessDto generateToken(AccessDto accessdto, string appkey, string appcode, ref string token)
        {
            try
            {

                if (string.IsNullOrEmpty(accessdto.CorreoElectronico))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Correo electronico no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(accessdto.NombreUsuario))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Nombre de usuario no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (string.IsNullOrEmpty(accessdto.CodigoUsuario))
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_VALOR_NULO_CODIGO, string.Format("{0}", "Codigo de usuario no puede ser nulo o vacío."));
                    throw new Exception(mensaje);
                }

                if (ObtenerValorParametro("AppKey") != appkey)
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_TOKEN_VALOR_INCORRECTO, string.Format("{0}", "Parametro de validación incorrecto."));
                    throw new Exception(mensaje);
                }

                if (ObtenerValorParametro("AppCode") != appcode)
                {
                    var mensaje = string.Format("{0}|{1}", ConstantesError.ERROR_TOKEN_VALOR_INCORRECTO, string.Format("{0}", "Parametro de validación incorrecto."));
                    throw new Exception(mensaje);
                }


                //VALIDACION DE APPCODE Y APPKEY

                var tokenresponse = GenerarTokenJWT(accessdto);

                token = tokenresponse.Token;
                accessdto.FechaInicioVigencia = tokenresponse.FechaInicioVigencia;
                accessdto.FechaFinVigencia = tokenresponse.FechaFinVigencia;
              
                return accessdto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #region private methods
        private ResponseContainerModel GenerarTokenJWT(AccessDto oaccessdto)
        {
            var paramkeytoken = ObtenerValorParametro(ConstantesParametros.TokenClave);
            var paramtokenminutes = ObtenerValorParametro(ConstantesParametros.TokenMinutos);

            var dicttokenparam = new Dictionary<string, string>
            {
                { ConstantesToken.Key, paramkeytoken },
                { ConstantesToken.Minutes, paramtokenminutes }
            };

            var dictclaims = new Dictionary<string, string>
            {
                { ConstantesUsuario.CodigoUsuario, oaccessdto.CodigoUsuario.ToString() },
                { ConstantesUsuario.CorreoElectronico, oaccessdto.CorreoElectronico },
                { ConstantesUsuario.NombreUsuario, oaccessdto.NombreUsuario },
                { ConstantesUsuario.NumeroDocumento, oaccessdto.NumeroDocumento },
                { ConstantesGenerico.IdentificadorUnico, Guid.NewGuid().ToString() },
            };

            var responseToken = _tokencontrol.GenerateJwtToken(dicttokenparam, dictclaims);

            return responseToken;
        }

        #endregion

    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class AsignacionControl: IAsignacionControl
    {
        private readonly IAsignacionLogica _asignacionLogica;
        public AsignacionControl()
        {
            _asignacionLogica = new AsignacionLogica();
        }

        public bool AsignacionIndividual(EProfesionalAsignacionDTO element)
        {
            try
            {
                var datos = ControlEntidadDtoMapper.Mapper.Map<EProfesionalAsignacionLogica>(element);
                return _asignacionLogica.AsignacionIndividual(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AsignacionGrupal(List<EProfesionalAsignacionDTO> element)
        {
            try
            {
                var datos = ControlEntidadDtoMapper.Mapper.Map<List<EProfesionalAsignacionLogica>>(element);
                return _asignacionLogica.AsignacionGrupal(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AsignarProfesionalClientes(List<EProfesionalAsignacionDTO> lista)
        {
            try
            {
                var datos = ControlEntidadDtoMapper.Mapper.Map<List<EProfesionalAsignacionLogica>>(lista);
                return _asignacionLogica.AsignarProfesionalClientes(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClienteDTO> obtenerClientes()
        {
            try
            {
                var clientes = _asignacionLogica.ObtenerListaClientes();
                return ControlEntidadDtoMapper.Mapper.Map<List<ClienteDTO>>(clientes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EVisitaSinAsignacionDTO> ObtenerVisitasSinAsignacion(string nrodocumento)
        {


            try
            {

                var listaVisita = _asignacionLogica.ObtenerVisitasSinAsignacion(nrodocumento);


                var datos = ControlEntidadDtoMapper.Mapper.Map<List<EVisitaSinAsignacionDTO>>(listaVisita);
                return datos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class CargaVisitaControl : ICargaVisitaControl
    {
        private readonly ICargaVisitaLogica _cargavisitalogica;

        public CargaVisitaControl()
        {
            _cargavisitalogica = new CargaVisitaLogica();
        }

        public bool RegistrarCargaVisita(ECargaVisitaDTO CargaVisitaDTO)
        {
            try
            {
                var objLogica = ControlEntidadDtoMapper.Mapper.Map<ECargaVisitaLogica>(CargaVisitaDTO);
                bool resultado = _cargavisitalogica.RegistrarCargaVisita(objLogica);

                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

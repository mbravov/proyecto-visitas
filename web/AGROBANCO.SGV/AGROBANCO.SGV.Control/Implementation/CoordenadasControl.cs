﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class CoordenadasControl : ICoordenadasControl
    {
        private readonly ICoordenadasLogica _coordenadaslogica;

        public CoordenadasControl()
        {
            _coordenadaslogica = new CoordenadasLogica();
        }

        public bool RegistrarCoordenadas(ECoordenadasDTO coordenadasDTO)
        {
            try
            {
                var objLogica = ControlEntidadDtoMapper.Mapper.Map<ECoordenadasLogica>(coordenadasDTO);
                bool resultado = _coordenadaslogica.RegistrarCoordenadas(objLogica);

                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

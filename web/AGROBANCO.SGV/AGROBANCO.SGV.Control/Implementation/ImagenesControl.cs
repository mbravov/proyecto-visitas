﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class ImagenesControl : IImagenesControl
    {
        private readonly IImagenesLogica _imageneslogica;

        public ImagenesControl()
        {
            _imageneslogica = new ImagenesLogica();
        }

        public bool RegistrarImagenes(EImagenesDTO imagenesDTO)
        {
            try
            {
                var objLogica = ControlEntidadDtoMapper.Mapper.Map<EImagenesLogica>(imagenesDTO);
                bool resultado = _imageneslogica.RegistrarImagenes(objLogica);

                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

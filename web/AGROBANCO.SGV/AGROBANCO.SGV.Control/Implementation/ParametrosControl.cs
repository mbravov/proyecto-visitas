﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class ParametrosControl : IParametrosControl

    {
        private readonly IParametrosLogica _ParametrosLogica;
        public ParametrosControl()
        {
            _ParametrosLogica = new ParametrosLogica();
        }

        public EDescargaMovilDTO DescargaParametrosMovil(String usuario, String imei)
        {
            var parametros = _ParametrosLogica.DescargaParametrosMovil(usuario,imei);
            return ControlEntidadDtoMapper.Mapper.Map<EDescargaMovilDTO>(parametros);
        }

        public List<EDescargaAgenciaDTO> DescargarAgencias()
        {
            var agencias = _ParametrosLogica.DescargarAgencias();
            return ControlEntidadDtoMapper.Mapper.Map<List<EDescargaAgenciaDTO>>(agencias);
        }
        public List<EDescargaFuncionarioDTO> DescargarFuncionarios(int CodAgencia)
        {
            var agencias = _ParametrosLogica.DescargarFuncionarios(CodAgencia);
            return ControlEntidadDtoMapper.Mapper.Map<List<EDescargaFuncionarioDTO>>(agencias);
        }

        public List<ParametroDTO> ObtenerParametros(int CodGrupo)
        {
            var parametros = _ParametrosLogica.ObtenerParametros(CodGrupo);
            return ControlEntidadDtoMapper.Mapper.Map<List<ParametroDTO>>(parametros);
        }

        public List<EAsociacionDTO> ObtenerAsociaciones(int CodAgencia)
        {
            var lista = _ParametrosLogica.ObtenerAsociaciones(CodAgencia);
            return ControlEntidadDtoMapper.Mapper.Map<List<EAsociacionDTO>>(lista);
        }

        public List<EAsociacionIntegranteDTO> ObtenerAsociacionIntegrantes(int CodAsociacion)
        {
            var lista = _ParametrosLogica.ObtenerAsociacionIntegrantes(CodAsociacion);
            return ControlEntidadDtoMapper.Mapper.Map<List<EAsociacionIntegranteDTO>>(lista);
        }
        public List<IMEIDTO> ObtenerIMEI()
        {
            var lista = _ParametrosLogica.ObtenerIMEI();
            return ControlEntidadDtoMapper.Mapper.Map<List<IMEIDTO>>(lista);
        }
        public int ObtenerValidacionVersion(string Version)
        {
            return _ParametrosLogica.ObtenerValidacionVersion(Version);
        }

        public int ResetearIMEI(String usuario)
        {
            return _ParametrosLogica.ResetearIMEI(usuario);
        }

        public List<CultivoDatoDTO> ObtenerCultivos()
        {
            var lista = _ParametrosLogica.ObtenerCultivos();
            return ControlEntidadDtoMapper.Mapper.Map<List<CultivoDatoDTO>>(lista);
        }

        public List<string> ObtenerEstadosCreditos()
        {
            return _ParametrosLogica.ObtenerEstadosCreditos();
        }
    }
}

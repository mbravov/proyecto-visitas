﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class PredioControl : IPredioControl
    {
        private readonly IPredioLogica _prediologica;

        public PredioControl()
        {
            _prediologica = new PredioLogica();
        }

        public PredioDTO ObtenerListaPredio(PredioDTO opredio, int pagina = 0, int pageSize = int.MaxValue)
        {
            try
            {

                var opredioLogica = ControlEntidadDtoMapper.Mapper.Map<EPredioLogica>(opredio);
                var lstprediodto = ControlEntidadDtoMapper.Mapper.Map<List<PredioDTO>>(_prediologica.ListaPredio(opredioLogica));
                var totalRegistros = lstprediodto.Count;

                if (pagina != 0)
                {
                    lstprediodto = lstprediodto.Skip((pagina - 1) * pageSize).Take(pageSize).ToList();
                }

                return new PredioDTO { Lista = lstprediodto, TotalRegistros = totalRegistros };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
   public class VinculacionControl: IVinculacionControl
    {


        private readonly IVinculacionLogica _vinculacionLogica;
        public VinculacionControl()
        {
            _vinculacionLogica = new VinculacionLogica();
        }

        public bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo)
        {
            return _vinculacionLogica.ActualizarTokenVinculacion(codigoToken, estadoNuevo);

        }

        public ETokenVinculacionDTO ObtenerTokenVinculacion(string codigoToken)
        {
            var vinculaciontoken = _vinculacionLogica.ObtenerTokenVinculacion(codigoToken);
            return ControlEntidadDtoMapper.Mapper.Map<ETokenVinculacionDTO>(vinculaciontoken);
        }

        public bool RegistrarTokenVinculacion(string codigotoken, string codigoapp)
        {
            return _vinculacionLogica.RegistrarTokenVinculacion(codigotoken, codigoapp);
        }
    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class VisitaControl : IVisitaControl
    {
        private readonly IVisitaLogica _visitalogica;

        public VisitaControl()
        {
            _visitalogica = new VisitaLogica();
        }

        public VisitaDTO ObtenerListaVisita(VisitaDTO ovisita, int pagina = 0, int pageSize = int.MaxValue)
        {
            try
            {
                var ovisitaLogica = ControlEntidadDtoMapper.Mapper.Map<EVisitaLogica>(ovisita);
                var lstvisitadto = ControlEntidadDtoMapper.Mapper.Map<List<VisitaDTO>>(_visitalogica.ListaVisita(ovisitaLogica));
                var totalRegistros = lstvisitadto.Count;

                if (pagina != 0)
                {
                    lstvisitadto = lstvisitadto.Skip((pagina - 1) * pageSize).Take(pageSize).ToList();
                }

                return new VisitaDTO { Lista = lstvisitadto, TotalRegistros = totalRegistros };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public VisitaDTO ObtenerListaVincularVisita(string documento)
        {
            try
            {
                var lstvisitadto = ControlEntidadDtoMapper.Mapper.Map<List<VisitaDTO>>(_visitalogica.ObtenerListaVincularVisita(documento));
                var totalRegistros = lstvisitadto.Count;

                return new VisitaDTO { Lista = lstvisitadto, TotalRegistros = totalRegistros };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional)
        {
            try
            {
                return _visitalogica.ReasignarVisitaPT(CodigoVisita, CodigoProfesional);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ReasignarMasivo(int anterior, int nuevo)
        {
            try
            {
                return _visitalogica.ReasignarMasivo(anterior, nuevo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Anulacion(int asignacion)
        {
            try
            {
                return _visitalogica.Anulacion(asignacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Aprobacion(int codigovisita, int estadovisita, string comentario)
        {
            try
            {
                return _visitalogica.Aprobacion(codigovisita, estadovisita, comentario);
            } 
            catch(Exception e)
            {
                throw e;
            }
        }
        public bool AsignarSolicitudCredito(int codvisita, string solicitudcredito)
        {
            try
            {
                return _visitalogica.AsignarSolicitudCredito(codvisita, solicitudcredito);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

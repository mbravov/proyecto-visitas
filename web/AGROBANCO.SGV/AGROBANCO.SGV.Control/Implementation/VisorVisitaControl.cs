﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class VisorVisitaControl : IVisorVisitaControl
    {


        private readonly IVisorVisitaLogica _VisorVisitaLogica;
        public VisorVisitaControl()
        {
            _VisorVisitaLogica = new VisorVisitaLogica();
        }


        public EVisorVisitaDTO ObtenerDatosVisita(int CodVisita)
        {
            var visor = _VisorVisitaLogica.ObtenerDatosVisita(CodVisita);
            return ControlEntidadDtoMapper.Mapper.Map<EVisorVisitaDTO>(visor);
        }
    }
}

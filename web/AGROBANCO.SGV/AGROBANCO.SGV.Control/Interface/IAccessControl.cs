﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IAccessControl
    {
        AccessDto generateToken(AccessDto usuariodto, string appkey, string appcode, ref string token);
    }
}

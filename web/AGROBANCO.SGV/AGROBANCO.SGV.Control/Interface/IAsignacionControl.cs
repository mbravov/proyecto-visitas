﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IAsignacionControl
    {
        List<ClienteDTO> obtenerClientes();
        bool AsignarProfesionalClientes(List<EProfesionalAsignacionDTO> lista);
        bool AsignacionIndividual(EProfesionalAsignacionDTO element);
        bool AsignacionGrupal(List<EProfesionalAsignacionDTO> element);
        List<EVisitaSinAsignacionDTO> ObtenerVisitasSinAsignacion(String nrodocumento);
    }
}

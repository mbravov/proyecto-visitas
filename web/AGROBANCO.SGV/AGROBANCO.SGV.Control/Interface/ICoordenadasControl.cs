﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface ICoordenadasControl
    {
        bool RegistrarCoordenadas(ECoordenadasDTO coordenadasDTO);
    }
}

﻿
using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;

namespace AGROBANCO.SGV.Control.Implementation
{
    public interface IParametrosControl
    {
        EDescargaMovilDTO DescargaParametrosMovil(String usuario, String imei);
        List<EDescargaAgenciaDTO> DescargarAgencias();
        List<EDescargaFuncionarioDTO> DescargarFuncionarios(int CodAgencia);
        List<ParametroDTO> ObtenerParametros(int CodGrupo);
        List<EAsociacionDTO> ObtenerAsociaciones(int CodAgencia);
        List<EAsociacionIntegranteDTO> ObtenerAsociacionIntegrantes(int CodAsociacion);
        List<IMEIDTO> ObtenerIMEI();
        int ResetearIMEI(String usuario);
        int ObtenerValidacionVersion(String Version);
        List<CultivoDatoDTO> ObtenerCultivos();
        List<String> ObtenerEstadosCreditos();
    }
}
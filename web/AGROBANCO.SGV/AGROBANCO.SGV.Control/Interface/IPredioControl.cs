﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IPredioControl
    {
        PredioDTO ObtenerListaPredio(PredioDTO opredio, int pagina = 0, int pageSize = int.MaxValue);
    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IProfesionalTecnicoControl
    {

        List<ProfesionalTecnicoDTO> ListaProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico);

        ProfesionalTecnicoListaDTO ObtenerListaProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico, int pagina = 0, int pageSize = int.MaxValue);

        bool RegistrarProfesionalTecnico(ProfesionalTecnicoDTO profesionalTecnicoDTO);

        bool ActualizarProfesionalTecnico(ProfesionalTecnicoDTO profesionalTecnicoDTO);

        ProfesionalTecnicoDTO ObtenerProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico);

    }
}

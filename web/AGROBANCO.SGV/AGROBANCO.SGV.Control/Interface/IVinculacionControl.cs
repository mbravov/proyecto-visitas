﻿using System;
using System.Collections.Generic;
using System.Text;
using AGROBANCO.SGV.Control.Dto;

namespace AGROBANCO.SGV.Control.Interface
{
   public interface IVinculacionControl
    {
        bool RegistrarTokenVinculacion(string codigotoken, string codigoapp);
        ETokenVinculacionDTO ObtenerTokenVinculacion(string codigoToken);
        bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo);

    }
}

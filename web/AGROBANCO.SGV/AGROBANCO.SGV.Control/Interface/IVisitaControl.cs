﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IVisitaControl
    {
        VisitaDTO ObtenerListaVisita(VisitaDTO ovisita, int pagina = 0, int pageSize = int.MaxValue);
        bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional);
        bool ReasignarMasivo(int anterior, int nuevo);
        bool Anulacion(int asignacion);
        bool Aprobacion(int codigovisita, int estadovisita, string comentario);
        VisitaDTO ObtenerListaVincularVisita(string documento);
        bool AsignarSolicitudCredito(int codvisita, string solicitudcredito);
    }
}

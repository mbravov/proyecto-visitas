﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface IVisorVisitaControl
    {
        EVisorVisitaDTO ObtenerDatosVisita(int CodVisita);



    }
}

﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AutoMapper;
using System;

namespace AGROBANCO.SGV.Control.Mapper
{
    public static class ControlEntidadDtoMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EProfesionalTecnicoLogica, ProfesionalTecnicoDTO>();
            CreateMap<ProfesionalTecnicoDTO, EProfesionalTecnicoLogica>();
            CreateMap<EDescargaAsignacionLogica, DescargaAsignacionDTO>().ReverseMap();
            CreateMap<EDescargaAgenciaLogica, EDescargaAgenciaDTO>().ReverseMap();
            CreateMap<EDescargaFuncionariosLogica, EDescargaFuncionarioDTO>().ReverseMap();
            CreateMap<EParametroLogica, ParametroDTO>().ReverseMap();
            CreateMap<ETokenVinculacionLogica, ETokenVinculacionDTO>().ReverseMap();
            CreateMap<EIMEILogica, IMEIDTO>().ReverseMap();
            CreateMap<EDescargaMovilLogica, EDescargaMovilDTO>().ReverseMap();
            CreateMap<DescargaCultivoLogica, DescargaCultivoDTO>().ReverseMap();
            CreateMap<DescargaMedidaLogica, DescargaMedidaDTO>().ReverseMap();
            CreateMap<DescargaCrianzaLogica, DescargaCrianzaDTO>().ReverseMap();
            CreateMap<DescargaUnidadFinanciarLogica, DescargaUnidadFinanciarDTO>().ReverseMap();
     

            CreateMap<EVisitaLogica, VisitaDTO>();
            CreateMap<VisitaDTO, EVisitaLogica>();

            CreateMap<EClienteLogica, ClienteDTO>();
            CreateMap<ClienteDTO, EClienteLogica>();

            CreateMap<EAsignacionLogica, AsignacionDTO>();
            CreateMap<AsignacionDTO, EAsignacionLogica>();

            CreateMap<ECargaVisitaLogica, ECargaVisitaDTO>();
            CreateMap<ECargaVisitaDTO, ECargaVisitaLogica>();

            CreateMap<ECoordenadasLogica, ECoordenadasDTO>();
            CreateMap<ECoordenadasDTO, ECoordenadasLogica>();

            CreateMap<EImagenesLogica, EImagenesDTO>();
            CreateMap<EImagenesDTO, EImagenesLogica>();

            CreateMap<EVisorVisitaLogica, EVisorVisitaDTO>();
            CreateMap<EVisorVisitaDTO, EVisorVisitaLogica>();

            CreateMap<EAsociacionDTO, EAsociacionLogica>();
            CreateMap<EAsociacionLogica, EAsociacionDTO>();
            CreateMap<EAsociacionIntegranteDTO, EAsociacionIntegranteLogica>();
            CreateMap<EAsociacionIntegranteLogica, EAsociacionIntegranteDTO>();

            CreateMap<EProfesionalAsignacionLogica, EProfesionalAsignacionDTO>();
            CreateMap<EProfesionalAsignacionDTO, EProfesionalAsignacionLogica>();

            CreateMap<PredioDTO, EPredioLogica>();
            CreateMap<EPredioLogica, PredioDTO>();
            CreateMap<EVisitaSinAsignacionLogica, EVisitaSinAsignacionDTO>();
            CreateMap<EVisitaSinAsignacionDTO, EVisitaSinAsignacionLogica>();

            CreateMap<CultivoDatoDTO, ECultivoLogica>();
            CreateMap<ECultivoLogica, CultivoDatoDTO>();

        }
    }


}

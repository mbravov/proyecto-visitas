﻿using System;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class FuncionGenerico
    {

        static public string ToStringYYYYmmDD(DateTime fechaConsulta)
        {
            var fecha = ToStringYYYYmm(fechaConsulta) + ToStringDD(fechaConsulta);
            return fecha;
        }

        static public string ToStringYYYY(DateTime fechaconuslta)
        {
            string val = Right("0000" + fechaconuslta.Year.ToString().Trim(), 4);
            return val;
        }

        static public string ToStringMM(DateTime fechaconsulta)
        {
            string val = Right("00" + fechaconsulta.Month.ToString().Trim(), 2);
            return val;
        }

        static public string ToStringDD(DateTime fechaconsulta)
        {
            string val = Right("00" + fechaconsulta.Day.ToString().Trim(), 2);
            return val;
        }

        static public string ToStringYYYYmm(DateTime fechaconsulta, string separador = "")
        {
            var val = ToStringYYYY(fechaconsulta) + separador + ToStringMM(fechaconsulta);
            return val;
        }

        static public string Right(string cadena, int length)
        {
            if (string.IsNullOrEmpty(cadena) || length < 1)
                return "";

            int n = cadena.Length;

            if (length > n)
            {
                length = n;
            }
            return cadena.Substring(n - length, length);
        }

        static public string EsCadenaNula<T>(T dato, string valor)
        {
            string val = valor;
            if (dato != null)
            {
                if (dato.GetType().FullName != "System.DBNull")
                {
                    val = dato.ToString();
                }
            }
            return val;
        }

        static public Int32 EsNuloInt32<T>(T dato, Int32 valor)
        {
            Int32 valRet = valor;

            if (dato != null)
            {
                string strDato = dato.ToString();
                if (strDato == "")
                {
                    valRet = 0;
                }
                else
                {
                    Int32 posDec = strDato.IndexOf(".");

                    if (posDec >= 0)
                    {
                        strDato = strDato.Substring(0, posDec);
                    }

                    Int32.TryParse(strDato, out valRet);
                }
            }
            return valRet;
        }

        static public Int16 EsNuloInt16<T>(T dato, Int16 valor)
        {
            Int16 val = valor;

            if (dato != null)
            {
                string strdato = dato.ToString();
                if (strdato == string.Empty)
                {
                    val = 0;
                }
                else
                {
                    Int16.TryParse(strdato, out val);
                }
            }
            return val;
        }

        static public Decimal EsNuloDecimal<T>(T dato, Decimal valor)
        {
            Decimal val = valor;

            if (dato != null)
            {
                string strDato = dato.ToString();
                if (strDato == "")
                {
                    val = 0;
                }
                else
                {
                    Decimal.TryParse(strDato, out val);
                }
            }
            return val;
        }
        static public DateTime EsFechaNula<T>(T dato, DateTime valor)
        {
            DateTime val = valor;

            if (dato != null)
            {
                if (dato.GetType().FullName != "System.DBNull")
                {
                    val = Convert.ToDateTime(dato);
                }
            }
            return val;
        }
        static public string DeByteAString(Byte[] asciiBytes)
        {
            return Encoding.UTF8.GetString(asciiBytes);
        }

        static public Byte[] DeStringAByte(string cadena)
        {
            return Encoding.UTF8.GetBytes(cadena);
        }

        static public string DeByteABase64(Byte[] asciiBytes)
        {
            return Convert.ToBase64String(asciiBytes, 0, asciiBytes.Length);
        }




    }
}

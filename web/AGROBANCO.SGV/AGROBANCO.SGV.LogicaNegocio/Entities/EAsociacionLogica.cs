﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{

    public class EAsociacionLogica
    {
        public int Codigo { get; set; }
        public string Valor { get; set; }
    }

    public class EAsociacionIntegranteLogica
    {
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public int Resultado { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
    }
}

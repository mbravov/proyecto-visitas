﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class ECoordenadasLogica
    {
        public int Codigo { get; set; }
        public string CodigoCoordenadaMobile { get; set; }
        public int CodigoVisita { get; set; }
        public int NumOrden { get; set; }
        public string Precision { get; set; }
        public string Longitud { get; set; }
        public string Latitud { get; set; }
        public string Altitud { get; set; }
    }
}

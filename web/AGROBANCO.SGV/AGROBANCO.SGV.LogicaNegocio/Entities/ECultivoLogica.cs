﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class ECultivoLogica
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}

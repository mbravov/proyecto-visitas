﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
  

    public class EDescargaMovilLogica
    {
        public int DiasEliminar { get; set; }
        public int Precision { get; set; }
        public decimal Hectareas { get; set; }
        public int Dispositivo { get; set; }

        public List<EDescargaAgenciaLogica> ListaAgencias { get; set; }
        public List<DescargaCultivoLogica> ListaCultivo { get; set; }
        public List<DescargaMedidaLogica> ListaMedida { get; set; }
        public List<DescargaCrianzaLogica> ListaCrianza { get; set; }
        public List<DescargaUnidadFinanciarLogica> ListaUnidadFinanciar { get; set; }

    }

    public class DescargaCultivoLogica
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }

    }
    public class DescargaMedidaLogica
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaCrianzaLogica
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaUnidadFinanciarLogica
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }




}

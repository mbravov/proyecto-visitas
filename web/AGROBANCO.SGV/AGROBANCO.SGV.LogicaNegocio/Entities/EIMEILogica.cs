﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{

    public class EIMEILogica
    {
        public string IdentificadorMovil { get; set; }
        public string Usuario { get; set; }
    }

}

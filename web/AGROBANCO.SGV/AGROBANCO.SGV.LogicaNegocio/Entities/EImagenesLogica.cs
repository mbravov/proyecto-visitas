﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class EImagenesLogica
    {
        public int Codigo { get; set; }
        public int CodigoVisita { get; set; }
        public string CodigoFotoMobile { get; set; }
        //public int codigofotolas { get; set; }
        public int NumOrdenCaptura { get; set; }
        public string LongCapturaImagen { get; set; }
        public string LatCapturaImagen { get; set; }
        public string AltitudCapturaImagen { get; set; }
        public string Precision { get; set; }
        public int TipoImagen { get; set; }
        public int TipoBien { get; set; }
        public string ArchivoByte { get; set; }
        public string NombreArchivo { get; set; }
        public string ExtensionArchivo { get; set; }
        public string UsuarioCreacion { get; set; }
    }
}

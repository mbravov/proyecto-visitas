﻿using System;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class EParametroLogica
    {
        public int CodigoParametro { get; set; }
        public int CodigoGrupo { get; set; }
        public int CodigoInterno { get; set; }
        public string NombreParametro { get; set; }
        public string NombreCorto { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public int Orden { get; set; }
        public int Estado { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}

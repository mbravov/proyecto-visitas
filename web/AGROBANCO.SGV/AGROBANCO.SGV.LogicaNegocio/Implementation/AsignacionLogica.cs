﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class AsignacionLogica: IAsignacionLogica
    {

        private readonly IAsignacionDatos _asignacionDatos;

        public AsignacionLogica()
        {
            _asignacionDatos = new AsignacionDatos();
        }
        public bool AsignacionIndividual(EProfesionalAsignacionLogica element)
        {
            var data = LogicaEntidadesMapper.Mapper.Map<EProfesionalAsignacionDatos>(element);
            return _asignacionDatos.AsignacionIndividual(data);
        }
        public bool AsignacionGrupal(List<EProfesionalAsignacionLogica> element)
        {
            var data = LogicaEntidadesMapper.Mapper.Map<List<EProfesionalAsignacionDatos>>(element);
            return _asignacionDatos.AsignacionGrupal(data);
        }
        public bool AsignarProfesionalClientes(List<EProfesionalAsignacionLogica> lista)
        {
            var datos = LogicaEntidadesMapper.Mapper.Map<List<EProfesionalAsignacionDatos>>(lista);
            bool resultado = _asignacionDatos.AsignarProfesionalClientes(datos);
            return resultado;
        }
        public List<EClienteLogica> ObtenerListaClientes()
        {
            var clienteDatos = _asignacionDatos.ObtenerListaClientes();
            return LogicaEntidadesMapper.Mapper.Map<List<EClienteLogica>>(clienteDatos);
        }

        public List<EVisitaSinAsignacionLogica> ObtenerVisitasSinAsignacion(string nrodocumento)
        {

            var listaVisita = _asignacionDatos.ObtenerVisitasSinAsignacion(nrodocumento);

            return LogicaEntidadesMapper.Mapper.Map<List<EVisitaSinAsignacionLogica>>(listaVisita);

        }
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;


namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class CargaVisitaLogica : ICargaVisitaLogica
    {
        private readonly ICargaVisitaDatos _ICargaVisitaDatos;

        public CargaVisitaLogica()
        {
            _ICargaVisitaDatos = new CargaVisitaDatos();
        }

        public bool RegistrarCargaVisita(ECargaVisitaLogica cargavisita)
        {
            var ocrgavisitadatos = LogicaEntidadesMapper.Mapper.Map<ECargaVisitaDatos>(cargavisita);

            bool resultado = _ICargaVisitaDatos.RegistrarCargaVisita(ocrgavisitadatos);

            return resultado;
        }
    }
}

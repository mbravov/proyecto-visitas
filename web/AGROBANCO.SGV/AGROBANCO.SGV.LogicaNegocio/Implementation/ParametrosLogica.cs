﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class ParametrosLogica : IParametrosLogica
    {
        private readonly IParametrosDatos _ParametrosDatos;
        public ParametrosLogica()
        {
            _ParametrosDatos = new ParametrosDatos();
        }

        public EDescargaMovilLogica DescargaParametrosMovil(String usuario, String imei)
        {
            var parametrosMovil = _ParametrosDatos.DescargaParametrosMovil(usuario,imei);
            return LogicaEntidadesMapper.Mapper.Map<EDescargaMovilLogica>(parametrosMovil);
        }

        public List<EDescargaAgenciaLogica> DescargarAgencias() 
        {
            var listaAgencias = _ParametrosDatos.DescargaAgencias();
            return LogicaEntidadesMapper.Mapper.Map<List<EDescargaAgenciaLogica>>(listaAgencias);
        }
        public List<EDescargaFuncionariosLogica> DescargarFuncionarios(int CodAgencia)
        {
            var listaFuncionarios = _ParametrosDatos.DescargaFuncionarios(CodAgencia);
            return LogicaEntidadesMapper.Mapper.Map<List<EDescargaFuncionariosLogica>>(listaFuncionarios);
        }

        public List<EParametroLogica> ObtenerParametros(int CodGrupo)
        {
            var listaParametros = _ParametrosDatos.ObtenerParametros(CodGrupo);
            return LogicaEntidadesMapper.Mapper.Map<List<EParametroLogica>>(listaParametros);
        }

        public List<EAsociacionLogica> ObtenerAsociaciones(int CodAgencia)
        {
            var lista = _ParametrosDatos.ObtenerAsociaciones(CodAgencia);
            return LogicaEntidadesMapper.Mapper.Map<List<EAsociacionLogica>>(lista);
        }

        public List<EAsociacionIntegranteLogica> ObtenerAsociacionIntegrantes(int CodAsociacion)
        {
            var lista = _ParametrosDatos.ObtenerAsociacionIntegrantes(CodAsociacion);
            return LogicaEntidadesMapper.Mapper.Map<List<EAsociacionIntegranteLogica>>(lista);
        }

        public List<EIMEILogica> ObtenerIMEI()
        {
            var lista = _ParametrosDatos.ObtenerIMEI();
            return LogicaEntidadesMapper.Mapper.Map<List<EIMEILogica>>(lista);
        }
        public int ObtenerValidacionVersion(string Version)
        {
            return _ParametrosDatos.ObtenerValidacionVersion(Version);
        }

        public int ResetearIMEI(string usuario)
        {
            return _ParametrosDatos.ResetearIMEI(usuario);
        }

        public List<ECultivoLogica> ObtenerCultivos()
        {
            var lista = _ParametrosDatos.ObtenerCultivos();
            return LogicaEntidadesMapper.Mapper.Map<List<ECultivoLogica>>(lista);
        }

        public List<string> ObtenerEstadosCreditos()
        {
            return _ParametrosDatos.ObtenerEstadosCreditos();
        }
    }
}

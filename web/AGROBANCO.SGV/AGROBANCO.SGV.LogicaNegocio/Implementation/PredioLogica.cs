﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class PredioLogica : IPredioLogica
    {
        private readonly IPredioDatos _IPrevioDatos;

        public PredioLogica()
        {
            _IPrevioDatos = new PredioDatos();
        }
        public List<EPredioLogica> ListaPredio(EPredioLogica predio)
        {
            var ovisitadatos = LogicaEntidadesMapper.Mapper.Map<EPredioDatos>(predio);
            var lstprediodatos = _IPrevioDatos.ObtenerListaPredio(ovisitadatos);

            return LogicaEntidadesMapper.Mapper.Map<List<EPredioLogica>>(lstprediodatos);
        }
    }
}

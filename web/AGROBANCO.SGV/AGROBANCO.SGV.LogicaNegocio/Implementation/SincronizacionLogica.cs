﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class SincronizacionLogica : ISincronizacionLogica
    {
        private readonly ISincronizacionDatos _sincronizacionDatos;
        public SincronizacionLogica()
        {
            _sincronizacionDatos = new SincronizacionDatos();
        }

        public List<EDescargaAsignacionLogica> DescargarAsignaciones(EDescargaAsignacionLogica filtro) 
        {
            var filtroDatos = LogicaEntidadesMapper.Mapper.Map<DescargaAsignacion>(filtro);
            var listaAsignaciones = _sincronizacionDatos.DescargarAsignaciones(filtroDatos);

            return LogicaEntidadesMapper.Mapper.Map<List<EDescargaAsignacionLogica>>(listaAsignaciones);
        }

    }
}

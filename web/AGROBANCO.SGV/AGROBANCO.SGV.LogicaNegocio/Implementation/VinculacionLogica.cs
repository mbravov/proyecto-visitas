﻿using System;
using System.Collections.Generic;
using System.Text;
using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class VinculacionLogica : IVinculacionLogica
    {

         private readonly IVinculacion _vinculacionDatos;
        public VinculacionLogica()
        {
            _vinculacionDatos = new VinculacionDatos();
        }


        public bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo)
        {
            return _vinculacionDatos.ActualizarTokenVinculacion(codigoToken, estadoNuevo);
        }

        public ETokenVinculacionLogica ObtenerTokenVinculacion(string codigoToken)
        {
            var vinculaDatos = _vinculacionDatos.ObtenerTokenVinculacion(codigoToken);
            return LogicaEntidadesMapper.Mapper.Map<ETokenVinculacionLogica>(vinculaDatos);
        }

        public bool RegistrarTokenVinculacion(string codigotoken, string codigoapp)
        {
            return _vinculacionDatos.RegistrarTokenVinculacion(codigotoken, codigoapp);

        }
    }
}

﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class VisitaLogica : IVisitaLogica
    {

        private readonly IVisitaDatos _IVisitaDatos;

        public VisitaLogica()
        {
            _IVisitaDatos = new VisitaDatos();
        }


        public List<EVisitaLogica> ListaVisita(EVisitaLogica visita)
        {
            var ovisitadatos = LogicaEntidadesMapper.Mapper.Map<EVisitaDatos>(visita);
            var lstvisitadatos = _IVisitaDatos.ObtenerListavisita(ovisitadatos);

            return LogicaEntidadesMapper.Mapper.Map<List<EVisitaLogica>>(lstvisitadatos);
        }

        public List<EVisitaLogica> ObtenerListaVincularVisita(string documento)
        {

            var lstvisitadatos = _IVisitaDatos.ObtenerListaVincularVisita(documento);

            return LogicaEntidadesMapper.Mapper.Map<List<EVisitaLogica>>(lstvisitadatos);
        }
        
        public bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional)
        {
            return _IVisitaDatos.ReasignarVisitaPT(CodigoVisita, CodigoProfesional);
        }

        public bool ReasignarMasivo(int anterior, int nuevo)
        {
            return _IVisitaDatos.ReasignarMasivo(anterior, nuevo);
        }

        public bool Anulacion(int asignacion)
        {
            return _IVisitaDatos.Anulacion(asignacion);
        }

        public bool Aprobacion(int codigovisita, int estadovisita, string comentario)
        {
            return _IVisitaDatos.Aprobacion(codigovisita, estadovisita, comentario);
        }
        public bool AsignarSolicitudCredito(int codvisita, string solicitudcredito)
        {
            return _IVisitaDatos.AsignarSolicitudCredito(codvisita, solicitudcredito);
        }
    }
}

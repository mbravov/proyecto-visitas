﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;
namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class VisorVisitaLogica : IVisorVisitaLogica
    {

        private readonly IVisorVisitaDatos _IVisorVisitaDatos;

        public VisorVisitaLogica()
        {
            _IVisorVisitaDatos = new VisorVisitaDatos();
        }
        public EVisorVisitaLogica ObtenerDatosVisita(int CodVisita)
        {
            var visor = _IVisorVisitaDatos.ObtenerDatosVisita(CodVisita);
            return LogicaEntidadesMapper.Mapper.Map<EVisorVisitaLogica>(visor);
        }
    }
}

﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IAsignacionLogica
    {
        List<EClienteLogica> ObtenerListaClientes();
        bool AsignarProfesionalClientes(List<EProfesionalAsignacionLogica> lista);
        bool AsignacionIndividual(EProfesionalAsignacionLogica element);
        bool AsignacionGrupal(List<EProfesionalAsignacionLogica> element);
        List<EVisitaSinAsignacionLogica> ObtenerVisitasSinAsignacion(String nrodocumento);
    }
}

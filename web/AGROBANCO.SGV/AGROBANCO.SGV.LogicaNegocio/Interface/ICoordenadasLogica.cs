﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface ICoordenadasLogica
    {
        bool RegistrarCoordenadas(ECoordenadasLogica oCoordenadas);
    }
}

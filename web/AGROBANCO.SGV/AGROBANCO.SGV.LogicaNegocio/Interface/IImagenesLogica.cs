﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IImagenesLogica
    {
        bool RegistrarImagenes(EImagenesLogica oImagenes);
    }
}

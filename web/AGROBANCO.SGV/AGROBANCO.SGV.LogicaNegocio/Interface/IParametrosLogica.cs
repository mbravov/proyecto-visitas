﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IParametrosLogica
    {
        EDescargaMovilLogica DescargaParametrosMovil(String usuario, String imei);
        List<EDescargaAgenciaLogica> DescargarAgencias();
        List<ECultivoLogica> ObtenerCultivos();
        List<EDescargaFuncionariosLogica> DescargarFuncionarios(int CodAgencia);
        List<EParametroLogica> ObtenerParametros(int CodGrupo);
        List<EAsociacionLogica> ObtenerAsociaciones(int CodAgencia);
        List<EAsociacionIntegranteLogica> ObtenerAsociacionIntegrantes(int CodAsociacion);
        List<EIMEILogica> ObtenerIMEI();
        int ObtenerValidacionVersion(String Version);
        int ResetearIMEI(String usuario);
        List<String> ObtenerEstadosCreditos();
    }
}

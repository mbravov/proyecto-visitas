﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IPredioLogica
    {
        List<EPredioLogica> ListaPredio(EPredioLogica predio);
    }
}

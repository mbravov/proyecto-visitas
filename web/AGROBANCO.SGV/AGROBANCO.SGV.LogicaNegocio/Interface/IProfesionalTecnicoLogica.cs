﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IProfesionalTecnicoLogica
    {
        List<EProfesionalTecnicoLogica> ListaProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico);
        List<EProfesionalTecnicoLogica> ObtenerListaProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico);

        bool RegistrarProfesionalTecnico(EProfesionalTecnicoLogica oProfesionalTecnico);
        bool ActualizarProfesionalTecnico(EProfesionalTecnicoLogica oProfesionalTecnico);
        EProfesionalTecnicoLogica ObtenerProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico);
    }
}

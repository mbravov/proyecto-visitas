﻿using System;
using System.Collections.Generic;
using System.Text;
using AGROBANCO.SGV.LogicaNegocio.Entities;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
   public interface IVinculacionLogica
    {
        bool RegistrarTokenVinculacion(string codigotoken, string codigoapp);
        ETokenVinculacionLogica ObtenerTokenVinculacion(string codigoToken);
        bool ActualizarTokenVinculacion(string codigoToken, int estadoNuevo);
    }
}

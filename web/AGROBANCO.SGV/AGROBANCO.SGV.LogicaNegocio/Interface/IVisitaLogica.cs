﻿using AGROBANCO.SGV.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IVisitaLogica
    {
        List<EVisitaLogica> ListaVisita(EVisitaLogica visita);
        bool ReasignarVisitaPT(int CodigoVisita, int CodigoProfesional);
        bool ReasignarMasivo(int anterior, int nuevo);
        bool Anulacion(int asignacion);
        bool Aprobacion(int codigovisita, int estadovisita, string comentario);
        bool AsignarSolicitudCredito(int codvisita, string solicitudcredito);
        List<EVisitaLogica> ObtenerListaVincularVisita(string documento);
    }
}

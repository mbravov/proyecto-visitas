﻿using System;
using System.Collections.Generic;
using System.Text;
using AGROBANCO.SGV.LogicaNegocio.Entities;
namespace AGROBANCO.SGV.LogicaNegocio.Interface
{
    public interface IVisorVisitaLogica
    {

        EVisorVisitaLogica ObtenerDatosVisita(int CodVisita);
    }
}

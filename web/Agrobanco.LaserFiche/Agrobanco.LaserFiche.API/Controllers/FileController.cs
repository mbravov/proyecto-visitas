﻿using Agrobanco.LaserFiche.API.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Agrobanco.LaserFiche.API.Controllers
{
    
    [RoutePrefix("api/file")]
    public class FileController : ApiController
    {
        [HttpPost]
        [Route("save")]
        public Respuesta SaveFile(DocumentModel document)
        {
            string outFileName = string.Empty;
            Respuesta respuesta = new Respuesta();
            DocumentModel docRespuesta = new DocumentModel();

            try
            {
                if (!string.IsNullOrEmpty(document.ArchivoBytes))
                {
                    byte[] archivo = Convert.FromBase64String(document.ArchivoBytes);

                    MemoryStream file = new MemoryStream(archivo);

                    LaserficheManager laserfiche = new LaserficheManager();

                    docRespuesta.CodigoLaserfiche = laserfiche.CargarDocumentoLaserFiche(file, document.Extension, document.Nombre, document.Folder, out outFileName);
                    docRespuesta.NombreDocumento = outFileName;

                    respuesta.Resultado = TypesConstants.Success;
                    respuesta.Data = docRespuesta;

                    return respuesta;
                }
            }
            catch (Exception ex)
            {
                respuesta.Resultado = TypesConstants.Error;
                respuesta.Mensaje = "Error Msg: " + ex.Message + Environment.NewLine + " StackTrace: " + ex.StackTrace;
            }

            return respuesta;
        }


        [HttpPost]
        [Route("savelist")]
        public  Respuesta SaveListImages(ImagenesModel document)
        {
            string outFileName = string.Empty;
            Respuesta respuesta = new Respuesta();
            ImagenesModel docRespuesta = new ImagenesModel();

            try
            {
                if (document.ListaImagenes.Count > 0) {


                    LaserficheManager laserfiche = new LaserficheManager();

                     docRespuesta =  laserfiche.ObtenerResultadoImportarcionImagenes(document.ListaImagenes, document.Folder.ToString());
                    if (docRespuesta.ResultadoOK)
                    {
                        respuesta.Resultado = TypesConstants.Success;
                        respuesta.Data = docRespuesta;
                    }
                    else
                    {
                        respuesta.Resultado = TypesConstants.Error;
                        respuesta.Mensaje = docRespuesta.ResultadoMensaje;
                        respuesta.Data = docRespuesta;
                    }
                    return respuesta;
                }



          
            }
            catch (Exception ex)
            {
                respuesta.Resultado = TypesConstants.Error;
                respuesta.Mensaje = "Error Msg: " + ex.Message + Environment.NewLine + " StackTrace: " + ex.StackTrace;
            }

            return respuesta;
        }


        [HttpPost]
        [Route("get")]
        public Respuesta GetFile(DocumentModel document)
        {
            Respuesta respuesta = new Respuesta();
            DocumentModel docRespuesta = new DocumentModel();
            LaserficheManager laserfiche = new LaserficheManager();

            try
            {
                if (document.CodigoLaserfiche > 0)
                {
                    string nombreDocumento = string.Empty;
                    string extension = string.Empty;
                    string strbase64File = laserfiche.ConsultaArchivoSimple(document.CodigoLaserfiche, out nombreDocumento, out extension);

                    docRespuesta.NombreDocumento = nombreDocumento;
                    docRespuesta.Extension = extension;
                    docRespuesta.ArchivoBytes = strbase64File;
                    docRespuesta.ContentType = TypesConstants.GetTypeMIME(extension);
                }

                respuesta.Resultado = TypesConstants.Success;
                respuesta.Data = docRespuesta;
            }
            catch (Exception ex)
            {
                respuesta.Resultado = TypesConstants.Error;
                respuesta.Mensaje = "Error Msg: " + ex.Message + Environment.NewLine + " StackTrace: " + ex.StackTrace;
            }

            return respuesta;
        }

        [HttpPost]
        [Route("getImage")]
        public Respuesta GetImage(DocumentModel document)
        {
            Respuesta respuesta = new Respuesta();
            DocumentModel docRespuesta = new DocumentModel();
            LaserficheManager laserfiche = new LaserficheManager();

            try
            {
                if (document.CodigoLaserfiche > 0)
                {
                    string nombreDocumento = string.Empty;
                    string extension = string.Empty;
                    string strbase64File = laserfiche.ConsultaImagen(document.CodigoLaserfiche, out nombreDocumento);

                    docRespuesta.NombreDocumento = nombreDocumento;
                    docRespuesta.Extension = extension;
                    docRespuesta.ArchivoBytes = strbase64File;
                    docRespuesta.ContentType = TypesConstants.GetTypeMIME(extension);
                }

                respuesta.Resultado = TypesConstants.Success;
                respuesta.Data = docRespuesta;
            }
            catch (Exception ex)
            {
                respuesta.Resultado = TypesConstants.Error;
                respuesta.Mensaje = "Error Msg: " + ex.Message + Environment.NewLine + " StackTrace: " + ex.StackTrace;
            }

            return respuesta;
        }

        [HttpPost]
        [Route("getListImage")]
        public Respuesta GetListImages(DocumentModel document)
        {
            Respuesta respuesta = new Respuesta();
            List<DocumentModel> docRespuestaLst = new List<DocumentModel>();
            LaserficheManager laserfiche = new LaserficheManager();         

            try
            {
                if (document.CodigoLaserficheAnidados != null) 
                    {
                        if (document.CodigoLaserficheAnidados.Length > 0)
                        {
                            string nombreDocumento = string.Empty;
                            string extension = string.Empty;
                            var strbase64File = laserfiche.ConsultaImagenLista(document.CodigoLaserficheAnidados, out nombreDocumento);

                            foreach (var i in strbase64File)
                            {
                                DocumentModel docRespuesta = new DocumentModel();
                                docRespuesta.NombreDocumento = nombreDocumento;
                                docRespuesta.Extension = extension;
                                docRespuesta.ArchivoBytes = i;
                                docRespuesta.ContentType = TypesConstants.GetTypeMIME(extension);
                                docRespuestaLst.Add(docRespuesta);
                            }

                        }

                        respuesta.Resultado = TypesConstants.Success;
                        respuesta.DataList = docRespuestaLst; 
                    }
            }
            catch (Exception ex)
            {
                respuesta.Resultado = TypesConstants.Error;
                respuesta.Mensaje = "Error Msg: " + ex.Message + Environment.NewLine + " StackTrace: " + ex.StackTrace;
            }

            return respuesta;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Agrobanco.LaserFiche.API
{
    public static class ApplicationKeys
    {
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];
        
        public static string LaserFicheRepository => ConfigurationManager.AppSettings["config:LfRepositorio"];
        public static string LaserFicheFolderBase => ConfigurationManager.AppSettings["config:LfFolderDocument"];

        public static string LaserFicheFolder => ConfigurationManager.AppSettings["config:LFApplicationName"];
        public static string LaserFicheEnableEncrip => ConfigurationManager.AppSettings["config:LaserFicheEnableEncrip"];
    }
}
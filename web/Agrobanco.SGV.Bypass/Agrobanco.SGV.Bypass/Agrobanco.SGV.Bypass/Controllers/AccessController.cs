﻿using Agrobanco.SGV.Bypass.Model;
using Agrobanco.SGV.Bypass.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/acceso")]
    [ApiController]
    public class AccessController : ControllerBase
    {

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(AuthAccessResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ProducesResponseType(502, Type = typeof(ResponseError))]
        public object GenerarToken(
            [Required][FromHeader(Name = "X-AppKey")] string appkey,
            [Required][FromHeader(Name = "X-AppCode")] string appcode,
            [Required][FromBody] AuthAccessRequest access)
        {
            try
            {

                Proxy proxi = new Proxy();
                var response = proxi.GenerarToken(access, appkey, appcode, out string Authorization);
                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", Authorization);

                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

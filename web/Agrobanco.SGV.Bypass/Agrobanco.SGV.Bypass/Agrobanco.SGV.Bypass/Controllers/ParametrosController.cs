﻿using Agrobanco.SGV.Bypass.Model;
using Agrobanco.SGV.Bypass.Providers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/parametro")]
    [ApiController]
    public class ParametrosController : ControllerBase
    {
        [HttpPost("version")]
        [ProducesResponseType(200, Type = typeof(ValidarVersion))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]

        public object ValidarVersion([FromBody] ValidarVersion valVersion)
        {
            try
            {
                Proxy proxi = new Proxy();
                var response = proxi.ValidarVersion(valVersion);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("movil")]
        [ProducesResponseType(200, Type = typeof(EDescargaMovilDTO))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ParametrosMovil([Required][FromHeader(Name = "Token")] string token, [FromBody] MovilRequest movilRequest)
        {
            try
            {

                Proxy proxi = new Proxy();
                var response = proxi.ParametrosMovil(movilRequest,token);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("agencias")]
        [ProducesResponseType(200, Type = typeof(List<EDescargaAgenciaDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Agencias([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {

                Proxy proxi = new Proxy();
                var response = proxi.Agencias(token);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listarCultivos")]
        [ProducesResponseType(200, Type = typeof(List<CultivoDatoDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object ListarCultivos([Required][FromHeader(Name = "Token")] string token)
        {
            try
            {
                Proxy proxi = new Proxy();
                var response = proxi.ListarCultivos(token);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

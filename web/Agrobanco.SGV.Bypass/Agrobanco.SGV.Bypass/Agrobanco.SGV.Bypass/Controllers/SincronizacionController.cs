﻿using Agrobanco.SGV.Bypass.Model;
using Agrobanco.SGV.Bypass.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Controllers
{
   
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/sincronizacion")]
    [ApiController]
    public class SincronizacionController : ControllerBase
    {

        [HttpPost("descarga")]
        [ProducesResponseType(200, Type = typeof(List<DescargaAsignacionDTO>))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        public object Descarga([Required][FromHeader(Name = "Token")] string token, [FromBody] SincronizacionRequest request)
        {
            try
            {
                Proxy proxi = new Proxy();
                var response = proxi.DescargaAsignacionDTO(request,token);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

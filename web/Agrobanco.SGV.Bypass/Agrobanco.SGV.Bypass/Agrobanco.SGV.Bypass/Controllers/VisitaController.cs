﻿using Agrobanco.SGV.Bypass.Model;
using Agrobanco.SGV.Bypass.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/visita")]
    [ApiController]
    public class VisitaController : ControllerBase
    {
        [HttpPost("cargavisita")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]

        public object CargarVisita([Required][FromHeader(Name = "Token")] string token, [FromBody] CargaVisitaModel paramCargaVisitaModel)
        {
            try
            {
                Proxy proxi = new Proxy();
                var response = proxi.CargarVisita(paramCargaVisitaModel,token);
                return Ok(response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}

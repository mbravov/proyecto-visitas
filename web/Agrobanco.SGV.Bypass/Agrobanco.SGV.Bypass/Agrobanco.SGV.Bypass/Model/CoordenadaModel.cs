﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class CoordenadaModel
    {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }

        [JsonPropertyName("codigocoordenadamobile")]
        public string CodigoCoordenadaMobile { get; set; }

        [JsonPropertyName("codigovisita")]
        public int CodigoVisita { get; set; }

        [JsonPropertyName("numorden")]
        public int NumOrden { get; set; }

        [JsonPropertyName("precision")]
        public string Precision { get; set; }

        [JsonPropertyName("longitud")]
        public string Longitud { get; set; }

        [JsonPropertyName("latitud")]
        public string Latitud { get; set; }

        [JsonPropertyName("altitud")]
        public string Altitud { get; set; }

        //[JsonPropertyName("usuariocreacion")]
        //public string usuariocreacion { get; set; }

        //[JsonPropertyName("fechacreacion")]
        //public DateTime? fechacreacion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class CultivoDatoDTO
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}

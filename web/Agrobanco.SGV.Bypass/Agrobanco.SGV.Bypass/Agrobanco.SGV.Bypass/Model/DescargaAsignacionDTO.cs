﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class DescargaAsignacionDTO
    {
        public string NumeroDocumento { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int Codigo { get; set; }
        public int Estado { get; set; }
        public int CodAgencia { get; set; }
        public string WebUser { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class EDescargaMovilDTO
    {
        public int DiasEliminar { get; set; }
        public int Precision { get; set; }
        public decimal Hectareas { get; set; }
        public int Dispositivo { get; set; }

        public List<EDescargaAgenciaDTO> ListaAgencias { get; set; }
        public List<DescargaCultivoDTO> ListaCultivo { get; set; }
        public List<DescargaMedidaDTO> ListaMedida { get; set; }
        public List<DescargaCrianzaDTO> ListaCrianza { get; set; }
        public List<DescargaUnidadFinanciarDTO> ListaUnidadFinanciar { get; set; }

    }

    public class DescargaCultivoDTO
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }

    }
    public class DescargaMedidaDTO
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaCrianzaDTO
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }
    public class DescargaUnidadFinanciarDTO
    {
        public String Codigo { get; set; }
        public int Orden { get; set; }
        public String Nombre { get; set; }
    }

    public class EDescargaAgenciaDTO
    {

        public int Codigo { get; set; }
        public string Oficina { get; set; }
        public string OficinaRegional { get; set; }



    }
}

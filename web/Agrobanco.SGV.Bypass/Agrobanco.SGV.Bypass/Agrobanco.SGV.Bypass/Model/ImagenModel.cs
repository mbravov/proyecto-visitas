﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class ImagenModel
    {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }

        [JsonPropertyName("codigovisita")]
        public int CodigoVisita { get; set; }

        [JsonPropertyName("codigofotomobile")]
        public string CodigoFotoMobile { get; set; }

        [JsonPropertyName("numordencaptura")]
        public int NumOrdenCaptura { get; set; }

        [JsonPropertyName("longcapturaimagen")]
        public string LongCapturaImagen { get; set; }

        [JsonPropertyName("latcapturaimagen")]
        public string LatCapturaImagen { get; set; }

        [JsonPropertyName("altitudcapturaimagen")]
        public string AltitudCapturaImagen { get; set; }

        [JsonPropertyName("precision")]
        public string Precision { get; set; }

        /*      1 = Principal, 2 = Complementarias  */
        [JsonPropertyName("tipoimagen")]
        public int TipoImagen { get; set; }

        /*      Predio, domicilio, otro bien     */
        [JsonPropertyName("tipobien")]
        public int TipoBien { get; set; }

        [JsonPropertyName("archivobyte")]
        public string ArchivoByte { get; set; }

        [JsonPropertyName("nombrearchivo")]
        public string NombreArchivo { get; set; }
        [JsonPropertyName("extensionarchivo")]
        public string ExtensionArchivo { get; set; }
        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }
        [JsonPropertyName("coddocumentolf")]
        public string coddocumentolf { get; set; }
        [JsonPropertyName("posicionlista")]
        public int posicionLista { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class ResponseSuccess
    {
        public ResponseSuccessDetalle success { get; set; }
    }

    public class ResponseSuccessDetalle
    {
        public string titulo { get; set; }
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }

    public class ResponseError
    {
        public ResponseErrorDetalle error { get; set; }
    }

    public class ResponseErrorDetalle
    {
        public string titulo { get; set; }
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class ServiceResponse
    {
        public bool Exito { get; set; }

        public string Mensaje { get; set; }
    }
}

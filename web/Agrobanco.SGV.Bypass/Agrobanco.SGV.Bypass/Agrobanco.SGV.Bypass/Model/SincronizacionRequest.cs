﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class SincronizacionRequest
    {
        [JsonPropertyName("webuser")]
        public string WebUser { get; set; }
    }
}

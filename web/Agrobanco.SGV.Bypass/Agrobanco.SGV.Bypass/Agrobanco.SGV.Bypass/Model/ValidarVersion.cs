﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Agrobanco.SGV.Bypass.Model
{
    public class ValidarVersion
    {
        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("resultado")]
        public int resultado { get; set; }


    }
}

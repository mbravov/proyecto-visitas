﻿using Agrobanco.SGV.Bypass.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agrobanco.SGV.Bypass.Comun;

namespace Agrobanco.SGV.Bypass.Providers
{
    public class Proxy
    {
        public AuthAccessResponse GenerarToken(AuthAccessRequest access, string appkey, string appcode, out string Authorization)
        {
            var respuesta = Services.Services.PostToken<AuthAccessResponse>(access, "api/v1/acceso", appkey,appcode,out Authorization);
            return respuesta;
        }

        public ValidarVersion ValidarVersion(ValidarVersion valVersion)
        {
            var respuesta = Services.Services.Post<ValidarVersion>(valVersion, "api/v1/parametro/version", "");
            return respuesta;
        }

        public List<DescargaAsignacionDTO> DescargaAsignacionDTO(SincronizacionRequest valVersion, string token)
        {
            var respuesta = Services.Services.Post<List<DescargaAsignacionDTO>>(valVersion, "api/sincronizacion/descarga", token);
            return respuesta;
        }

        public ServiceResponse CargarVisita(CargaVisitaModel paramCargaVisitaModel, string token)
        {
            var respuesta = Services.Services.Post<ServiceResponse>(paramCargaVisitaModel, "api/v1/visita/cargavisita", token);
            return respuesta;
        }

        public EDescargaMovilDTO ParametrosMovil(MovilRequest movilRequest, string token)
        {
            var respuesta = Services.Services.Post<EDescargaMovilDTO>(movilRequest, "api/v1/parametro/movil", token);
            return respuesta;
        }
        public List<EDescargaAgenciaDTO> Agencias(string token)
        {
            var respuesta = Services.Services.Post<List<EDescargaAgenciaDTO>>(null, "api/v1/parametro/agencias", token);
            return respuesta;
        }

        public List<CultivoDatoDTO> ListarCultivos(string token)
        {
            var respuesta = Services.Services.Post<List<CultivoDatoDTO>>(null, "api/v1/parametro/listarCultivos", token);
            return respuesta;
        }
    }
}

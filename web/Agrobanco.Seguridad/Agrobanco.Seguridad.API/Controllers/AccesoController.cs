﻿using Agrobanco.Seguridad.API.Providers;

using Agrobanco.Seguridad.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using Agrobanco.Seguridad.API.Comun;

namespace Agrobanco.Seguridad.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccesoController : ControllerBase
    {
        [HttpPost("authenticate")]
        //[ProducesResponseType(200, Type = typeof(EloginResponse))]
        //[ProducesResponseType(400, Type = typeof(ResponseError))]
        //[ProducesResponseType(500, Type = typeof(ResponseError))]
        //[ProducesResponseType(502, Type = typeof(ResponseError))]
        public object Authenticate(ELoginRequest eLoginRequest)
        {
            try
            {
                EloginResponse eloginResponse = new EloginResponse();
                ListUsuarioPerfilAplicacion perfiles = new ListUsuarioPerfilAplicacion();
                ModulosAplicacion modulos = new ModulosAplicacion();

                eLoginRequest.Password = Util.MD5Hash(eLoginRequest.Password);

                ProxySGSAPI proxySGSAPI = new ProxySGSAPI();

                eloginResponse = proxySGSAPI.AutenticarUsuario(eLoginRequest);
                string token = eloginResponse.vToken;

                if (eloginResponse.eServicioResponse.Resultado == 1)
                {
                    eloginResponse.vPassword = string.Empty;
                }

                if (!string.IsNullOrEmpty(eLoginRequest.UrlAcceso) && eLoginRequest.UrlAcceso.Length > 0)
                {
                    perfiles = proxySGSAPI.ObtenerPerfiles(eLoginRequest, token);

                    if (perfiles.eServicioResponse.Resultado == 1)
                    {
                        eloginResponse.ListUsuarioPerfil = perfiles.ListUsuarioPerfil;
                    }
                }

                if (perfiles.eServicioResponse.Resultado == 1 && perfiles.ListUsuarioPerfil.Count > 0)
                {
                    var perfil = perfiles.ListUsuarioPerfil.FirstOrDefault();

                    if (perfil != null)
                    {
                        modulos = proxySGSAPI.ObtenerModulos(eLoginRequest, perfil.IdPerfil, token);
                    }

                    if (modulos.eServicioResponse.Resultado == 1 && modulos.ListaModulo != null && modulos.ListaModulo.Count > 0)
                    {
                        eloginResponse.ListaModulo = modulos.ListaModulo;
                        //eloginResponse.ListaModulo = modulos.ListaModulo.Skip((eLoginRequest.PageNumber - 1)* eLoginRequest.PageSize).Take(eLoginRequest.PageSize).ToList();

                    }
                }

                Response.Headers.Add("Access-Control-Expose-Headers", "Authorization");
                Response.Headers.Add("Authorization", "Bearer " + eloginResponse.vToken);

                return Ok(eloginResponse);
            }
            catch (Exception ex)
            {
                return  ex;
            }
        }        
    }

    
}

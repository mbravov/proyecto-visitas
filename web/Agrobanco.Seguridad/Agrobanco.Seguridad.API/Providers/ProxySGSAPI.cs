﻿using Agrobanco.Seguridad.API.Comun;
using Agrobanco.Seguridad.API.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.Seguridad.API.Providers
{
    public class ProxySGSAPI
    {
        private readonly HttpClient cliente;

        public EloginResponse AutenticarUsuario (ELoginRequest eLoginRequest)
        {
            EloginResponse response = new EloginResponse();
            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") +  "/acceso/authenticate";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(urlRequest);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                HttpResponseMessage httpResponse = new HttpResponseMessage();
                StringContent sc = new StringContent(JsonConvert.SerializeObject(eLoginRequest), Encoding.UTF8, "application/json");

                httpResponse = client.PostAsync(urlRequest, sc).Result;

                if (httpResponse.IsSuccessStatusCode)
                {
                    var message = httpResponse.Content.ReadAsStringAsync();
                    response = JsonConvert.DeserializeObject<EloginResponse>(message.Result);
                }
                else
                {
                    string msj = string.Concat(httpResponse.StatusCode.ToString(), " - ", httpResponse.ReasonPhrase);

                    response.eServicioResponse.Resultado = 0;
                    response.eServicioResponse.Mensaje = msj;
                }

                return response;
            }

        }

        
        public ListUsuarioPerfilAplicacion ObtenerPerfiles(ELoginRequest eLoginRequest, string token)
        {
            ListUsuarioPerfilAplicacion response = new ListUsuarioPerfilAplicacion();
            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") + $"/perfil/obtenerPerfilesAplicacion?usuarioWeb={eLoginRequest.Username}&urlAplicacion={eLoginRequest.UrlAcceso}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(urlRequest);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

                HttpResponseMessage httpResponse = new HttpResponseMessage();
                //StringContent sc = new StringContent(JsonConvert.SerializeObject(eLoginRequest), Encoding.UTF8, "application/json");

                httpResponse = client.GetAsync(urlRequest).Result;

                if (httpResponse.IsSuccessStatusCode)
                {
                    var message = httpResponse.Content.ReadAsStringAsync();
                    response = JsonConvert.DeserializeObject<ListUsuarioPerfilAplicacion>(message.Result);
                }
                else
                {
                    string msj = string.Concat(httpResponse.StatusCode.ToString(), " - ", httpResponse.ReasonPhrase);

                    response.eServicioResponse.Resultado = 0;
                    response.eServicioResponse.Mensaje = msj;
                }

                return response;
            }

        }

        public ModulosAplicacion ObtenerModulos(ELoginRequest eLoginRequest, string idPerfil, string token)
        {
            ModulosAplicacion response = new ModulosAplicacion();

            string urlRequest = Util.ObtenerValueFromKey("UrlSGSAPI") + $"/modulo/obtenermodulos/{idPerfil}/{eLoginRequest.Username}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(urlRequest);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (!string.IsNullOrEmpty(token))
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

                HttpResponseMessage httpResponse = new HttpResponseMessage();

                httpResponse = client.GetAsync(urlRequest).Result;

                if (httpResponse.IsSuccessStatusCode)
                {
                    var message = httpResponse.Content.ReadAsStringAsync();
                    response.ListaModulo = JsonConvert.DeserializeObject<List<Modulo>>(message.Result);

                }
                else
                {
                    string msj = string.Concat(httpResponse.StatusCode.ToString(), " - ", httpResponse.ReasonPhrase);

                    response.eServicioResponse.Resultado = 0;
                    response.eServicioResponse.Mensaje = msj;
                }

                return response;
            }

        }        
    }
}

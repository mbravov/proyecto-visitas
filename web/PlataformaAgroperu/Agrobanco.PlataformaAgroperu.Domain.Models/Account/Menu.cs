﻿using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Account
{
    public class Menu
    {
        public int CodigoMenu { get; set; }
        public string DescripcionMenu { get; set; }
        public string Controller { get; set; }
        public string Icono { get; set; }
        public List<Opcion> Opciones { get; set; }
        
    }
}

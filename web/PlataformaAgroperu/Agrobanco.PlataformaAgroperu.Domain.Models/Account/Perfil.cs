﻿using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Account
{
    public class Perfil
    {
        //public string idmenu { get; set; }
        //public string menunombre { get; set; }
        //public string idsubmenu { get; set; }
        //public string submenunombre { get; set; }
        public List<Menu> listamenu { get; set; }
        //public List<SubMenu> listasubmenu { get; set; }
    }
}

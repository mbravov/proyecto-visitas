﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Account
{
    public class Opcion
    {
        public string Descripcion { get; set; }
        public string Action { get; set; }
    }
}

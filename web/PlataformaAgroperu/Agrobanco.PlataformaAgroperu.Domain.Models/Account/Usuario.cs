﻿using System;
using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Account
{
    public class Usuario
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int CodigoAgencia { get; set; }
        public string NombreAgencia { get; set; }
        public string CodigoFuncionario { get; set; }
        public int CodigoPerfil { get; set; }
        public string Perfil { get; set; }
        public string Plataforma { get; set; }
        public bool HomePageEstadistico { get; set; }



        public List<Menu> ListaMenu { get; set; }
    }
}

﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Common
{
    public class IntegranteSentinel
    {
        public string tipo_documento { get; set; }
        public string nro_documento { get; set; }
    }
}
﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Common
{
    public class DocumentoEvaluar
    {
        public string tipo { get; set; }
        public string documento { get; set; }
        public DocumentoEvaluar(string ptipo, string pdocumento)
        {
            tipo = ptipo;
            documento = pdocumento;
        }
    }
}

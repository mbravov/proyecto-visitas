﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Common
{
    public class ParametricasModel
    {
        public string key { get; set; }
        public string code { get; set; }
        public string value { get; set; }
        public string flag { get; set; }
        public string trama { get; set; }

        public ParametricasModel()
        {
        }
        public ParametricasModel(string pkey, string pcode, string pvalue)
        {
            key = pkey;
            code = pcode;
            value = pvalue;
        }
        public ParametricasModel(string pkey, string pcode, string pvalue, string pflag)
        {
            key = pkey;
            code = pcode;
            value = pvalue;
            flag = pflag;
        }
    }
}

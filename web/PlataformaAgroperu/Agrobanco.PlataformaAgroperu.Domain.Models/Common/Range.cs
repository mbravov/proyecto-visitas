﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Common
{
    public class Cell
    {
        public string value { get; set; }
        public int row { get; set; }
        public int column { get; set; }
        public string LetterColumn { get; set;}

        public Cell() { }

        public Cell(string pvalue, int pfila = 0, int pcolumn = 0)
        {
            value = pvalue;
            row = pfila;
            column = pcolumn;
            LetterColumn = getLetterColumn(pcolumn);
        }

        public Cell(int pfila = 0, int pcolumn = 0)
        {
            value = "";
            row = pfila;
            column = pcolumn;
            LetterColumn = getLetterColumn(pcolumn);
        }

        public Cell(Cell cell)
        {
            value = cell.value;
            row = cell.row;
            column = cell.column;
            LetterColumn = getLetterColumn(cell.column);
        }

        private string getLetterColumn(int NumberColumn)
        {
            switch (NumberColumn)
            {
                case 0: return "A";
                case 1: return "B";
                case 2: return "C";
                case 3: return "D";
                case 4: return "E";
                case 5: return "F";
                case 6: return "G";
                case 7: return "H";
                case 8: return "I";
                case 9: return "J";
                case 10: return "K";
                case 11: return "L";
                case 12: return "M";
                case 13: return "N";
                case 14: return "O";
                case 15: return "P";
                case 16: return "Q";
                case 17: return "R";
                case 18: return "S";
                case 19: return "T";
                case 20: return "U";
                case 21: return "V";
                case 22: return "W";
                case 23: return "X";
                case 24: return "Y";
                case 25: return "Z";
                case 26: return "AA";
                case 27: return "AB";
                case 28: return "AC";
                case 29: return "AD";
                case 30: return "AE";
                case 31: return "AF";
                case 32: return "AG";
                case 33: return "AH";
                case 34: return "AI";
                case 35: return "AJ";
                case 36: return "AK";
                case 37: return "AL";
                case 38: return "AM";
                case 39: return "AN";
                case 40: return "AO";
                case 41: return "AP";
                case 42: return "AQ";
                case 43: return "AR";
                case 44: return "AS";
                case 45: return "AT";
                case 46: return "AU";
                case 47: return "AV";
                case 48: return "AW";
                case 49: return "AX";
                case 50: return "AY";
                case 51: return "AZ";
                default:
                    break;
            }
            return string.Empty;
        }

    }
}

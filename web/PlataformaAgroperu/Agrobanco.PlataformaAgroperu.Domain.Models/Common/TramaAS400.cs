﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Common
{
    public class TramaAS400
    {
        public string e_par1 { get; set; }
        public string e_codtrx { get; set; }
        public string e_codapl { get; set; }
        public string e_correla { get; set; }
        public string e_fec_a { get; set; }
        public string e_fec_m { get; set; }
        public string e_fec_d { get; set; }
        public string e_hora { get; set; }
        public string e_estenv { get; set; }
        public string e_nroint { get; set; }
        public string e_empresa { get; set; }
        public string e_filler { get; set; }
        public TramaAS400(string CodigoTransaccion)
        {
            DateTime fec = DateTime.Now;
            e_codtrx = CodigoTransaccion;
            e_codapl = "JR";
            e_correla = sha_hash(fec.ToLongDateString() + fec.Millisecond.ToString());
            e_fec_a = fec.Year.ToString();
            e_fec_m = fec.Month.ToString().PadLeft(2, '0');
            e_fec_d = fec.Day.ToString().PadLeft(2, '0');
            e_hora = fec.Hour.ToString().PadLeft(2, '0') + fec.Minute.ToString().PadLeft(2, '0') + fec.Second.ToString().PadLeft(2, '0');
            e_estenv = "0";
            e_nroint = "000001";
            e_empresa = "001";
            e_filler = "";
            e_par1 = e_codtrx + e_codapl + e_correla + e_fec_a + e_fec_m + e_fec_d + e_hora + e_estenv + e_nroint + e_empresa + e_filler;
        }


        public TramaAS400(string CodigoTransaccion, string CodigoAplicacion, string Empresa = "001", string Filler = "")
        {
            DateTime fec = DateTime.Now;
            e_codtrx = CodigoTransaccion;
            e_codapl = CodigoAplicacion;
            e_correla = sha_hash(fec.ToLongDateString() + fec.Millisecond.ToString());
            e_fec_a = fec.Year.ToString();
            e_fec_m = fec.Month.ToString().PadLeft(2, '0');
            e_fec_d = fec.Day.ToString().PadLeft(2, '0');
            e_hora = fec.Hour.ToString().PadLeft(2, '0') + fec.Minute.ToString().PadLeft(2, '0') + fec.Second.ToString().PadLeft(2, '0');
            e_estenv = "0";
            e_nroint = "000001";
            e_empresa = Empresa.PadLeft(3,'0');
            e_filler = Filler;
            e_par1 = e_codtrx + e_codapl + e_correla + e_fec_a + e_fec_m + e_fec_d + e_hora + e_estenv + e_nroint + e_empresa + e_filler;
        }


        public static String sha_hash(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA1 hash = SHA1Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }
            return Sb.ToString();
        }
    }

    public class VariableINOUT
    {
        public string e_cod { get; set; }
        public string e_codgrp { get; set; }
        public string e_msg { get; set; }
    }


    public class ModelPrueba
    {
        public string DEABNK { get; set; }
        public string DEABRN { get; set; }
        public string DEACCY { get; set; }
        public string DEAGLN { get; set; }
        public string DEAACC { get; set; }
        public string DEATRF { get; set; }
        public string DEAPBR { get; set; }
        public string DEASDM { get; set; }
        public string DEASDD { get; set; }
        public string DEASDY { get; set; }
    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models
{
    public class ExcelFiltroAgroperu
    {
        public Cell TipoDocumento { get; set; }
        public Cell NumeroDocumento { get; set; }
        public Cell ApellidoPaterno { get; set; }
        public Cell ApellidoMaterno { get; set; }
        public Cell PrimerNombre { get; set; }
        public Cell SegundoNombre { get; set; }
        
        public ExcelFiltroAgroperu(){}

        public ExcelFiltroAgroperu(int row)
        {
            TipoDocumento = new Cell(row, 1);
            NumeroDocumento = new Cell(row, 2);
            ApellidoPaterno = new Cell(row, 3);
            ApellidoMaterno = new Cell(row, 4);
            PrimerNombre = new Cell(row, 5);
            SegundoNombre = new Cell(row, 6);
        }

        public string ObtenerTramaDatos()
        {
            return
                TipoDocumento.value + "|" +
                NumeroDocumento.value + "|" +
                ApellidoPaterno.value + "|" +
                ApellidoMaterno.value + "|" +
                PrimerNombre.value + "|" +
                SegundoNombre?.value + "|*|";
        }

    }

    //public enum ExcelAgroperuColumn
    //{
    //    TipoDocumento = 1,
    //    NumeroDocumento = 2,
    //    ApellidoPaterno = 3,
    //    ApellidoMaterno = 4,
    //    PrimerNombre = 5,
    //    SegundoNombre = 6

    //}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models
{
    public class FiltroAgroperuModel
    {
        public decimal CodigoFiltro { get; set; }
        public string Descripcion { get; set; }
        public string NombreExcel { get; set; }
        public string CodigoFuncionario { get; set; }
        public string NombreFuncionario { get; set; }
        public int CodigoAgencia { get; set; }
        public List<ExcelFiltroAgroperu> Excel { get; set; }
        public int Registros { get; set; }
        public string Fecha { get; set; }
        public string Usuario { get; set; }
        public string TramaOpciones { get; set; }


        public FiltroAgroperuModel() { }

        public string ObtenerTramaDatos()
        {
            string trama = "";
            foreach (var item in Excel)
                trama = trama + item.ObtenerTramaDatos();

            return trama;
        }
    }
}

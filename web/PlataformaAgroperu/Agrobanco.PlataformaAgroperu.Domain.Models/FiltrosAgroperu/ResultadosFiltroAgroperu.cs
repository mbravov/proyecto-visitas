﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.FiltrosAgroperu
{
    public class ResultadosFiltroAgroperu
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string NombresIngresados { get; set; }
        public string NombresSBS { get; set; }

    }
}

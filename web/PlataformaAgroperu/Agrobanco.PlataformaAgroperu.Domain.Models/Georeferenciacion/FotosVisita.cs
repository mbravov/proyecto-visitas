﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Georeferenciacion
{
    public class FotosVisita
    {
        public int IdVisita { get; set; }
        public string Agencia { get; set; }
        public string Funcionario { get; set; }
        public string NumeroSolicitud { get; set; }
        public decimal Area { get; set; }
        public string Cultivo { get; set; }        
        public List<FotoModel> Fotos { get; set; }
        public List<Coordenada> Perimetro { get; set; }
        public string tramaPoligono { get; set; }

        // Trama para Insertar Datos Fotos a SqlServer
        public string TramaIdLaserFiche { get; }
        public string TramaFechaFoto { get; }
        public string TramaLatitud { get; }
        public string TramaLongitud { get; }
        public string TramaObservacion { get; }

        public FotosVisita() { }

        public FotosVisita(List<FotoModel> lstFotos, List<Coordenada> perimetro = null, bool tramaDatosFotos = false)
        {
            IdVisita = lstFotos.First().EvalId;
            Agencia = lstFotos.First().Agencia;
            Funcionario = lstFotos.First().Funcionario;
            NumeroSolicitud = lstFotos.First().NroSolicitud;
            Area = lstFotos.First().Distancia;
            Cultivo = lstFotos.First().Cultivo;
            Fotos = lstFotos;

            if (tramaDatosFotos)
            {
                TramaIdLaserFiche = TramaFechaFoto = TramaLatitud = TramaLongitud = TramaObservacion = string.Empty;
                foreach (var foto in lstFotos)
                {                    
                    TramaIdLaserFiche = TramaIdLaserFiche + foto.IdLaserFiche + "|";
                    TramaFechaFoto = TramaFechaFoto + foto.FechaFoto + "|";
                    TramaLatitud = TramaLatitud + foto.Latitud + "|";
                    TramaLongitud = TramaLongitud + foto.Longitud + "|";
                    TramaObservacion = TramaObservacion + foto.Observacion + "|";
                }
            }
            if (perimetro != null)
            {
                Perimetro = perimetro;
                tramaPoligono = string.Empty;
                foreach (var coordenada in perimetro)
                    tramaPoligono = $"{tramaPoligono}{coordenada.Longitud} {coordenada.Latitud},";
                tramaPoligono = $"{tramaPoligono}{perimetro.First().Longitud} {perimetro.First().Latitud}";
            }
        }

        public string GetCodigoCultivo()
        {
            return Cultivo.Split('_')[0].ToString();
        }
        public string GetDescripcionCultivo()
        {
            return Cultivo.Split('_')[1];
        }


    }

    public class Coordenada
    {        
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}

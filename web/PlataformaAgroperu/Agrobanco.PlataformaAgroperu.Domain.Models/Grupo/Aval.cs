﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Aval
    {
        public Cell TipoAval { get; set; }
        public Cell TipoDocumento { get; set; }
        public Cell NroDocumento { get; set; }
        public Cell ApePaterno { get; set; }
        public Cell ApeMaterno { get; set; }
        public Cell PrimerNombre { get; set; }
        public Cell SegundoNombre { get; set; }
        public Cell Plazo { get; set; }
        public Cell FechaSiembra { get; set; }
        public Cell Criterio { get; set; }
        //public string getTrama()
        //{
        //    return TipoAval.value + "|" + TipoDocumento.value + "|" + NroDocumento.value + "|" + ApePaterno.value + "|" + ApeMaterno.value + "|" + PrimerNombre.value + "|" + SegundoNombre.value + "|" + Plazo.value + "|" + FechaSiembra.value + "|" + Criterio.value;
        //}
    }
}

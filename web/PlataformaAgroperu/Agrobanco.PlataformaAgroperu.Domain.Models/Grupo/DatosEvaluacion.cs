﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class DatosEvaluacion
    {
        public string CalificacionSBS { get; set; }
        public decimal DeudaAgrobanco { get; set; }
        public decimal DeudaRenovacion { get; set; }
        public int NroOtrasEntidades { get; set; }
        public decimal DeudaOtrasEntidades { get; set; }
        public string ListaNegativa { get; set; }
        public int RSLTVALOR { get; set; }
        public string RSLTDESC { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Garantia
    {
        public Cell TipoGarantia { get; set; }
        public Cell ValorRealizacion { get; set; }
        public Cell TienePoliza { get; set; }
        //public string getTrama()
        //{
        //    return TipoGarantia.value + "|" + ValorRealizacion.value + "|" + TienePoliza.value;
        //}
    }
}

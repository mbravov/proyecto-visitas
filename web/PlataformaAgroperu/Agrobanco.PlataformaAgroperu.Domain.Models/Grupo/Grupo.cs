﻿using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Grupo
    {
        public GrupoDatos DatosGrupo { set; get; }
        public List<GrupoIntegrante> ExcelIntegrantes { get; set; }
       // public string TramaIntegrantes { get; set; }
        public string getTrama() {
            string trama = "";
            foreach (var item in ExcelIntegrantes)
            {
                trama = trama + item.getTrama();
            }
            return trama;
        }

        public string getTramaProspecto()
        {
            string trama = "";
            foreach (var item in ExcelIntegrantes)
            {
                trama = trama + item.getTramaProspecto();
            }
            return trama;
        }

        public string getTramaPadronAgroperu()
        {
            string trama = "";
            foreach (var item in ExcelIntegrantes)
            {
                trama = trama + item.getTramaPadronAgroperu();
            }
            return trama;
        }

        public string getTramaGrupoAgroperu()
        {
            string trama = "";
            foreach (var item in ExcelIntegrantes)
            {
                trama = trama + item.getTramaGrupoAgroperu();
            }
            return trama;
        }
    }
}

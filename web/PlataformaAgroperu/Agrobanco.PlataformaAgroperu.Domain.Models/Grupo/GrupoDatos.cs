﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Account;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class GrupoDatos
    {
        public string NombreGrupo { get; set; }
        public string CodJuntaRegante { get; set; }
        public string CodComisionRegante { get; set; }
        public string TecnicoTipoDocumento { get; set; }
        public string TecnicoNroDocumento { get; set; }
        public string TecnicoNombre { get; set; }
        public string Cultivo { get; set; }
        public string Sector { get; set; }
        public string Programa { get; set; }
        public string PagoAsistenteTecnico { get; set; }

        public string Banco { get; set; }
        public string FormaAbono { get; set; }
        public string Destino { get; set; }
        public string Tasa { get; set; }
        public string FormaPago { get; set; }
        public string SeguroDesgravamen { get; set; }
        public string Analista { get; set; }
        public string NombreAnalista { get; set; }
        public int CodigoAgencia { get; set; }
        public int Nivel { get; set; }
        public string Usuario { get; set; }
        public string Estado { get; set; }
        public string CodigoGrupo { get; set; }

        //public string Plazo { get; set; }
        //public string Departamento { get; set; }
        //public string Provincia { get; set; }
        //public string Distrito { get; set; }
        
        

        public string getTrama() {
            return 
                NombreGrupo + "|" + 
                CodJuntaRegante + "|" + 
                CodComisionRegante + "|" + 
                TecnicoTipoDocumento + "|" + 
                TecnicoNombre + "|" + 
                Cultivo + "|" + 
                TecnicoNroDocumento + "|" + 
                Banco + "|" + 
                FormaAbono + "|" + 
                Destino + "|" + 
                Tasa + "|" + 
                FormaPago + "|" + 
                SeguroDesgravamen + "|" + 
                Sector + "|" + 
                PagoAsistenteTecnico + "|" + 
                Programa + "|*|";
        }

        public GrupoDatos()
        { }
        public GrupoDatos(Usuario oUsuario, int nivel,string estado, string trama)
        {
            string[] arr = trama.Split('|');
            NombreGrupo = arr[0];
            CodJuntaRegante = arr[1];
            CodComisionRegante= arr[2];
            TecnicoTipoDocumento = arr[3];
            TecnicoNombre = arr[4];
            Cultivo = arr[5];
            TecnicoNroDocumento = arr[6];
            Banco = arr[7];
            FormaAbono = arr[8];
            Destino = arr[9];
            Tasa = arr[10];
            FormaPago = arr[11];
            SeguroDesgravamen = arr[12];
            Sector = arr[13];
            Programa = arr[14];
            PagoAsistenteTecnico = arr[15];

            Analista = oUsuario.CodigoFuncionario;
            NombreAnalista = oUsuario.Name;
            CodigoAgencia = oUsuario.CodigoAgencia;
            Nivel = nivel;
            Usuario = oUsuario.User;
            Estado = estado;
        }
    }
}

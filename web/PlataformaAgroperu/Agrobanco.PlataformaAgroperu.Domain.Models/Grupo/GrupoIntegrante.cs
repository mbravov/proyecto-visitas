﻿using System;
using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class ListasSolicitudes
    {
        public List<GrupoIntegrante> Lista1 { get; set; }
        public List<GrupoIntegrante> Lista2 { get; set; }

        public ListasSolicitudes()
        {
            Lista1 = new List<GrupoIntegrante>();
            Lista2 = new List<GrupoIntegrante>();
        }
    }

    public class GrupoIntegrante
    {
        public string NumeroSolicitud { get; set; }
        public string EstadoSolicitud { get; set; }

        public string EstadoEvaluacion { get; set; }
        public string EstadoTipoCuota { get; set; }
        public string EstadoGeoreferencia { get; set; }
        public string EstadoInformeVisita { get; set; }
        public string EstadoFlujoCaja { get; set; }
        public string EstadoInformeComercial { get; set; }
        public string EstadoPropuesta { get; set; }

        public Persona DatosCliente { get; set; }
        public Persona DatosConyugue { get; set; }
        public Predio DatosPredio { get; set; }
        public Solicitud DatosSolicitud { get; set; }
        public Garantia DatosGarantia1 { get; set; }
        public Garantia DatosGarantia2 { get; set; }
        public DatosEvaluacion EvaluacionCliente { get; set; }
        public DatosEvaluacion EvaluacionConyugue { get; set; }
        public Aval DatosAval1 { get; set; }
        public InformeVisita DatosInformeVisita { get; set; }
        // Solo para Consulta
        public List<Garantia> Garantias { get; set; }
        public List<Aval> Avales { get; set; }
        public string TramaOpciones { get; set; }
        public string Trama1 { get; set; }
        public string Trama2 { get; set; }
        public string FlagExcepcion { get; set; }
        
        public string InicioVinculacion { get; set; }/*AGRO-REQ-023 VISITAS VINCULACIÓN */

        public string getTrama()
        {
            if (DatosCliente?.Celular.value == "" || DatosCliente?.Celular.value == null)
            {
                DatosCliente.Celular.value = "0";
            }
            if (DatosCliente.Experiencia.value == "" || DatosCliente.Experiencia.value == null)
            {
                DatosCliente.Experiencia.value = "0";
            }
            if (DatosCliente.NacimientoDia.value == "" || DatosCliente.NacimientoDia.value == null)
            {
                DatosCliente.NacimientoDia.value = "0";
            }
            if (DatosCliente.NacimientoMes.value == "" || DatosCliente.NacimientoMes.value == null)
            {
                DatosCliente.NacimientoMes.value = "0";
            }
            if (DatosCliente.NacimientoAnio.value == "" || DatosCliente.NacimientoAnio.value == null)
            {
                DatosCliente.NacimientoAnio.value = "0";
            }
            if (DatosSolicitud.NroUnidadesTotales.value == "" || DatosSolicitud.NroUnidadesTotales.value == null)
            {
                DatosSolicitud.NroUnidadesTotales.value = "0";
            }
            if (DatosSolicitud.NroUnidadesFinanciamiento.value == "" || DatosSolicitud.NroUnidadesFinanciamiento.value == null)
            {
                DatosSolicitud.NroUnidadesFinanciamiento.value = "0";
            }
            if (DatosSolicitud.MontoSolicitadoSoles.value == "" || DatosSolicitud.MontoSolicitadoSoles.value == null)
            {
                DatosSolicitud.MontoSolicitadoSoles.value = "0";
            }
            if (DatosInformeVisita.Plazo.value == "" || DatosInformeVisita.Plazo.value == null)
            {
                DatosInformeVisita.Plazo.value = "0";
            }
            if (DatosInformeVisita.FechaSiembra.value == "" || DatosInformeVisita.FechaSiembra.value == null)
            {
                DatosInformeVisita.FechaSiembra.value = "0";
            }
            if (DatosSolicitud.NroUnidadesConduccion.value == "" || DatosSolicitud.NroUnidadesConduccion.value == null)
            {
                DatosSolicitud.NroUnidadesConduccion.value = "0";
            }

            var cadena = 
                DatosCliente?.TipoDocumento.value + "|" +
                DatosCliente?.NroDocumento.value + "|" +
                DatosCliente?.ApePaterno.value + "|" +
                DatosCliente?.ApeMaterno.value + "|" +
                DatosCliente?.NomPrimer.value + "|" +
                DatosCliente?.NomSegundo.value + "|" +
                DatosCliente?.Direccion.value + "|" +
                DatosCliente?.Sexo.value + "|" +
                DatosCliente?.EstadoCivil.value + "|" +
                DatosCliente?.Celular.value+ "|" +
                DatosCliente?.Experiencia.value + "|" +
                DatosCliente?.NacimientoDia.value + "|" +
                DatosCliente?.NacimientoMes.value  + "|" +
                DatosCliente?.NacimientoAnio.value + "|" +
                DatosConyugue?.TipoDocumento.value + "|" +
                DatosConyugue?.NroDocumento.value + "|" +
                DatosPredio?.Departamento.value + "|" +//<<<<<-------DEPArtamento
                DatosPredio?.Provincia.value + "|" +//<<<<<-------provincia
                DatosPredio?.Distrito.value + "|" +
                DatosPredio?.Referencia.value + "|" +
                DatosPredio?.RegimenTenencia.value + "|" +
                DatosPredio?.UnidadCatastral.value + "|" +
                DatosPredio?.EstadoCampo.value + "|" +
             DatosSolicitud?.TipoUnidadFinanciamiento.value + "|" +
             DatosSolicitud?.NroUnidadesTotales.value + "|" +
             DatosSolicitud?.NroUnidadesFinanciamiento.value  + "|" +
             DatosSolicitud?.NroUnidadesConduccion.value + "|" +
             DatosSolicitud?.MontoSolicitadoSoles.value+ "|" +
             //DatosSolicitud.NroUnidadesTotales.value + "|" +
             //DatosSolicitud.NroUnidadesFinanciamiento.value + "|" +
             
             //DatosSolicitud.MontoSolicitadoSoles.value + "|" +
             DatosSolicitud?.EnvioCronograma.value + "|" +
             DatosCliente?.Correo.value + "|" +//<<<<<-------correo de cliente
             DatosSolicitud?.PagoAdelantado.value + "|" +
             DatosGarantia1?.TipoGarantia.value + "|" +
             DatosGarantia1?.ValorRealizacion.value + "|" +
             DatosGarantia1?.TienePoliza.value + "|" +
             DatosGarantia2?.TipoGarantia.value + "|" +
             DatosGarantia2?.ValorRealizacion.value + "|" +
             DatosGarantia2?.TienePoliza.value + "|" +
             DatosAval1?.TipoAval.value + "|" +
             DatosAval1?.TipoDocumento.value + "|" +
             DatosAval1?.NroDocumento.value + "|" +
             DatosAval1?.ApePaterno.value + "|" +
             DatosAval1?.ApeMaterno.value + "|" +
             DatosAval1?.PrimerNombre.value + "|" +
             DatosAval1?.SegundoNombre.value + "|" +
             //objAval.Plazo.value + "|" +
             //objAval.FechaSiembra.value + "|" +
             //objAval.Criterio.value + "|*|";
             //DatosInformeVisita.Plazo.value + "|" +
             DatosInformeVisita?.Plazo.value  + "|" +
             DatosInformeVisita?.FechaSiembra.value  + "|*|";
            //   DatosInformeVisita.FechaSiembra.value + "|*|";
            //DatosInformeVisita.Criterio.value + "|*|";

            return cadena;
        }

        public string getTramaProspecto()
        {
            return
                DatosCliente?.TipoDocumento.value + "|" +
                DatosCliente?.NroDocumento.value + "|" +
                DatosCliente?.ApePaterno.value + "|" +
                DatosCliente?.ApeMaterno.value + "|" +
                DatosCliente?.NomPrimer.value + "|" +
                DatosCliente?.NomSegundo.value + "|" +
                DatosCliente?.Direccion.value + "|" +                
                DatosConyugue?.TipoDocumento.value + "|" +
                DatosConyugue?.NroDocumento.value + "|*|";
        }

        public string getTramaPadronAgroperu()
        {
            if (DatosCliente?.Celular.value == "" || DatosCliente?.Celular.value == null)
            {
                DatosCliente.Celular.value = "0";
            }
            if (DatosCliente.Experiencia.value == "" || DatosCliente.Experiencia.value == null)
            {
                DatosCliente.Experiencia.value = "0";
            }
            if (DatosCliente.NacimientoDia.value == "" || DatosCliente.NacimientoDia.value == null)
            {
                DatosCliente.NacimientoDia.value = "0";
            }
            if (DatosCliente.NacimientoMes.value == "" || DatosCliente.NacimientoMes.value == null)
            {
                DatosCliente.NacimientoMes.value = "0";
            }
            if (DatosCliente.NacimientoAnio.value == "" || DatosCliente.NacimientoAnio.value == null)
            {
                DatosCliente.NacimientoAnio.value = "0";
            }
            if (DatosSolicitud.NroUnidadesTotales.value == "" || DatosSolicitud.NroUnidadesTotales.value == null)
            {
                DatosSolicitud.NroUnidadesTotales.value = "0";
            }
            if (DatosSolicitud.NroUnidadesFinanciamiento.value == "" || DatosSolicitud.NroUnidadesFinanciamiento.value == null)
            {
                DatosSolicitud.NroUnidadesFinanciamiento.value = "0";
            }
            if (DatosSolicitud.MontoSolicitadoSoles.value == "" || DatosSolicitud.MontoSolicitadoSoles.value == null)
            {
                DatosSolicitud.MontoSolicitadoSoles.value = "0";
            }
            if (DatosInformeVisita.Plazo.value == "" || DatosInformeVisita.Plazo.value == null)
            {
                DatosInformeVisita.Plazo.value = "0";
            }
            if (DatosInformeVisita.FechaSiembra.value == "" || DatosInformeVisita.FechaSiembra.value == null)
            {
                DatosInformeVisita.FechaSiembra.value = "0";
            }
            if (DatosSolicitud.NroUnidadesConduccion.value == "" || DatosSolicitud.NroUnidadesConduccion.value == null)
            {
                DatosSolicitud.NroUnidadesConduccion.value = "0";
            }
            if (DatosSolicitud.NivelTecnologico.value == "" || DatosSolicitud.NivelTecnologico.value == null)
            {
                DatosSolicitud.NivelTecnologico.value = "0";
            }
            var cadena =
                DatosCliente?.TipoDocumento.value + "|" +
                DatosCliente?.NroDocumento.value + "|" +
                DatosCliente?.ApePaterno.value + "|" +
                DatosCliente?.ApeMaterno.value + "|" +
                DatosCliente?.NomPrimer.value + "|" +
                DatosCliente?.NomSegundo.value + "|" +
                DatosCliente?.Direccion.value + "|" +
                DatosCliente?.Sexo.value + "|" +
                DatosCliente?.EstadoCivil.value + "|" +
                DatosCliente?.Celular.value + "|" +
                DatosCliente?.WhatsApp.value + "|" +
                DatosCliente?.Experiencia.value + "|" +
                DatosCliente?.NacimientoDia.value + "|" +
                DatosCliente?.NacimientoMes.value + "|" +
                DatosCliente?.NacimientoAnio.value + "|" +
                DatosConyugue?.TipoDocumento.value + "|" +
                DatosConyugue?.NroDocumento.value + "|" +
                DatosConyugue?.ApePaterno.value + "|" +
                DatosConyugue?.ApeMaterno.value + "|" +
                DatosConyugue?.NomPrimer.value + "|" +
                DatosConyugue?.NomSegundo.value + "|" +
                DatosPredio?.Departamento.value + "|" +//<<<<<-------DEPArtamento
                DatosPredio?.Provincia.value + "|" +//<<<<<-------provincia
                DatosPredio?.Distrito.value + "|" +
                DatosPredio?.Referencia.value + "|" +
                DatosPredio?.RegimenTenencia.value + "|" +
                DatosPredio?.UnidadCatastral.value + "|" +
                DatosSolicitud?.Sector.value + "|" +
                DatosSolicitud?.Programa.value + "|" +
                DatosSolicitud?.TipoUnidadFinanciamiento.value + "|" +
                DatosSolicitud?.NroUnidadesTotales.value + "|" +
                DatosSolicitud?.NroUnidadesFinanciamiento.value + "|" +
                DatosSolicitud?.NroUnidadesConduccion.value + "|" +
                DatosSolicitud?.Cultivo.value + "|" +
                DatosSolicitud?.NivelTecnologico.value + "|" +
                DatosSolicitud?.MontoSolicitadoSoles.value + "|" +
                DatosSolicitud?.EnvioCronograma.value + "|" +
                DatosCliente?.Correo.value + "|" +  //<<<<<-------correo de cliente
                DatosSolicitud?.PagoAdelantado.value + "|" +
                DatosInformeVisita?.Plazo.value + "|" +
                DatosInformeVisita?.FechaSiembra.value + "|*|";
            
            return cadena;
        }

        public string getTramaGrupoAgroperu()
        {
            if (DatosCliente?.Celular.value == "" || DatosCliente?.Celular.value == null)
            {
                DatosCliente.Celular.value = "0";
            }
            if (DatosCliente.Experiencia.value == "" || DatosCliente.Experiencia.value == null)
            {
                DatosCliente.Experiencia.value = "0";
            }
            if (DatosCliente.NacimientoDia.value == "" || DatosCliente.NacimientoDia.value == null)
            {
                DatosCliente.NacimientoDia.value = "0";
            }
            if (DatosCliente.NacimientoMes.value == "" || DatosCliente.NacimientoMes.value == null)
            {
                DatosCliente.NacimientoMes.value = "0";
            }
            if (DatosCliente.NacimientoAnio.value == "" || DatosCliente.NacimientoAnio.value == null)
            {
                DatosCliente.NacimientoAnio.value = "0";
            }
            if (DatosSolicitud.NroUnidadesTotales.value == "" || DatosSolicitud.NroUnidadesTotales.value == null)
            {
                DatosSolicitud.NroUnidadesTotales.value = "0";
            }
            if (DatosSolicitud.NroUnidadesFinanciamiento.value == "" || DatosSolicitud.NroUnidadesFinanciamiento.value == null)
            {
                DatosSolicitud.NroUnidadesFinanciamiento.value = "0";
            }
            if (DatosSolicitud.MontoSolicitadoSoles.value == "" || DatosSolicitud.MontoSolicitadoSoles.value == null)
            {
                DatosSolicitud.MontoSolicitadoSoles.value = "0";
            }
            if (DatosInformeVisita.Plazo.value == "" || DatosInformeVisita.Plazo.value == null)
            {
                DatosInformeVisita.Plazo.value = "0";
            }
            if (DatosInformeVisita.FechaSiembra.value == "" || DatosInformeVisita.FechaSiembra.value == null)
            {
                DatosInformeVisita.FechaSiembra.value = "0";
            }
            if (DatosSolicitud.NroUnidadesConduccion.value == "" || DatosSolicitud.NroUnidadesConduccion.value == null)
            {
                DatosSolicitud.NroUnidadesConduccion.value = "0";
            }
            if (DatosSolicitud.NivelTecnologico.value == "" || DatosSolicitud.NivelTecnologico.value == null)
            {
                DatosSolicitud.NivelTecnologico.value = "0";
            }

            var cadena =
                DatosCliente?.TipoDocumento.value + "|" +
                DatosCliente?.NroDocumento.value + "|" +
                DatosCliente?.ApePaterno.value + "|" +
                DatosCliente?.ApeMaterno.value + "|" +
                DatosCliente?.NomPrimer.value + "|" +
                DatosCliente?.NomSegundo.value + "|" +
                DatosCliente?.Direccion.value + "|" +
                DatosCliente?.Sexo.value + "|" +
                DatosCliente?.EstadoCivil.value + "|" +
                DatosCliente?.Celular.value + "|" +
                DatosCliente?.WhatsApp.value + "|" +
                DatosCliente?.Experiencia.value + "|" +
                DatosCliente?.NacimientoDia.value + "|" +
                DatosCliente?.NacimientoMes.value + "|" +
                DatosCliente?.NacimientoAnio.value + "|" +
                
                DatosConyugue?.TipoDocumento.value + "|" +
                DatosConyugue?.NroDocumento.value + "|" +
                DatosConyugue?.ApePaterno.value + "|" +
                DatosConyugue?.ApeMaterno.value + "|" +
                DatosConyugue?.NomPrimer.value + "|" +
                DatosConyugue?.NomSegundo.value + "|" +

                DatosPredio?.Departamento.value + "|" +//<<<<<-------DEPArtamento
                DatosPredio?.Provincia.value + "|" +//<<<<<-------provincia
                DatosPredio?.Distrito.value + "|" +
                DatosPredio?.Referencia.value + "|" +
                DatosPredio?.RegimenTenencia.value + "|" +
                DatosPredio?.UnidadCatastral.value + "|" +

                DatosSolicitud?.TipoUnidadFinanciamiento.value + "|" +
                DatosSolicitud?.NroUnidadesTotales.value + "|" +
                DatosSolicitud?.NroUnidadesFinanciamiento.value + "|" +
                DatosSolicitud?.NroUnidadesConduccion.value + "|" +
                DatosSolicitud?.MontoSolicitadoSoles.value + "|" +             
                DatosSolicitud?.EnvioCronograma.value + "|" +
                DatosCliente?.Correo.value + "|" +//<<<<<-------correo de cliente
                DatosSolicitud?.PagoAdelantado.value + "|" +

                DatosGarantia1?.TipoGarantia.value + "|" +
                DatosGarantia1?.ValorRealizacion.value + "|" +
                DatosGarantia1?.TienePoliza.value + "|" +
                DatosGarantia2?.TipoGarantia.value + "|" +
                DatosGarantia2?.ValorRealizacion.value + "|" +
                DatosGarantia2?.TienePoliza.value + "|" +

                DatosAval1?.TipoAval.value + "|" +
                DatosAval1?.TipoDocumento.value + "|" +
                DatosAval1?.NroDocumento.value + "|" +
                DatosAval1?.ApePaterno.value + "|" +
                DatosAval1?.ApeMaterno.value + "|" +
                DatosAval1?.PrimerNombre.value + "|" +
                DatosAval1?.SegundoNombre.value + "|" +

                DatosInformeVisita?.Plazo.value + "|" +
                DatosInformeVisita?.FechaSiembra.value + "|" +

                DatosSolicitud?.NivelTecnologico.value + "|*|";


            return cadena;
        }

    }
}

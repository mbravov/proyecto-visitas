﻿using System;
using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class InfoGrupo
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string JuntaRegante { get; set; }
        public string Nivel { get; set; }
        public string PagoAsistenteTecnico { get; set; }
        public string Estado { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaEvaluacion { get; set; }
        public int HoraCreacion { get; set; }
        public int TotalIntegrantes { get; set; }
        public int TotalAprobados { get; set; }
        public int TotalRechazdos { get; set; }
        public int TotalEvaluacion { get; set; }
        public string Evaluacion_html { get; set; }
        public int TotalGeoreferencia { get; set; }
        public int TotalPropuesta { get; set; }
        public decimal MontoSolicitado { get; set; }
        public string TramaOpciones { get; set; }


    }
}


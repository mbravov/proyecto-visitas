﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class InformeVisita
    {
        public Cell Plazo { get; set; }
        public Cell FechaSiembra { get; set; }
        public Cell FechaSiembraOriginal { get; set; }

        //public Range Criterio { get; set; }
    }
}

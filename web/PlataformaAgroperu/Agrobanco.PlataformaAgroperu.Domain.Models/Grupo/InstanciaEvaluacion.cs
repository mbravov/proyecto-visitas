﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class InstanciaEvaluacion
    {
        public string Comentario { get; set; }
        public string Usuario { get; set; }
    }
}

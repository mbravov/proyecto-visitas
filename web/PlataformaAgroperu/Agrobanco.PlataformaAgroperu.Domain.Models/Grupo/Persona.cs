﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Persona
    {
        public Cell TipoDocumento { get; set; }
        public Cell NroDocumento { get; set; }
        public Cell ApePaterno { get; set; }
        public Cell ApeMaterno { get; set; }
        public Cell NomPrimer { get; set; }
        public Cell NomSegundo { get; set; }
        public Cell Sexo { get; set; }
        public Cell EstadoCivil { get; set; }
        public Cell Celular { get; set; }
        public Cell WhatsApp { get; set; }
        public Cell Experiencia { get; set; }
        public Cell NacimientoAnio { get; set; }
        public Cell NacimientoMes { get; set; }
        public Cell NacimientoDia { get; set; }
        public Cell Direccion { get; set; }
        public Cell Correo { get; set; }
        public string Password { get; set; }
        //public string getTramaCliente()
        //{
        //    return
        //        TipoDocumento.value + "|" +
        //        NroDocumento.value + "|" +
        //        ApePaterno.value + "|" +
        //        ApeMaterno.value + "|" +
        //        NomPrimer.value + "|" +
        //        NomSegundo.value + "|" +
        //        Sexo.value + "|" +
        //        EstadoCivil.value + "|" +
        //        Celular.value + "|" +
        //        Experiencia.value + "|" +
        //        NacimientoDia.value + "|" +
        //        NacimientoMes.value + "|" +
        //        NacimientoAnio.value + "|" +
        //        Correo.value;
        //}
        //public string getTramaConyugue()
        //{
        //    return
        //        TipoDocumento.value + "|" +
        //        NroDocumento.value;
        //}
    }

    public class DatosPersona
    {
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string NomPrimer { get; set; }
        public string NomSegundo { get; set; }
        public string Sexo { get; set; }
        public string EstadoCivil { get; set; }
        public string Celular { get; set; }
        public string Experiencia { get; set; }
        public string NacimientoAnio { get; set; }
        public string NacimientoMes { get; set; }
        public string NacimientoDia { get; set; }
        public string Direccion { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        //public string getTramaCliente()
        //{
        //    return
        //        TipoDocumento.value + "|" +
        //        NroDocumento.value + "|" +
        //        ApePaterno.value + "|" +
        //        ApeMaterno.value + "|" +
        //        NomPrimer.value + "|" +
        //        NomSegundo.value + "|" +
        //        Sexo.value + "|" +
        //        EstadoCivil.value + "|" +
        //        Celular.value + "|" +
        //        Experiencia.value + "|" +
        //        NacimientoDia.value + "|" +
        //        NacimientoMes.value + "|" +
        //        NacimientoAnio.value + "|" +
        //        Correo.value;
        //}
        //public string getTramaConyugue()
        //{
        //    return
        //        TipoDocumento.value + "|" +
        //        NroDocumento.value;
        //}
    }
}

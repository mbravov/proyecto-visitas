﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Predio
    {
        public Cell Departamento { get; set; }
        public Cell Provincia { get; set; }
        public Cell Distrito { get; set; }
        public Cell Referencia { get; set; }
        public Cell RegimenTenencia { get; set; }
        public Cell UnidadCatastral { get; set; }
        public Cell EstadoCampo { get; set; }
        public string Cultivo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }

        //public Range TipoUnidadFinanciamiento { get; set; }
        //public Range NroUnidadesTotales { get; set; }
        //public Range NroUnidadesFinanciamiento { get; set; }
        //public Range MontoSolicitadoSoles { get; set; }        
        //public Range EnvioCronograma { get; set; }
        //public Range Correo { get; set; }
        //public Range PagoAdelantado { get; set; }
        //public string getTrama()
        //{
        //    return Departamento.value + "|" + Provincia.value + "|" + Distrito.value + "|" + Referencia.value + "|" + RegimenTenencia.value + "|" + UnidadCatastral.value + "|" + EstadoCampo.value;
        //    //+ "|" + TipoUnidadFinanciamiento.value + "|" + NroUnidadesTotales.value + "|" + NroUnidadesFinanciamiento.value + "|" + MontoSolicitadoSoles.value + "|" + EnvioCronograma.value + "|" + Correo.value + "|" + PagoAdelantado.value;
        //}
    }
}

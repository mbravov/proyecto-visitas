﻿using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Propuesta
    {
        public string NroGrupo_00 { get; set; }
        public string NroPropuesta_01 { get; set; }
        public string FechaGenPropuesta_02 { get; set; }
        public string Estado_03 { get; set; }
        public string Agencia_04 { get; set; }
        public string Autonomia_05 { get; set; }
        public string Nombre_RzSocial_06 { get; set; }
        public string TipoDocumento_07 { get; set; }
        public string Cliente_08 { get; set; }
        public string Codigo_09 { get; set; }
        public string Edad_10 { get; set; }
        public string Departamento_11 { get; set; }
        public string Provincia_12 { get; set; }
        public string Distrito_13 { get; set; }
        public string Comision_14 { get; set; }
        public string Dir_Predio_14_1 { get; set; }
        public string Dir_solicitante_14_2 { get; set; }
        public string Producto_15 { get; set; }
        public string Destino_16 { get; set; }
        public string MontoSolicitado_17 { get; set; }
        public string Plazo_18 { get; set; }
        public string FormaPago_19 { get; set; }
        public string Moneda_20 { get; set; }
        public string Tasa_21 { get; set; }
        public string ProductoIBS_22 { get; set; }
        public string SegDegravamen_23 { get; set; }
        public string SegAgricola_24 { get; set; }
        public string Gestor_25 { get; set; }
        public string PorcentajeComisionGestor_26 { get; set; }
        public string ServicioGestor_27 { get; set; }
        public string NumGarantia_28 { get; set; }
        public string NumGarantia_28_1 { get; set; }

        public string Gar_Tipo_29 { get; set; }
        public string Gar_Tipo_29_1 { get; set; }
        public string Gar_Tipo_29_2_AVA { get; set; }
        public string Gar_Descripcion_30 { get; set; }
        public string Gar_Descripcion_30_1 { get; set; }
        public string Gar_Descripcion_30_1_AVA { get; set; }
        public string Gar_Documento_30_2_AVA { get; set; }
        public string Gar_Nombre_Avalador_30_3_AVA { get; set; }
        public string Gar_VG_31 { get; set; }
        public string Gar_VG_31_1 { get; set; }
        public string Gar_VRI_32 { get; set; }
        public string Gar_VRI_32_1 { get; set; }

        public string Gar_FTasacion_33 { get; set; }
        public string Gar_Poliza_34 { get; set; }
        public string Gar_Poliza_34_1 { get; set; }
        public string Gar_NroPartida_35 { get; set; }
        public string Gar_OfRegistral_36 { get; set; }
        public string Desembolso_Fini_37 { get; set; }
        public string Desembolso_Fini_37_1 { get; set; }

        public string Desembolso_Ffin_38 { get; set; }
        public string Desembolso_Ffin_38_1 { get; set; }
        public string Gar_Etapa_39 { get; set; }
        public string Gar_Etapa_39_1 { get; set; }
        public string Gar_Porcentaje_40 { get; set; }
        public string Gar_Porcentaje_40_1 { get; set; }
        public string Gar_FinxUP_41 { get; set; }
        public string Gar_FinxUP_41_1 { get; set; }
        public string Gar_UP_42 { get; set; }
        public string Gar_UP_42_1 { get; set; }
        public string Gar_TotalFin_43 { get; set; }
        public string Gar_TotalFin_43_1 { get; set; }
        public string CondicionesEspeciales_44 { get; set; }
        public string Comentarios1_45 { get; set; }
        public string Comentarios2_46 { get; set; }
        public string Fecha_47 { get; set; }
        public string Aprobado_48 { get; set; }
        // public string Rechazado_49 { get; set; }
        public string Nro_acta_49 { get; set; }

        //   public string Resolucion_50 { get; set; }



        public string Autonomia_51 { get; set; }
        public string FechaImpresion_52 { get; set; }
        public string Analista_53 { get; set; }
        public string Mora_54 { get; set; }
        public string P_R_REF_55 { get; set; }
        public string Mora_56 { get; set; }
        public string P_R_REF_57 { get; set; }


        public string Solicitante_58 { get; set; }
        public string Calif_SBS_59 { get; set; }
        public string Calif_Interna_60 { get; set; }
        public string NumeroEntidades_61 { get; set; }
        public string RLAFT_62 { get; set; }
        public string Patrimonio_63 { get; set; }
        public string OtrosIngresos_64 { get; set; }
        public string ExpoPropuesta_65 { get; set; }
        public string ExposicionVigente_66 { get; set; }

        public string ExposicionTotal_67 { get; set; }
        public string DeudaSistema_68 { get; set; }
        public string Conyuguea_69 { get; set; }
        public string Califsbs_70 { get; set; }
        public string Caliinterna_71 { get; set; }
        public string NumeroEntidades_72 { get; set; }
        public string Deudasistema_73 { get; set; }



        public string Producto_74 { get; set; }
        public string ExpSector_75 { get; set; }
        public string UnidadProd_76 { get; set; }
        public string UP_Total_77 { get; set; }
        public string UP_Financiar_78 { get; set; }
        public string CalificacionLAFT_78A { get; set; }

        public string ProduccionxUP_79 { get; set; }
        public string UnidadMedida_80 { get; set; }

        public string ProduccionTotal_81 { get; set; }
        public string Precio_82 { get; set; }
        public string Costo_83 { get; set; }
        public string Ingreso_84 { get; set; }
        public string Costo_85 { get; set; }
        public string BeneficioCosto_86 { get; set; }
        public string HASposee_87 { get; set; }
        public string HASpermiso_agua_88 { get; set; }
        public string HASriego_cultivo_89 { get; set; }
        public string HASfinanciadas_90 { get; set; }

        public string EsPEP { get; set; }
        public string PagoAsistenteTecnico { get; set; }
        public int AprobadoExcepcion { get; set; }
        public string ComentarioAnalista { get; set; }
        public string ObservacionJefe { get; set; }


        public List<InstanciaEvaluacion> linstancias { get; set; }
        public List<PlanDesembolso> PlanDesembolsos { get; set; }


    }

    public class PlanDesembolso
    {
        public string Solicitud { get; set; }
        public int NumeroDesembolso { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string DescripcionEtapa { get; set; }
        public string Porcentaje { get; set; }
        public string TotalFinanciado { get; set; }
    }
}

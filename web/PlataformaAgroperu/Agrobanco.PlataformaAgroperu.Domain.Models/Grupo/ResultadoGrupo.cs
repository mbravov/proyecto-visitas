﻿namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class ResultadoGrupo
    {
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string NombreIntegrante { get; set; }
        public string Solicitud { get; set; }
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
    }
}

﻿
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Grupo
{
    public class Solicitud
    {
        public Cell Sector { get; set; }
        public Cell Programa { get; set; }
        public Cell Cultivo { get; set; }
        public Cell TipoUnidadFinanciamiento { get; set; }
        public Cell NroUnidadesTotales { get; set; }
        public Cell NroUnidadesFinanciamiento { get; set; }
        public Cell NroUnidadesConduccion { get; set; }
        public string Moneda { get; set; }
        public Cell MontoSolicitadoSoles { get; set; }
        public Cell EnvioCronograma { get; set; }
        public Cell PagoAdelantado { get; set; }
        public Cell NivelTecnologico { get; set; }
        public string FormaAbono { get; set; }
        public string Banco { get; set; }
        public string CCI { get; set; }

        //public string getTrama()
        //{
        //    return TipoUnidadFinanciamiento.value + "|" + NroUnidadesTotales.value + "|" + NroUnidadesFinanciamiento.value + "|" + MontoSolicitadoSoles.value + "|" + EnvioCronograma.value + "|"  + PagoAdelantado.value;
        //}
    }
}

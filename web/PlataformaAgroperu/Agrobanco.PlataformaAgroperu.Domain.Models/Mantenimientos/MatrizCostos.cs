﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos
{
    public class MatrizCostos
    {
        public int Codigo { get; set; }
        public string Sector { get; set; }
        public string SectorDescripcion { get; set; }
        public string Programa { get; set; }
        public string NivelTecnologico { get; set; }
        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public string TipoProducto { get; set; }
        public string Priorizacion { get; set; }
        public string UnidadFinanciamiento { get; set; }
        public string Ubigeo { get; set; }  
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public decimal Rendimiento { get; set; }
        public string UnidadRendimiento { get; set; }
        public decimal PrecioVentaChacra { get; set; }
        public string UnidadPrecioVenta { get; set; }
        public decimal CostoProduccion { get; set; }
        public string UnidadCosto { get; set; }
        public int PeriodoFenologico { get; set; }
        public int PeriodoPostCosecha { get; set; }
        public int FechaInicioCampana { get; set; }
        public int FechaFinCampana { get; set; }
        public int Campana { get; set; }
        public int SiembraMesInicio { get; set; }
        public int SiembraMesFin { get; set; }
        public int CosechaMesInicio { get; set; }
        public int CosechaMesFin { get; set; }

        public decimal MontoFinanciar { get; set; }
        public decimal PorcentajeFinanciar { get; set; }
        public decimal AsistenciaTecnica { get; set; }
        public decimal AsistenciaTecnicaPorcentaje { get; set; }
        public int Plazo { get; set; }
        public string TipoCronograma { get; set; }
        public int MaximoCuotas { get; set; }
        public int MaximoDiasGracia { get; set; }
        public string CronogramasPermitidos { get; set; }

        public int NumeroDesembolsos { get; set; }
        public string TramaDesembolsoPorcentajes { get; set; }
        public string TramaDesembolsoMesInicio { get; set; }
        public string TramaDesembolsoMesFin { get; set; }

        public int NumeroPagosAT { get; set; }
        public string TramaPagosATPorcentajes { get; set; }
        public string TramaPagosATDias { get; set; }

        public int NumeroInfoVisitas { get; set; }
        public string TramaInfoVisitaMeses { get; set; }

        public List<MatrizCostoDetalle> Desembolsos { get; set; }
        public List<MatrizCostoDetalle> PagosAsistenteTecnico { get; set; }

        public string Opciones { get; set; }
        public string Estado { get; set; }


        public MatrizCostos() { }
    }

    public class MatrizCostoDetalle
    {
        public int Secuencia { get; set; }
        public int Valor1 { get; set; }
        public int Valor2 { get; set; }
        public decimal Porcentaje { get; set; }
    }
}

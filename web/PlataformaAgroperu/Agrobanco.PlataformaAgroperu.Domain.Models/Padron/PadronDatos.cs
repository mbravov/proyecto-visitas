﻿
namespace Agrobanco.PlataformaAgroperu.Domain.Models.Padron
{
    public class PadronDatos
    {
        public int CodigoPadron { get; set; }
        public string IdAsPadron { get; set; }        
        public string NombrePadron { get; set; }
        public string NombreJunta { get; set; }
        public string NombreComision { get; set; }
        public int NroRegistros { get; set; }
        public int NroAprobados { get; set; }
        public string Estado { get; set; }
        public string Usuario { get; set; }
        public string Agencia { get; set; }        
        public string FechaCreacion { get; set; }


    }
}

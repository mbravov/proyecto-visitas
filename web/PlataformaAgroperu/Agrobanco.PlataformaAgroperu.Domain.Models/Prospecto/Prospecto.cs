﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Prospecto
{
    public class Prospecto
    {
        public ProspectoDatos DatosProspecto { get; set; }
        public List<ProspectoIntegranteItem> Integrantes { get; set; }
        public List<ProspectoIntegrante> ExcelIntegrantes { get; set; }
        public string oTrama { get; set; }
    }
}

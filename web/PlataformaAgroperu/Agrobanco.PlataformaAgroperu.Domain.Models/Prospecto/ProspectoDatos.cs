﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Prospecto
{
    public class ProspectoDatos
    {
        public int CodigoProspecto { get; set; }
        public string IdAsProspecto { get; set; }
        public string Nombre { get; set; }
        public int Estado { get; set; }
        public string Usuario { get; set; }
        public string Agencia { get; set; }
        public int NroRegistros { get; set; }
        public string FechaCreacion { get; set; }

    }
}

﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Domain.Models.Prospecto
{
    public class ProspectoIntegrante
    {
        public Cell TipoDocumento { get; set; }
        public Cell NumeroDocumento { get; set; }
        public Cell ApPaterno { get; set; }
        public Cell ApMaterno { get; set; }
        public Cell PrimerNombre { get; set; }
        public Cell SegundoNombre { get; set; }
        public Cell Celular { get; set; }
        public Cell Cony_TipoDocumento { get; set; }
        public Cell Cony_NumeroDocumento { get; set; }   
        public string getTrama()
        {
            return TipoDocumento.value + "|" + NumeroDocumento.value + "|" + ApPaterno.value + "|" + ApMaterno.value + "|" + PrimerNombre.value + "|" + SegundoNombre.value +
                "|" + Celular.value + "|" + Cony_TipoDocumento.value + "|" + Cony_NumeroDocumento.value;
        }
    }
    public class ProspectoIntegranteItem
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string Celular { get; set; }
        public string Cony_TipoDocumento { get; set; }
        public string Cony_NumeroDocumento { get; set; }
        public ProspectoIntegranteItem(ProspectoIntegrante obj) {
            TipoDocumento = obj.TipoDocumento.value;
            NumeroDocumento = obj.NumeroDocumento.value;
            ApPaterno = obj.ApPaterno.value;
            ApMaterno = obj.ApMaterno.value;
            PrimerNombre = obj.PrimerNombre.value;
            SegundoNombre = obj.SegundoNombre.value;
            Celular = obj.Celular.value;
            Cony_TipoDocumento = obj.Cony_TipoDocumento.value;
            Cony_NumeroDocumento = obj.Cony_NumeroDocumento.value;
        }
    }
    //public class ProspectoComparer : IEqualityComparer<ProspectoIntegrante>
    //{
    //    public bool Equals(ProspectoIntegrante x, ProspectoIntegrante y)
    //    {
    //        if (x.NumeroDocumento == y.NumeroDocumento)
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //    public int GetHashCode(ProspectoIntegrante obj)
    //    {
    //        return obj.NumeroDocumento.GetHashCode();
    //    }
    //    //public bool Equals(ProspectoIntegrante x, ProspectoIntegrante y)
    //    //{
    //    //    //Check whether the compared objects reference the same data.
    //    //    if (Object.ReferenceEquals(x, y)) return true;

    //    //    //Check whether any of the compared objects is null.
    //    //    if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
    //    //        return false;

    //    //    //Check whether the products' properties are equal.
    //    //    return x.NumeroDocumento == y.NumeroDocumento && x.TipoDocumento == y.TipoDocumento;
    //    //}
    //    //public int GetHashCode(ProspectoIntegrante oProspecto)
    //    //{
    //    //    //Check whether the object is null
    //    //    if (Object.ReferenceEquals(oProspecto, null)) return 0;

    //    //    //Get hash code for the Name field if it is not null.
    //    //    int hashProspectoDocumento = oProspecto.NumeroDocumento == null ? 0 : oProspecto.NumeroDocumento.GetHashCode();

    //    //    //Get hash code for the Code field.
    //    //    int hashProspectoTipoDocumento = oProspecto.TipoDocumento.GetHashCode();

    //    //    //Calculate the hash code for the product.
    //    //    return hashProspectoDocumento ^ hashProspectoTipoDocumento;
    //    //}

    //}
}

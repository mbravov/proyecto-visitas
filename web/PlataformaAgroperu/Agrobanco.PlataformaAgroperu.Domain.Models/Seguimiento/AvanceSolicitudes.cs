﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agrobanco.PlataformaAgroperu.Domain.Models.Seguimiento
{
    public class AvanceSolicitudes
    {
        public int CodigoAgencia { get; set; }
        public string Agencia { get; set; }
        public string CodigoAnalista { get; set; }
        public string NombreAnalista { get; set; }
        public decimal MontoTotalSolicitado { get; set; }
        public int NroTotalSolicitudes { get; set; }
        public int NumeroEvaluacion { get; set; }
        public decimal MontoEvaluacion { get; set; }
        public int NumeroGeoreferencia { get; set; }
        public decimal MontoGeoreferencia { get; set; }
        public int NumeroPropuesta { get; set; }
        public decimal MontoPropuesta { get; set; }
        public int NroPropuestaGenerada { get; set; }
        public decimal MontoPropuestaGenerada { get; set; }
        public decimal NroDesembolsoMes { get; set; }
        public decimal MontoDesembolsoMes { get; set; }
    }

    public class SegModelCharts
    {
        public string[] Labels { get; set; }
        public SegIndicadores SetDatosDetalle { get; set; }
        public SegIndicadores SetDatosTotal { get; set; }
        public SegModelCharts()
        {
            SetDatosTotal = new SegIndicadores();
            SetDatosDetalle = new SegIndicadores();
        }

        public SegModelCharts(List<SegIndicadores> IndicadoresDetalle, List<SegIndicadores> IndicadoresTotal)
        {
            SetDatosTotal = new SegIndicadores();
            SetDatosDetalle.SetArray(IndicadoresDetalle);
            SetDatosTotal.SetArray(IndicadoresTotal);
        }

        public void SetIndicadores(List<SegIndicadores> IndicadoresDetalle, SegIndicadores IndicadoresTotal)
        {
            SetDatosDetalle.SetArray(IndicadoresDetalle);
            SetDatosTotal = IndicadoresTotal;
        }

    }

    public class SegReporteSolcitudes
    {
        public string key { get; set; }
        public string Sector { get; set; }
        public string Estado { get; set; }
        public decimal Numero { get; set; }
        public decimal MontoSolicitado { get; set; }
        public decimal Saldo { get; set; }
        public decimal Extorno { get; set; }
    }

    public class SegIndicadores
    {
        public string key { get; set; }
        public SegEstado Agricola { get; set; }
        public SegEstado Pecuario { get; set; }
        public SegEstado Forestal { get; set; }
        //public Montos APF_Sector { get; set; }

        public SegSector Admision { get; set; }
        public SegSector Propuesta { get; set; }
        public SegSector Aprobado { get; set; }
        public SegSector Desembolsado { get; set; }
        public SegSector Anulado { get; set; }

        public Montos Totales { get; set; }

        public SegIndicadores()
        {
            Agricola = new SegEstado();
            Pecuario = new SegEstado();
            Forestal = new SegEstado();

            Admision = new SegSector();
            Propuesta = new SegSector();
            Aprobado = new SegSector();
            Desembolsado = new SegSector();
            Anulado = new SegSector();


        }

        public void SetAgricola(string Estado, Montos Montos)
        {
            switch (Estado)
            {
                case "1": Agricola.Admision = Admision.Agricola = Montos; break;
                case "8": Agricola.Propuesta = Propuesta.Agricola = Montos; break;
                case "82": Agricola.Aprobada = Aprobado.Agricola = Montos; break;
                case "10": Agricola.Desembolsado = Desembolsado.Agricola = Montos; break;
                case "13": Agricola.Anulados = Anulado.Agricola = Montos; break;
                default:
                    break;
            }
        }

        public void SetPecuario(string Estado, Montos totales)
        {
            switch (Estado)
            {
                case "1": Pecuario.Admision = Admision.Pecuario = totales; break;
                case "8": Pecuario.Propuesta = Propuesta.Pecuario = totales; break;
                case "82": Pecuario.Aprobada = Aprobado.Pecuario = totales; break;
                case "10": Pecuario.Desembolsado = Desembolsado.Pecuario = totales; break;
                case "13": Pecuario.Anulados = Anulado.Pecuario = totales; break;
                default:
                    break;
            }
        }

        public void SetForestal(string Estado, Montos totales)
        {
            switch (Estado)
            {
                case "1": Forestal.Admision = Admision.Forestal = totales; break;
                case "8": Forestal.Propuesta = Propuesta.Forestal = totales; break;
                case "82": Forestal.Aprobada = Aprobado.Forestal = totales; break;
                case "10": Forestal.Desembolsado = Desembolsado.Forestal = totales; break;
                case "13": Forestal.Anulados = Anulado.Forestal = totales; break;
                default:
                    break;
            }
        }

        public void SetTotales()
        {
            Agricola.SetTotal();
            Pecuario.SetTotal();
            Forestal.SetTotal();
            Admision.SetTotal();
            Propuesta.SetTotal();
            Aprobado.SetTotal();
            Desembolsado.SetTotal();
            Anulado.SetTotal();
            
        }

        public void SetArray(IEnumerable<SegIndicadores> Data)
        {
            Agricola.SetArray(Data.Select(x => x.Agricola));
            Pecuario.SetArray(Data.Select(x => x.Pecuario));
            Forestal.SetArray(Data.Select(x => x.Forestal));

            Admision.SetArray(Data.Select(x => x.Admision));
            Propuesta.SetArray(Data.Select(x => x.Propuesta));
            Aprobado.SetArray(Data.Select(x => x.Aprobado));
            Desembolsado.SetArray(Data.Select(x => x.Desembolsado));
            Anulado.SetArray(Data.Select(x => x.Anulado));
        }

    }

    public class SegSector
    {
        public Montos Agricola { get; set; }
        public Montos Pecuario { get; set; }
        public Montos Forestal { get; set; }
        public Montos Total { get; set; }

        public SegSector()
        {
            Agricola = new Montos();
            Pecuario = new Montos();
            Forestal = new Montos();
            Total = new Montos();
        }
        public SegSector(Montos _Agricola, Montos _Pecuario, Montos _Forestal)
        {
            Agricola = _Agricola;
            Pecuario = _Pecuario;
            Forestal = _Forestal;
            Total = new Montos(Agricola.NumeroSolicitudes + Pecuario.NumeroSolicitudes + Forestal.NumeroSolicitudes, 
                Agricola.MontoSolicitado + Pecuario.MontoSolicitado + Forestal.MontoSolicitado,
                Agricola.Saldo + Pecuario.Saldo + Forestal.Saldo,
                Agricola.Extorno + Pecuario.Extorno + Forestal.Extorno);
        }

        public void SetTotal()
        {
            Total = new Montos(
                Agricola.NumeroSolicitudes + Pecuario.NumeroSolicitudes + Forestal.NumeroSolicitudes,
                Agricola.MontoSolicitado + Pecuario.MontoSolicitado + Forestal.MontoSolicitado,
                Agricola.Saldo + Pecuario.Saldo + Forestal.Saldo,
                Agricola.Extorno + Pecuario.Extorno + Forestal.Extorno);
        }

        public void SetArray(IEnumerable<SegSector> Estados)
        {
            Agricola.SetArray(Estados.Select(x => x.Agricola));
            Pecuario.SetArray(Estados.Select(x => x.Pecuario));
            Forestal.SetArray(Estados.Select(x => x.Forestal));
            Total.SetArray(Estados.Select(x => x.Total));
        }
    }

    public class SegEstado
    {
        public Montos Admision { get; set; }
        public Montos Propuesta { get; set; }
        public Montos Aprobada { get; set; }
        public Montos Desembolsado { get; set; }
        public Montos Anulados { get; set; }
        public Montos Total { get; set; }

        public SegEstado()
        {
            Admision = new Montos();
            Propuesta = new Montos();
            Aprobada = new Montos();
            Desembolsado = new Montos();
            Anulados = new Montos();
            Total = new Montos();
        }

        public SegEstado(Montos _Admision, Montos _Propuesta, Montos _Aprobada, Montos _Desembolsado,
            Montos _Anulados, Montos _Total)
        {
            Admision = _Admision;
            Propuesta = _Propuesta;
            Aprobada = _Aprobada;
            Desembolsado = _Desembolsado;
            Anulados = _Anulados;
            Total = _Total;
        }

        public void SetTotal()
        {
            Total = new Montos(Admision.NumeroSolicitudes + Propuesta.NumeroSolicitudes + Aprobada.NumeroSolicitudes + Desembolsado.NumeroSolicitudes,
                Admision.MontoSolicitado + Propuesta.MontoSolicitado + Aprobada.MontoSolicitado + Desembolsado.MontoSolicitado,
                Desembolsado.Saldo, Desembolsado.Extorno); // Saldo y Desembolso Solo para estado Desembolsado
        }

        public void SetArray(IEnumerable<SegEstado> Sector)
        {
            Admision.SetArray(Sector.Select(x => x.Admision));
            Propuesta.SetArray(Sector.Select(x => x.Propuesta));
            Aprobada.SetArray(Sector.Select(x => x.Aprobada));
            Desembolsado.SetArray(Sector.Select(x => x.Desembolsado));
            Anulados.SetArray(Sector.Select(x => x.Anulados));
            Total.SetArray(Sector.Select(x => x.Total));
        }

    }

    public class Montos
    {
        public decimal NumeroSolicitudes { get; set; }
        public decimal MontoSolicitado { get; set; }
        public decimal Saldo { get; set; }
        public decimal Extorno { get; set; }

        public decimal[] _NumeroSolicitudes { get; set; }
        public decimal[] _MontoSolicitado { get; set; }
        public decimal[] _Saldo { get; set; }
        public decimal[] _Extorno { get; set; }

        public Montos()
        {
            NumeroSolicitudes = 0;
            MontoSolicitado = 0;
        }

        public Montos(decimal _Numero, decimal _MontoSolicitado, decimal _Saldo, decimal _Extorno)
        {
            NumeroSolicitudes = _Numero;
            MontoSolicitado = _MontoSolicitado;
            Saldo = _Saldo;
            Extorno = _Extorno;
        }

        public void SetArray(IEnumerable<Montos> montos)
        {
            _NumeroSolicitudes = montos.Select(x => x.NumeroSolicitudes).ToArray();
            _MontoSolicitado = montos.Select(x => x.MontoSolicitado).ToArray();
            _Saldo = montos.Select(x => x.Saldo).ToArray();
            _Extorno = montos.Select(x => x.Extorno).ToArray();
        }

    }



    
    

}

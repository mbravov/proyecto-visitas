﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;

namespace Agrobanco.PlataformaAgroperu.Domain.Common
{
    public class CommonValidations
    {
        #region Constantes
        //private const string SectorAgricola = "1";
        //private const string SectorPecuario = "2";
        //private const string SectorForestal = "3";

        //private const string ProgramaGenerico = "0001";
        //private const string ProgramaMadreDios = "0002";

        //private const string TecnologiaAlta = "1";
        //private const string TecnologiaMedia = "2";
        //private const string TecnologiaTradicional = "3";
        #endregion

        public static bool TipoDocumento(string TipoDocumento)
        {
            return Array.IndexOf(Codigos.TipoDocumentos,TipoDocumento) == -1 ? false : true;
        }
        public static bool TipoDocumento(string TipoDocumento, out string message)
        {   
            var result = Array.IndexOf(Codigos.TipoDocumentos, TipoDocumento) == -1 ? false : true;
            message = result ? string.Empty : MessageError.TipoDocumento;
            return result;
        }
        public static bool NumeroDocumento(string Numero, string TipoDocumento)
        {
            if (TipoDocumento == "DNI")            
                return StringValidation.OnlyNumbers(Numero, 8, true);
            
            if (TipoDocumento == "RUC")
                return StringValidation.OnlyNumbers(Numero, 11, true);

            return false;
        }
        public static bool NumeroDocumento(string Numero, string TipoDocumento, out string message)
        {
            var ok = false;
            if (TipoDocumento == "DNI")
            {
                ok = StringValidation.OnlyNumbers(Numero, 8, true);
                message = MessageError.NumeroDNI;
            }
            else if(TipoDocumento == "RUC")
            {
                ok = StringValidation.OnlyNumbers(Numero, 11, true);
                message = MessageError.NumeroRUC;
            }
            else
            {
                message = MessageError.NumeroDocumento;
            }                

            return ok;
        }
        public static bool Lista(string[] codigos, string value, out string messageOut, bool empty = false)
        {
            var result = empty;
            if (!string.IsNullOrWhiteSpace(value))            
                result = Array.IndexOf(codigos, value) == -1 ? false : true;            
            messageOut = result ? string.Empty : codigos[0];
            return result;
        }
        public static bool Lista(string[] codigos, string value, bool empty = false)
        {
            var result = empty;
            if (!string.IsNullOrWhiteSpace(value))
                result = Array.IndexOf(codigos, value) == -1 ? false : true;
            return result;
        }

        #region Validaciones Con Class Cell
        // GENERICAS
        public static void ValidarSoloLetras(Cell cell, ref ExcelResponseModel errors, string DescripcionCell,
                              int MaxLength, bool EqualMax = false, int MinLength = 0, bool empty = false)
        {
            var msjObligatorio = empty ? "" : " es obligatorio,";
            
            if (!StringValidation.OnlyLetters(cell.value, MaxLength, EqualMax, MinLength, empty))
            {
                var mensaje = $"{DescripcionCell}{msjObligatorio} debe contener solo letras y tener un máximo de {MaxLength} caracteres";
                errors.listErrors.Add(new ExcelErrors(cell, mensaje));
            }
        }

        public static void ValidarSoloNumeros(Cell cell, ref ExcelResponseModel errors, string DescripcionCell,
                                       int MaxLength, bool EqualMax = false, int MinLength = 0, bool empty = false)
        {
            var msjObligatorio = empty ? "" : " es obligatorio,";
            if (!StringValidation.OnlyNumbers(cell.value, MaxLength, EqualMax, MinLength, empty))
            {
                var mensaje = $"{DescripcionCell}{msjObligatorio} debe contener solo números y tener un máximo de {MaxLength} caracteres";
                errors.listErrors.Add(new ExcelErrors(cell, mensaje));
            }
        }

        public static void ValidarDatoNumerico(Cell cell, ref ExcelResponseModel errors, string DescripcionCell,
                                       decimal MinValue = decimal.MinValue, decimal MaxValue = decimal.MaxValue, bool AllowZero = true, bool empty=false)
        {
            var msjObligatorio = empty ? "" : " es obligatorio,";
            if (!StringValidation.IsNumeric(cell.value, MinValue, MaxValue, AllowZero, empty))
            {
                var mensaje = $"{DescripcionCell}{msjObligatorio} debe ser un dato numérico";
                errors.listErrors.Add(new ExcelErrors(cell, mensaje));
            }
        }

        public static void ValidarLongitudCedena(Cell cell, ref ExcelResponseModel errors, string DescripcionCell,
                              int MaxLength, bool EqualMax = false, int MinLength = 0)
        {
            var msjObligatorio = MinLength > 0 ? " es obligatorio," : "";
            if (!StringValidation.Length(cell.value, MaxLength, EqualMax, MinLength))
            {
                var mensaje = $"{DescripcionCell}{msjObligatorio} debe contener solo un máximo de {MaxLength} caracteres";
                errors.listErrors.Add(new ExcelErrors(cell, mensaje));
            }
        }

        public static void ValidarLista(string[] codigos, Cell cell, ref ExcelResponseModel errors, bool empty = false)
        {
            var msjObligatorio = empty ? "" : ", este dato es obligatorio";
            var result = CommonValidations.Lista(codigos, cell.value, out string mensaje, empty);
            if (!result)
                errors.listErrors.Add(new ExcelErrors(cell, $"{mensaje}{msjObligatorio}"));
        }

        public static void ValidarFecha(Cell cellDia, Cell cellMes, Cell cellAnio, ref ExcelResponseModel errors, string DescripcionCell,
                                 bool empty = false)
        {
            var Cadena = $"{cellDia.value}{cellMes.value}{cellAnio.value}";
            if (String.IsNullOrWhiteSpace(Cadena))
            {
                if (!empty)
                {
                    var mensaje = $"{DescripcionCell} es obligatoria, favor verificar";
                    errors.listErrors.Add(new ExcelErrors(cellDia, mensaje));
                }
            }
            else
            {
                var fecha = $"{cellDia.value}-{cellMes.value}-{cellAnio.value}";
                if (!StringValidation.IsDate(fecha))
                {
                    var mensaje = $"{DescripcionCell} inválida, favor verificar fecha ingresada: " +
                        $"dia: {cellDia.value}, mes: {cellMes.value}:, año: {cellAnio.value}";
                    //errors.listErrors.Add(new ExcelErrors(fecha, mensaje, cellDia.row.ToString(), cellDia.column.ToString()));
                    errors.listErrors.Add(new ExcelErrors(cellDia, fecha, mensaje));
                }
            }
        }

        public static void ValidarFecha(Cell cell, ref ExcelResponseModel errors, string DescripcionCell,
                         bool empty = false)
        {
            
            if (String.IsNullOrWhiteSpace(cell.value))
            {
                if (!empty)
                {
                    var mensaje = $"{DescripcionCell} es obligatoria, favor verificar";
                    errors.listErrors.Add(new ExcelErrors(cell, mensaje));
                }
            }
            else
            {
                var Longitud = cell.value.Length;
                
                if (Longitud != 10)
                {
                    var mensaje = $"{DescripcionCell} inválida, favor verificar el formato de fecha ingresada";
                    errors.listErrors.Add(new ExcelErrors(cell, mensaje));
                }                
                else
                {
                    var Separador1 = cell.value.Substring(2, 1)?.ToString();
                    var Separador2 = cell.value.Substring(5, 1)?.ToString();
                    var fecha = $"{cell.value.Substring(0, 2)}-{cell.value.Substring(3, 2)}-{cell.value.Substring(6, 4)}";

                    if (Separador1 != "/" || Separador2 != "/")
                    {
                        var mensaje = $"{DescripcionCell} inválida, favor verificar el formato de fecha ingresada";
                        errors.listErrors.Add(new ExcelErrors(cell, mensaje));
                    }
                    else
                    {
                        if (!StringValidation.IsDate(fecha))
                        {
                            var mensaje = $"{DescripcionCell} inválida, favor verificar fecha ingresada";
                            errors.listErrors.Add(new ExcelErrors(cell, mensaje));
                        }
                    }

                    
                    
                }
                


                
            }
        }

        // VALIDACIONES ESPECÍFICAS

        public static void ValidarConyugue(string EstadoCivil, Persona Conyugue, ref ExcelResponseModel errors)
        {
            if(EstadoCivil == "2" || EstadoCivil == "5")
            {
                var messageErrors = "";
                var msjObligatorio = ", este dato es obligatorio de acuerdo al estado civil ingresado";
                if (!CommonValidations.TipoDocumento(Conyugue.TipoDocumento.value, out messageErrors))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.TipoDocumento, $"{messageErrors}{msjObligatorio}"));

                if (!CommonValidations.NumeroDocumento(Conyugue.NroDocumento.value, Conyugue.TipoDocumento.value, out messageErrors))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.NroDocumento, $"{messageErrors}{msjObligatorio}"));

                CommonValidations.ValidarSoloLetras(Conyugue.ApePaterno, ref errors, "Apellido Paterno", MaxLength: 30, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Conyugue.ApeMaterno, ref errors, "Apellido Materno", MaxLength: 30, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Conyugue.NomPrimer, ref errors, "Primer Nombre", MaxLength: 45, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Conyugue.NomSegundo, ref errors, "Segundo Nombre", MaxLength: 45, empty: true);
            }
            else
            {
                var mensaje = $"De acuerdo al Estado Civil, no es necesario ingresar datos de Conyugue, verifique.";
                if (!StringValidation.Length(Conyugue.TipoDocumento.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.TipoDocumento, mensaje));

                if (!StringValidation.Length(Conyugue.NroDocumento.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.NroDocumento, mensaje));

                if (!StringValidation.Length(Conyugue.ApePaterno.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.ApePaterno, mensaje));

                if (!StringValidation.Length(Conyugue.ApeMaterno.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.ApeMaterno, mensaje));

                if (!StringValidation.Length(Conyugue.NomPrimer.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.ApePaterno, mensaje));

                if (!StringValidation.Length(Conyugue.NomSegundo.value, 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(Conyugue.ApeMaterno, mensaje));
            }
        }
        public static void ValidarGarantia(Garantia garantia, ref ExcelResponseModel errors)
        {
            if (!String.IsNullOrWhiteSpace(garantia.TipoGarantia.value))
            {
                CommonValidations.ValidarDatoNumerico(garantia.ValorRealizacion, ref errors, "Valor Graven", AllowZero: false);
                CommonValidations.ValidarLista(Codigos.TienePoliza, garantia.TienePoliza, ref errors);
            }
            else
            {                
                if (!StringValidation.Length(garantia.ValorRealizacion.value.Trim(), 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(garantia.TipoGarantia, 
                                                          "Para ingresar Valor de Realización, es necesario un Tipo de Garantía."));
                if (!StringValidation.Length(garantia.TienePoliza.value.Trim(), 0, EqualMax: true))
                    errors.listErrors.Add(new ExcelErrors(garantia.TipoGarantia,
                                                          "Para ingresar un dato en Póliza, es necesario un Tipo de Garantía."));
            }
        }
        public static void ValidarAval(Aval Aval, ref ExcelResponseModel response)
        {
            if (!String.IsNullOrWhiteSpace(Aval.TipoAval.value))
            {
                if (!CommonValidations.TipoDocumento(Aval.TipoDocumento.value, out string mensaje))
                    response.listErrors.Add(new ExcelErrors(Aval.TipoDocumento, $"{mensaje}"));

                if (!CommonValidations.NumeroDocumento(Aval.NroDocumento.value, Aval.TipoDocumento.value, out mensaje))
                    response.listErrors.Add(new ExcelErrors(Aval.TipoDocumento, $"{mensaje}"));

                CommonValidations.ValidarSoloLetras(Aval.ApePaterno, ref response, "Apellido Paterno Aval", MaxLength: 30, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Aval.ApeMaterno, ref response, "Apellido Materno Aval", MaxLength: 30, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Aval.PrimerNombre, ref response, "Primer Nombre", MaxLength: 45, MinLength: 2);
                CommonValidations.ValidarSoloLetras(Aval.SegundoNombre, ref response, "Segundo Nombre", MaxLength: 45, empty: true);
            }
            else
            {
                if (!StringValidation.Length(Aval.TipoDocumento.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.TipoDocumento,
                                                          "Para ingresar Tipo Documento Aval, es necesario un Tipo de Aval."));
                if (!StringValidation.Length(Aval.NroDocumento.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.NroDocumento,
                                                          "Para ingresar Documento Aval, es necesario un Tipo de Aval."));
                if (!StringValidation.Length(Aval.ApePaterno.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.ApePaterno,
                                                          "Para ingresar Apellido Pateno Aval, es necesario un Tipo de Aval."));
                if (!StringValidation.Length(Aval.ApeMaterno.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.ApeMaterno,
                                                          "Para ingresar Apellido Materno Aval, es necesario un Tipo de Aval."));
                if (!StringValidation.Length(Aval.PrimerNombre.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.PrimerNombre,
                                                          "Para ingresar Nombre Aval, es necesario un Tipo de Aval."));
                if (!StringValidation.Length(Aval.SegundoNombre.value.Trim(), 0, EqualMax: true))
                    response.listErrors.Add(new ExcelErrors(Aval.SegundoNombre,
                                                          "Para ingresar Nombre Aval, es necesario un Tipo de Aval."));
            }
        }
        public static void ReglasPorMontoSolicitado_FondoAgroperu(
            Aval Aval, Garantia Garantia, Cell Tenencia, string Sector, string Programa, string MontoSolicitado, ref ExcelResponseModel response)
        {
            if (CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value))            
                if (StringValidation.IsNumeric(MontoSolicitado, out decimal montoSolicitadoOut))               
                    if (!StringValidation.IsNumeric(Garantia.ValorRealizacion.value, MinValue: montoSolicitadoOut * 1.5m))                    
                        response.listErrors.Add(new ExcelErrors(Garantia.ValorRealizacion,
                            $"El Valor Gravamen debe de ser mayor o igual al 1.5 del Monto Solicitado"));

            switch (Sector)
            {
                case ApplicationConstants.SectorAgricola:

                    if (StringValidation.IsNumeric(MontoSolicitado, 20001m, 30000m)) // Nivel 2
                    {
                        if (!CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value, out string mensaje2030))
                            response.listErrors.Add(new ExcelErrors(Garantia.TipoGarantia,
                                $"{mensaje2030}, de acuerdo al Monto Solicitado, este dato es obligatorio."));

                        if (Tenencia.value == Codigos.RegimenTenencia[1])
                            if (!CommonValidations.Lista(Codigos.TipoAval, Aval.TipoAval.value, out string mensaje2))
                                response.listErrors.Add(new ExcelErrors(Aval.TipoAval,
                                    $"{mensaje2}, de acuerdo al Regimen tenencia, este dato es obligatorio."));
                    }
                    break;
                    

                case ApplicationConstants.SectorForestal:

                    if (StringValidation.IsNumeric(MontoSolicitado, 0, 42000m)) // Nivel 1
                        if (!CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value))
                        {
                            if (!CommonValidations.Lista(Codigos.TipoFianza, Aval.TipoAval.value, out string mensaje))
                                response.listErrors.Add(new ExcelErrors(Aval.TipoAval,
                                     $"{mensaje}, de acuerdo al Monto Solicitado, este dato es obligatorio."));
                        }

                    if (StringValidation.IsNumeric(MontoSolicitado, 42001m, 142000m)) // Nivel 2
                    {
                        if (!CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value, out string mensaje2))
                            response.listErrors.Add(new ExcelErrors(Garantia.TipoGarantia,
                                $"{mensaje2}, de acuerdo al Monto Solicitado, este dato es obligatorio."));

                        if (!CommonValidations.Lista(Codigos.TipoFianza, Aval.TipoAval.value, out string mensaje))
                            response.listErrors.Add(new ExcelErrors(Aval.TipoAval,
                                 $"{mensaje}, de acuerdo al Monto Solicitado, este dato es obligatorio."));

                    }

                    break;
                default: // Pecuario
                    //if (StringValidation.IsNumeric(MontoSolicitado, 10001m, 20000m)) // Nivel 2
                    //    if (!CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value))
                    //    {
                    //        if (!CommonValidations.Lista(Codigos.TipoFianza, Aval.TipoAval.value, out string mensaje1020))
                    //            response.listErrors.Add(new ExcelErrors(Aval.TipoAval,
                    //                $"{mensaje1020}, de acuerdo al Monto Solicitado, este dato es obligatorio."));
                    //    }

                    if (StringValidation.IsNumeric(MontoSolicitado, 20001m, 30000m)) // Nivel 3
                    {
                        if (!CommonValidations.Lista(Codigos.TipoGarantiaHipotecarias, Garantia.TipoGarantia.value, out string mensaje2030))
                            response.listErrors.Add(new ExcelErrors(Garantia.TipoGarantia,
                                $"{mensaje2030}, de acuerdo al Monto Solicitado, este dato es obligatorio."));
                    
                        if (Tenencia.value == Codigos.RegimenTenencia[1])
                            if (!CommonValidations.Lista(Codigos.TipoAval, Aval.TipoAval.value, out string mensaje2))
                                response.listErrors.Add(new ExcelErrors(Aval.TipoAval,
                                    $"{mensaje2}, de acuerdo al Regimen tenencia, este dato es obligatorio."));
                    }
                    break;
            }

            
        }
        public static void Padron_ValidarFechaSiembra(Cell FechaSiembra, string Sector, string Programa, ref ExcelResponseModel response)
        {   
            if(FechaSiembra.value != "")
                if (Sector == ApplicationConstants.SectorPecuario)
                    response.listErrors.Add(new ExcelErrors(FechaSiembra, "Para el Sector Pecuario, no se debe ingresar Fecha de siembra."));
        }
        public static void Grupo_ValidarFechaSiembra(Cell FechaSiembra, string Sector, string Programa, ref ExcelResponseModel response)
        {
            if (FechaSiembra.value != "")
            {
                if (Sector == ApplicationConstants.SectorPecuario)
                    response.listErrors.Add(new ExcelErrors(FechaSiembra, "Para el Sector Pecuario, no se debe ingresar Fecha de siembra."));
            }

            if (FechaSiembra.value == "")
            {
                if (Sector == ApplicationConstants.SectorAgricola)
                    response.listErrors.Add(new ExcelErrors(FechaSiembra, "Para Sector Agrícola, este dato es obligatorio."));
            }
                
        }
        public static void ValidarNivelTecnologico(Cell NivelTecnologico, string Sector, string Programa, ref ExcelResponseModel response)
        {
            if (NivelTecnologico.value == "")
            {
                if (Sector == ApplicationConstants.SectorPecuario)
                    response.listErrors.Add(new ExcelErrors(NivelTecnologico, "El Nivel Tecnológico para el Sector Pecuario, es dato OBLIGATORIO"));
            }
        }

        #endregion
    }

    struct Codigos
    {
        public static readonly string[] TipoDocumentos = { MessageError.TipoDocumento, "DNI", "RUC" };
        public static readonly string[] Sexo = { MessageError.Sexo, "M", "F" };
        public static readonly string[] EstadoCivil = { MessageError.EstadoCivil, "1", "2", "3", "4", "5" };
        public static readonly string[] WhatsApp = { MessageError.WhatsApp, "1", "2" };
        public static readonly string[] Departamento = { MessageError.Departamento, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" };
        public static readonly string[] Provincia = { MessageError.Pronvincia, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" };
        public static readonly string[] Distrito = { MessageError.Distrito, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" };
        public static readonly string[] RegimenTenencia = { MessageError.RegimenTenencia, "CPOS", "CPRO" };
        public static readonly string[] EstadoCampo = { MessageError.EstadoCampo, "1", "2", "3", "4" };
        public static readonly string[] UnidadFinancimiento = { MessageError.UnidadFinanciamiento,
            "ALEV","ARRB","BOLS","CG","COLM","DOCE","FARD","GLB","HAS","HMAQ","JABA","KG","LIBR","LT","MC","PACA","PIEM","PIET","PLG2","PLTL","PT","QQ","SACO","TM","UND"};
        public static readonly string[] EnvioCronograma = { MessageError.EnvioCronograma, "0", "1", "2", "3" };
        public static readonly string[] QueHacerPagoAdelantado = { MessageError.QueHacerPagoAdelantado, "1", "2" };
        public static readonly string[] TipoGarantiaHipotecarias = { MessageError.TipoGarantia, "RSOL", "RDOL", "USOL", "UDOL", };
        public static readonly string[] TienePoliza = { MessageError.TienePoliza, "S", "N" };
        public static readonly string[] TipoAvalFianza = { MessageError.TipoAvalFianza, "HVAL", "GVAL", "HFSO", "GFSO" };
        public static readonly string[] TipoAval = { MessageError.TipoAval, "HVAL", "GVAL"};
        public static readonly string[] TipoFianza = { MessageError.TipoFianza, "HFSO", "GFSO" };
        public static readonly string[] NivelTecnologico = { MessageError.NivelTenologico, "1", "2", "3" };
        public static readonly string[] Sector = { MessageError.Sector, "1", "2", "3" };
        public static readonly string[] Programa = { MessageError.Programa, "0001", "0002"};

    }
    struct MessageError
    {
        public static readonly string TipoDocumento = "Tipo Documento invalido";
        public static readonly string Sexo = "Seleccione un Sexo válido";
        public static readonly string EstadoCivil = "Selecione un Estado Civil válido";
        public static readonly string WhatsApp = "Seleccione una opción válida para WhatsApp";
        public static readonly string Departamento = "Departamento inválido, Verifique Ubigeo ingresado";
        public static readonly string Pronvincia = "Provincia no pertenece al departamento seleccionado";
        public static readonly string Distrito = "Distrito no pertenece a la provincia seleccionada";
        public static readonly string RegimenTenencia = "Seleccione un Régimen de Tenencia válido";
        public static readonly string EstadoCampo = "Seleccione Estado Campo válido";
        public static readonly string UnidadFinanciamiento = "Unidad Financiamiento inválido";
        public static readonly string EnvioCronograma = "Envío Cronograma inválido";
        public static readonly string QueHacerPagoAdelantado = "Ingresar Qué Hacer Pago Adelantado";
        public static readonly string TipoGarantia = "Tipo Garantía inválido";
        public static readonly string TienePoliza = "Tiene Póliza inválido";
        public static readonly string TipoAvalFianza = "Tipo de Aval/Fianza inválido";
        public static readonly string TipoAval = "Tipo de Aval inválido";
        public static readonly string TipoFianza = "Tipo Fianza inválido";
        public static readonly string NivelTenologico = "Nivel Tecnológico inválido";
        public static readonly string Sector = "Sector inválido";
        public static readonly string Programa = "Programa inválido";

        public static readonly string NumeroDNI = "El número de DNI debe contener 8 caracteres numéricos";
        public static readonly string NumeroRUC = "El número de RUC debe contener 11 caracteres numéricos";
        public static readonly string NumeroDocumento = "Tipo de documento no válido para el número de documento";
        
    }

}

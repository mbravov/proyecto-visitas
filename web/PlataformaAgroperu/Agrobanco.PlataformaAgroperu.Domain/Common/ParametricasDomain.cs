﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;

namespace Agrobanco.PlataformaAgroperu.Domain.Common
{
    public class ParametricasDomain
    {
        public List<ParametricasModel> ListarParametricasGrupo(int codOficinaRegional)
        {
            var grupoData = new ParametricasData();
            return grupoData.ListarParamtricasGrupo(codOficinaRegional);
        }

        public List<ParametricasModel> ListarParemetricasMatrizCostos()
        {
            var Data = new ParametricasData();
            return Data.ListarParamtricasMatrizCosto();
        }

        public List<ParametricasModel> ListarComisionRegante(string CodJuntaRegante)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarComisionRegante(CodJuntaRegante);

        }

        public List<ParametricasModel> ListarUbigeos(string Ubigeo)
        {
            var grupoData = new ParametricasData();
            return grupoData.ListarUbigeos(Ubigeo);
        }

        public List<ParametricasModel> ListarDepartamentos()
        {
            var Data = new ParametricasData();
            return Data.ListarDepartamentos();
        }

        public List<ParametricasModel> ListarProvincias(string CodDepartamento)
        {
            var grupoData = new ParametricasData();
            return grupoData.ListarProvincias(CodDepartamento);
        }

        public List<ParametricasModel> ListarDistritos(string CodProvincia)
        {
            var grupoData = new ParametricasData();
            return grupoData.ListarDistritos(CodProvincia);
        }

        public List<ParametricasModel> ListarOrganizaciones(int CodAgencia)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarOrganizaciones(CodAgencia);

        }

        public List<ParametricasModel> ListarComisiones(string CodigoOrganizacion)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarComisiones(CodigoOrganizacion);

        }

        public List<ParametricasModel> ListarAgencias(int CodAgencia)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarAgencias(CodAgencia);
        }

        public List<ParametricasModel> ListarFuncionarios(int CodAgencia)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.Listarfuncionarios(CodAgencia);
        }

        public static List<ParametricasModel> ListarCultivos()
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarCultivos();
        }

        public static List<ParametricasModel> ListarProductos(string CodigoSector)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarProductos(CodigoSector);
        }

        public static List<ParametricasModel> ListarProductos(string CodigoSector, string CodigoPrograma)
        {
            var parametricasData = new ParametricasData();
            return parametricasData.ListarProductos(CodigoSector, CodigoPrograma);
        }






    }
}

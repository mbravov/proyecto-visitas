﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class AccountDomain
    {
        public Usuario BuscarUsuario(string v_usuario,string v_clave)
        {
            var userData = new AccountData();
            return userData.BuscarUsuario(v_usuario,v_clave);
        }

        public Usuario BuscarUsuario(int TokenID)
        {
            var userData = new AccountData();
            return userData.BuscarUsuario(TokenID);
        }
    }
}

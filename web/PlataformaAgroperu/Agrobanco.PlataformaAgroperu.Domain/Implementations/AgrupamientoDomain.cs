﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos;
using Agrobanco.PlataformaAgroperu.Domain.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class AgrupamientoDomain
    {
        public void SubirAgrupamientoFromDocument(ref ExcelResponseModel respon, Usuario oUsuario, string pcabecera,Stream stream,string documentExtension,string nomFile, string FechaCliente)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.AgrupamientoData();
                GrupoDatos Cabecera = new GrupoDatos(oUsuario, 0, "1", pcabecera);

                List<GrupoIntegrante> oIntegranteExcel = 
                    formatoPersistence.LeerExcelAgrupamiento(ref respon, stream, documentExtension, out string Sector, out string Programa);


                if (oIntegranteExcel.Any())
                {
                    Grupo oGrupo = new Grupo();
                    oGrupo.DatosGrupo = Cabecera;
                    oGrupo.ExcelIntegrantes = oIntegranteExcel;

                    if(Cabecera.Sector == Sector && Cabecera.Programa == Programa)
                    {
                        ValidarExcelAgrupamiento(ref respon, oGrupo);
                        if (!respon.listErrors.Any())
                            formatoPersistence.RegistrarAgrupamiento(ref respon, oGrupo, FechaCliente);

                    }
                    else
                    {
                        respon.code = "02";
                        respon.message = "El Sector y programa no coinciden con los datos ingresados en el archivo, verifique.";
                    }

                    
                }
                else
                {
                    respon.code = "02";
                    respon.message = "No se encontraron registros en el archivo, verifique.";
                }
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, "Grupo Domain");
                throw;
            }                      
        }
        
        public List<InfoGrupo> ConsultarAgrupamientosPorAgenciaAnalista(string CodAgencia, string CodFuncionario)
        {
            var formatoPersistence = new Persistence.Implementations.AgrupamientoData();
            return formatoPersistence.ListarAgrupamientosPorAgenciaAnalista(CodAgencia, CodFuncionario);
        }
        public Grupo ConsultarAgrupamientoInfo(string CodAgrupacion)
        {
            var formatoPersistence = new Persistence.Implementations.AgrupamientoData();
            return formatoPersistence.ConsultarAgrupamientoInfo(CodAgrupacion);
        }
        public List<GrupoIntegrante> ListarIntegrantesPorGrupo(string CodAgrupamiento)
        {
            var Persistence = new Persistence.Implementations.AgrupamientoData();
            return Persistence.ListarIntegrantesPorAgrupamiento(CodAgrupamiento);
        }
        public List<GrupoIntegrante> ListarSolicitudesPorAgenciaAnalista(string CodAgencia, string CodAnalista)
        {
            var Persistence = new Persistence.Implementations.AgrupamientoData();
            return Persistence.ListarSolicitudesPorAgenciaAnalista(CodAgencia, CodAnalista);
        }

        public int ActualizarIntegrantePorDocumento(string nrosolicitud,string IdIntegrante, string idgrupo, string fabono, string banco, string cuenta, string correo, string celular)
        {
            var formatoPersistence = new Persistence.Implementations.AgrupamientoData();
            return formatoPersistence.ActualizaIntegrante(nrosolicitud, IdIntegrante, idgrupo, fabono, banco,cuenta,correo,celular);
        }
        public List<GrupoIntegrante> ConsultarIntegrantePorDocumento(decimal Solicitud)
        {
            var formatoPersistence = new Persistence.Implementations.AgrupamientoData();
            return formatoPersistence.ConsultarIntegranteInfo(Solicitud);
        }

        private void ValidarExcelAgrupamiento(ref ExcelResponseModel response, Grupo grupo)
        {
            int Fila = 0;
            response.code = "03";
            response.message = "Se encontraron observaciones, verifique";
            try
            {
                #region Validad Duplicados                
                List<DocumentoEvaluar> lintegrantes = new List<DocumentoEvaluar>();
                foreach (var item in grupo.ExcelIntegrantes)
                {
                    lintegrantes.Add(new DocumentoEvaluar(item.DatosCliente.TipoDocumento.value, item.DatosCliente.NroDocumento.value));
                    if (item.DatosConyugue.TipoDocumento.value != "")
                    {
                        lintegrantes.Add(new DocumentoEvaluar(item.DatosConyugue.TipoDocumento.value, item.DatosConyugue.NroDocumento.value));
                    }

                }                
                var duplicados = lintegrantes.GroupBy(e => new { e.tipo, e.documento }).Where(e => e.Count() >= 2).Select(e => e.First());
                ExcelErrors oErr;
                foreach (var item1 in duplicados)
                {
                    oErr = new ExcelErrors(item1.documento, "Número de Documento Cliente repetido", "0", "0");
                    response.listErrors.Add(oErr);
                }
                #endregion

                var messageErrors = "";
                foreach (var registro in grupo.ExcelIntegrantes)
                {
                    var Cliente = registro.DatosCliente;
                    var Conyugue = registro.DatosConyugue;
                    var Predio = registro.DatosPredio;
                    var Solicitud = registro.DatosSolicitud;
                    var Garantia1 = registro.DatosGarantia1;
                    var Garantia2 = registro.DatosGarantia2;
                    var Aval = registro.DatosAval1;
                    var Visita = registro.DatosInformeVisita;

                    #region ClienteValidacion
                    if (!CommonValidations.TipoDocumento(Cliente.TipoDocumento.value, out messageErrors))
                        response.listErrors.Add(new ExcelErrors(Cliente.TipoDocumento, messageErrors));
                    if (!CommonValidations.NumeroDocumento(Cliente.NroDocumento.value, Cliente.TipoDocumento.value, out messageErrors))
                        response.listErrors.Add(new ExcelErrors(Cliente.NroDocumento, messageErrors));
                    CommonValidations.ValidarSoloLetras(Cliente.ApePaterno, ref response, "Apellido Paterno", MaxLength: 30, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.ApeMaterno, ref response, "Apellido Materno", MaxLength: 30, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.NomPrimer, ref response, "Primer Nombre", MaxLength: 45, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.NomSegundo, ref response, "Segundo Nombre", MaxLength: 45, empty: true);
                    CommonValidations.ValidarLongitudCedena(Cliente.Direccion, ref response, "Dirección", MaxLength: 100, MinLength:2);

                    CommonValidations.ValidarLista(Codigos.Sexo, Cliente.Sexo, ref response);
                    CommonValidations.ValidarLista(Codigos.EstadoCivil, Cliente.EstadoCivil, ref response);
                    CommonValidations.ValidarSoloNumeros(Cliente.Celular, ref response, "Celular", MaxLength: 9, EqualMax: true);
                    CommonValidations.ValidarLista(Codigos.WhatsApp, Cliente.WhatsApp, ref response);
                    CommonValidations.ValidarLongitudCedena(Cliente.Correo, ref response, "Correo", MaxLength: 30);
                    CommonValidations.ValidarSoloNumeros(Cliente.Experiencia, ref response, "Años Experiencia", 2, empty: true);
                    CommonValidations.ValidarFecha(Cliente.NacimientoDia, Cliente.NacimientoMes, Cliente.NacimientoAnio, ref response, "Fecha Nacimiento");
                    #endregion

                    #region ConyugueValidacion
                    CommonValidations.ValidarConyugue(Cliente.EstadoCivil.value, Conyugue, ref response);
                    #endregion

                    #region PredioValidacion
                    var ProvinciaDep = new Cell { value = "", row = Predio.Provincia.row, column = Predio.Provincia.column, LetterColumn = Predio.Provincia.LetterColumn };
                    var DistritpDep = new Cell { value = "", row = Predio.Distrito.row, column = Predio.Distrito.column, LetterColumn = Predio.Distrito.LetterColumn };
                    if (Predio.Provincia.value.Length > 2) ProvinciaDep.value = Predio.Provincia.value.Substring(0, 2);
                    if (Predio.Distrito.value.Length > 2) DistritpDep.value = Predio.Distrito.value.Substring(0, 2);

                    CommonValidations.ValidarLista(Codigos.Departamento, Predio.Departamento, ref response);
                    CommonValidations.ValidarLista(Codigos.Provincia, ProvinciaDep, ref response);
                    CommonValidations.ValidarLista(Codigos.Distrito, DistritpDep, ref response);
                    CommonValidations.ValidarLongitudCedena(Predio.Referencia, ref response, "Referencia/Dirección", MaxLength: 80, MinLength: 3);
                    CommonValidations.ValidarLista(Codigos.RegimenTenencia, Predio.RegimenTenencia, ref response);
                    CommonValidations.ValidarLongitudCedena(Predio.UnidadCatastral, ref response, "Unidad Catastral", MaxLength: 10);
                    #endregion

                    #region SolicitudValidacion
                    CommonValidations.ValidarLista(Codigos.UnidadFinancimiento, Solicitud.TipoUnidadFinanciamiento, ref response);
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesTotales, ref response, "Nro. Unidades Totales");
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesFinanciamiento, ref response, "Nro. Unidades a Financiar");
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesConduccion, ref response, "Nro. Unidades Conducción/Area total");
                    //CommonValidations.ValidarSoloNumeros(Solicitud.Cultivo, ref response, "Cultivo", MaxLength: 6, EqualMax: true);
                    CommonValidations.ValidarDatoNumerico(Solicitud.MontoSolicitadoSoles, ref response, "Monto Solicitado");
                    CommonValidations.ValidarLista(Codigos.EnvioCronograma, Solicitud.EnvioCronograma, ref response);
                    //-- correo en cliente
                    CommonValidations.ValidarLista(Codigos.QueHacerPagoAdelantado, Solicitud.PagoAdelantado, ref response);
                    CommonValidations.ValidarLista(Codigos.NivelTecnologico, Solicitud.NivelTecnologico, ref response, empty: true);
                    CommonValidations.ValidarNivelTecnologico(Solicitud.NivelTecnologico, grupo.DatosGrupo.Sector, grupo.DatosGrupo.Programa, ref response);
                    #endregion

                    #region GarantiasValidacion
                    CommonValidations.ValidarLista(Codigos.TipoGarantiaHipotecarias, Garantia1.TipoGarantia, ref response, empty: true);
                    CommonValidations.ValidarGarantia(Garantia1, ref response);
                    CommonValidations.ValidarGarantia(Garantia2, ref response);
                    #endregion

                    #region AvalValidacion
                    CommonValidations.ValidarLista(Codigos.TipoAvalFianza, Aval.TipoAval, ref response, empty: true);
                    CommonValidations.ValidarAval(Aval, ref response);
                    #endregion

                    #region VisitaValidacion
                    CommonValidations.ValidarSoloNumeros(Visita.Plazo, ref response, "Plazo(Meses)", MaxLength: 3);
                    CommonValidations.ValidarFecha(Visita.FechaSiembraOriginal, ref response, "Fecha Siembra", empty:true); // Fecha YYYYMMDD
                    CommonValidations.Grupo_ValidarFechaSiembra(Visita.FechaSiembra, grupo.DatosGrupo.Sector, grupo.DatosGrupo.Programa, ref response);
                    #endregion

                    #region ReglasAdicionales
                    CommonValidations.ReglasPorMontoSolicitado_FondoAgroperu(
                        Aval, Garantia1, Predio.RegimenTenencia, grupo.DatosGrupo.Sector, grupo.DatosGrupo.Programa,
                        Solicitud.MontoSolicitadoSoles.value, ref response);

                    #endregion

                    Fila++;
                }

            }
            catch (System.Exception ex)
            {
                response.code = "10";
                response.message = ex.Message + " error en fila" + Fila.ToString();
                Logger.LogException(ex, response.message);
                throw;
            }
        }

        public void ActualizarFormaPagoIntegrante(string CodAgrupamiento, string NumeroDocumento, string FormaAbono, string Banco, string NumeroCuenta)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            grupoPersistence.ActualizaFormaPagoIntegrante(CodAgrupamiento, NumeroDocumento, FormaAbono, Banco, NumeroCuenta);
        }

        public void ActualizarPagoAsistenteTecnico(decimal CodigoGrupo, int PagoAsistenteTecnico, string Usuario)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            grupoPersistence.ActualizarPagoAsistenteTecnico(CodigoGrupo, PagoAsistenteTecnico, Usuario);
        }

        public string RegistrarTipoCronograma(string NumeroSolicitud, string TipoCuota,
            int NumeroCuotas, int FrecuenciaPago, string UnidadFrecuencia, DateTime FechaGracia, int PlazoMaximo,
            string PorcentajesCuotas, string FechasCuotas, string Usuario, out string Mensaje)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            var Anio = 0;
            var Mes = 0;
            var Dia = 0;

            string IdResultado = "0000";
            var mensajeAS = "";

            switch (TipoCuota)
            {
                case "1":                    
                    Anio = FechaGracia.Year - 2000;
                    Mes = FechaGracia.Month;
                    Dia = FechaGracia.Day;
                    IdResultado = grupoPersistence.ValidarPlazoCronograma(NumeroSolicitud,
                        (FechaGracia.Year * 10000 + Mes * 100 + Dia), TipoCuota, NumeroCuotas, FrecuenciaPago, UnidadFrecuencia,
                        out mensajeAS);
                    break;

                case "3":
                    FrecuenciaPago = 0;
                    UnidadFrecuencia = "";
                    break;

                case "9":
                    break;
            }

            Mensaje = mensajeAS;
            if (IdResultado == "0000")
            {
                grupoPersistence.RegistrarTipoCronograma(NumeroSolicitud, TipoCuota,
                    NumeroCuotas, FrecuenciaPago, UnidadFrecuencia, Anio, Mes, Dia,
                    PorcentajesCuotas, FechasCuotas, Usuario);
               return "100";
            }
            else
            {
                return IdResultado;
            }

                
        }

        public ParametricasModel ValidarPeriodoGracia(DateTime FechaGracia)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            return grupoPersistence.ValidarPeriodoGracia(FechaGracia);
        }

        public int SubirDocumento(Stream stream, string fileExtension, string tipo, string NumeroSolicitud, string NumeroDocumento, string Observacion, string usuario)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            var IdLaserFiche = grupoPersistence.CargarDocumentoLaserFiche(stream, fileExtension, tipo, NumeroSolicitud, NumeroDocumento, out var fileName);

            grupoPersistence.RegistrarDocumentoLaserFiche(IdLaserFiche, NumeroSolicitud, tipo, fileName, usuario, Observacion);
            return IdLaserFiche;
        }

        public (MemoryStream stream, string fileName) BajarDocumentoLaserFiche(int IdDocument, out string extension)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            var Documento = grupoPersistence.DescagarDocumentoLaserFiche(IdDocument, out extension);
            return Documento;
        }
        public void RegistrarAprobacionPorExcepcion(Stream file, string extension, string NumeroSolicitud, 
            string NumeroDocumento, string Motivo, string Comentario, string usuario, string TipoFile)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            var IdLaser = grupoPersistence.CargarDocumentoLaserFiche(file, extension, TipoFile, NumeroSolicitud, NumeroDocumento, out string fileName);
            grupoPersistence.RegistrarDocumentoLaserFiche(IdLaser, NumeroSolicitud, TipoFile, fileName, usuario);
            grupoPersistence.RegistrarExcepcion(NumeroSolicitud, Motivo, Comentario, IdLaser, usuario);
        }

        public void AnularSolicitud(decimal NumeroSolicitud)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            grupoPersistence.AnularSolicitud(NumeroSolicitud);

        }

        public void AnularGrupo(int CodigoGrupo)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            grupoPersistence.AnularAgrupamiento(CodigoGrupo);
        }

        //public MatrizCostos ObtenerTipoCronograma(int CodigoCosto)
        //{
        //    var Persistence = new Persistence.Implementations.AgrupamientoData();
        //    return Persistence.ObtenerTipoCronograma(CodigoCosto);
        //}

        #region SeguimientoSolicitudes

        public List<GrupoIntegrante> ListarSolicitudesEnRevision(string CodAgencia, string CodAnalista)
        {
            var Persistence = new Persistence.Implementations.AgrupamientoData();
            return Persistence.ListarSolicitudesEnRevision(CodAgencia, CodAnalista);
        }

        public List<GrupoIntegrante> ListarSolicitudes(string CodAgencia, string CodAnalista)
        {
            var Persistence = new Persistence.Implementations.AgrupamientoData();
            return Persistence.ListarSolicitudes(CodAgencia, CodAnalista);
        }

        public void AprobarExcepcion(string NumeroSolicitud, string Observacion, string Respuesta, string Usuario)
        {
            var grupoPersistence = new Persistence.Implementations.AgrupamientoData();
            grupoPersistence.AprobarPorExcepcion(NumeroSolicitud, Observacion, Respuesta, Usuario);
        }

        #endregion



    }
}

﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;
using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class ConsultasDomain
    {
        public List<ResultadoGrupo> ListarResultados(string CodAgencia, string CodAnalista, int CodigoGrupo)
        {
            var data = new ConsultasData();
            List<ResultadoGrupo> lista = data.ListarResultado(CodAgencia, CodAnalista, CodigoGrupo);
            return lista;
        }
        public List<GrupoIntegrante> ListarSolicitudes(string CodigoGrupo)
        {
            var data = new ConsultasData();            
            List<GrupoIntegrante> lista = data.ListarSolicitudesDetallado(CodigoGrupo);
            return lista;
        }
        public List<ResultadoGrupo> ListarResultadosPadron(string CodAgencia, string CodigoGrupo)
        {
            var data = new ConsultasData();
            List<ResultadoGrupo> lista = data.ListarResultadoPadron(CodAgencia, CodigoGrupo);
            return lista;
        }

        public List<GrupoIntegrante> ListarSolicitudes(string CodAgencia, string CodAnalista)
        {
            var data = new ConsultasData();
            List<GrupoIntegrante> lista = data.ListarSolicitudes(CodAgencia, CodAnalista);
            return lista;
        }


    }
}

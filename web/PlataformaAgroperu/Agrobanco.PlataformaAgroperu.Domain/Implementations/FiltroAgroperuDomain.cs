﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Agrobanco.PlataformaAgroperu.Domain.Models;
using Agrobanco.PlataformaAgroperu.Domain.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class FiltroAgroperuDomain
    {
        public FiltroAgroperuDomain() { }

        public void CargarExcelFiltroAgroperu(ref ExcelResponseModel ResponseModel, Usuario oUsuario, Stream stream, string documentExtension, string descripcionFiltro, string FechaCliente)
        {
            try
            {
                var persistence = new Persistence.Implementations.FiltroAgroperuData();
                FiltroAgroperuModel agroperuModel = new FiltroAgroperuModel
                {
                    Descripcion = descripcionFiltro,
                    Excel = persistence.LeerExcelFiltroAgroperu(ref ResponseModel, stream, documentExtension)
                };

                ValidarExcelFiltro(ref ResponseModel, agroperuModel.Excel);
                if (!ResponseModel.listErrors.Any())
                {
                    persistence.RegistrarFiltroAgroperu(ref ResponseModel, agroperuModel, FechaCliente, oUsuario.User);
                    //formatoPersistence.RegistrarAgrupamiento(ref respon, oGrupo, FechaCliente);
                    //respon.lstCalificacionesSBS = respon.code == "99" ? formatoPersistence.ConsultarCalifSBSxGrupo(respon.idReg) : null;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, ex.Message);
                throw;
            }
        }

        private void ValidarExcelFiltro(ref ExcelResponseModel response, List<ExcelFiltroAgroperu> excelFiltro)
        {
            var messageErrors = "";
            foreach (var registro in excelFiltro)
            {
                if (!CommonValidations.TipoDocumento(registro.TipoDocumento.value, out messageErrors))
                    response.listErrors.Add(new ExcelErrors(registro.TipoDocumento, messageErrors));

                if (!CommonValidations.NumeroDocumento(registro.NumeroDocumento.value, registro.TipoDocumento.value, out messageErrors))
                    response.listErrors.Add(new ExcelErrors(registro.NumeroDocumento, messageErrors));

                if (!StringValidation.OnlyLetters(registro.ApellidoPaterno.value , 50, MinLength: 3))
                    response.listErrors.Add(new ExcelErrors(registro.ApellidoPaterno, "Apellido Paterno debe contener solo letras y tener un máximo de 50 caracteres"));

                if (!StringValidation.OnlyLetters(registro.ApellidoMaterno.value, 50, MinLength: 3))
                    response.listErrors.Add(new ExcelErrors(registro.ApellidoMaterno, "Apellido Materno debe contener solo letras y tener un máximo de 50 caracteres"));

                if (!StringValidation.OnlyLetters(registro.PrimerNombre.value, 50, MinLength: 3))
                    response.listErrors.Add(new ExcelErrors(registro.PrimerNombre, "Primer Nombre debe contener solo letras y tener un máximo de 50 caracteres"));

                if (!StringValidation.OnlyLetters(registro.SegundoNombre.value, 50, empty: true))
                    response.listErrors.Add(new ExcelErrors(registro.SegundoNombre, "Segundo Nombre debe contener solo letras y tener un máximo de 50 caracteres"));
            }
        }

        public List<FiltroAgroperuModel> ListarFiltrosAgroperu(int CodAgencia)
        {
            var dataPersistence = new Persistence.Implementations.FiltroAgroperuData();
            var lstFiltros = dataPersistence.ConsultarFiltrosAgroperu(CodAgencia);
            return lstFiltros;
        }

        public MemoryStream DescargarResultadosFiltroAgroperu(int CodigoFiltro)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.FiltroAgroperuData();
                return formatoPersistence.DescargarResultadosFiltroAgroperu(CodigoFiltro);
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, ex.Message);
                throw;
            }
        }


    }
}

﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Georeferenciacion;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class GeoreferenciacionDomain
    {
        public List<FotoModel> ObtenerFotosPorDocumento(string NroDocumento, string NroSolicitud)
        {
            List<FotoModel> lstFotos = new List<FotoModel>();
            var data = new GeoreferenciacionData();
            lstFotos = data.GetFotosPorDocumentoLF(NroDocumento, NroSolicitud);
            return lstFotos;
        }

        public List<FotoModel> ObtenerFotosPosSolicitud(string NumeroSolicitud)
        {
            List<FotoModel> lstFotos = new List<FotoModel>();
            var data = new GeoreferenciacionData();
            lstFotos = data.GetFotosPorSolicitud(NumeroSolicitud);
            return lstFotos;
        }

        public List<FotoModel> ListarFotosCercanas(string Latitud, string Longitud)
        {
            var data = new GeoreferenciacionData();
            var lstClientes = data.GetFotosCercanas(Latitud, Longitud);
            return lstClientes;
        }

        public void SubirFotoPorNumeroSolicitud(string NumeroSolicitud, string NumeroDocumento, MemoryStream imgStream, ref FotoModel foto, string Usuario)
        {
            string base64String = string.Empty;
            var data = new GeoreferenciacionData();
            var RegLaserFiche = data.CargarFotoJPG(NumeroSolicitud, NumeroDocumento, imgStream);

            foto.IdLaserFiche = RegLaserFiche.id;
            foto.ImgCod = RegLaserFiche.fileName;
            foto.NroSolicitud = NumeroSolicitud;
            data.InsertarFotoPorSolicitud(NumeroSolicitud, NumeroDocumento, foto, Usuario);

            byte[] imageBytes = imgStream.ToArray();
            base64String = Convert.ToBase64String(imageBytes);
            imageBytes = null;
            //img.stream.Close();
            foto.imgBase64 = base64String;
            //foto.ImgCod = img.fileName;            

        }

        public void VincularFotosPorNumeroSolicitud(string NumeroSolicitud, string NumeroDocumento, string tramaCodImagenes, List<FotoModel> lstCodImgVin, List<FotoModel> lstCodImgLib, string Usuario)
        {
            var data = new GeoreferenciacionData();
            try
            {
                data.VincularFotosPorSolcitud(NumeroSolicitud, NumeroDocumento, tramaCodImagenes, tramaCodImagenes, Usuario);
                data.VincularFotosLaserFiche(NumeroSolicitud, NumeroDocumento, lstCodImgVin, lstCodImgLib);

            }
            catch (Exception)
            {
                data.Rollback();
                throw;
            }
        }

        public void GrabarDatosSpacialesSQL(string NumeroSolicitud)
        {
            var data = new GeoreferenciacionData();
            //data.SetDataSpatial(NumeroSolicitud);
            
        }

    }
}

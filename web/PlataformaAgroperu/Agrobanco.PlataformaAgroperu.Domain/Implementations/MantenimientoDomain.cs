﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class MantenimientoDomain
    {
        public void CrearComision(int CodigoOrganizacion, string NombreComision, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.CrearComision(CodigoOrganizacion, NombreComision, Usuario);
        }

        public void ActualizarComision(int CodigoOrganizacion, int CodigoComision, string NombreComision, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.ActualizaComision(CodigoOrganizacion, CodigoComision, NombreComision, Usuario);
        }

        public void ElimiarComision(int CodigoOrganizacion, int CodigoComision, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.EliminaComision(CodigoOrganizacion, CodigoComision, Usuario);
        }

        public void GuardarProfesionalTecnio(Models.Grupo.DatosPersona ProfesionalTecnico, string CodigoAgencia, string CodigoOrganizacion, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.CrearProfesionalTecnico(ProfesionalTecnico, CodigoAgencia, CodigoOrganizacion, Usuario);


        }


        #region MatrizCostos

        public List<MatrizCostos> ListarCostos(string CodigoSector, string CodigPrograma, bool Historico)
        {
            var MantenimientoData = new MantenimientoData();
            var ListaCostos = new List<MatrizCostos>();
            
            if (Historico)
            {
                ListaCostos = MantenimientoData.ListarCostosHistorico();
            }
            else
            {
                ListaCostos = MantenimientoData.ListarCostos();
            }

            if (CodigoSector != "0")
                ListaCostos = ListaCostos.Where(x => x.Sector == CodigoSector).ToList();

            if (CodigPrograma != "0")
                ListaCostos = ListaCostos.Where(x => x.Programa == CodigPrograma).ToList();

            return ListaCostos;
        }

        public List<ParametricasModel> ObtenerDetalleCosto(int Codigo)
        {
            var MantenimientoData = new MantenimientoData();
            return MantenimientoData.ObtenerDetalleCosto(Codigo);
        }

        public void RegistrarCosto(MatrizCostos Costo, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.RegistrarCosto(Costo, Usuario);
        }
        public void DeshabilitarCosto(int CodigoCosto, string Usuario)
        {
            var MantenimientoData = new MantenimientoData();
            MantenimientoData.DeshablitarCosto(CodigoCosto, Usuario);
        }

        public MemoryStream DescargarMatrizCostos()
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.MantenimientoData();
                return formatoPersistence.ObtenerExcelMatrizCostos();
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, "Metodo Descargar Excel");
                throw;
            }
        }

        #endregion
    }




}

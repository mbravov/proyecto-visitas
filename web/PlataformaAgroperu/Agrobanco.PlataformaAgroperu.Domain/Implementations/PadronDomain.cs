﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Domain.Models.Padron;
using Agrobanco.PlataformaAgroperu.Domain.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class PadronDomain
    {
        public void SubirPadronFromDocument(ref ExcelResponseModel respon, Usuario oUsuario, string pcabecera, Stream stream, string documentExtension, string FechaCliente)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.PadronData();
                GrupoDatos Cabecera = new GrupoDatos(oUsuario, 0, "1", pcabecera);

                List<GrupoIntegrante> oIntegranteExcel = formatoPersistence.LeerExcelPadron(ref respon, stream, documentExtension);
                if (oIntegranteExcel.Any())
                {
                    Grupo oGrupo = new Grupo();
                    oGrupo.DatosGrupo = Cabecera;
                    oGrupo.ExcelIntegrantes = oIntegranteExcel;
                    ValidarExcelPadron(ref respon, oGrupo);

                    if (!respon.listErrors.Any())
                        formatoPersistence.RegistrarPadron(ref respon, oGrupo, FechaCliente);
                }
                else
                {
                    respon.code = "02";
                    respon.message = "No se encontraron registros en el archivo, verifique.";
                }
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, "Padron Domain");
                throw;
            }
        }
        private void ValidarExcelPadron(ref ExcelResponseModel response, Grupo oAgruCab)
        {
            var Fila = 0;
            response.code = "03";
            response.message = "Se encontraron observaciones, verifique";
            try
            {
                #region VALIDA DUPLICADOS

                List<DocumentoEvaluar> lintegrantes = new List<DocumentoEvaluar>();
                foreach (var item in oAgruCab.ExcelIntegrantes)
                {
                    lintegrantes.Add(new DocumentoEvaluar(item.DatosCliente.TipoDocumento.value,
                                                          item.DatosCliente.NroDocumento.value));

                    if (item.DatosConyugue.TipoDocumento.value != "")
                    {
                        lintegrantes.Add(new DocumentoEvaluar(item.DatosConyugue.TipoDocumento.value,
                                                              item.DatosConyugue.NroDocumento.value));
                    }
                }
                var duplicados = lintegrantes.GroupBy(e => new { e.tipo, e.documento }).Where(e => e.Count() >= 2).Select(e => e.First());
                ExcelErrors oErr;
                foreach (var item1 in duplicados)
                {
                    oErr = new ExcelErrors(item1.documento, "Número de Documento Cliente repetido", "0", "0");
                    response.listErrors.Add(oErr);
                }
                #endregion

                var messageErrors = "";
                foreach (var registro in oAgruCab.ExcelIntegrantes)
                {
                    var Cliente = registro.DatosCliente;
                    var Conyugue = registro.DatosConyugue;
                    var Predio = registro.DatosPredio;
                    var Solicitud = registro.DatosSolicitud;
                    var Visita = registro.DatosInformeVisita;

                    #region ClienteValidacion
                    if (!CommonValidations.TipoDocumento(Cliente.TipoDocumento.value, out messageErrors))
                        response.listErrors.Add(new ExcelErrors(Cliente.TipoDocumento, messageErrors));
                    if (!CommonValidations.NumeroDocumento(Cliente.NroDocumento.value, Cliente.TipoDocumento.value, out messageErrors))
                        response.listErrors.Add(new ExcelErrors(Cliente.NroDocumento, messageErrors));
                    CommonValidations.ValidarSoloLetras(Cliente.ApePaterno, ref response, "Apellido Paterno", MaxLength: 30, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.ApeMaterno, ref response, "Apellido Materno", MaxLength: 30, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.NomPrimer, ref response, "Primer Nombre", MaxLength: 45, MinLength: 2);
                    CommonValidations.ValidarSoloLetras(Cliente.NomSegundo, ref response, "Segundo Nombre", MaxLength: 45, empty: true);
                    CommonValidations.ValidarLongitudCedena(Cliente.Direccion, ref response, "Dirección", MaxLength: 100);

                    CommonValidations.ValidarLista(Codigos.Sexo, Cliente.Sexo, ref response, empty: true);
                    CommonValidations.ValidarLista(Codigos.EstadoCivil, Cliente.EstadoCivil, ref response, empty: true);
                    CommonValidations.ValidarSoloNumeros(Cliente.Celular, ref response, "Celular", MaxLength: 9, EqualMax: true);
                    CommonValidations.ValidarLista(Codigos.WhatsApp, Cliente.WhatsApp, ref response);
                    CommonValidations.ValidarLongitudCedena(Cliente.Correo, ref response, "Correo", MaxLength: 30);
                    CommonValidations.ValidarSoloNumeros(Cliente.Experiencia, ref response, "Años Experiencia", 2, empty: true);
                    CommonValidations.ValidarFecha(Cliente.NacimientoDia, Cliente.NacimientoMes, Cliente.NacimientoAnio, ref response, "Fecha Nacimiento", empty: true);
                    #endregion

                    #region ConyugueValidacion
                    CommonValidations.ValidarConyugue(Cliente.EstadoCivil.value, Conyugue, ref response);
                    #endregion

                    #region PredioValidacion
                    var ProvinciaDep = new Cell { value = "", row = Predio.Provincia.row, column = Predio.Provincia.column, LetterColumn = Predio.Provincia.LetterColumn };
                    var DistritpDep = new Cell { value = "", row = Predio.Distrito.row, column = Predio.Distrito.column, LetterColumn = Predio.Distrito.LetterColumn };
                    if (Predio.Provincia.value.Length > 2) ProvinciaDep.value = Predio.Provincia.value.Substring(0, 2);
                    if (Predio.Distrito.value.Length > 2) DistritpDep.value = Predio.Distrito.value.Substring(0, 2);

                    CommonValidations.ValidarLista(Codigos.Departamento, Predio.Departamento, ref response);
                    CommonValidations.ValidarLista(Codigos.Provincia, ProvinciaDep, ref response);
                    CommonValidations.ValidarLista(Codigos.Distrito, DistritpDep, ref response);
                    CommonValidations.ValidarLongitudCedena(Predio.Referencia, ref response, "Referencia/Dirección", MaxLength: 80, MinLength: 3);
                    CommonValidations.ValidarLista(Codigos.RegimenTenencia, Predio.RegimenTenencia, ref response, empty: true);
                    CommonValidations.ValidarLongitudCedena(Predio.UnidadCatastral, ref response, "Unidad Catastral", MaxLength: 10);
                    #endregion

                    #region SolicitudValidacion

                    CommonValidations.ValidarLista(Codigos.Sector, Solicitud.Sector, ref response);
                    CommonValidations.ValidarLista(Codigos.Programa, Solicitud.Programa, ref response);
                    CommonValidations.ValidarLista(Codigos.UnidadFinancimiento, Solicitud.TipoUnidadFinanciamiento, ref response);
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesTotales, ref response, "Nro. Unidades Totales", empty: true);
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesFinanciamiento, ref response, "Nro. Unidades a Financiar", empty: true);
                    CommonValidations.ValidarDatoNumerico(Solicitud.NroUnidadesConduccion, ref response, "Nro. Unidades Conducción/Area total");
                    CommonValidations.ValidarSoloNumeros(Solicitud.Cultivo, ref response, "Producto", MaxLength: 6, EqualMax: true);

                    CommonValidations.ValidarLista(Codigos.NivelTecnologico, Solicitud.NivelTecnologico, ref response, empty: true);
                    CommonValidations.ValidarNivelTecnologico(Solicitud.NivelTecnologico, Solicitud.Sector.value, Solicitud.Programa.value, ref response);

                    CommonValidations.ValidarDatoNumerico(Solicitud.MontoSolicitadoSoles, ref response, "Monto Solicitado", empty: true);
                    CommonValidations.ValidarLista(Codigos.EnvioCronograma, Solicitud.EnvioCronograma, ref response, empty: true);
                    //-- correo en cliente
                    CommonValidations.ValidarLista(Codigos.QueHacerPagoAdelantado, Solicitud.PagoAdelantado, ref response, empty: true);
                    
                    
                    #endregion

                    #region Garantia1Validacion
                    //CommonValidations.ValidarLista(Codigos.TipoGarantia, Garantia1.TipoGarantia, ref response, empty: true);
                    #endregion

                    #region Garantia2Validacion
                    #endregion

                    #region AvalValidacion                    
                    #endregion

                    #region VisitaValidacion
                    CommonValidations.ValidarSoloNumeros(Visita.Plazo, ref response, "Plazo(Meses)", MaxLength: 3, empty: true);
                    CommonValidations.ValidarFecha(Visita.FechaSiembraOriginal, ref response, "Fecha Siembra", empty: true); // Fecha YYYYMMDD
                    CommonValidations.Padron_ValidarFechaSiembra(Visita.FechaSiembra, Solicitud.Sector.value, Solicitud.Programa.value, ref response);
                    #endregion

                    Fila++;
                }

            }
            catch (System.Exception ex)
            {
                response.code = "10";
                response.message = ex.Message + " error en fila" + Fila.ToString();
                Logger.LogException(ex, response.message);
                throw;
            }
        }

        public List<PadronDatos> ListarPadronPorAgencia(string CodAgencia, string flagFiltro)
        {
            var padronPersistence = new Persistence.Implementations.PadronData();
            var lstPadrones = padronPersistence.ConsultarPadronPorAgencia(CodAgencia, flagFiltro);
            return lstPadrones;
        }
        public MemoryStream DescargarPadron(ExcelResponseModel respon, int CodigoPadron)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.PadronData();
                return formatoPersistence.ObtenePadronResultado(respon, CodigoPadron);
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, "Metodo Descargar PAdron");
                throw;
            }
        }
    }
}

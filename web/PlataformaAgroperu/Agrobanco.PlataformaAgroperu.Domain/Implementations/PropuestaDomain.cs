﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class PropuestaDomain
    {
        public Propuesta ObtenerDatosPropuesta(ExcelResponseModel respon, string CodSolicitud)
        {
            var formatoPersistence = new Persistence.Implementations.PropuestaData();
            return formatoPersistence.ConsultarPropuestaInfo(ref respon, CodSolicitud);
        }
        public string GenerarPropuesta(ExcelResponseModel respon, string CodSolicitud)
        {
            var formatoPersistence = new Persistence.Implementations.PropuestaData();
            return formatoPersistence.GenerarPropuesta(CodSolicitud);
        }
    }
}

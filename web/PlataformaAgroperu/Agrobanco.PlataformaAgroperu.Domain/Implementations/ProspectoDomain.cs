﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Prospecto;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class ProspectoDomain
    {
        public void SubirProspectoFromDocument(ref ExcelResponseModel respon, string NombreProspecto, Usuario usuario, Stream fileExcel, string documentExtension, string nomFile, string FechaCliente)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.ProspectoData();
                Prospecto Prospecto = formatoPersistence.LeerExcelProspecto(ref respon, NombreProspecto, fileExcel, documentExtension, nomFile);
                ValidarExcelProspecto(ref respon, Prospecto);

                if (!respon.listErrors.Any())
                {
                    // informeVisita.DocumentId = infVisitaPersistence.CargarDocumento(token, stream, documentExtension);
                    formatoPersistence.RegistrarProspecto(ref respon, Prospecto, usuario, FechaCliente);
                    //respon.lstCalificacionesSBS = respon.code == "99" ? formatoPersistence.ConsultarCalifSBSxFiltro(respon.idReg) : null;
                }
            }
            catch (System.Exception ex)
            {
                respon.code = "10";
                respon.message = ex.Message;
                throw;
            }


        }

        public List<ParametricasModel> ConsultaResumenCalificacionesSBS_Prospecto(string CodigoProspecto)
        {
            var prospectoPersistence = new Persistence.Implementations.ProspectoData();
            var lstCalifProspecto = prospectoPersistence.ConsultarCalifSBSxFiltro(CodigoProspecto);
            return lstCalifProspecto;
        }

        private void ValidarExcelProspecto(ref ExcelResponseModel respon, Prospecto excelProspecto)
        {
            List<ExcelErrors> listErrors = new List<ExcelErrors>();

            //var duplicados = excelProspecto.ExcelIntegrantes.GroupBy(e => e.NumeroDocumento.value).Where(e => e.Count() >= 2).Select(e => e.First());
            var duplicados = excelProspecto.Integrantes.GroupBy(e => new { e.NumeroDocumento, e.TipoDocumento }).Where(e => e.Count() >= 2).Select(e => e.First());

            ExcelErrors oErr;

            foreach (var item in duplicados)
            {
                oErr = new ExcelErrors(item.NumeroDocumento, "Número de Documento repetido", "0", "0");
                listErrors.Add(oErr);
            }

            //OBLIGATORIO/SOLO NUMERO/SOLO LETRA/LONGITUD/MAX/MIN
            foreach (var item in excelProspecto.ExcelIntegrantes)
            {
                listErrors.AddRange(DataValidation.Valida(item.TipoDocumento, "S|N|S|S|N|S|3|0|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.NumeroDocumento, "S|S|N|N|N|S|11|8|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.ApPaterno, "S|N|S|N|S|S|30|1|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.ApMaterno, "S|N|S|N|S|S|30|1|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.PrimerNombre, "S|N|S|N|S|S|30|1|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.SegundoNombre, "N|N|S|N|S|S|30|1|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.Celular, "S|S|N|N|N|S|9|0|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.Cony_TipoDocumento, "N|N|S|N|N|S|3|0|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.Valida(item.Cony_NumeroDocumento, "N|S|N|N|N|S|11|8|N|N|N|N|N|N|N|N"));
                listErrors.AddRange(DataValidation.ValidaEnConjuntoDocumento(item.TipoDocumento, "N|N|N|N|S|11|8|N|N|N|N|N|N|N|N|N", item.NumeroDocumento));
                listErrors.AddRange(DataValidation.ValidaEnConjuntoDocumento(item.Cony_TipoDocumento, "N|N|N|N|S|11|8|N|N|N|N|N|N|N|N|N", item.Cony_NumeroDocumento));
            }
            respon.listErrors = listErrors;
        }

        public List<ProspectoDatos> ListarProspectosPorAgenciaUsuario(string CodAgencia, string CodFuncionario)
        {
            var prospectoPersistence = new Persistence.Implementations.ProspectoData();
            var lstProspectos = prospectoPersistence.ConsultarProspectosPorAgenciaUsuario(CodAgencia, CodFuncionario);
            return lstProspectos;
        }

        public MemoryStream DescargarProspecto(ExcelResponseModel respon, int CodigoProspecto)
        {
            try
            {
                var formatoPersistence = new Persistence.Implementations.ProspectoData();
                return formatoPersistence.ObteneProspectoResultado(respon, CodigoProspecto);                
            }
            catch (System.Exception ex)
            {
                Logger.LogException(ex, "Método descargar Prospecto");
                throw;
            }
        }


    }
}

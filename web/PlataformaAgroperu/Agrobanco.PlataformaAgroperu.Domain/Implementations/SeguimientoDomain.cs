﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models.Seguimiento;
using Agrobanco.PlataformaAgroperu.Persistence.Implementations;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;

namespace Agrobanco.PlataformaAgroperu.Domain.Implementations
{
    public class SeguimientoDomain
    {
        public List<AvanceSolicitudes> ListarAvances(int CodAgencia)
        {
            var dataSeguimiento = new SeguimientoData();
            return dataSeguimiento.ListarAvancesSolicitudes(CodAgencia);
        }

        public List<AvanceSolicitudes> ListarAvancesPorAgencia(int CodAgencia)
        {
            var dataSeguimiento = new SeguimientoData();
            return dataSeguimiento.ListarAvancesSolicitudesPorAgencia(CodAgencia);
        }

        public SegIndicadores ObtenerReporteSolicitudes(int Agencia, string Analista)
        {
            var dataSeguimiento = new SeguimientoData();
            var data = dataSeguimiento.ObtenerReporteSolcitudes(Agencia, Analista);

            var Informe = new SegIndicadores();
            foreach (var item in data)
            {   
                switch (item.Sector)
                {
                    case ApplicationConstants.SectorAgricola: Informe.SetAgricola(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                    case ApplicationConstants.SectorPecuario: Informe.SetPecuario(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                    case ApplicationConstants.SectorForestal: Informe.SetForestal(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                    default:
                        break;
                }
            }

            Informe.SetTotales();
            return Informe;
        }

        public SegModelCharts ObtenerReporteSolicitudesChart(int Agencia)
        {
            var dataSeguimiento = new SeguimientoData();
            var data = dataSeguimiento.ObtenerReporteSolcitudes(Agencia);

            var dataFuncionario = new List<SegIndicadores>();
            var dataAgencia = new List<SegIndicadores>();
            
            var Grupos = data.GroupBy(x => x.key).ToList();
            foreach (var grupo in Grupos)
            {
                var datoItem = new SegIndicadores();
                datoItem.key = grupo.Key;
                foreach (var item in grupo)

                    switch (item.Sector)
                    {
                        case ApplicationConstants.SectorAgricola:
                            datoItem.SetAgricola(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorPecuario:
                            datoItem.SetPecuario(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorForestal:
                            datoItem.SetForestal(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        default:
                            break;
                    }
                datoItem.SetTotales();

                if (datoItem.key.Equals("TOTAL"))
                {
                    dataAgencia.Add(datoItem);
                }
                else
                {
                    dataFuncionario.Add(datoItem);
                    
                }
            }

            //dataFuncionario.OrderBy(x => x.Desembolsado.Total.MontoSolicitado);
            var modelChart = new SegModelCharts();
            modelChart.Labels = dataFuncionario.Select(x => x.key).ToArray();
            modelChart.SetIndicadores(dataFuncionario,dataAgencia.First());
            return modelChart;
        }

        public SegModelCharts ObtenerReporteSolicitudesChart()
        {
            var dataSeguimiento = new SeguimientoData();
            var data = dataSeguimiento.ObtenerReporteSolcitudes();

            var dataAgencias = new List<SegIndicadores>();
            var dataTotal = new List<SegIndicadores>();

            var Grupos = data.GroupBy(x => x.key).ToList();
            foreach (var grupo in Grupos)
            {
                var datoItem = new SegIndicadores();
                datoItem.key = grupo.Key;
                foreach (var item in grupo)

                    switch (item.Sector)
                    {
                        case ApplicationConstants.SectorAgricola:
                            datoItem.SetAgricola(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorPecuario:
                            datoItem.SetPecuario(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorForestal:
                            datoItem.SetForestal(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        default:
                            break;
                    }
                datoItem.SetTotales();

                if (datoItem.key.Equals("TOTAL"))
                {
                    dataTotal.Add(datoItem);
                }
                else
                {
                    dataAgencias.Add(datoItem);

                }
            }

            //dataFuncionario.OrderBy(x => x.Desembolsado.Total.MontoSolicitado);
            var modelChart = new SegModelCharts();
            modelChart.Labels = dataAgencias.Select(x => x.key).ToArray();
            modelChart.SetIndicadores(dataAgencias, dataTotal.First());
            return modelChart;
        }

        public List<SegIndicadores> ObtenerReporteSolicitudes(int Agencia)
        {
            var dataSeguimiento = new SeguimientoData();
            var data = dataSeguimiento.ObtenerReporteSolcitudes(Agencia);

            var dataFuncionario = new List<SegIndicadores>();
            var dataAgencia = new List<SegIndicadores>();

            var Grupos = data.GroupBy(x => x.key).ToList();
            foreach (var grupo in Grupos)
            {
                var datoItem = new SegIndicadores();
                datoItem.key = grupo.Key;
                foreach (var item in grupo)

                    switch (item.Sector)
                    {
                        case ApplicationConstants.SectorAgricola:
                            datoItem.SetAgricola(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorPecuario:
                            datoItem.SetPecuario(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        case ApplicationConstants.SectorForestal:
                            datoItem.SetForestal(item.Estado, new Montos(item.Numero, item.MontoSolicitado, item.Saldo, item.Extorno)); break;
                        default:
                            break;
                    }
                datoItem.SetTotales();

                if (datoItem.key.Equals("TOTAL"))
                {
                    dataAgencia.Add(datoItem);
                }
                else
                {
                    dataFuncionario.Add(datoItem);
                }
            }


            return dataFuncionario;

        }

    }
}

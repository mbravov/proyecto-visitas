﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using System.Collections.Generic;

namespace Agrobanco.PlataformaAgroperu.Domain.ServiceImplementations
{
    public class ApiGrupoDomain
    {
        public List<IntegranteSentinel> ObtenerIntegrantePadronSentinel(int CodModulo, int CodGrupo)
        {
            var grupoPersistence = new Persistence.Implementations.ApiGrupoData();
            return grupoPersistence.ObtenerIntegrantePadronSentinel(CodModulo, CodGrupo);
            //return null;
        }
        public List<IntegranteSentinel> ObtenerIntegranteGrupoSentinel(int CodModulo, int CodGrupo)
        {
            var grupoPersistence = new Persistence.Implementations.ApiGrupoData();
            return grupoPersistence.ObtenerIntegranteGrupoSentinel(CodModulo, CodGrupo);
            //return null;
        }
        //public void GuardarIntegranteSentinel(int codmodulo, string datos_sentinel, string codigo)
        //{
        //    var grupoPersistence = new Persistence.Implementations.ApiGrupoData();
        //    grupoPersistence.RegistrarInfoSentinel(codmodulo, datos_sentinel, codigo);
        //    //return null;
        //}

    }
}

﻿using System;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions
{
    public class DomainValidationException : Exception
    {
        public DomainValidationException(string mensaje) : base(mensaje) { }
    }
}
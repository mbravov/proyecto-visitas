﻿using System;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions
{
    public class InvalidDataBaseOperationException : Exception
    {
        public InvalidDataBaseOperationException(string mensaje) : base(mensaje) { }

    }
}
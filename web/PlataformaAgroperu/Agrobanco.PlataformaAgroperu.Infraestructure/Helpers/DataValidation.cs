﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Helpers
{
    public class DataValidation
    {
        //antes
        //public static bool OnlyLetters1(string cadena)
        //{
        //    bool ok = false;
        //    if (string.IsNullOrWhiteSpace(cadena) == false)
        //    {
        //        for (int i = 0; i <= cadena.Length - 1; i++)
        //        {
        //            if (char.IsDigit(cadena[i]))
        //            {
        //                break;
        //            }
        //        }
        //        ok = true;
        //    }
        //    return ok;
        //}

        public static bool OnlyDecimal(string cadena)
        {
            bool ok = true;
            long b = 0;
            if (string.IsNullOrWhiteSpace(cadena) == false)
            {
                b = cadena.LongCount(letra => letra.ToString() == ".");
                if (cadena.Contains(".") && b == 1)
                {
                    for (int i = 0; i <= cadena.Length - 1; i++)
                    {
                        if (cadena[i].ToString() != ".")
                        {

                            if (!char.IsDigit(cadena[i]))
                            {
                                ok = false;
                                break;
                            }
                        }
                    }
                }
            }
            return ok;
        }
        public static bool OnlyLetters1(string cadena)
        {
            bool ok = true;
            if (string.IsNullOrWhiteSpace(cadena) == false)
            {
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (char.IsDigit(cadena[i]))
                    {
                        ok = false;
                        break;
                    }
                }

            }
            return ok;
        }
        public static bool OnlyNumbers1(string cadena)
        {
            bool ok = true;
            if (string.IsNullOrWhiteSpace(cadena) == false)
            {
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (!char.IsDigit(cadena[i]))
                    {
                        ok = false;
                        break;
                    }
                }
            }
            //if (decimal.Parse(cadena) > 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return ok;
        }

        public static bool OnlyNumbers2(string cadena)
        {
            //bool ok = true;
            string[] numeros = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            //char[] valorNumero = cadena.ToArray();
            if (cadena == "")
            {
                return false;
            }
            foreach (char item in cadena)
            {
                if ((Array.IndexOf(numeros, item.ToString()) < 0))
                {
                    return false;
                }
            }
            return true;
        }
        public static bool OnlyDecimal1(string cadena)
        {
            bool ok = true;            
            string[] numeros = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            char[] valorNumero = cadena.ToArray();
            if (string.IsNullOrWhiteSpace(cadena) == true) return ok;
            if ((Array.IndexOf(numeros, ".") == 1))
            {
                foreach (var item in valorNumero)
                {
                    if (!char.IsDigit(item))
                    {
                        return false;
                    }
                }
            }
            else
            {
                foreach (var item in valorNumero)
                {
                    if (!char.IsDigit(item))
                    {
                        return false;
                    }
                }
            }


            //if (string.IsNullOrWhiteSpace(cadena) == false)
            //{
            //    b = cadena.LongCount(letra => letra.ToString() == ".");
            //    if (cadena.Contains(".") && b == 1)
            //    {
            //        for (int i = 0; i <= cadena.Length - 1; i++)
            //        {
            //            if (cadena[i].ToString() != ".")
            //            {

            //                if (!char.IsDigit(cadena[i]))
            //                {
            //                    ok = false;
            //                    break;
            //                }
            //            }
            //        }
            //    }

            //}
            return ok;
        }
        public static bool ValidateLength1(string cadena, int MaxLength, int MinLength = 0)
        {
            bool ok = false;
            if (MinLength == 0)
            { ok = cadena.Length == MaxLength ? true : false; }
            else
            { ok = cadena.Length >= MinLength && cadena.Length <= MaxLength ? true : false; }

            return ok;
        }

        public static bool solo1Blanco(string cadena)
        {
            if (cadena.ToString().TrimEnd().Contains("  "))
            {
                return false;
            }
            return true;
        }
        public static string ConvertirEntero(int iVal, bool esColumna = true)
        {
            int iAlpha, iRemainder;
            iVal = iVal + 1;
            string identificadorCelda = iVal.ToString();
            if (esColumna)
            {
                iAlpha = (iVal / 27);
                iRemainder = iVal - (iAlpha * 26);
                if (iAlpha > 0)
                {
                    identificadorCelda = Convert.ToChar(iAlpha + 64).ToString();
                }
                if (iRemainder > 0)
                {
                    identificadorCelda = Convert.ToChar(iRemainder + 64).ToString();
                }
                if (iVal > 26)
                {
                    return identificadorCelda.ToString().PadLeft(2, 'A');
                }
            }
            else
            {
                iVal = iVal + 1;
            }

            return identificadorCelda.ToString();
        }

        //public static List<ExcelErrors> Valida(Range oErr1, string claves)
        //{
        //    List<ExcelErrors> ListErr = new List<ExcelErrors>();
        //    string[] lclaves = claves.Split('|');
        //    //OBLIGATORIO/solo NUMERO/solo letra/solo tipo documento/solo 1 espacio entre palabras/longitud max-longitud min
        //    //Valida campo obligatorio
        //    if ((lclaves[0].ToString() == "S") && (string.IsNullOrWhiteSpace(oErr1.value.ToString())))
        //    {
        //        //oErr1.value.ToString()
        //        ListErr.Add(new ExcelErrors("NO REGISTRADO", "Celda debe ingresarse", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }
        //    //Valida solo numeros
        //    if ((lclaves[1].ToString() == "S") && (OnlyNumbers1(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
        //    {
        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo números", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }
        //    //Valida solo letras
        //    if ((lclaves[2].ToString() == "S") && (OnlyLetters1(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
        //    {
        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo letras", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }
        //    //Valida solo tipo documento
        //    if ((lclaves[3].ToString() == "S") && (OnlyDoc(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
        //    {
        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda invalida, debe ser DNI o RUC", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }

        //    if ((lclaves[4].ToString() == "S") && (solo1Blanco(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
        //    {
        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo un espacio en blanco entre palabras", oErr1.row.ToString(), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }
        //    //Valida longitud
        //    if ((lclaves[5].ToString() == "S") && (ValidateLength1(oErr1.value.ToString(), Int32.Parse(lclaves[6].ToString()), Int32.Parse(lclaves[7].ToString())) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
        //    {
        //        if (lclaves[7].ToString() == "0")
        //        {
        //            ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe tener " + lclaves[6].ToString() + " caracteres de longitud", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        }
        //        else
        //        {
        //            ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe tener entre " + lclaves[7].ToString() + " y " + lclaves[6].ToString() + " caracteres de longitud", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        }
        //        return ListErr;
        //    }
        //    if ((lclaves[8].ToString() == "S") && (OnlyDecimal(oErr1.value.ToString()) == false))
        //    {

        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe ser un numero decimal", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }
        //    if ((lclaves[9].ToString() == "S") && (ValidaDatosRelacionados("SEXO", oErr1.value.ToString()) == false))
        //    {
        //        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda invalida ,se espera Masculino o Femenino", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
        //        return ListErr;
        //    }

        //    return ListErr;
        //}
        public static List<ExcelErrors> ValidaxClaves(Cell oErr, string claves, string[] arreglo)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');

            if (oErr.value != "")
            {
                if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oErr.value) == -1))
                {
                    ListErr.Add(new ExcelErrors(oErr.value.ToString(), "Celda invalida o inactivo,debe seleccionar un valor del listado", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    return ListErr;
                }
            }
            else
            {
                if (lclaves[0].ToString() == "S")
                {
                    ListErr.Add(new ExcelErrors("NO REGISTRADO", "Celda debe ingresarse", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                }
            }
            return ListErr;
        }
        public static List<ExcelErrors> ValidaMonto(Cell oErr, string claves, string[] arreglo)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            if (oErr.value != "" && OnlyDecimal1(oErr.value.ToString()) == true)
            {
                if ((lclaves[16].ToString() == "S") && (decimal.Parse(oErr.value) == 0))
                {
                    ListErr.Add(new ExcelErrors(oErr.value.ToString(), "Valor debe ser mayor a 0", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    return ListErr;
                }
            }
            return ListErr;
        }
        public static List<ExcelErrors> ValidaxFormatoFecha(Cell oErr, string claves, string[] arreglo)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            if ((lclaves[16].ToString() == "S") && (oErr.value != ""))
            {
                long n = oErr.value.LongCount(letra => letra.ToString() == "/");
                if (n != 2)
                {
                    ListErr.Add(new ExcelErrors(oErr.value.ToString(), "Celda invalida ,se espera el formato DD/MM/AAAA", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    return ListErr;
                }
                //else {
                //    dato.value = dato.value.Substring(6, 4) + dato.value.Substring(3, 2) + dato.value.Substring(0, 2);
                //}
            }
            return ListErr;
        }
        public static List<ExcelErrors> ValidaxClavesFechaCadena(Cell oErr, string claves, string[] arreglo)
        {
            string oDia, oMes, oAnho;
            string fecha = oErr.value;
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            //dato.value.Substring(6, 4) + dato.value.Substring(3, 2) + dato.value.Substring(0, 2);
            if (fecha.Length != 10)
            {
                string msg = "";
                if (oErr.value.ToString() == "")
                {
                    msg = "NO REGISTRADO";
                }
                else
                {
                    msg = oErr.value;
                }
                ListErr.Add(new ExcelErrors(msg, "Debe ingresar una fecha completa", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                return ListErr;
            }
            else
            {
                if (fecha.IndexOf('/') != 2)
                {
                    ListErr.Add(new ExcelErrors(oErr.value, "Celda invalida ,se espera el formato DD/MM/AAAA", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    return ListErr;
                }
            }
            long n = oErr.value.LongCount(letra => letra.ToString() == "/");
            if (n != 2)
            {
                string msg = "";
                if (oErr.value.ToString() == "")
                {
                    msg = "NO REGISTRADO";
                }
                else
                {
                    msg = oErr.value;
                }
                ListErr.Add(new ExcelErrors(msg, "Celda invalida ,se espera el formato DD/MM/AAAA", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));

                return ListErr;
            }
            oDia = int.Parse(fecha.Substring(0, 2)).ToString();
            oMes = int.Parse(fecha.Substring(3, 2)).ToString();
            oAnho = int.Parse(fecha.Substring(6, 4)).ToString();

            string[] dias31 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30","31"};
            string[] dias30 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30"};
            string[] mes_31dias = { "1", "3", "5", "7", "8", "10", "12" };
            string[] mes_30dias = { "2", "4", "6", "9", "11" };
            string[] mes = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };

            string[] lclaves = claves.Split('|');
            if (oDia != "")
            {
                //if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oDia.value) == -1))
                //{
                //    ListErr.Add(new ExcelErrors(oDia.value.ToString(), "Debe seleccionar un dia del listado", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                //    return ListErr;
                //}
                if (oMes != "")
                {
                    if (Array.IndexOf(mes, oMes) == -1)
                    {
                        ListErr.Add(new ExcelErrors(oDia.ToString(), "Debe ingresar un mes valido", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                        // return ListErr;
                    }
                    else
                    {
                        if (Array.IndexOf(mes_30dias, oMes) == -1)
                        {
                            if (Array.IndexOf(dias30, oDia) == -1)
                            {
                                ListErr.Add(new ExcelErrors(oDia.ToString(), "El mes elegido tiene 30 dias,solo se recibe valores entre 1 y 30 dias", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                                //       return ListErr;
                            }

                        }
                        if (Array.IndexOf(mes_31dias, oMes) == -1)
                        {
                            if (Array.IndexOf(dias31, oDia) == -1)
                            {
                                ListErr.Add(new ExcelErrors(oDia.ToString(), "El mes elegido tiene 31 dias,solo se recibe valores entre 1 y 31 dias", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                                //     return ListErr;
                            }
                        }
                    }

                }

            }

            if (oAnho != "")
            {
                if ((lclaves[16].ToString() == "S") && (oAnho.Length != 4))
                {
                    ListErr.Add(new ExcelErrors(oAnho.ToString(), "Debe seleccionar un año del listado", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    // return ListErr;
                }
                if ((lclaves[16].ToString() == "S") && (OnlyNumbers1(oAnho) == false))
                {
                    ListErr.Add(new ExcelErrors(oAnho.ToString(), "Solo se espera un numero de 4 digitos,seleccionelo", ConvertirEntero(oErr.row, false), ConvertirEntero(oErr.column)));
                    // return ListErr;
                }
            }
            return ListErr;
        }


        public static List<ExcelErrors> ValidaxClavesFecha(Cell oDia, Cell oMes, Cell oAnho, string claves, string[] arreglo)
        {
            string[] dias31 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30","31"};
            string[] dias30 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30"};
            string[] dias29 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29"};
            string[] mes_31dias = { "1", "3", "5", "7", "8", "10", "12" };
            string[] mes_30dias = { "4", "6", "9", "11" };
            string[] mes_29dias = { "2" };
            string[] mes = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            if (oDia.value != "")
            {
                //if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oDia.value) == -1))
                //{
                //    ListErr.Add(new ExcelErrors(oDia.value.ToString(), "Debe seleccionar un dia del listado", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                //    return ListErr;
                //}
                if (oMes.value != "")
                {
                    if (Array.IndexOf(mes, oMes.value) == -1)
                    {
                        ListErr.Add(new ExcelErrors(oDia.value.ToString(), "Debe ingresar un mes valido", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                        // return ListErr;
                    }
                    else
                    {
                        if (Array.IndexOf(mes_30dias, oMes.value) != -1)
                        {
                            if (Array.IndexOf(dias30, oDia.value) == -1)
                            {
                                ListErr.Add(new ExcelErrors(oDia.value.ToString(), "El mes elegido tiene 30 dias,solo se recibe valores entre 1 y 30 dias", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                                //       return ListErr;
                            }

                        }
                        if (Array.IndexOf(mes_31dias, oMes.value) != -1)
                        {
                            if (Array.IndexOf(dias31, oDia.value) == -1)
                            {
                                ListErr.Add(new ExcelErrors(oDia.value.ToString(), "El mes elegido tiene 31 dias,solo se recibe valores entre 1 y 31 dias", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                                //     return ListErr;
                            }
                        }
                        if (Array.IndexOf(mes_29dias, oMes.value) != -1)
                        {
                            if (Array.IndexOf(dias29, oDia.value) == -1)
                            {
                                ListErr.Add(new ExcelErrors(oDia.value.ToString(), "El mes elegido tiene máximo 29 dias,solo se recibe valores entre 1 y 29 dias", ConvertirEntero(oDia.row, false), ConvertirEntero(oDia.column)));
                                //     return ListErr;
                            }
                        }
                    }

                }

            }

            if (oAnho.value != "")
            {
                if ((lclaves[16].ToString() == "S") && (oAnho.value.Length != 4))
                {
                    ListErr.Add(new ExcelErrors(oAnho.value.ToString(), "Debe seleccionar un año del listado", ConvertirEntero(oAnho.row, false), ConvertirEntero(oAnho.column)));
                    // return ListErr;
                }
                if ((lclaves[16].ToString() == "S") && (OnlyNumbers1(oAnho.value) == false))
                {
                    ListErr.Add(new ExcelErrors(oAnho.value.ToString(), "Solo se espera un numero de 4 digitos,seleccionelo", ConvertirEntero(oAnho.row, false), ConvertirEntero(oAnho.column)));
                    // return ListErr;
                }
            }
            return ListErr;
        }
        public static List<ExcelErrors> ValidaxClavesDepartamentos(Cell oErr1, Cell oErr2, Cell oErr3, string claves, string[] arreglo)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            if (oErr1.value != "")
            {
                if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oErr1.value) == -1))
                {
                    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar un departamento del listado", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    return ListErr;
                }
            }
            if (oErr2.value != "")
            {
                if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oErr2.value.Substring(0, 2)) == -1))
                {
                    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar una provincia para el departamento", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    return ListErr;
                }
            }
            if (oErr3.value != "")
            {
                if ((lclaves[16].ToString() == "S") && (Array.IndexOf(arreglo, oErr3.value.Substring(0, 2)) == -1))
                {
                    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar un distrito para el departamento", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    return ListErr;
                }
            }
            if (lclaves[16].ToString() == "S")
            {
                if (oErr1.value != "" && oErr2.value != "" && oErr3.value != "")
                {

                    if (oErr1.value != oErr2.value.Substring(0, 2))
                    {
                        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "No coincide el departamento con la provincia seleccionada ", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    }
                    if (oErr1.value != oErr3.value.Substring(0, 2))
                    {
                        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "No coincide el departamento con el distrito seleccionado ", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    }
                    if (oErr2.value.Substring(0, 2) != oErr3.value.Substring(0, 2))
                    {
                        ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "No coincide la provincia con el distrito seleccionado ", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                    }
                }
            }

            return ListErr;
        }

        public static bool ValidaDiaMes(string clave, int dia, string mes)
        {
            List<ParametricasModel> lconstantes = new List<ParametricasModel>();
            lconstantes.Add(new ParametricasModel("MES", "31", "01"));
            lconstantes.Add(new ParametricasModel("MES", "29", "02"));
            lconstantes.Add(new ParametricasModel("MES", "31", "03"));
            lconstantes.Add(new ParametricasModel("MES", "30", "04"));
            lconstantes.Add(new ParametricasModel("MES", "31", "05"));
            lconstantes.Add(new ParametricasModel("MES", "30", "06"));
            lconstantes.Add(new ParametricasModel("MES", "31", "07"));
            lconstantes.Add(new ParametricasModel("MES", "31", "08"));
            lconstantes.Add(new ParametricasModel("MES", "30", "09"));
            lconstantes.Add(new ParametricasModel("MES", "31", "10"));
            lconstantes.Add(new ParametricasModel("MES", "30", "11"));
            lconstantes.Add(new ParametricasModel("MES", "31", "12"));
            ParametricasModel Item = null;
            if (clave == "MES")
            {
                Item = lconstantes.Find(c => c.value == mes.PadLeft(2, '0'));
                if (int.Parse(mes) <= 12)
                {
                    if (dia <= int.Parse(Item.code))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            if (Item is null)
            {
                return false;
            }
            return false;
        }
        public static bool ValidaDatosRelacionados(string clave, string valor)
        {
            List<ParametricasModel> lconstantes = new List<ParametricasModel>();
            //lconstantes.Add(new ParametricasModel("TIPODOC","1","DNI"));
            //lconstantes.Add(new ParametricasModel("TIPODOC","2","RUC"));
            lconstantes.Add(new ParametricasModel("SEXO", "M", "Masculino"));
            lconstantes.Add(new ParametricasModel("SEXO", "F", "Femenino"));

            ParametricasModel Item = null;
            if (clave == "SEXO")
            {
                Item = lconstantes.Find(c => c.code == valor);
            }
            if (Item is null)
            {
                return false;
            }
            return true;
        }
        public static List<ExcelErrors> Valida(Cell oErr1, string claves)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            //OBLIGATORIO/solo NUMERO/solo letra/solo tipo documento/solo 1 espacio entre palabras/longitud max-longitud min
            //Valida campo obligatorio
            if ((lclaves[0].ToString() == "S") && (string.IsNullOrWhiteSpace(oErr1.value.ToString())))
            {
                //oErr1.value.ToString()
                ListErr.Add(new ExcelErrors("NO REGISTRADO", "Celda debe ingresarse", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            //Valida solo numeros
            if ((lclaves[1].ToString() == "S") && (OnlyNumbers1(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo números", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            //Valida solo letras
            if ((lclaves[2].ToString() == "S") && (OnlyLetters1(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo letras", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            //Valida solo tipo documento
            if ((lclaves[3].ToString() == "S") && (OnlyDoc(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda invalida, debe ser DNI o RUC", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }

            if ((lclaves[4].ToString() == "S") && (solo1Blanco(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo un espacio en blanco entre palabras", oErr1.row.ToString(), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            //Valida longitud
            if ((lclaves[5].ToString() == "S") && (ValidateLength1(oErr1.value.ToString(), Int32.Parse(lclaves[6].ToString()), Int32.Parse(lclaves[7].ToString())) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false))
            {
                if (lclaves[7].ToString() == "0")
                {
                    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe tener " + lclaves[6].ToString() + " caracteres de longitud", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                }
                else
                {
                    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe tener entre " + lclaves[7].ToString() + " y " + lclaves[6].ToString() + " caracteres de longitud", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                }
                return ListErr;
            }
            if ((lclaves[8].ToString() == "S") && (OnlyDecimal(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe ser un numero decimal", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            if ((lclaves[9].ToString() == "S") && (ValidaDatosRelacionados("SEXO", oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda invalida ,se espera Masculino o Femenino", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }

            return ListErr;
        }
        public static List<ExcelErrors> ValidaEnConjuntoGarantia(Cell oErr1, string claves, Cell oErr2, Cell oErr3)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            //indica que no es obligatorio que los dos esten completos
            if ((lclaves[13].ToString() == "S") && (oErr1.value.ToString() != "") && (oErr2.value.ToString().TrimEnd() == ""))
            {
                ListErr.Add(new ExcelErrors("NO REGISTRADO", "Debe ingresar un valor de realización", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
            }
            if ((lclaves[14].ToString() == "S") && (oErr1.value.ToString() != "") && (oErr3.value.ToString().TrimEnd() == ""))
            {
                ListErr.Add(new ExcelErrors("NO REGISTRADO", "Debe ingresar una poliza", ConvertirEntero(oErr3.row, false), ConvertirEntero(oErr3.column)));
            }
            //if ((lclaves[13].ToString() == "S") && (ValidaGarantia(int.Parse(oErr1.value.ToString()), oErr2.value.ToString()) == false))
            //{
            //    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar un valor de realización", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
            //    return ListErr;
            //}
            //if ((lclaves[14].ToString() == "S") && (ValidaGarantia(int.Parse(oErr1.value.ToString()), oErr2.value.ToString()) == false))
            //{
            //    ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar una poliza", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
            //    return ListErr;
            //}
            return ListErr;
        }
        public static List<ExcelErrors> ValidaEnConjuntoCorreo(Cell oErr1, string claves, Cell oErr2)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            //indica que no es obligatorio que los dos esten completos
            if (((lclaves[0].ToString() == "N") && (oErr1.value.ToString() == "1") && (oErr2.value.ToString().TrimEnd() == "")) == true)
            {
                //sin embargo debe soportar que los dos esten
                //if ((lclaves[0].ToString() == "N") && ((oErr1.value.ToString() == "1") || (oErr2.value.ToString().TrimEnd() == "")))
                //{
                ListErr.Add(new ExcelErrors("NO REGISTRADO", "No ha registrado el correo electronico", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                return ListErr;
                //}
            }
            return ListErr;
        }

        public static List<ExcelErrors> ValidaEnConjuntoDocumento(Cell oErr1, string claves, Cell oErr2)
        {
            List<ExcelErrors> ListErr = new List<ExcelErrors>();
            string[] lclaves = claves.Split('|');
            //indica que no es obligatorio que los dos esten completos
            if (((lclaves[0].ToString() == "N") && (oErr1.value.ToString() == "") && (oErr2.value.ToString().TrimEnd() == "")) == false)
            {
                //sin embargo debe soportar que los dos esten
                if ((lclaves[0].ToString() == "N") && ((oErr1.value.ToString() == "") || (oErr2.value.ToString().TrimEnd() == "")))
                {
                    ListErr.Add(new ExcelErrors("NO REGISTRADO", "Debe registrarse tanto el Tipo como el Numero del documento", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                    return ListErr;
                }
            }
            if ((lclaves[1].ToString() == "S") && (OnlyNumbers2(oErr1.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda debe contener solo números", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            if ((lclaves[1].ToString() == "S") && (OnlyNumbers2(oErr2.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr2.value.ToString(), "Celda debe contener solo números", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                return ListErr;
            }
            //dni se escoge el parametro menor lclaves[6].ToString() 
            if ((lclaves[4].ToString() == "S") && (oErr1.value.ToString() == "DNI") && (ValidateLength1(oErr2.value.ToString(), Int32.Parse(lclaves[6].ToString()), 0) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr2.value.ToString()) == false))
            {

                ListErr.Add(new ExcelErrors(oErr2.value.ToString(), "Numero DNI debe tener 8 caracteres", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                return ListErr;
            }
            if ((lclaves[4].ToString() == "S") && (oErr1.value.ToString() == "RUC") && (ValidateLength1(oErr2.value.ToString(), Int32.Parse(lclaves[5].ToString()), 0) == false) && (string.IsNullOrWhiteSpace(oErr1.value.ToString()) == false) && (string.IsNullOrWhiteSpace(oErr2.value.ToString()) == false))
            {
                //if ((oErr1.value.ToString() == "") || (oErr2.value.ToString().TrimEnd() == ""))
                //{
                //    ListErr.Add(new ExcelErrors(oErr2.value.ToString(), "Debe registrarse tanto el Tipo como el Numero del documento", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                //    return ListErr;
                //}
                ListErr.Add(new ExcelErrors(oErr2.value.ToString(), "Numero RUC debe tener 11 caracteres", ConvertirEntero(oErr2.row, false), ConvertirEntero(oErr2.column)));
                return ListErr;
            }
            //validar fechas
            if ((lclaves[10].ToString() == "S") && (ValidaDiaMes("MES", int.Parse(oErr1.value.ToString()), oErr2.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Celda invalida ,valor no corresponde a un dia del mes ", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            //envio correo
            if ((lclaves[11].ToString() == "S") && (ValidaEnvioCorreo(int.Parse(oErr1.value.ToString()), oErr2.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Si ha elegido la opción envio de correo , debe indicarlo ", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }
            if ((lclaves[12].ToString() == "S") && (ValidaCorreo(int.Parse(oErr1.value.ToString()), oErr2.value.ToString()) == false))
            {
                ListErr.Add(new ExcelErrors(oErr1.value.ToString(), "Debe ingresar una direccion de correo completa", ConvertirEntero(oErr1.row, false), ConvertirEntero(oErr1.column)));
                return ListErr;
            }

            return ListErr;
        }
        public static bool ValidaEnvioCorreo(int valor1, string valor2)
        {
            if (valor1 == 1 && (valor2 == "" || valor2.Contains('@') == false))
            {
                return false;
            }
            return true;
        }
        public static bool ValidaCorreo(int valor1, string valor2)
        {
            if (valor2.Contains('@') == false && valor2.Contains('.') == false)
            {
                return false;
            }
            return true;
        }


        public static bool OnlyDoc(string cadena)
        {
            bool ok = true;
            if ((cadena.ToUpper() == "DNI") || (cadena.ToUpper() == "RUC"))
            {
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        public static bool OnlyNumbers(string cadena)
        {
            bool ok = true;
            if (string.IsNullOrWhiteSpace(cadena))
            {
                ok = false;
            }
            else
            {
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (!char.IsDigit(cadena[i]))
                    {
                        ok = false;
                        break;
                    }
                }
            }
            return ok;
        }

        public static bool ValidateLength(string cadena, int MaxLength, bool EqualMax = false, int MinLength = 0)
        {
            bool ok = false;
            if (EqualMax)
            { ok = cadena.Length == MaxLength ? true : false; }
            else
            { ok = cadena.Length >= MinLength && cadena.Length <= MaxLength ? true : false; }

            return ok;
        }

        public static bool OnlyLetters(string cadena, bool empty = false)
        {
            bool ok = false;
            if (string.IsNullOrWhiteSpace(cadena))
            {
                ok = empty;
            }
            else
            {
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (char.IsDigit(cadena[i]))
                    {
                        break;
                    }
                }
                ok = true;
            }
            return ok;
        }

        public static bool IsNumeric(string cadena, decimal MinValue = decimal.MinValue, decimal MaxValue = decimal.MaxValue, bool AllowZero = true)
        {
            bool isNumber;
            isNumber = Decimal.TryParse(Convert.ToString(cadena), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out decimal isItNumeric);

            if (isNumber)
                isNumber = (isItNumeric >= MinValue && isItNumeric <= MaxValue) ? true : false;

            if (isNumber)
                isNumber = isItNumeric == decimal.Zero ? AllowZero : true;

            return isNumber;
        }

        public static bool IsDate(string cadena)
        {
            bool ok;
            DateTime date;
            ok = DateTime.TryParse(Convert.ToString(cadena), out date);
            return ok;
        }
    }
}

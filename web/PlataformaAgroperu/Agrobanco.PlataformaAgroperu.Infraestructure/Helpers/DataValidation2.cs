﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Agrobanco.PlataformaAgroperu.Infraestructure.Helpers
{
    public enum Campo
    {
        TipoDocumento,
        NroDocumento,
        NroDocumentoDNI,
        NroDocumentoRUC,
        ApePaterno,
        ApeMaterno,
        NomPrimer,
        NomSegundo,
        DirSolicitante,
        Sexo,
        EstadoCivil,
        Celular,
        Experiencia,
        NacimientoDia,
        NacimientoMes,
        NacimientoAnio,
        Departamento,
        Provincia,
        Distrito,
        Referencia,
        RegimenTenencia,
        UnidadCatastral,
        EstadoCampo,
        TipoUnidadFinanciamiento,
        NroUnidadesTotales,
        NroUnidadesFinanciamiento,
        NroUnidadesConduccion,
        MontoSolicitadoSoles,
        EnvioCronograma,
        Correo,
        PagoAdelantado,
        TipoGarantia,
        ValorRealizacion,
        TienePoliza,
        TipoAval,
        Plazo,
        FechaSiembra
    }
    public enum Tipo_Arreglo
    {
        Vacio,
        Mayor0,
        Sexo,
        EstadoCivil,
        TipoDocumento,
        Departamento,
        Tenencia,
        EstadoCampo,
        TipoUnidadFinanciamiento,
        EnvioCronograma,
        QueHacerPagoAdelantado,
        TipoGarantia,
        CuentaPoliza,
        TipoAval,
        CriterioResumen,
        Numeros,
        Decimales,
        Dias31,
        Dias30,
        Mes_31dias,
        Mes_30dias,
        Mes
    };

    public class Longitud
    {
        public bool indicar_usuario { get; set; }
        public int inferior { get; set; }
        public int superior { get; set; }
        public bool esvalidado { get; set; }
        public string valorvacio { get; set; }
        public Campo campo { get; set; }
        public Longitud(Campo valor_campo, int valor_superior, int valor_inferior = 0, bool esvisibleusuario = false,bool pesvalidado= true,string pvalorvacio = "")
        {
            campo = valor_campo;
            inferior = valor_inferior;
            superior = valor_superior;
            indicar_usuario = esvisibleusuario;
            esvalidado = pesvalidado;
            valorvacio = pvalorvacio;
        }
    }

    public class DataValidation2
    {
        public static List<ExcelErrors> Buscar(Campo tipo, Cell oErr)
        {            
            string mensaje = "";
            List<ExcelErrors> lerrores = new List<ExcelErrors>();
            string valor_buscado = oErr.value.ToString();
            string[] codigos_sexo = { "M", "F" };
            string[] codigos_estadocivil = { "1", "2", "3", "4", "5" };
            string[] codigos_tipodocumento = { "DNI", "RUC" };
            string[] codigos_departamentos = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" };
            string[] codigos_tenencia = { "CAAP", "CA1V", "CCOM", "CTPR", "CTYR", "CPOS" };
            string[] codigos_estadocampo = { "1", "2", "3", "4" };
            string[] codigos_tipounidadfinanciamiento = { "HAS", "CG" };
            string[] codigos_enviocronograma = { "0", "1", "2", "3" };
            string[] codigos_quehacerpagoadelantado = { "1", "2" };
            string[] codigos_tipogarantia = { "HPCO", "GPCO", "HHTN", "HHVT", "GHTN", "GHVT", "HHRN", "GHRN" };
            string[] codigos_cuentapoliza = { "S", "N" };
            string[] codigos_tipoaval = { "HVAL", "GVAL" };
            string[] codigos_criterioresumen = { "1", "2", "3" };
            string[] numeros = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            string[] decimales = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "." };
            string[] dias31 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30","31"};
            string[] dias30 = { "1","2","3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
                ,"20","21","22","23","24","25","26","27","28","29","30"};
            string[] mes_31dias = { "1", "3", "5", "7", "8", "10", "12" };
            string[] mes_30dias = { "2", "4", "6", "9", "11" };
            string[] mes = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
            string[] arreglo = null;
            List<Longitud> llongitud = new List<Longitud>();
            llongitud.Add(new Longitud(Campo.TipoDocumento, 3, 3));
            llongitud.Add(new Longitud(Campo.NroDocumento, 11, 8, true));
            llongitud.Add(new Longitud(Campo.NroDocumentoDNI, 8, 8, true));
            llongitud.Add(new Longitud(Campo.NroDocumentoRUC, 11, 11, true));
            llongitud.Add(new Longitud(Campo.ApeMaterno, 30, 2, true));
            llongitud.Add(new Longitud(Campo.ApePaterno, 30, 2, true));
            llongitud.Add(new Longitud(Campo.NomPrimer, 45, 2, true));
            llongitud.Add(new Longitud(Campo.NomSegundo, 45, 2, true,false));
            llongitud.Add(new Longitud(Campo.DirSolicitante, 100, 2, true));
            llongitud.Add(new Longitud(Campo.Sexo, 1, 1));
            llongitud.Add(new Longitud(Campo.EstadoCivil, 1, 1));
            llongitud.Add(new Longitud(Campo.Celular, 9, 9, true,true,"0"));
            llongitud.Add(new Longitud(Campo.Experiencia, 2, 1, true));
            llongitud.Add(new Longitud(Campo.NacimientoDia, 2, 1, true,true,"0"));
            llongitud.Add(new Longitud(Campo.NacimientoMes, 2, 1, true, true, "0"));
            llongitud.Add(new Longitud(Campo.NacimientoAnio, 4, 4, true, true, "0"));
            llongitud.Add(new Longitud(Campo.Departamento, 2, 2, true));
            llongitud.Add(new Longitud(Campo.Provincia, 4, 4, true));
            llongitud.Add(new Longitud(Campo.Distrito, 4, 4, true));
            llongitud.Add(new Longitud(Campo.Referencia, 80, 2, true));
            llongitud.Add(new Longitud(Campo.RegimenTenencia, 4, 4));
            llongitud.Add(new Longitud(Campo.UnidadCatastral, 10, 10, true));
            llongitud.Add(new Longitud(Campo.EstadoCampo, 1, 1));
            llongitud.Add(new Longitud(Campo.TipoUnidadFinanciamiento, 4, 4));
            llongitud.Add(new Longitud(Campo.NroUnidadesTotales, 10, 10, true));
            llongitud.Add(new Longitud(Campo.NroUnidadesFinanciamiento, 10, 10, true));
            llongitud.Add(new Longitud(Campo.NroUnidadesConduccion, 10, 1, false,true,"0"));
            llongitud.Add(new Longitud(Campo.MontoSolicitadoSoles, 17, 17, true));
            llongitud.Add(new Longitud(Campo.EnvioCronograma, 1, 1));
            llongitud.Add(new Longitud(Campo.Correo, 30, 30, true));
            llongitud.Add(new Longitud(Campo.PagoAdelantado, 1, 1));
            llongitud.Add(new Longitud(Campo.TipoGarantia, 4, 4));
            llongitud.Add(new Longitud(Campo.ValorRealizacion, 15, 15, true));
            llongitud.Add(new Longitud(Campo.TienePoliza, 1, 1));
            llongitud.Add(new Longitud(Campo.TipoAval, 4, 4));
            llongitud.Add(new Longitud(Campo.Plazo, 3, 3));
            llongitud.Add(new Longitud(Campo.FechaSiembra, 8, 8));
            //llongitud.Add(new Longitud(Campo.Criterio, 1, 1));
            int estado = 0;

            Longitud oLong = llongitud.Where(str => str.campo == tipo).ToList()[0];
            mensaje = "El campo " + oLong.campo.ToString();
            if (oLong.esvalidado == false && oErr.value==oLong.valorvacio) {                
                return lerrores;
            }
       
            if (ValidaLongitud(oErr.value.ToString(), oLong.superior, oLong.inferior) == false)
            {
                if (oLong.indicar_usuario == true)
                {
                    if (oLong.superior > oLong.inferior)
                    {
                        mensaje = mensaje+" debe tener entre " + oLong.superior.ToString() + " y " + oLong.inferior.ToString() + " caracteres";
                    }
                    else
                    {
                        mensaje = mensaje +  " debe tener " + oLong.superior.ToString() + " caracteres";
                    }
                }
                if (oLong.esvalidado== true)
                {
                    mensaje = mensaje +  " es obligatorio";
                }
                else
                {
                    mensaje = mensaje +  " no es valido";
                }
                RegistrarError(lerrores, oErr, mensaje);
                return lerrores;
            }
     

            switch (tipo)
            {
                //case Tipo_Arreglo.Vacio:
                //    if (string.IsNullOrWhiteSpace(oErr.value.ToString())) {
                //        mensaje = "El valor es vacio";
                //        estado = 1;
                //        RegistrarError(lerrores, oErr, mensaje);
                //    }
                //    break;
                //case Tipo_Arreglo.Mayor0:
                //    if (int.Parse(oErr.value.ToString())==0) {
                //        mensaje = "El valor no puede ser 0";
                //        estado = 1;
                //        RegistrarError(lerrores, oErr, mensaje);
                //    }
                //    break;
                case Campo.Sexo:

                    arreglo = codigos_sexo;
                    //mensaje = "El sexo";

                    break;
                case Campo.EstadoCivil:
                    arreglo = codigos_estadocivil;
                    //mensaje = "El estado civil";
                    break;
                case Campo.TipoDocumento:
                    arreglo = codigos_tipodocumento;
                    //mensaje = "El tipo de documento";
                    break;
                case Campo.Departamento:
                    arreglo = codigos_departamentos;
                    //mensaje = "El departamento";
                    break;
                case Campo.RegimenTenencia:
                    arreglo = codigos_tenencia;
                    //mensaje = "El código de tenencia";
                    break;
                case Campo.EstadoCampo:
                    arreglo = codigos_estadocampo;
                    //mensaje = "El estado de campo";
                    break;
                case Campo.TipoUnidadFinanciamiento:
                    arreglo = codigos_tipounidadfinanciamiento;
                    //mensaje = "El tipo de unidad de financiamiento";
                    break;
                case Campo.EnvioCronograma:
                    arreglo = codigos_enviocronograma;
                    //mensaje = "El valor ";
                    break;
                case Campo.PagoAdelantado:
                    arreglo = codigos_quehacerpagoadelantado;
                    //mensaje = "El valor ";
                    break;
                case Campo.TipoGarantia:
                    arreglo = codigos_tipogarantia;
                    //mensaje = "El tipo de garantia";
                    break;
                case Campo.TienePoliza:
                    arreglo = codigos_cuentapoliza;
                    //mensaje = "La cuenta poliza";
                    break;
                case Campo.TipoAval:
                    arreglo = codigos_tipoaval;
                    //mensaje = "El tipo de aval";
                    break;
                //case Campo.Criterio:
                //    arreglo = codigos_criterioresumen;
                //    mensaje = "El criterio resumen";
                //    break;
                //case Campo.Numeros:
                //    arreglo = numeros;
                //    mensaje = "El valor numérico";
                //    break;
                //case Tipo_Arreglo.Decimales:
                //    b = valor_buscado.LongCount(letra => letra.ToString() == ".");
                //    if (b == 1)
                //    {
                //        arreglo = decimales;
                //        mensaje = "El valor decimal";
                //    }
                //    else
                //    {
                //        mensaje = "No es un valor decimal válido";
                //    }
                //    break;
                //case Tipo_Arreglo.Dias31:
                //    arreglo = dias31;
                //    mensaje = "El día no corresponde al mes seleccionado";
                //    estado = 2;
                //    break;
                //case Tipo_Arreglo.Dias30:
                //    arreglo = dias30;
                //    mensaje = "El día no corresponde al mes seleccionado";
                //    estado = 2;
                //    break;
                //case Tipo_Arreglo.Mes_31dias:
                //    arreglo = mes_31dias;
                //    mensaje = "El mes es de 31 dias";
                //    estado = 2;
                //    break;
                //case Tipo_Arreglo.Mes_30dias:
                //    arreglo = mes_30dias;
                //    mensaje = "El mes es de 30 dias";
                //    estado = 2;
                //    break;
                //case Tipo_Arreglo.Mes:
                //    arreglo = mes;
                //    estado = 2;
                //    break;
                default:
                    return lerrores;
            }
            if (estado == 2)
            {
                mensaje = mensaje + " no es válido o no corresponde al listado";
            }

            if (valor_buscado == "" || valor_buscado == null || arreglo == null) { return lerrores; }
            if (Array.IndexOf(arreglo, valor_buscado.ToUpper()) < 0)
            {
                RegistrarError(lerrores, oErr, mensaje);
                return lerrores;
            }
            return lerrores;
        }
        public static bool ValidaLongitud(string cadena, int MaxLength, int MinLength = 0)
        {
            bool ok = false;
            if (MinLength == 0)
            { ok = cadena.Length == MaxLength ? true : false; }
            else
            { ok = cadena.Length >= MinLength && cadena.Length <= MaxLength ? true : false; }

            return ok;
        }
        public static bool solo1Blanco(string cadena)
        {
            if (cadena.ToString().TrimEnd().Contains("  "))
            {
                return false;
            }
            return true;
        }
        public static string PosicionCelda(int iVal, bool esColumna = true)
        {
            int iAlpha, iRemainder;
            iVal = iVal + 1;
            string identificadorCelda = iVal.ToString();
            if (esColumna)
            {
                iAlpha = (iVal / 27);
                iRemainder = iVal - (iAlpha * 26);
                if (iAlpha > 0)
                {
                    identificadorCelda = Convert.ToChar(iAlpha + 64).ToString();
                }
                if (iRemainder > 0)
                {
                    identificadorCelda = Convert.ToChar(iRemainder + 64).ToString();
                }
                if (iVal > 26)
                {
                    return identificadorCelda.ToString().PadLeft(2, 'A');
                }
            }
            else
            {
                iVal = iVal + 1;
            }

            return identificadorCelda.ToString();
        }
        public static void RegistrarError(List<ExcelErrors> errores, Cell valor_celda_error, string mensaje)
        {
            string valor_encontrado = "";
            if (valor_celda_error.value == "")
            {
                valor_encontrado = "NO REGISTRADO";
            }
            errores.Add(new ExcelErrors(valor_encontrado, mensaje, PosicionCelda(valor_celda_error.row, false), PosicionCelda(valor_celda_error.column)));
        }

    }
}

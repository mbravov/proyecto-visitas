﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Helpers
{
    public class StringValidation
    {
        public static bool OnlyNumbers(string cadena, bool empty = false)
        {
            bool ok = true;
            if (string.IsNullOrWhiteSpace(cadena))            
                ok = empty;
            
            else            
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (!char.IsDigit(cadena[i]))
                    {
                        ok = false;
                        break;
                    }
                }
            
            return ok;
        }

        public static bool OnlyNumbers(string cadena, int MaxLength, bool EqualMax = false, int MinLength = 0, bool empty = false)
        {
            bool ok = false;
            if(OnlyNumbers(cadena, empty))                
                ok = Length(cadena, MaxLength, EqualMax, MinLength);

            return ok;
        }


        public static bool Length(string cadena, int MaxLength, bool EqualMax = false, int MinLength = 0)
        {
            bool ok = false;
            cadena = cadena.Trim();
            if (EqualMax)
                ok = cadena.Length == MaxLength ? true : false;
            else
                ok = cadena.Length >= MinLength && cadena.Length <= MaxLength ? true : false;

            return ok;
        }

        public static bool OnlyLetters(string cadena, bool empty = false, bool AllowSpace = true)
        {
            bool ok = false;
            if (string.IsNullOrWhiteSpace(cadena))
            {
                ok = empty;
            }
            else
            {
                var ExitFor = false;
                for (int i = 0; i <= cadena.Length - 1; i++)
                {
                    if (!char.IsLetter(cadena[i]))                    
                        ExitFor = char.IsWhiteSpace(cadena[i]) ? !AllowSpace : true;                    
                    if(ExitFor) break;
                }
                ok = !ExitFor;
            }
            return ok;
        }

        public static bool OnlyLetters(string cadena, int MaxLength, bool EqualMax = false, int MinLength = 0, bool empty = false, bool AllowSpace = true)
        {
            bool ok = false;
            if (OnlyLetters(cadena, empty, AllowSpace))
                ok = Length(cadena, MaxLength, EqualMax, MinLength);

            return ok;
        }

        public static bool IsNumeric(string cadena)
        {
            if (cadena.Contains(",") || cadena.Contains("-"))
                return false;

            bool isNumber;
            isNumber = Decimal.TryParse(Convert.ToString(cadena), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out decimal isItNumeric);

            return isNumber;
        }

        public static bool IsNumeric(string cadena, out decimal isItNumeric)
        {            
            bool isNumber;
            isNumber = Decimal.TryParse(Convert.ToString(cadena), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out isItNumeric);

            if (cadena.Contains(",") || cadena.Contains("-"))
                return false;

            return isNumber;
        }

        public static bool IsNumeric(string cadena, decimal MinValue = decimal.MinValue, decimal MaxValue = decimal.MaxValue, 
            bool AllowZero = true, bool AllowEmpty = false, bool EqualMaxValue = false)
        {
            if (String.IsNullOrWhiteSpace(cadena))
                return AllowEmpty;

            bool isNumber;
            isNumber = IsNumeric(cadena, out decimal isItNumeric);

            if (isNumber)
                isNumber = (isItNumeric >= MinValue && isItNumeric <= MaxValue) ? true : false;

            if (isNumber)
                isNumber = isItNumeric == decimal.Zero ? AllowZero : true;

            if (isNumber)
                isNumber = isItNumeric == decimal.MaxValue ? true : !EqualMaxValue;

            return isNumber;
        }

        public static bool IsDate(string cadena, bool empty = false)
        {
            if (string.IsNullOrWhiteSpace(cadena))
                return empty;

            bool ok;
            DateTime date;
            ok = DateTime.TryParse(Convert.ToString(cadena), out date);
            return ok;
        }

    }
}

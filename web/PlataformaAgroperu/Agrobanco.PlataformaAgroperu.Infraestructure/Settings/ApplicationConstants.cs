﻿using System;
using System.Configuration;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Settings
{
    public static class ApplicationConstants
    {
        // CONSTANTES DE MODULOS
        public const string ModPreevaluacion = "01";
        public const string ModEvaluacionCualitativa = "02";
        public const string ModGeoreferencia = "03";
        public const string ModInformevisita = "04";
        public const string ModFlujoCaja = "05";
        public const string ModPropuesta = "08";
        public const string ModFlujoAutonomia = "09";
        public const string ModWeb = "10";

        public const string SubModLeerGeo = "02";
        public const string SubModLeerIv = "02";
        public const string SubModLeerFc = "02";

        public const string MantenimientoGeo = "03";
        public const string MantenimientoIv = "03";
        public const string MantenimientoFc = "03";
        public const bool ActivarEvento = true;


        // CONSTANTES DE CODIGO DE PLATAFORMA
        public const string IdPlaformaAgroperu = "020";

        // CONSTANTES NEGOCIO
        public const string SectorAgricola = "1";
        public const string SectorPecuario = "2";
        public const string SectorForestal = "3";
        public const string EstadoAdmision = "1";
        public const string EstadoPropuesta = "8";
        public const string EstadoPropuestaAprobada = "82";
        public const string EstadoDesembolsado = "10";
        public const string EstadoAnulado = "13";

        public const string ProgramaGenerico = "0001";
        public const string ProgramaMadreDios = "0002";

        public const string TecnologiaAlta = "1";
        public const string TecnologiaMedia = "2";
        public const string TecnologiaTradicional = "3";

        public const string ComodinTotal = "99";


    }



}

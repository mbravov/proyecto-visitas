﻿using System;
using System.Configuration;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Settings
{
    public static class ApplicationKeys
    {
        public static string RutaTemporales => ConfigurationManager.AppSettings["config:RutaTemporales"];
        
        // KEYS PARA CONECCIÓN BASE DE DATOS
        public static string Db2RegeditFolder => ConfigurationManager.AppSettings["config:Db2ApplicationName"];        
        public static string SqlRegeditFolder => ConfigurationManager.AppSettings["config:SqlApplicationName"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];

        // KEYS CONFIGURACIÓN LASERFICHE
        public static string LaserFicheRegeditFolder => ConfigurationManager.AppSettings["config:LFApplicationName"];
        public static string LaserFicheRepository => ConfigurationManager.AppSettings["config:LfRepositorio"];
        public static string LaserFicheFolderBase => ConfigurationManager.AppSettings["config:LfFolderDocument"];
        public static string LaserFicheTemplateName => ConfigurationManager.AppSettings["config:LfTemplate"];


        // KEYS PARA FTP Y LECTURA EXCEL
        public static string FtpServer => ConfigurationManager.AppSettings["config:FtpServerImagen"];
        public static string FtpUser => ConfigurationManager.AppSettings["config:FtpUser"];
        public static string FtpPassword => ConfigurationManager.AppSettings["config:FtpPassword"];
        public static  string CadenaConexionExcel => ConfigurationManager.AppSettings["Excel"];
        public static string MaximoFilas => ConfigurationManager.AppSettings["MaxFilasFI"];
                
        // PLANTILLAS DE FILTROS
        public static string PlantillaAgrupamiento => ConfigurationManager.AppSettings["config:Agrupamiento"];
        public static string PlantillaProspecto => ConfigurationManager.AppSettings["config:ProspectoPlantilla"];
        public static string VersionProspecto => ConfigurationManager.AppSettings["config:ProspectoVersion"];
        public static string VersionFiltroAgroperu => ConfigurationManager.AppSettings["config:FiltroAgroperuVersion"];

        public static string ExcelPadronAgroperuSheetName => ConfigurationManager.AppSettings["config:ExcelPadronAgroperuSheetName"];
        public static string ExcelPadronAgroperuVersion => ConfigurationManager.AppSettings["config:ExcelPadronAgroperuVersion"];
        public static string ExcelPadronAgroperuFecha => ConfigurationManager.AppSettings["config:ExcelPadronAgroperuFecha"];
        public static int ExcelPadronAgroperuStartRow => Convert.ToInt32(ConfigurationManager.AppSettings["config:ExcelPadronAgroperuStartRow"]);

        public static string ExcelGrupoAgroperuSheetName => ConfigurationManager.AppSettings["config:ExcelGrupoAgroperuSheetName"];
        public static string ExcelGrupoAgroperuVersion => ConfigurationManager.AppSettings["config:ExcelGrupoAgroperuVersion"];
        public static string ExcelGrupoAgroperuFecha => ConfigurationManager.AppSettings["config:ExcelGrupoAgroperuFecha"];
        public static int ExcelGrupoAgroperuStartRow => Convert.ToInt32(ConfigurationManager.AppSettings["config:ExcelGrupoAgroperuStartRow"]);



        // KEYS PLANTILLA FLUJO CAJA
        public static string FlujoCajaHojaFlujoCaja => ConfigurationManager.AppSettings["config:CargarFlujoCaja"];
        public static string FlujoCajaHojaResumen => ConfigurationManager.AppSettings["config:CargarFlujoCajaResumen"];

        // KEYS PARA SERVICE SENTINEL
        public static string Db2Esquema => ConfigurationManager.AppSettings["config:DbEschema"];
        public static string ws_usuario => ConfigurationManager.AppSettings["config:usuario_ws"];
        public static string ws_clave => ConfigurationManager.AppSettings["config:clave_ws"];
        public static string ws_key => ConfigurationManager.AppSettings["config:key_ws"];
        public static string ws_servicio => ConfigurationManager.AppSettings["config:servicio_ws"];
        public static string ws_ruta => ConfigurationManager.AppSettings["config:ruta_log"];
        public static string ws_urlapi => ConfigurationManager.AppSettings["config:url_apiMEG"];

        // KEYS PARA UBICACIÓN DE ARCHIVOS
        public static string FirmaRepositoryPath => ConfigurationManager.AppSettings["config:FirmaRepoPath"];

        // KEYS URL APLICACIONES EXTERNAS
        public static string Url_AppMEG => ConfigurationManager.AppSettings["config:Url_AppMEG"];

        public static string HomePerfiles => ConfigurationManager.AppSettings["config:HomePerfiles"];

    }

    

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Infraestructure.Settings
{
    public static class As400Constants
    {
        public const string Banco01 = "01";
        public const string Banco12 = "12";
        public const string MonitorTransacciones = "AGRUSRLIB.T20000";
        public const string AplicacionJR = "JR";
        public const string AplicacionFC = "FC";
        public const string AplicacionUT = "UT";
        public const string AplicacionFondoAgroperu = "FA";
        

        public static Transactions Transaccion;
        public static Keys Claves;

        static As400Constants()
        {
            Transaccion.GeneraCodigo = "0101";
            Transaccion.EvaluaFiltroAgroperu = "0109";
            Transaccion.EvaluaPadronAgroperu = "0111";
            Transaccion.EvaluaGrupoAgroperu = "0112";
            Transaccion.GenerarPropuestaAgroperu = "0114";
            Transaccion.ObtenerVencimiento_Plazo = "0115";

            Claves.CodigoPadron = "JRPROS";
            Claves.CodigoGrupo = "TCPTCF";
        }

    }

    public struct Transactions
    {
        public string GeneraCodigo;
        public string EvaluaFiltroAgroperu;
        public string EvaluaPadronAgroperu;
        public string EvaluaGrupoAgroperu;
        public string GenerarPropuestaAgroperu;
        public string ObtenerVencimiento_Plazo;



    }

    public struct Keys
    {
        public string CodigoPadron;
        public string CodigoGrupo;
    }

    public struct Modulo
    {
        //public string FiltroProspecto;
        //public string FiltroPadron;
        //public string FiltroAgrupamiento;
        //public string FiltroAgroperu;
    }

}

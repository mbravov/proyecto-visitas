﻿namespace Agrobanco.PlataformaAgroperu.Infraestructure.Settings
{
    public class StoreConstants
    {
        static string _EsquemaDB2;
        public static ProceduresPadron Padron;
        public static ProceduresProspecto Prospecto;
        public static ProceduresAgrupamiento Agrupamiento;
        public static ProceduresGeoreferenciacion Georeferenciacion;
        public static ProceduresParametricas Parametricas;
        public static ProceduresAccount User;
        public static ProceduresSeguimiento Seguimiento;
        public static ProceduresServicio Servicio;
        public static ProceduresConsultas Consultas;
        public static ProceduresMantenimiento Mantenimiento;
        public static ProceduresFiltroAgroperu FiltroAgroperu;
        public static ProceduresPTC PTC;

        static StoreConstants()
        {
            _EsquemaDB2 = ApplicationKeys.Db2Esquema;

            Servicio.UP_MEG_SEL_DATA_PADRON_SENTINEL = $"{_EsquemaDB2}.UP_MEG_SEL_DATA_PADRON_SENTINEL";
            Servicio.UP_MEG_SEL_DATA_GRUPO_SENTINEL = $"{_EsquemaDB2}.UP_MEG_SEL_DATA_GRUPO_SENTINEL";
            Servicio.UP_MEG_INS_DATA_SENTINEL = $"{_EsquemaDB2}.UP_MEG_INS_DATA_SENTINEL";

            Padron.UP_MEG_SEL_PADRONX_AGENCIA_USUARIO = $"{_EsquemaDB2}.UP_MEG_SEL_PADRONX_AGENCIA_USUARIO";
            Padron.UP_MEG_SEL_CONSULTA_ERRORES_PADRON = $"{_EsquemaDB2}.UP_MEG_SEL_CONSULTA_ERRORES_PADRON";
            Padron.UP_MEG_INS_EXCEL_PADRON = $"{_EsquemaDB2}.UP_MEG_INS_EXCEL_PADRON";
            Padron.UP_MEG_INS_EXCEL_PADRON2 = $"{_EsquemaDB2}.UP_MEG_INS_EXCEL_PADRON2";
            Padron.UP_MEG_SEL_RESULTADO_PADRON = $"{_EsquemaDB2}.UP_MEG_SEL_RESULTADO_PADRON";
            Padron.UP_MEG_SEL_RESULTADO_PADRON_PROSPECTO = $"{_EsquemaDB2}.UP_MEG_SEL_RESULTADO_PADRON_PROSPECTO";

            Padron.UP_PFA_INS_EXCEL_PADRON = $"{_EsquemaDB2}.UP_PFA_INS_EXCEL_PADRON";
            Padron.UP_PFA_SEL_PADRONX_AGENCIA_USUARIO = $"{_EsquemaDB2}.UP_PFA_SEL_PADRONX_AGENCIA_USUARIO";

            Padron.UP_PFA_SEL_RESULTADO_PADRON = $"{_EsquemaDB2}.UP_PFA_SEL_RESULTADO_PADRON";
            Padron.UP_PFA_SEL_RESULTADO_PADRON_MOTIVOS = $"{_EsquemaDB2}.UP_PFA_SEL_RESULTADO_PADRON_MOTIVOS";
            
            Prospecto.UP_MEG_INS_EXCEL_PROSPECTO = $"{_EsquemaDB2}.UP_MEG_INS_EXCEL_PROSPECTO";
            Prospecto.UP_MEG_SEL_PROSPECTOSX_AGENCIA_USUARIO = $"{_EsquemaDB2}.UP_MEG_SEL_PROSPECTOSX_AGENCIA_USUARIO";
            Prospecto.UP_MEG_SEL_RESULTADO_PROSPECTO = $"{_EsquemaDB2}.UP_MEG_SEL_RESULTADO_PROSPECTO";//falta
            Prospecto.UP_MEG_SEL_RESUM_CALIF_SBS_PROSPECTO = $"{_EsquemaDB2}.UP_MEG_SEL_RESUM_CALIF_SBS_PROSPECTO";//falta


            Agrupamiento.UP_MEG_INS_EXCEL_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_MEG_INS_EXCEL_AGRUPAMIENTO";
            //Agrupamiento.UP_MEG_SEL_COMSIONX_JUNTA_REGANTE = $"{_EsquemaDB2}.UP_MEG_SEL_COMSIONX_JUNTA_REGANTE";
            Agrupamiento.UP_MEG_SEL_DATOS_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_MEG_SEL_DATOS_AGRUPAMIENTO";//falta
            
            Agrupamiento.UP_MEG_SEL_LISTAR_AGRUPAMIENTOS = $"{_EsquemaDB2}.UP_MEG_SEL_LISTAR_AGRUPAMIENTOS";//falta
            Agrupamiento.UP_MEG_SEL_LISTAR_INTEGRANTES = $"{_EsquemaDB2}.UP_MEG_SEL_LISTAR_INTEGRANTES";//falta
            //Agrupamiento.UP_MEG_SEL_PARAMETRICAS_GRUPO = $"{_EsquemaDB2}.UP_MEG_SEL_PARAMETRICAS_GRUPO";
            Agrupamiento.UP_MEG_SEL_RESUM_CALIF_SBS_AGRUPAMIENTO = $"{ _EsquemaDB2}.UP_MEG_SEL_RESUM_CALIF_SBS_AGRUPAMIENTO";//falta
            Agrupamiento.UP_MEG_SEL_UBIGEOS_XDEPARTAMENTO = $"{_EsquemaDB2}.UP_MEG_SEL_UBIGEOS_XDEPARTAMENTO";//falta
            Agrupamiento.UP_MEG_ACT_FORMA_PAGO_INTEGRANTE = $"{_EsquemaDB2}.UP_MEG_ACT_FORMA_PAGO_INTEGRANTE";
            //Agrupamiento.UP_PFA_SEL_PROPUESTA_INTEGRANTE = $"{_EsquemaDB2}.UP_MEG_SEL_PROPUESTA_INTEGRANTE";
            Agrupamiento.UP_MEG_SEL_GARANTIASX_INTEGRANTE = $"{_EsquemaDB2}.UP_MEG_SEL_GARANTIASX_INTEGRANTE";
            Agrupamiento.UP_MEG_SEL_AVALX_INTEGRANTE = $"{_EsquemaDB2}.UP_MEG_SEL_AVALX_INTEGRANTE";
            //Agrupamiento.UP_MEG_SEL_CONSULTA_ERRORES_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_MEG_SEL_CONSULTA_ERRORES_AGRUPAMIENTO";
            Agrupamiento.UP_MEG_ACT_DATOS_INTEGRANTE = $"{_EsquemaDB2}.UP_MEG_ACT_DATOS_INTEGRANTE";
            Agrupamiento.UP_MEG_INS_REGFILES_LASERFICHE = $"{_EsquemaDB2}.UP_MEG_INS_REGFILES_LASERFICHE";
            //Agrupamiento.UP_MEG_SEL_SOLICITUDES_DETALLADO = $"{_EsquemaDB2}.UP_MEG_SEL_SOLICITUDES_DETALLADO";
            
            Agrupamiento.UP_MEG_ACT_ANULAR_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_MEG_ACT_ANULAR_AGRUPAMIENTO";

            Agrupamiento.UP_PFA_INS_EXCEL_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_PFA_INS_EXCEL_AGRUPAMIENTO";
            Agrupamiento.UP_PFA_SEL_LISTAR_AGRUPAMIENTOS = $"{_EsquemaDB2}.UP_PFA_SEL_LISTAR_AGRUPAMIENTOS";
            Agrupamiento.UP_PFA_SEL_DATOS_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_PFA_SEL_DATOS_AGRUPAMIENTO";
            Agrupamiento.UP_PFA_SEL_LISTAR_INTEGRANTES = $"{_EsquemaDB2}.UP_PFA_SEL_LISTAR_INTEGRANTES";
            Agrupamiento.UP_PFA_SEL_LISTAR_SOLICITUDES = $"{_EsquemaDB2}.UP_PFA_SEL_LISTAR_SOLICITUDES";
            Agrupamiento.UP_PFA_SEL_SOLICITUDES_XFUNCIONARIOS = $"{_EsquemaDB2}.UP_PFA_SEL_SOLICITUDES_XFUNCIONARIOS";
            Agrupamiento.UP_PFA_SEL_CONSULTA_ERRORES_AGRUPAMIENTO = $"{_EsquemaDB2}.UP_PFA_SEL_CONSULTA_ERRORES_AGRUPAMIENTO";
            Agrupamiento.UP_PFA_SEL_CONSULTA_SOLICITUDES_XGRUPO = $"{_EsquemaDB2}.UP_PFA_SEL_CONSULTA_SOLICITUDES_XGRUPO";
            Agrupamiento.UP_PFA_SEL_PROPUESTA_INTEGRANTE = $"{_EsquemaDB2}.UP_PFA_SEL_PROPUESTA_INTEGRANTE";
            Agrupamiento.UP_PFA_SEL_DATOS_INTEGRANTE = $"{_EsquemaDB2}.UP_PFA_SEL_DATOS_INTEGRANTE";
            Agrupamiento.UP_PFA_INS_REGISTRA_TIPO_CRONOGRAMA = $"{_EsquemaDB2}.UP_PFA_INS_REGISTRA_TIPO_CRONOGRAMA";
            Agrupamiento.UP_PFA_INS_REG_APROB_EXCEPCION = $"{_EsquemaDB2}.UP_PFA_INS_REG_APROB_EXCEPCION";
            Agrupamiento.UP_PFA_ACT_APROBAR_EXCEPCION = $"{_EsquemaDB2}.UP_PFA_ACT_APROBAR_EXCEPCION";
            Agrupamiento.UP_PFA_ACT_ACTUALIZA_PAGO_AT = $"{_EsquemaDB2}.UP_PFA_ACT_ACTUALIZA_PAGO_AT";

            Agrupamiento.UP_PFA_ACT_ANULAR_SOLICITUD = $"{_EsquemaDB2}.UP_PFA_ACT_ANULAR_SOLICITUD";

            Agrupamiento.UP_PFA_SEL_SOLICITUDES_REVISION = $"{_EsquemaDB2}.UP_PFA_SEL_SOLICITUDES_REVISION";
            Agrupamiento.UP_PFA_SEL_VALIDA_PERIODO_GRACIA = $"{_EsquemaDB2}.UP_PFA_SEL_VALIDA_PERIODO_GRACIA";

            Georeferenciacion.UP_PFA_ACT_VINCULAR_FOTO_XSOLICITUD = $"{_EsquemaDB2}.UP_PFA_ACT_VINCULAR_FOTO_XSOLICITUD";
            Georeferenciacion.UP_PFA_INS_FOTO_POR_SOLICITUD = $"{_EsquemaDB2}.UP_PFA_INS_FOTO_POR_SOLICITUD";
            Georeferenciacion.UP_MEG_SEL_FOTOS_CERCANAS = $"{_EsquemaDB2}.UP_MEG_SEL_FOTOS_CERCANAS";
            Georeferenciacion.UP_MEG_SEL_IMAGENESX_DOCUMENTO_SOLICITUD = $"{_EsquemaDB2}.UP_MEG_SEL_IMAGENESX_DOCUMENTO_SOLICITUD";
            Georeferenciacion.UP_MEG_SEL_IMAGENESXSOLICITUD = $"{_EsquemaDB2}.UP_MEG_SEL_IMAGENESXSOLICITUD";
            Georeferenciacion.UP_MEG_SEL_PERIMETRO = $"{_EsquemaDB2}.UP_MEG_SEL_PERIMETRO"; // NUEVO
            Georeferenciacion.UP_MEG_SEL_INF_VISITA_XSOLICITUD = $"{_EsquemaDB2}.UP_MEG_SEL_INF_VISITA_XSOLICITUD"; // NUEVO
            Georeferenciacion.UP_GEO_INS_CREAR_POLIGONO = "UP_GEO_INS_CREAR_POLIGONO"; // NUEVO
            Georeferenciacion.UP_GEO_INS_DATOS_FOTOS = "UP_GEO_INS_DATOS_FOTOS"; // NUEVO
            Georeferenciacion.UP_GEO_INS_DATOS_SPATIAL = "UP_GEO_INS_DATOS_SPATIAL"; // NUEVO


            Parametricas.UP_UTIL_SEL_DEPARTAMENTOS = $"{_EsquemaDB2}.UP_UTIL_SEL_DEPARTAMENTOS";
            Parametricas.UP_UTIL_SEL_PROVINCIAS = $"{_EsquemaDB2}.UP_UTIL_SEL_PROVINCIAS";
            Parametricas.UP_UTIL_SEL_DISTRITOS = $"{_EsquemaDB2}.UP_UTIL_SEL_DISTRITOS";
            //Parametricas.UP_MEG_PRM_SEL_AGENCIAS = $"{_EsquemaDB2}.UP_MEG_PRM_SEL_AGENCIAS"; // NUEVO
            //Parametricas.UP_MEG_SEL_FUNCIONARIOS = $"{_EsquemaDB2}.UP_MEG_SEL_FUNCIONARIOS"; // NUEVO
            //Parametricas.UP_PFA_PRM_SEL_ORGANIZACIONES = $"{_EsquemaDB2}.UP_MEG_PRM_SEL_ORGANIZACIONES";
            //Parametricas.UP_PFA_PRM_SEL_COMISIONES_POR_ORGANIZACION = $"{_EsquemaDB2}.UP_MEG_PRM_SEL_COMISIONES_POR_ORGANIZACION";

            Parametricas.UP_PFA_SEL_COMSIONX_JUNTA_REGANTE = $"{_EsquemaDB2}.UP_PFA_SEL_COMSIONX_JUNTA_REGANTE";
            Parametricas.UP_PFA_SEL_PARAMETRICAS_GRUPO = $"{_EsquemaDB2}.UP_PFA_SEL_PARAMETRICAS_GRUPO";
            Parametricas.UP_PFA_SEL_PARAMETRICAS_MATRIZ_COSTOS = $"{_EsquemaDB2}.UP_PFA_SEL_PARAMETRICAS_MATRIZ_COSTOS";
            Parametricas.UP_UTIL_SEL_CULTIVOS = $"{_EsquemaDB2}.UP_UTIL_SEL_CULTIVOS";
            Parametricas.UP_UTIL_SEL_AGENCIAS = $"{_EsquemaDB2}.UP_UTIL_SEL_AGENCIAS";
            Parametricas.UP_UTIL_SEL_FUNCIONARIOS = $"{_EsquemaDB2}.UP_UTIL_SEL_FUNCIONARIOS";
            Parametricas.UP_UTIL_SEL_CULTIVOS_POR_SECTOR = $"{_EsquemaDB2}.UP_UTIL_SEL_CULTIVOS_POR_SECTOR";
            Parametricas.UP_PFA_SEL_PRODUCTO_POR_SECTOR_PROGRAMA = $"{_EsquemaDB2}.UP_PFA_SEL_PRODUCTO_POR_SECTOR_PROGRAMA";


            Parametricas.UP_PFA_PRM_SEL_ORGANIZACIONES = $"{_EsquemaDB2}.UP_PFA_PRM_SEL_ORGANIZACIONES";
            Parametricas.UP_PFA_PRM_SEL_COMISIONES_POR_ORGANIZACION = $"{_EsquemaDB2}.UP_PFA_PRM_SEL_COMISIONES_POR_ORGANIZACION";

            User.UP_MEG_SEL_USUARIO = $"{_EsquemaDB2}.UP_MEG_SEL_USUARIO";
            User.UP_MEG_SEL_MENUS_XPERFIL = $"{_EsquemaDB2}.UP_MEG_SEL_MENUS_XPERFIL";
            User.UP_AGR_SEL_USUARIO = $"{_EsquemaDB2}.UP_AGR_SEL_USUARIO";
            User.UP_AGR_SEL_MENUS_XPERFIL = $"{_EsquemaDB2}.UP_AGR_SEL_MENUS_XPERFIL";

            Seguimiento.UP_PFA_SEL_AVANCE_SOLICITUDES = $"{_EsquemaDB2}.UP_PFA_SEL_AVANCE_SOLICITUDES";
            Seguimiento.UP_PFA_SEL_AVANCE_SOLICITUDES_XAGENCIA = $"{_EsquemaDB2}.UP_PFA_SEL_AVANCE_SOLICITUDES_XAGENCIA";
            Seguimiento.UP_PFA_SEL_SEG_XFUNCIONARIO = $"{_EsquemaDB2}.UP_PFA_SEL_SEG_XFUNCIONARIO";
            Seguimiento.UP_PFA_SEL_SEG_XAGENCIAS = $"{_EsquemaDB2}.UP_PFA_SEL_SEG_XAGENCIAS";
            Seguimiento.UP_PFA_SEL_SEGUIMIENTO_AGROPERU = $"{_EsquemaDB2}.UP_PFA_SEL_SEGUIMIENTO_AGROPERU";


            Consultas.UP_MEG_SEL_SOLICITUDESX_AGENCIA_USUARIO = $"{_EsquemaDB2}.UP_MEG_SEL_SOLICITUDESX_AGENCIA_USUARIO";

            Mantenimiento.UP_MEG_INS_CREAR_COMISION = $"{_EsquemaDB2}.UP_MEG_INS_CREAR_COMISION";
            Mantenimiento.UP_MEG_UPD_ACTUALIZA_COMISION = $"{_EsquemaDB2}.UP_MEG_UPD_ACTUALIZA_COMISION";
            Mantenimiento.UP_MEG_UPD_ELIMINA_COMISION = $"{_EsquemaDB2}.UP_MEG_UPD_ELIMINA_COMISION";
            Mantenimiento.UP_MEG_INS_CREAR_PROFTECNICO = $"{_EsquemaDB2}.UP_MEG_INS_CREAR_PROFTECNICO";

            Mantenimiento.UP_PFA_SEL_COSTOS = $"{_EsquemaDB2}.UP_PFA_SEL_COSTOS";
            Mantenimiento.UP_PFA_SEL_COSTOS_HISTORICO = $"{_EsquemaDB2}.UP_PFA_SEL_COSTOS_HISTORICO";
            Mantenimiento.UP_PFA_SEL_COSTOS_DESCARGAR = $"{_EsquemaDB2}.UP_PFA_SEL_COSTOS_DESCARGAR";            
            Mantenimiento.UP_PFA_SEL_COSTO_POR_CODIGO = $"{_EsquemaDB2}.UP_PFA_SEL_COSTO_POR_CODIGO";
            Mantenimiento.UP_PFA_SEL_COSTOS_DETALLE = $"{_EsquemaDB2}.UP_PFA_SEL_COSTOS_DETALLE";
            Mantenimiento.UP_PFA_SEL_COSTOS_DETALLE_TOTAL = $"{_EsquemaDB2}.UP_PFA_SEL_COSTOS_DETALLE_TOTAL";
            Mantenimiento.UP_PFA_INS_REGISTRAR_COSTO = $"{_EsquemaDB2}.UP_PFA_INS_REGISTRAR_COSTO";
            Mantenimiento.UP_PFA_ACT_DESHABILITAR_COSTO = $"{_EsquemaDB2}.UP_PFA_ACT_DESHABILITAR_COSTO";

            FiltroAgroperu.UP_MEG_INS_EXCEL_FILTROAGROPERU = $"{_EsquemaDB2}.UP_MEG_INS_EXCEL_FILTROAGROPERU";
            FiltroAgroperu.UP_MEG_SEL_LISTAR_FILTROSAGROPERU = $"{_EsquemaDB2}.UP_MEG_SEL_LISTAR_FILTROSAGROPERU";
            FiltroAgroperu.UP_MEG_SEL_RESULTADOS_FILTROAGROPERU = $"{_EsquemaDB2}.UP_MEG_SEL_RESULTADOS_FILTROAGROPERU";

            PTC.UP_PTC_SEL_TOKEN_DATA = $"{_EsquemaDB2}.UP_PTC_SEL_TOKEN_DATA";

        }
    }
    public struct ProceduresServicio
    {
        public string UP_MEG_SEL_DATA_PADRON_SENTINEL;
        public string UP_MEG_SEL_DATA_GRUPO_SENTINEL;
        public string UP_MEG_INS_DATA_SENTINEL;
    }

    public struct ProceduresPadron
    {
        public string UP_MEG_SEL_PADRONX_AGENCIA_USUARIO;
        public string UP_MEG_SEL_CONSULTA_ERRORES_PADRON;
        public string UP_MEG_INS_EXCEL_PADRON;
        public string UP_MEG_INS_EXCEL_PADRON2;
        public string UP_MEG_SEL_RESULTADO_PADRON;
        public string UP_MEG_SEL_RESULTADO_PADRON_PROSPECTO;

        public string UP_PFA_INS_EXCEL_PADRON;
        public string UP_PFA_SEL_PADRONX_AGENCIA_USUARIO;

        public string UP_PFA_SEL_RESULTADO_PADRON;
        public string UP_PFA_SEL_RESULTADO_PADRON_MOTIVOS;

    }
    public struct ProceduresProspecto
    {
        public string UP_MEG_INS_EXCEL_PROSPECTO;
        public string UP_MEG_SEL_PROSPECTOSX_AGENCIA_USUARIO;
        public string UP_MEG_SEL_RESULTADO_PROSPECTO;
        public string UP_MEG_SEL_RESUM_CALIF_SBS_PROSPECTO;
    }
    public struct ProceduresAgrupamiento
    {

        public string UP_MEG_INS_EXCEL_AGRUPAMIENTO;
        //public string UP_MEG_SEL_COMSIONX_JUNTA_REGANTE; // PFA - PARAMETRICAS
        public string UP_MEG_SEL_DATOS_AGRUPAMIENTO;
        //public string UP_MEG_SEL_DATOS_INTEGRANTE;
        public string UP_MEG_SEL_LISTAR_AGRUPAMIENTOS;
        public string UP_MEG_SEL_LISTAR_INTEGRANTES;
        //public string UP_MEG_SEL_PARAMETRICAS_GRUPO; // PARAMETRICAS PFA
        public string UP_MEG_SEL_RESUM_CALIF_SBS_AGRUPAMIENTO;
        public string UP_MEG_SEL_UBIGEOS_XDEPARTAMENTO;
        public string UP_MEG_ACT_FORMA_PAGO_INTEGRANTE;
        //public string UP_MEG_SEL_PROPUESTA_INTEGRANTE;
        public string UP_MEG_SEL_GARANTIASX_INTEGRANTE;
        public string UP_MEG_SEL_AVALX_INTEGRANTE;
        //public string UP_MEG_SEL_CONSULTA_ERRORES_AGRUPAMIENTO;
        public string UP_MEG_ACT_DATOS_INTEGRANTE;
        public string UP_MEG_INS_REGFILES_LASERFICHE;
        //public string UP_MEG_SEL_SOLICITUDES_DETALLADO;
        //public string UP_MEG_INS_REG_APROB_EXCEPCION; 
        public string UP_MEG_ACT_ANULAR_AGRUPAMIENTO; 

        public string UP_PFA_INS_EXCEL_AGRUPAMIENTO;
        public string UP_PFA_SEL_LISTAR_AGRUPAMIENTOS;
        public string UP_PFA_SEL_DATOS_AGRUPAMIENTO;
        public string UP_PFA_SEL_LISTAR_INTEGRANTES;
        public string UP_PFA_SEL_LISTAR_SOLICITUDES;
        public string UP_PFA_SEL_SOLICITUDES_XFUNCIONARIOS;
        public string UP_PFA_SEL_CONSULTA_ERRORES_AGRUPAMIENTO;
        public string UP_PFA_SEL_CONSULTA_SOLICITUDES_XGRUPO;
        public string UP_PFA_SEL_DATOS_INTEGRANTE;
        public string UP_PFA_INS_REGISTRA_TIPO_CRONOGRAMA;
        public string UP_PFA_ACT_ACTUALIZA_PAGO_AT;

        public string UP_PFA_INS_REG_APROB_EXCEPCION;
        public string UP_PFA_ACT_APROBAR_EXCEPCION;
        public string UP_PFA_SEL_SOLICITUDES_REVISION;

        public string UP_PFA_ACT_ANULAR_SOLICITUD;
        public string UP_PFA_SEL_VALIDA_PERIODO_GRACIA;

        public string UP_PFA_SEL_PROPUESTA_INTEGRANTE;

    }
    public struct ProceduresGeoreferenciacion
    {
        public string UP_PFA_ACT_VINCULAR_FOTO_XSOLICITUD;
        public string UP_PFA_INS_FOTO_POR_SOLICITUD;
        public string UP_MEG_SEL_FOTOS_CERCANAS;
        public string UP_MEG_SEL_IMAGENESX_DOCUMENTO_SOLICITUD;
        public string UP_MEG_SEL_IMAGENESXSOLICITUD;
        
        //NUEVOS STORES
        public string UP_MEG_SEL_PERIMETRO;
        public string UP_MEG_SEL_INF_VISITA_XSOLICITUD;
        // STORES SQL
        public string UP_GEO_INS_CREAR_POLIGONO;
        public string UP_GEO_INS_DATOS_FOTOS;
        public string UP_GEO_INS_DATOS_SPATIAL;
    }
    public struct ProceduresParametricas
    {
        public string UP_UTIL_SEL_DEPARTAMENTOS;
        public string UP_UTIL_SEL_PROVINCIAS;
        public string UP_UTIL_SEL_DISTRITOS;
        //public string UP_MEG_PRM_SEL_AGENCIAS;
        //public string UP_MEG_SEL_FUNCIONARIOS;
        //public string UP_MEG_PRM_SEL_ORGANIZACIONES;
        //public string UP_MEG_PRM_SEL_COMISIONES_POR_ORGANIZACION;

        public string UP_PFA_SEL_PARAMETRICAS_GRUPO;
        public string UP_PFA_SEL_COMSIONX_JUNTA_REGANTE;
        public string UP_PFA_SEL_PARAMETRICAS_MATRIZ_COSTOS;

        public string UP_UTIL_SEL_CULTIVOS;
        public string UP_UTIL_SEL_FUNCIONARIOS;
        public string UP_UTIL_SEL_AGENCIAS;
        public string UP_UTIL_SEL_CULTIVOS_POR_SECTOR;
        public string UP_PFA_SEL_PRODUCTO_POR_SECTOR_PROGRAMA;

        public string UP_PFA_PRM_SEL_ORGANIZACIONES;
        public string UP_PFA_PRM_SEL_COMISIONES_POR_ORGANIZACION;



    }
    public struct ProceduresAccount
    {
        public string UP_MEG_SEL_USUARIO;
        public string UP_MEG_SEL_MENUS_XPERFIL;

        public string UP_AGR_SEL_USUARIO;
        public string UP_AGR_SEL_MENUS_XPERFIL;
    }
    public struct ProceduresSeguimiento
    {
        public string UP_PFA_SEL_AVANCE_SOLICITUDES;
        public string UP_PFA_SEL_AVANCE_SOLICITUDES_XAGENCIA;
        public string UP_PFA_SEL_SEG_XFUNCIONARIO;
        public string UP_PFA_SEL_SEG_XAGENCIAS;
        public string UP_PFA_SEL_SEGUIMIENTO_AGROPERU;
    }
    public struct ProceduresConsultas
    {
        public string UP_MEG_SEL_SOLICITUDESX_AGENCIA_USUARIO;
    }
    public struct ProceduresMantenimiento
    {
        public string UP_MEG_INS_CREAR_COMISION;
        public string UP_MEG_UPD_ACTUALIZA_COMISION;
        public string UP_MEG_UPD_ELIMINA_COMISION;
        public string UP_MEG_INS_CREAR_PROFTECNICO;

        public string UP_PFA_SEL_COSTOS;
        public string UP_PFA_SEL_COSTOS_HISTORICO;
        public string UP_PFA_SEL_COSTOS_DESCARGAR;        
        public string UP_PFA_SEL_COSTO_POR_CODIGO;
        public string UP_PFA_SEL_COSTOS_DETALLE;
        public string UP_PFA_SEL_COSTOS_DETALLE_TOTAL;
        public string UP_PFA_INS_REGISTRAR_COSTO;
        public string UP_PFA_ACT_DESHABILITAR_COSTO;



    }
    public struct ProceduresFiltroAgroperu
    {
        public string UP_MEG_INS_EXCEL_FILTROAGROPERU;
        public string UP_MEG_SEL_LISTAR_FILTROSAGROPERU;
        public string UP_MEG_SEL_RESULTADOS_FILTROAGROPERU;
    }
    public struct ProceduresPTC
    {
        public string UP_PTC_SEL_TOKEN_DATA;
    }


}

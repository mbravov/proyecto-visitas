﻿namespace Agrobanco.PlataformaAgroperu.Infraestructure.Settings
{
    public static class TypesConstants
    {
        public const string ExcelExtension = "xlsx";
        public const string ExcelExtensionXls = "xls";
        public const string WordExtension = "docx";
        public const string PdfExtension = "pdf";
        public const string JpgExtension = "jpg";
        public const string JpegExtension = "jpeg";

        public const string ExcelMIME = "application/vnd.ms-excel";
        public const string WordMIME = "application/msword";
        public const string PdfMIME = "application/pdf";
        public const string JpgMIME = "image/jpg";
        public const string JpegMIME = "image/jpeg";
        public const string DefaultMIME = "application/octet-stream";

        public static string GetTypeMIME(string extension)
        {
            var MIME = string.Empty;
            switch (extension)
            {
                case ExcelExtension:
                    MIME = ExcelMIME;
                    break;
                case ExcelExtensionXls:
                    MIME = ExcelMIME;
                    break;
                case WordExtension:
                    MIME = WordMIME;
                    break;
                case PdfExtension:
                    MIME = PdfMIME;
                    break;
                case JpgExtension:
                    MIME = JpgMIME;
                    break;
                case JpegExtension:
                    MIME = JpegMIME;
                    break;
                default:
                    MIME = DefaultMIME;
                    break;
            }
            return MIME;
        }


    }
}

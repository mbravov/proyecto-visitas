﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.Security.Criptography;
using Laserfiche.DocumentServices;
using Laserfiche.RepositoryAccess;

namespace Agrobanco.PlataformaAgroperu.Persistence.ContentManager
{
    public class LaserFicheDocumentAccess : IDisposable
    {
        private Server _serverLf;
        private Session _sessionLf;
        private readonly string _repositoryName;
        private readonly string _userName;
        private readonly string _password;
        private readonly string _hostName;

#pragma warning disable CS0649
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
#pragma warning restore CS0649
        public LaserFicheDocumentAccess(string repositoryName)
        {

            Compose();
            _decriptService.Application = ApplicationKeys.LaserFicheRegeditFolder;
            _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;

            _repositoryName = repositoryName;
            _userName = _decriptService.ReadValue("Usuario");
            _password = _decriptService.ReadValue("Clave");
            _hostName = _decriptService.ReadValue("Server"); // "41.50.13.193";
        }

        private DocumentInfo GetDocumentInfo(int entryId)
        {
            DocumentInfo documentInfo = null;
            
            try
            {
                _serverLf = new Server(_hostName);
                _sessionLf = GetSession();
                var repositoryCollection = _serverLf.GetRepositories();
                var repository = repositoryCollection[_repositoryName];
                if (!_sessionLf.IsAuthenticated)
                    _sessionLf.LogIn(_userName, _password, repository);
                documentInfo = Document.GetDocumentInfo(entryId, _sessionLf);
                
            }
            catch (LaserficheRepositoryException Lfex)
            {
                Console.WriteLine(Lfex);
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e);
                //throw new TimeoutException(e.Message, e);
            }
            finally
            {
                documentInfo?.Save();
                documentInfo?.Unlock();
                documentInfo?.Dispose();
            }

            return documentInfo;
        }

        public MemoryStream GetDocument(int entryId, out string fileName, out string Extension)
        {
            var documentInfo = GetDocumentInfo(entryId);
            MemoryStream stream = null;
            fileName = null;
            Extension = null;
            if (documentInfo != null)
            {
                var documentExporter = new DocumentExporter { IncludeAnnotations = true };
                stream = new MemoryStream();
                fileName = documentInfo.GetLocalName();
                Extension = documentInfo.Extension;
                
                if (!documentInfo.Extension.Equals("pdf", StringComparison.CurrentCultureIgnoreCase) || documentInfo.PageCount == 0)
                    documentExporter.ExportElecDoc(documentInfo, stream);
                else
                    documentExporter.ExportPdf(documentInfo, documentInfo.AllPages, PdfExportOptions.None, stream);
            }


            return stream;
        }


        public MemoryStream GetImage(int entryId, out string fileName)
        {
            var documentInfo = GetDocumentInfo(entryId);
            MemoryStream stream = null;
            fileName = null;
            if (documentInfo != null)
            {
                var documentExporter = new DocumentExporter { IncludeAnnotations = true };
                stream = new MemoryStream();
                fileName = documentInfo.GetLocalName();
                if (!documentInfo.Extension.Equals("pdf", StringComparison.CurrentCultureIgnoreCase))
                    if (documentInfo.ElecDocumentSize != 0)
                        documentExporter.ExportElecDoc(documentInfo, stream);
                    else
                        documentExporter.ExportPage(documentInfo, 1, stream);
                else
                    throw new InvalidOperationException("Archivo no es una imagen");
            }
            
            return stream;
        }

        public int RegisterPdfWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName)
        {
            var documentId = RegisterDocumentWithMetaData(stream, metaData, filePath, volumen, templateName, "application/pdf");
            return documentId;
        }


        public int RegisterExcelWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName, bool overwrite = false)
        {
            var documentId = RegisterDocumentWithMetaData(stream, metaData, filePath, volumen, templateName, "application/vnd.ms-excel", overwrite);
            return documentId;
        }

        public int RegisterImageWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName, bool overwrite = false, string contenType = "")
        {
            var documentId = RegisterDocumentWithMetaData(stream, metaData, filePath, volumen, templateName, contenType, overwrite);
            return documentId;
        }


        public int RegisterImageWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName, out string fileName, bool overwrite = false, string contenType = "")
        {
            var documentId = RegisterDocumentWithMetaData(stream, metaData, filePath, volumen, templateName, contenType, overwrite);
            var docInfo = GetDocumentInfo(documentId);
            fileName = docInfo.Name;
            return documentId;
        }

        public int RegisterFileWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName, out string fileName, bool overwrite = false, string contenType = "")
        {
            var documentId = RegisterDocumentWithMetaData(stream, metaData, filePath, volumen, templateName, contenType, overwrite);
            var docInfo = GetDocumentInfo(documentId);
            fileName = docInfo.Name;
            return documentId;
        }

        private int RegisterDocumentWithMetaData(MemoryStream stream, Dictionary<string, object> metaData, string filePath, string volumen, string templateName, string contenType = "", bool overwrite = false)
        {
            DocumentInfo documentInfo = null;
            try
            {
                _sessionLf = GetSession();
                var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
                if (!_sessionLf.IsAuthenticated)
                    _sessionLf.LogIn(_userName, _password, repositoryReg);

                if (overwrite)
                {
                    var entry = Entry.TryGetEntryInfo(filePath, _sessionLf);
                    if (entry != null)
                    {
                        entry.Delete();
                        _sessionLf.Save();
                    }
                }

                var document = new DocumentInfo(_sessionLf);
                var documentMetadata = new FieldValueCollection();

                document.Create(filePath, volumen, EntryNameOption.AutoRename);
                var lfDocumentId = document.Id;
                documentInfo = (DocumentInfo)Entry.GetEntryInfo(lfDocumentId, _sessionLf);

                foreach (var metadataEntry in metaData)
                    documentMetadata.Add(metadataEntry.Key, metadataEntry.Value);

                var documentImporter = new DocumentImporter();




                documentInfo.SetTemplate(templateName, documentMetadata);
                documentImporter.Document = documentInfo;

                if (string.IsNullOrEmpty(contenType))
                    documentImporter.ImportImages(stream);
                else
                    documentImporter.ImportEdoc(contenType, stream);
                return lfDocumentId;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                //_sessionLf.LogOut();
                documentInfo?.Save();
                documentInfo?.Unlock();
                documentInfo?.Dispose();
            }
        }

        public void MoveDocument(int lfDocumentId, string newPath)
        {
            DocumentInfo documentInfo = null;
            try
            {
                _sessionLf = GetSession();
                var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
                if (!_sessionLf.IsAuthenticated)
                    _sessionLf.LogIn(_userName, _password, repositoryReg);
                documentInfo = (DocumentInfo)Entry.GetEntryInfo(lfDocumentId, _sessionLf);
                documentInfo.MoveTo(newPath, EntryNameOption.AutoRename);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                documentInfo?.Save();
                documentInfo?.Unlock();
                documentInfo?.Dispose();
            }
        }

        public void DeleteDocumentIfExist(string filePath)
        {
            try
            {
                _sessionLf = GetSession();
                var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
                if (!_sessionLf.IsAuthenticated)
                    _sessionLf.LogIn(_userName, _password, repositoryReg);

                var entry = Entry.TryGetEntryInfo(filePath, _sessionLf);
                if (entry == null) return;
                entry.Delete();
                _sessionLf.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        public FolderInfo GetOrCreateFolderInfo(string path)
        {
            _sessionLf = GetSession();
            var repositoryReg = new RepositoryRegistration { ServerName = _hostName, Name = _repositoryName };
            if (!_sessionLf.IsAuthenticated)
                _sessionLf.LogIn(_userName, _password, repositoryReg);
            switch (path)
            {
                case null:
                    throw new ArgumentNullException(nameof(path));
                case "":
                    throw new ArgumentException(nameof(path));
            }
            var entry = Entry.TryGetEntryInfo(path, _sessionLf);
            switch (entry)
            {
                case null:
                    var folderInf = CreateHelper(path, _sessionLf);
                    //_sessionLf.LogOut();
                    return folderInf;
                case FolderInfo folder:
                    folder.Unlock();
                    //_sessionLf.LogOut();
                    return folder;
            }
            throw new DuplicateObjectException("Object already exists");
        }

        private FolderInfo CreateHelper(string path, Session session)
        {
            EntryInfo entry = null;
            var toCreate = new Stack<string>();
            if (!path.StartsWith("\\"))
                path = "\\" + path;

            // This is equivalent to the recursion in the other
            // solution, but with an explicit stack instead of relying
            // on the call stack for state.
            while (entry == null)
            {
                toCreate.Push(path.Split('\\').Last());
                path = path.Substring(0, path.LastIndexOf('\\'));
                if (path == "")
                    path = "\\";
                entry = Entry.TryGetEntryInfo(path, session);
            }
            if (!(entry is FolderInfo folder))
                throw new DuplicateObjectException("La raiz de la ruta no es una carpeta.");

            // Walk back up the stack as if returning from recursive
            // calls.
            FolderInfo parent = null;
            try
            {
                while (toCreate.Count > 0)
                {
                    parent?.Dispose();
                    parent = folder;
                    folder = new FolderInfo(session);
                    path = toCreate.Pop();
                    folder.Create(parent, path, EntryNameOption.None);
                }
            }
            catch (Exception)
            {
                // Guarantee that folder does not leak if an Exception
                // is thrown.
                folder.Dispose();
                throw;
            }
            finally
            {
                // Guarantee that parent does not leak.
                parent?.Dispose();
            }
            folder.Unlock();
            return folder;
        }

        private Session GetSession()
        {
            if (_sessionLf is null)
                _sessionLf = new Session();
            return _sessionLf;
        }

        public void Dispose()
        {
            _sessionLf?.Close();
            if (_sessionLf.IsAuthenticated)
                _sessionLf?.LogOut();
            _serverLf?.Dispose();
            GC.SuppressFinalize(_sessionLf);
        }


        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }
    }
}
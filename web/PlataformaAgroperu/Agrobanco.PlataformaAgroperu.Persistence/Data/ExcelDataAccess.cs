﻿using System;
using System.IO;
using Agrobanco.PlataformaAgroperu.Domain.Models;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using NPOI.HSSF.UserModel;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Agrobanco.PlataformaAgroperu.Persistence.Data
{
    public class ExcelDataAccess : IDisposable
    {
        private readonly Stream _stream;
        readonly IWorkbook _workbook;
        private string _sheetName;        
        public DateTime fecha_creacion;
        public string creator;

        public string SheetName
        {
            get => _sheetName;
        }

        public ExcelDataAccess(Stream stream, string excelFileExtension, int sheetIndex = 0)
        {
            _stream = stream;
            
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                ms.Position = 0;
                _workbook = excelFileExtension.Equals(TypesConstants.ExcelExtension, StringComparison.OrdinalIgnoreCase) ? (IWorkbook)new XSSFWorkbook(ms) : new HSSFWorkbook(ms);
                //WorkbookFactory
            }
            _sheetName = _workbook.GetSheetAt(sheetIndex).SheetName;
        }

        public ExcelDataAccess(Stream stream, string excelFileExtension, string sheetName)
        {
            _stream = stream;            
            
            MemoryStream ms=null;

            var buffer = new byte[16 * 1024];
            //var buffer1 = new byte[16 * 1024];
            using (ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                ms.Position = 0;
                _workbook = excelFileExtension.Equals(TypesConstants.ExcelExtension, StringComparison.OrdinalIgnoreCase) ? (IWorkbook)new XSSFWorkbook(ms) : new HSSFWorkbook(ms);

                 //SheetName= _workbook.GetSheetAt(0).SheetName;
            }
            _sheetName = _workbook.GetSheetAt(0).SheetName;

        }


        public T GetCellValue<T>(int fila, int columna)
        {
            var sheet = _workbook.GetSheet(_sheetName);
            var cellValue = sheet.GetRow(fila).GetCellValue(columna).Trim();
            if (string.IsNullOrWhiteSpace(cellValue))
                return default(T);
            var value = (T)Convert.ChangeType(cellValue, typeof(T), null);
            return value;
        }

        public Cell GetCell(int fila, int columna)
        {
            Cell cell=new Cell(fila, columna);            
            cell.value = ObtenerValor(GetCellValue<string>(fila, columna));
            return cell;
            
        }

        public void GetCell(ref Cell cell)
        {            
            cell.value = ObtenerValor(GetCellValue<string>(cell.row, cell.column));            
        }

        public void SelectWorkBookSheet(string sheetName)
        {
            _sheetName = sheetName;
        }

        private string ObtenerValor(string cellValue)
        {
            string[] arr = null;

            string valor = null;
            if (string.IsNullOrWhiteSpace(cellValue)==false)
            {
                arr = cellValue.Split('_');
                if (arr.Length == 2)
                {
                    valor = arr[1].ToString().TrimEnd();
                }
                else {
                    valor = arr[0];
                }

            }
            else {
                valor = "";
            }                                        
            return valor;
        }
        public bool EsValido(string cell, bool validaVacio)
        {
            bool val = true;
            if (validaVacio == true)
            {
                if (string.IsNullOrWhiteSpace(cell) == true) { val = false; };
            }
            return val;
        }

        public IRow GetRow(int fila)
        {
            var sheet = _workbook.GetSheet(_sheetName);
            var row = sheet.GetRow(fila);
            return row;
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

    }
}
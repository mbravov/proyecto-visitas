﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.Security.Criptography;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;

namespace Agrobanco.PlataformaAgroperu.Persistence.Data
{
    public class MsSqlDataAccess : IDataAccess
    {

        private readonly string _connectionString;

        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;

        private SqlConnection _dbConnection;

        [NonSerialized]
        private SqlDataAdapter _dbDataAdapter;

        [NonSerialized]
        private SqlCommand _dbCommand;
        private bool _isAutoConnectionMgmt = false;

        [NonSerialized]
        private SqlTransaction _dbTransaction;

        [NonSerialized]
        private SqlCommandBuilder _dbCommandBuilder;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool AutoConnection
        {
            get => (_isAutoConnectionMgmt);
            set => _isAutoConnectionMgmt = value;
        }

        public void OpenConnection()
        {
            try
            {
                if (_dbConnection != null)
                {
                    if (_dbConnection.State != ConnectionState.Open)
                    {
                        _dbConnection = new SqlConnection(_connectionString);
                        _dbConnection.Open();
                    }
                    else
                    {
                        // else nothing
                    }
                }
                else
                {
                    _dbConnection = new SqlConnection(_connectionString);
                    _dbConnection.Open();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public MsSqlDataAccess()
        {
            _isAutoConnectionMgmt = true;
            Compose();
            _decriptService.Application = ApplicationKeys.SqlRegeditFolder;
            _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;
            var conectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = _decriptService.ReadValue("Server"),
                InitialCatalog = _decriptService.ReadValue("Database"),
                UserID = _decriptService.ReadValue("Usuario"),
                Password = _decriptService.ReadValue("Clave")
            };
            _connectionString = conectionStringBuilder.ConnectionString;
            this.AutoConnection = true;
        }

        ~MsSqlDataAccess()
        {
            this.Dispose();
        }

        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        public void CloseConnection()
        {
            try
            {
                if (_dbConnection != null)
                {
                    if (_dbConnection.State != ConnectionState.Closed)
                    {
                        _dbConnection.Close();
                    }
                    else
                    {
                        // else nothing
                    }
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataSet(string strDbQuery, out DataSet dsResult)
        {
            dsResult = new DataSet();

            try
            {
                this.OpenConnection();

                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataSet(string strDbQuery, DataSet dsResult)
        {
            try
            {
                this.OpenConnection();

                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataTable(string strDbQuery, out DataTable dtResult)
        {
            dtResult = new DataTable();
            try
            {
                this.OpenConnection();
                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataTable(string strDbQuery, DataTable dtResult)
        {
            try
            {
                this.OpenConnection();
                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataReader(string strDbQuery, out IDataReader drResult)
        {
            drResult = null;
            SqlConnection dbConnection = new SqlConnection(this._connectionString);

            SqlCommand dbCommand = new SqlCommand(strDbQuery, dbConnection);
            try
            {
                dbCommand.CommandType = CommandType.Text;

                dbCommand.Connection.Open();

                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException)
            {
                if (dbCommand.Connection.State.Equals(ConnectionState.Open))
                    dbCommand.Connection.Close();
                throw;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetScalarValue(string strDbQuery, out string strResult)
        {
            strResult = string.Empty;
            try
            {
                this.OpenConnection();

                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                strResult = _dbCommand.ExecuteScalar().ToString();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetXmlReader(string strDbQuery, out XmlReader xmlReaderResult)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void ExecuteQuery(string strDbQuery, out int intRowsAffected)
        {
            intRowsAffected = 0;

            try
            {
                this.OpenConnection();

                _dbCommand = new SqlCommand(strDbQuery, _dbConnection);
                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                intRowsAffected = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void BatchUpdate(DataSet dataSetToBeUpdated)
        {
            if (this._dbDataAdapter != null)
            {
                this.OpenConnection();

                if (_dbTransaction != null)
                {
                    if (_dbDataAdapter.UpdateCommand != null)
                    {
                        _dbDataAdapter.UpdateCommand.Connection = _dbConnection;
                        _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.DeleteCommand != null)
                    {
                        _dbDataAdapter.DeleteCommand.Connection = _dbConnection;
                        _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.InsertCommand != null)
                    {
                        _dbDataAdapter.InsertCommand.Connection = _dbConnection;
                        _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.SelectCommand != null)
                    {
                        _dbDataAdapter.SelectCommand.Connection = _dbConnection;
                        _dbDataAdapter.SelectCommand.Transaction = _dbTransaction;
                    }
                }
                else
                {
                    // else nothing
                }

                this._dbDataAdapter.Update(dataSetToBeUpdated);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }

            }
            else
            {
                throw new Exception("The Data Access Component Object was destroyed, Dababase could not be updated.");
            }
        }

        public void BatchUpdate(DataTable dataTableToBeUpdated)
        {
            if (this._dbDataAdapter != null)
            {
                this.OpenConnection();

                if (_dbTransaction != null)
                {
                    if (_dbDataAdapter.UpdateCommand != null)
                    {
                        _dbDataAdapter.UpdateCommand.Connection = _dbConnection;
                        _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.DeleteCommand != null)
                    {
                        _dbDataAdapter.DeleteCommand.Connection = _dbConnection;
                        _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.InsertCommand != null)
                    {
                        _dbDataAdapter.InsertCommand.Connection = _dbConnection;
                        _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.SelectCommand != null)
                    {
                        _dbDataAdapter.SelectCommand.Connection = _dbConnection;
                        _dbDataAdapter.SelectCommand.Transaction = _dbTransaction;
                    }
                }
                else
                {
                    // else nothing
                }

                this._dbDataAdapter.Update(dataTableToBeUpdated);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }

            }
            else
            {
                throw new Exception("The Data Access Component Object was destroyed, Dababase could not be updated.");
            }
        }

        public int MannualBatchUpdate(DataTable dataTableToBeUpdated, IDbCommand insertCommand, IDbCommand updateCommand, IDbCommand deleteCommand)
        {
            this.OpenConnection();

            _dbDataAdapter = new SqlDataAdapter();

            if (insertCommand != null)
            {
                _dbDataAdapter.InsertCommand = (SqlCommand)insertCommand;
                _dbDataAdapter.InsertCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                }
            }
            if (updateCommand != null)
            {
                _dbDataAdapter.UpdateCommand = (SqlCommand)updateCommand;
                _dbDataAdapter.UpdateCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                }
            }
            if (deleteCommand != null)
            {
                _dbDataAdapter.DeleteCommand = (SqlCommand)deleteCommand;
                _dbDataAdapter.DeleteCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                }
            }

            int intAffectedRows = 0;
            intAffectedRows = _dbDataAdapter.Update(dataTableToBeUpdated);

            if (_isAutoConnectionMgmt)
            {
                this.CloseConnection();
            }
            return intAffectedRows;
        }

        public IDbCommand CreateCommand(string strQueryOrSp, CommandType commandType)
        {
            IDbCommand command = new SqlCommand();
            if (strQueryOrSp != null && !strQueryOrSp.Trim().Equals(string.Empty))
            {
                command.CommandText = strQueryOrSp;
            }
            else
            {
                throw new InvalidOperationException("Invalid Command Text.");
            }

            if (!commandType.Equals(null))
            {
                command.CommandType = commandType;
            }
            else
            {
                throw new InvalidOperationException("Invalid Command Type.");
            }

            return (IDbCommand)command;
        }

        public IDataParameter CreateParameter(string paramName, DbType paramType, int paramSize, ParameterDirection paramDirection, string sourceColumn)
        {
            if (paramName == null || paramName.Trim().Equals(string.Empty))
            {
                throw new InvalidOperationException("Invalid Parameter Name.");
            }

            if (paramType.Equals(null))
            {
                throw new InvalidOperationException("Invalid Parameter Type.");
            }

            if (paramDirection.Equals(null))
            {
                paramDirection = ParameterDirection.Input;
            }

            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = paramName;
            parameter.DbType = paramType;
            parameter.Size = paramSize;
            parameter.Direction = paramDirection;

            if (sourceColumn != null && !sourceColumn.Trim().Equals(string.Empty))
            {
                parameter.SourceColumn = sourceColumn;
            }

            return ((IDbDataParameter)parameter);

        }

        private void CreateCommand(string strProcedureName, SqlParameter[] arrDbParameter)
        {
            try
            {
                this.OpenConnection();
                _dbCommand = new SqlCommand(strProcedureName, _dbConnection);
                _dbCommand.CommandType = CommandType.StoredProcedure;
                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }
                else
                {
                    // else nothing
                }

                if (arrDbParameter != null)
                {
                    foreach (SqlParameter parameterLoop in arrDbParameter)
                    {
                        _dbCommand.Parameters.Add(parameterLoop);
                    }
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);
                _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, IDataParameter[] arrDbParameter)
        {
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows)
        {
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, null);
                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows, IDataParameter[] arrDbParameter)
        {
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows, IDataParameter[] arrDbParameter, int intCommandTimeOut)
        {
            //int intOldCommandTimeOut;
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);
                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out object objScalarResult)
        {
            objScalarResult = null;
            try
            {
                this.CreateCommand(strProcedureName, null);
                objScalarResult = _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out object objScalarResult, IDataParameter[] arrDbParameter)
        {
            objScalarResult = null;
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                objScalarResult = _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out IDataReader drResult)
        {
            drResult = null;

            try
            {
                SqlConnection dbConnection = new SqlConnection(this._connectionString);

                SqlCommand dbCommand = new SqlCommand(strProcedureName, dbConnection);

                dbCommand.CommandType = CommandType.StoredProcedure;

                dbCommand.Connection.Open();

                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out IDataReader drResult, IDataParameter[] arrDbParameter)
        {
            drResult = null;

            try
            {
                SqlConnection dbConnection = new SqlConnection(this._connectionString);

                SqlCommand dbCommand = new SqlCommand(strProcedureName, dbConnection);

                dbCommand.CommandType = CommandType.StoredProcedure;

                if (arrDbParameter != null)
                {
                    foreach (SqlParameter parameterLoop in arrDbParameter)
                    {
                        dbCommand.Parameters.Add(parameterLoop);
                    }
                }

                dbCommand.Connection.Open();
                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataSet dsResult)
        {
            dsResult = new DataSet();

            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataSet dsResult)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataSet dsResult, IDataParameter[] arrDbParameter)
        {
            dsResult = new DataSet();
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataSet dsReturn, IDataParameter[] arrDbParameter)
        {
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsReturn);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataTable dtReturn)
        {
            dtReturn = new DataTable();
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtReturn);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataTable dtResult)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataTable dtResult, IDataParameter[] arrDbParameter)
        {
            dtResult = new DataTable();

            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataTable dtResult, IDataParameter[] arrDbParameter)
        {
            try
            {
                this.CreateCommand(strProcedureName, (SqlParameter[])arrDbParameter);

                _dbDataAdapter = new SqlDataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new SqlCommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out XmlReader xmlReaderResult)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void RunStoredProcedure(string strProcedureName, out XmlReader xmlReaderResult, IDataParameter[] arrDbParameter)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void BeginTransaction(IsolationLevel objIsoLevel)
        {
            try
            {
                this.OpenConnection();
                _dbTransaction = _dbConnection.BeginTransaction(objIsoLevel);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (SqlException except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void BeginTransaction(string strNameOfTransaction, IsolationLevel objIsoLevel)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void RollbackTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Rollback();
                }
                catch (InvalidOperationException except)
                {
                    throw except;
                }
                catch (SqlException except)
                {
                    throw except;
                }
                catch (Exception except)
                {
                    throw except;
                }
            }
            else
            {
                Exception objException = new Exception("Transaction Object was disposed, Can not Rollback");
                throw objException;
            }
        }

        public void RollbackTransaction(string strNameOfTransactionOrSavePoint)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void CommitTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Commit();
                }
                catch (InvalidOperationException except)
                {
                    throw except;
                }
                catch (SqlException except)
                {
                    throw except;
                }
                catch (Exception except)
                {
                    throw except;
                }
            }
            else
            {
                Exception objException = new Exception("Transaction Object was disposed, Can not Commit");
                throw objException;
            }
        }

        public void SetSavePoint(string strSavePointName)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public SqlParameter CreateParameter(string name, int size, object value = null, int scale = -1, SqlDbType dataType = SqlDbType.VarChar, ParameterDirection direction = ParameterDirection.Input)
        {
            
            SqlParameter parameter = new SqlParameter(name, dataType);

            if (size != -1)
                parameter.Size = size;

            if (scale != -1)
                parameter.Scale = Convert.ToByte(scale);

            parameter.Direction = direction;

            parameter.Value = SetDbNullIfDefaultValue(value, dataType);            

            return parameter;
        }

        private object SetDbNullIfDefaultValue(object value, SqlDbType dataType)
        {
            object returnValue = DBNull.Value;
            switch (dataType)
            {
                //case SqlDbType.iDB2Clob:
                //    returnValue = value;
                //    break;
                case SqlDbType.Date:
                    returnValue = value ?? DBNull.Value;
                    break;
                case SqlDbType.Decimal:
                    returnValue = value;
                    break;
                case SqlDbType.Int:
                    returnValue = value;
                    break;
                //case SqlDbType.iDB2Numeric:
                //    returnValue = value;
                //    break;
                case SqlDbType.VarChar:
                    returnValue = string.IsNullOrEmpty((string)value) ? DBNull.Value : value;
                    break;
                case SqlDbType.NVarChar:
                    returnValue = string.IsNullOrEmpty((string)value) ? DBNull.Value : value;
                    break;
            }
            return returnValue;
        }

    }
}
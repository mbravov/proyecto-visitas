﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class AccountData
    {
        private readonly Db2DataAccess _db2DataContext;
        public AccountData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        public Usuario BuscarUsuario(string v_usuario,string v_clave)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("V_USUARIO", 15, v_usuario);
            lDataParam[1] = _db2DataContext.CreateParameter("V_CLAVE", 32, v_clave);
            Logger.LogInformation("Inicio: Consultando usuario");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.User.UP_MEG_SEL_USUARIO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta usuario");
            List<Usuario> lusuario = new List<Usuario>();
            Usuario usuario = null;
            while (reader.Read())
            {
                usuario = new Usuario();
                usuario.User = reader.GetString("USUARIO");
                usuario.Password = reader.GetString("CLAVE");
                usuario.Name = reader.GetString("NOMBRE_USUARIO");
                usuario.CodigoAgencia = reader.GetInt("CODIGO_AGENCIA");
                usuario.NombreAgencia = reader.GetString("NOMBRE_AGENCIA");
                usuario.CodigoFuncionario = reader.GetString("CODIGO_FUNCIONARIO").Trim();
                usuario.CodigoPerfil = reader.GetInt("CODIGO_PERFIL");
                usuario.Perfil = reader.GetString("PERFIL");
                
                lusuario.Add(usuario);
            }

            if (usuario==null) {
                return null;
            }

            var ListMenu = ListarMenuPorPerfil(usuario.CodigoPerfil);
            lusuario[0].ListaMenu = ListMenu;

            return lusuario[0];
        }
        public Usuario BuscarUsuario(int TokenId)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_IDTOKEN", 12, TokenId, 0, iDB2DbType.iDB2Numeric);

            Logger.LogInformation("Inicio: Consultando usuario por Token");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.PTC.UP_PTC_SEL_TOKEN_DATA , out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta usuario por Token");
            List<Usuario> lusuario = new List<Usuario>();
            Usuario usuario = null;
            while (reader.Read())
            {
                usuario = new Usuario();
                usuario.User = reader.GetString("USUARIO");
                //usuario.Password = reader.GetString("CLAVE");
                usuario.Name = reader.GetString("WEBNOM");
                usuario.CodigoAgencia = reader.GetInt("AGENCIA");
                usuario.NombreAgencia = reader.GetString("DES_AGENCIA");
                usuario.CodigoFuncionario = reader.GetString("CODFUNC");
                usuario.CodigoPerfil = reader.GetInt("IDDOCUMENTO");
                usuario.Perfil = reader.GetString("DESCRIPCION1");
                usuario.Plataforma = reader.GetString("PLATAFORMA");

                lusuario.Add(usuario);
                //break;
            }

            if (usuario == null)
            {
                return null;
            }

            var perfiles = ApplicationKeys.HomePerfiles;
            usuario.HomePageEstadistico = perfiles.Contains(usuario.CodigoPerfil.ToString());

            var ListMenu = ListarMenuPorPerfil(usuario.CodigoPerfil);
            lusuario[0].ListaMenu = ListMenu;

            return lusuario[0];
        }

        private List<Menu> ListarMenuPorPerfil(int CodigoPerfil)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("V_PERFIL", 6, CodigoPerfil, 0, iDB2DbType.iDB2Decimal);            
            lDataParam[1] = _db2DataContext.CreateParameter("V_APLICACION", 6, "020", 0, iDB2DbType.iDB2Integer);

            Logger.LogInformation("Inicio: Consultando Menus");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.User.UP_AGR_SEL_MENUS_XPERFIL, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta MENU");
            List<Menu> LstMenu = new List<Menu>();
            Menu menu = null;
            Opcion opcion = null;
            var CodMenu = 0;
            while (reader.Read())
            {
                CodMenu = reader.GetInt("CODIGO_MENU");                

                Menu findMenu = LstMenu.Find(x => x.CodigoMenu == CodMenu);
                if(findMenu == null)
                {
                    menu = new Menu();
                    menu.Opciones = new List<Opcion>();
                    menu.CodigoMenu = reader.GetInt("CODIGO_MENU");
                    menu.Controller = reader.GetString("CONTROLLER");
                    menu.DescripcionMenu = reader.GetString("DES_MENU");
                    menu.Icono = reader.GetString("ICONO");
                    opcion = new Opcion();
                    opcion.Action = reader.GetString("ACTION");
                    opcion.Descripcion = reader.GetString("DES_OPCION");
                    menu.Opciones.Add(opcion);
                    LstMenu.Add(menu);
                }
                else
                {
                    LstMenu.Remove(findMenu);
                    opcion = new Opcion();
                    opcion.Action = reader.GetString("ACTION");
                    opcion.Descripcion = reader.GetString("DES_OPCION");
                    findMenu.Opciones.Add(opcion);
                    LstMenu.Add(findMenu);
                }
            }

            return LstMenu;
        }



    }
}

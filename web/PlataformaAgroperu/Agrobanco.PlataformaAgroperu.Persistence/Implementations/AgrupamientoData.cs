﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.ContentManager;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class AgrupamientoData
    {
        private readonly Db2DataAccess _db2DataContext;
        public AgrupamientoData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }
        
        public void RegistrarAgrupamiento(ref ExcelResponseModel respon, Grupo Agrupamiento, string FechaCliente)
        {
            try
            {
                var parameters = new iDB2Parameter[12];
                var (IdResult, CodigoGrupo) = GenerarCodigoGrupo();
                if (IdResult == "0000")
                {
                    string idgrupo = CodigoGrupo;
                    parameters[0] = _db2DataContext.CreateParameter("VI_TimeClient", 50, FechaCliente);
                    parameters[1] = _db2DataContext.CreateParameter("VI_CodigoGrupo", 12, idgrupo);
                    parameters[2] = _db2DataContext.CreateParameter("VI_NumeroRegistros", 4, Agrupamiento.ExcelIntegrantes.Count, 0, iDB2DbType.iDB2Integer);
                    parameters[3] = _db2DataContext.CreateParameter("VI_CodigoAnalista", 4, Agrupamiento.DatosGrupo.Analista);
                    parameters[4] = _db2DataContext.CreateParameter("VI_NombreAnalista", 35, Agrupamiento.DatosGrupo.NombreAnalista);
                    parameters[5] = _db2DataContext.CreateParameter("VI_CodigoAgencia", 2, Agrupamiento.DatosGrupo.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                    parameters[6] = _db2DataContext.CreateParameter("IV_Nivel", 1, 0, 0, iDB2DbType.iDB2Integer);
                    parameters[7] = _db2DataContext.CreateParameter("VI_Usuario", 10, Agrupamiento.DatosGrupo.Usuario);
                    parameters[8] = _db2DataContext.CreateParameter("VI_TramaCabecera", 20000, Agrupamiento.DatosGrupo.getTrama());
                    parameters[9] = _db2DataContext.CreateParameter("VI_TramaDetalle", 1000000, Agrupamiento.getTramaGrupoAgroperu(), 0, iDB2DbType.iDB2Clob);
                    parameters[10] = _db2DataContext.CreateParameter("VI_Plataforma", 3, ApplicationConstants.IdPlaformaAgroperu);
                    parameters[11] = _db2DataContext.CreateParameter("VO_MSG", 150, "", 0, iDB2DbType.iDB2VarChar);
                    parameters[11].Direction = System.Data.ParameterDirection.Output;
                    Logger.LogInformation("Registrando Agrupamiento");
                    Logger.LogDbParameters(parameters);
                    _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_INS_EXCEL_AGRUPAMIENTO, parameters, 100);
                    Logger.LogInformation("Fin Registrando Agrupamiento");
                    string msg = parameters[11].Value.ToString();
                    respon = new ExcelResponseModel(msg);

                    string ev = EvaluarGrupo(idgrupo);
                    Logger.LogInformation("Fin Evaluar Agrupamiento");
                }
            }
            catch (Exception ex)
            {
                respon.code = "10";
                respon.message= "SP:RegistrarAgrupamiento:" + ex.Message;
            }
        }

        public (string IdResult, string CodigoGrupo) GenerarCodigoGrupo()
        {
            ParametricasData oPar = new ParametricasData();
            var Result = oPar.EjecutarProgramaT20000(
                Transaccion: As400Constants.Transaccion.GeneraCodigo,
                Aplicacion: As400Constants.AplicacionJR,
                Empresa: As400Constants.Banco01,
                Filler: "f",
                Parametro: As400Constants.Claves.CodigoGrupo);

            return (Result.Substring(0, 4), Result.Substring(4, 12));
        }

        public string EvaluarGrupo(string CodigoGrupo)
        {
            ParametricasData oPar = new ParametricasData();
            return oPar.EjecutarProgramaT20000(
                Transaccion: As400Constants.Transaccion.EvaluaGrupoAgroperu,
                Aplicacion: As400Constants.AplicacionFondoAgroperu,
                Empresa: As400Constants.Banco12,
                Filler: CodigoGrupo,
                Parametro: CodigoGrupo);
        }

        public string ValidarPlazoCronograma(
            string NumeroSolicitud, int PeriodoGraciaFecha, string TipoCuota, int NumeroCuotas, int Frecuencia, string UnidadFrecuencia,
            out string Mensaje)
        {
            ParametricasData oPar = new ParametricasData();
            DateTime Fecha = DateTime.Now;
            var E_par2 = "";
            var Solicitud = NumeroSolicitud.PadLeft(12, '0');
            var FechaOperacion = Fecha.Year.ToString() + Fecha.Month.ToString().PadLeft(2, '0') + Fecha.Day.ToString().PadLeft(2, '0');
            var FechaGracia = PeriodoGraciaFecha.ToString();
            var Tipo = TipoCuota;
            var NroFrecuencia = Frecuencia.ToString().PadLeft(4, '0');
            var TerFrecuencia = UnidadFrecuencia;
            var Cuotas = NumeroCuotas.ToString().PadLeft(3, '0');

            E_par2 = $"{Solicitud}{FechaOperacion}{FechaGracia}{Tipo}{NroFrecuencia}{TerFrecuencia}{Cuotas}";

            var Result = oPar.EjecutarProgramaT20000(
                Transaccion: As400Constants.Transaccion.ObtenerVencimiento_Plazo,
                Aplicacion: As400Constants.AplicacionUT,
                Empresa: As400Constants.Banco01,
                Filler: "",
                Parametro: E_par2);

            var IdResult = Result.Substring(0, 4);
            var PlazoResult = Result.Substring(24,4);
            Mensaje = Result.Substring(28, 74);
            var Plazo = IdResult == "0000" ? Convert.ToInt32(PlazoResult) : 0;

            return  IdResult;
        }


        public MemoryStream ExportToExcel(DataTable sourceTable)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Sheet1");
            IRow headerRow = sheet.CreateRow(0);

            // Create Header Style
            ICellStyle headerCellStyle = workbook.CreateCellStyle();

            var font = workbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.FontName = "Calibri";
            //font.Color = IndexedColors.DarkGreen.Index;

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            headerCellStyle.SetFont(font);
            headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Blue.Index;
            //headerCellStyle.FillPattern = FillPattern.BigSpots;
            //headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Pink.Index;
            //headerCellStyle.FillBackgroundColor= IndexedColors.Orange.Index;
            //headerCellStyle.FillForegroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillPattern = FillPattern.SolidForeground;
            //headerCellStyle.FillBackgroundColorColor= IndexedColors.RoyalBlue.Index;
            // headerCellStyle.FillBackgroundColor;
            //HSSFColor lightGray = setColor(workbook, (byte)0xE0, (byte)0xE0, (byte)0xE0);
            //style2.setFillForegroundColor(lightGray.getIndex());
            //headerCellStyle.FillForegroundColor = g;
            //headerCellStyle.FillForegroundColor = HSSFColor.Lim
            //headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            // Create Date Style
            ICellStyle dateCellStyle = workbook.CreateCellStyle();
            dateCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy");

            ICellStyle numCellStyle = workbook.CreateCellStyle();
            numCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.000");


            ICellStyle cellStyle = workbook.CreateCellStyle();
            IDataFormat hssfDataFormat = workbook.CreateDataFormat();
            //String cellVal = "2500";
            //cellStyle.setDataFormat(hssfDataFormat.getFormat("#,##0.000"));
            //numCellStyle.setCellStyle(cellStyle);
            //numCellStyle.setCellValue(new Double(cellVal));
            //numCellStyle.setCellType(Cell.CELL_TYPE_NUMERIC);

            // Build Header
            int i = 0;
            foreach (DataColumn column in sourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.SetCellValue(column.ColumnName);

                if (i == 1 || i == 2)
                {
                    headerCell.CellStyle = numCellStyle;
                }
                else
                {
                    headerCell.CellStyle = headerCellStyle;
                }
                i++;
            }

            // Build Details (rows)
            int rowIndex = 1;
            int sheetIndex = 1;
            const int maxRows = 65536;

            foreach (DataRow row in sourceTable.Rows)
            {
                // Start new sheet max rows reached
                if (rowIndex % maxRows == 0)
                {
                    // Auto size columns on current sheet
                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        sheet.AutoSizeColumn(h);
                    }

                    sheetIndex++;
                    sheet = workbook.CreateSheet("Sheet" + sheetIndex);
                    IRow additionalHeaderRow = sheet.CreateRow(0);

                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                        additionalHeaderColumn.CellStyle = headerRow.GetCell(h).CellStyle;
                        additionalHeaderColumn.SetCellValue(headerRow.GetCell(h).RichStringCellValue);
                    }

                    rowIndex = 1;
                }

                // Create new row in sheet
                IRow dataRow = sheet.CreateRow(rowIndex);

                foreach (DataColumn column in sourceTable.Columns)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);

                    switch (column.DataType.FullName)
                    {
                        case "System.String":
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                        case "System.Int":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            double val;
                            dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                            break;
                        case "System.DateTime":
                            DateTime dt = new DateTime(1900, 01, 01);
                            DateTime.TryParse(row[column].ToString(), out dt);

                            dataCell.SetCellValue(dt);
                            dataCell.CellStyle = dateCellStyle;
                            break;
                        default:
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                    }
                }

                rowIndex++;
            }
            MemoryStream ms = new MemoryStream();
            for (int h = 0; h < headerRow.LastCellNum; h++)
            {
                sheet.AutoSizeColumn(h);
            }
            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;
            return ms;
            //ExportToExcelx(workbook, fileName);
        }

        public GrupoDatos ConsultarAgrupamientoCab(string pcodagrup)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 2, pcodagrup);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_DATOS_AGRUPAMIENTO, out IDataReader reader, lDataParam);
            GrupoDatos oAgrupCab = new GrupoDatos();
            while (reader.Read())
            {

                oAgrupCab.CodigoGrupo = reader.GetString("CODGRUPO");
                oAgrupCab.NombreGrupo = reader.GetString("NOMBGRUPO");
                oAgrupCab.CodJuntaRegante = reader.GetString("CODJREGANT");
                oAgrupCab.CodComisionRegante = reader.GetString("CODCOMIS");
                oAgrupCab.TecnicoTipoDocumento = reader.GetString("ASIST_TIPO_DOC");
                oAgrupCab.TecnicoNroDocumento = reader.GetString("ASIST_NUM_DOC");
                oAgrupCab.TecnicoNombre = reader.GetString("ASIST_NOMBRE");
                oAgrupCab.Banco = reader.GetString("BANCO");
                oAgrupCab.FormaAbono = reader.GetString("FORMABONO");
                oAgrupCab.Destino = reader.GetString("DESTINO");
                oAgrupCab.Tasa = reader.GetString("TASA");
                oAgrupCab.FormaPago = reader.GetString("FORMAPAGO");
                oAgrupCab.Cultivo = reader.GetString("CULTIVO");
                oAgrupCab.SeguroDesgravamen = reader.GetString("SEGDESCOMP");
            }
            return oAgrupCab;
        }

        public int ActualizaIntegrante(string nrosolicitud ,string IdIntegrante,string idgrupo,string fabono,string banco,string cuenta,string correo, string celular)
        {
            var lDataParam = new iDB2Parameter[8];
            int n = 0;
            try
            {                
                lDataParam[0] = _db2DataContext.CreateParameter("VI_NRO_SOLICITUD", 15, nrosolicitud);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_IDINTEGRANTE", 15, IdIntegrante);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_CODGRUPO", 9, idgrupo, 0, iDB2DbType.iDB2Decimal);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_FORMAABONO", 4, fabono);
                lDataParam[4] = _db2DataContext.CreateParameter("VI_BANCO", 3, banco);
                lDataParam[5] = _db2DataContext.CreateParameter("VI_CUENTA", 20, cuenta);                
                lDataParam[6] = _db2DataContext.CreateParameter("VI_CORREO", 30, correo);
                lDataParam[7] = _db2DataContext.CreateParameter("VI_CEL", 15, celular);

                Logger.LogInformation("Inicio: Actualizar Datos Integrante");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_MEG_ACT_DATOS_INTEGRANTE, lDataParam);
                Logger.LogInformation("Fin: Actualizar Datos Integrante");
                ParametricasData oPar = new ParametricasData();
           
               // string codrsp = oPar.ReevaluarIntegrante(idgrupo + ""+ nrosolicitud);
                return n;

            }
            catch (Exception e)
            {
                n = -1;
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public List<GrupoIntegrante> ConsultarIntegranteInfo(decimal Solicitud)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 15, Solicitud, 0, iDB2DbType.iDB2Decimal);            
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_DATOS_INTEGRANTE, out IDataReader reader, lDataParam);
                        
            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();

            GrupoIntegrante oInfoCliente = null;
                        
            Persona Cliente = null;
            Persona Conyugue = null;
            Domain.Models.Grupo.Predio oPredio = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;
            Domain.Models.Grupo.Garantia oGarantia1 = null;
            Domain.Models.Grupo.Garantia oGarantia2 = null;
            DatosEvaluacion EvaluacionCliente = null;
            DatosEvaluacion EvaluacionConyugue = null;

            while (reader.Read())
            {
                oInfoCliente = new GrupoIntegrante();
                oInfoCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPODOCTIT")),
                    NroDocumento = new Cell(reader.GetString("NUMDOCTIT")),
                    ApePaterno = new Cell(reader.GetString("APEPATERNO")),
                    ApeMaterno = new Cell(reader.GetString("APEMATERNO")),
                    NomPrimer = new Cell(reader.GetString("NOMBRE1")),
                    NomSegundo = new Cell(reader.GetString("NOMBRE2")),
                    Sexo = new Cell(reader.GetString("SEXO")),
                    EstadoCivil = new Cell(reader.GetString("ESTCIVIL")),
                    Celular = new Cell(reader.GetString("CELULAR")),
                    Experiencia = new Cell(reader.GetString("ANHOSEXPER")),
                    NacimientoDia = new Cell(reader.GetString("DIANACIM")),
                    NacimientoMes = new Cell(reader.GetString("MESNACIM")),
                    NacimientoAnio = new Cell(reader.GetString("ANH0NACIM")),
                    Correo = new Cell(reader.GetString("CORREO"))
                };                

                Conyugue = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPODOCCON")),
                    NroDocumento = new Cell(reader.GetString("NUMDOCCON"))
                };

                oPredio = new Domain.Models.Grupo.Predio
                {
                    Departamento = new Cell(reader.GetString("DEPARTAMEN")),
                    Provincia = new Cell(reader.GetString("PROVINCIA")),
                    Distrito = new Cell(reader.GetString("DISTRITO")),
                    Referencia = new Cell(reader.GetString("REFERENCIA")),
                    RegimenTenencia = new Cell(reader.GetString("TENENCIA")),
                    UnidadCatastral = new Cell(reader.GetString("UNDCATAST")),
                    EstadoCampo = new Cell(reader.GetString("ESTCAMPO"))
                };

                oSolicitud = new Solicitud
                {
                    TipoUnidadFinanciamiento = new Cell(reader.GetString("UNDFINANC")),
                    NroUnidadesTotales = new Cell(reader.GetString("NRUNDTOTAL")),
                    NroUnidadesFinanciamiento = new Cell(reader.GetString("NRUNDFINAN")),
                    MontoSolicitadoSoles = new Cell(reader.GetString("MNTSOLICIT")),
                    EnvioCronograma = new Cell(reader.GetString("ENVIOEECC")),
                    PagoAdelantado = new Cell(reader.GetString("PAGADELANT")),
                    FormaAbono = reader.GetString("FORMA_ABONO"),
                    Banco = reader.GetString("BANCO"),
                    CCI = reader.GetString("CCI")
                };                

                //oGarantia1 = new Garantia();
                //oGarantia1.TipoGarantia= new Range(reader.GetString("TIPOGARAN1"));
                //oGarantia1.ValorRealizacion = new Range(reader.GetString("VRGARAN1"));
                //oGarantia1.TienePoliza = new Range(reader.GetString("FLGPOLIZA1"));

                //oGarantia2 = new Garantia();
                //oGarantia2.TipoGarantia = new Range(reader.GetString("TIPOGARAN2"));
                //oGarantia2.ValorRealizacion = new Range(reader.GetString("VRGARAN2"));
                //oGarantia2.TienePoliza = new Range(reader.GetString("FLGPOLIZA2"));

                oInfoCliente.DatosCliente = Cliente;
                oInfoCliente.DatosConyugue = Conyugue;
                oInfoCliente.DatosPredio = oPredio;
                oInfoCliente.DatosSolicitud = oSolicitud;
                oInfoCliente.DatosGarantia1 = oGarantia1;
                oInfoCliente.DatosGarantia2 = oGarantia2;
                oInfoCliente.EvaluacionCliente = EvaluacionCliente;
                oInfoCliente.EvaluacionConyugue = EvaluacionConyugue;
                listaClientes.Add(oInfoCliente);
            }

            return listaClientes;
        }

        public List<GrupoIntegrante> ListarIntegrantesPorAgrupamiento(string CodAgrupamiento)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 6, CodAgrupamiento, 0, iDB2DbType.iDB2Integer);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_LISTAR_INTEGRANTES, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.EstadoSolicitud = reader.GetString("ESTADO_SOLICITUD");
                oDataCliente.EstadoEvaluacion = reader.GetString("ESTADO_EVALUACION");
                oDataCliente.EstadoTipoCuota = reader.GetString("ESTADO_TC");
                oDataCliente.EstadoGeoreferencia = reader.GetString("ESTADO_GEO");
                oDataCliente.EstadoInformeVisita = reader.GetString("ESTADO_IV");
                oDataCliente.EstadoFlujoCaja = reader.GetString("ESTADO_FC");
                oDataCliente.EstadoInformeComercial = reader.GetString("ESTADO_IC");
                oDataCliente.EstadoPropuesta = reader.GetString("ESTADO_PRO");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                oDataCliente.FlagExcepcion = reader.GetString("EXCEPCION");
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString())
                };
                /*CAMBIOS AGRO-REQ-023: VISITAS - VINCULACIÓN*/
                /*string strVinculacion = "<div class='btn-group'><a class='btn btn-success btn-xs dropdown-toggle' data-toggle='dropdown'>" +
                    "<i class='fa fa-check'></i></a>" +
                    "<ul class='dropdown-menu dropdown-menu-right'><li>" +
                    "<a data-toggle='modal' data-target='#myModal-VinculacionVisitas' data-sol='" + reader.GetString("NRO_SOLICITUD") + "' data-doc='" + reader.GetString("NUM_DOC") + "'); ' href='#'> Vincular Visita</a></li></ul></div>";
                */

                oDataCliente.InicioVinculacion = reader.GetString("ESTADO_VINVIS");

                /**/


                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;
                listaClientes.Add(oDataCliente);
            }
            return listaClientes;
        }

        public List<GrupoIntegrante> ListarSolicitudesPorAgenciaAnalista(string CodAgencia, string CodAnalista)
        {
            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 4, ApplicationConstants.IdPlaformaAgroperu);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_LISTAR_SOLICITUDES, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.EstadoSolicitud = reader.GetString("ESTADO_SOLICITUD");
                oDataCliente.EstadoEvaluacion = reader.GetString("ESTADO_EVALUACION");
                oDataCliente.EstadoTipoCuota = reader.GetString("ESTADO_TC");
                oDataCliente.EstadoGeoreferencia = reader.GetString("ESTADO_GEO");
                oDataCliente.EstadoInformeVisita = reader.GetString("ESTADO_IV");
                oDataCliente.EstadoFlujoCaja = reader.GetString("ESTADO_FC");
                oDataCliente.EstadoInformeComercial = reader.GetString("ESTADO_IC");
                oDataCliente.EstadoPropuesta = reader.GetString("ESTADO_PRO");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                oDataCliente.FlagExcepcion = reader.GetString("EXCEPCION");

                /*CAMBIOS AGRO-REQ-023: VISITAS - VINCULACIÓN*/
                /*string strVinculacion = "<div class='btn-group'><a class='btn btn-success btn-xs dropdown-toggle' data-toggle='dropdown'>" +
                    "<i class='fa fa-check'></i></a>" +
                    "<ul class='dropdown-menu dropdown-menu-right'><li>" +
                    "<a data-toggle='modal' data-target='#myModal-VinculacionVisitas' data-sol='" + reader.GetString("NRO_SOLICITUD")+"' data-doc='"+reader.GetString("NUM_DOC")+ "'); ' href='#'> Vincular Visita</a></li></ul></div>";                
                */
                oDataCliente.InicioVinculacion = reader.GetString("ESTADO_VINVIS");
                
                /**/

                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString())
                };

                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;                
                listaClientes.Add(oDataCliente);
            }
            return listaClientes;
        }

        public Grupo ConsultarAgrupamientoInfo(string CodAgrupacion)
        {
            var Agrupamiento = new Grupo();
            Agrupamiento.DatosGrupo = ConsultarAgrupamientoCab(CodAgrupacion);
            Agrupamiento.ExcelIntegrantes = ListarIntegrantesPorAgrupamiento(CodAgrupacion);
            return Agrupamiento;
        }

        public List<InfoGrupo> ListarAgrupamientosPorAgenciaAnalista(string CodAgencia, string CodAnalista)
        {

            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 4, ApplicationConstants.IdPlaformaAgroperu);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_LISTAR_AGRUPAMIENTOS, out IDataReader reader, lDataParam);
            List<InfoGrupo> lagrupmodel = new List<InfoGrupo>();
            InfoGrupo oAgrup = null;
            while (reader.Read())
            {
                oAgrup = new InfoGrupo();
                oAgrup.Codigo = reader.GetString("CODIGO");
                oAgrup.Nombre = reader.GetString("DESCRIPCION");
                oAgrup.JuntaRegante = reader.GetString("JREGANT");
                oAgrup.PagoAsistenteTecnico = reader.GetString("PAGO_AT");                
                oAgrup.FechaCreacion = reader.GetString("FECHA_CREACION");
                oAgrup.FechaEvaluacion = reader.GetString("FECHA_EVALUACION");
                oAgrup.HoraCreacion = reader.GetInt("HORA_CREACION");
                oAgrup.TotalIntegrantes = reader.GetInt("INTEGRANTES");
                oAgrup.TotalAprobados = reader.GetInt("APROBADOS");
                oAgrup.TotalEvaluacion = reader.GetInt("EVALUACION");                
                oAgrup.TotalPropuesta = reader.GetInt("PROPUESTA");
                oAgrup.MontoSolicitado = reader.GetDecimal("MONTO_SOLICITADO");
                oAgrup.Estado = reader.GetString("ESTADO");
                oAgrup.TramaOpciones = reader.GetString("OPCIONES");
                lagrupmodel.Add(oAgrup);
            }
            return lagrupmodel;
        }

        public List<GrupoIntegrante> LeerExcelAgrupamiento(ref ExcelResponseModel respon, Stream stream, string fileExtension, out string Sector, out string Programa)
        {
            var Fila = ApplicationKeys.ExcelGrupoAgroperuStartRow;
            try
            {
                List<GrupoIntegrante> LstIntegrantes = new List<GrupoIntegrante>();
                var excelData = new ExcelDataAccess(stream, fileExtension);
                if (excelData.SheetName != ApplicationKeys.ExcelGrupoAgroperuSheetName)
                {
                    respon.code = "10";
                    respon.message = "Plantilla de agrupamiento no válida";
                    throw new System.ArgumentException(respon.message, "codigo plantilla");
                }
                var version = excelData.GetCell(0, 0).value;
                if (version != ApplicationKeys.ExcelGrupoAgroperuVersion)
                {
                    respon.code = "11";
                    respon.message = "Descarge la versión actual de la plantilla";
                    throw new System.ArgumentException(respon.message);
                }

                Sector = excelData.GetCell(2, 1).value;
                Programa = excelData.GetCell(3, 1).value;
                var TipoUnidadesFinanciar = excelData.GetCell(4,1);
                //if (!(TipoUnidadesFinanciar.value == "HAS" && Sector == "1" || TipoUnidadesFinanciar.value == "CG" && Sector == "2"))
                //{
                //    respon.code = "12";
                //    respon.message = "El Sector elegido no coincide con el Tipo de Unidades a Financiar indicado en el Excel.";
                //    throw new System.ArgumentException(respon.message);
                //}

                IRow rw = excelData.GetRow(Fila);
                while (ValidarRegistro(rw))
                {
                    var objCliente = new Persona
                    {
                        TipoDocumento = excelData.GetCell(Fila, 0),
                        NroDocumento = excelData.GetCell(Fila, 1),
                        ApePaterno = excelData.GetCell(Fila, 2),
                        ApeMaterno = excelData.GetCell(Fila, 3),
                        NomPrimer = excelData.GetCell(Fila, 4),
                        NomSegundo = excelData.GetCell(Fila, 5),
                        Direccion = excelData.GetCell(Fila, 6),
                        Sexo = excelData.GetCell(Fila, 7),
                        EstadoCivil = excelData.GetCell(Fila, 8),
                        Celular = excelData.GetCell(Fila, 9),
                        WhatsApp = excelData.GetCell(Fila, 10),
                        Experiencia = excelData.GetCell(Fila, 11),
                        NacimientoDia = excelData.GetCell(Fila, 12),
                        NacimientoMes = excelData.GetCell(Fila, 13),
                        NacimientoAnio = excelData.GetCell(Fila, 14),
                        Correo = excelData.GetCell(Fila, 32)
                    };
                    var objConyugue = new Persona
                    {
                        TipoDocumento = excelData.GetCell(Fila, 15),
                        NroDocumento = excelData.GetCell(Fila, 16),
                        ApePaterno = excelData.GetCell(Fila, 17),
                        ApeMaterno = excelData.GetCell(Fila, 18),
                        NomPrimer = excelData.GetCell(Fila, 19),
                        NomSegundo = excelData.GetCell(Fila, 20)
                    };
                    var objPredio = new Predio
                    {
                        Departamento = excelData.GetCell(Fila, 21),
                        Provincia = excelData.GetCell(Fila, 22),
                        Distrito = excelData.GetCell(Fila, 23),
                        Referencia = excelData.GetCell(Fila, 24),
                        RegimenTenencia = excelData.GetCell(Fila, 25),
                        UnidadCatastral = excelData.GetCell(Fila, 26)
                    };
                    var objSolicitud = new Solicitud
                    {
                        TipoUnidadFinanciamiento = TipoUnidadesFinanciar,
                        NroUnidadesTotales = excelData.GetCell(Fila, 27),
                        NroUnidadesFinanciamiento = excelData.GetCell(Fila, 28),
                        NroUnidadesConduccion = excelData.GetCell(Fila, 29),                        
                        MontoSolicitadoSoles = excelData.GetCell(Fila, 30),
                        EnvioCronograma = excelData.GetCell(Fila, 31),
                        // 32 correo en cliente
                        PagoAdelantado = excelData.GetCell(Fila, 33),
                        NivelTecnologico = excelData.GetCell(Fila, 49) // FINAL
                        
                    };
                    var objGarantia1 = new Domain.Models.Grupo.Garantia
                    {
                        TipoGarantia = excelData.GetCell(Fila, 34),
                        ValorRealizacion = excelData.GetCell(Fila, 35),
                        TienePoliza = excelData.GetCell(Fila, 36)
                    };
                    var objGarantia2 = new Domain.Models.Grupo.Garantia
                    {
                        TipoGarantia = excelData.GetCell(Fila, 37),
                        ValorRealizacion = excelData.GetCell(Fila, 38),
                        TienePoliza = excelData.GetCell(Fila, 39)
                    };
                    var objAval = new Domain.Models.Grupo.Aval
                    {
                        TipoAval = excelData.GetCell(Fila, 40),
                        TipoDocumento = excelData.GetCell(Fila, 41),
                        NroDocumento = excelData.GetCell(Fila, 42),
                        ApePaterno = excelData.GetCell(Fila, 43),
                        ApeMaterno = excelData.GetCell(Fila, 44),
                        PrimerNombre = excelData.GetCell(Fila, 45),
                        SegundoNombre = excelData.GetCell(Fila, 46)                        
                    };
                    var objInformeVisita = new Domain.Models.Grupo.InformeVisita
                    {
                        Plazo = excelData.GetCell(Fila, 47),
                        FechaSiembra = cambiarFecha(excelData.GetCell(Fila, 48)),
                        FechaSiembraOriginal = excelData.GetCell(Fila,48)
                        // 49 NIVEL TECNOLOGICO - EN SOLICITUD
                    };

                    LstIntegrantes.Add(new GrupoIntegrante
                    {
                        DatosCliente = objCliente,
                        DatosConyugue = objConyugue,
                        DatosPredio = objPredio,
                        DatosSolicitud = objSolicitud,
                        DatosGarantia1 = objGarantia1,
                        DatosGarantia2 = objGarantia2,
                        DatosAval1 = objAval,
                        DatosInformeVisita = objInformeVisita
                    });
                    Fila = Fila + 1;
                    rw = excelData.GetRow(Fila);
                }

                return LstIntegrantes;
            }
            catch (Exception e)
            {
                if (respon.code == "")
                {
                    respon.code = "10";
                    respon.message = e.Message;
                }
                Logger.LogException(e, $"Leer excel FILA: {Fila}");
                throw;
            }

        }

        public Cell cambiarFecha(Cell dato)
        {

            if (dato.value != "")
            {
                if (dato.value.ToString().Contains("/"))
                {
                    dato.value = dato.value.Substring(6, 4) + dato.value.Substring(3, 2) + dato.value.Substring(0, 2);
                }

            }
            return dato;
        }

        private bool ValidarRegistro(IRow rw)
        {
            if (rw is null)
                return false;

            foreach (var item in rw.Cells)
                if (item.GetCellValue().Trim() != "")
                    return true;

            return false;
        }

        public void RegistrarTipoCronograma(string NumeroSolicitud, string TipoCuota, 
            int NumeroCuotas, int FrecuenciaPago, string UnidadFrecuencia, int AnioGracia, int MesGracia, int DiaGracia,
            string PorcentajesCuotas, string FechasCuotas, string Usuario)
        {
            var lDataParam = new iDB2Parameter[11];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_TIPO_CUOTA", 1, TipoCuota);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_CUOTAS", 3, NumeroCuotas, 0, iDB2DbType.iDB2Decimal);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_FRECUENCIA_PAGO", 5, FrecuenciaPago, 0, iDB2DbType.iDB2Decimal);
                lDataParam[4] = _db2DataContext.CreateParameter("VI_UNIDAD_FRECUENCIA", 1, UnidadFrecuencia);
                lDataParam[5] = _db2DataContext.CreateParameter("VI_GRACIA_ANIO", 4, AnioGracia, 0, iDB2DbType.iDB2Decimal);
                lDataParam[6] = _db2DataContext.CreateParameter("VI_GRACIA_MES", 2, MesGracia, 0, iDB2DbType.iDB2Decimal);
                lDataParam[7] = _db2DataContext.CreateParameter("VI_GRACIA_DIA", 2, DiaGracia, 0, iDB2DbType.iDB2Decimal);                
                lDataParam[8] = _db2DataContext.CreateParameter("VI_CUOTA_PORCENTAJE", 500, PorcentajesCuotas);
                lDataParam[9] = _db2DataContext.CreateParameter("VI_CUOTA_FECHAS", 500, FechasCuotas);
                lDataParam[10] = _db2DataContext.CreateParameter("VI_USER", 10, Usuario);

                Logger.LogInformation("Inicio: Registro de Tipo Cronograma");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_INS_REGISTRA_TIPO_CRONOGRAMA, lDataParam);
                Logger.LogInformation("Fin: Registro de Tipo Cronograma");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public ParametricasModel ValidarPeriodoGracia(DateTime FechaGracia)
        {
            var parametricas = new ParametricasModel();
            var lDataParam = new iDB2Parameter[1];

            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_CODIGO", 12, FechaGracia, 0, iDB2DbType.iDB2Date);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_VALIDA_PERIODO_GRACIA, out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    parametricas.key = reader.GetString("FERIADO");
                    parametricas.value = reader.GetString("DIFERENCIA_DIAS");

                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }

            
            return parametricas;
        }

        public void ActualizaFormaPagoIntegrante(string CodAgrupamiento, string NumeroDocumento, string FormaAbono, string Banco, string NumeroCuenta)
        {
            var lDataParam = new iDB2Parameter[5];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_DOC_INTEGRANTE", 15, NumeroDocumento);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_CODGRUPO", 9, CodAgrupamiento, 0, iDB2DbType.iDB2Decimal);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_CODGRUPO", 4, FormaAbono);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_BANCO", 3, Banco);
                lDataParam[4] = _db2DataContext.CreateParameter("VI_CUENTA", 20, NumeroCuenta);

                Logger.LogInformation("Inicio: Registro de Banco de Abono");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_MEG_ACT_FORMA_PAGO_INTEGRANTE, lDataParam);
                Logger.LogInformation("Fin: Registro de Banco de Abono");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public int CargarDocumentoLaserFiche(Stream stream, string fileExtension, string tipo, string NumeroSolicitud, string NumeroDocumento, out string fileName)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            var ldDocumentId = 0;
            var metadata = new Dictionary<string, object>
            {
                {"NumeroSolicitud", NumeroSolicitud},
                {"DocumentoCliente", NumeroDocumento}
            };
            stream.Position = 0;
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            ms.Position = 0;
            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}\{NumeroSolicitud}";

            try
            {
                lfhelper.GetOrCreateFolderInfo(destinPath);
                var documentPath = $@"{destinPath}\IV_{NumeroDocumento}.{fileExtension}";
                switch (tipo)
                {
                    case "1": documentPath = $@"{destinPath}\IV_{NumeroDocumento}.{fileExtension}"; break;
                    case "2": documentPath = $@"{destinPath}\FC_{NumeroDocumento}.{fileExtension}"; break;
                    case "11": documentPath = $@"{destinPath}\IC_{NumeroDocumento}.{fileExtension}"; break;
                    default: documentPath = $@"{destinPath}\OTRO_{NumeroDocumento}.{fileExtension}"; break;
                }

                ldDocumentId = lfhelper.RegisterFileWithMetaData(ms, metadata, documentPath, "Default", ApplicationKeys.LaserFicheTemplateName, out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

            }
            catch (Exception exc)
            {
                Logger.LogException(exc, "Cargar Documento LaserFiche");
                throw;
            }
            finally
            {

                lfhelper.Dispose();
                lfhelper = null;
            }

            
            return ldDocumentId;
        }

        public void RegistrarDocumentoLaserFiche(int Id, string NroSolicitud, string tipo, string fileName, string usuario, string observacion = "")
        {
            var lDataParam = new iDB2Parameter[7];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 4, ApplicationConstants.IdPlaformaAgroperu);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_IDLF", 9, Id, 0, iDB2DbType.iDB2Numeric);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, NroSolicitud, 0, iDB2DbType.iDB2Decimal);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_TIPO", 2, tipo);
                lDataParam[4] = _db2DataContext.CreateParameter("VI_NOMBRE", 40, fileName);
                lDataParam[5] = _db2DataContext.CreateParameter("VI_OBSERVACION", 80, observacion);
                lDataParam[6] = _db2DataContext.CreateParameter("VI_USER", 10, usuario);

                Logger.LogInformation("Inicio: Registro de documento LaserFiche");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_MEG_INS_REGFILES_LASERFICHE, lDataParam);
                Logger.LogInformation("Fin: Registro de documento LaserFiche");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public (MemoryStream stream, string fileName) DescagarDocumentoLaserFiche(int IdDocument, out string extension)
        {
            var lfHelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);

            var fileName = "";            
            MemoryStream stream = null;
            try
            {
                stream = lfHelper.GetDocument(IdDocument, out fileName, out extension);                
            }
            catch (Exception exc)
            {
                Logger.LogException(exc, "Descar Documento en grupo");
                throw;
            }
            finally
            {
                lfHelper.Dispose();
                lfHelper = null;
            }

            
            return (stream, fileName);
        }

        public void RegistrarExcepcion(string NumeroSolicitud, string Motivo, string Comentario, int IdLaserFiche, string usuario)
        {
            var lDataParam = new iDB2Parameter[5];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_MOTIVO", 4, Motivo);                
                lDataParam[2] = _db2DataContext.CreateParameter("VI_COMENTARIO", 210, Comentario);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_ID_LASERFICHE", 9, IdLaserFiche, 0, iDB2DbType.iDB2Integer);
                lDataParam[4] = _db2DataContext.CreateParameter("VI_USER", 10, usuario);

                Logger.LogInformation("Inicio: Registro de aprobacion por excepcion");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_INS_REG_APROB_EXCEPCION, lDataParam);
                Logger.LogInformation("Fin: Registro de aprobacion por excepcion");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public void ActualizarPagoAsistenteTecnico(decimal CodigoGrupo, int PagoAsistenteTecnico, string Usuario)
        {
            var lDataParam = new iDB2Parameter[3];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_GRUPO", 12, CodigoGrupo, 0, iDB2DbType.iDB2Decimal);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_PAGO", 4, PagoAsistenteTecnico, 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_USER", 10, Usuario);

                Logger.LogInformation("Inicio: Actualiza Pago Asistente Tecnico");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_ACT_ACTUALIZA_PAGO_AT, lDataParam);
                Logger.LogInformation("Fin: Actualiza Pago Asistente Tecnico");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public void AnularAgrupamiento(int CodigoGrupo)
        {
            var lDataParam = new iDB2Parameter[1];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 7, CodigoGrupo, 0, iDB2DbType.iDB2Integer);
                
                Logger.LogInformation("Inicio: Anulacion de Agrupamiento");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_MEG_ACT_ANULAR_AGRUPAMIENTO, lDataParam);
                Logger.LogInformation("Fin: Anulacion de Agrupamiento");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public void AnularSolicitud(decimal NumeroSolicitud)
        {
            var lDataParam = new iDB2Parameter[1];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);

                Logger.LogInformation("Inicio: Anulacion de Solicitud");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_ACT_ANULAR_SOLICITUD, lDataParam);
                Logger.LogInformation("Fin: Anulacion de Solicitud");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        #region SeguimientoSolicitudes

        public List<GrupoIntegrante> ListarSolicitudesEnRevision(string CodAgencia, string CodAnalista)
        {
            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 4, ApplicationConstants.IdPlaformaAgroperu);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_SOLICITUDES_REVISION, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                oDataCliente.Trama1 = reader.GetString("SUSTENTO");
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString()),
                    Cultivo = new Cell(reader.GetString("PRODUCTO"))
                };

                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;
                listaClientes.Add(oDataCliente);
            }
            return listaClientes;
        }

        public List<GrupoIntegrante> ListarSolicitudes(string CodigoAgencia, string CodigoAnalista)
        {
            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodigoAnalista);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodigoAgencia);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 4, ApplicationConstants.IdPlaformaAgroperu);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_SOLICITUDES_XFUNCIONARIOS, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.EstadoSolicitud = reader.GetString("ESTADO_SOLICITUD");
                oDataCliente.EstadoEvaluacion = reader.GetString("ESTADO_EVALUACION");
                oDataCliente.EstadoTipoCuota = reader.GetString("ESTADO_TC");
                oDataCliente.EstadoGeoreferencia = reader.GetString("ESTADO_GEO");
                oDataCliente.EstadoInformeVisita = reader.GetString("ESTADO_IV");
                oDataCliente.EstadoFlujoCaja = reader.GetString("ESTADO_FC");
                oDataCliente.EstadoInformeComercial = reader.GetString("ESTADO_IC");
                oDataCliente.EstadoPropuesta = reader.GetString("ESTADO_PRO");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString())
                };

                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;
                listaClientes.Add(oDataCliente);
            }
            return listaClientes;
        }

        public void AprobarPorExcepcion(string NumeroSolicitud, string Observacion, string Respuesta, string usuario)
        {
            var lDataParam = new iDB2Parameter[4];
            try
            {
                lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_OBSERVACION", 210, Observacion);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_RESPUESTA", 1, Respuesta);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_USER", 10, usuario);

                Logger.LogInformation("Inicio: Aprobar Excepcion");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_ACT_APROBAR_EXCEPCION, lDataParam);
                Logger.LogInformation("Fin: Aprobar excepcion");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }
        #endregion

    }
}

﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class ApiGrupoData
    {
        private readonly Db2DataAccess _db2DataContext;
        public ApiGrupoData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        //public void RegistrarInfoSentinel(int codmodulo, string data_sentinel, string codigo)
        //{
        //    try
        //    {
        //        Logger.LogInformation(codigo+" Inicio: RegistrarInfoSentinel ::codmodulo=" +codmodulo+"codigo="+codigo);
        //        var parameters = new iDB2Parameter[1];
        //        parameters[0] = _db2DataContext.CreateParameter("VI_DATA", 1000000, data_sentinel, 0, iDB2DbType.iDB2Clob);
        //        _db2DataContext.RunStoredProcedure(StoreConstants.Servicio.UP_MEG_INS_DATA_SENTINEL, parameters, 100);
        //        ParametricasData oPar = new ParametricasData();
        //        Logger.LogInformation(codigo + " Fin: RegistrarInfoSentinel::UP_MEG_INS_DATA_SENTINEL");
        //        Logger.LogInformation(codigo + " Inicio: RegistrarInfoSentinel::EvaluarResultadoSentinel");
        //        string codrsp = oPar.EvaluarResultadoSentinel(codmodulo, codigo.PadLeft(12, '0'));
        //        Logger.LogInformation(codigo+" Fin: RegistrarInfoSentinel::EvaluarResultadoSentinel"+ codrsp);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LogException(ex, "RegistrarInfoSentinel::"+ex.InnerException);
        //        //respon.code = "10";
        //        //respon.message = "SP:RegistrarAgrupamiento:" + ex.Message;
        //    }
        //}

        public List<IntegranteSentinel> ObtenerIntegrantePadronSentinel(int CodModulo, int CodGrupo)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 15, CodGrupo, 0, iDB2DbType.iDB2Integer);
            Logger.LogInformation("Inicio: Consulta Lista de Padron");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Servicio.UP_MEG_SEL_DATA_PADRON_SENTINEL, out IDataReader reader, lDataParam);
            IntegranteSentinel oIntegrante = null;
            List<IntegranteSentinel> lista = new List<IntegranteSentinel>();
            while (reader.Read())
            {
                oIntegrante = new IntegranteSentinel();
                oIntegrante.tipo_documento = reader.GetString("MPITID");
                oIntegrante.nro_documento = reader.GetString("MPIIDN");
                lista.Add(oIntegrante);
            }
            Logger.LogInformation("Fin: Consulta Padron");
            return lista;
        }
        public List<IntegranteSentinel> ObtenerIntegranteGrupoSentinel(int CodModulo, int CodGrupo)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 15, CodGrupo, 0, iDB2DbType.iDB2Integer);
            Logger.LogInformation("Inicio: Consulta Lista de Grupo");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Servicio.UP_MEG_SEL_DATA_GRUPO_SENTINEL, out IDataReader reader, lDataParam);
            IntegranteSentinel oIntegrante = null;
            List<IntegranteSentinel> lista = new List<IntegranteSentinel>();
            while (reader.Read())
            {
                oIntegrante = new IntegranteSentinel();
                oIntegrante.tipo_documento = reader.GetString("MPITID");
                oIntegrante.nro_documento = reader.GetString("MPIIDN");
                lista.Add(oIntegrante);
            }
            Logger.LogInformation("Fin: Consulta Grupo");
            return lista;
        }
    }

}

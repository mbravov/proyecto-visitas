﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class ConsultasData
    {
        private readonly Db2DataAccess _db2DataContext;
        public ConsultasData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }
        public List<GrupoIntegrante> ListarSolicitudesDetallado(string CodAgrupamiento)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODGRUPO", 6, CodAgrupamiento, 0, iDB2DbType.iDB2Integer);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_CONSULTA_SOLICITUDES_XGRUPO, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.EstadoSolicitud = reader.GetString("ESTADO_SOLICITUD");
                oDataCliente.EstadoEvaluacion = reader.GetString("ESTADO_EVALUACION");
                oDataCliente.EstadoTipoCuota = reader.GetString("ESTADO_TC");
                oDataCliente.EstadoGeoreferencia = reader.GetString("ESTADO_GEO");
                oDataCliente.EstadoInformeVisita = reader.GetString("ESTADO_IV");
                oDataCliente.EstadoFlujoCaja = reader.GetString("ESTADO_FC");
                oDataCliente.EstadoInformeComercial = reader.GetString("ESTADO_IC");
                oDataCliente.EstadoPropuesta = reader.GetString("ESTADO_PRO");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString())
                };

                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;
                listaClientes.Add(oDataCliente);
            }
            return listaClientes;
        }
        public List<ResultadoGrupo> ListarResultado(string CodAgencia, string CodAnalista, int CodigoGrupo)
        {
            var lDataParam = new IBM.Data.DB2.iSeries.iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista, 0, iDB2DbType.iDB2VarChar);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia, 0, iDB2DbType.iDB2VarChar);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_CODGRUPO", 12, CodigoGrupo, 0, iDB2DbType.iDB2Integer);

            Logger.LogInformation("Inicio: Consultando Resultado");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_CONSULTA_ERRORES_AGRUPAMIENTO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Resultado");
            List<ResultadoGrupo> LstErrores = new List<ResultadoGrupo>();
            ResultadoGrupo oResultado = null;
            //Opcion opcion = null;
            //var CodMenu = 0;
            while (reader.Read())
            {
                oResultado = new ResultadoGrupo();
                oResultado.TipoDocumento = reader.GetString("MFITID");
                oResultado.NroDocumento = reader.GetString("MFIIDN");
                oResultado.NombreIntegrante = reader.GetString("MFINOC");
                oResultado.Solicitud = reader.GetString("CADSOL");
                oResultado.Codigo = reader.GetString("CADCOD");
                oResultado.Mensaje = reader.GetString("MOTDES");
                LstErrores.Add(oResultado);
            }
            return LstErrores;
        }


        public List<ResultadoGrupo> ListarResultadoPadron(string CodAgencia, string CodigoGrupo)
        {
            var lDataParam = new IBM.Data.DB2.iSeries.iDB2Parameter[2];
           // lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista, 0, iDB2DbType.iDB2VarChar);
            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia, 0, iDB2DbType.iDB2VarChar);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_CODGRUPO", 12, int.Parse(CodigoGrupo), 0, iDB2DbType.iDB2Integer);

            Logger.LogInformation("Inicio: Consultando Resultado");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Padron.UP_MEG_SEL_CONSULTA_ERRORES_PADRON, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Resultado");
            List<ResultadoGrupo> LstErrores = new List<ResultadoGrupo>();
            ResultadoGrupo oResultado = null;
            //Opcion opcion = null;
            //var CodMenu = 0;
            while (reader.Read())
            {
                oResultado = new ResultadoGrupo();
                oResultado.TipoDocumento = reader.GetString("MPITID");
                oResultado.NroDocumento = reader.GetString("MPIIDN");
                oResultado.NombreIntegrante = reader.GetString("MPINOC");
                oResultado.Solicitud = reader.GetString("CADSOL");
                oResultado.Codigo = reader.GetString("CADCOD");
                oResultado.Mensaje = reader.GetString("MOTDES");
                LstErrores.Add(oResultado);
            }
            return LstErrores;
        }

        public List<GrupoIntegrante> ListarSolicitudes(string CodAgencia, string CodAnalista)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodAnalista);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia);
            _db2DataContext.RunStoredProcedure(StoreConstants.Consultas.UP_MEG_SEL_SOLICITUDESX_AGENCIA_USUARIO, out IDataReader reader, lDataParam);

            List<GrupoIntegrante> listaClientes = new List<GrupoIntegrante>();
            GrupoIntegrante oDataCliente = null;

            Persona Cliente = null;
            Domain.Models.Grupo.Solicitud oSolicitud = null;
            Predio oPredio = null;

            while (reader.Read())
            {
                oDataCliente = new GrupoIntegrante();
                oDataCliente.NumeroSolicitud = reader.GetString("NRO_SOLICITUD");
                oDataCliente.EstadoSolicitud = reader.GetString("ESTADO_SOLICITUD");
                oDataCliente.EstadoEvaluacion = reader.GetString("ESTADO_EVALUACION");
                oDataCliente.TramaOpciones = reader.GetString("OPCIONES");
                
                Cliente = new Persona
                {
                    TipoDocumento = new Cell(reader.GetString("TIPO_DOC")),
                    NroDocumento = new Cell(reader.GetString("NUM_DOC")),
                    NomPrimer = new Cell(reader.GetString("NOMBRES")),
                    Celular = new Cell(reader.GetString("CELULAR"))
                };
                oSolicitud = new Solicitud
                {
                    Moneda = reader.GetString("MONEDA"),
                    MontoSolicitadoSoles = new Cell(reader.GetDecimal("MONTO").ToString())
                };
                oPredio = new Predio
                {
                    Cultivo = reader.GetString("CULTIVO"),
                    Latitud = reader.GetString("LATITUD"),
                    Longitud = reader.GetString("LONGITUD")
                };

                oDataCliente.DatosCliente = Cliente;
                oDataCliente.DatosSolicitud = oSolicitud;
                oDataCliente.DatosPredio = oPredio;
                listaClientes.Add(oDataCliente);

            }
            return listaClientes;
        }
    

    }
}

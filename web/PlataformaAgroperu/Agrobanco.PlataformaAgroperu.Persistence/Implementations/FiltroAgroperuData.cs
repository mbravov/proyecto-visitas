﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Domain.Models;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;


namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class FiltroAgroperuData
    {
        private readonly Db2DataAccess _db2DataContext;
        public FiltroAgroperuData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        public List<ExcelFiltroAgroperu> LeerExcelFiltroAgroperu(ref ExcelResponseModel responseModel, Stream stream, string fileExtension)
        {
            var Registros = new List<ExcelFiltroAgroperu>();            
            try
            {
                var excelData = new ExcelDataAccess(stream, fileExtension);
                var version = excelData.GetCellValue<string>(0, 0);
                if (version != ApplicationKeys.VersionFiltroAgroperu)
                {
                    responseModel.code = "10";
                    responseModel.message = "Plantilla de Prospecto no válida";
                    throw new System.ArgumentException(responseModel.message, "codigo plantilla");
                }

                var rowIndex = 1;
                IRow row = excelData.GetRow(rowIndex);

                while (ValidarRegistro(row))
                {
                    var registro = new ExcelFiltroAgroperu(rowIndex);

                    registro.TipoDocumento = excelData.GetCell(rowIndex, registro.TipoDocumento.column);
                    registro.NumeroDocumento = excelData.GetCell(rowIndex, registro.NumeroDocumento.column);
                    registro.ApellidoPaterno = excelData.GetCell(rowIndex, registro.ApellidoPaterno.column);
                    registro.ApellidoMaterno = excelData.GetCell(rowIndex, registro.ApellidoMaterno.column);
                    registro.PrimerNombre = excelData.GetCell(rowIndex, registro.PrimerNombre.column);
                    registro.SegundoNombre = excelData.GetCell(rowIndex, registro.SegundoNombre.column);
                    Registros.Add(registro);

                    rowIndex = rowIndex+1;
                    row = excelData.GetRow(rowIndex);
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw;
            }
            return Registros;
        }

        public void RegistrarFiltroAgroperu(ref ExcelResponseModel responseModel, FiltroAgroperuModel filtroAgroperu, string FechaCliente, string Usuario)
        {

            try
            {
                var parameters = new iDB2Parameter[11];

                ParametricasData parametricas = new ParametricasData();
                var CodigoAs400 = GenerarCodigoFiltro();

                //CodigoAs400 = CodigoAs400.CodigoFiltro;

                parameters[0] = _db2DataContext.CreateParameter("VI_TIMCLI", 50, FechaCliente, 0, iDB2DbType.iDB2VarChar);
                parameters[1] = _db2DataContext.CreateParameter("VI_CODAS400", 12, CodigoAs400.CodigoFiltro, 0, iDB2DbType.iDB2VarChar);
                parameters[2] = _db2DataContext.CreateParameter("VI_NROFIL", 1, filtroAgroperu.Excel.Count, 0, iDB2DbType.iDB2Integer);
                parameters[3] = _db2DataContext.CreateParameter("VI_CODAGENTE", 4, filtroAgroperu.CodigoFuncionario);
                parameters[4] = _db2DataContext.CreateParameter("VI_NOMAGENTE", 35, filtroAgroperu.NombreFuncionario);
                parameters[5] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 1, filtroAgroperu.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                parameters[6] = _db2DataContext.CreateParameter("VI_USUARIO", 10, Usuario, 0, iDB2DbType.iDB2VarChar);
                parameters[7] = _db2DataContext.CreateParameter("VI_CAB_AGR", 20000, filtroAgroperu.Descripcion, 0, iDB2DbType.iDB2VarChar);
                parameters[8] = _db2DataContext.CreateParameter("VI_DET_AGR", 1000000, filtroAgroperu.ObtenerTramaDatos(), 0, iDB2DbType.iDB2Clob);
                parameters[9] = _db2DataContext.CreateParameter("VI_DESCRIPCION", 60, filtroAgroperu.Descripcion);
                parameters[10] = _db2DataContext.CreateParameter("VO_MSG", 150, "", 0, iDB2DbType.iDB2VarChar, ParameterDirection.Output);
                parameters[10].Direction = System.Data.ParameterDirection.Output;

                Logger.LogInformation("Registrando Filtro Agruperu");
                Logger.LogDbParameters(parameters);
                _db2DataContext.RunStoredProcedure(StoreConstants.FiltroAgroperu.UP_MEG_INS_EXCEL_FILTROAGROPERU, parameters, 100);
                Logger.LogInformation("Fin Registrando Agrupamiento");

                var resp = parametricas.EjecutarProgramaT20000(As400Constants.Transaccion.EvaluaFiltroAgroperu, As400Constants.AplicacionFC, CodigoAs400.CodigoFiltro);

                string msg = parameters[10].Value.ToString();
                responseModel = new ExcelResponseModel(msg);
            }
            catch (Exception ex)
            {
                responseModel.code = "10";
                responseModel.message = "SP:RegistrarAgrupamiento:" + ex.Message;
            }
        }

        public (string IdResult, string CodigoFiltro) GenerarCodigoFiltro()
        {
            ParametricasData oPar = new ParametricasData();
            var Result = oPar.EjecutarProgramaT20000(
                As400Constants.Transaccion.GeneraCodigo,
                As400Constants.AplicacionJR,
                As400Constants.Claves.CodigoPadron);

            return (Result.Substring(0, 4), Result.Substring(4, 12));
        }

        private bool ValidarRegistro(IRow rw)
        {
            if (rw is null)
                return false;

            foreach (var item in rw.Cells)            
                if (item.GetCellValue().Trim() != "")                
                    return true;

            return false;
        }

        public List<FiltroAgroperuModel> ConsultarFiltrosAgroperu(int CodAgencia)
        {
            var lstFiltro = new List<FiltroAgroperuModel>();

            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 8, CodAgencia, 0, iDB2DbType.iDB2Integer);
            
            Logger.LogInformation("Inicio: Consulta Lista de Filtros Agroperu");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.FiltroAgroperu.UP_MEG_SEL_LISTAR_FILTROSAGROPERU, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Lista de Filtros Agroperu");

            FiltroAgroperuModel filtro = null;
            while (reader.Read())
            {
                filtro = new FiltroAgroperuModel();
                filtro.CodigoFiltro = reader.GetDecimal("CODIGO");
                filtro.Descripcion = reader.GetString("DESCRIPCION");
                filtro.Registros = reader.GetInt("REGISTROS");
                filtro.Fecha = reader.GetString("FECHA");
                filtro.Usuario = reader.GetString("USUARIO");
                filtro.TramaOpciones = reader.GetString("OPC");

                lstFiltro.Add(filtro);
            }
            return lstFiltro;
        }

        public MemoryStream DescargarResultadosFiltroAgroperu(int CodigoFiltro)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODFILTRO", 1, CodigoFiltro, 0, iDB2DbType.iDB2Integer);
            DataSet dt = new DataSet();

            _db2DataContext.RunStoredProcedure(StoreConstants.FiltroAgroperu.UP_MEG_SEL_RESULTADOS_FILTROAGROPERU, out DataTable table1, lDataParam);
            dt.Tables.Add(table1);            
            return ExportToExcel(dt.Tables[0]);
        }
        public MemoryStream ExportToExcel(DataTable sourceTable)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Sheet1");
            IRow headerRow = sheet.CreateRow(0);


            // Create Header Style
            ICellStyle headerCellStyle = workbook.CreateCellStyle();

            var font = workbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.FontName = "Calibri";
            //font.Color = IndexedColors.DarkGreen.Index;

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            headerCellStyle.SetFont(font);
            headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Blue.Index;
            //headerCellStyle.FillPattern = FillPattern.BigSpots;
            //headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Pink.Index;
            //headerCellStyle.FillBackgroundColor= IndexedColors.Orange.Index;
            //headerCellStyle.FillForegroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillPattern = FillPattern.SolidForeground;
            //headerCellStyle.FillBackgroundColorColor= IndexedColors.RoyalBlue.Index;
            // headerCellStyle.FillBackgroundColor;
            //HSSFColor lightGray = setColor(workbook, (byte)0xE0, (byte)0xE0, (byte)0xE0);
            //style2.setFillForegroundColor(lightGray.getIndex());
            //headerCellStyle.FillForegroundColor = g;
            //headerCellStyle.FillForegroundColor = HSSFColor.Lim
            //headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            // Create Date Style
            ICellStyle dateCellStyle = workbook.CreateCellStyle();
            dateCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy");

            ICellStyle numCellStyle = workbook.CreateCellStyle();
            numCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.000");


            ICellStyle cellStyle = workbook.CreateCellStyle();
            IDataFormat hssfDataFormat = workbook.CreateDataFormat();
            //String cellVal = "2500";
            //cellStyle.setDataFormat(hssfDataFormat.getFormat("#,##0.000"));
            //numCellStyle.setCellStyle(cellStyle);
            //numCellStyle.setCellValue(new Double(cellVal));
            //numCellStyle.setCellType(Cell.CELL_TYPE_NUMERIC);

            // Build Header
            int i = 0;
            foreach (DataColumn column in sourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.SetCellValue(column.ColumnName);

                if (i == 1 || i == 2)
                {
                    headerCell.CellStyle = numCellStyle;
                }
                else
                {
                    headerCell.CellStyle = headerCellStyle;
                }
                i++;
            }

            // Build Details (rows)
            int rowIndex = 1;
            int sheetIndex = 1;
            const int maxRows = 65536;

            foreach (DataRow row in sourceTable.Rows)
            {
                // Start new sheet max rows reached
                if (rowIndex % maxRows == 0)
                {
                    // Auto size columns on current sheet
                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        sheet.AutoSizeColumn(h);
                    }

                    sheetIndex++;
                    sheet = workbook.CreateSheet("Sheet" + sheetIndex);
                    IRow additionalHeaderRow = sheet.CreateRow(0);

                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                        additionalHeaderColumn.CellStyle = headerRow.GetCell(h).CellStyle;
                        additionalHeaderColumn.SetCellValue(headerRow.GetCell(h).RichStringCellValue);
                    }

                    rowIndex = 1;
                }

                // Create new row in sheet
                IRow dataRow = sheet.CreateRow(rowIndex);

                foreach (DataColumn column in sourceTable.Columns)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);

                    switch (column.DataType.FullName)
                    {
                        case "System.String":
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                        case "System.Int":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            double val;
                            dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                            break;
                        case "System.DateTime":
                            DateTime dt = new DateTime(1900, 01, 01);
                            DateTime.TryParse(row[column].ToString(), out dt);

                            dataCell.SetCellValue(dt);
                            dataCell.CellStyle = dateCellStyle;
                            break;
                        default:
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                    }
                }

                rowIndex++;
            }
            MemoryStream ms = new MemoryStream();
            for (int h = 0; h < headerRow.LastCellNum; h++)
            {
                sheet.AutoSizeColumn(h);
            }
            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;
            return ms;
            //ExportToExcelx(workbook, fileName);
        }

    }
}

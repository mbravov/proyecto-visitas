﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Georeferenciacion;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using Agrobanco.PlataformaAgroperu.Persistence.ContentManager;
using IBM.Data.DB2.iSeries;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class GeoreferenciacionData
    {
        private readonly Db2DataAccess _db2DataContext;
        //private readonly MsSqlDataAccess _SqlDataAccess;

        public GeoreferenciacionData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            //_SqlDataAccess = new MsSqlDataAccess();            
        }

        public void Rollback()
        {
            _db2DataContext.RollbackTransaction();
        }

        // trae fotos LaserFiche
        public List<FotoModel> GetFotosPorDocumentoLF(string NumeroDocumento, string NumeroSolicitud)
        {
            List<FotoModel> lstFotos = null;
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = new iDB2Parameter("NumDocumento", NumeroDocumento);
            lDataParam[1] = new iDB2Parameter("NroSolicitud", NumeroSolicitud);
            Logger.LogInformation("Inicio: leyendo datos fotos por documento");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_MEG_SEL_IMAGENESX_DOCUMENTO_SOLICITUD, out IDataReader reader, lDataParam);
            lstFotos = new List<FotoModel>();
            while (reader.Read())
            {
                lstFotos.Add(new FotoModel
                {
                    CtaCod = reader.GetString("NumDoc"),
                    ImgCod = reader.GetString("ImgCod"),
                    //NomCli = reader.GetString("NomCli"),
                    NroSolicitud = reader.GetString("NroSolicitud"),
                    FechaRegistro = reader.GetString("FecReg"),
                    Latitud = reader.GetString("LATITUD"),
                    Longitud = reader.GetString("LONGITUD"),
                    Observacion = reader.GetString("OBS"),
                    Estado = reader.GetString("EST"),
                    UltActualizacion = reader.GetString("UltAct"),
                    FechaFoto = reader.GetString("FechaFoto"),
                    FechaVinculacion = reader.GetString("FecVinculacion"),
                    IdLaserFiche = reader.GetIntOrNull("LFID").GetValueOrDefault(0)
                });
            }
            reader.Close();
            Logger.LogInformation("Fin: leyendo datos fotos por documento");

            Logger.LogInformation("Inicio: leyendo fotos LaserFicheRepository");
            string base64String = string.Empty;
            foreach (FotoModel foto in lstFotos)
            {   
                var img = DescagarImgLaserFiche(foto.IdLaserFiche);
                if (img.stream != null)
                {
                    byte[] imageBytes = img.stream?.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    img.stream.Close();
                    foto.imgBase64 = base64String;
                    foto.ImgCod = img.fileName;
                }
                else
                {
                    lstFotos = null;
                    break;
                }
            }
            Logger.LogInformation("Fin: leyendo fotos LaserFicheRepository");
            return lstFotos;
        }

        private (MemoryStream stream, string fileName) DescagarImgLaserFiche(int IdImagen)
        {
            var lfHelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);

            MemoryStream stream = null;
            var fileName = "";

            try
            {
                stream = lfHelper.GetImage(IdImagen, out fileName);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "DescargarImgLaserFiche");
                throw;
            }
            finally
            {
                lfHelper.Dispose();
                lfHelper = null;
            }

            
            return (stream, fileName);
        }

        public List<FotoModel> GetFotosCercanas(string Latitud, string Longitud)
        {
            List<FotoModel> lstClientes = null;
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = new iDB2Parameter("VI_LATITUD", Latitud);
            lDataParam[1] = new iDB2Parameter("VI_LONGITUD", Longitud);
            Logger.LogInformation("Inicio: leyendo datos Clientes con predios cercanos");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_MEG_SEL_FOTOS_CERCANAS, out IDataReader reader, lDataParam);

            lstClientes = new List<FotoModel>();
            while (reader.Read())
            {
                lstClientes.Add(new FotoModel
                {
                    Credito = reader["Credito"].ToString(),
                    NroDocumento = reader["Documento"].ToString(),
                    NombreCliente = reader["NombreCliente"].ToString(),
                    Distancia = Convert.ToDecimal(reader["DistanciaKm"]),
                    Cultivo = reader["Cultivo"].ToString(),
                    Latitud = reader["Latitud"].ToString(),
                    Longitud = reader["Longitud"].ToString()
                });
            }
            reader.Close();
            Logger.LogInformation("Fin: leyendo datos fotos por documento");
            return lstClientes;
        }

        public (int id, string fileName) CargarFotoJPG(string NumeroSolicitud, string NumeroDocumento, Stream stream)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            var fileExtension = "jpg";
            var ldDocumentId = 0;
            var FileName = "";
            //var FileName;
            var metadata = new Dictionary<string, object>
            {
                {"NumeroSolicitud", NumeroSolicitud},
                {"DocumentoCliente", NumeroDocumento}
            };
            stream.Position = 0;
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            ms.Position = 0;
            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}\{NumeroSolicitud}";

            try
            {
                lfhelper.GetOrCreateFolderInfo(destinPath);
                var documentPath = $@"{destinPath}\GR_{NumeroDocumento}.{fileExtension}";
                ldDocumentId = lfhelper.RegisterImageWithMetaData(ms, metadata, documentPath, "Default", ApplicationKeys.LaserFicheTemplateName, out FileName, contenType: TypesConstants.JpgMIME);

            }
            catch (Exception exc)
            {
                Logger.LogException(exc, "CargarFotoJPG");
                throw;
            }
            finally
            {
                lfhelper.Dispose();
                lfhelper = null;
            }

            return (ldDocumentId, FileName);
        }

        public void InsertarFotoPorSolicitud(string NumeroSolicitud, string NumeroDocumento, FotoModel foto, string Usuario)
        {
            var DataParam = new iDB2Parameter[10];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_IMGCOD", 30, foto.ImgCod);
                DataParam[1] = _db2DataContext.CreateParameter("VI_FECHAFOTO", 30, foto.FechaFoto);
                DataParam[2] = _db2DataContext.CreateParameter("VI_LATITUD", 30, foto.Latitud);
                DataParam[3] = _db2DataContext.CreateParameter("VI_LONGITUD", 30, foto.Longitud);
                DataParam[4] = _db2DataContext.CreateParameter("VI_LFID", 8, foto.IdLaserFiche.ToString());
                DataParam[5] = _db2DataContext.CreateParameter("VI_OBS", 500, foto.Observacion);
                DataParam[6] = _db2DataContext.CreateParameter("VI_NROSOLICITUD", 9, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
                DataParam[7] = _db2DataContext.CreateParameter("VI_NUMDOC", 12, NumeroDocumento);
                //DataParam[8] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 5, CodigoAgencia, 0, iDB2DbType.iDB2Numeric);
                DataParam[8] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 20, ApplicationConstants.IdPlaformaAgroperu);
                DataParam[9] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);


                Logger.LogInformation("Inicio: Registro de fotos a solicitud");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_PFA_INS_FOTO_POR_SOLICITUD, DataParam);
                Logger.LogInformation("Fin: Registro de fotos a solicitud");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public void VincularFotosPorSolcitud(string NumeroSolicitud, string NumeroDocumento, string tramaCodImagenes, string tramaIdLF, string Usuario)
        {
            string CodError = string.Empty;
            string MsgError = string.Empty;
            var lDataParam = new iDB2Parameter[8];
            try
            {
                //lDataParam[0] = _db2DataContext.CreateParameter("VI_TOKENID", 12, token.TokenId, 0, iDB2DbType.iDB2Numeric);
                lDataParam[0] = _db2DataContext.CreateParameter("VI_NROSOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
                lDataParam[1] = _db2DataContext.CreateParameter("VI_NUMDOC", 15, NumeroDocumento);
                lDataParam[2] = _db2DataContext.CreateParameter("VI_USUARIO", 10, Usuario);
                lDataParam[3] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 20, "015");
                lDataParam[4] = _db2DataContext.CreateParameter("VI_LST_IMAGENES", 250, tramaCodImagenes);
                lDataParam[5] = _db2DataContext.CreateParameter("VI_LST_LFID", 250, tramaIdLF);
                lDataParam[6] = _db2DataContext.CreateParameter("VO_CODERROR", 3, direction: ParameterDirection.Output);
                lDataParam[7] = _db2DataContext.CreateParameter("VO_MSGERROR", 50, direction: ParameterDirection.Output);

                Logger.LogInformation("Inicio: Registro de vinculación de fotos a solicitud");
                Logger.LogDbParameters(lDataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_PFA_ACT_VINCULAR_FOTO_XSOLICITUD, lDataParam);
                Logger.LogInformation("Fin: Registro de vinculación de fotos a solicitud");
                CodError = Convert.ToString(lDataParam[6].Value);
                MsgError = Convert.ToString(lDataParam[7].Value);
                if (!CodError.Equals("0"))
                {
                    Logger.LogError(MsgError);
                    throw new InvalidDataBaseOperationException(MsgError);
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }

        public void VincularFotosLaserFiche(string NumeroSolicitud, string NumeroDocumento, List<FotoModel> lstIdImgVin, List<FotoModel> lstIdImgLib = null)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            var fileExtension = "jpg";            

            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}\{NumeroSolicitud}";

            try
            {
                lfhelper.GetOrCreateFolderInfo(destinPath);
                foreach (FotoModel IdImg in lstIdImgVin)
                {
                    var documentPathVin = $@"{destinPath}\GR_{NumeroDocumento}.{fileExtension}";
                    lfhelper.MoveDocument(IdImg.IdLaserFiche, documentPathVin);
                }

                if (lstIdImgLib != null)
                {
                    var destinPathLib = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}";
                    lfhelper.GetOrCreateFolderInfo(destinPathLib);
                    foreach (FotoModel IdImg in lstIdImgLib)
                    {
                        var documentPathLib = $@"{destinPathLib}\GR_{NumeroDocumento}.{fileExtension}";
                        lfhelper.MoveDocument(IdImg.IdLaserFiche, documentPathLib);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Vincular Fotos");
                throw;
            }
            finally
            {
                lfhelper.Dispose();
                lfhelper = null;
            }
        }

        public List<FotoModel> GetFotosPorSolicitud(string NumeroSolicitud)
        {
            List<FotoModel> lstFotos = null;
            var ftpClient = new FtpHelper();
            var lDataParam = new iDB2Parameter[1];
            
            lDataParam[0] = _db2DataContext.CreateParameter("VI_NROSOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
            Logger.LogInformation("Inicio: leyendo datos fotos por documento");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_MEG_SEL_IMAGENESXSOLICITUD, out IDataReader reader, lDataParam);
            lstFotos = new List<FotoModel>();
            while (reader.Read())
            {
                lstFotos.Add(new FotoModel
                {
                    CtaCod = reader.GetString("NumDoc"),
                    ImgCod = reader.GetString("ImgCod"),                    
                    FechaRegistro = reader.GetString("FecReg"),
                    Latitud = reader.GetString("LATITUD"),
                    Longitud = reader.GetString("LONGITUD"),
                    Observacion = reader.GetString("OBS"),
                    Estado = reader.GetString("EST"),
                    UltActualizacion = reader.GetString("UltAct"),
                    FechaFoto = reader.GetString("FechaFoto"),
                    FechaVinculacion = reader.GetString("FecVinculacion"),
                    IdLaserFiche = reader.GetIntOrNull("LFID").GetValueOrDefault(0)
                });
            }
            Logger.LogInformation("Fin: leyendo datos fotos por solicitud");
            reader.Close();

            Logger.LogInformation("Inicio: leyendo fotos LaserFicheRepository");

            string base64String = string.Empty;
            foreach (FotoModel foto in lstFotos)
            {
                if (foto.IdLaserFiche != 0)
                {
                    var img = DescagarImgLaserFiche(foto.IdLaserFiche);
                    if (img.stream != null)
                    {
                        byte[] imageBytes = img.stream?.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                        imageBytes = null;
                        img.stream.Close();
                        foto.imgBase64 = base64String;
                        foto.ImgCod = img.fileName;
                    }
                    else
                    {
                        lstFotos = null;
                        break;
                    }
                }
            }
            Logger.LogInformation("Fin: leyendo fotos por ftp del servidor y LaserFicheRepository");
            return lstFotos;
        }

        //public void SetDataSpatial(string NumeroSolicitud)
        //{
        //    // Lista Fotos
        //    List<FotoModel> lstFotos = null;            
        //    var lDataParam = new iDB2Parameter[1];
        //    lDataParam[0] = _db2DataContext.CreateParameter("VI_NROSOLICITUD", 12, NumeroSolicitud, 0, iDB2DbType.iDB2Decimal);
        //    Logger.LogInformation("Inicio: leyendo datos fotos por documento");
        //    Logger.LogDbParameters(lDataParam);
        //    _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_MEG_SEL_INF_VISITA_XSOLICITUD, out IDataReader reader, lDataParam);
        //    lstFotos = new List<FotoModel>();
        //    while (reader.Read())
        //    {
        //        lstFotos.Add(new FotoModel
        //        {
        //            EvalId = reader.GetIntOrNull("EVALID").GetValueOrDefault(0),
        //            Agencia = reader.GetString("AGENCIA"),
        //            Funcionario = reader.GetString("FUNCIONARIO"),
        //            IdLaserFiche = reader.GetIntOrNull("LFID").GetValueOrDefault(0),
        //            FechaFoto = reader.GetString("FechaFoto"),
        //            Latitud = reader.GetString("LATITUD"),
        //            Longitud = reader.GetString("LONGITUD"),
        //            Observacion = reader.GetString("OBS"),
        //            NroSolicitud = reader.GetString("MFISOL"),
        //            Cultivo = reader.GetString("CULTIVO"),
        //            Distancia = reader.GetDecimal("AREA")
        //        });
        //    }
        //    Logger.LogInformation("Fin: leyendo datos fotos por solicitud");
        //    reader.Close();

        //    // Lista coordenada de perimetro
        //    var Parametros = new iDB2Parameter[1];
        //    Parametros[0] = _db2DataContext.CreateParameter("VI_EVALID", 12, lstFotos.First().EvalId, 0, iDB2DbType.iDB2Integer);
        //    Logger.LogInformation("Inicio: leyendo Puntos de Perimetro");
        //    Logger.LogDbParameters(Parametros);
        //    _db2DataContext.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_MEG_SEL_PERIMETRO, out IDataReader readerPrm, Parametros);
        //    var lstPerimetros = new List<Coordenada>();
        //    while (readerPrm.Read())
        //    {
        //        lstPerimetros.Add(new Coordenada
        //        {                    
        //            Latitud = readerPrm.GetString("XCOORD"),
        //            Longitud = readerPrm.GetString("YCOORD")
        //        }); 
        //    }
        //    Logger.LogInformation("Fin: leyendo Puntos de Perimetro");
        //    readerPrm.Close();

        //    // Instancia Insert SQL 
        //    var data = new FotosVisita(lstFotos, lstPerimetros, true);
        //    string CodError = string.Empty;
        //    string MsgError = string.Empty;
        //    var sqlParametros = new SqlParameter[17];
        //    try
        //    { 
        //        sqlParametros[0] = _SqlDataAccess.CreateParameter("@IdVisita", 12, data.IdVisita, 0, SqlDbType.Int);
        //        sqlParametros[1] = _SqlDataAccess.CreateParameter("@CodigoAgencia", 6, data.Agencia.Split('_')[0], 0, SqlDbType.Int);
        //        sqlParametros[2] = _SqlDataAccess.CreateParameter("@NombreAgencia", 50, data.Agencia.Split('_')[1]);
        //        sqlParametros[3] = _SqlDataAccess.CreateParameter("@CodigoFuncionario", 4, data.Funcionario.Split('_')[0]);
        //        sqlParametros[4] = _SqlDataAccess.CreateParameter("@NombreFuncionario", 50, data.Funcionario.Split('_')[1]);
        //        sqlParametros[5] = _SqlDataAccess.CreateParameter("@NumeroSolicitud", 12, data.NumeroSolicitud, 0, SqlDbType.Decimal);
        //        sqlParametros[6] = _SqlDataAccess.CreateParameter("@Area", 12, data.Area, 2, SqlDbType.Decimal);
        //        sqlParametros[7] = _SqlDataAccess.CreateParameter("@CodigoCultivo", 6, data.Cultivo.Split('_')[0]);
        //        sqlParametros[8] = _SqlDataAccess.CreateParameter("@DescripcionCultivo", 30, data.Cultivo.Split('_')[1]);
        //        sqlParametros[9] = _SqlDataAccess.CreateParameter("@TramaCoordenadas", 1000, data.tramaPoligono);

        //        sqlParametros[10] = _SqlDataAccess.CreateParameter("@lstCadenaidlf", 700, data.TramaIdLaserFiche);
        //        sqlParametros[11] = _SqlDataAccess.CreateParameter("@lstCadenalat", 700, data.TramaLatitud);
        //        sqlParametros[12] = _SqlDataAccess.CreateParameter("@lstCadenalon", 700, data.TramaLongitud);
        //        sqlParametros[13] = _SqlDataAccess.CreateParameter("@lstCadenafecha", 700, data.TramaFechaFoto);
        //        sqlParametros[14] = _SqlDataAccess.CreateParameter("@lstCadenaobs", 2000, data.TramaObservacion);
        //        sqlParametros[15] = _SqlDataAccess.CreateParameter("@CodError", 5, direction: ParameterDirection.Output);
        //        sqlParametros[16] = _SqlDataAccess.CreateParameter("@MsgError", 70, direction: ParameterDirection.Output);


        //        Logger.LogInformation("Inicio: Inserta Datos espaciales");
        //        Logger.LogDbParameters(sqlParametros);
        //        _SqlDataAccess.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_GEO_INS_DATOS_SPATIAL, sqlParametros);

        //        CodError = Convert.ToString(sqlParametros[15].Value);
        //        MsgError = Convert.ToString(sqlParametros[16].Value);
        //        if (!CodError.Equals("0"))
        //        {
        //            Logger.LogError(MsgError);
        //            throw new InvalidDataBaseOperationException(MsgError);
        //        }
        //        Logger.LogInformation("Fin: Inserta Datos espaciales");
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException(e, e.Message);
        //        throw new InvalidDataBaseOperationException(e.Message);
        //    }

        //    //CrearPoligonoSQL(DatosFotos);
        //    //InserDatosFotoSQL(DatosFotos);
        //}

        //public void CrearPoligonoSQL(FotosVisita data)
        //{
        //    string CodError = string.Empty;
        //    string MsgError = string.Empty;
        //    var sqlParametros = new SqlParameter[6];
        //    try
        //    {
        //        sqlParametros[0] = _SqlDataAccess.CreateParameter("@IdVisita", 12, data.IdVisita, 0, SqlDbType.Int);
        //        sqlParametros[1] = _SqlDataAccess.CreateParameter("@NumeroSolicitud", 12, data.NumeroSolicitud, 0, SqlDbType.Decimal);
        //        sqlParametros[2] = _SqlDataAccess.CreateParameter("@Area", 12, data.Area, 2, SqlDbType.Decimal);
        //        sqlParametros[3] = _SqlDataAccess.CreateParameter("@CodigoCultivo", 6, data.Cultivo.Split('_')[0]);
        //        sqlParametros[4] = _SqlDataAccess.CreateParameter("@DescripcionCultivo", 30, data.Cultivo.Split('_')[1]);
        //        sqlParametros[5] = _SqlDataAccess.CreateParameter("@TramaCoordenadas", 1000, data.tramaPoligono);


        //        Logger.LogInformation("Inicio: Inserta Datos espaciales");
        //        Logger.LogDbParameters(sqlParametros);
        //        _SqlDataAccess.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_GEO_INS_CREAR_POLIGONO, sqlParametros);
        //        Logger.LogInformation("Fin: Inserta Datos espaciales");
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException(e, e.Message);
        //        throw new InvalidDataBaseOperationException(e.Message);
        //    }
        //}

        //public void InserDatosFotoSQL(FotosVisita data)
        //{
        //    string CodError = string.Empty;
        //    string MsgError = string.Empty;
        //    var sqlParametros = new SqlParameter[8];
        //    try
        //    {
        //        sqlParametros[0] = _SqlDataAccess.CreateParameter("@IdVisita", 12, data.IdVisita, 0, SqlDbType.Int);
        //        sqlParametros[1] = _SqlDataAccess.CreateParameter("@lstCadenaidlf", 700, data.TramaIdLaserFiche);
        //        sqlParametros[2] = _SqlDataAccess.CreateParameter("@lstCadenalat", 700, data.TramaLatitud);
        //        sqlParametros[3] = _SqlDataAccess.CreateParameter("@lstCadenalon", 700, data.TramaLongitud);
        //        sqlParametros[4] = _SqlDataAccess.CreateParameter("@lstCadenafecha", 700, data.TramaFechaFoto);
        //        sqlParametros[5] = _SqlDataAccess.CreateParameter("@lstCadenaobs", 2000, data.TramaObservacion);
        //        sqlParametros[6] = _SqlDataAccess.CreateParameter("@CodError", 5, direction:ParameterDirection.Output);
        //        sqlParametros[7] = _SqlDataAccess.CreateParameter("@MsgError", 70, direction: ParameterDirection.Output);

        //        Logger.LogInformation("Inicio: Registro Datos Fotos");
        //        Logger.LogDbParameters(sqlParametros);
        //        _SqlDataAccess.RunStoredProcedure(StoreConstants.Georeferenciacion.UP_GEO_INS_DATOS_FOTOS, sqlParametros);

        //        CodError = Convert.ToString(sqlParametros[6].Value);
        //        MsgError = Convert.ToString(sqlParametros[7].Value);
        //        if (!CodError.Equals("0"))
        //        {
        //            Logger.LogError(MsgError);
        //            throw new InvalidDataBaseOperationException(MsgError);
        //        }
        //        Logger.LogInformation("Fin: Registro Datos Fotos");
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException(e, e.Message);
        //        throw new InvalidDataBaseOperationException(e.Message);
        //    }
        //}

    }
}

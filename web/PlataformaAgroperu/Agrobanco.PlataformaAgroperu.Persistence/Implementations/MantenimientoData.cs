﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System;


namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class MantenimientoData
    {
        #region Constructores
        private readonly Db2DataAccess _db2DataContext;
        public MantenimientoData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }
        #endregion

        #region Comsiones
        public void CrearComision(int CodigoOrganizacion, string NombreComision, string Usuario)
        {            
            var DataParam = new iDB2Parameter[3];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_ORGANIZACION", 4, CodigoOrganizacion, 0, iDB2DbType.iDB2Decimal);
                DataParam[1] = _db2DataContext.CreateParameter("VI_NOMBRE_COMISION", 50, NombreComision);                
                DataParam[2] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);


                Logger.LogInformation("Inicio: Crear Comision Store");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_MEG_INS_CREAR_COMISION, DataParam);
                Logger.LogInformation("Fin: Registro de fotos a solicitud");                
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
            
        }        
        public void ActualizaComision(int CodigoOrganizacion, int CodigoComision, string NombreComision, string Usuario)
        {
            
            var DataParam = new iDB2Parameter[4];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_ORGANIZACION", 4, CodigoOrganizacion, 0, iDB2DbType.iDB2Decimal);
                DataParam[1] = _db2DataContext.CreateParameter("VI_CODIGO_COMISION", 4, CodigoComision, 0, iDB2DbType.iDB2Decimal);
                DataParam[2] = _db2DataContext.CreateParameter("VI_NOMBRE_COMISION", 50, NombreComision);
                DataParam[3] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);


                Logger.LogInformation("Inicio: Actualiza Comision Store");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_MEG_UPD_ACTUALIZA_COMISION, DataParam);
                Logger.LogInformation("Fin: Actualiza Comision");
            
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
            
        }
        public void EliminaComision(int CodigoOrganizacion, int CodigoComision, string Usuario)
        {
            
            var DataParam = new iDB2Parameter[3];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_ORGANIZACION", 4, CodigoOrganizacion, 0, iDB2DbType.iDB2Decimal);
                DataParam[1] = _db2DataContext.CreateParameter("VI_CODIGO_COMISION", 4, CodigoComision, 0, iDB2DbType.iDB2Decimal);                
                DataParam[2] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);


                Logger.LogInformation("Inicio: Eliminar Comision Store");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_MEG_UPD_ELIMINA_COMISION, DataParam);
                Logger.LogInformation("Fin: Eliminar Comision");
                
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
            
        }
        #endregion

        #region ProfesionalTecnicos
        public void CrearProfesionalTecnico(DatosPersona ProfesionalTecnico, string CodigoAgencia, string CodigoOrganizacion, string Usuario)
        {
            var DataParam = new iDB2Parameter[10];            
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_IDN", 15, ProfesionalTecnico.NroDocumento);
                DataParam[1] = _db2DataContext.CreateParameter("VI_PWD", 32, ProfesionalTecnico.Password);
                DataParam[2] = _db2DataContext.CreateParameter("VI_EMAIL", 60, ProfesionalTecnico.Correo);
                DataParam[3] = _db2DataContext.CreateParameter("VI_NOMBRE1", 60, ProfesionalTecnico.NomPrimer);
                DataParam[4] = _db2DataContext.CreateParameter("VI_NOMBRE2", 60, ProfesionalTecnico.NomSegundo);
                DataParam[5] = _db2DataContext.CreateParameter("VI_APELLIDO1", 60, ProfesionalTecnico.ApePaterno);
                DataParam[6] = _db2DataContext.CreateParameter("VI_APELLIDO2", 60, ProfesionalTecnico.ApeMaterno);
                DataParam[7] = _db2DataContext.CreateParameter("VI_AGENCIA", 5, CodigoAgencia, 0, iDB2DbType.iDB2Decimal);
                DataParam[8] = _db2DataContext.CreateParameter("VI_ORGANIZACION", 4, CodigoOrganizacion, 0, iDB2DbType.iDB2Decimal);
                DataParam[9] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);


                Logger.LogInformation("Inicio: Crear Profesional tecnico");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_MEG_INS_CREAR_PROFTECNICO, DataParam);
                Logger.LogInformation("Fin: Registro Profesional Tecnico");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }

        }
        #endregion
        
        #region MatrizCostos
        public List<MatrizCostos> ListarCostos()
        {
            var listReturn = new List<MatrizCostos>();
            _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_SEL_COSTOS, out IDataReader reader);
            
            while (reader.Read())
            {
                listReturn.Add(new MatrizCostos
                {
                    Codigo = reader.GetInt("CODIGO"),
                    Sector = reader.GetString("SECTOR"),
                    SectorDescripcion = reader.GetString("SECTOR_DESCRIPCION"),
                    Programa = reader.GetString("PROGRAMA"),
                    NivelTecnologico = reader.GetString("NIVEL_TECNOLOGICO"),
                    CodigoProducto = reader.GetString("CODIGO_PRODUCTO"),
                    NombreProducto = reader.GetString("PRODUCTO"),
                    DescripcionProducto = reader.GetString("DESCRIPCION"),
                    TipoProducto = reader.GetString("TIPO_PRODUCTO"),
                    Priorizacion = reader.GetString("PRIORIZACION"),
                    UnidadFinanciamiento = reader.GetString("UNIDAD_FINANCIAMIENTO"),
                    Ubigeo = reader.GetString("UBIGEO"),
                    Departamento = reader.GetString("DEPARTAMENTO"),
                    Provincia = reader.GetString("PROVINCIA"),
                    Distrito = reader.GetString("DISTRITO"),

                    Rendimiento = reader.GetDecimal("RENDIMIENTO"),
                    UnidadRendimiento = reader.GetString("UNIDAD_RENDIMIENTO"),
                    PrecioVentaChacra = reader.GetDecimal("PRECIO_VENTA_CHACRA"),
                    UnidadPrecioVenta = reader.GetString("UNIDAD_PRECIO"),
                    CostoProduccion = reader.GetDecimal("COSTO_PRODUCCION"),
                    UnidadCosto = reader.GetString("UNIDAD_COSTO"),
                    PeriodoFenologico = reader.GetInt("PERIODO_FENOLOGICO"),
                    PeriodoPostCosecha = reader.GetInt("PERIODO_POSTCOSECHA"),
                    FechaInicioCampana = reader.GetInt("INICIO_CAMPANA"),
                    FechaFinCampana = reader.GetInt("FIN_CAMPANA"),
                    Campana = reader.GetInt("CAMPANA"),
                    SiembraMesInicio = reader.GetInt("INICIO_SIEMBRA"),
                    SiembraMesFin = reader.GetInt("FIN_SIEMBRA"),
                    CosechaMesInicio = reader.GetInt("INICIO_COSECHA"),
                    CosechaMesFin = reader.GetInt("FIN_COSECHA"),

                    MontoFinanciar = reader.GetDecimal("MONTO_FINANCIAMIENTO"),
                    PorcentajeFinanciar = reader.GetDecimal("PORCENT_FINANCIAMIENTO"),
                    AsistenciaTecnica = reader.GetDecimal("ASISTENCIA_TECNICA"),
                    AsistenciaTecnicaPorcentaje = reader.GetDecimal("ASISTENCIA_TECNICA_PORCENTAJE"),
                    Plazo = reader.GetInt("PLAZO"),
                    TipoCronograma = reader.GetString("TIPO_CUOTA"),
                    MaximoCuotas = reader.GetInt("MAXIMO_CUOTAS"),
                    MaximoDiasGracia = reader.GetInt("MAXIMO_PERIODOGRACIA"),
                    CronogramasPermitidos = reader.GetString("CUOTAS_PERMITIDAS"),
                    Opciones = reader.GetString("OPCIONES")
                });
            }
            return listReturn;
        }
        public List<MatrizCostos> ListarCostosHistorico()
        {
            var listReturn = new List<MatrizCostos>();
            _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_SEL_COSTOS_HISTORICO, out IDataReader reader);

            while (reader.Read())
            {
                listReturn.Add(new MatrizCostos
                {
                    Codigo = reader.GetInt("CODIGO"),
                    Sector = reader.GetString("SECTOR"),
                    SectorDescripcion = reader.GetString("SECTOR_DESCRIPCION"),
                    Programa = reader.GetString("PROGRAMA"),
                    NivelTecnologico = reader.GetString("NIVEL_TECNOLOGICO"),
                    CodigoProducto = reader.GetString("CODIGO_PRODUCTO"),
                    NombreProducto = reader.GetString("PRODUCTO"),
                    DescripcionProducto = reader.GetString("DESCRIPCION"),
                    TipoProducto = reader.GetString("TIPO_PRODUCTO"),
                    Priorizacion = reader.GetString("PRIORIZACION"),
                    UnidadFinanciamiento = reader.GetString("UNIDAD_FINANCIAMIENTO"),
                    Ubigeo = reader.GetString("UBIGEO"),
                    Departamento = reader.GetString("DEPARTAMENTO"),
                    Provincia = reader.GetString("PROVINCIA"),
                    Distrito = reader.GetString("DISTRITO"),

                    Rendimiento = reader.GetDecimal("RENDIMIENTO"),
                    UnidadRendimiento = reader.GetString("UNIDAD_RENDIMIENTO"),
                    PrecioVentaChacra = reader.GetDecimal("PRECIO_VENTA_CHACRA"),
                    UnidadPrecioVenta = reader.GetString("UNIDAD_PRECIO"),
                    CostoProduccion = reader.GetDecimal("COSTO_PRODUCCION"),
                    UnidadCosto = reader.GetString("UNIDAD_COSTO"),
                    PeriodoFenologico = reader.GetInt("PERIODO_FENOLOGICO"),
                    PeriodoPostCosecha = reader.GetInt("PERIODO_POSTCOSECHA"),
                    FechaInicioCampana = reader.GetInt("INICIO_CAMPANA"),
                    FechaFinCampana = reader.GetInt("FIN_CAMPANA"),
                    Campana = reader.GetInt("CAMPANA"),
                    SiembraMesInicio = reader.GetInt("INICIO_SIEMBRA"),
                    SiembraMesFin = reader.GetInt("FIN_SIEMBRA"),
                    CosechaMesInicio = reader.GetInt("INICIO_COSECHA"),
                    CosechaMesFin = reader.GetInt("FIN_COSECHA"),

                    MontoFinanciar = reader.GetDecimal("MONTO_FINANCIAMIENTO"),
                    PorcentajeFinanciar = reader.GetDecimal("PORCENT_FINANCIAMIENTO"),
                    AsistenciaTecnica = reader.GetDecimal("ASISTENCIA_TECNICA"),
                    AsistenciaTecnicaPorcentaje = reader.GetDecimal("ASISTENCIA_TECNICA_PORCENTAJE"),
                    Plazo = reader.GetInt("PLAZO"),
                    TipoCronograma = reader.GetString("TIPO_CUOTA"),
                    MaximoCuotas = reader.GetInt("MAXIMO_CUOTAS"),
                    MaximoDiasGracia = reader.GetInt("MAXIMO_PERIODOGRACIA"),
                    CronogramasPermitidos = reader.GetString("CUOTAS_PERMITIDAS"),
                    Opciones = reader.GetString("OPCIONES"),
                    Estado = reader.GetString("ESTADO")
                });
            }
            return listReturn;
        }
        public List<ParametricasModel> ObtenerDetalleCosto(int CodigoCosto)
        {
            var listReturn = new List<ParametricasModel>();
            var DataParam = new iDB2Parameter[1];

            DataParam[0] = _db2DataContext.CreateParameter("VI_CODIGO", 12, CodigoCosto, 0, iDB2DbType.iDB2Integer);
            _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_SEL_COSTOS_DETALLE, out IDataReader reader, DataParam);

            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("TIPO"),
                    code = reader.GetString("SECUENCIA"),
                    value = reader.GetString("VALORES"),
                    flag = reader.GetString("PORCENTAJES")

                });
            }
            reader.Close();
            return listReturn;
        }
        public MemoryStream ObtenerExcelMatrizCostos()
        {
            
            DataSet dataSet = new DataSet();
            _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_SEL_COSTOS_DESCARGAR, out DataTable table1);
            _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_SEL_COSTOS_DETALLE_TOTAL, out DataTable table2);
            dataSet.Tables.Add(table1);
            dataSet.Tables.Add(table2);
            return ExportToExcel(dataSet);
        }
        public void RegistrarCosto(MatrizCostos Costo, string Usuario)
        {
            var DataParam = new iDB2Parameter[44];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_CODIGO", 12, Costo.Codigo, 0, iDB2DbType.iDB2Integer);
                DataParam[1] = _db2DataContext.CreateParameter("VI_SECTOR", 2, Costo.Sector, 0, iDB2DbType.iDB2Integer);
                DataParam[2] = _db2DataContext.CreateParameter("VI_PROGRAMA", 4, Costo.Programa);
                DataParam[3] = _db2DataContext.CreateParameter("VI_NIVEL_TECNOLOGICO", 2, Costo.NivelTecnologico, 0, iDB2DbType.iDB2Integer);
                DataParam[4] = _db2DataContext.CreateParameter("VI_PRODUCTO", 6, Costo.CodigoProducto);
                DataParam[5] = _db2DataContext.CreateParameter("VI_DESCRIPCION_PRODUCTO", 80, Costo.DescripcionProducto);
                DataParam[6] = _db2DataContext.CreateParameter("VI_TIPO_PRODUCTO", 4, Costo.TipoProducto);
                DataParam[7] = _db2DataContext.CreateParameter("VI_PRIORIZACION", 4, Costo.Priorizacion);
                DataParam[8] = _db2DataContext.CreateParameter("VI_UNIDAD_FINANCIAMIENTO", 4, Costo.UnidadFinanciamiento);
                DataParam[9] = _db2DataContext.CreateParameter("VI_UBIGEO", 6, Costo.Ubigeo);

                DataParam[10] = _db2DataContext.CreateParameter("VI_RENDIMIENTO", 15, Costo.Rendimiento, 2, iDB2DbType.iDB2Decimal);
                DataParam[11] = _db2DataContext.CreateParameter("VI_UNIDAD_RENDIMIENTO", 5, Costo.UnidadRendimiento);
                DataParam[12] = _db2DataContext.CreateParameter("VI_PRECIO_VENTA_CHACRA", 15, Costo.PrecioVentaChacra, 2, iDB2DbType.iDB2Decimal);
                DataParam[13] = _db2DataContext.CreateParameter("VI_UNIDAD_PRECIO", 5, Costo.UnidadPrecioVenta);
                DataParam[14] = _db2DataContext.CreateParameter("VI_COSTO_PRODUCCION", 15, Costo.CostoProduccion, 2, iDB2DbType.iDB2Decimal);
                DataParam[15] = _db2DataContext.CreateParameter("VI_UNIDAD_COSTO", 5, Costo.UnidadCosto);
                DataParam[16] = _db2DataContext.CreateParameter("VI_PERIODO_FENOLOGICO", 3, Costo.PeriodoFenologico, 0, iDB2DbType.iDB2Integer);
                DataParam[17] = _db2DataContext.CreateParameter("VI_PERIODO_POST_COSECHA", 3, Costo.PeriodoPostCosecha, 0, iDB2DbType.iDB2Integer);
                DataParam[18] = _db2DataContext.CreateParameter("VI_INICIO_CAMPANA", 12, Costo.FechaInicioCampana, 0, iDB2DbType.iDB2Integer);
                DataParam[19] = _db2DataContext.CreateParameter("VI_FIN_CAMPANA", 12, Costo.FechaFinCampana, 0, iDB2DbType.iDB2Integer);
                DataParam[20] = _db2DataContext.CreateParameter("VI_CAMPANA", 12, Costo.Campana, 0, iDB2DbType.iDB2Integer);
                DataParam[21] = _db2DataContext.CreateParameter("VI_INICIO_SIEMBRA", 2, Costo.SiembraMesInicio, 0, iDB2DbType.iDB2Integer);
                DataParam[22] = _db2DataContext.CreateParameter("VI_FIN_SIEMBRA", 2, Costo.SiembraMesFin, 0, iDB2DbType.iDB2Integer);
                DataParam[23] = _db2DataContext.CreateParameter("VI_INICIO_COSECHA", 2, Costo.CosechaMesInicio, 0, iDB2DbType.iDB2Integer);
                DataParam[24] = _db2DataContext.CreateParameter("VI_FIN_COSECHA", 2, Costo.CosechaMesFin, 0, iDB2DbType.iDB2Integer);

                DataParam[25] = _db2DataContext.CreateParameter("VI_MONTO_FINANCIAMIENTO", 15, Costo.MontoFinanciar, 2, iDB2DbType.iDB2Decimal);
                DataParam[26] = _db2DataContext.CreateParameter("VI_PORCENT_FINANCIAMIENTO", 4, Costo.PorcentajeFinanciar, 2, iDB2DbType.iDB2Decimal);
                DataParam[27] = _db2DataContext.CreateParameter("VI_ASISTENCIA_TECNICA", 15, Costo.AsistenciaTecnica, 2, iDB2DbType.iDB2Decimal);
                DataParam[28] = _db2DataContext.CreateParameter("VI_ASISTENCIA_TECNICA_PORCENTAJE", 4, Costo.AsistenciaTecnicaPorcentaje, 2, iDB2DbType.iDB2Decimal);
                DataParam[29] = _db2DataContext.CreateParameter("VI_PLAZO", 12, Costo.Plazo, 0, iDB2DbType.iDB2Integer);
                DataParam[30] = _db2DataContext.CreateParameter("VI_TIPO_CUOTA", 12, Costo.TipoCronograma, 0, iDB2DbType.iDB2Integer);
                DataParam[31] = _db2DataContext.CreateParameter("VI_NUMERO_CUOTAS", 12, Costo.MaximoCuotas, 0, iDB2DbType.iDB2Integer);
                DataParam[32] = _db2DataContext.CreateParameter("VI_PERIODO_GRACIA", 12, Costo.MaximoDiasGracia, 0, iDB2DbType.iDB2Integer);
                DataParam[33] = _db2DataContext.CreateParameter("VI_CUOTAS_PERMITIDAS", 10, Costo.CronogramasPermitidos);

                DataParam[34] = _db2DataContext.CreateParameter("VI_NUMERO_DESEMBOLSOS", 3, Costo.NumeroDesembolsos, 0, iDB2DbType.iDB2Integer);
                DataParam[35] = _db2DataContext.CreateParameter("VI_DESEMBOLSOS_PORCENTAJES", 200, Costo.TramaDesembolsoPorcentajes);
                DataParam[36] = _db2DataContext.CreateParameter("VI_DESEMBOLSOS_MES_INICIO", 200, Costo.TramaDesembolsoMesInicio);
                DataParam[37] = _db2DataContext.CreateParameter("VI_DESEMBOLSOS_MES_FIN", 200, Costo.TramaDesembolsoMesFin);

                DataParam[38] = _db2DataContext.CreateParameter("VI_NUMERO_PAGOS_AT", 3, Costo.NumeroPagosAT, 0, iDB2DbType.iDB2Integer);
                DataParam[39] = _db2DataContext.CreateParameter("VI_PAGOS_AT_PORCENTAJES", 200, Costo.TramaPagosATPorcentajes);
                DataParam[40] = _db2DataContext.CreateParameter("VI_PAGOS_AT_DIAS", 200, Costo.TramaPagosATDias);

                DataParam[41] = _db2DataContext.CreateParameter("VI_NUMERO_VISITAS", 3, Costo.NumeroInfoVisitas, 0, iDB2DbType.iDB2Integer);
                DataParam[42] = _db2DataContext.CreateParameter("VI_MESES_VISITAS", 200, Costo.TramaInfoVisitaMeses);

                DataParam[43] = _db2DataContext.CreateParameter("VI_USUARIO", 15, Usuario);

                Logger.LogInformation("Inicio: Crear Matriz de costo");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_INS_REGISTRAR_COSTO, DataParam);
                Logger.LogInformation("Fin: Registro Matriz de Costo");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }
        public void DeshablitarCosto(int CodigoCosto, string Usuario)
        {
            var DataParam = new iDB2Parameter[2];
            try
            {
                DataParam[0] = _db2DataContext.CreateParameter("VI_CODIGO", 12, CodigoCosto, 0, iDB2DbType.iDB2Integer);
                DataParam[1] = _db2DataContext.CreateParameter("VI_USUARIO", 10, Usuario);

                Logger.LogInformation("Inicio: Deshabilitar Matriz de costo");
                Logger.LogDbParameters(DataParam);
                _db2DataContext.RunStoredProcedure(StoreConstants.Mantenimiento.UP_PFA_ACT_DESHABILITAR_COSTO, DataParam);
                Logger.LogInformation("Fin: Deshabilitar Matriz de Costo");
            }
            catch (Exception e)
            {
                Logger.LogException(e, e.Message);
                throw new InvalidDataBaseOperationException(e.Message);
            }
        }
        #endregion

        public MemoryStream ExportToExcel(DataSet SetTables)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet1 = workbook.CreateSheet("Matriz Costos");
            ISheet sheet2 = workbook.CreateSheet("Detalle");

            IRow headerRow1 = sheet1.CreateRow(0);
            IRow headerRow2 = sheet2.CreateRow(0);

            // Create Header Style
            ICellStyle headerCellStyle = workbook.CreateCellStyle();

            var font = workbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.FontName = "Calibri";
            //font.Color = IndexedColors.DarkGreen.Index;

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            headerCellStyle.SetFont(font);
            headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Blue.Index;
            //headerCellStyle.FillPattern = FillPattern.BigSpots;
            headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Pink.Index;
            headerCellStyle.FillBackgroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillForegroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillPattern = FillPattern.SolidForeground;


            //HSSFColor lightGray = setColor(workbook, (byte)0xE0, (byte)0xE0, (byte)0xE0);
            //style2.setFillForegroundColor(lightGray.getIndex());
            //headerCellStyle.FillForegroundColor = g;
            //headerCellStyle.FillForegroundColor = HSSFColor.Lim
            //headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            // Create Date Style
            ICellStyle dateCellStyle = workbook.CreateCellStyle();
            dateCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy");

            ICellStyle numCellStyle = workbook.CreateCellStyle();
            numCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.000");


            ICellStyle cellStyle = workbook.CreateCellStyle();
            IDataFormat hssfDataFormat = workbook.CreateDataFormat();
            //String cellVal = "2500";
            //cellStyle.setDataFormat(hssfDataFormat.getFormat("#,##0.000"));
            //numCellStyle.setCellStyle(cellStyle);
            //numCellStyle.setCellValue(new Double(cellVal));
            //numCellStyle.setCellType(Cell.CELL_TYPE_NUMERIC);

            // Build Header

            int i = 0;
            // Build Details (rows)
            int rowIndex = 1;
            int sheetIndex = 1;
            const int maxRows = 65536;

            foreach (DataColumn column in SetTables.Tables[0].Columns)
            {
                ICell headerCell = headerRow1.CreateCell(column.Ordinal);
                headerCell.SetCellValue(column.ColumnName);
                headerCell.CellStyle = headerCellStyle;
                //if (i == 1 || i == 2)
                //{
                //    headerCell.CellStyle = numCellStyle;
                //}
                //else
                //{
                //    headerCell.CellStyle = headerCellStyle;
                //}
                i++;
            }



            foreach (DataRow row in SetTables.Tables[0].Rows)
            {
                // Start new sheet max rows reached
                if (rowIndex % maxRows == 0)
                {
                    // Auto size columns on current sheet
                    for (int h = 0; h < headerRow1.LastCellNum; h++)
                    {
                        sheet1.AutoSizeColumn(h);
                    }

                    sheetIndex++;
                    sheet1 = workbook.CreateSheet("Resultados" + sheetIndex);
                    IRow additionalHeaderRow = sheet1.CreateRow(0);

                    for (int h = 0; h < headerRow1.LastCellNum; h++)
                    {
                        ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                        additionalHeaderColumn.CellStyle = headerRow1.GetCell(h).CellStyle;
                        additionalHeaderColumn.SetCellValue(headerRow1.GetCell(h).RichStringCellValue);
                    }

                    rowIndex = 1;
                }

                // Create new row in sheet
                IRow dataRow = sheet1.CreateRow(rowIndex);

                foreach (DataColumn column in SetTables.Tables[0].Columns)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);

                    switch (column.DataType.FullName)
                    {
                        case "System.String":
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                        case "System.Int":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            double val;
                            dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                            break;
                        case "System.DateTime":
                            DateTime dt = new DateTime(1900, 01, 01);
                            DateTime.TryParse(row[column].ToString(), out dt);

                            dataCell.SetCellValue(dt);
                            dataCell.CellStyle = dateCellStyle;
                            break;
                        default:
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                    }
                }

                rowIndex++;
            }

            if (SetTables.Tables.Count > 1)
            {
                i = 0;
                // Build Details (rows)
                rowIndex = 1;
                sheetIndex = 1;

                foreach (DataColumn column in SetTables.Tables[1].Columns)
                {
                    ICell headerCell = headerRow2.CreateCell(column.Ordinal);
                    headerCell.SetCellValue(column.ColumnName);
                    headerCell.CellStyle = headerCellStyle;
                    i++;
                }

                foreach (DataRow row in SetTables.Tables[1].Rows)
                {
                    // Start new sheet max rows reached
                    if (rowIndex % maxRows == 0)
                    {
                        // Auto size columns on current sheet
                        for (int h = 0; h < headerRow2.LastCellNum; h++)
                        {
                            sheet2.AutoSizeColumn(h);
                        }

                        sheetIndex++;
                        sheet2 = workbook.CreateSheet("Resultados" + sheetIndex);
                        IRow additionalHeaderRow = sheet2.CreateRow(0);

                        for (int h = 0; h < headerRow2.LastCellNum; h++)
                        {
                            ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                            additionalHeaderColumn.CellStyle = headerRow2.GetCell(h).CellStyle;
                            additionalHeaderColumn.SetCellValue(headerRow2.GetCell(h).RichStringCellValue);
                        }

                        rowIndex = 1;
                    }

                    // Create new row in sheet
                    IRow dataRow = sheet2.CreateRow(rowIndex);

                    foreach (DataColumn column in SetTables.Tables[1].Columns)
                    {
                        ICell dataCell = dataRow.CreateCell(column.Ordinal);

                        switch (column.DataType.FullName)
                        {
                            case "System.String":
                                dataCell.SetCellValue(row[column].ToString());
                                break;
                            case "System.Int":
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Double":
                            case "System.Decimal":
                                double val;
                                dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                                break;
                            case "System.DateTime":
                                DateTime dt = new DateTime(1900, 01, 01);
                                DateTime.TryParse(row[column].ToString(), out dt);

                                dataCell.SetCellValue(dt);
                                dataCell.CellStyle = dateCellStyle;
                                break;
                            default:
                                dataCell.SetCellValue(row[column].ToString());
                                break;
                        }
                    }

                    rowIndex++;
                }
            }



            MemoryStream ms = new MemoryStream();

            for (int h = 0; h < headerRow1.LastCellNum; h++)
                sheet1.AutoSizeColumn(h);

            if (SetTables.Tables.Count > 1)
                for (int h = 0; h < headerRow2.LastCellNum; h++)
                    sheet2.AutoSizeColumn(h);

            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;
            return ms;
            //ExportToExcelx(workbook, fileName);
        }
    }
}

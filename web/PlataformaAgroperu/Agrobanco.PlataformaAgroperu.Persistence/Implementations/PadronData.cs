﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Domain.Models.Padron;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class PadronData
    {
        private readonly Db2DataAccess _db2DataContext;
        public PadronData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }
        public List<PadronDatos> ConsultarPadronPorAgencia(string CodAgencia, string flagFiltro)
        {
            var lstPadron = new List<PadronDatos>();

            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 8, CodAgencia, 0, iDB2DbType.iDB2Integer);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_CODFUNCIONARIO", 10, flagFiltro, 0, iDB2DbType.iDB2VarChar);
            lDataParam[2] = _db2DataContext.CreateParameter("VI_PLATAFORMA", 10, ApplicationConstants.IdPlaformaAgroperu, 0, iDB2DbType.iDB2VarChar);

            Logger.LogInformation("Inicio: Consulta Lista de Padron");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Padron.UP_PFA_SEL_PADRONX_AGENCIA_USUARIO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Padron");

            PadronDatos padron = null;
            while (reader.Read())
            {
                padron = new PadronDatos();
                padron.CodigoPadron = reader.GetInt("COD_PADRON");
                padron.NombrePadron = reader.GetString("NOMBRE_PADRON");
                padron.NombreJunta = reader.GetString("NOMBRE_JUNTA");
                padron.NombreComision = reader.GetString("NOMBRE_COMISION");                
                padron.NroRegistros = reader.GetInt("NRO_REGISTROS");
                padron.NroAprobados = reader.GetInt("NRO_APROBADOS");
                padron.Estado = reader.GetString("ESTADO");
                padron.FechaCreacion = reader.GetString("FECHA_CREACION");
                lstPadron.Add(padron);
            }
            return lstPadron;
        }
        public void RegistrarPadron(ref ExcelResponseModel respon, Grupo Padron, string FechaCliente)
        {
            try
            {
                var parameters = new iDB2Parameter[12];
                ParametricasData oPar = new ParametricasData();
                
                //if(!Padron.DatosGrupo.CodComisionRegante.Equals("0"))                
                //    oPar.EnviarHistorico(Padron.DatosGrupo.CodJuntaRegante.PadLeft(10, '0'), Padron.DatosGrupo.CodComisionRegante.PadLeft(4, '0'));

                var trama = Padron.getTramaPadronAgroperu();
                var Result = GenerarCodigoPadron(); //oPar.GenerarCodigoFiltro("JRPROS");//"0000123456789012";//
                
                if (Result.IdResult == "0000")
                {
                    string idgrupo = Result.CodigoPadron;
                    parameters[0] = _db2DataContext.CreateParameter("VI_TimeClient", 50, FechaCliente, 0, iDB2DbType.iDB2VarChar);
                    parameters[1] = _db2DataContext.CreateParameter("VI_CodigoPadron", 12, idgrupo, 0, iDB2DbType.iDB2VarChar);
                    parameters[2] = _db2DataContext.CreateParameter("VI_NumeroRegistros", 1, Padron.ExcelIntegrantes.Count, 0, iDB2DbType.iDB2Integer);
                    parameters[3] = _db2DataContext.CreateParameter("VI_CodigoAnalista", 4, Padron.DatosGrupo.Analista, 0, iDB2DbType.iDB2VarChar);
                    parameters[4] = _db2DataContext.CreateParameter("VI_NombreAnalista", 35, Padron.DatosGrupo.NombreAnalista, 0, iDB2DbType.iDB2VarChar);
                    parameters[5] = _db2DataContext.CreateParameter("VI_CodigoAgencia", 1, Padron.DatosGrupo.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                    parameters[6] = _db2DataContext.CreateParameter("VI_Nivel", 1, 0, 0, iDB2DbType.iDB2Integer);
                    parameters[7] = _db2DataContext.CreateParameter("VI_Usuario", 10, Padron.DatosGrupo.Usuario);
                    parameters[8] = _db2DataContext.CreateParameter("VI_TramaCabecera", 20000, Padron.DatosGrupo.getTrama(), 0, iDB2DbType.iDB2VarChar);
                    parameters[9] = _db2DataContext.CreateParameter("VI_TramaDetalle", 1000000, trama, 0, iDB2DbType.iDB2Clob);
                    parameters[10] = _db2DataContext.CreateParameter("VI_Plataforma", 3, ApplicationConstants.IdPlaformaAgroperu);
                    parameters[11] = _db2DataContext.CreateParameter("VO_MSG", 150, "", 0, iDB2DbType.iDB2VarChar);
                    parameters[11].Direction = System.Data.ParameterDirection.Output;

                    Logger.LogInformation("Registrando Padron");
                    Logger.LogDbParameters(parameters);
                    _db2DataContext.RunStoredProcedure(StoreConstants.Padron.UP_PFA_INS_EXCEL_PADRON, parameters, 100);
                    Logger.LogInformation("Fin Registrando Padron");
                    string msg = parameters[11].Value.ToString();
                    respon = new ExcelResponseModel(msg);

                    Logger.LogInformation("Evaluar Padron Inicio");
                    string ev = EvaluarPadron(idgrupo);
                    Logger.LogInformation("Fin Evaluar Padron Padron");

                }
            }
            catch (Exception ex)
            {
                respon.code = "10";
                respon.message = "SP:RegistrarPadron:" + ex.Message;
            }
        }
        public string EvaluarPadron(string CodigoPadron)
        {
            ParametricasData oPar = new ParametricasData();
            return oPar.EjecutarProgramaT20000(
                As400Constants.Transaccion.EvaluaPadronAgroperu,
                As400Constants.AplicacionFondoAgroperu,
                As400Constants.Banco12,
                CodigoPadron,
                CodigoPadron);
        }
        public (string IdResult, string CodigoPadron) GenerarCodigoPadron()
        {
            ParametricasData oPar = new ParametricasData();
            var Result = oPar.EjecutarProgramaT20000(
                As400Constants.Transaccion.GeneraCodigo, 
                As400Constants.AplicacionJR,
                As400Constants.Claves.CodigoPadron);
            
            return (Result.Substring(0,4), Result.Substring(4,12)) ;
        }

        public MemoryStream ObtenePadronResultado(ExcelResponseModel respon, int CodigoPadron)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODFILTRO", 1, CodigoPadron, 0, iDB2DbType.iDB2Integer);
            DataSet dt = new DataSet();
            
            _db2DataContext.RunStoredProcedure(StoreConstants.Padron.UP_PFA_SEL_RESULTADO_PADRON, out DataTable table1, lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Padron.UP_PFA_SEL_RESULTADO_PADRON_MOTIVOS, out DataTable table2, lDataParam);
            dt.Tables.Add(table1);
            dt.Tables.Add(table2);
            return ExportToExcel(dt);
        }
        public MemoryStream ExportToExcel(DataSet SetTables)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet1 = workbook.CreateSheet("Resultados");
            ISheet sheet2 = workbook.CreateSheet("Motivos");
            
            IRow headerRow1 = sheet1.CreateRow(0);
            IRow headerRow2 = sheet2.CreateRow(0);

            // Create Header Style
            ICellStyle headerCellStyle = workbook.CreateCellStyle();

            var font = workbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.FontName = "Calibri";
            //font.Color = IndexedColors.DarkGreen.Index;

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            headerCellStyle.SetFont(font);
            headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Blue.Index;
            //headerCellStyle.FillPattern = FillPattern.BigSpots;
            headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Pink.Index;
            headerCellStyle.FillBackgroundColor= IndexedColors.Orange.Index;
            //headerCellStyle.FillForegroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillPattern = FillPattern.SolidForeground;
            
            
            //HSSFColor lightGray = setColor(workbook, (byte)0xE0, (byte)0xE0, (byte)0xE0);
            //style2.setFillForegroundColor(lightGray.getIndex());
            //headerCellStyle.FillForegroundColor = g;
            //headerCellStyle.FillForegroundColor = HSSFColor.Lim
            //headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            // Create Date Style
            ICellStyle dateCellStyle = workbook.CreateCellStyle();
            dateCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy");

            ICellStyle numCellStyle = workbook.CreateCellStyle();
            numCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.000");


            ICellStyle cellStyle = workbook.CreateCellStyle();
            IDataFormat hssfDataFormat = workbook.CreateDataFormat();
            //String cellVal = "2500";
            //cellStyle.setDataFormat(hssfDataFormat.getFormat("#,##0.000"));
            //numCellStyle.setCellStyle(cellStyle);
            //numCellStyle.setCellValue(new Double(cellVal));
            //numCellStyle.setCellType(Cell.CELL_TYPE_NUMERIC);

            // Build Header

            int i = 0;
            // Build Details (rows)
            int rowIndex = 1;
            int sheetIndex = 1;
            const int maxRows = 65536;

            foreach (DataColumn column in SetTables.Tables[0].Columns)
            {
                ICell headerCell = headerRow1.CreateCell(column.Ordinal);
                headerCell.SetCellValue(column.ColumnName);

                if (i == 1 || i == 2)
                {
                    headerCell.CellStyle = numCellStyle;
                }
                else
                {
                    headerCell.CellStyle = headerCellStyle;
                }
                i++;
            }

            

            foreach (DataRow row in SetTables.Tables[0].Rows)
            {
                // Start new sheet max rows reached
                if (rowIndex % maxRows == 0)
                {
                    // Auto size columns on current sheet
                    for (int h = 0; h < headerRow1.LastCellNum; h++)
                    {
                        sheet1.AutoSizeColumn(h);
                    }

                    sheetIndex++;
                    sheet1 = workbook.CreateSheet("Resultados" + sheetIndex);
                    IRow additionalHeaderRow = sheet1.CreateRow(0);

                    for (int h = 0; h < headerRow1.LastCellNum; h++)
                    {
                        ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                        additionalHeaderColumn.CellStyle = headerRow1.GetCell(h).CellStyle;
                        additionalHeaderColumn.SetCellValue(headerRow1.GetCell(h).RichStringCellValue);
                    }

                    rowIndex = 1;
                }

                // Create new row in sheet
                IRow dataRow = sheet1.CreateRow(rowIndex);

                foreach (DataColumn column in SetTables.Tables[0].Columns)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);

                    switch (column.DataType.FullName)
                    {
                        case "System.String":
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                        case "System.Int":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            double val;
                            dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                            break;
                        case "System.DateTime":
                            DateTime dt = new DateTime(1900, 01, 01);
                            DateTime.TryParse(row[column].ToString(), out dt);

                            dataCell.SetCellValue(dt);
                            dataCell.CellStyle = dateCellStyle;
                            break;
                        default:
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                    }
                }

                rowIndex++;
            }

            if (SetTables.Tables.Count > 1)
            {
                i = 0;
                // Build Details (rows)
                rowIndex = 1;
                sheetIndex = 1;

                foreach (DataColumn column in SetTables.Tables[1].Columns)
                {
                    ICell headerCell = headerRow2.CreateCell(column.Ordinal);
                    headerCell.SetCellValue(column.ColumnName);

                    if (i == 1 || i == 2)
                    {
                        headerCell.CellStyle = numCellStyle;
                    }
                    else
                    {
                        headerCell.CellStyle = headerCellStyle;
                    }
                    i++;
                }

                foreach (DataRow row in SetTables.Tables[1].Rows)
                {
                    // Start new sheet max rows reached
                    if (rowIndex % maxRows == 0)
                    {
                        // Auto size columns on current sheet
                        for (int h = 0; h < headerRow2.LastCellNum; h++)
                        {
                            sheet2.AutoSizeColumn(h);
                        }

                        sheetIndex++;
                        sheet2 = workbook.CreateSheet("Resultados" + sheetIndex);
                        IRow additionalHeaderRow = sheet2.CreateRow(0);

                        for (int h = 0; h < headerRow2.LastCellNum; h++)
                        {
                            ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                            additionalHeaderColumn.CellStyle = headerRow2.GetCell(h).CellStyle;
                            additionalHeaderColumn.SetCellValue(headerRow2.GetCell(h).RichStringCellValue);
                        }

                        rowIndex = 1;
                    }

                    // Create new row in sheet
                    IRow dataRow = sheet2.CreateRow(rowIndex);

                    foreach (DataColumn column in SetTables.Tables[1].Columns)
                    {
                        ICell dataCell = dataRow.CreateCell(column.Ordinal);

                        switch (column.DataType.FullName)
                        {
                            case "System.String":
                                dataCell.SetCellValue(row[column].ToString());
                                break;
                            case "System.Int":
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Double":
                            case "System.Decimal":
                                double val;
                                dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                                break;
                            case "System.DateTime":
                                DateTime dt = new DateTime(1900, 01, 01);
                                DateTime.TryParse(row[column].ToString(), out dt);

                                dataCell.SetCellValue(dt);
                                dataCell.CellStyle = dateCellStyle;
                                break;
                            default:
                                dataCell.SetCellValue(row[column].ToString());
                                break;
                        }
                    }

                    rowIndex++;
                }
            }
            


            MemoryStream ms = new MemoryStream();

            for (int h = 0; h < headerRow1.LastCellNum; h++)            
                sheet1.AutoSizeColumn(h);

            if (SetTables.Tables.Count > 1)
                for (int h = 0; h < headerRow2.LastCellNum; h++)
                    sheet2.AutoSizeColumn(h);

            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;
            return ms;
            //ExportToExcelx(workbook, fileName);
        }

        public List<GrupoIntegrante> LeerExcelPadron(ref ExcelResponseModel respon, Stream stream, string fileExtension)
        {
            var Fila = ApplicationKeys.ExcelPadronAgroperuStartRow;
            try
            {
                List<GrupoIntegrante> LstIntegrantes = new List<GrupoIntegrante>();
                var excelData = new ExcelDataAccess(stream, fileExtension);

                if (excelData.SheetName != ApplicationKeys.ExcelPadronAgroperuSheetName)
                {
                    respon.code = "10";
                    respon.message = "Plantilla de padrón no valida";
                    throw new System.ArgumentException(respon.message, "codigo plantilla");
                }

                string version = excelData.GetCell(0, 0).value;
                if (version != ApplicationKeys.ExcelPadronAgroperuVersion)
                {
                    respon.code = "11";
                    respon.message = "Descarge la versión actual de la plantilla";
                    throw new System.ArgumentException(respon.message);
                }

                
                IRow rw = excelData.GetRow(Fila);
                
                while (ValidarRegistro(rw))
                {   
                    var objCliente = new Persona
                    {
                        TipoDocumento = excelData.GetCell(Fila, 0),
                        NroDocumento = excelData.GetCell(Fila, 1),
                        ApePaterno = excelData.GetCell(Fila, 2),
                        ApeMaterno = excelData.GetCell(Fila, 3),
                        NomPrimer = excelData.GetCell(Fila, 4),
                        NomSegundo = excelData.GetCell(Fila, 5),
                        Direccion = excelData.GetCell(Fila, 6),
                        Sexo = excelData.GetCell(Fila, 7),
                        EstadoCivil = excelData.GetCell(Fila, 8),
                        Celular = excelData.GetCell(Fila, 9),
                        WhatsApp = excelData.GetCell(Fila, 10),
                        Experiencia = excelData.GetCell(Fila, 11),
                        NacimientoDia = excelData.GetCell(Fila, 12),
                        NacimientoMes = excelData.GetCell(Fila, 13),
                        NacimientoAnio = excelData.GetCell(Fila, 14),
                        Correo = excelData.GetCell(Fila, 37)
                    };
                    var objConyugue = new Persona
                    {
                        TipoDocumento = excelData.GetCell(Fila, 15),
                        NroDocumento = excelData.GetCell(Fila, 16),
                        ApePaterno = excelData.GetCell(Fila, 17),
                        ApeMaterno = excelData.GetCell(Fila, 18),
                        NomPrimer = excelData.GetCell(Fila, 19),
                        NomSegundo = excelData.GetCell(Fila, 20)
                    };
                    var objPredio = new Predio
                    {
                        Departamento = excelData.GetCell(Fila, 21),
                        Provincia = excelData.GetCell(Fila, 22),
                        Distrito = excelData.GetCell(Fila, 23),
                        Referencia = excelData.GetCell(Fila, 24),
                        RegimenTenencia = excelData.GetCell(Fila, 25),
                        UnidadCatastral = excelData.GetCell(Fila, 26)
                    };
                    var objSolicitud = new Solicitud
                    {
                        Sector = excelData.GetCell(Fila, 27),
                        Programa = excelData.GetCell(Fila,28),
                        TipoUnidadFinanciamiento = excelData.GetCell(Fila, 29),
                        NroUnidadesTotales = excelData.GetCell(Fila, 30),
                        NroUnidadesFinanciamiento = excelData.GetCell(Fila, 31),
                        NroUnidadesConduccion = excelData.GetCell(Fila, 32),
                        Cultivo = excelData.GetCell(Fila, 33),
                        NivelTecnologico = excelData.GetCell(Fila, 34),
                        MontoSolicitadoSoles = excelData.GetCell(Fila, 35),                        
                        EnvioCronograma = excelData.GetCell(Fila, 36),
                        // Correo 
                        PagoAdelantado = excelData.GetCell(Fila, 38)                        
                    };
                    var objInformeVisita = new InformeVisita
                    {
                        Plazo = excelData.GetCell(Fila, 39),
                        FechaSiembraOriginal = excelData.GetCell(Fila, 40),
                        FechaSiembra = cambiarFecha(excelData.GetCell(Fila, 40))
                    };

                    LstIntegrantes.Add(new GrupoIntegrante
                    {
                        DatosCliente = objCliente,
                        DatosConyugue = objConyugue,
                        DatosPredio = objPredio,
                        DatosSolicitud = objSolicitud,                        
                        DatosInformeVisita = objInformeVisita
                    });

                    Fila = Fila + 1;
                    rw = excelData.GetRow(Fila);
                }

                return LstIntegrantes;

            }
            catch (Exception e)
            {
                if (respon.code=="") {
                    respon.code = "10";
                    respon.message = e.Message;
                }
                Logger.LogException(e, $"Leer excel FILA: {Fila}");
                throw;
            }

        }
        private bool ValidarRegistro(IRow rw)
        {
            if (rw is null)
                return false;

            foreach (var item in rw.Cells)
                if (item.GetCellValue().Trim() != "")
                    return true;

            return false;
        }
        public Cell cambiarFecha(Cell dato)
        {

            if (dato.value != "")
            {
                if (dato.value.ToString().Contains("/"))
                {
                    dato.value = dato.value.Substring(6, 4) + dato.value.Substring(3, 2) + dato.value.Substring(0, 2);
                }

            }
            return dato;
        }



    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using IBM.Data.DB2.iSeries;
using System.Security.Cryptography;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class ParametricasData
    {
        private readonly Db2DataAccess _db2DataContext;
        public ParametricasData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        public string EjecutarProgramaT20000(string Transaccion, string Aplicacion, string Parametro)
        {

            TramaAS400 oTrama = new TramaAS400(Transaccion, Aplicacion);
            var lDataParam = new iDB2Parameter[2];

            lDataParam[0] = _db2DataContext.CreateParameter("@PARAM1", 336, oTrama.e_par1, direction: ParameterDirection.InputOutput);
            lDataParam[1] = _db2DataContext.CreateParameter("@PARAM2", 30000, Parametro, direction: ParameterDirection.InputOutput);


            Logger.LogInformation($"LLama al programa: { DateTime.Now } : { DateTime.Now.Millisecond }");

            _db2DataContext.RunRPGLE(As400Constants.MonitorTransacciones, lDataParam, "@PARAM2", out string Result);

            Logger.LogInformation($"Fin de llamada : { DateTime.Now } : { DateTime.Now.Millisecond }");

            return Result;

        }
        public string EjecutarProgramaT20000(string Transaccion, string Aplicacion, string Empresa, string Filler, string Parametro)
        {

            TramaAS400 oTrama = new TramaAS400(Transaccion, Aplicacion, Empresa, Filler);
            var lDataParam = new iDB2Parameter[2];

            lDataParam[0] = _db2DataContext.CreateParameter("@PARAM1", 336, oTrama.e_par1, direction: ParameterDirection.InputOutput);
            lDataParam[1] = _db2DataContext.CreateParameter("@PARAM2", 30000, Parametro, direction: ParameterDirection.InputOutput);


            Logger.LogInformation($"LLama al programa: { DateTime.Now } : { DateTime.Now.Millisecond }");

            _db2DataContext.RunRPGLE(As400Constants.MonitorTransacciones, lDataParam, "@PARAM2", out string Result);

            Logger.LogInformation($"Fin de llamada : { DateTime.Now } : { DateTime.Now.Millisecond }");

            return Result;

        }

        // Metodos Paramétricos

        public List<ParametricasModel> ListarComisionRegante(string CodJuntaRegante)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_COD_JUNTA_REGANTE", 2, CodJuntaRegante);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_SEL_COMSIONX_JUNTA_REGANTE, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR"),
                    flag = reader.GetString("FLAG")
                });
            }
            return listReturn;
        }
        
        public List<ParametricasModel> ListarParamtricasGrupo(int codOficinaRegional)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_OFI_REG", 4, codOficinaRegional, 0, iDB2DbType.iDB2Integer);
            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_SEL_PARAMETRICAS_GRUPO, out IDataReader reader,lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarParamtricasMatrizCosto()
        {
            var listReturn = new List<ParametricasModel>();
            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_SEL_PARAMETRICAS_MATRIZ_COSTOS, out IDataReader reader);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarUbigeos(string ubigeo)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_UBIGEO", 6, ubigeo);

            _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_MEG_SEL_UBIGEOS_XDEPARTAMENTO, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarDepartamentos()
        {
            var listReturn = new List<ParametricasModel>();
            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_DEPARTAMENTOS, out IDataReader reader);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarProvincias(string CodDepartamento)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_NU_IDDEPART", 2, CodDepartamento);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_PROVINCIAS, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarDistritos(string CodProvincia)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_NU_IDPROVIN", 4, CodProvincia);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_DISTRITOS, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarAgencias(int CodAgencia)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia, 0, iDB2DbType.iDB2Integer);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_AGENCIAS, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    //key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> Listarfuncionarios(int CodAgencia)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia, 0, iDB2DbType.iDB2Integer);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_FUNCIONARIOS, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    //key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarOrganizaciones(int CodAgencia)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 4, CodAgencia, 0, iDB2DbType.iDB2Integer);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_PRM_SEL_ORGANIZACIONES, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    //key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarComisiones(string CodigoOrganizacion)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];

            lDataParam[0] = _db2DataContext.CreateParameter("VI_ORGANIZACION", 2, CodigoOrganizacion);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_PRM_SEL_COMISIONES_POR_ORGANIZACION, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR"),
                    flag = reader.GetString("FLAG"),
                    trama = reader.GetString("TRAMA")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarProductos(string CodigoSector)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_SECTOR", 5, CodigoSector);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_CULTIVOS_POR_SECTOR, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarProductos(string CodigoSector, string CodigoPrograma)
        {
            var listReturn = new List<ParametricasModel>();
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_SECTOR", 4, CodigoSector);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_PROGRAMA", 4, CodigoPrograma);

            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_PFA_SEL_PRODUCTO_POR_SECTOR_PROGRAMA, out IDataReader reader, lDataParam);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR"),
                    flag = reader.GetString("FLAG")
                });
            }
            return listReturn;
        }

        public List<ParametricasModel> ListarCultivos()
        {
            var listReturn = new List<ParametricasModel>();
            _db2DataContext.RunStoredProcedure(StoreConstants.Parametricas.UP_UTIL_SEL_CULTIVOS, out IDataReader reader);
            while (reader.Read())
            {
                listReturn.Add(new ParametricasModel
                {
                    key = reader.GetString("CLAVE"),
                    code = reader.GetString("CODIGO"),
                    value = reader.GetString("VALOR")
                });
            }
            return listReturn;
        }

    }
}

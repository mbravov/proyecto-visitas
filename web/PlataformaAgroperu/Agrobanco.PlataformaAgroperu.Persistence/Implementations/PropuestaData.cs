﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class PropuestaData
    {
        private readonly Db2DataAccess _db2DataContext;
        public PropuestaData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        public string GenerarPropuesta(string NumeroSolicitud)
        {
            ParametricasData oPar = new ParametricasData();
            return oPar.EjecutarProgramaT20000(
                Transaccion: As400Constants.Transaccion.GenerarPropuestaAgroperu,
                Aplicacion: As400Constants.AplicacionFondoAgroperu,
                Empresa: As400Constants.Banco12,
                Filler: "",
                Parametro: NumeroSolicitud.PadLeft(12, '0'));
        }


        public List<InstanciaEvaluacion> ListarInstanciaEvaluacion(string CodSolicitud)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = new iDB2Parameter("VI_NROSOLICITUD", CodSolicitud) { Direction = ParameterDirection.Input };
            List<InstanciaEvaluacion> linstancias = new List<InstanciaEvaluacion>();
            _db2DataContext.RunStoredProcedure("AGRCYFILES.UP_MEG_SEL_PROPUESTA_INSTANCIA_COMENTARIO", out IDataReader reader2, lDataParam);

            while (reader2.Read())
            {
                var instancia = new InstanciaEvaluacion
                {
                    Comentario = reader2.GetString("COMENTARIO"),
                    Usuario = reader2.GetString("CNODSC")
                };
                linstancias.Add(instancia);
            }
            reader2.Close();
            return linstancias;
        }
        public string ConvertirNumero(string valor)
        {
            return String.Format("{0,12:N2}", double.Parse(valor));
        }
        public Propuesta ConsultarPropuestaInfo(ref ExcelResponseModel respon, string CodSolicitud)
        {
            Propuesta oPropuesta = null;
            try
            {
                var lDataParam = new iDB2Parameter[1];
                //lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 2, CodSolicitud);
                lDataParam[0] = _db2DataContext.CreateParameter("VI_SOLICITUD", 12, CodSolicitud, 0, iDB2DbType.iDB2VarChar);
                _db2DataContext.RunStoredProcedure(StoreConstants.Agrupamiento.UP_PFA_SEL_PROPUESTA_INTEGRANTE, out IDataReader reader, lDataParam);
                oPropuesta = new Propuesta();
                if (reader.Read())
                {
                    oPropuesta.NroGrupo_00 = reader.GetString("MFICOF");
                    oPropuesta.EsPEP = reader.GetString("MFICLP");
                    oPropuesta.NroPropuesta_01 = reader.GetString("DPCSOL");
                    oPropuesta.FechaGenPropuesta_02 = reader.GetString("DPCFGP");
                    oPropuesta.Estado_03 = reader.GetString("DPCSITD");
                    oPropuesta.Agencia_04 = reader.GetString("DPCBRND");
                    oPropuesta.Autonomia_05 = reader.GetString("DPCATN");//blanco
                    oPropuesta.Nombre_RzSocial_06 = reader.GetString("DPCNBM");
                    oPropuesta.TipoDocumento_07 = reader.GetString("DPCIDN");
                    oPropuesta.Cliente_08 = reader.GetString("DPCFRC");
                    oPropuesta.Codigo_09 = reader.GetString("DPCCUN");
                    oPropuesta.Edad_10 = reader.GetString("DPCEDD");
                    oPropuesta.Departamento_11 = reader.GetString("DPCUBIDE");//no hay con este nombre
                    oPropuesta.Provincia_12 = reader.GetString("DPCUBIPR");
                    oPropuesta.Distrito_13 = reader.GetString("DPCUBIDI");
                    oPropuesta.Comision_14 = reader.GetString("COMNMBC");//BLANCO
                    oPropuesta.Dir_Predio_14_1 = reader.GetString("DPCREF");//BLANCO
                    oPropuesta.Dir_solicitante_14_2 = reader.GetString("DPCDCS");//BLANCO
                    oPropuesta.Producto_15 = reader.GetString("DPCCULD");
                    oPropuesta.Destino_16 = reader.GetString("DPCDESD");
                    oPropuesta.MontoSolicitado_17 = ConvertirNumero(reader.GetString("DPCMCL")); //String.Format("{0,12:N3}", double.Parse(reader.GetString("DPCMCL")));
                    oPropuesta.Plazo_18 = reader.GetString("DPCPLZ");
                    oPropuesta.FormaPago_19 = reader.GetString("DPCFRPD");
                    oPropuesta.Moneda_20 = reader.GetString("DPCMND");
                    oPropuesta.Tasa_21 = reader.GetString("DPCINC");
                    oPropuesta.ProductoIBS_22 = reader.GetString("DPCPRD");//BLANCO
                    oPropuesta.SegDegravamen_23 = reader.GetString("DPCSDD");
                    oPropuesta.SegAgricola_24 = reader.GetString("DPCSAF");
                    oPropuesta.Gestor_25 = reader.GetString("DPCGST");
                    oPropuesta.PorcentajeComisionGestor_26 = reader.GetString("DPCPGS");
                    oPropuesta.ServicioGestor_27 = reader.GetString("DPCSGS");
                    oPropuesta.NumGarantia_28 = " ";// reader.GetString("DPCNGR");
                    oPropuesta.Gar_Tipo_29 = reader.GetString("DPCTGR");
                    oPropuesta.Gar_Tipo_29_1 = reader.GetString("DPCTGR1");
                    oPropuesta.Gar_Tipo_29_2_AVA = reader.GetString("DPCTAVA");
                    // oPropuesta.Gar_Descripcion_30_1_AVA = reader.GetString("DPCTAVA");
                    oPropuesta.Gar_Descripcion_30 = reader.GetString("DPCDGR");
                    oPropuesta.Gar_Descripcion_30_1 = reader.GetString("DPCDGR1");
                    oPropuesta.Gar_VRI_32 = ConvertirNumero(reader.GetString("DPCIVR"));
                    oPropuesta.Gar_VRI_32_1 = ConvertirNumero(reader.GetString("DPCIVR1"));
                    oPropuesta.Gar_Poliza_34 = reader.GetString("DPCFPL");
                    oPropuesta.Gar_Poliza_34_1 = reader.GetString("DPCFPL1");
                    //oPropuesta.Gar_Descripcion_30_1_AVA = reader.GetString("DPCDAVA").TrimEnd() + "/" + reader.GetString("DPCANOM").TrimEnd() + " " + reader.GetString("DPCTDAV").TrimEnd() + ":" + reader.GetString("DPCNDAV").TrimEnd();
                    oPropuesta.Gar_Descripcion_30_1_AVA = reader.GetString("DPCDAVA").TrimEnd();
                    oPropuesta.Gar_Nombre_Avalador_30_3_AVA = reader.GetString("DPCANOM").TrimEnd();
                    oPropuesta.Gar_Documento_30_2_AVA = reader.GetString("DPCTDAV").TrimEnd() + " : " + reader.GetString("DPCNDAV").TrimEnd();

                    oPropuesta.Gar_VG_31 = " ";// reader.GetString("DPCIVG");

                    oPropuesta.Gar_FTasacion_33 = " ";// reader.GetString("DPCFTS");

                    oPropuesta.Gar_NroPartida_35 = " ";// reader.GetString("DPCNPR");
                    oPropuesta.Gar_OfRegistral_36 = " ";// reader.GetString("DPCOFR");
                    //oPropuesta.Desembolso_Fini_37 = reader.GetString("DPCIFD");
                    //oPropuesta.Desembolso_Fini_37_1 = reader.GetString("DPCIFD1");
                    //oPropuesta.Desembolso_Ffin_38 = reader.GetString("DPCFFD");
                    //oPropuesta.Desembolso_Ffin_38_1 = reader.GetString("DPCFFD1");
                    //oPropuesta.Gar_Etapa_39 = reader.GetString("DPCETD");
                    //oPropuesta.Gar_Etapa_39_1 = reader.GetString("DPCETD1");
                    //oPropuesta.Gar_Porcentaje_40 = reader.GetString("DPCPCD");
                    //oPropuesta.Gar_Porcentaje_40_1 = reader.GetString("DPCPCD1");
                    //oPropuesta.Gar_FinxUP_41 = reader.GetString("DPCFUP");
                    //oPropuesta.Gar_FinxUP_41_1 = reader.GetString("DPCFUP1");
                    //oPropuesta.Gar_UP_42 = reader.GetString("DPCUDP0");
                    //oPropuesta.Gar_UP_42_1 = reader.GetString("DPCUDP1");
                    //oPropuesta.Gar_TotalFin_43 = ConvertirNumero(reader.GetString("DPCTFET"));
                    //oPropuesta.Gar_TotalFin_43_1 = ConvertirNumero(reader.GetString("DPCTFE1T"));
                    oPropuesta.CondicionesEspeciales_44 = " ";// BORRADO reader.GetString("DPCCED");
                    oPropuesta.Comentarios1_45 = " "; // BORRADO reader.GetString("DPCC1D");
                    oPropuesta.Comentarios2_46 = " ";// BORRADO reader.GetString("DPCC2D");
                    oPropuesta.Fecha_47 = reader.GetString("PROFDS");
                    oPropuesta.Aprobado_48 = reader.GetString("RESOL");
                    oPropuesta.Nro_acta_49 = reader.GetString("DPCNAC");
                    oPropuesta.Autonomia_51 = reader.GetString("DPCATN");                    
                    oPropuesta.Analista_53 = reader.GetString("DPCCAD");
                    oPropuesta.Mora_54 = reader.GetString("DPCFMA");
                    oPropuesta.P_R_REF_55 = reader.GetString("DPCFCA");
                    oPropuesta.Mora_56 = reader.GetString("DPCFMG");
                    oPropuesta.P_R_REF_57 = reader.GetString("DPCFCG");
                    oPropuesta.Solicitante_58 = reader.GetString("DPCNBM");
                    oPropuesta.Calif_SBS_59 = reader.GetString("CAL_TIT");// reader.GetString("CNODSC");//ANTES DPCCST
                    oPropuesta.Calif_Interna_60 = reader.GetString("DPCCBT");
                    oPropuesta.NumeroEntidades_61 = reader.GetString("DPCNET");
                    oPropuesta.RLAFT_62 = reader.GetString("DPCRLT");
                    oPropuesta.Patrimonio_63 = reader.GetString("DPCPTT");
                    oPropuesta.OtrosIngresos_64 = ConvertirNumero(reader.GetString("DPCOIT"));
                    oPropuesta.ExpoPropuesta_65 = ConvertirNumero(reader.GetString("DPCEPT"));
                    oPropuesta.ExposicionVigente_66 = ConvertirNumero(reader.GetString("DPCEVT"));
                    oPropuesta.ExposicionTotal_67 = ConvertirNumero(reader.GetString("DPCETT"));
                    oPropuesta.DeudaSistema_68 = ConvertirNumero(reader.GetString("DPCDST"));
                    oPropuesta.Conyuguea_69 = reader.GetString("DPCNMC");
                    oPropuesta.Califsbs_70 = reader.GetString("CAL_CON");
                    oPropuesta.Caliinterna_71 = reader.GetString("DPCCBC");
                    oPropuesta.NumeroEntidades_72 = reader.GetString("DPCNEC");
                    oPropuesta.Deudasistema_73 = reader.GetString("DPCDSC");
                    oPropuesta.Producto_74 = reader.GetString("DPCCULD"); //ANTES DPCCUL
                    oPropuesta.ExpSector_75 = reader.GetString("DPCEXS");
                    oPropuesta.UnidadProd_76 = reader.GetString("DPCUDP");
                    oPropuesta.UP_Total_77 = reader.GetString("DPCUPT");
                    oPropuesta.UP_Financiar_78 = reader.GetString("DPCUPF");
                    oPropuesta.CalificacionLAFT_78A = reader.GetString("LAFT"); // AGREGA
                    oPropuesta.ProduccionxUP_79 = reader.GetString("DPCPUP");
                    oPropuesta.UnidadMedida_80 = reader.GetString("DPCUMP");
                    oPropuesta.ProduccionTotal_81 = reader.GetString("DPCPRT");
                    oPropuesta.Precio_82 = reader.GetString("DPCPUM");
                    oPropuesta.Costo_83 = reader.GetString("DPCCUM");
                    oPropuesta.Ingreso_84 = reader.GetString("DPCING");
                    oPropuesta.Costo_85 = reader.GetString("DPCIMC");
                    oPropuesta.BeneficioCosto_86 = reader.GetString("DPCIBC");
                    oPropuesta.HASposee_87 = reader.GetString("DPCHSP");
                    oPropuesta.HASpermiso_agua_88 = reader.GetString("DPCHPA");
                    oPropuesta.HASriego_cultivo_89 = reader.GetString("DPCHCL");
                    oPropuesta.HASfinanciadas_90 = reader.GetString("DPCHCF");

                    oPropuesta.PagoAsistenteTecnico = reader.GetString("PAGO_AT");
                    oPropuesta.AprobadoExcepcion = reader.GetInt("EXCEPCION");
                    oPropuesta.ComentarioAnalista = reader.GetString("COMENTARIO");
                    oPropuesta.ObservacionJefe = reader.GetString("OBSERVACION");
                    oPropuesta.PlanDesembolsos = new List<PlanDesembolso>();
                    if (reader.NextResult())
                    {
                        
                        while (reader.Read())
                        {
                            var Plan = new PlanDesembolso
                            {
                                Solicitud = reader.GetString("SOLICITUD"),
                                NumeroDesembolso = reader.GetInt("NUM_DESEMB"),
                                FechaInicio = reader.GetString("INICIO"),
                                FechaFin = reader.GetString("FIN"),
                                DescripcionEtapa = reader.GetString("ETAPA"),
                                Porcentaje = reader.GetString("PORCENTAJE"),
                                TotalFinanciado = ConvertirNumero(reader.GetString("TOTAL_FINANCIADO"))
                            };
                            oPropuesta.PlanDesembolsos.Add(Plan);
                        }
                    }
                    reader.Close();
                }

                oPropuesta.linstancias = ListarInstanciaEvaluacion(CodSolicitud);
            }
            catch (Exception ex)
            {
                respon.code = "10";
                respon.message = "SP:ConsultarPropuestaInfo:" + ex.Message;
            }
            return oPropuesta;
        }
    }
}

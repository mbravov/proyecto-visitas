﻿using Agrobanco.PlataformaAgroperu.Domain.Models;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Prospecto;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using IBM.Data.DB2.iSeries;
using NPOI.HSSF.UserModel;

using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class ProspectoData
    {
        private readonly Db2DataAccess _db2DataContext;
        public ProspectoData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }
        public List<ParametricasModel> ConsultarCalifSBSxFiltro(string idFiltro)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODFILTRO", 8, idFiltro, 0, iDB2DbType.iDB2Integer);
            Logger.LogInformation("Inicio: Consulta resumen filtro SBS");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Prospecto.UP_MEG_SEL_RESUM_CALIF_SBS_PROSPECTO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta resumen filtro SBS");
            
            List<ParametricasModel> lstCalificaciones = new List<ParametricasModel>();
            ParametricasModel calificacion = null;
            while (reader.Read())
            {
                calificacion = new ParametricasModel();
                calificacion.key = reader.GetString("CALIFSBS");
                calificacion.value = reader.GetInt("CONT").ToString();
                lstCalificaciones.Add(calificacion);
            }
            return lstCalificaciones;
        }

        public void RegistrarProspecto(ref ExcelResponseModel respon, Prospecto Prospecto, Usuario usuario, string FechaCliente)
        {
            var parameters = new iDB2Parameter[10];
            ParametricasData oPar = new ParametricasData();
            // parameters[0] = _db2DataContext.CreateParameter("VI_CAB_AGR", 64000, "NOMBGRUPO|1|NOMJREGANT|2|NOMCOMIS|3|NOMCOMIT|BA|FORM|DEST|40|APAG|3|PROD|CIIU|CULTI|SEGD|3.0|COMP|4.0|0000|00|00|1|USU|*|", 0, iDB2DbType.iDB2Clob);
            // parameters[1] = _db2DataContext.CreateParameter("VI_DET_AGR", 64000, "CTIT|DOCTIT|APEPATE|APEMATE|NOMBRE1|NOMBRE2|S|E|2334|2|12|2|1978|TIP|3232|0101|0102|0101|DERFRFR|TENEN|UNDCAT|1|UND|2|1|3|E|@MAIL|P|TGAR|1200|F|TGAR|1300|F|*|", 0, iDB2DbType.iDB2Clob);
            // parameters[0] = _db2DataContext.CreateParameter("VI_CAB_AGR", 64000, "DEDE|841|XX|0|XX|0|XX|9|1|01|1.0|SCH|12|LIV1|0121|010061|SEGD|10.0|SEGA|20.0|18|1801|180101|1|prueba|*|", 0, iDB2DbType.iDB2Clob);
            // parameters[1] = _db2DataContext.CreateParameter("VI_DET_AGR", 64000, "DNI|10765799|guevara|tinoco|paul|mario|M|1|993599221|1978|2|15|DNI|100000|01|0101|010101|refe 1|CAAP|nose|1|CG|3|4|4500|5|1|email@gmail.com|1|HPCO|300|S|GHTN|2000|S|*|DNI|90330330|gamarra|perez|jose|marco|M|1|859659662|1977|4|1|DNI|200000|03|0304|030406|derrfrf|CA1V|frgttgtgh|3|CG|2|2|2000|3|1|email@gmail.com|2|HHTN|200|S|HHTN|10000|S|*|", 0, iDB2DbType.iDB2Clob);
            //string codrsp = oPar.ObtenerCodigo("JRCAGR");//"0000123456789012";
            var codrsp = GenerarCodigoProspecto();

            if (codrsp.IdResult == "0000")
            {
                
                parameters[0] = _db2DataContext.CreateParameter("VI_TIMCLI", 50, FechaCliente, 0, iDB2DbType.iDB2VarChar);
                parameters[1] = _db2DataContext.CreateParameter("VI_CODAS400", 12, codrsp.CodigoProspecto, 0, iDB2DbType.iDB2VarChar);
                parameters[2] = _db2DataContext.CreateParameter("VI_NROFIL", 1, Prospecto.ExcelIntegrantes.Count, 0, iDB2DbType.iDB2Integer);

                parameters[3] = _db2DataContext.CreateParameter("VI_NOMFILTRO", 100, Prospecto.DatosProspecto.Nombre, 0, iDB2DbType.iDB2VarChar);
                parameters[4] = _db2DataContext.CreateParameter("VI_CODAGENTE", 4, usuario.CodigoFuncionario);
                parameters[5] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 4, usuario.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                parameters[6] = _db2DataContext.CreateParameter("VI_NIVEL", 1, 1, 0, iDB2DbType.iDB2Integer);

                parameters[7] = _db2DataContext.CreateParameter("VI_USUARIO", 10, usuario.User, 0, iDB2DbType.iDB2VarChar);
                parameters[8] = _db2DataContext.CreateParameter("VI_DET_AGR", 64000, Prospecto.oTrama, 0, iDB2DbType.iDB2Clob);

                parameters[9] = _db2DataContext.CreateParameter("VO_MSG", 150, "", 0, iDB2DbType.iDB2VarChar);
                parameters[9].Direction = System.Data.ParameterDirection.Output;
                Logger.LogInformation("Registrando Prospecto");
                Logger.LogDbParameters(parameters);
                _db2DataContext.RunStoredProcedure(StoreConstants.Prospecto.UP_MEG_INS_EXCEL_PROSPECTO, parameters);
                Logger.LogInformation("Fin Registrando Prospecto");
                string msg = parameters[9].Value.ToString();
                respon = new ExcelResponseModel(msg);
            }
            else {
                string msg = "Error al generar codigo";
                respon = new ExcelResponseModel(msg);
            }
            
        }

        public (string IdResult, string CodigoProspecto) GenerarCodigoProspecto()
        {
            ParametricasData oPar = new ParametricasData();
            var Result = oPar.EjecutarProgramaT20000(
                As400Constants.Transaccion.GeneraCodigo,
                As400Constants.AplicacionJR,
                "TCPTCF");

            return (Result.Substring(0, 4), Result.Substring(4, 12));
        }

        public List<ProspectoDatos> ConsultarProspectosPorAgenciaUsuario(string CodAgencia, string CodFuncionario)
        {
            var lstProspectos = new List<ProspectoDatos>();

            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 8, CodAgencia, 0, iDB2DbType.iDB2Integer);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_CODFUNCIONARIO", 10, CodFuncionario, 0, iDB2DbType.iDB2VarChar);
            Logger.LogInformation("Inicio: Consulta Lista de Prospectos");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Prospecto.UP_MEG_SEL_PROSPECTOSX_AGENCIA_USUARIO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Prospectos");

            ProspectoDatos prospecto = null;
            while (reader.Read())
            {
                prospecto = new ProspectoDatos();
                prospecto.CodigoProspecto = reader.GetInt("COD_PROSPECTO");
                prospecto.Nombre = reader.GetString("NOMBRE_PROSPECTO");
                prospecto.NroRegistros = reader.GetInt("NRO_REGISTROS");
                prospecto.Estado = reader.GetInt("ESTADO");
                prospecto.FechaCreacion = reader.GetString("FECHA_CREACION");
                lstProspectos.Add(prospecto);
            }
            return lstProspectos;
        }

        public Prospecto LeerExcelProspecto(ref ExcelResponseModel respon, string NombreProspecto, Stream fileExcel, string fileExtension, string nomFile)
        {
            try
            {
                Prospecto excelProspecto = new Prospecto();

                ProspectoDatos datosProspecto = new ProspectoDatos();
                var excelData = new ExcelDataAccess(fileExcel, fileExtension, nomFile);
                //datosProspecto.Usuario = Usuario;
                datosProspecto.Nombre = NombreProspecto;
                //datosProspecto.Agencia = CodAgencia;
                excelProspecto.DatosProspecto = datosProspecto;
                if (excelData.SheetName != ApplicationKeys.PlantillaProspecto)
                {
                    respon.code = "10";
                    respon.message = "Plantilla de prospecto no valida";
                    throw new System.ArgumentException(respon.message, "codigo plantilla");
                }
                string vtrama = "";
                ProspectoIntegrante Integrante = null;
                List<ProspectoIntegrante> ListIntegrantes = new List<ProspectoIntegrante>();
                List<ProspectoIntegranteItem> ListIntegrantesItems = new List<ProspectoIntegranteItem>();
                //List<ProspectoIntegrante> repetidos = new List<ProspectoIntegrante>();
                Cell celda, celda1;
                var fila = 2;
                celda = excelData.GetCell(fila, 1);
                while (string.IsNullOrWhiteSpace(celda.value) == false)
                {
                    celda = excelData.GetCell(fila, 1);
                    celda1 = excelData.GetCell(fila, 2);
                    //for (var fila = 2; fila <= 7000; fila++)
                    //{
                    //    celda = excelData.GetCellRange(fila, 1);
                    //    if (string.IsNullOrWhiteSpace(celda.value) == true) { break; };
                    Integrante = new ProspectoIntegrante
                    {
                        TipoDocumento = excelData.GetCell(fila, 1),
                        NumeroDocumento = excelData.GetCell(fila, 2),
                        ApPaterno = excelData.GetCell(fila, 3),
                        ApMaterno = excelData.GetCell(fila, 4),
                        PrimerNombre = excelData.GetCell(fila, 5),
                        SegundoNombre = excelData.GetCell(fila, 6),
                        Celular = excelData.GetCell(fila, 7),
                        Cony_TipoDocumento = excelData.GetCell(fila, 8),
                        Cony_NumeroDocumento = excelData.GetCell(fila, 9),
                    };
                    fila = fila + 1;
                    if ((string.IsNullOrWhiteSpace(celda.value) == false) && (string.IsNullOrWhiteSpace(celda1.value) == false))
                    {
                        //if (ListIntegrantes.Contains(Integrante) == false)
                        //{
                        //    ListIntegrantes.Add(Integrante);
                        //}
                        //else
                        //{
                        //    repetidos.Add(Integrante);
                        //}
                        //if (fila>3) {

                        //lista = new List<ProspectoIntegrante>();
                        //lista.Add(Integrante);
                        //}
                        ListIntegrantes.Add(Integrante);
                        ListIntegrantesItems.Add(new ProspectoIntegranteItem(Integrante));
                        vtrama = vtrama + Integrante.getTrama() + "|*|";
                    }
                    //HashSet<ProspectoIntegrante> hSet = new HashSet<ProspectoIntegrante>(ListIntegrantes.ToArray());
               
                    //if (hSet.Contains(Integrante) == false)
                    //{
                    //    hSet.Add(Integrante);
                    //}
                    //else {
                    //    repetidos.Add(Integrante);
                    //}

                    //vtrama = vtrama + Integrante.getTrama() + "|*|";
                }
                excelProspecto.ExcelIntegrantes = ListIntegrantes;
                excelProspecto.Integrantes = ListIntegrantesItems;
                excelProspecto.oTrama = vtrama;

                //List<ProspectoIntegrante> l3 = ListIntegrantes.Except(lista, comparer).ToList();
                //var results = ListIntegrantes.Distinct();

                //var duplicates = ListIntegrantes.GroupBy(s => s.NumeroDocumento).SelectMany(grp => grp.Skip(1));
                //var myDistinctList = ListIntegrantes.GroupBy(i => i.NumeroDocumento.ToString().TrimEnd()).Select(g => g.First()).ToList();

                //var agrupacion = ListIntegrantes.GroupBy(x => x).Select(g => new { Text = g.Key.NumeroDocumento, Count = g.Count() }).ToList();
                //var count1 = ListIntegrantes.GroupBy(e => e.NumeroDocumento).Where(e => e.Count()>= 2);
    
                

                
                return excelProspecto;
            }
            catch (Exception e)
            {
                string captura = e.Message;
                respon.listErrors.Add(new ExcelErrors("11", captura, "", ""));
                //Console.WriteLine(e);
                throw;
            }         
        }

        public MemoryStream ObteneProspectoResultado(ExcelResponseModel respon, int CodigoProspecto)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODFILTRO", 1, CodigoProspecto, 0, iDB2DbType.iDB2Integer);
            DataSet dt = null;
            _db2DataContext.RunStoredProcedure(StoreConstants.Prospecto.UP_MEG_SEL_RESULTADO_PROSPECTO, out dt, lDataParam);
            return ExportToExcel(dt.Tables[0]);
        }

        public MemoryStream ExportToExcel(DataTable sourceTable)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Sheet1");
            IRow headerRow = sheet.CreateRow(0);

            // Create Header Style
            ICellStyle headerCellStyle = workbook.CreateCellStyle();

            var font = workbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.FontName = "Calibri";
            //font.Color = IndexedColors.DarkGreen.Index;

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            headerCellStyle.SetFont(font);
            headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Blue.Index;
            //headerCellStyle.FillPattern = FillPattern.BigSpots;
            //headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Pink.Index;
            //headerCellStyle.FillBackgroundColor= IndexedColors.Orange.Index;
            //headerCellStyle.FillForegroundColor = IndexedColors.Orange.Index;
            //headerCellStyle.FillPattern = FillPattern.SolidForeground;
            //headerCellStyle.FillBackgroundColorColor= IndexedColors.RoyalBlue.Index;
            // headerCellStyle.FillBackgroundColor;
            //HSSFColor lightGray = setColor(workbook, (byte)0xE0, (byte)0xE0, (byte)0xE0);
            //style2.setFillForegroundColor(lightGray.getIndex());
            //headerCellStyle.FillForegroundColor = g;
            //headerCellStyle.FillForegroundColor = HSSFColor.Lim
            //headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            // Create Date Style
            ICellStyle dateCellStyle = workbook.CreateCellStyle();
            dateCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("m/d/yy");

            ICellStyle numCellStyle = workbook.CreateCellStyle();
            numCellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("#,##0.000");


            ICellStyle cellStyle = workbook.CreateCellStyle();
            IDataFormat hssfDataFormat = workbook.CreateDataFormat();
            //String cellVal = "2500";
            //cellStyle.setDataFormat(hssfDataFormat.getFormat("#,##0.000"));
            //numCellStyle.setCellStyle(cellStyle);
            //numCellStyle.setCellValue(new Double(cellVal));
            //numCellStyle.setCellType(Cell.CELL_TYPE_NUMERIC);

            // Build Header
            int i = 0;
            foreach (DataColumn column in sourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.SetCellValue(column.ColumnName);

                if (i == 1 || i == 2)
                {
                    headerCell.CellStyle = numCellStyle;
                }
                else
                {
                    headerCell.CellStyle = headerCellStyle;
                }
                i++;
            }

            // Build Details (rows)
            int rowIndex = 1;
            int sheetIndex = 1;
            const int maxRows = 65536;

            foreach (DataRow row in sourceTable.Rows)
            {
                // Start new sheet max rows reached
                if (rowIndex % maxRows == 0)
                {
                    // Auto size columns on current sheet
                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        sheet.AutoSizeColumn(h);
                    }

                    sheetIndex++;
                    sheet = workbook.CreateSheet("Sheet" + sheetIndex);
                    IRow additionalHeaderRow = sheet.CreateRow(0);

                    for (int h = 0; h < headerRow.LastCellNum; h++)
                    {
                        ICell additionalHeaderColumn = additionalHeaderRow.CreateCell(h);
                        additionalHeaderColumn.CellStyle = headerRow.GetCell(h).CellStyle;
                        additionalHeaderColumn.SetCellValue(headerRow.GetCell(h).RichStringCellValue);
                    }

                    rowIndex = 1;
                }

                // Create new row in sheet
                IRow dataRow = sheet.CreateRow(rowIndex);

                foreach (DataColumn column in sourceTable.Columns)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);

                    switch (column.DataType.FullName)
                    {
                        case "System.String":
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                        case "System.Int":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            double val;
                            dataCell.SetCellValue(Double.TryParse(row[column].ToString(), out val) ? val : 0);
                            break;
                        case "System.DateTime":
                            DateTime dt = new DateTime(1900, 01, 01);
                            DateTime.TryParse(row[column].ToString(), out dt);

                            dataCell.SetCellValue(dt);
                            dataCell.CellStyle = dateCellStyle;
                            break;
                        default:
                            dataCell.SetCellValue(row[column].ToString());
                            break;
                    }
                }

                rowIndex++;
            }
            MemoryStream ms = new MemoryStream();
            for (int h = 0; h < headerRow.LastCellNum; h++)
            {
                sheet.AutoSizeColumn(h);
            }
            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;
            return ms;
            //ExportToExcelx(workbook, fileName);
        }



    }
}

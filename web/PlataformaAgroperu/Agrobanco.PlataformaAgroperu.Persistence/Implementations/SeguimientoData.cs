﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Seguimiento;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Persistence.Common;
using Agrobanco.PlataformaAgroperu.Persistence.Data;
using Agrobanco.PlataformaAgroperu.Infraestructure.Exceptions;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
namespace Agrobanco.PlataformaAgroperu.Persistence.Implementations
{
    public class SeguimientoData
    {
        private readonly Db2DataAccess _db2DataContext;
        public SeguimientoData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
        }

        public List<AvanceSolicitudes> ListarAvancesSolicitudes(int CodAgencia)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 3, CodAgencia, 0, iDB2DbType.iDB2Decimal);
            _db2DataContext.RunStoredProcedure(StoreConstants.Seguimiento.UP_PFA_SEL_AVANCE_SOLICITUDES, out IDataReader reader, lDataParam);
            List<AvanceSolicitudes> ListSolicitudes = new List<AvanceSolicitudes>();
            AvanceSolicitudes avance = null;
            while (reader.Read())
            {
                avance = new AvanceSolicitudes();
                avance.CodigoAgencia = reader.GetInt("COD_AGENCIA");
                avance.Agencia = reader.GetString("AGENCIA");
                avance.MontoTotalSolicitado = reader.GetDecimal("MONTO_SOLICITADO");
                avance.NroTotalSolicitudes = reader.GetInt("SOLICITUDES");
                avance.MontoGeoreferencia = reader.GetDecimal("MONTO_GEOREFERENCIA");
                avance.NumeroGeoreferencia = reader.GetInt("NRO_GEOREFERENCIA");
                avance.MontoPropuesta = reader.GetDecimal("MONTO_PROPUESTA");
                avance.NumeroPropuesta = reader.GetInt("NRO_PROPUESTA");
                avance.MontoPropuestaGenerada = reader.GetDecimal("MONTO_PROPUESTA_APROBADA");
                avance.NroPropuestaGenerada = reader.GetInt("NRO_PROPUESTA_APROBADA");
                avance.NroDesembolsoMes = reader.GetDecimal("NRO_DESEMBOLSO_MES");
                avance.MontoDesembolsoMes = reader.GetDecimal("MONTO_DESEMBOLSO_MES");

                ListSolicitudes.Add(avance);

            }
            return ListSolicitudes;
        }

        public List<AvanceSolicitudes> ListarAvancesSolicitudesPorAgencia(int CodAgencia)
        {

            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_CODAGENCIA", 3, CodAgencia, 0, iDB2DbType.iDB2Decimal);
            Logger.LogInformation("Inicio: Consulta Avance de Solicitudes Por Agencia");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Seguimiento.UP_PFA_SEL_AVANCE_SOLICITUDES_XAGENCIA, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Avance de Solicitudes Por Agencia");

            List<AvanceSolicitudes> ListSolicitudes = new List<AvanceSolicitudes>();
            AvanceSolicitudes avance = null;
            while (reader.Read())
            {
                avance = new AvanceSolicitudes();
                avance.CodigoAnalista = reader.GetString("COD_ANALISTA");
                avance.NombreAnalista = reader.GetString("ANALISTA");
                avance.MontoTotalSolicitado = reader.GetDecimal("MONTO_SOLICITADO");
                avance.NroTotalSolicitudes = reader.GetInt("SOLICITUDES");
                avance.MontoGeoreferencia = reader.GetDecimal("MONTO_GEOREFERENCIA");
                avance.NumeroGeoreferencia = reader.GetInt("NRO_GEOREFERENCIA");
                avance.MontoPropuesta = reader.GetDecimal("MONTO_PROPUESTA");
                avance.NumeroPropuesta = reader.GetInt("NRO_PROPUESTA");
                avance.MontoPropuestaGenerada = reader.GetDecimal("MONTO_PROPUESTA_APROBADA");
                avance.NroPropuestaGenerada = reader.GetInt("NRO_PROPUESTA_APROBADA");
                avance.NroDesembolsoMes = reader.GetDecimal("NRO_DESEMBOLSO_MES");
                avance.MontoDesembolsoMes = reader.GetDecimal("MONTO_DESEMBOLSO_MES");

                ListSolicitudes.Add(avance);

            }
            return ListSolicitudes;
        }

        // Lista Total de Solicitudes por Estados
        public List<SegReporteSolcitudes> ObtenerReporteSolcitudes(int CodAgencia, string CodigoAnalista)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 3, CodAgencia, 0, iDB2DbType.iDB2Integer);
            lDataParam[1] = _db2DataContext.CreateParameter("VI_ANALISTA", 4, CodigoAnalista);
            Logger.LogInformation("Inicio: Consulta Avance de Solicitudes Por Funcionario");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Seguimiento.UP_PFA_SEL_SEG_XFUNCIONARIO, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Avance de Solicitudes Por Funcionario");

            List<SegReporteSolcitudes> reporteSolcitudes = new List<SegReporteSolcitudes>();

            SegReporteSolcitudes datos = null;
            while (reader.Read())
            {
                datos = new SegReporteSolcitudes();
                datos.key = CodigoAnalista;
                datos.Sector = reader.GetString("SECTOR");
                datos.Estado = reader.GetString("ESTADO_SOLCITUD");
                datos.Numero = reader.GetDecimal("NUMERO_SOLICITUDES");
                datos.MontoSolicitado = reader.GetDecimal("MONTO_SOLICITUDES");
                reporteSolcitudes.Add(datos);
            }
            return reporteSolcitudes;
            
        }

        public List<SegReporteSolcitudes> ObtenerReporteSolcitudes(int CodAgencia)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("VI_AGENCIA", 3, CodAgencia, 0, iDB2DbType.iDB2Integer);            
            Logger.LogInformation("Inicio: Consulta Avance de Solicitudes Por Agencia");
            Logger.LogDbParameters(lDataParam);
            _db2DataContext.RunStoredProcedure(StoreConstants.Seguimiento.UP_PFA_SEL_SEG_XAGENCIAS, out IDataReader reader, lDataParam);
            Logger.LogInformation("Fin: Consulta Avance de Solicitudes Por Agencia");

            List<SegReporteSolcitudes> reporteSolcitudes = new List<SegReporteSolcitudes>();

            SegReporteSolcitudes datos = null;
            while (reader.Read())
            {
                datos = new SegReporteSolcitudes();
                datos.key = reader.GetString("FUNCIONARIO");
                datos.Sector = reader.GetString("SECTOR");
                datos.Estado = reader.GetString("ESTADO_SOLCITUD");
                datos.Numero = reader.GetDecimal("NUMERO_SOLICITUDES");
                datos.MontoSolicitado = reader.GetDecimal("MONTO_SOLICITUDES");
                datos.Saldo = reader.GetDecimal("SALDO");
                datos.Extorno = reader.GetDecimal("EXTORNO");
                reporteSolcitudes.Add(datos);
            }
            return reporteSolcitudes;
        }

        public List<SegReporteSolcitudes> ObtenerReporteSolcitudes()
        {
            Logger.LogInformation("Inicio: Consulta Avance de Solicitudes Por Agencia");
            
            _db2DataContext.RunStoredProcedure(StoreConstants.Seguimiento.UP_PFA_SEL_SEGUIMIENTO_AGROPERU, out IDataReader reader);
            Logger.LogInformation("Fin: Consulta Avance de Solicitudes Por Agencia");

            List<SegReporteSolcitudes> reporteSolcitudes = new List<SegReporteSolcitudes>();

            SegReporteSolcitudes datos = null;
            while (reader.Read())
            {
                datos = new SegReporteSolcitudes();
                datos.key = reader.GetString("AGENCIA");
                datos.Sector = reader.GetString("SECTOR");
                datos.Estado = reader.GetString("ESTADO_SOLCITUD");
                datos.Numero = reader.GetDecimal("NUMERO_SOLICITUDES");
                datos.MontoSolicitado = reader.GetDecimal("MONTO_SOLICITUDES");
                datos.Saldo = reader.GetDecimal("SALDO");
                datos.Extorno = reader.GetDecimal("EXTORNO");
                reporteSolcitudes.Add(datos);
            }
            return reporteSolcitudes;
        }

    }
}

﻿using System;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.PlataformaAgroperu.Web.App_Helpers
{
    public class BaseController : Controller
    {
        protected Usuario Usuario
        {
            get
            {
                if (!(Session is null))
                {
                    var usuario = (Usuario)Session["oUsuario"];
                    if (usuario is null)
                        RedirectToAction("Login", "Account");
                    else
                        return usuario;                    
                    // throw new Exception("Session Finalizada");
                    
                }
                return null;
            }
            private set => throw new NotImplementedException();
        }
    }
}
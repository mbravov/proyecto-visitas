﻿using System.Web.Optimization;

namespace Agrobanco.PlataformaAgroperu.Web
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/css", "*.min.css"));

            //bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/fonts", "*.eot"));
            //bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/fonts", "*.svg"));
            //bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/fonts", "*.ttf"));
            //bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/fonts", "*.woff"));
            //bundles.Add(new StyleBundle("~/content/smartadmin").IncludeDirectory("~/content/fonts", "*.otf"));

            //            bundles.Add(new StyleBundle("~/content/smartadmin").Include("~/content/fonts/fontawesome-webfont.eot",
            //                "~/content/fonts/fontawesome-webfont.svg",
            //"~/content/fonts/fontawesome-webfont.ttf",
            //"~/content/fonts/fontawesome-webfont.woff",
            //"~/content/fonts/FontAwesome.otf",
            //"~/content/fonts/glyphicons-halflings-regular.eot",
            //"~/content/fonts/glyphicons-halflings-regular.svg",
            //"~/content/fonts/glyphicons-halflings-regular.ttf",
            //"~/content/fonts/glyphicons-halflings-regular.woff"
            //                ));

            bundles.Add(new ScriptBundle("~/scripts/smartadmin").Include(
                "~/scripts/app.config.js",
                "~/scripts/plugin/jquery-touch/jquery.ui.touch-punch.min.js",
                "~/scripts/bootstrap/bootstrap.min.js",
                "~/scripts/notification/SmartNotification.min.js",
                "~/scripts/smartwidgets/jarvis.widget.min.js",
                "~/scripts/plugin/jquery-validate/jquery.validate.min.js",
                "~/scripts/plugin/masked-input/jquery.maskedinput.min.js",
                "~/scripts/plugin/select2/select2.min.js",
                "~/scripts/plugin/bootstrap-slider/bootstrap-slider.min.js",
                "~/scripts/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
                "~/scripts/plugin/msie-fix/jquery.mb.browser.min.js",
                "~/scripts/plugin/fastclick/fastclick.min.js",
                "~/scripts/plugin/superbox/superbox.min.js",
                "~/scripts/plugin/superbox/superboxConsulta.min.js",
                "~/scripts/plugin/file-input/fileinput.min.js",
                "~/scripts/plugin/file-input/locales/es.js",                
                "~/scripts/plugin/file-input/exif.js",
                "~/scripts/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js",
                "~/scripts/plugin/jquery-blockui/jquery.blockUI.min.js",
                "~/scripts/app.min.js"));

            bundles.Add(new ScriptBundle("~/scripts/full-calendar").Include(
                "~/scripts/plugin/moment/moment.min.js",
                "~/scripts/plugin/fullcalendar/jquery.fullcalendar.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/charts").Include(
                //"~/scripts/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js",
                "~/scripts/plugin/sparkline/jquery.sparkline.min.js",
                //"~/scripts/plugin/morris/morris.min.js",
                //"~/scripts/plugin/morris/raphael.min.js",
                //"~/scripts/plugin/flot/jquery.flot.cust.min.js",
                //"~/scripts/plugin/flot/jquery.flot.resize.min.js",
                //"~/scripts/plugin/flot/jquery.flot.time.min.js",
                //"~/scripts/plugin/flot/jquery.flot.fillbetween.min.js",
                //"~/scripts/plugin/flot/jquery.flot.orderBar.min.js",
                //"~/scripts/plugin/flot/jquery.flot.pie.min.js",
                //"~/scripts/plugin/flot/jquery.flot.tooltip.min.js",
                //"~/scripts/plugin/dygraphs/dygraph-combined.min.js",
                "~/scripts/plugin/chartjs/chart.min.js",
                "~/scripts/plugin/chartjs/chartjs-plugin-sort.js",
                "~/scripts/plugin/chartjs/chartjs-plugin-datalabels.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/datatables").Include(
                "~/scripts/plugin/datatables/jquery.dataTables.min.js",
                "~/scripts/plugin/datatables/dataTables.colVis.min.js",
                "~/scripts/plugin/datatables/dataTables.tableTools.min.js",
                "~/scripts/plugin/datatables/dataTables.bootstrap.min.js",
                "~/scripts/plugin/datatables/dataTables.select.min.js",
                "~/scripts/plugin/datatable-responsive/datatables.responsive.min.js",
                "~/scripts/plugin/datatables/dataTables.buttons.min.js",
                "~/scripts/plugin/datatables/jszip.min.js",
                "~/scripts/plugin/datatables/buttons.html5.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/jq-grid").Include(
                "~/scripts/plugin/jqgrid/jquery.jqGrid.min.js",
                "~/scripts/plugin/jqgrid/grid.locale-en.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/forms").Include(
                "~/scripts/plugin/jquery-form/jquery-form.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/smart-chat").Include(
                "~/scripts/smart-chat-ui/smart.chat.ui.min.js",
                "~/scripts/smart-chat-ui/smart.chat.manager.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/vector-map").Include(
                "~/scripts/plugin/vectormap/jquery-jvectormap-1.2.2.min.js",
                "~/scripts/plugin/vectormap/jquery-jvectormap-world-mill-en.js"
                ));

            // REFERENCIA PARA VISTAS
            bundles.Add(new ScriptBundle("~/scripts/MantenimientoComisiones").Include(
                "~/scripts/app/Mantenimiento/Comisiones.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/MantenimientoMatrizCostos").Include(
                "~/scripts/app/Mantenimiento/MatrizCostos.js"
                ));


            bundles.Add(new ScriptBundle("~/scripts/ConsultaSolicitudes").Include(
                "~/Scripts/app/Consultas/ConsultaSolicitudes.js",
                "~/Scripts/app/Consultas/ConsultaDetalleIntegrante.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/ResultadosEvaluaciones").Include(
                "~/Scripts/app/Consultas/ResultadosAgrupamiento.js",
                "~/Scripts/app/Consultas/ResultadosPadron.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/Georeferenciacion").Include(
                "~/Scripts/app/GeoReferenciacion/Datatables.js",
                "~/Scripts/app/GeoReferenciacion/UploadImages.js",
                "~/Scripts/app/GeoReferenciacion/LinkImages.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/BandejaAgrupamiento").Include(
                "~/Scripts/app/Grupo/BandejaAgrupamiento.js",
                "~/Scripts/app/Grupo/DetalleGrupo.js",
                "~/Scripts/app/Grupo/DetalleIntegrante.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/NuevoAgrupamiento").Include(
                "~/Scripts/app/Grupo/NuevoGrupo.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/RevisionSolicitudes").Include(
                "~/Scripts/app/Grupo/RevisionSolicitudes.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/Padron").Include(
                "~/Scripts/app/Padron/ConsultaPadron.js",
                "~/Scripts/app/Padron/NuevoPadron.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/Prospectos").Include(
                "~/Scripts/app/Prospecto/ConsultaProspectos.js",
                "~/Scripts/app/Prospecto/NuevoProspecto.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/FiltrosAgroperu").Include(
                "~/Scripts/app/FiltrosAgroperu/FiltrosEvaluar.js",
                "~/Scripts/app/FiltrosAgroperu/FiltrosConsultas.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/Inicio").Include(
                "~/Scripts/app/Seguimiento/Inicio.js"
                ));

            BundleTable.EnableOptimizations = true;
        }
    }
}
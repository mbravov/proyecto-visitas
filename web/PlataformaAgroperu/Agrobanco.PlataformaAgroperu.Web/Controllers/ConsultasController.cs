﻿using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class ConsultasController : BaseController
    {
        public JsonResult ResultadosProspectos(string CodGrupo)
        {

            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConsultaSolicitudes()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            return View();
        }
        public JsonResult ObtenerSolicitudes(string codgrupo)
        {   
            var ivDomain = new ConsultasDomain();
            List<GrupoIntegrante> lista = ivDomain.ListarSolicitudes(codgrupo);            
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ResultadosGrupos()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;
            return View();
        }
        public ActionResult ResultadosPadron()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;
            return View();
        }
        public JsonResult ConsultaResultadosPadron(string CodAgencia, string CodigoGrupo)
        {
            //string CodAgencia, string CodAnalista, int CodigoGrupo

            var ivDomain = new ConsultasDomain();
            List<ResultadoGrupo> lista = ivDomain.ListarResultadosPadron(CodAgencia, CodigoGrupo);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ConsultaResultadosGrupos(string CodAgencia, string CodAnalista, int CodigoGrupo)
        {
            //string CodAgencia, string CodAnalista, int CodigoGrupo
            
            var ivDomain = new ConsultasDomain();
            List<ResultadoGrupo> lista = ivDomain.ListarResultados(CodAgencia, CodAnalista, CodigoGrupo);
            return Json(lista, JsonRequestBehavior.AllowGet);

        }


        public ActionResult BandejaCliente()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            return View();
        }

        public JsonResult ListarSolicitudes(string CodAgencia, string CodAnalista)
        {
            var consultasDomain = new ConsultasDomain();
            var lista = consultasDomain.ListarSolicitudes(CodAgencia, CodAnalista);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


    }
}
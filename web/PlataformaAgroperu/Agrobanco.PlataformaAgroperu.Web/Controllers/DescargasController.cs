﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class DescargasController : BaseController
    {
        // GET: Descargas
        public ActionResult Formatos()
        {
            if (Usuario is null)
                return RedirectToAction("Login", "Account");

            ViewBag.PadronAgroperuVersion = ApplicationKeys.ExcelPadronAgroperuVersion;
            ViewBag.PadronAgroperuFecha = ApplicationKeys.ExcelPadronAgroperuFecha;

            ViewBag.GrupoAgroperuVersion = ApplicationKeys.ExcelGrupoAgroperuVersion;            
            ViewBag.GrupoAgroperuFecha = ApplicationKeys.ExcelGrupoAgroperuFecha;

            return View();
        }

        public virtual ActionResult DescargarPlantillaAgrupamiento()
        {
            string file = "PLANTILLA_GRUPO_AGROPERU.xlsx";
            string fullPath = Path.Combine(Server.MapPath("~/Content/UploadedFolder"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        public virtual ActionResult DescargarPlantillaPadron()
        {
            string file = "PLANTILLA_PADRON_AGROPERU.xlsx";
            string fullPath = Path.Combine(Server.MapPath("~/Content/UploadedFolder"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        public virtual ActionResult DescargarPlantillaFiltroAgroperu()
        {
            string file = "PLANTILLA_FILTRO_AGROPERU.xlsx";
            string fullPath = Path.Combine(Server.MapPath("~/Content/UploadedFolder"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }
    }
}
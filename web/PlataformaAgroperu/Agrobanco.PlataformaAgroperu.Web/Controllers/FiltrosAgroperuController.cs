﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using System.IO;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class FiltrosAgroperuController : BaseController
    {
        // GET: FiltrosAgroperu
        public ActionResult Index()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        public ActionResult Evaluar()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        public ActionResult Consultar()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();

        }


        public JsonResult RegistrarFiltro(string Descripcion, string FechaCliente)
        {
            ExcelResponseModel respon = new ExcelResponseModel();
            if (Request.Files.Count != 1) return Json(new ExcelResponseModel("01|Error Archivo Adjunto||"), JsonRequestBehavior.AllowGet);
            var fileExcel = Request.Files[0];
            var memorystream = new MemoryStream();
            var filtro = new FiltroAgroperuDomain();
            var fileExtension = fileExcel?.FileName.Split('.');
            fileExcel?.InputStream.CopyTo(memorystream);
            memorystream.Position = 0;
            try
            {
                if (fileExtension != null)
                {
                    filtro.CargarExcelFiltroAgroperu(ref respon, Usuario, memorystream, fileExtension[fileExtension.Length - 1], Descripcion, FechaCliente);

                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return Json(respon, JsonRequestBehavior.AllowGet);
                //throw;
            }
            return Json(respon, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConsultarFiltrosPorAgencia(int CodAgencia)
        {
            var filtrosDomain = new FiltroAgroperuDomain();            
            var lista = filtrosDomain.ListarFiltrosAgroperu(CodAgencia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DescargarFiltroAgroperu(int CodigoFiltro, string Descripcion)
        {
            var ivDomain = new FiltroAgroperuDomain();
            var respon = new ExcelResponseModel();
            MemoryStream ms = ivDomain.DescargarResultadosFiltroAgroperu(CodigoFiltro);
            StreamResponse(CodigoFiltro.ToString(), Descripcion, ms);
            return null;
        }
        private void StreamResponse(string fileCode, string fileName, MemoryStream stream)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", $"inline;filename={fileName}_{fileCode}.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

    }
}
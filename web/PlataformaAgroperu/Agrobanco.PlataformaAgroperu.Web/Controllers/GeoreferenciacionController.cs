﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Georeferenciacion;
using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class GeoreferenciacionController : BaseController
    {
        // GET: Georeferenciacion
        public ActionResult Vincular(string NroDocumento, string NroSolicitud, string Nombre)
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            var data = new Dictionary<string, string>
            {
                ["NumeroDocumento"] = NroDocumento,
                ["NumeroSolicitud"] = NroSolicitud,
                ["Nombre"] = Nombre
            };
            TempData["DataCliente"] = data;
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            if(Usuario == null)
                return RedirectToAction("Login", "Account");

            var fotoGeoRef = new GeoreferenciacionDomain();
            var NroSolicitud = "";
            var NroDocumento = "";
            var Nombre = "";

            if (TempData["DataCliente"] != null)
            {
                if (TempData["DataCliente"] is Dictionary<string, string> dict)
                {
                    NroDocumento = dict["NumeroDocumento"];
                    NroSolicitud = dict["NumeroSolicitud"];
                    Nombre = dict["Nombre"];
                };

                var lstFotos = fotoGeoRef.ObtenerFotosPorDocumento(NroDocumento, NroSolicitud);
                lstFotos?.RemoveAll(r => string.IsNullOrEmpty(r.imgBase64));

                GuardarSessionFotos(lstFotos);

                ViewBag.NumeroSolicitud = NroSolicitud;
                ViewBag.NumeroDocumento = NroDocumento;
                ViewBag.Nombre = Nombre;

                return View(lstFotos);
            }
            else
            {
                return RedirectToAction("BandejaAgrupamiento", "Grupo");
            }            
        }

        private void GuardarSessionFotos(List<FotoModel> lstfotos)
        {
            Session["LstFotosTot"] = lstfotos;
        }

        public ActionResult ConsultarFotosCercanas(string Latitud, string Longitud)
        {
            var fotoDomain = new GeoreferenciacionDomain();
            List<FotoModel> lstClientes = new List<FotoModel>();
            try
            {
                lstClientes = fotoDomain.ListarFotosCercanas(Latitud, Longitud);
            }
            catch (System.Exception ex)
            {
                
                lstClientes = null;
                throw;
                //throw new InvalidDataBaseOperationException(ex.Message);
            }
            return Json(lstClientes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubirNuevaFoto()
        {
            var DomainfotoGeoRef = new GeoreferenciacionDomain();

            FotoModel fotoModel = null;
            fotoModel = new FotoModel();
            var NumeroSolicitud = Request["NumeroSolicitud"];
            var NumeroDocumento = Request["NumeroDocumento"];
            fotoModel.FechaFoto = Request["FechaFoto"];
            fotoModel.Latitud = Request["FLatitud"];
            fotoModel.Longitud = Request["FLongitud"];
            fotoModel.Observacion = Request["FObservacion"];

            if (string.IsNullOrWhiteSpace(fotoModel?.Latitud) ||
                string.IsNullOrWhiteSpace(fotoModel?.Longitud) ||
                string.IsNullOrWhiteSpace(fotoModel?.FechaFoto) ||
                fotoModel?.Latitud == "0" || fotoModel?.Longitud == "0" || fotoModel?.FechaFoto == "0")
            {
                var result = new Dictionary<string, object>
                {
                    {"error", "La imagen no cuenta con datos georeferenciales válidos"}
                };
                return Json(result);
            }

            try
            {
                var memoryStream = new MemoryStream();
                HttpPostedFileBase file = Request.Files["file-es[]"];

                file?.InputStream.CopyTo(memoryStream);



                DomainfotoGeoRef.SubirFotoPorNumeroSolicitud(NumeroSolicitud, NumeroDocumento, memoryStream, ref fotoModel, Usuario.User);

                List<FotoModel> lstFotosTot = (List<FotoModel>)Session["LstFotosTot"];
                lstFotosTot.Add(fotoModel);
                Session["LstFotosTot"] = lstFotosTot;
            }
            catch (Exception e)
            {
                var result = new Dictionary<string, object>
                {
                    {"error", e.Message}
                };
                //Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(result);
            }
            return Json(fotoModel);
        }

        public ActionResult RegistrarGeoreferenciacion(string tramaCodImagenes, string[] lstCodImg, string[] lstCodImgD, string NumeroDocumento, string NumeroSolicitud)
        {
            var result = string.Empty;
            var fotoGeoRef = new GeoreferenciacionDomain();

            List<FotoModel> lstFotosTot = (List<FotoModel>)Session["LstFotosTot"];
            var lstFotosVin = new List<FotoModel>();
            var lstFotosLib = new List<FotoModel>();

            foreach (string IdImg in lstCodImg)
                lstFotosVin.AddRange(lstFotosTot.Where(f => f.NroSolicitud == "0" && f.IdLaserFiche.ToString() == IdImg).ToList());

            if (lstCodImgD != null)
                foreach (string IdImgD in lstCodImgD)
                    lstFotosLib.AddRange(lstFotosTot.Where(f => f.NroSolicitud != "0" && f.IdLaserFiche.ToString() == IdImgD).ToList());

            try
            {
                if (lstFotosVin.Count > 0 || lstFotosLib.Count > 0)
                {
                    fotoGeoRef.VincularFotosPorNumeroSolicitud(NumeroSolicitud, NumeroDocumento, tramaCodImagenes, lstFotosVin, lstFotosLib, Usuario.User);
                }

                result = tramaCodImagenes + " Datos Guardados con éxito...!!";
            }
            catch (System.Exception ex)
            {
                result = ex.Message;
                throw;
            }
            return Json(result);
        }

        public ActionResult ConsultaFotosSolicitud(string NumeroSolicitud, string NumeroDocumento, string Nombre)
        {
            var data = new Dictionary<string, string>
            {
                ["NumeroDocumento"] = NumeroDocumento,
                ["NumeroSolicitud"] = NumeroSolicitud,
                ["Nombre"] = Nombre
            };
            TempData["DataCliente"] = data;
            return RedirectToAction("ConsultaFotos");
        }

        public ActionResult ConsultaFotos()
        {
            var fotoGeoRef = new GeoreferenciacionDomain();
            var NroSolicitud = "";
            var NroDocumento = "";
            var Nombre = "";

            if (TempData["DataCliente"] != null)
            {
                if (TempData["DataCliente"] is Dictionary<string, string> dict)
                {
                    NroDocumento = dict["NumeroDocumento"];
                    NroSolicitud = dict["NumeroSolicitud"];
                    Nombre = dict["Nombre"];
                };

                var lstFotos = fotoGeoRef.ObtenerFotosPosSolicitud(NroSolicitud);

                lstFotos?.RemoveAll(r => string.IsNullOrEmpty(r.imgBase64));
                ViewBag.NumeroSolicitud = NroSolicitud;
                ViewBag.NumeroDocumento = NroDocumento;
                ViewBag.Nombre = Nombre;

                return View(lstFotos);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}
﻿using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Grupo;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Web.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;
using System.Configuration;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class GrupoController : BaseController
    {
        // GET: Grupo
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NuevoGrupo()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            return View();
        }

        public JsonResult GrabarGrupo(string pcabecera,string FechaCliente)
        {
            ExcelResponseModel respon=new ExcelResponseModel();
            if (Request.Files.Count != 1) return Json(new ExcelResponseModel("01|Error Archivo Adjunto||"), JsonRequestBehavior.AllowGet);
            var fileExcel = Request.Files[0];
            var memorystream = new MemoryStream();
            var ivDomain = new AgrupamientoDomain();
            var fileExtension = fileExcel?.FileName.Split('.');
            fileExcel?.InputStream.CopyTo(memorystream);
            memorystream.Position = 0;
            try
            {
                if (fileExtension != null)
                {
                    Usuario oUsuario = Usuario;                    
                    ivDomain.SubirAgrupamientoFromDocument(ref respon, oUsuario, pcabecera, memorystream, fileExtension[fileExtension.Length - 1], ApplicationKeys.PlantillaAgrupamiento, FechaCliente);
                    
                }
            }
            catch (Exception ex)
            {
                respon.message = ex.Message;
                return Json(respon, JsonRequestBehavior.AllowGet);
                //throw;
            }
            return Json(respon, JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult BandejaAgrupamiento()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;

            ViewBag.UrlBaseSGV = ConfigurationManager.AppSettings["config:UrlBaseSGV"];
            ViewBag.UrlBaseSGVAPI = ConfigurationManager.AppSettings["config:UrlBaseSGVAPI"];

            return View();
        }
        [HttpPost]
        public JsonResult ListarAgrupamientos(string CodAgencia, string CodAnalista)
        {
            var ivDomain = new AgrupamientoDomain();            
            List<InfoGrupo> lista = ivDomain.ConsultarAgrupamientosPorAgenciaAnalista(CodAgencia, CodAnalista);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ListarSolicitudes()
        {
            var grupoDomain = new AgrupamientoDomain();
            List<GrupoIntegrante> lista = grupoDomain.ListarSolicitudesPorAgenciaAnalista(Usuario.CodigoAgencia.ToString(), Usuario.CodigoFuncionario);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult ObtenerDatosCrongrama(int CodigoCosto)
        //{
        //    var grupoDomain = new AgrupamientoDomain();
        //    var lista = grupoDomain.ObtenerTipoCronograma(CodigoCosto);
        //    return Json(lista, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult ListarIntegrantesPorAgrupamiento(string CodAgrupamiento)
        {
            var grupoDomain = new AgrupamientoDomain();
            List<GrupoIntegrante> lista = grupoDomain.ListarIntegrantesPorGrupo(CodAgrupamiento);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ObtenerDatosAgrupamiento(string CodAgrupamiento)
        {
            var ivDomain = new AgrupamientoDomain();
            Grupo oAgrup = ivDomain.ConsultarAgrupamientoInfo(CodAgrupamiento);

            return Json(oAgrup, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ObtenerDatosIntegrante(decimal NumeroSolicitud)
        {
            var ivDomain = new AgrupamientoDomain();
            List<GrupoIntegrante> oAgrupDet = ivDomain.ConsultarIntegrantePorDocumento(NumeroSolicitud);

            return Json(oAgrupDet, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActualizarDatosIntegrante(string NumSolicitud,string IdIntegrante,string CodAgrupamiento, string FormaAbono, string Banco, string NumeroCuenta,string Correo,string Celular)
        {
            var ivDomain = new AgrupamientoDomain();
         
            int n=ivDomain.ActualizarIntegrantePorDocumento(NumSolicitud, IdIntegrante, CodAgrupamiento,FormaAbono,Banco,NumeroCuenta,Correo,Celular);

            return Json(n.ToString(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActualizarFormaPagoIntegrante(string CodAgrupamiento, string CodIntegrante, string FormaAbono, string Banco, string NumeroCuenta)
        {
            var result = string.Empty;
            var agrupamiento = new AgrupamientoDomain();
            try
            {
                agrupamiento.ActualizarFormaPagoIntegrante(CodAgrupamiento, CodIntegrante, FormaAbono, Banco, NumeroCuenta);
                result = " Datos Guardados con éxito...!!";
            }
            catch (System.Exception ex)
            {
                result = ex.Message;
                throw;
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarPagoAsistenteTecnico(decimal CodigoGurpo, int PagoAsistenteTecnico)
        {
            var result = string.Empty;
            var agrupamiento = new AgrupamientoDomain();
            try
            {
                agrupamiento.ActualizarPagoAsistenteTecnico(CodigoGurpo, PagoAsistenteTecnico, Usuario.User);
                result = "100";
            }
            catch (System.Exception ex)
            {
                result = ex.Message;
                throw;
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult RegistrarTipoCronograma(string NumeroSolicitud, string TipoCuota,
            int NumeroCuotas, int FrecuenciaPago, string UnidadFrecuencia, DateTime FechaGracia, int Plazo,
            string PorcentajesCuotas, string FechasCuotas)
        {

            if (Usuario == null)
                return Json("Su sesión ha caducado, favor volver a ingresar a la aplicación.", JsonRequestBehavior.AllowGet);

            var ResponseModel = new ResponseInfoModel();
            var Mensaje = "";
            try
            {
                var grupoDomain = new AgrupamientoDomain();
                ResponseModel.TipoError = grupoDomain.RegistrarTipoCronograma(NumeroSolicitud, TipoCuota, NumeroCuotas, FrecuenciaPago, UnidadFrecuencia, 
                    FechaGracia, Plazo, PorcentajesCuotas, FechasCuotas, Usuario.User, out Mensaje);

                ResponseModel.Message = Mensaje;
            }
            catch (Exception ex)
            {
                ResponseModel.Exception = ex;
                ResponseModel.TipoError = "33";
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json(ResponseModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidarPeriodoGracia(DateTime FechaGracia)
        {
            var result = new ParametricasModel();
            try
            {
                var grupoDomain = new AgrupamientoDomain();
                result = grupoDomain.ValidarPeriodoGracia(FechaGracia);
            }
            catch (Exception ex)
            {
                result.value = ex.Message;
                return Json(result, JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubirDocumentoPorSolicitud(string NumeroSolicitud, string NumeroDocumento, string TipoDocumento, string Observacion)
        {
            int IdDocument = 0;
            if (Request.Files.Count != 1)
                return Json(IdDocument, JsonRequestBehavior.AllowGet);

            var file = Request.Files[0];
            var memorystream = new MemoryStream();
            var fileExtension = file?.FileName.Split('.');
            file?.InputStream.CopyTo(memorystream);
            memorystream.Position = 0;

            try
            {
                if (fileExtension != null)
                {
                    var grupoDomain = new AgrupamientoDomain();
                    IdDocument = grupoDomain.SubirDocumento(memorystream, fileExtension[fileExtension.Length - 1], TipoDocumento, NumeroSolicitud, NumeroDocumento, Observacion, Usuario.User);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json(IdDocument, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DescargarDocumentoPorId(int IdLaserFiche)
        {
            var grupoDomain = new AgrupamientoDomain();
            var document = grupoDomain.BajarDocumentoLaserFiche(IdLaserFiche, out var extension);
            if(document.stream != null)
            {
                var ext = document.fileName.Split('.');
                DocumentoStreamResponse(document.fileName, document.stream, ext[ext.Length - 1]);
            }
            else
            {
                return RedirectToAction("ServerNotAvailable", "Error");
            }

            return null;
        }

        private void DocumentoStreamResponse(string fileName, MemoryStream stream, string extension)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", $"inline;filename={fileName}");
            Response.ContentType = TypesConstants.GetTypeMIME(extension);
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        [HttpPost]
        public JsonResult EnviarParaAprobacion(string NumeroSolicitud, string NumeroDocumento, string Motivo, string Comentario)
        {
            int IdDocument = 0;
            if (Request.Files.Count != 1)
                return Json(IdDocument, JsonRequestBehavior.AllowGet);

            var file = Request.Files[0];
            var memorystream = new MemoryStream();
            var fileExtension = file?.FileName.Split('.');
            file?.InputStream.CopyTo(memorystream);
            memorystream.Position = 0;

            try
            {
                var grupoDomain = new AgrupamientoDomain();
                grupoDomain.RegistrarAprobacionPorExcepcion(memorystream, fileExtension[fileExtension.Length - 1],
                   NumeroSolicitud, NumeroDocumento, Motivo, Comentario, Usuario.User, TipoFile:"9");

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json("Exito", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AnularSolicitud(decimal NumeroSolicitud)
        {
            try
            {
                var grupoDomain = new AgrupamientoDomain();
                grupoDomain.AnularSolicitud(NumeroSolicitud);
                return Json("100", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //throw;
            }
            
        }

        [HttpPost]
        public JsonResult AnularGrupo(int CodigoGrupo)
        {
            try
            {
                var grupoDomain = new AgrupamientoDomain();
                grupoDomain.AnularGrupo(CodigoGrupo);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                throw;
            }
            return Json("Exito", JsonRequestBehavior.AllowGet);
        }

        #region Vista SeguimientoSolicitudes

        public ActionResult RevisionSolicitudes()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;
            return View();
        }

        [HttpPost]
        public JsonResult ListarSolicitudesEnRevision(string CodAgencia, string CodAnalista)
        {
            var grupoDomain = new AgrupamientoDomain();
            
            List<GrupoIntegrante> Revision = grupoDomain.ListarSolicitudesEnRevision(CodAgencia, CodAnalista);
            List<GrupoIntegrante> Solicitudes = grupoDomain.ListarSolicitudes(CodAgencia, CodAnalista);

            var lista = new ListasSolicitudes
            {
                Lista1 = Solicitudes,
                Lista2 = Revision
            };

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarSolicitudesConsulta(string CodAgencia, string CodAnalista)
        {
            var grupoDomain = new AgrupamientoDomain();
            List<GrupoIntegrante> lista = grupoDomain.ListarSolicitudes(CodAgencia, CodAnalista);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AprobarExcepcion(string NumeroSolicitud, string Observacion, string Respuesta)
        {
            try
            {
                var grupoDomain = new AgrupamientoDomain();
                grupoDomain.AprobarExcepcion(NumeroSolicitud, Observacion, Respuesta, Usuario.User);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return Json("100", JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
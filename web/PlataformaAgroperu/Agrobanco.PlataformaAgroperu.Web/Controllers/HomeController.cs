﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Seguimiento;


namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            SegIndicadores Reporte = null;
            try
            {
                if (Usuario.CodigoPerfil == 2001)
                {
                    var domainSeguimiento = new SeguimientoDomain();
                    Reporte = domainSeguimiento.ObtenerReporteSolicitudes(Usuario.CodigoAgencia, Usuario.CodigoFuncionario);
                }
                
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                Reporte = null;
                throw;
            }

            return View(Reporte);
        }

        [HttpPost]
        public ActionResult ObtenerReporteSolocitudes()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            SegModelCharts Reporte = null;
            try
            {
                var domainSeguimiento = new SeguimientoDomain();
                if (Usuario.CodigoPerfil == 2001)
                {
                    Reporte = null;
                }
                else if (Usuario.CodigoPerfil == 3001)
                {
                    Reporte = domainSeguimiento.ObtenerReporteSolicitudesChart(Usuario.CodigoAgencia);
                }
                else if(Usuario.HomePageEstadistico)
                {
                    Reporte = domainSeguimiento.ObtenerReporteSolicitudesChart();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                Reporte = null;
                throw;
            }

            return Json(Reporte, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Colocaciones()
        //{
        //    return View();
        //}


        

    }
}
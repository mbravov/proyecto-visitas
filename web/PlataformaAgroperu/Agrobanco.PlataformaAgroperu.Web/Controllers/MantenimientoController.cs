﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Web.Models;
using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Mantenimientos;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class MantenimientoController : BaseController
    {
        // GET: Mantenimiento
        public ActionResult Index()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;

            return View();
        }

        #region Vistas
        public ActionResult Comisiones()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;

            return View();
        }
        public ActionResult ProfTecnicos()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            var result = new ResponseInfoModel();
            if (Session["Response"] != null)
                result = (ResponseInfoModel)Session["Response"];

            ViewBag.Result = Json(result, JsonRequestBehavior.AllowGet);
            Session["Response"] = null;

            return View();
        }
        public ActionResult Result()
        {

            if (Session["Response"] == null)
                return RedirectToAction("Login", "Account");

            var response = (ResponseInfoModel)Session["Response"];
            Session["Response"] = null;

            if (response.TipoError == "0")
            {
                return View("Error", response);
            }
            else
            {
                return View(response);
            }


        }
        [HttpGet]
        public ActionResult MatrizCostos()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();
        }
        #endregion


        #region Consultas

        #endregion

        #region Metodos 

        // Mantenimiento Comisión
        [HttpPost]
        public ActionResult CrearComision(int CodigoOrganizacion, string NombreComision)
        {
            var domain = new MantenimientoDomain();
            var result = new ResponseInfoModel();

            try
            {
                domain.CrearComision(CodigoOrganizacion, NombreComision, Usuario.User);
                result.TipoError = "0";
            }
            catch (Exception ex)
            {
                result.TipoError = "10";
                result.Message = ex.Message;
                result.Exception = ex;
                result.StackTrace = ex.StackTrace;
                throw;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActualizarComision(int CodigoOrganizacion, int CodigoComision, string NombreComision)
        {
            var domain = new MantenimientoDomain();
            var result = new ResponseInfoModel();

            try
            {
                domain.ActualizarComision(CodigoOrganizacion, CodigoComision, NombreComision, Usuario.User);
                result.TipoError = "0";
            }
            catch (Exception ex)
            {
                result.TipoError = "10";
                result.Message = ex.Message;
                result.Exception = ex;
                result.StackTrace = ex.StackTrace;
                throw;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EliminarComision(int CodigoOrganizacion, int CodigoComision)
        {
            var domain = new MantenimientoDomain();
            var result = new ResponseInfoModel();

            try
            {
                domain.ElimiarComision(CodigoOrganizacion, CodigoComision, Usuario.User);
                result.TipoError = "0";
            }
            catch (Exception ex)
            {
                result.TipoError = "10";
                result.Message = ex.Message;
                result.Exception = ex;
                result.StackTrace = ex.StackTrace;
                throw;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Mantenimiento Profesionales Técnicos
        [HttpGet]
        public ActionResult RegistrarProfesionalTecnico()
        {
            var result = new ResponseInfoModel();
            ViewBag.Result = Json(result, JsonRequestBehavior.AllowGet);
            return RedirectToAction("ProfTecnicos");
        }
        [HttpPost]
        public ActionResult RegistrarProfesionalTecnico(FormCollection formCollection)
        {
            var domain = new MantenimientoDomain();
            var result = new ResponseInfoModel();

            if (formCollection == null)
                return RedirectToAction("ProfTecnicos");

            var datos = new Domain.Models.Grupo.DatosPersona();
            datos.NroDocumento = formCollection["PT_DNI"];
            datos.Correo = formCollection["PT_email"];
            datos.NomPrimer = formCollection["PT_PrimerNombre"];
            datos.NomSegundo = formCollection["PT_SegundoNombre"];
            datos.ApePaterno = formCollection["PT_ApellidoPaterno"];
            datos.ApeMaterno = formCollection["PT_ApellidoMaterno"];
            datos.Password = Security.MD5Hash($"{datos.ApePaterno}{datos.NroDocumento}");

            var CodigoAgencia = formCollection["PT_Agencia"];
            var Organizacion = formCollection["PT_Organizacion"];

            formCollection = null;

            try
            {
                domain.GuardarProfesionalTecnio(datos, CodigoAgencia, Organizacion, Usuario.User);
                result.TipoError = "100";
            }
            catch (Exception ex)
            {
                result.TipoError = "10";
                result.Message = ex.Message;
                result.Exception = ex;
                result.StackTrace = ex.StackTrace;
                throw;
            }

            Session["Response"] = result;

            return RedirectToAction("ProfTecnicos");
        }

        // Mantenimiento MatrizCOstos
        [HttpPost]
        public ActionResult ListarCostos(string Sector = "0", string Programa = "0", bool Historico = false)
        {
            var Domain = new MantenimientoDomain();
            var lista = Domain.ListarCostos(Sector, Programa, Historico);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarCostoDetalle(int CodigoCosto)
        {
            var Domain = new MantenimientoDomain();
            var lista = new List<ParametricasModel>();
            try
            {
                lista = Domain.ObtenerDetalleCosto(CodigoCosto);
            }
            catch (Exception ex)
            {   
                throw;
            }

            
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult RegistrarMatrizCostos(MatrizCostos MatrizCosto)
        {
            var Domain = new MantenimientoDomain();
            var Result = new ResponseInfoModel();

            if (Usuario is null)
            {
                Result.TipoError = "0";
                Result.Message = "Session caducada";
                ViewBag.Result = Result;
                return Json(Result, JsonRequestBehavior.AllowGet);

            }

            try
            {
                Domain.RegistrarCosto(MatrizCosto, Usuario.User);

                Result.TipoError = "100";
                Result.Message = "Datos guardados Exitosamente!";
                ViewBag.Result = Result;
            }
            catch (Exception ex)
            {
                Result.TipoError = "99";
                Result.Message = ex.Message;
                ViewBag.Result = Result;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeshabilitarMatrizCostos(int CodigoCosto)
        {
            var Domain = new MantenimientoDomain();
            var Result = new ResponseInfoModel();

            if (Usuario is null)
            {
                Result.TipoError = "0";
                Result.Message = "Session caducada";
                //ViewBag.Result = Result;
                return Json(Result, JsonRequestBehavior.AllowGet);

            }
            try
            {
                Domain.DeshabilitarCosto(CodigoCosto, Usuario.User);

                Result.TipoError = "100";
                Result.Message = "Datos guardados Exitosamente!";
                //ViewBag.Result = Result;
            }
            catch (Exception ex)
            {
                Result.TipoError = "99";
                Result.Message = ex.Message;
                //ViewBag.Result = Result;
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DescargarMatrizCostos()
        {
            var ivDomain = new MantenimientoDomain();
            MemoryStream stream = ivDomain.DescargarMatrizCostos();
            var fileName = $"MatrizCostos_{DateTime.Now.ToString("yyyyMMdd")}";
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", $"inline;filename={fileName}.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
            return null;
        }


        #endregion


    }
}
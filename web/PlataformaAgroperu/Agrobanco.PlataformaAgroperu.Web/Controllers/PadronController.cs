﻿using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using System;
using System.IO;
using System.Web.Mvc;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class PadronController : BaseController
    {
        // GET: Padron
        public ActionResult NuevoPadron()
        {
            if(Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();
        }
        public ActionResult ConsultaPadron()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        public JsonResult GrabarPadron(string pcabecera, string FechaCliente)
        {
            ExcelResponseModel respon = new ExcelResponseModel();
            if (Request.Files.Count != 1) return Json(new ExcelResponseModel("01|Error Archivo Adjunto||"), JsonRequestBehavior.AllowGet);
            var fileExcel = Request.Files[0];
            var memorystream = new MemoryStream();
            var ivDomain = new PadronDomain();
            var fileExtension = fileExcel?.FileName.Split('.');
            fileExcel?.InputStream.CopyTo(memorystream);
            memorystream.Position = 0;
            try
            {
                if (fileExtension != null)
                {
                    Usuario oUsuario = Usuario;
                    ivDomain.SubirPadronFromDocument(ref respon, oUsuario, pcabecera, memorystream, fileExtension[fileExtension.Length - 1], FechaCliente);
                }
            }
            catch (Exception ex)
            {
                return Json(respon, JsonRequestBehavior.AllowGet);
                //throw;
            }
            return Json(respon, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConsultarPadronPorAgencia(string CodAgencia)
        {
            var padronDomain = new PadronDomain();
            var FlagFiltro = "0";
            var lstPadron = padronDomain.ListarPadronPorAgencia(CodAgencia, FlagFiltro);
            return Json(lstPadron, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DescargarPadron(int CodigoPadron, string NombrePadron)
        {
            var ivDomain = new PadronDomain();
            var respon = new ExcelResponseModel();
            MemoryStream ms = ivDomain.DescargarPadron(respon, CodigoPadron);
            PadronStreamResponse(CodigoPadron.ToString(), NombrePadron, ms);
            return null;
        }
        private void PadronStreamResponse(string fileCode, string fileName, MemoryStream stream)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", $"inline;filename={fileName}_{fileCode}.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Domain.Common;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using Agrobanco.PlataformaAgroperu.Web.Models;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class ParametricasController : BaseController
    {
        // GET: Parameters
        [HttpPost]
        public ActionResult ListarParametricasGrupo()
        {
            var parametricasDomain = new ParametricasDomain();
            Usuario oUsuario = Usuario;
            var lista = parametricasDomain.ListarParametricasGrupo(oUsuario.CodigoAgencia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarParametricasMatrizCostos()
        {
            var parametricasDomain = new ParametricasDomain();
            Usuario oUsuario = Usuario;
            var lista = parametricasDomain.ListarParemetricasMatrizCostos();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarComisionRegante(string CodJuntaRegante)
        {
            var parametricasDomain = new ParametricasDomain();
            var lista = parametricasDomain.ListarComisionRegante(CodJuntaRegante);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarOrganizaciones(int CodigoAgencia = 0)
        {
            CodigoAgencia = CodigoAgencia == 0 ? Usuario.CodigoAgencia : CodigoAgencia;

            var parametricasDomain = new ParametricasDomain();
            var lista = parametricasDomain.ListarOrganizaciones(CodigoAgencia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarComisiones(string CodigoOrganizacion)
        {
            var parametricasDomain = new ParametricasDomain();
            var lista = parametricasDomain.ListarComisiones(CodigoOrganizacion);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarUbigeos(string Ubigeo)
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarUbigeos(Ubigeo);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarDepartamentos()
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarDepartamentos();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarProvinciaPorDep(string CodDepartamento)
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarProvincias(CodDepartamento);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarDistritoPorProv(string CodProvincia)
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarDistritos(CodProvincia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarAgencias()
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarAgencias(Usuario.CodigoAgencia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarFuncionarios(int CodAgencia)
        {
            var grupoDomain = new ParametricasDomain();
            var lista = grupoDomain.ListarFuncionarios(CodAgencia);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarCultivos()
        {            
            var lista = ParametricasDomain.ListarCultivos();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarCultivosPorSector(string CodigoSector)
        {
            var lista = ParametricasDomain.ListarProductos(CodigoSector);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListarProductosPorSectorPrograma(string CodigoSector, string CodigoPrograma)
        {
            var lista = ParametricasDomain.ListarProductos(CodigoSector, CodigoPrograma);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


    }
}

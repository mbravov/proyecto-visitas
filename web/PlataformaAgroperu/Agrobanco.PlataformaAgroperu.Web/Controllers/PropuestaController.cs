﻿using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Web.Models;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Web.Mvc;
using System.Xml.Linq;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;
using Rectangle = iTextSharp.text.Rectangle;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    class _events : PdfPageEventHelper
    {
        public string nro_propuesta { get; set; }
        public string fpropuesta { get; set; }
        public string estado { get; set; }
        public string agencia { get; set; }
        public string autonomia { get; set; }
        public string analista { get; set; }
        public string ngrupo { get; set; }
        public Font fBold { get; set; }
        public Font font { get; set; }
        public Image logo { get; set; }

        public _events()
        {
        }
        public _events(string pnpropuesta, string pfpropuesta, string pestado, string pagencia, string pautonomia, string panalista, Font pfontBold, Font pfont, Image plogo, string grupo)
        {
            nro_propuesta = pnpropuesta;
            fpropuesta = pfpropuesta;
            estado = pestado;
            agencia = pagencia;
            autonomia = pautonomia;
            fBold = pfontBold;
            font = pfont;
            logo = plogo;
            analista = panalista;
            ngrupo = grupo;
        }
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            var p = new Paragraph { Font = new Font(fBold) { Size = 11F } };
            p.Add("PROPUESTA DE CREDITO \n\r");
            p.Add("Fondo Agroperú \n\r");
            p.Font = new Font(font) { Size = 9F };

            var TextoAgroperu = new Paragraph { Font = new Font(fBold) { Size = 11F } };
            TextoAgroperu.Add("Fondo Agroperú \n\r");
            TextoAgroperu.Font = new Font(font) { Size = 9F };

            var bg_celda_titulo = new BaseColor(System.Drawing.Color.Silver);
            var bg_cabecera = new BaseColor(96, 96, 96); //System.Drawing.Color.Black);

            PdfPCell cell;
            PdfPTable table = new PdfPTable(new float[] { 1, 1, 0.8f, 1.4f, 0.6f, 0.7f });

            cell = new PdfPCell(logo);
            cell.Rowspan = 1;
            cell.Colspan = 2;
            cell.Border = Rectangle.NO_BORDER;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);

            MostrarCelda(table, "N° de Propuesta", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, nro_propuesta, font);
            MostrarCelda(table, "FGeneración", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, fpropuesta, font);

            
            cell = new PdfPCell(p);//1,1
            cell.HorizontalAlignment = 1;
            cell.Rowspan = 4;
            cell.Colspan = 2;
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);


            MostrarCelda(table, "Estado", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, estado, font);
            MostrarCelda(table, "Agencia", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, agencia, font);

            MostrarCelda(table, "Autonomia", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, autonomia, font, 1, null, 3, 0);

            MostrarCelda(table, "Funcionario", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, analista, font, 1, null, 3, 0);

            ////cell = new PdfPCell();//1,1
            ////cell.HorizontalAlignment = 1;
            ////cell.Rowspan = 1;
            ////cell.Colspan = 2;
            ////cell.Border = Rectangle.NO_BORDER;
            ////table.AddCell(cell);
            

            //cell = new PdfPCell();//1,1
            //cell.HorizontalAlignment = 1;
            ////cell.Rowspan = 3;
            //cell.Colspan = 0;
            //cell.Border = Rectangle.NO_BORDER;
            //table.AddCell(cell);

            MostrarCelda(table, "N° de Grupo", fBold, 0, bg_celda_titulo);
            MostrarCelda(table, ngrupo, font, 1, null, 3, 0);

            document.Add(table);
            document.Add(new Paragraph(Environment.NewLine));

        }
        public void MostrarCelda(PdfPTable table, string texto, Font font, int haligment = 1, BaseColor bg = null, int colspan = 1, int rowspan = 1)
        {
            if (string.IsNullOrWhiteSpace(texto))
            {
                texto = "  ";
            }
            PdfPCell cell = new PdfPCell(new Phrase(texto, new Font(font))) { HorizontalAlignment = haligment, BackgroundColor = bg };
            cell.Colspan = colspan;
            cell.Rowspan = rowspan;

            table.AddCell(cell);
        }
    }
    public class PropuestaController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Generar(string CodSolicitud)
        {
            ExcelResponseModel respon = new ExcelResponseModel();
            try
            {
                var ivDomain = new PropuestaDomain();
                string respuesta = ivDomain.GenerarPropuesta(respon, CodSolicitud);

                respon.code = "99";
                respon.message = respuesta;
            }
            catch (Exception ex)
            {
                respon.code = "10";
                respon.message = ex.Message;
                return Json(respon, JsonRequestBehavior.AllowGet);
            }

            return Json(respon, JsonRequestBehavior.AllowGet);
        }

        public void MostrarCelda(PdfPTable table, string texto, Font font, int haligment = 1, BaseColor bg = null, int colspan = 1, int rowspan = 1)
        {
            if (string.IsNullOrWhiteSpace(texto))
            {
                texto = "  ";
            }
            PdfPCell cell = new PdfPCell(new Phrase(texto, new Font(font))) { HorizontalAlignment = haligment, BackgroundColor = bg };
            cell.Colspan = colspan;
            cell.Rowspan = rowspan;

            table.AddCell(cell);
        }
        public string Completa(string cadena)
        {
            if (cadena == "S")
            {
                return "SI";
            }
            else if (cadena == "N")
            {
                return "NO";
            }
            return "";
        }

        
        public void CreatePdf(string cod_solicitud)
        {
            if (Usuario == null)
                Response.End();

            ExcelResponseModel respon = new ExcelResponseModel();
            var propuestaDomain = new PropuestaDomain();
            var firmRepoPathBase = ApplicationKeys.FirmaRepositoryPath;
            var propuesta = propuestaDomain.ObtenerDatosPropuesta(respon, cod_solicitud);
            var fontColor = new BaseColor(0, 0, 0);
            var fontColor_Titulo = new BaseColor(255, 255, 255);
            var parametros = GetParametros();

            var stream = new MemoryStream();

            //CONFIG PAGES


            var baseFont = parametros.BaseFont;
            var baseFontBold = parametros.BaseFontBold;

            var rec = new Rectangle(PageSize.A4);
            var doc = new Document(rec, -50, -50, 20, 20);



            //PAGE 1
            // #region page1

            var imageUrl = $"{Server.MapPath("..")}/Content/img/banner-agrobanco.jpg";
            var logoAgrobanco = Image.GetInstance(imageUrl);
            logoAgrobanco.ScaleToFit(90F, 50F);
            logoAgrobanco.SpacingAfter = 0F;
            logoAgrobanco.Alignment = Element.ALIGN_RIGHT;
            Font fontBold = FontFactory.GetFont("TIMES_ROMAN", 9, 1, fontColor);
            Font font = FontFactory.GetFont("TIMES_ROMAN", 9, 0, fontColor);
            Font font_Titulo = FontFactory.GetFont("TIMES_ROMAN", 9, 1, fontColor_Titulo);
            _events e = new _events(propuesta.NroPropuesta_01, ConvertToDateTime(propuesta.FechaGenPropuesta_02), propuesta.Estado_03, propuesta.Agencia_04, propuesta.Autonomia_05, propuesta.Analista_53, fontBold, font, logoAgrobanco, propuesta.NroGrupo_00);
            PdfWriter writer = PdfWriter.GetInstance(doc, stream);
            writer.PageEvent = e;
            doc.Open();

            var bg_cabecera = new BaseColor(128, 128, 128);// System.Drawing.Color.Black);
            var bg_celda_titulo = new BaseColor(System.Drawing.Color.Silver);//System.Drawing.Color.Silver);
            PdfPCell cell;

            //doc.Add(new Paragraph(Environment.NewLine));

            //Informacion solicitante
            PdfPTable tableInfo = new PdfPTable(1);
            MostrarCelda(tableInfo, "I. Información del Solicitante", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo);
            string descliente = "";
            if (propuesta.EsPEP == "SI")
            {
                descliente = propuesta.Nombre_RzSocial_06;
                PdfPTable tableInf = new PdfPTable(new float[] { 4, 0.5f, 1, 1, 1, 1 });//PdfPTable(5);
                MostrarCelda(tableInf, "Nombre / Razon Social", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "PEP", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "DNI / RUC", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Cliente", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Código", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Edad", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, descliente, font);
                MostrarCelda(tableInf, "SI", font);
                MostrarCelda(tableInf, propuesta.TipoDocumento_07, font);
                MostrarCelda(tableInf, propuesta.Cliente_08, font);
                MostrarCelda(tableInf, propuesta.Codigo_09, font);
                MostrarCelda(tableInf, propuesta.Edad_10, font);
                doc.Add(tableInf);
            }
            else
            {
                descliente = propuesta.Nombre_RzSocial_06;
                PdfPTable tableInf = new PdfPTable(new float[] { 4, 1, 1, 1, 1 });//PdfPTable(5);
                MostrarCelda(tableInf, "Nombre / Razon Social", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "DNI / RUC", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Cliente", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Código", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, "Edad", fontBold, 1, bg_celda_titulo);
                MostrarCelda(tableInf, descliente, font);
                MostrarCelda(tableInf, propuesta.TipoDocumento_07, font);
                MostrarCelda(tableInf, propuesta.Cliente_08, font);
                MostrarCelda(tableInf, propuesta.Codigo_09, font);
                MostrarCelda(tableInf, propuesta.Edad_10, font);
                doc.Add(tableInf);
            }

            PdfPTable tableInf2 = new PdfPTable(4);
            MostrarCelda(tableInf2, "Departamento", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf2, "Provincia", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf2, "Distrito", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf2, "Comisión", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf2, propuesta.Departamento_11, font);
            MostrarCelda(tableInf2, propuesta.Provincia_12, font);
            MostrarCelda(tableInf2, propuesta.Distrito_13, font);
            MostrarCelda(tableInf2, propuesta.Comision_14, font);
            doc.Add(tableInf2);

            PdfPTable tableInf3 = new PdfPTable(2);
            MostrarCelda(tableInf3, "Dirección Predio", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf3, "Dirección Solicitante", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInf3, propuesta.Dir_Predio_14_1, font);
            MostrarCelda(tableInf3, propuesta.Dir_solicitante_14_2, font);
            doc.Add(tableInf3);

            doc.Add(new Paragraph(Environment.NewLine));

            //datos de la propuesta
            PdfPTable tableInfo1 = new PdfPTable(1);
            MostrarCelda(tableInfo1, "II. Datos de la Propuesta", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo1);
            PdfPTable tableProp1 = new PdfPTable(new float[] { 3, 1.5f, 1.5f, 0.5f, 1.5f, 0.7f, 0.5f });
            MostrarCelda(tableProp1, "Producto", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Destino", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Monto Aprobado", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Plazo", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Forma de Pago", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Moneda", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, "Tasa", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp1, propuesta.Producto_15, font);
            MostrarCelda(tableProp1, propuesta.Destino_16, font);
            MostrarCelda(tableProp1, propuesta.MontoSolicitado_17, font);
            MostrarCelda(tableProp1, propuesta.Plazo_18, font);
            MostrarCelda(tableProp1, propuesta.FormaPago_19, font);
            MostrarCelda(tableProp1, propuesta.Moneda_20, font);
            MostrarCelda(tableProp1, propuesta.Tasa_21, font);
            doc.Add(tableProp1);

            PdfPTable tableProp2 = new PdfPTable(new float[] { 0.9f, 0.6f, 3.1f, 1.1f});
            MostrarCelda(tableProp2, "SegDesgravamen", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp2, "SegAgricola", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp2, "Asistente Técnico", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp2, "Pago Asistente Técnico", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableProp2, propuesta.SegDegravamen_23, font);
            MostrarCelda(tableProp2, Completa(propuesta.SegAgricola_24), font);
            MostrarCelda(tableProp2, propuesta.Gestor_25, font);
            MostrarCelda(tableProp2, propuesta.PagoAsistenteTecnico, font);
            doc.Add(tableProp2);

            doc.Add(new Paragraph(Environment.NewLine));

            //garantias
            PdfPTable tableInfo2 = new PdfPTable(1);
            MostrarCelda(tableInfo2, "III. Garantías", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo2);
            //PdfPTable tableGarantias = new PdfPTable(new float[] { 0.6f, 3.2f,x 0.5f, 0.6f, 1.1f, 0.5f, 1, 1, 1 });
            PdfPTable tableGarantias = new PdfPTable(new float[] { 0.6f, 3.2f, 1, 0.5f });
            MostrarCelda(tableGarantias, "Tipo", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableGarantias, "Descripción", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableGarantias, "Valor de Gravamen", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableGarantias, "Póliza", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableGarantias, "", fontBold, 1, bg_celda_titulo);//NumGarantía
            //MostrarCelda(tableGarantias, "", fontBold, 1, bg_celda_titulo);//VG
            //MostrarCelda(tableGarantias, "", fontBold, 1, bg_celda_titulo);//FTasación
            //MostrarCelda(tableGarantias, "", fontBold, 1, bg_celda_titulo);//#Partida
            //MostrarCelda(tableGarantias, "", fontBold, 1, bg_celda_titulo);//Of.Registral

            if (propuesta.Gar_Tipo_29 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_Tipo_29, font);
            }
            if (propuesta.Gar_Descripcion_30 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_Descripcion_30, font);
            }
            if (propuesta.Gar_VRI_32 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_VRI_32, font);
            }
            
            MostrarCelda(tableGarantias, Completa(propuesta.Gar_Poliza_34), font);
            //MostrarCelda(tableGarantias, propuesta.NumGarantia_28, font);
            //MostrarCelda(tableGarantias, propuesta.Gar_VG_31, font);
            //MostrarCelda(tableGarantias, ConvertToDateTime(propuesta.Gar_FTasacion_33), font);
            //MostrarCelda(tableGarantias, propuesta.Gar_NroPartida_35, font);
            //MostrarCelda(tableGarantias, propuesta.Gar_OfRegistral_36, font);
            //GARANTIA 2
            if (propuesta.Gar_Tipo_29_1 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_Tipo_29_1, font);
            }
            if (propuesta.Gar_Descripcion_30_1 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_Descripcion_30_1, font);
            }

            if (propuesta.Gar_VRI_32_1 != "0")
            {
                MostrarCelda(tableGarantias, propuesta.Gar_VRI_32_1, font);
            }
            
            MostrarCelda(tableGarantias, Completa(propuesta.Gar_Poliza_34_1), font);
            //MostrarCelda(tableGarantias, propuesta.NumGarantia_28_1, font);
            //MostrarCelda(tableGarantias, propuesta.Gar_VG_31_1, font);
            //MostrarCelda(tableGarantias, " ", font);
            //MostrarCelda(tableGarantias, " ", font);
            //MostrarCelda(tableGarantias, " ", font);
            doc.Add(tableGarantias);

            doc.Add(new Paragraph(Environment.NewLine));

            //aval
            PdfPTable tableInfo21 = new PdfPTable(1);
            MostrarCelda(tableInfo21, "IV. Aval/Fianza", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo21);
            PdfPTable tableInfo211 = new PdfPTable(new float[] { 1, 2, 2, 3 });
            MostrarCelda(tableInfo211, "Tipo", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInfo211, "Descripción", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInfo211, "Documento", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInfo211, "Nombres y Apellidos", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableInfo211, propuesta.Gar_Tipo_29_2_AVA, font);
            MostrarCelda(tableInfo211, propuesta.Gar_Descripcion_30_1_AVA, font);
            MostrarCelda(tableInfo211, propuesta.Gar_Documento_30_2_AVA, font);
            MostrarCelda(tableInfo211, propuesta.Gar_Nombre_Avalador_30_3_AVA, font);

            doc.Add(tableInfo211);

            doc.Add(new Paragraph(Environment.NewLine));

            //desembolso
            PdfPTable tableInfo3 = new PdfPTable(1);
            MostrarCelda(tableInfo3, "V. Plan de Desembolso", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo3);

            PdfPTable tableDesembolso = new PdfPTable(new float[] { 1, 1, 3, 1, 1 });
            MostrarCelda(tableDesembolso, "F/Ini", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableDesembolso, "F/Fin", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableDesembolso, "Etapa", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableDesembolso, "%", fontBold, 1, bg_celda_titulo);            
            MostrarCelda(tableDesembolso, "Total Fin.", fontBold, 1, bg_celda_titulo);

            //MostrarCelda(tableDesembolso, ConvertToDateTime(propuesta.Desembolso_Fini_37), font);
            //MostrarCelda(tableDesembolso, ConvertToDateTime(propuesta.Desembolso_Ffin_38), font);
            //MostrarCelda(tableDesembolso, propuesta.Gar_Etapa_39, font);
            //MostrarCelda(tableDesembolso, propuesta.Gar_Porcentaje_40, font);            
            //MostrarCelda(tableDesembolso, propuesta.Gar_TotalFin_43, font);

            //MostrarCelda(tableDesembolso, ConvertToDateTime(propuesta.Desembolso_Fini_37_1), font);
            //MostrarCelda(tableDesembolso, ConvertToDateTime(propuesta.Desembolso_Ffin_38_1), font);
            //MostrarCelda(tableDesembolso, propuesta.Gar_Etapa_39_1, font);
            //MostrarCelda(tableDesembolso, propuesta.Gar_Porcentaje_40_1, font);            
            //MostrarCelda(tableDesembolso, propuesta.Gar_TotalFin_43_1, font);

            foreach (var item in propuesta.PlanDesembolsos)
            {
                MostrarCelda(tableDesembolso, ConvertToDateTime(item.FechaInicio), font);
                MostrarCelda(tableDesembolso, ConvertToDateTime(item.FechaFin), font);
                MostrarCelda(tableDesembolso, item.DescripcionEtapa, font);
                MostrarCelda(tableDesembolso, item.Porcentaje, font);
                MostrarCelda(tableDesembolso, item.TotalFinanciado, font);
            }

            doc.Add(tableDesembolso);

            

            doc.Add(new Paragraph(Environment.NewLine));
            //comentarios
            //doc.Add(new Paragraph(Environment.NewLine));


            //Indicadores del solicitante
            PdfPTable tableInfo7 = new PdfPTable(1);
            MostrarCelda(tableInfo7, "VI. Indicadores del Solicitante", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo7);
            PdfPTable tableIndicadores = new PdfPTable(new float[] { 2.5f, 3f, 1.2f });
            MostrarCelda(tableIndicadores, "Solicitante", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores, "Calificación Financiera", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores, "Calif Interna", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores, "Numero de Entidades", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores, "RLAFT", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores, propuesta.Solicitante_58, font);
            MostrarCelda(tableIndicadores, propuesta.Calif_SBS_59, font);
            //MostrarCelda(tableIndicadores, propuesta.Calif_Interna_60, font);
            MostrarCelda(tableIndicadores, propuesta.NumeroEntidades_61, font);
            //MostrarCelda(tableIndicadores, propuesta.RLAFT_62, font);
            doc.Add(tableIndicadores);

            //cabeceras2
            PdfPTable tableIndicadores2 = new PdfPTable(4);
            //MostrarCelda(tableIndicadores2, "Patrimonio", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores2, "Otros Ingresos", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores2, "Monto Solicitado", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores2, "Exposición Vigente en el Fondo Agroperú", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores2, "Exposición Total", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores2, "Exposición en el Sistema Financiero (sin incluir el FondoAgroperu)", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores2, propuesta.Patrimonio_63, font);
            //MostrarCelda(tableIndicadores2, propuesta.OtrosIngresos_64, font);
            MostrarCelda(tableIndicadores2, propuesta.ExpoPropuesta_65, font);
            MostrarCelda(tableIndicadores2, propuesta.ExposicionVigente_66, font);
            MostrarCelda(tableIndicadores2, propuesta.ExposicionTotal_67, font);
            MostrarCelda(tableIndicadores2, propuesta.DeudaSistema_68, font);
            doc.Add(tableIndicadores2);

            PdfPTable tableIndicadores3 = new PdfPTable(new float[] { 1.5f, 3f, 1.2f, 1.2f });
            MostrarCelda(tableIndicadores3, "Conyugue", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores3, "Calificación Financiera", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores3, "Calif Interna", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores3, "Numero de Entidades", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores3, "DeudaSistema", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores3, propuesta.Conyuguea_69, font);
            MostrarCelda(tableIndicadores3, propuesta.Califsbs_70, font);
            //MostrarCelda(tableIndicadores3, propuesta.Caliinterna_71, font);
            MostrarCelda(tableIndicadores3, propuesta.NumeroEntidades_72, font);
            MostrarCelda(tableIndicadores3, propuesta.Deudasistema_73, font);
            doc.Add(tableIndicadores3);

            doc.Add(new Paragraph(Environment.NewLine));

            //Indicadores de la propuesta
            PdfPTable tableInfo8 = new PdfPTable(1);
            MostrarCelda(tableInfo8, "VII. Indicadores de la propuesta", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo8);

            PdfPTable tableIndicadores4 = new PdfPTable(new float[] { 2.7f, 0.7f, 1.2f, 0.8f, 1.2f, 1 });
            MostrarCelda(tableIndicadores4, "PRODUCTO", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, "ExpSector", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, "Unidad de Prod (UP)", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, "UP Total", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, "UP a Financiar", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, "Calificación LA/FT", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores4, propuesta.Producto_74, font);
            MostrarCelda(tableIndicadores4, propuesta.ExpSector_75 + " Años", font);
            MostrarCelda(tableIndicadores4, propuesta.UnidadProd_76, font);
            MostrarCelda(tableIndicadores4, propuesta.UP_Total_77, font);
            MostrarCelda(tableIndicadores4, propuesta.UP_Financiar_78, font);
            MostrarCelda(tableIndicadores4, propuesta.CalificacionLAFT_78A, font);
            doc.Add(tableIndicadores4);
            PdfPTable tableIndicadores5 = new PdfPTable(8);
            MostrarCelda(tableIndicadores5, "Producción x UP", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Unidad Medida (UM)", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Producción Total", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Precio (S/ x UM)", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Costo (S/ x UM)", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Ingreso", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Costo", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, "Benef / Costo", fontBold, 1, bg_celda_titulo);
            MostrarCelda(tableIndicadores5, propuesta.ProduccionxUP_79, font);
            MostrarCelda(tableIndicadores5, propuesta.UnidadMedida_80, font);
            MostrarCelda(tableIndicadores5, propuesta.ProduccionTotal_81, font);
            MostrarCelda(tableIndicadores5, propuesta.Precio_82, font);
            MostrarCelda(tableIndicadores5, propuesta.Costo_83, font);
            MostrarCelda(tableIndicadores5, propuesta.Ingreso_84, font);
            MostrarCelda(tableIndicadores5, propuesta.Costo_85, font);
            MostrarCelda(tableIndicadores5, propuesta.BeneficioCosto_86, font);
            doc.Add(tableIndicadores5);
            doc.NewPage();


            //Aprobado por Excepción SI/NO
            PdfPTable tableInfoAprobacionExcepcion = new PdfPTable(1);
            MostrarCelda(tableInfoAprobacionExcepcion, $"VIII. Aprobado por Excepción: {(propuesta.AprobadoExcepcion == 1 ? "SÍ" : "NO")}", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfoAprobacionExcepcion);

            if (propuesta.AprobadoExcepcion == 1)
            {
                PdfPTable tableComentarioAnalista = new PdfPTable(1);
                tableComentarioAnalista.AddCell(new PdfPCell(new Phrase("ANALISTA COMERCIAL", new Font(fontBold))) { HorizontalAlignment = 0, BackgroundColor = bg_celda_titulo });
                tableComentarioAnalista.AddCell(new PdfPCell(new Phrase(propuesta.ComentarioAnalista, new Font(baseFont))) { HorizontalAlignment = 0 });
                doc.Add(tableComentarioAnalista);
                
                PdfPTable tableComentarioJefe = new PdfPTable(1);
                tableComentarioJefe.AddCell(new PdfPCell(new Phrase("ADMINISTRADOR DE AGENCIA", new Font(fontBold))) { HorizontalAlignment = 0, BackgroundColor = bg_celda_titulo });
                tableComentarioJefe.AddCell(new PdfPCell(new Phrase(propuesta.ObservacionJefe, new Font(baseFont))) { HorizontalAlignment = 0 });
                doc.Add(tableComentarioJefe);

            }
            

            doc.Add(new Paragraph(Environment.NewLine));

            string titulo9 = "IX.";
            string titulo10 = "X.";
            string titulo11 = "XI.";
            //nuevo campo movido
            if (propuesta.linstancias.Count > 0)
            {

                PdfPTable tableInfo4 = new PdfPTable(1);
                MostrarCelda(tableInfo4, titulo9 + " Comentarios del flujo de aprobación de la Propuesta", font_Titulo, 0, bg_cabecera);                
                doc.Add(tableInfo4);
                PdfPTable headerTable2 = new PdfPTable(1);
                string cad = "";
                string palabra = "";
                //bool continuar = true;
                int i = 0;
                foreach (var item in propuesta.linstancias)
                {
                    i = 0;
                    headerTable2 = new PdfPTable(1);
                    headerTable2.AddCell(new PdfPCell(new Phrase(item.Usuario, new Font(fontBold))) { HorizontalAlignment = 0, BackgroundColor = bg_celda_titulo });
                    doc.Add(headerTable2);
                    headerTable2 = new PdfPTable(1);
                    cad = item.Comentario;

                    while (cad.Length >= 78)
                    {
                        palabra = cad.Substring(0, 78);
                        cad = cad.Substring(78, cad.Length - 78);
                        headerTable2.AddCell(new PdfPCell(new Phrase(palabra, new Font(baseFont))) { HorizontalAlignment = 0 });
                    }
                    palabra = cad.Substring(0, cad.Length);
                    headerTable2.AddCell(new PdfPCell(new Phrase(palabra, new Font(baseFont))) { HorizontalAlignment = 0 });
                    doc.Add(headerTable2);
                    
                }
                doc.Add(new Paragraph(Environment.NewLine));
            }
            else
            {

                titulo10 = "IX.";
                titulo11 = "X.";
            }

            //resolucion
            PdfPTable tableInfo5 = new PdfPTable(1);
            MostrarCelda(tableInfo5, titulo10 + " Resolución", font_Titulo, 0, bg_cabecera);
            doc.Add(tableInfo5);
            PdfPTable tableResolucion = new PdfPTable(1);
            //MostrarCelda(tableResolucion, "Condiciones Especiales", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableResolucion, propuesta.CondicionesEspeciales_44, font);
            doc.Add(tableResolucion);

            //doc.Add(new Paragraph(Environment.NewLine));
            //doc.Add(new Paragraph(Environment.NewLine));
            PdfPTable tab1 = new PdfPTable(8);
            MostrarCelda(tab1, "Fecha", font);
            MostrarCelda(tab1, propuesta.Fecha_47, font);
            MostrarCelda(tab1, " ", font);
            MostrarCelda(tab1, "Resolución", font);
            MostrarCelda(tab1, propuesta.Aprobado_48, font);
            MostrarCelda(tab1, " ", font);
            MostrarCelda(tab1, "Nro Acta ", font);
            MostrarCelda(tab1, propuesta.Nro_acta_49, font);
            doc.Add(tab1);
            doc.Add(new Paragraph(Environment.NewLine));
            PdfPTable table1 = new PdfPTable(4);
            MostrarCelda(table1, titulo11 + " Firma y Sello de Comité", font_Titulo, 0, bg_cabecera, 2);
            MostrarCelda(table1, "Autonomía", font_Titulo, 0, bg_cabecera);
            MostrarCelda(table1, propuesta.Autonomia_51, font);
            doc.Add(table1);
            doc.Add(new Paragraph(Environment.NewLine));
            doc.Add(new Paragraph(Environment.NewLine));
            doc.Add(new Paragraph(Environment.NewLine));
            doc.Add(new Paragraph(Environment.NewLine));
            PdfPTable table2 = new PdfPTable(4);
            MostrarCelda(table2, "Gerente General o Integrante 1", font);
            MostrarCelda(table2, "Gerente Riesgos o Integrante 2", font);
            MostrarCelda(table2, "Gerente Comercial o Integrante 3", font);
            MostrarCelda(table2, "Integrante 4", font);
            //MostrarCelda(table2," ", font, 1, null, 4);
            //MostrarCelda(table2," ", font, 1, null, 4);
            //MostrarCelda(table2," ", font, 1, null, 4);
            //table2.DefaultCell.Border = Rectangle.NO_BORDER;
            doc.Add(table2);
            doc.Add(new Paragraph(Environment.NewLine));
            PdfPTable table3 = new PdfPTable(4);
            //cell = new PdfPCell(new Phrase(" "));
            //cell.Border = Rectangle.NO_BORDER;
            //table3.AddCell(cell);
            cell = new PdfPCell(new Phrase(" "));
            cell.Border = Rectangle.NO_BORDER;
            table3.AddCell(cell);
            cell = new PdfPCell(new Phrase(" "));
            //cell.Border = Rectangle.LEFT_BORDER;
            //cell.Border = Rectangle.TOP_BORDER;
            //cell.Border = Rectangle.BOTTOM_BORDER;
            cell.Border = Rectangle.NO_BORDER;
            table3.AddCell(cell);
            // MostrarCelda(table3, "", font);
            //MostrarCelda(table3, "", font);
            //MostrarCelda(table3, "Fecha Impresión", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(table3, ConvertToDateTime(propuesta.FechaImpresion_52), font);
            // table3.WidthPercentage = 30.0f;            
            // table3.HorizontalAlignment = Element.ALIGN_RIGHT;
            doc.Add(table3);
            //PdfDiv div = new PdfDiv();
            //div.AddElement(table3);
            //div.Float=PdfDiv.FloatType.RIGHT;
            ////div.PaddingLeft = 100;
            //doc.Add(div);

            doc.Add(new Paragraph(Environment.NewLine));
            doc.Add(new Paragraph(Environment.NewLine));



            //Hectareas de riego
            //PdfPTable tableInfo9 = new PdfPTable(1);
            //MostrarCelda(tableInfo9, "XII. Hectareas de Riego (RADA)", font_Titulo, 0, bg_cabecera);
            //doc.Add(tableInfo9);
            //PdfPTable tableIndicadores6 = new PdfPTable(4);
            //MostrarCelda(tableIndicadores6, "HAS que posee", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores6, "HAS con permiso de agua", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores6, "HAS con riego cultivo a financiar", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores6, "HAS financiadas", fontBold, 1, bg_celda_titulo);
            //MostrarCelda(tableIndicadores6, propuesta.HASposee_87, font);
            //MostrarCelda(tableIndicadores6, propuesta.HASpermiso_agua_88, font);
            //MostrarCelda(tableIndicadores6, propuesta.HASriego_cultivo_89, font);
            //MostrarCelda(tableIndicadores6, propuesta.HASfinanciadas_90, font);
            //doc.Add(tableIndicadores6);



            //cell = new PdfPCell(new Phrase("Row 2 ,Col 1"));
            //table.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Row 2 and row 3, Col 2 and Col 3"));
            //cell.Rowspan = 2;
            //cell.Colspan = 2;
            //table.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Row 3, Col 1"));
            //table.AddCell(cell);

            //doc.Add(table);



            doc.Close();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", "inline;filename=propuesta.pdf");
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        public ParametrosPdf GetParametros()
        {
            var item = new ParametrosPdf();

            var config = XElement.Load(Server.MapPath("..") + "/Content/xml/Propuesta.xml");

            var baseFont = config.Element("BaseFont");
            if (baseFont == null) return item;
            var fontFamily = baseFont.Element("FontFamily").Attribute("name").Value;
            var fontSize = Convert.ToSingle(baseFont.Element("FonSize").Attribute("name").Value);
            var r = Convert.ToInt32(baseFont.Element("FontColor").Attribute("r").Value);
            var g = Convert.ToInt32(baseFont.Element("FontColor").Attribute("g").Value);
            var b = Convert.ToInt32(baseFont.Element("FontColor").Attribute("b").Value);
            var fontColor = new BaseColor(r, g, b);

            item.BaseFont = FontFactory.GetFont(fontFamily, fontSize, fontColor);


            baseFont = config.Element("BaseFontBold");
            fontFamily = baseFont.Element("FontFamily").Attribute("name").Value;
            fontSize = Convert.ToSingle(baseFont.Element("FonSize").Attribute("name").Value);
            r = Convert.ToInt32(baseFont.Element("FontColor").Attribute("r").Value);
            g = Convert.ToInt32(baseFont.Element("FontColor").Attribute("g").Value);
            b = Convert.ToInt32(baseFont.Element("FontColor").Attribute("b").Value);
            fontColor = new BaseColor(r, g, b);
            var fontBold = Convert.ToInt32(baseFont.Element("FontBold").Attribute("name").Value);

            item.BaseFontBold = FontFactory.GetFont(fontFamily, fontSize, fontBold, fontColor);

            return item;
        }

        public static string ConvertToDateTime(string fecha)
        {
            string result = "";
            if (fecha != null)
            {
                if (fecha.Length == 8)
                {
                    string _day = fecha.Substring(6, 2);
                    string _mes = fecha.Substring(4, 2);
                    string _anio = fecha.Substring(0, 4);
                    result = $"{_day}-{_mes}-{_anio}";
                }
            }
            return result;
        }

    }
}
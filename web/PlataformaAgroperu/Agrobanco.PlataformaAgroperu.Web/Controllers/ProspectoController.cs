﻿using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using Agrobanco.PlataformaAgroperu.Domain.Models.Account;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class ProspectoController : BaseController
    {
        // GET: Prospecto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NuevoProspecto()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            return View();
        }

        //public JsonResult GrabarProspecto(string pcabecera, string FechaCliente)
        //{
        //    ExcelResponseModel respon = new ExcelResponseModel();
        //    if (Request.Files.Count != 1) return Json(new ExcelResponseModel("01|Error Archivo Adjunto||"), JsonRequestBehavior.AllowGet);
        //    var fileExcel = Request.Files[0];
        //    var memorystream = new MemoryStream();
        //    var ivDomain = new PadronDomain();
        //    var fileExtension = fileExcel?.FileName.Split('.');
        //    fileExcel?.InputStream.CopyTo(memorystream);
        //    memorystream.Position = 0;
        //    try
        //    {
        //        if (fileExtension != null)
        //        {
        //            Usuario oUsuario = Usuario;
        //            ivDomain.SubirPadronFromDocument(ref respon, oUsuario, pcabecera, memorystream, fileExtension[fileExtension.Length - 1], ApplicationKeys.PlantillaAgrupamiento, FechaCliente);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        respon.message = $"{respon.message} | {ex.Message}";
        //        return Json(respon, JsonRequestBehavior.AllowGet);
        //        //throw;
        //    }
        //    return Json(respon, JsonRequestBehavior.AllowGet);
        //}



        public ActionResult Consulta()
        {
            if(Usuario is null)
                return RedirectToAction("Login", "Account");

            ViewBag.CodigoAgencia = Usuario.CodigoAgencia;
            ViewBag.CodigoFuncionario = Usuario.CodigoFuncionario;
            return View();
        }

        public ActionResult ConsultarProspectoPorAgencia(string CodAgencia)
        {
            var padronDomain = new PadronDomain();
            var FlagFiltro = "9999";
            var lstPadron = padronDomain.ListarPadronPorAgencia(CodAgencia, FlagFiltro);
            return Json(lstPadron, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ConsultarProspectoPorAgenciaUsuario(string CodAgencia, string CodFuncionario)
        //{
        //    var prospectoDomain = new ProspectoDomain();
        //    var lstProspectos = prospectoDomain.ListarProspectosPorAgenciaUsuario(CodAgencia, CodFuncionario);
        //    return Json(lstProspectos, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult DescargarProspecto(int CodigoProspecto, string NombreProspecto)
        {
            var ivDomain = new ProspectoDomain();
            var respon = new ExcelResponseModel();
            MemoryStream ms = ivDomain.DescargarProspecto(respon, CodigoProspecto);
            ProspectoStreamResponse(CodigoProspecto.ToString(), NombreProspecto, ms);
            return null;
            //MemoryStream ms = ExcelHelper.DataToExcel(dt);
            //FileStream fs = new FileStream("C:\\Users\\jangeles\\Desktop\\pruebaa3.xls", FileMode.Create);
            //ms.WriteTo(fs);
            //fs.Close();
            //ms.Close();
            //return Json(respon, JsonRequestBehavior.AllowGet);
        }

        private void ProspectoStreamResponse(string fileCode, string fileName, MemoryStream stream)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", $"inline;filename={fileName}_{fileCode}.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        
    }
}
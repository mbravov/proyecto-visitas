﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrobanco.PlataformaAgroperu.Domain.Models.Seguimiento;
using Agrobanco.PlataformaAgroperu.Domain.Implementations;
using Agrobanco.PlataformaAgroperu.Web.App_Helpers;

namespace Agrobanco.PlataformaAgroperu.Web.Controllers
{
    public class SeguimientoController : BaseController
    {
        // GET: Seguimiento
        public ActionResult Index()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            return View();
        }

        #region Avance de Solicitudes
        public ActionResult AvanceSolicitudes()
        {
            if (Usuario == null)
                return RedirectToAction("Login", "Account");
            //var domainSeguimiento = new SeguimientoDomain();
            //var ListAvance = domainSeguimiento.ListarAvances(Usuario.CodigoAgencia);

            return View();
        }

        [HttpPost]
        public ActionResult ConsultarAvanceSolicitudes()
        {
            var domainSeguimiento = new SeguimientoDomain();
            var ListAvance = domainSeguimiento.ListarAvances(Usuario.CodigoAgencia);

            return Json(ListAvance, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConsultarAvanceSolicitudesPorAgencia(int CodAgencia)
        {
            var domainSeguimiento = new SeguimientoDomain();
            var ListAvance = domainSeguimiento.ListarAvancesPorAgencia(CodAgencia);


            return Json(ListAvance, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ReporteAgroperu

        

        #endregion


    }
}
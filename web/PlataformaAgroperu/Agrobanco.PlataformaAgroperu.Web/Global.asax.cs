﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Agrobanco.PlataformaAgroperu.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //Process.Start("chrome", @"http://www.stackoverflow.net/");
        }
        protected void Session_End(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Response.Redirect(" SessionTimeOut.cshtml");
        }
        //protected void Application_Error(object sender, EventArgs e) {
        //    System.Web.HttpContext.Current.Response.Redirect("SessionTimeOut.cshtml");
        //}

    }
}
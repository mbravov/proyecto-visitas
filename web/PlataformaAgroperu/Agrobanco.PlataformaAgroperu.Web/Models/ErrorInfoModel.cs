﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;

namespace Agrobanco.PlataformaAgroperu.Web.Models
{
    public class ResponseInfoModel
    {
        public Exception Exception { get; set; }
        public string TipoError { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
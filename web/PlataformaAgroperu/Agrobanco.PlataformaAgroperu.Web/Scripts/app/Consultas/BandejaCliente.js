﻿var map;
var Layerfotos;
var LayerPredio = null;
var LayerTotalPredios;
var LayerRiosPeru;
var LayerViasPeru;
var LayerPrediosFuncionario;


$(document).ready(function () {
    var responsiveHelper_datatable_Integrantes = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    /* Config. Tabla Solcitudes */
    var oIntegrante = $('#datatable_Integrantes').DataTable({
        "data": [],        
        "order": [1, "asc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Integrantes) {
                responsiveHelper_datatable_Integrantes = new ResponsiveDatatablesHelper($('#datatable_Integrantes'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Integrantes.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Integrantes.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                'searchable': false,
                'orderable': false,
                //'align': 'center',
                'width': '2%',
                'className': 'text-center',
                "data": "TramaOpciones"
            },
            {
                "data": "NumeroSolicitud",
                'width': '5%'
            },
            //{ "data": "DatosCliente.TipoDocumento.value" },
            {
                "data": "DatosCliente.NroDocumento.value",
                'width': '5%'
            },
            { "data": "DatosCliente.NomPrimer.value" },
            {
                "data": "DatosPredio.Cultivo",
                'width': '15%'
            },
            //{ "data": "DatosCliente.Celular.value" },
            //{ "data": "DatosSolicitud.Moneda" },
            {
                "data": "DatosSolicitud.MontoSolicitadoSoles.value",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                "data": "EstadoEvaluacion",
                "className": "text-center",
                "width": "5%"
            }
            
        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });
    ListarAgencias();
    // Metodos de botones
    $("#CboAgencia").on('change', function (e) {
        var CboAnalista = $("#CboAnalista");
        var dataRequest = {
            CodAgencia: this.value
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarFuncionarios',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {
                CboAnalista.find('option').remove();
                CboAnalista.append($('<option>', { value: "", text: "[Seleccione Comision]" }));
                $.each(response, function (key, value) {
                    CboAnalista.append($('<option>', { value: value.code, text: value.value }));
                });

            },
            error: function () {
                StopLoading();
                CboAnalista.find('option').remove();
                CboAnalista.prepend($('<option></option>').html('Error al Funcionarios'));
            },
            complete: function () {
                StopLoading();
            }
        });
    });
    $('#btnBuscarSolicitudes').click(function () {
        $('#datatable_Integrantes').DataTable().clear().draw();
        google.maps.event.addDomListener(window, 'load', init);
        ListarSolicitudes();
    });

    //Funciones
    function ListarAgencias() {
        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarAgencias',
            //data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                if (response.length > 0) {
                    $.each(response, function (key, value) {
                        $("#CboAgencia").append($('<option>', { value: value.code, text: value.value }));
                    });
                }
            },
            error: function () {
                StopLoading();
            },
            complete: function () {
                StopLoading();
            }
        });
    }
    function ListarSolicitudes() {
        var requestData = {
            CodAgencia: $("#CboAgencia").val(),
            CodAnalista: $("#CboAnalista").val()
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Consultas/ListarSolicitudes',
            data: JSON.stringify(requestData),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                if (response.length > 0) {
                    $('#datatable_Integrantes').DataTable().clear().draw();
                    $('#datatable_Integrantes').dataTable().fnAddData(response);
                } else {
                    ShowInfoBox("Información", "La búsqueda no obtuvo ningun resultado, intente con otros filtros.");
                }
            },
            error: function () {
                StopLoading();
            },
            complete: function () {
                StopLoading();
            }
        });
    }

    
    function init() {
        var options = {
            scrollwheel: false,
            scaleControl: false,
            backgroundColor: "black",
            navigationControlOptions: {
                position: google.maps.ControlPosition.RIGHT,
                style: google.maps.NavigationControlStyle.DEFAULT
            },
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
            

        };
        map = new google.maps.Map(document.getElementById("map"), options);
        map.setCenter(new google.maps.LatLng(-13, -76.5));
        map.setZoom(5);
        map.setMapTypeId(google.maps.MapTypeId.HYBRID);

        
        //Define el WMS capas Agrobanco
        Layerfotos = GetLayer('fotos'); 
        map.overlayMapTypes.push(null);

        LayerTotalPredios = GetLayer('GeoMaePoligonos');
        map.overlayMapTypes.push(null);

        LayerRiosPeru = GetLayer('Rios_Quebradas');
        map.overlayMapTypes.push(null);   

        LayerViasPeru = GetLayer('viasperu');
        map.overlayMapTypes.push(null);  

        

        $('#chkPuntosFotos').click(function () {            
            if (this.checked) {
                map.overlayMapTypes.setAt(0, Layerfotos);
            } else {
                map.overlayMapTypes.setAt(0, null);
            }
        });

        $('#chkPrediosCercanos').click(function () {

            if (this.checked) {
                map.overlayMapTypes.setAt(1, LayerTotalPredios);
            } else {
                map.overlayMapTypes.setAt(1, null);
            }
        });

        $('#chkPrediosAnalista').click(function () {
            var cod = $("#CboAnalista").val();
            if (cod === "" || cod === null) {
                ShowInfoBox("Atención", "Seleccione Analista");
                return;
            }
            
            if (this.checked) {
                LayerPrediosFuncionario = GetLayer('vistafuncionarios', cod);
                map.overlayMapTypes.push(null);                
                map.overlayMapTypes.setAt(4, LayerPrediosFuncionario);
            } else {
                map.overlayMapTypes.setAt(4, null);
            }
        });

        $('#chkRios').click(function () {
            if (this.checked) {                
                map.overlayMapTypes.setAt(2, LayerRiosPeru);
            } else {
                map.overlayMapTypes.setAt(2, null);
            }
        });

        $('#chkViasPeru').click(function () {
            if (this.checked) {
                map.overlayMapTypes.setAt(3, LayerViasPeru);
            } else {
                map.overlayMapTypes.setAt(3, null);
            }
        });

    }
    google.maps.event.addDomListener(window, 'load', init);
});



function MostrarPoligono(IdVisita, Lat, Lon) { 
    //Define el WMS de unidades Administrativas
    //map.overlayMapTypes.setAt(7, null);
    var i = IdVisita + 4;
    var la = map.overlayMapTypes.getAt(i);

    if (la === null || la === undefined) {

        LayerPredio = GetLayer('vistaPoligonos', IdVisita);
        map.overlayMapTypes.push(null);
        map.overlayMapTypes.setAt(i, LayerPredio);

        //if (LayerPredio === null) {
        //    LayerPredio = GetLayer('vistaPoligonos', IdVisita);
        //    map.overlayMapTypes.push(null);
        //    map.overlayMapTypes.setAt(i, LayerPredio);
        //} else {
        //    map.overlayMapTypes.setAt(i, LayerPredio);
        //}        
        map.setCenter(new google.maps.LatLng(Lat, Lon));
        map.setZoom(16);
    } else {
        map.overlayMapTypes.setAt(i, null);
    }
    
}

function GetLayer(NameLayer, params = '') {
    Layer = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            var proj = map.getProjection();
            var zfactor = Math.pow(2, zoom);
            // Obtiene las coordenadas Long Lat
            var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 /
                zfactor, coord.y * 256 / zfactor));
            var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 /
                zfactor, (coord.y + 1) * 256 / zfactor));
            //crea la cadena del Bounding box
            var bbox = (top.lng()) + "," +
                (bot.lat()) + "," +
                (bot.lng()) + "," +
                (top.lat());
            //URL del WMS
            var url = "http://172.20.1.167:8080/geoserver/Agrobanco/wms?";
            url += "&REQUEST=GetMap"; //Operación WMS
            url += "&SERVICE=WMS"; //servicio WMS
            url += "&VERSION=1.1.1"; //Versión WMS
            url += "&LAYERS=Agrobanco:" + NameLayer; //Rios_Quebradas"; //Capas WMS
            url += "&FORMAT=image/png"; //Formato WMS
            url += "&BGCOLOR=0xFFFFFF"; //Color de fondo
            url += "&TRANSPARENT=TRUE"; //Transparencia activada
            url += "&SRS=EPSG:4326"; //establece WGS84
            url += "&BBOX=" + bbox; // Establece el bounding box
            url += "&WIDTH=256"; //Tamaño de tesela en google
            url += "&HEIGHT=256";

            url = params === '' ? url : url + "&ViewParams=param:" + params;
            return url; // devuelve la URL para la tesela
        },
        tileSize: new google.maps.Size(256, 256),
        isPng: true
    });

    return Layer;
}
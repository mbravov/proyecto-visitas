﻿var cboTipDoc = $("#ddlTipDoc"),
    txtNumDoc = $("#TxtNumDoc"),
    txtPriNom = $("#TxtPrimerNombre"),
    txtSegNom = $("#TxtSegundoNombre"),
    txtApePat = $("#TxtApPaterno"),
    txtApeMat = $("#TxtApMaterno"),
    cboSexo__ = $("#ddlSexo"),
    cboEstCiv = $("#ddlEstadoCivil"),
    txtNumCel = $("#TxtCelular"),
    txtExperi = $("#TxtExperiencia"),
    txtFecNac = $("#TxtFecha"),
    cboTipDocCony = $("#ddlTipoDocCony"),
    txtNumDocCony = $("#TxtNumDocCony");

function ObtenerDatosIntegrante(IdIntegrante) {
    var requestData = {
        NumeroSolicitud: IdIntegrante
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ObtenerDatosIntegrante',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            SetDatosIntegrante(response[0]);
            ObtenerUbigeos(response[0].DatosPredio.Distrito.value);
            ViewAndHide("#div-DetalleIntegrante", "#div-DetalleAgrupamiento");
        },
        error: function () {
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, no se pudo completar la solcitud");
        },
        complete: function () {
            StopLoading();
        }
    });
}

function ObtenerUbigeos(IntegranteUbigeo) {
    var CboDepa = $("#ddlDepartamento");
    var CboProv = $("#ddlProvincia");
    var CboDist = $("#ddlDistrito");
    var requestData = {
        Ubigeo: IntegranteUbigeo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarUbigeos',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            $.each(response, function (key, value) {
                switch (value.key) {
                    case "DEP": CboDepa.append($('<option>', { value: value.code, text: value.value })); break;
                    case "PRV": CboProv.append($('<option>', { value: value.code, text: value.value })); break;
                    case "DIS": CboDist.append($('<option>', { value: value.code, text: value.value })); break;
                }
            });            
            CboDepa.val(IntegranteUbigeo.substr(0, 2));
            CboProv.val(IntegranteUbigeo.substr(0, 4));
            CboDist.val(IntegranteUbigeo);
        },
        error: function () {
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, no se pudo completar la solcitud");
        },
        complete: function () {
            StopLoading();
        }
    });
}

function SetDatosIntegranteSolicitud(Integrante) {
    var cliente = Integrante.DatosCliente;
    $("#LblNomIntegrante").text(cliente.ApePaterno.value + ' ' + cliente.ApeMaterno.value + ', ' + cliente.NomPrimer.value);
    cboTipDoc.val(cliente.TipoDocumento.value);
    txtNumDoc.val(cliente.NroDocumento.value);
    txtPriNom.val(cliente.NomPrimer.value);
    txtSegNom.val(cliente.NomSegundo.value);
    txtApePat.val(cliente.ApePaterno.value);
    txtApeMat.val(cliente.ApeMaterno.value);
    cboSexo__.val(cliente.Sexo.value);
    cboEstCiv.val(cliente.EstadoCivil.value);
    txtNumCel.val(cliente.Celular.value);
    txtExperi.val(cliente.Experiencia.value);
    txtFecNac.val(pad(cliente.NacimientoDia.value, 2) + '/' + pad(cliente.NacimientoMes.value, 2) + '/' + cliente.NacimientoAnio.value);
    $("#TxtCorreo").val(cliente.Correo.value);

    var conyugue = Integrante.DatosConyugue;
    cboTipDocCony.val(conyugue.TipoDocumento.value);
    txtNumDocCony.val(conyugue.NroDocumento.value);

    var predio = Integrante.DatosPredio;
    $("#ddlDepartamento").val(predio.Departamento.value);
    $("#ddlProvincia").val(predio.Provincia.value);
    $("#ddlDistrito").val(predio.Distrito.value);
    $("#TxtReferencia").val(predio.Referencia.value);
    $("#ddlTenencia").val(predio.RegimenTenencia.value);
    $("#TxtUnidadCatastral").val(predio.UnidadCatastral.value);
    $("#ddlEstadoCampo").val(predio.EstadoCampo.value);

    var solicitud = Integrante.DatosSolicitud;
    var sol = Integrante.NumeroSolicitud;
    console.log(sol);
    $("#hidnrosolicitud").val(sol);  
    $("#ddlFinanciamiento").val(solicitud.TipoUnidadFinanciamiento.value);
    $("#TxtUnidTotales").val(solicitud.NroUnidadesTotales.value);
    $("#TxtUnidFinanciadas").val(solicitud.NroUnidadesFinanciamiento.value);
    $("#TxtMontoSolicitado").val(solicitud.MontoSolicitadoSoles.value);
    $("#ddlEnvioCronograma").val(solicitud.EnvioCronograma.value);

    $("#ddlAdelanto").val(solicitud.PagoAdelantado.value);
    $("#ddlFormaAbono_Cliente").val(solicitud.FormaAbono);
    $("#ddlFormaAbono_Cliente").trigger("change");

    
    if (solicitud.FormaAbono === '02') {
        $("#ddlBanco_Abono").val(solicitud.Banco);
        $("#TxtCCI").val(solicitud.CCI);
    }
    $('#btnGuardarFormaAbono').addClass('disabled');
    
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

$("#ddlDepartamento").on('change', function (e) {
    var CboProv = $("#ddlProvincia");
    var CboDis = $("#ddlDistrito");

    var dataRequest = {
        CodDepartamento: this.value
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarProvinciaPorDep',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            CboProv.find('option').remove();
            CboProv.append($('<option>', { value: "0", text: "[Seleccione Provincia]" }));
            CboDis.find('option').remove();
            CboDis.append($('<option>', { value: "0", text: "[Seleccione Distrito]" }));

            $.each(response, function (key, value) {
                CboProv.append($('<option>', { value: value.code, text: value.value }));
            });
        },
        error: function () {
            StopLoading();
            CboProv.find('option').remove();
            CboProv.prepend($('<option></option>').html('Error al cargar Provincias'));
        },
        complete: function () {
            StopLoading();
        }
    });

});

$("#ddlProvincia").on('change', function (e) {
    var CboDis = $("#ddlDistrito");
    var dataRequest = {
        CodProvincia: this.value
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarDistritoPorProv',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            CboDis.find('option').remove();
            CboDis.append($('<option>', { value: "0", text: "[Seleccione Distrito]" }));
            $.each(response, function (key, value) {
                CboDis.append($('<option>', { value: value.code, text: value.value }));
            });
        },
        error: function () {
            StopLoading();
            CboDis.find('option').remove();
            CboDis.prepend($('<option></option>').html('Error al cargar Distritos'));
        },
        complete: function () {
            StopLoading();
        }
    });

});



$("#btnRegresarRelInt").click(function () {
    ViewAndHide("#div-DetalleAgrupamiento", "#div-DetalleIntegrante");
    $('.tab-Cliente').find('input:text').val('');
});
﻿$(document).ready(function () {
    //var responsiveHelper_datatable_Agrupamiento = undefined;
    var responsiveHelper_datatable_Integrantes = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    // Modal Tipo de Cronograma
    $('#myModal-TipoCronograma').on('show.bs.modal', function (e) {
        $('.div-ValidacionPlazo').hide();
        $(e.currentTarget).find('.help-block').remove();
        var Tipo = $(e.relatedTarget).data().tip,
            sol = $(e.relatedTarget).data().sol,
            nom = $(e.relatedTarget).data().nom,
            tcr = $(e.relatedTarget).data().tcr;
        var Cuotas = 0, MaxCuotas = 1;

        var DiasGracia = 0;
        var Permitidos = '';
        var FrecuenciaPago = '', UnidadFrecuencia = '', FechaGracia = '', Porcentajes = '', Fechas = '';

        switch (tcr) {
            case 1:
                FrecuenciaPago = $(e.relatedTarget).data().fpa;
                UnidadFrecuencia = $(e.relatedTarget).data().ufr;
                FechaGracia = $(e.relatedTarget).data().pgr;

                break;

            case 3:
                Porcentajes = $(e.relatedTarget).data().prc;
                Fechas = $(e.relatedTarget).data().fec;
                break;

            default:
                break;
        }

        Cuotas = $(e.relatedTarget).data().ncu;
        $('.lectura').val('');
        if (Tipo == '1') {

            Permitidos = tcr;
            $('.lectura').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').hide();
            $('#Sec-DiasGracia').hide();
            $('#BtnCancelarTipoCronograma').text('Cerrar');
        }
        else {
            Permitidos = $(e.relatedTarget).data().cpe;
            MaxCuotas = $(e.relatedTarget).data().mxc;
            $(e.currentTarget).find('#ModalTC-hidMaxDiasGracia').val($(e.relatedTarget).data().mxg);
            $(e.currentTarget).find('#ModalTC-hidMaxPlazo').val($(e.relatedTarget).data().mxp);

            $('.lectura').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').show();
            $('#Sec-DiasGracia').show();
            $('#BtnCancelarTipoCronograma').text('Cancelar');

            $('#ModalTC-NumeroCuotas').prop('disabled', false);
        }

        $(e.currentTarget).find('#ModalTC-NomCliente').text(sol + ' - ' + nom);
        $(e.currentTarget).find('#ModalTC-hidNroSolicitud').val(sol);
        $('#ModalTC-UnidadFrecuencia').val('M');

        //Asigna Tipos de Cuota
        $('#ModalTC-TipoCuota').find('option').remove();
        var Cadena = Permitidos.toString();

        for (var i = 0; i < Cadena.length; i++) {
            switch (Cadena.charAt(i)) {
                case '1': $('#ModalTC-TipoCuota').append($('<option>', { value: "1", text: "Cuotas Fijas" })); break;
                case '3': $('#ModalTC-TipoCuota').append($('<option>', { value: "3", text: "Cuotas Variables" })); break;
                case '9': $('#ModalTC-TipoCuota').append($('<option>', { value: "9", text: "Vencimiento" })); break;
            }
        }

        // Asigna Número de cuotas
        $('#ModalTC-NumeroCuotas').find('option').remove();
        $('#RegistroCuotas').empty();
        if (tcr == '1') {
            $('#ModalTC-FechaGracia').val(FechaGracia);
            $('#ModalTC-FrecuenciaPago').val(FrecuenciaPago);
        }

        var ListaPorcentajes = [];
        var ListaFechas = [];

        if (Tipo == '1') {
            ListaPorcentajes = Porcentajes.split('|');
            ListaFechas = Fechas.split('|');
            $('#ModalTC-NumeroCuotas').append($('<option>', { value: Cuotas, text: Cuotas }));
            if (tcr == '3') {
                for (var i = 1; i <= Cuotas; i++) {
                    var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                    $('#RegistroCuotas').append('' +
                        '<div class= "" >' +
                        '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                        '<section class="col col-4">' +
                        '<label class="input">' +
                        '<i class="icon-append">%</i>' +
                        '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" disabled="disabled" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                        '</label>' +
                        '</section>' +
                        '<section class="col col-5">' +
                        '<label class="input">' +
                        '<i class="icon-append fa fa-calendar"></i>' +
                        '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" disabled="disabled" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                        '</label>' +
                        '</section>' +
                        '</div>'
                    );
                }
            }
        }
        else {
            if (Cuotas == 0) {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                    if (tcr == "3") {
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }
            }
            else {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                }

                if (tcr == '3') {
                    ListaPorcentajes = Porcentajes.split('|');
                    ListaFechas = Fechas.split('|');
                    for (var i = 1; i <= Cuotas; i++) {
                        var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }

            }
        }

        $('.cntrlFecha').datepicker({
            dateFormat: 'dd/mm/yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
        });

        // Setea valores predeterminados
        $('#ModalTC-TipoCuota').val(tcr);
        $('#ModalTC-NumeroCuotas').val(Cuotas);
        switch (tcr) {
            case 9:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').hide();
                $('#ModalTC-NumeroCuotas').val(1);
                $('#ModalTC-NumeroCuotas').attr('disabled', true);
                break;
            case 1:
                $('#ModalTC-div-CuotasFijas').show();
                $('#ModalTC-div-CuotasVariables').hide();
                break;
            case 3:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').show();
                break;
            default:
        }

    });

// Filtros Tabla Integrantes
    $("#datatable_Integrantes thead th input[type=text]").on('keyup change', function () {
    oIntegrante
        .column($(this).parent().index() + ':visible')
        .search(this.value)
        .draw();
});
/* Config. Tabla Integrantes */
    var oIntegrante = $('#datatable_Integrantes').DataTable({
    "data": [],
    //"oSearch": { "sSearch": "APROBADO" },
    "order": [1, "asc"],
    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
    "autoWidth": true,
    "preDrawCallback": function () {
        // Initialize the responsive datatables helper once.
        if (!responsiveHelper_datatable_Integrantes) {
            responsiveHelper_datatable_Integrantes = new ResponsiveDatatablesHelper($('#datatable_Integrantes'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_Integrantes.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_Integrantes.respond();
    },
    "bServerSide": false,
    "columns": [
        {
            'searchable': false,
            'orderable': false,
            'align': 'center',
            'width': '3%',
            'className': 'dt-body-center',
            "data": "TramaOpciones"
        },
        {
            "data": "NumeroSolicitud",
            'width': '5%'
        },        
        {
            "data": "DatosCliente.NroDocumento.value",
            'width': '5%'
        },
        { "data": "DatosCliente.NomPrimer.value" },
        //{
        //    "data": "EstadoSolicitud",
        //    'width': '5%'
        //},
        {
            "data": "DatosSolicitud.MontoSolicitadoSoles.value",
            "render": $.fn.dataTable.render.number(",", ".", 2),
            "className": "text-right",
            "width": "7%"
        },
        {
            "data": "EstadoEvaluacion",
            "className": "text-center",
            "width": "5%"
        },
        {
            "data": "EstadoTipoCuota",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoGeoreferencia",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoInformeVisita",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoFlujoCaja",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoInformeComercial",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoPropuesta",
            "className": "text-center",
            "width": "3%"
        },
        {
            "data": "EstadoSolicitud",
            "className": "text-center",
            'width': '3%'
        }

    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }
    });

});
function VerDetalleIntegrante(IdIntegrante) {
    ObtenerDatosIntegranteSolicitud(IdIntegrante);

}
function VerPropuesta(NumeroSolicitud) {
    //window.location.href = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    var url = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    window.open(url, '_blank');
}
function ConsultarGeoreferencia(NumeroSolicitud, NumeroDocumento, Nombre) {
    window.location.href = '../Georeferenciacion/ConsultaFotosSolicitud?NumeroSolicitud=' + NumeroSolicitud + '&NumeroDocumento=' + NumeroDocumento + '&Nombre=' + Nombre;
}

function DescargarDocumento(idLaserFiche) {
    var url = '../Grupo/DescargarDocumentoPorId?IdLaserFiche=' + idLaserFiche;
    window.open(url, '_blank');
}
$("#btnRegresarSolicitudes").click(function () {
    ViewAndHide("#div-RelacionIntegrante", "#div-DetalleIntegrante");
    
});
function ObtenerDatosIntegranteSolicitud(IdIntegrante) {
    var requestData = {        
        NumeroSolicitud: IdIntegrante
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ObtenerDatosIntegrante',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            SetDatosIntegranteSolicitud(response[0]);
            ObtenerUbigeos(response[0].DatosPredio.Distrito.value);
            ViewAndHide("#div-DetalleIntegrante", "#div-RelacionIntegrante");           
        },
        error: function () {
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, no se pudo completar la solcitud");
        },
        complete: function () {
            StopLoading();
        }
    });
}

$('#btnBuscar').click(function () {
    if ($("#TxtNumeroAgrupamiento").val() === "") {
        ShowWarningBox("Mensaje Sistemas", "Debe ingresar el numero de grupo");
        return;
    }
    $('#datatable_Integrantes').DataTable().clear().draw();
    ListarIntegrantes1($("#TxtNumeroAgrupamiento").val());
});
function ListarIntegrantes1(IdGrupo) {

    var requestData = {
        codgrupo: IdGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Consultas/ObtenerSolicitudes',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            console.log(response)
            if (response.length > 0) {
                $('#datatable_Integrantes').DataTable().clear().draw();
                $('#datatable_Integrantes').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "Código no registrado");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}


﻿var responsiveHelper_datatable_ResultadoAgrupamiento = undefined;

var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};

$('#btnBuscar').click(function () {
    var codigo_grupo = $("#TxtNumeroAgrupamiento").val();
    if ($("#cboAgencia").val() === null) {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar la agencia");
        return;
    }
    if ($("#cboFuncionario").val() === null) {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar el funcionario");
        return;
    }
    if ($("#TxtNumeroAgrupamiento").val() === "") {
        ShowWarningBox("Mensaje Sistemas", "Debe ingresar el numero de agrupamiento para realizar la busqueda");
        return;
    }
    Listar_Resultados($("#cboAgencia").val(), $("#cboFuncionario").val(), codigo_grupo);
});

// Filtros Tabla Agrupamientos
$("#datatable_ResultadoAgrupamiento thead th input[type=text]").on('keyup change', function () {
    oGrupo
        .column($(this).parent().index() + ':visible')
        .search(this.value)
        .draw();
});
/* Config Tabla Agrupamiento  */
var oGrupo = $('#datatable_ResultadoAgrupamiento').DataTable({
    "data": [],
    "order": [[2, "desc"]],
    //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
    //    "t" +
    //    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
    "autoWidth": true,
    "preDrawCallback": function () {
        // Initialize the responsive datatables helper once.
        if (!responsiveHelper_datatable_ResultadoAgrupamiento) {
            responsiveHelper_datatable_ResultadoAgrupamiento = new ResponsiveDatatablesHelper($('#datatable_ResultadoAgrupamiento'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_ResultadoAgrupamiento.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_ResultadoAgrupamiento.respond();
    },
    "bServerSide": false,
    "columns": [
        //{
        //    'searchable': false,
        //    'orderable': false,
        //    'align': 'center',
        //    'className': 'dt-body-center',
        //    'render': function (data, type, dataItem, meta) {
        //        return '';
        //    }
        //},
        { "data": "TipoDocumento" },
        { "data": "NroDocumento" },
        { "data": "NombreIntegrante" },
        { "data": "Solicitud" },
        { "data": "Codigo" },
        { "data": "Mensaje" }
    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }
    
});

function Listar_Resultados(pAgencia, pAnalista, IdGrupo) {

    var requestData = {
        CodAgencia: pAgencia,
        CodAnalista: pAnalista,
        CodigoGrupo: IdGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Consultas/ConsultaResultadosGrupos',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_ResultadoAgrupamiento').DataTable().clear().draw();
                $('#datatable_ResultadoAgrupamiento').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "Código Agrupamiento no existe.");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}
function ListarParametricasGrupo(Parametricas) {

    $.each(Parametricas, function (key, value) {
        switch (value.key) {
    
            case "AGEN": $("#cboAgencia").append($('<option>', { value: value.code, text: value.value })); break;
            case "FUNC": $("#cboFuncionario").append($('<option>', { value: value.code, text: value.value })); break;
        }
    });
}

function ObtenerParametricasGrupo() {
    //var dataRequest = {
    //    codOficinaRegional: 50
    //};
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarParametricasGrupo',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            ListarParametricasGrupo(response);
        },
        error: function () {
            StopLoading();
            //combo.find('option').remove();
            //combo.prepend($('<option></option>').html('No se pudo obtener lista'));
        },
        complete: function () {
            StopLoading();

            $("#cboAgencia").val($("#hidCodAgencia").val());
            $("#cboFuncionario").val($("#hidCodFuncionario").val());
            //$("#btnBuscar").trigger("click");
        }
    });
}
﻿var responsiveHelper_datatable_ResultadoPadron = undefined;

var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};

$('#btnBuscarPadron').click(function () {
    var codigo_grupo = $("#TxtNumeroPadron").val();
    if ($("#cboAgencia").val() === null) {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar la agencia");
        return;
    }
    //if ($("#cboFuncionario").val() === null) {
    //    ShowWarningBox("Mensaje Sistemas", "Debe seleccionar el funcionario");
    //    return;
    //}
    if ($("#TxtNumeroPadron").val() === "") {
        ShowWarningBox("Mensaje Sistemas", "Debe ingresar el numero de padron para realizar la busqueda");
        return;
    }
    Listar_ResultadosPadron($("#cboAgencia").val(), codigo_grupo);
});

// Filtros Tabla Padron
$("#datatable_ResultadoPadron thead th input[type=text]").on('keyup change', function () {
    oGrupo
        .column($(this).parent().index() + ':visible')
        .search(this.value)
        .draw();
});
/* Config Tabla Padron  */
var oGrupo = $('#datatable_ResultadoPadron').DataTable({
    "data": [],
    "order": [[2, "desc"]],
    //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
    //    "t" +
    //    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
    "autoWidth": true,
    "preDrawCallback": function () {
        // Initialize the responsive datatables helper once.
        if (!responsiveHelper_datatable_ResultadoPadron) {
            responsiveHelper_datatable_ResultadoPadron = new ResponsiveDatatablesHelper($('#datatable_ResultadoPadron'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_ResultadoPadron.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_ResultadoPadron
            .respond();
    },
    "bServerSide": false,
    "columns": [
        //{
        //    'searchable': false,
        //    'orderable': false,
        //    'align': 'center',
        //    'className': 'dt-body-center',
        //    'render': function (data, type, dataItem, meta) {
        //        return '';
        //    }
        //},
        { "data": "TipoDocumento" },
        { "data": "NroDocumento" },
        { "data": "NombreIntegrante" },
        { "data": "Solicitud" },
        { "data": "Codigo" },
        { "data": "Mensaje" }
    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }

});

function Listar_ResultadosPadron(pAgencia, IdGrupo) {

    var requestData = {
        CodAgencia: pAgencia,
        //CodAnalista: '',
        CodigoGrupo: IdGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Consultas/ConsultaResultadosPadron',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_ResultadoPadron').DataTable().clear().draw();
                $('#datatable_ResultadoPadron').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "Código Padrón no existe");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}
function ListarParametricasGrupo(Parametricas) {

    $.each(Parametricas, function (key, value) {
        switch (value.key) {

            case "AGEN": $("#cboAgencia").append($('<option>', { value: value.code, text: value.value })); break;
            case "FUNC": $("#cboFuncionario").append($('<option>', { value: value.code, text: value.value })); break;
        }
    });
}

function ObtenerParametricasGrupo() {
    //var dataRequest = {
    //    codOficinaRegional: 50
    //};
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarParametricasGrupo',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
            //combo.find('option').remove();
            //combo.prepend($('<option></option>').html('Cargando...'));
        },
        success: function (response) {
            ListarParametricasGrupo(response);
        },
        error: function () {
            StopLoading();
            //combo.find('option').remove();
            //combo.prepend($('<option></option>').html('No se pudo obtener lista'));
        },
        complete: function () {
            StopLoading();
            console.log($("#hidCodAgencia").val());
            console.log($("#hidCodFuncionario").val());

            $("#cboAgencia").val($("#hidCodAgencia").val());
            $("#cboFuncionario").val($("#hidCodFuncionario").val());
            //$("#btnBuscar").trigger("click");
        }
    });
}
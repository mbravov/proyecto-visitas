﻿var responsiveHelper_datatable_Filtros = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};

$(document).ready(function () {
    
    $("#datatable_FiltrosAgroperu thead th input[type=text]").on('keyup change', function () {
        oGrupo
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    });
    /* Config Tabla Padrones  */
    var oGrupo = $('#datatable_FiltrosAgroperu').DataTable({
        "data": [],
        //"order": [[1, 'desc']],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Filtros) {
                responsiveHelper_datatable_Filtros = new ResponsiveDatatablesHelper($('#datatable_FiltrosAgroperu'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Filtros.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Filtros.respond();
        },
        "bServerSide": false,
        "columns": [            
            {
                "data": "CodigoFiltro",
                "width": "5%"
            },

            {
                "data": "Descripcion"
            },
            {
                "data": "Registros",
                "render": $.fn.dataTable.render.number(",", ".", 0),
                "className": "text-right",
                "width": "10%"
            },
            {
                "data": "Fecha",
                "width": "12%"
            },
            {
                "data": "Usuario",
                "width": "12%"
            },
            {
                "data": "TramaOpciones",
                'searchable': false,
                'orderable': false,
                'align': 'center',
                "width": "10%"
            }
        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });



});

$('#btnLimpiar').click(function () {
    $('#datatable_Padrones').DataTable().clear().draw();
});


$("#btnBuscarFiltros").click(function () {
    ConsultarFiltro();
});

function ConsultarFiltro() {
    var _CodAgencia = $("#CboAgencia").val();

    var requestData = {
        CodAgencia: _CodAgencia
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../FiltrosAgroperu/ConsultarFiltrosPorAgencia',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_FiltrosAgroperu').DataTable().clear().draw();
                $('#datatable_FiltrosAgroperu').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "La búsqueda no obtuvo ningun resultado, intente con otros filtros.");
            }
        },
        error: function () {
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, si el error persiste reportarlo con Sistemas");
        },
        complete: function () {
            StopLoading();
        }
    });
}

function DescargarResultados(CodigoFiltro, Descripcion) {
    window.location.href = '../FiltrosAgroperu/DescargarFiltroAgroperu?CodigoFiltro=' + CodigoFiltro + '&Descripcion=' + Descripcion;
}
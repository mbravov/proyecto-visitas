﻿var responsiveHelper_datatable_tabletools = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};




$(document).ready(function () {
    $('#datatable_ErrorsFiltros').dataTable({
        "data": [],
        "autoWidth": true,
        "preDrawCallback": function () {
            if (!responsiveHelper_datatable_tabletools) {
                responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_ErrorsFiltros'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_tabletools.respond();
        },
        "bServerSide": false,
        "columns": [
            { "data": "row", "width": "5%" },
            { "data": "column", "width": "5%" },
            { "data": "value", "width": "15%" },
            { "data": "msg", "width": "80%" }
        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    $("#btnFiltrarAgroperu").click(function () {
        
        if ($("#TxtDescripcion").val() === "") {
            ShowWarningBox("Mensaje Sistemas", "Favor ingresar una descripción para el filtro");
            return;
        };
        var cadena = $("#fileExcel").val();
        if (cadena === "") {
            ShowWarningBox("Mensaje Sistemas", "Debe seleccionar el archivo excel correspondiente a filtroAgroperu");
            return;
        }
        if (cadena.indexOf(".xls") === -1) {
            ShowWarningBox("Mensaje Sistemas", "El formato del archivo de Prospecto debe ser un excel .xls o .xlsx");
            return;
        }

        var filtroDescripcion = $("#TxtDescripcion").val();
        

        var tiempo = new Date();
        var anho = '00' + tiempo.getFullYear();
        var time = anho.substring(anho.length - 4, 8);
        var mes = '00' + (tiempo.getMonth() + 1);
        time = time + mes.substring(mes.length - 2, 4);
        var dia = '00' + tiempo.getDate();
        time = time + dia.substring(dia.length - 2, 4);
        var hora = '00' + tiempo.getHours();
        time = time + hora.substring(hora.length - 2, 4);
        var minuto = '00' + tiempo.getMinutes();
        time = time + minuto.substring(minuto.length - 2, 4);
        var segundo = '00' + tiempo.getSeconds();
        time = time + segundo.substring(segundo.length - 2, 4);

        var formData = new FormData();
        var totalFiles = document.getElementById("fileExcel").files.length;
        if (totalFiles === 1) {
            var file = document.getElementById("fileExcel").files[0];
            formData.append("fileExcel", file);
            formData.append("Descripcion", filtroDescripcion);
            formData.append("FechaCliente", time);

            $.ajax({

                type: "POST",
                url: '../FiltrosAgroperu/RegistrarFiltro',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    StarLoading();
                },
                success: function (response) {

                    if (response.code === '99') {
                        $("#div-Prospecto").hide("slow");
                        $("#divResultados").show("slow");
                        //SetResultados(response);
                        ShowSuccessBox("Mensaje Sistemas", "Registro exitoso");
                    } else if (response.code === '11') {
                        //ShowWarningBox("Mensaje Sistemas", "No se encontraron datos en el archivo");
                        ShowWarningBox("Mensaje Sistemas", response.message);
                        return;
                    } else if (response.code === '10') {
                        ShowErrorBox("Mensaje Sistemas", response.message);
                        return;
                    } else {
                        ShowWarningBox("Mensaje Sistemas", "Se encontraron observaciones , ver mensajes en listado inferior ");
                        $('#datatable_ErrorsFiltros').DataTable().clear().draw();
                        $('#datatable_ErrorsFiltros').dataTable().fnAddData(response.listErrors);
                    }
                },
                error: function () {
                    StopLoading();
                    ShowErrorBox("Mensaje Sistemas", "Ocurrió un error al momento de grabar");
                },
                complete: function () {
                    StopLoading();
                }
            });
        }
    });

    
})
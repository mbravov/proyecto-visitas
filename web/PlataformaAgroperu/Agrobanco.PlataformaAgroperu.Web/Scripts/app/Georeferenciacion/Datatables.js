﻿var ventana;
var responsiveHelper_datatable_tabletools = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};
$('#datatable_Clientes').dataTable({
    "bFilter": false,
    //"bJQueryUI": true,
    //"sPaginationType": "full_numbers",
    "sScrollY": 150,
    "bLengthChange": false,
    "data": [],
    "autoWidth": true,
    "preDrawCallback": function () {
        if (!responsiveHelper_datatable_tabletools) {
            responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_Clientes'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_tabletools.respond();
    },
    "bServerSide": false,
    "columns": [
        { "data": "Credito", "width": "12%" },
        { "data": "NroDocumento", "width": "12%" },
        { "data": "NombreCliente" },
        { "data": "Distancia", "width": "12%" },
        { "data": "Cultivo", "width": "14%" }
    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }
});

// METODO DE VALIDAR FOTOS CERCANAS
function GetFotosCercanas(prmLat, prmLon) {

    var requestData = {
        Latitud: prmLat,
        Longitud: prmLon
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Georeferenciacion/ConsultarFotosCercanas',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            $('#datatable_Clientes').DataTable().clear().draw();
            if (!$.isEmptyObject(response)) {
                $('#datatable_Clientes').dataTable().fnAddData(response);
                MarcarPuntosCercanos(response);
            }
            console.log("Actualmente no está disponible el calculo de Predios Cercanos.");
        },
        error: function () {
            StopLoading();
            ShowWarningBox("No se pudo consultar predios cercanos. \n por favor reportarlo y continuar.");
        },
        complete: function () {
            StopLoading();
        }
    });
}

function MarcarPuntosCercanos(lstClientes) {

    for (var i in lstClientes) {
        var tittle = lstClientes[i].NombreCliente + '(' + lstClientes[i].Distancia + 'm)';
        MapsAddMarket(lstClientes[i].Latitud, lstClientes[i].Longitud, tittle, "", "", 1);
    }

}
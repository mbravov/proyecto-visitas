﻿// Control de número de fotos
function ControlNumFotos() {
    var FotVin = $("#divFotosVinculadas .superbox-list").length;
    var FotLib = $("#divFotosLibres .superbox-list").length;
    $("#NumFotVinculadas").text(FotVin);
    $("#NumFotLibres").text(FotLib);

    if (FotLib === 0) { $("#divVacioLib").show(); }
    else { $("#divVacioLib").hide(); }
    if (FotVin === 0) { $("#divVacioVin").show(); }
    else { $("#divVacioVin").hide(); }
}

// EVENTOS DE VINCULACIÓN
function VincularFail() {
    ShowWarningBox("Georeferenciación", "Imagen no Disponible");
}

function Vincular() {
    var IdFoto = $("#p1").text();
    var nomDiv = "#div_" + IdFoto;
    var nomImg = "#" + IdFoto;

    //OCULTA VISTA DE FOTO
    $("#superbox-Foto").attr("style", "display: none;");
    $(nomDiv).removeClass("active");
    $(nomImg).attr("panel", "s2");
    $(nomDiv).detach().appendTo("#divFotosVinculadas");

    //ACTIVA LA FICHA DE FOTOS VINCULADAS
    $("#li_s1").removeClass("active");
    $("#li_s2").addClass("active");
    $("#s1").removeClass("active in");
    $("#s2").addClass("active in");

    //MANEJA EL NÚMERO DE FOTOS DE CADA FICHA
    ControlNumFotos();
}

function Quitar() {
    var IdFoto = $("#p1").text();
    var nomDiv = "#div_" + IdFoto;
    var nomImg = "#" + IdFoto;

    // OCULTA VISTA DE FOTO
    $("#superbox-Foto").attr("style", "display: none;");
    $(nomDiv).removeClass("active");
    $(nomImg).attr("panel", "s1");
    $(nomDiv).detach().appendTo("#divFotosLibres");

    // MANEJA EL NÚMERO DE FOTOS DE CADA FICHA
    ControlNumFotos();
}

//Obtiene Fotos Vinculadas y no vinculadas
function GetTramaFotosVinLib() {
    var lstIdFotos = [];
    var lstIdFotosDesVin = [];

    //Agrega CodImg Vinculadas, filtrando las etiquetas img=vista
    $("#divFotosVinculadas img").each(function () {
        if ($(this).attr("id") !== "vista")
        {
            lstIdFotos.push($(this).attr("id"));
        }        
    });

    //Agrega CodImg Desvinculadas, filtrando las etiquetas img=vista
    $("#divFotosLibres img").each(function () {
        if ($(this).attr("id") !== "vista")
        {
            lstIdFotosDesVin.push($(this).attr("id"));
        }
        
    });

    // Arma parametros en JSON   

    var tramaImg = lstIdFotos.length > 0 ? lstIdFotos.join('|') + '|' : "";
    var requestData = {
        tramaCodImagenes: tramaImg, // Trama '|'
        lstCodImg: lstIdFotos, // array
        lstCodImgD: lstIdFotosDesVin, // array
        NumeroDocumento: $("#hidNroDocumento").val(),
        NumeroSolicitud: $("#hidNroSolicitud").val()
    };

    return requestData;
}

$("#btnGuardar").click(function () {

    var requestData = GetTramaFotosVinLib();
    if (requestData.tramaCodImagenes.length < 1) {
        ShowWarningBox("Georeferenciación", "No ha seleccionado imagenes a Georeferenciar...!!");
        return false;
    }

    // Vincula foto (Servidor)
    $.ajax({
        cache: false,
        type: "POST",
        url: '../GeoReferenciacion/RegistrarGeoreferenciacion', //'@Url.Content("~/GeoReferenciacion/RegistrarGeoreferenciacion/")',
        contentType: 'application/json',
        processData: false,
        data: JSON.stringify(requestData),
        beforeSend: function () {
            StarLoading();
            $("#btnGuardar").attr("disabled", "disabled");
        },
        success: function (response) {
            ShowSuccessBox("Georeferenciación", "Operación finalizada con éxito...!!");
            $("#btnGuardar").attr("disabled", "disabled");
            //$("#divExitoVin").show();
            setTimeout("finalizar();", 2000);

        },
        error: function () {
            StopLoading();
            ShowErrorBox("Ocurrió un  error al procesar la solicitud. \nVuelva a intentarlo, si el problema persiste contactese con Sistemas.");

        },
        complte: function () {
            StopLoading();
        }
    });


});
function finalizar() {
    window.location.href = '../Grupo/BandejaAgrupamiento';
}
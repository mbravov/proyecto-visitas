﻿var tipos = ['jpg']; // formatos para el input

function DivFotoContentString(foto) {
    var ContentString =
        '<div id="div_' + foto.IdLaserFiche + '" class="superbox-list"> ' +
        '   <img id="' + foto.IdLaserFiche + '" src="data:image/jpg;base64,' + foto.imgBase64 + '" data-img="data:image/jpg;base64,' + foto.imgBase64 + '" ' +
        '        alt="Observ. :' + foto.Observ + '" title="' + foto.ImgCod + '" class="superbox-img" id-foto="' + foto.IdLaserFiche + '" ' +
        '        panel="s2" fecfoto="fecha foto: ' + foto.FecFoto + '" latitud="Latitud: ' + foto.Latitud + '" longitud="Longitud: ' + foto.Longitud + '"> ' +
        '</div > ';
    return ContentString;
}

function MarketContentString(DecLat, DecLon, SexLat, SexLon) {
    var ContentString =
        //'<div class="gm-style-iw" style="top: 9px; position: absolute; left: 15px; width: 300px;"> ' +
        '		<div style="overflow: auto;">' +
        '			<table style="border-spacing: 10px;">' +
        '				<tbody>' +
        '					<tr><th>tipo</th><th>latitud</th><th>longitud</th></tr>' +
        '					<tr><td><strong>GD &nbsp;&nbsp; : &nbsp; </strong></td><td> ' + parseFloat(DecLat).toFixed(12) + ' &nbsp;</td><td> ' + parseFloat(DecLon).toFixed(12) + ' </td></tr>' +
        //"					<tr><td><strong>GMS : &nbsp; </strong></td><td> " + SexLat + " &nbsp;</td><td> " + SexLon + " </td></tr>" +
        '				</tbody>' +
        '			</table>' +
        '		</div>';
    //'</div>';
    return ContentString;
}

// Inicializa Mapa
function InitMap() {
    $(window).unbind('gMapsLoaded');
    $(window).bind('gMapsLoaded', MapsDefault);
    window.loadGoogleMaps();
}

// Setea Mapa
var MapsDefault = function () {
    // Ubicación Perú
    MapsLinkCenter(-10.457721239161893, -76.6714958398465, 4, 'roadmap');
    MapsAddMarket(-10.457721239161893, -76.6714958398465, "Perú");
};

// Centra mapa en las coordenas recibidas
function MapsLinkCenter(latitud, longitud, prmZoom = 16, MapType = 'hybrid') {
    var mapOptions = {
        center: new google.maps.LatLng(latitud, longitud),
        zoom: prmZoom,
        mapTypeId: MapType
    };
    map = new google.maps.Map(document.getElementById('map_canvas2'), mapOptions);
}

// Agrega marcador al map
function MapsAddMarket(latitud, longitud, descripcion = "No tittle", SexLat = "", SexLong = "", color = 0) {
    //var image = {
    //    url: 'Content/img/plus.png', //ruta de la imagen
    //    size: new google.maps.Size(40, 60), //tamaño de la imagen
    //    origin: new google.maps.Point(0, 0), //origen de la iamgen
    //    //el ancla de la imagen, el punto donde esta marcando, en nuestro caso el centro inferior.
    //    anchor: new google.maps.Point(20, 60)
    //};
    var iconColor = color === 0 ? '' : 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitud, longitud),
        map: map,
        animation: google.maps.Animation.DROP,
        //draggable: true,
        icon: iconColor, // image
        title: descripcion
    });

    var InfoMarket = new google.maps.InfoWindow({
        content: MarketContentString(latitud, longitud, SexLat, SexLong)
    });
    marker.addListener('click', function () {
        InfoMarket.open(map, marker);
    });
}

// METODOS DE CONVERSION DE COORDENADAS
function ConvertLatitud_SexagesimalADecimal(ref, grados, minutos, segundos) {
    var signo = ref === 'N' ? 1 : -1;
    var coordenada = (grados + minutos / 60 + segundos / 3600) * signo;
    return coordenada;
}
function ConvertLongitud_SexagesimalADecimal(ref, grados, minutos, segundos) {
    var signo = ref === 'E' ? 1 : -1;
    var coordenada = (grados + minutos / 60 + segundos / 3600) * signo;
    return coordenada;
}

// VALIDA METADATA DE IMAGEN
function ValidarMetaData(LatitudRef, Latitud, LongitudRef, Longitud, AltitudRef, Altitud, FechaFoto) {

    if (typeof Latitud === 'undefined' || typeof Longitud === 'undefined' || typeof FechaFoto === 'undefined') {
        return false;
    }
    else {
        var data_Lat = Latitud.toString().split(','),
            data_Lon = Longitud.toString().split(','),
            DecLatitud = ConvertLatitud_SexagesimalADecimal(LatitudRef, parseFloat(data_Lat[0]), parseFloat(data_Lat[1]), parseFloat(data_Lat[2])),
            DecLongitud = ConvertLongitud_SexagesimalADecimal(LongitudRef, parseFloat(data_Lon[0]), parseFloat(data_Lon[1]), parseFloat(data_Lon[2]));

        $("#hidLatitud").val(DecLatitud); $("#hidLongitud").val(DecLongitud);
        $("#TxtLatitud").val(LatitudRef + " " + data_Lat[0] + "° " + data_Lat[1] + "' " + parseFloat(data_Lat[2]).toFixed(2) + "''");
        $("#TxtLongitud").val(LongitudRef + " " + data_Lon[0] + "° " + data_Lon[1] + "' " + parseFloat(data_Lon[2]).toFixed(2) + "''");
        $("#TxtAltitud").val(parseFloat(Altitud).toFixed(2) + " m.");
        $("#TxtFechaFoto").val(FechaFoto);

        return true;
    }
}

// EVENTOS FILE INPUT
$('#file-es').fileinput({
    language: 'es',
    uploadUrl: '../Georeferenciacion/SubirNuevaFoto',
    uploadAsync: false,
    maxFileCount: 1,
    maxFileSize: 2000,
    showCaption: false,
    showUpload: true,
    initialPreviewShowDelete: false,
    removeFromPreviewOnError: false,
    allowedFileExtensions: tipos,
    fileActionSettings: { showRemove: false, showUpload: false },
    uploadExtraData: function (previewId, index) {
        var obj = {};
        var FecFoto = $("#TxtFechaFoto").val(),
            Latitud = $("#hidLatitud").val(),
            Longitud = $("#hidLongitud").val(),
            observ = $("#TxtObservacion").val(),
            NroSolicitud = $("#hidNroSolicitud").val(),
            NroDocumento = $("#hidNroDocumento").val();
        obj = { NumeroSolicitud: NroSolicitud, NumeroDocumento: NroDocumento, FechaFoto: FecFoto, FLatitud: Latitud, FLongitud: Longitud, FObservacion: observ };
        return obj;
    }
});

// EVENTO AL CARGAR MINIATURA
$('#file-es').on('fileloaded', function (event, file, previewId, index, reader) {

    EXIF.getData(file, function () {
        var LatitudRef = EXIF.getTag(this, "GPSLatitudeRef"),
            Latitud = EXIF.getTag(this, "GPSLatitude"),
            LongitudRef = EXIF.getTag(this, "GPSLongitudeRef"),
            Longitud = EXIF.getTag(this, "GPSLongitude"),
            AltitudRef = EXIF.getTag(this, "GPSAltitudeRef"),
            Altitud = EXIF.getTag(this, "GPSAltitude"),
            DateTime1 = EXIF.getTag(this, "DateTime"),
            DateTime2 = EXIF.getTag(this, "DateTimeOriginal");

        var DateTime = DateTime1 === undefined ? DateTime2 : DateTime1;
        if (ValidarMetaData(LatitudRef, Latitud, LongitudRef, Longitud, AltitudRef, Altitud, DateTime)) {
            $('.fileinput-upload').removeClass('hide');
            var DecLat = $("#hidLatitud").val(), DecLon = $("#hidLongitud").val();
            MapsLinkCenter(DecLat, DecLon);
            MapsAddMarket(DecLat, DecLon, "Nombre Cliente");
            GetFotosCercanas(DecLat, DecLon);
        }
        else {
            ShowWarningBox("Georeferenciación", "Imagen no cuenta con coordenadas de geolocalización ...");
            $('.fileinput-upload').addClass('hide');
        }
    });

    


});

$('#file-es').on('filepreajax', function (event, previewId, index) {

    if ($("#hidLatitud").val() === '' || $("#hidLongitud").val() === '' || $("#TxtFechaFoto").val() === '' ||
        $("#hidLatitud").val() === '0' || $("#hidLongitud").val() === '0' || $("#TxtFechaFoto").val() === '0' ||
        $("#hidLatitud").val() === 'NaN' || $("#hidLongitud").val() === 'NaN' || $("#TxtFechaFoto").val() === 'NaN' ) {
        return {
            message: 'Datos de geolocalización de la imagen no son correctos, por favor verificar',
            data: { key1: 'Key 1', detail1: 'Detail 1' }
        }
    }

});

// EVENTO AL TERMINAR LA CARGA EXITOSA DE IMAGENES(ajax)
$('#file-es').on('filebatchuploadsuccess', function (event, data) {
    $("#divFotosVinculadas").append(DivFotoContentString(data.response));

    $("#superbox-Foto").remove();
    var contVin = $("#divFotosVinculadas").html().toString();
    var contLib = $("#divFotosLibres").html().toString();
    $("#divFotosVinculadas").html("");
    $("#divFotosLibres").html("");
    $("#divFotosVinculadas").html(contVin);
    $("#divFotosLibres").html(contLib);
    $('.superbox').SuperBox();

    ControlNumFotos();
    ShowSuccessBox("Georeferenciación", "Imagen subida con éxito");
    $('#file-es').fileinput('clear');
    //ACTIVA LA FICHA DE FOTOS VINCULADAS
    $("#li_s5").removeClass("active");
    $("#li_s2").addClass("active");
    $("#s3").removeClass("active in");
    $("#s2").addClass("active in");

});

// EVENTO LIMPIAR FILE
$('#file-es').on('filecleared', function (event) {
    $("#divAlert").empty();
    $("#divAlert").hide();
    $("#TxtLatitud").val(''); $("#TxtLongitud").val(''); $("#TxtAltitud").val(''); $("#TxtFechaFoto").val(''); $("#TxtObservacion").val('');
    $("#hidLatitud").val(''); $("#hidLongitud").val('');
    $('#datatable_Clientes').DataTable().clear().draw();
    InitMap();
});
﻿$(document).ready(function () {
    var responsiveHelper_datatable_Agrupamiento = undefined;
    var responsiveHelper_datatable_Integrantes = undefined;
    var responsiveHelper_datatable_Solicitudes = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    var elemento;
    //$('.pie').sparkline('html', {
    //    type: 'pie',
    //    height: '23px',
    //    sliceColors: ['#016950', '#016950', '#016950', '#9074b1'],
    //});
    //Dialog para confirmación de propuesta
    $("#dialog-message").dialog({
        autoOpen: false,
        modal: true,

        position: { my: 'top', at: 'top+150' },
        title: "Propuesta Generada",
        buttons: [{
            html: "<i class='fa fa-check'></i>&nbsp; OK",
            "class": "btn btn-success",
            click: function () {
                $(this).dialog("close");
            }
        }]

    });

    //Modal adjuntar documentos
    $('#myModal').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data().id,
            nom = $(e.relatedTarget).data().nom,
            doc = $(e.relatedTarget).data().doc,
            opc = $(e.relatedTarget).data().opc;

        $(e.currentTarget).find('.help-block').remove();
        $(e.currentTarget).find('.form-group').removeClass('has-error');
        $(e.currentTarget).find('.form-file').val('');
        $(e.currentTarget).find('#myModalNom').text(id + ' - ' + nom);
        $(e.currentTarget).find('#hidNroSolicitud').val(id);
        $(e.currentTarget).find('#hidNroDocumento').val(doc);
        $(e.currentTarget).find('#file-cboTipo').val(opc);
    });

    // Modal Aprobacion por excepcion
    $('#myModal-AprobExcepcion').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data().id,
            nom = $(e.relatedTarget).data().nom,
            doc = $(e.relatedTarget).data().doc;

        //$(e.currentTarget).find('.help-block').remove();
        //$(e.currentTarget).find('.form-group').removeClass('has-error');
        $(e.currentTarget).find('.form-file').val('');
        $(e.currentTarget).find('#modal-txtCliente').val(nom);
        $(e.currentTarget).find('#modal-hidNroSolicitud').val(id);
        $(e.currentTarget).find('#modal-hidNroDocumento').val(doc);
    });

    // Modal Tipo de Cronograma
    $('#myModal-TipoCronograma').on('show.bs.modal', function (e) {
        $('.div-ValidacionPlazo').hide();
        $(e.currentTarget).find('.help-block').remove();
        var Tipo = $(e.relatedTarget).data().tip,
            sol = $(e.relatedTarget).data().sol,
            nom = $(e.relatedTarget).data().nom,
            tcr = $(e.relatedTarget).data().tcr;
        var Cuotas = 0, MaxCuotas = 1; 

        var DiasGracia = 0;
        var Permitidos = ''; 
        var FrecuenciaPago = '', UnidadFrecuencia = '', FechaGracia = '', Porcentajes = '', Fechas = '';

        switch (tcr) {
            case 1:
                FrecuenciaPago = $(e.relatedTarget).data().fpa;
                UnidadFrecuencia = $(e.relatedTarget).data().ufr;
                FechaGracia = $(e.relatedTarget).data().pgr;

                break;

            case 3:
                Porcentajes = $(e.relatedTarget).data().prc;
                Fechas = $(e.relatedTarget).data().fec;
                break;

            default:
                break;
        }

        Cuotas = $(e.relatedTarget).data().ncu;
        $('.lectura').val('');
        if (Tipo == '1') {

            Permitidos = tcr;            
            $('.lectura').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').hide();
            $('#Sec-DiasGracia').hide();
            $('#BtnCancelarTipoCronograma').text('Cerrar');
        }
        else {
            Permitidos = $(e.relatedTarget).data().cpe;
            MaxCuotas = $(e.relatedTarget).data().mxc;
            $(e.currentTarget).find('#ModalTC-hidMaxDiasGracia').val($(e.relatedTarget).data().mxg);
            $(e.currentTarget).find('#ModalTC-hidMaxPlazo').val($(e.relatedTarget).data().mxp);

            $('.lectura').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').show();
            $('#Sec-DiasGracia').show();
            $('#BtnCancelarTipoCronograma').text('Cancelar');

            $('#ModalTC-NumeroCuotas').prop('disabled', false);
        }

        $(e.currentTarget).find('#ModalTC-NomCliente').text(sol + ' - ' + nom);
        $(e.currentTarget).find('#ModalTC-hidNroSolicitud').val(sol);
        $('#ModalTC-UnidadFrecuencia').val('M');

        //Asigna Tipos de Cuota
        $('#ModalTC-TipoCuota').find('option').remove();
        var Cadena = Permitidos.toString();
        
        for (var i = 0; i < Cadena.length; i++) {
            switch (Cadena.charAt(i)) {
                case '1': $('#ModalTC-TipoCuota').append($('<option>', { value: "1", text: "Cuotas Fijas" })); break;
                case '3': $('#ModalTC-TipoCuota').append($('<option>', { value: "3", text: "Cuotas Variables" })); break;
                case '9': $('#ModalTC-TipoCuota').append($('<option>', { value: "9", text: "Vencimiento" })); break;
            }
        }

        // Asigna Número de cuotas
        $('#ModalTC-NumeroCuotas').find('option').remove();
        $('#RegistroCuotas').empty();
        if (tcr == '1') {
            $('#ModalTC-FechaGracia').val(FechaGracia);
            $('#ModalTC-FrecuenciaPago').val(FrecuenciaPago);
        }

        var ListaPorcentajes = [];
        var ListaFechas = [];

        if (Tipo == '1') {
            ListaPorcentajes = Porcentajes.split('|');
            ListaFechas = Fechas.split('|');
            $('#ModalTC-NumeroCuotas').append($('<option>', { value: Cuotas, text: Cuotas }));
            if (tcr == '3') {
                for (var i = 1; i <= Cuotas; i++) {
                    var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                    $('#RegistroCuotas').append('' +
                        '<div class= "" >' +
                        '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                        '<section class="col col-4">' +
                        '<label class="input">' +
                        '<i class="icon-append">%</i>' +
                        '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" disabled="disabled" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                        '</label>' +
                        '</section>' +
                        '<section class="col col-5">' +
                        '<label class="input">' +
                        '<i class="icon-append fa fa-calendar"></i>' +
                        '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" disabled="disabled" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                        '</label>' +
                        '</section>' +
                        '</div>'
                    );                    
                }
            }
        }
        else {
            if (Cuotas == 0) {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                    if (tcr == "3") {
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }
            }
            else {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                }

                if (tcr == '3') {
                    ListaPorcentajes = Porcentajes.split('|');
                    ListaFechas = Fechas.split('|');
                    for (var i = 1; i <= Cuotas; i++) {
                        var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }
                
            }
        }

        $('.cntrlFecha').datepicker({
            dateFormat: 'dd/mm/yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
        });

        // Setea valores predeterminados
        $('#ModalTC-TipoCuota').val(tcr);
        $('#ModalTC-NumeroCuotas').val(Cuotas);
        switch (tcr) {
            case 9:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').hide();
                $('#ModalTC-NumeroCuotas').val('1');
                $('#ModalTC-NumeroCuotas').attr('disabled', true);
                break;
            case 1:
                $('#ModalTC-div-CuotasFijas').show();
                $('#ModalTC-div-CuotasVariables').hide();
                break;
            case 3:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').show();
                break;
            default:
        }

    });
    $('#BtnAtendidoVinculacionVisita').click(function () {
        window.location.href = window.location.origin + window.location.pathname;
    });

    //Modal Vinculacion Visitas
    $('#myModal-VinculacionVisitas').on('show.bs.modal', function (e) {
        debugger;
        var numeroSolicitud = $(e.relatedTarget).data().sol,
            numeroDocumento = $(e.relatedTarget).data().doc
        var date = new Date();
        var requestData = {
            idtoken: numeroSolicitud + '' + numeroDocumento + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds(),
        };
        var UrlBaseSGV = $("#UrlBaseSGV").val();
        var UrlBaseSGVAPI = $("#UrlBaseSGVAPI").val();

        $.ajax({
            cache: false,
            type: "POST",
            url: UrlBaseSGVAPI + 'vinculacion/registrar/',
            data: JSON.stringify(requestData),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {

                if (response.exito == true) {
                    var data = response.data;
                    var IdToken = requestData.idtoken;
                    var urlvisita = UrlBaseSGV + "vinculacion?IdToken=" + IdToken + "&NumeroDocumento=" + numeroDocumento + "&NumeroSolicitud=" + numeroSolicitud+"&output=embed";
                    //window.open(urlvisita, '_blank');
                    $("#iframevisita").attr("src", urlvisita);
                }
                else {
                    //ShowInfoBox("Información", response.Message);
                    ShowErrorBox("Error", response.Message)
                }
            },
            error: function (e) {
                console.log(e);
                StopLoading();
            },
            complete: function () {
                StopLoading();
            }
        });
       
    });

    // Filtros Tabla Agrupamientos
    $("#datatable_Agrupamientos thead th input[type=text]").on('keyup change', function () {
        oGrupo
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    });
                /* Config Tabla Agrupamiento  */
    var oGrupo = $('#datatable_Agrupamientos').DataTable({
        "data": [],
        "order": [1, "desc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Agrupamiento) {
                responsiveHelper_datatable_Agrupamiento = new ResponsiveDatatablesHelper($('#datatable_Agrupamientos'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Agrupamiento.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Agrupamiento.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                'searchable': false,
                'orderable': false,
                'align': 'center',
                'width': '3%',
                'className': 'dt-body-center',
                'data': 'TramaOpciones'
                //'render': function (data, type, dataItem, meta) {
                //    return '<div class="btn-group">' +
                //        '<a href="javascript:void(0);" class="btn btn-default btn-xs" dropdown-toggle" data-toggle="dropdown">' +
                //        '<i class="fa fa-gear fa-lg"></i> <i class="fa fa-caret-down"></i></a>' +
                //        '<ul class="dropdown-menu">' +
                //        '<li> <a href="javascript:VerDetalleGrupo(\'' + dataItem.Codigo + '\');"> Detalles Grupo </a> </li>' +
                //        //'<li class="divider"></li>' +                        
                //        //'<li> <a href="javascript:VerEstadisticasGrupo(\'' + dataItem.Codigo + '\',\'' + dataItem.Nombre + '\');"><i class="fa fa-bar-chart-o"></i> Resumen Calif. SBS </a> </li>' +
                //        '</ul>' +
                //        '</div>';

            },
            {
                "data": "Codigo",
                "width": "3%"
            },
            {
                "data": "Nombre"
            },
            {
                "data": "JuntaRegante"
            },
            {
                "data": "TotalIntegrantes",
                "render": $.fn.dataTable.render.number(",", ".", 0),
                "className": "text-right",
                "width": "3%"
            },
            {
                "data": "TotalAprobados",
                "render": $.fn.dataTable.render.number(",", ".", 0),
                "className": "text-right",
                "width": "3%"
            },
            {
                "data": "MontoSolicitado",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                "data": "Estado",
                "width": "5%"
            },
            {
                "data": "FechaCreacion",
                "width": "7%"
            },
            {
                "data": "PagoAsistenteTecnico",
                "width": "5%"
            }
        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    // Filtros Tabla Integrantes
    $("#datatable_Integrantes thead th input[type=text]").on('keyup change', function () {
        oIntegrante
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    });
                /* Config. Tabla Integrantes */
    var oIntegrante = $('#datatable_Integrantes').DataTable({
        "data": [],
        //"oSearch": { "sSearch": "APROBADO" },
        "order": [1, "asc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Integrantes) {
                responsiveHelper_datatable_Integrantes = new ResponsiveDatatablesHelper($('#datatable_Integrantes'), breakpointDefinition);
            }
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.FlagExcepcion == '1') {
                $(row).find('td:eq(1)').addClass('text-warning');
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Integrantes.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Integrantes.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                'searchable': false,
                'orderable': false,
                'align': 'center',
                'width': '3%',
                'className': 'dt-body-center',
                "data": "TramaOpciones"
            },
            {
                "data": "NumeroSolicitud",
                'width': '5%'
            },
            //{ "data": "DatosCliente.TipoDocumento.value" },
            {
                "data": "DatosCliente.NroDocumento.value",
                'width': '5%'
            },
            { "data": "DatosCliente.NomPrimer.value" },            
            {
                "data": "DatosSolicitud.MontoSolicitadoSoles.value",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                "data": "EstadoEvaluacion",
                "className": "text-center",
                "width": "5%"
            },
            {
                "data": "EstadoTipoCuota",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoGeoreferencia",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeVisita",
                "className": "text-center",
                "width": "3%"
            },
            ///*AGRO-REQ-023 VISITAS VINCULACIÓN */
            {
                "data": "InicioVinculacion",
                "className": "text-center",
                "width": "3%"
            },
            ///*AGRO-REQ-023 VISITAS VINCULACIÓN */
            {
                "data": "EstadoFlujoCaja",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeComercial",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoPropuesta",
                "className": "text-center",
                "width": "3%"
            },          

            {
                "data": "EstadoSolicitud",
                "className": "text-center",
                'width': '3%'
            }

        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    // Filtros Tabla Solicitudes
    $("#datatable_Solicitudes thead th input[type=text]").on('keyup change', function () {
        oSolicitudes
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    });
                /* Config. Tabla Solicitudes */
    var oSolicitudes = $('#datatable_Solicitudes').DataTable({
        "data": [],
        //"oSearch": { "sSearch": "APROBADO" },
        "order": [1, "desc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Solicitudes) {
                responsiveHelper_datatable_Solicitudes = new ResponsiveDatatablesHelper($('#datatable_Solicitudes'), breakpointDefinition);
            }
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.FlagExcepcion == '1') {
                $(row).find('td:eq(1)').addClass('text-warning');
            }
            //$(row).find('td:eq(12)').find('span:first').sparkline('html', { type: 'pie', height: '23px' });
            
            //console.log($(row).find('td:eq(12)').find('span:first'));

        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Solicitudes.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Solicitudes.respond();
            //$('.paginate_button.next:not(.disabled)', this.api().table().container())
            //    .on('click', function () {
            //        alert('next');
            //    }); 
        },
        "bServerSide": false,
        "columns": [
            {
                "data": "NumeroSolicitud",
                'width': '5%'
            },
            //{ "data": "DatosCliente.TipoDocumento.value" },
            {
                "data": "DatosCliente.NroDocumento.value",
                'width': '5%'
            },
            { "data": "DatosCliente.NomPrimer.value" },
            {
                "data": "DatosSolicitud.MontoSolicitadoSoles.value",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                "data": "EstadoEvaluacion",
                "className": "text-center",
                "width": "5%"
            },
            {
                "data": "EstadoTipoCuota",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoGeoreferencia",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeVisita",
                "className": "text-center",
                "width": "3%"
            },
            ///*AGRO-REQ-023 VISITAS VINCULACIÓN */
            {
                "data": "InicioVinculacion",
                "className": "text-center",
                "width": "3%"
            },
            ///*AGRO-REQ-023 VISITAS VINCULACIÓN */
            {
                "data": "EstadoFlujoCaja",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeComercial",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoPropuesta",
                "className": "text-center",
                "width": "3%"
            },       

            {
                "data": "EstadoSolicitud",
                "className": "text-center",
                'width': '3%'
            }

        ],
        "fnDrawCallback": function (oSettings) {
            //$('.pie:not(:has(canvas))').sparkline('html', {
            //    type: 'pie', height: '23px'
            //});
        },
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });


    // Evento cambio de página
    //$('#datatable_Solicitudes').on('page.dt', function () {
    //    $('.pie').sparkline('html', { type: 'pie', height: '23px' });
    //    $('.pie').removeClass('pie');
    //});

    //ObtenerParametricasGrupo();
    $('.data-Grupo').attr('Disabled', 'Disabled');
    $('.data-Integrante').attr('Disabled', 'Disabled');

    ListarSolicitudes();
    ObtenerParametricasGrupo()
    $('#btnBuscar').click(function () {
        $('#datatable_Agrupamientos').DataTable().clear().draw();
        ListarSolicitudes();
        ListarAgrupamiento();
    });

    // Actualiza Pago Asitente Tecnico
    $('#Grupos').on('change', '.chkAT', function () {
        var IdGrupo = $(this).attr('id');
        var Pago = $(this).is(':checked') ? 1 : 0;
        var chkData = $(this).is(':checked');
        elemento = $(this);

        //$(this).prop('checked', chkData)

        //        console.log(chkData, !chkData, flagAT);

        var formData = new FormData();
        formData.append("CodigoGurpo", IdGrupo);
        formData.append("PagoAsistenteTecnico", Pago);


        $.ajax({
            cache: false,
            type: "POST",
            url: '../Grupo/ActualizarPagoAsistenteTecnico',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                //StarLoading();
            },
            success: function (response) {
                if (response === "100") {
                    //ShowSuccessBox("Info", "Proceso realizado con éxito");                    
                } else {
                    ShowWarningBox("No se pudo procesar su solicitud", response);
                    elemento.prop('checked', !chkData);
                }
            },
            error: function () {
                //StopLoading();
                ShowWarningBox("Atención", "No se pudo completar la operación");
                elemento.prop('checked', !chkData);
            },
            complete: function () {
                //StopLoading();
            }
        });





    })

    $('#BtnAdjuntarArchivo').click(function () {
        var nroSolicitud = $('#hidNroSolicitud').val();
        var nroDocumento = $('#hidNroDocumento').val();
        var Categoria = $('#file-cboTipo').val();
        var Comentario = $('#file-cboComentario').val();
        var valid = true;
        $('.help-block').remove();
        $('.form-group').removeClass('has-error');

        if (Categoria === "0" || Categoria === null) {
            $('.cboCategoria').addClass('has-error');
            $('#file-cboTipo').after('<em class="help-block"><i class="fa fa-warning"></i> Seleccione una opción</em>');
            valid = false;
        }
        if (Comentario === '') {
            $('.txtComentario').addClass('has-error');
            $('#file-cboComentario').after('<em class="help-block"><i class="fa fa-warning"></i> Por favor ingrese un comentario</em>');
            valid = false;
        }
        if ($('#file-DocText').val() === '') {
            $('.fileDoc').addClass('has-error');
            $('.input-file').after('<em class="help-block"><i class="fa fa-warning"></i> Adjunte un archivo</em>');
            valid = false;
        }

        if (valid) {
            var formData = new FormData();
            //console.log(document.getElementById("file-Doc").files.length);
            var file = document.getElementById("file-Doc").files[0];
            formData.append("fileDoc", file);
            formData.append("NumeroSolicitud", nroSolicitud);
            formData.append("NumeroDocumento", nroDocumento);
            formData.append("TipoDocumento", Categoria);
            formData.append("Observacion", Comentario);

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Grupo/SubirDocumentoPorSolicitud',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response > 0) {
                        ShowSuccessBox("Info", "Archivo Adjuntado con éxito");
                    } else {
                        ShowWarningBox("Su Proceso no pudo ser completado", response);
                    }
                },
                error: function () {
                    StopLoading();
                    ShowWarningBox("Atención", "Su solicitud no pudo ser completada, favor verifique que el tamaño del archivo que intenta cargar no sobrepase los 4MB");
                },
                complete: function () {
                    StopLoading();
                    ListarSolicitudes();
                    ListarIntegrantes($("#hidCodAgrupamiento").val());
                    $('#CancelarAdjuntar').trigger("click");
                }
            });
        }
    });

    $('#modal-BtnAprobar').click(function () {
        var nroSolicitud = $('#modal-hidNroSolicitud').val();
        var nroDocumento = $('#modal-hidNroDocumento').val();
        var motivo = $('#modal-Motivo').val();
        var Comentario = $('#modal-txtComentario').val();
        var valid = true;

        if (Comentario.length < 3) {
            ShowWarningBox("Info", "Ingrese comentario válido");
            return
        }

        if ($('#modal-ArchivoText').val() === '') {
            ShowWarningBox("Info", "Adjunte Documento de Sustento");
            valid = false;
            return
        }

        if (valid) {
            var file = document.getElementById("modal-Archivo").files[0];
            var formData = new FormData();
            formData.append("fileSustento", file);
            formData.append("NumeroSolicitud", nroSolicitud);
            formData.append("NumeroDocumento", nroDocumento);
            formData.append("Motivo", motivo);
            formData.append("Comentario", Comentario);

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Grupo/EnviarParaAprobacion',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response === "Exito") {
                        ShowSuccessBox("Info", "Proceso realizado con éxito");
                    } else {
                        ShowWarningBox("Info", "Su proceso no pudo ser completado");
                    }
                },
                error: function () {
                    StopLoading();
                    ShowErrorBox("Error", "No se pudo completar la operación");
                },
                complete: function () {
                    StopLoading();
                    ListarIntegrantes($("#hidCodAgrupamiento").val());
                    $('#BtnCancelarAprobacion').trigger("click");
                }
            });
        }
    });

    // Eventos para Tipo de Cronograma
    $('#ModalTC-TipoCuota').change(function () {
        $('.div-ValidacionPlazo').hide();
        switch ($('#ModalTC-TipoCuota').val()) {
            case "9":
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').hide();
                $('#ModalTC-NumeroCuotas').val(1);
                $('#ModalTC-NumeroCuotas').prop('disabled', 'disabled');
                //$('#ModalTC-NumeroCuotas').trigger('change');
                break;
            case "1":
                $('#ModalTC-div-CuotasFijas').show();
                $('#ModalTC-div-CuotasVariables').hide();
                $('#ModalTC-NumeroCuotas').removeAttr('disabled');
                break;
            case "3":
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').show();
                $('#ModalTC-NumeroCuotas').removeAttr('disabled');
                $('#ModalTC-NumeroCuotas').trigger('change');
                break;
            default:
        }
    });
    $('#ModalTC-NumeroCuotas').change(function () {
        $('#RegistroCuotas').empty();

        for (var i = 1; i <= $('#ModalTC-NumeroCuotas').val(); i++) {

            $('#RegistroCuotas').append('' +
                '<div class= "" >' +
                '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append">%</i>' +
                '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100">' +
                '</label>' +
                '</section>' +
                '<section class="col col-5">' +
                '<label class="input">' +
                '<i class="icon-append fa fa-calendar"></i>' +
                '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy">' +
                '</label>' +
                '</section>' +
                '</div>'
            );
        }
        $('.cntrlFecha').datepicker({
            dateFormat: 'dd/mm/yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });
    });
    $('.plazo').change(function () {
        $('.fphelp').remove();        
        $('#ModalTC-hidMaxPlazo').attr('result', 'false');

        if (($('#ModalTC-FrecuenciaPago').val() - Math.floor($('#ModalTC-FrecuenciaPago').val())) != 0) {
            $('#ModalTC-FrecuenciaPago').after('<em class="help-block fphelp"><i class="fa fa-warning"></i> valor inválido </em>');
            return;
        }

        //var FrePago = isNaN($('#ModalTC-FrecuenciaPago').val()) || $('#ModalTC-FrecuenciaPago').val() == '' ?
        //    0 : $('#ModalTC-FrecuenciaPago').val();

        //var Plazo = parseInt(FrePago) * $('#ModalTC-NumeroCuotas').val();

        //if (Plazo > $('#ModalTC-hidMaxPlazo').val()) {
        //    //$('#ModalTC-FrecuenciaPago').addClass('has-error');
        //    $('#ModalTC-FrecuenciaPago').after('<em class="help-block fphelp"><i class="fa fa-warning"></i> Supera el plazo máximo </em>');
            
        //} else {
        //    $('#ModalTC-hidMaxPlazo').attr('result', 'true');
        //}

        $('#ModalTC-hidMaxPlazo').attr('result', 'true');

    });
    $('#ModalTC-FechaGracia').change(function () {
        $('.fghelp').remove();
        

        $('#ModalTC-FechaGracia').attr('result', 'false');
        $('#ModalTC-hidMaxDiasGracia').attr('result', 'false');

        var FechaGracia = $(this).val();
        if (FechaGracia == '' || FechaGracia == null) {            
            $('#ModalTC-FechaGracia').after('<em class="help-block fghelp"><i class="fa fa-warning"></i> Fecha inválida </em>');
            return;
        }

        var partesFecGracia = FechaGracia.split('/');
        var anioG = partesFecGracia[2];
        var mesG = partesFecGracia[1] - 1;
        var diaG = partesFecGracia[0];
        var dtG = new Date(anioG, mesG, diaG);
        
        if (dtG == null || anioG.length != 4 || dtG.getMonth() != mesG) {
            //ShowWarningBox('Observación', 'Fecha de Gracia incorrecto, verifique');            
            $('#ModalTC-FechaGracia').after('<em class="help-block fghelp"><i class="fa fa-warning"></i> Fecha inválida </em>');
            return;
        }

        var formData = new FormData();
        formData.append("FechaGracia", FechaGracia);

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Grupo/ValidarPeriodoGracia',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {
                if (response.key != "") {

                    $('#ModalTC-DiasGracia').val(response.value);

                    if (response.key != '0') {
                        
                        $('#ModalTC-FechaGracia').after('<em class="help-block fghelp"><i class="fa fa-warning"></i> Fecha es feriado </em>');
                    }
                    else {
                        if (response.value < 0) {
                            $('#ModalTC-FechaGracia').after('<em class="help-block fghelp"><i class="fa fa-warning"></i> Debe ser mayor o igual a fecha actual </em>');
                        } else {
                            $('#ModalTC-FechaGracia').attr('result', 'true');
                        }

                        //if (response.value > parseInt($("#ModalTC-hidMaxDiasGracia").val()) ) {
                        //    $('#ModalTC-DiasGracia').after('<em class="help-block fghelp"><i class="fa fa-warning"></i> Supera el máximo permitido </em>');
                        //} else {
                        //    $('#ModalTC-hidMaxDiasGracia').attr('result', 'true');
                        //}
                    }

                } else {
                    ShowWarningBox("No se pudo procesar su solicitud", response.value);
                }
            },
            error: function () {
                StopLoading();
                ShowWarningBox("Atención", "No se pudo completar la operación");
            },
            complete: function () {
                StopLoading();
            }
        });
    });

    $('#BtnGuardarTipoCronograma').click(function () {
        $('.div-ValidacionPlazo').hide();
        var nroSolicitud = $('#ModalTC-hidNroSolicitud').val();
        var TipoCuota = $('#ModalTC-TipoCuota').val();
        var FechaGracia = $('#ModalTC-FechaGracia').val();
        var NumeroCuotas = $('#ModalTC-NumeroCuotas').val();
        var FrecuenciaPago = $('#ModalTC-FrecuenciaPago').val();
        var UnidadFrecuencia = $('#ModalTC-UnidadFrecuencia').val();
        var Plazo = $('#ModalTC-hidMaxPlazo').val() == '' ? 0 : $('#ModalTC-hidMaxPlazo').val();
        var ValidaFechaCuotas = true;

        var Porcentajes = "|", Fechas = "|";

        if (TipoCuota == '' || TipoCuota == null) {
            ShowWarningBox('Observación', 'Seleccione un Tipo de Cuota válido');
            return;
        }

        // Cuota Fija
        if (TipoCuota == "1") {
            if (isNaN(FrecuenciaPago) || FrecuenciaPago == '' || FrecuenciaPago < 1 || (FrecuenciaPago - Math.floor(FrecuenciaPago)) != 0 ) {
                ShowWarningBox('Observación', 'Ingrese una Frecuencia de Pago válido');
                return;
            }
            
            if (UnidadFrecuencia == '' || UnidadFrecuencia == null) {
                ShowWarningBox('Observación', 'Seleccione Unidad de Frecuencia');
                return;
            }

            if (FechaGracia == '' || FechaGracia == null) {
                ShowWarningBox('Observación', 'Periodo de Gracia inválida, verifique');
                return;
            }

            var partesFecGracia = FechaGracia.split('/');
            var anioG = partesFecGracia[2];
            var mesG = partesFecGracia[1] - 1;
            var diaG = partesFecGracia[0];
            var dtG = new Date(anioG, mesG, diaG);
            if (dtG == null || anioG.length != 4) {
                ShowWarningBox('Observación', 'Periodo de Gracia inválida, verifique');
                return;
            }

            if ($('#ModalTC-FechaGracia').attr('result') != 'true'){
                ShowWarningBox('Observación', 'Periodo de Gracia incorrecto, verifique');
                return;
            }

            //if ($('#ModalTC-hidMaxDiasGracia').attr('result') != 'true') {
            //    ShowWarningBox('Observación', 'Días de gracia incorrecto, verifique');
            //    return;
            //}

            if ($('#ModalTC-hidMaxPlazo').attr('result') != 'true') {
                ShowWarningBox('Observación', 'El Número de cuotas y la fecuencia de pago no coinciden con el plazo de la solicitud, verifique');
                return;
            }
        }

        //Tipo Variable
        if ($('#ModalTC-TipoCuota').val() == "3") {

            var nPorcent = 0;
            $('.input-porcentajeCuota').each(function (i, obj) {
                Porcentajes = Porcentajes + $(this).val() + "|";
                var total = $(this).val();
                total = (total == null || total == undefined || total == "") ? 0 : total;
                nPorcent = nPorcent + parseInt(total);
            });
            $('.input-fechaCuota').each(function (i, obj) {
                var partesFec = $(this).val().split('/');
                var anio = partesFec[2].toString();
                var mes = partesFec[1];
                var dia = partesFec[0];

                var dt = new Date(anio, mes - 1, dia);
                if (dt == null || anio.length != 4) {
                    ValidaFechaCuotas = false;
                    return;
                }
                Fechas = Fechas + dia.toString().padStart(2, '0') + mes.toString().padStart(2, '0') + anio.substr(2, 2) + "|";
            });

            if (nPorcent !== 100) {
                ShowWarningBox('Observación', 'Los porcentajes de las cuotas no suman 100, verificar...');
                return;
            }
            if (!ValidaFechaCuotas) {
                ShowWarningBox('Observación', 'Fechas de cuotas inválidas, verificar...');
                return;
            }
            FrecuenciaPago = 0;
            UnidadFrecuencia = "";
            FechaGracia = "01/01/2020";

        }

        // Vencimiento
        if ($('#ModalTC-TipoCuota').val() == "9") {
            FrecuenciaPago = 0;
            UnidadFrecuencia = "";
            FechaGracia = "01/01/2020";
        }


        var formData = new FormData();
        formData.append("NumeroSolicitud", nroSolicitud);
        formData.append("TipoCuota", TipoCuota);
        formData.append("NumeroCuotas", NumeroCuotas);
        formData.append("FrecuenciaPago", FrecuenciaPago);
        formData.append("UnidadFrecuencia", UnidadFrecuencia);
        formData.append("FechaGracia", FechaGracia);
        formData.append("Plazo", Plazo);
        formData.append("PorcentajesCuotas", Porcentajes);
        formData.append("FechasCuotas", Fechas);        

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Grupo/RegistrarTipoCronograma',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                if (response.TipoError == "100") {
                    ShowSuccessBox("Info", "Proceso realizado con éxito");
                    $('#BtnCancelarTipoCronograma').trigger("click");
                    ListarSolicitudes();
                    ListarIntegrantes($("#hidCodAgrupamiento").val());
                }
                else {
                    if (response.TipoError = "9999") {
                        $('#p-ValidacionPlazo').text(response.Message);
                        $('.div-ValidacionPlazo').show();
                    }
                    else {
                        ShowWarningBox("Atención", "No se pudo completar su solicitude. \n " + response.Message);
                    }
                }

                

            },
            error: function () {
                StopLoading();
                ShowWarningBox("Atención", "No se pudo completar la operación");
            },
            complete: function () {
                StopLoading();
            }
        });

        

    });

});



// Metodos de Consultas Iniciales
function ListarSolicitudes() {
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ListarSolicitudes',
        //data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Solicitudes').DataTable().clear().draw();
                $('#datatable_Solicitudes').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "No se encuentran Solicitudes en proceso");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
            //ObtenerParametricasGrupo();            
        }
    });
}
function ListarAgrupamiento() {
    var requestData = {
        CodAgencia: $("#cboAgencia").val(),
        CodAnalista: $("#cboFuncionario").val()
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ListarAgrupamientos',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        //beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Agrupamientos').DataTable().clear().draw();
                $('#datatable_Agrupamientos').dataTable().fnAddData(GetAgrupamientosJson(response));
            } else {
                ShowInfoBox("Información", "La búsqueda no obtuvo ningun resultado, intente con otros filtros.");
            }
        },
        error: function () {
            //StopLoading();
        },
        complete: function () {
            //$("#Grupos").find(':checkbox').addClass('chkAT');

        }
    });
}
function ObtenerParametricasGrupo() {
    var dataRequest = {
        codOficinaRegional: 50
    };
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarParametricasGrupo',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        //beforeSend: function () {
        //    StarLoading();
        //},
        success: function (response) {
            //ListarParametricasGrupo(response);
            $.each(response, function (key, value) {
                switch (value.key) {
                    case "JUNT": cboJunta.append($('<option>', { value: value.code, text: value.value })); break;
                    case "BANK": cboBanco.append($('<option>', { value: value.code, text: value.value })); break;
                    case "ABON": cboAbono.append($('<option>', { value: value.code, text: value.value })); $("#ddlFormaAbono_Cliente").append($('<option>', { value: value.code, text: value.value })); break;
                    case "DEST": cboDesti.append($('<option>', { value: value.code, text: value.value })); break;
                    case "PAGO": cboFPago.append($('<option>', { value: value.code, text: value.value })); break;
                    case "CULT": cboCulti.append($('<option>', { value: value.code, text: value.value })); break;
                    case "DESG": cboDesgr.append($('<option>', { value: value.code, text: value.value })); break;
                    case "TASA": cboTasa.append($('<option>', { value: value.code, text: value.value })); break;
                    case "BANCO": $("#ddlBanco_Abono").append($('<option>', { value: value.code, text: value.value })); break;
                    case "AGEN": $("#cboAgencia").append($('<option>', { value: value.code, text: value.value })); break;
                    case "FUNC": $("#cboFuncionario").append($('<option>', { value: value.code, text: value.value })); break;
                }
            });
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            //StopLoading();
            $("#cboAgencia").val($("#hidCodAgencia").val());
            $("#cboFuncionario").val($("#hidCodFuncionario").val());
            ListarAgrupamiento();
        }
    });
}

function showModal() {
    $('#dialog-message').dialog('open');
    return false;
}

$('#btnLimpiar').click(function () {
    $('#datatable_Agrupamientos').DataTable().clear().draw();
});
// Métodos Agrupamiento
function UpdListaAgrupamientos() {
    var requestData = {
        CodAgencia: $("#cboAgencia").val(),
        CodAnalista: $("#cboFuncionario").val()
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ListarAgrupamientos',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Agrupamientos').DataTable().clear().draw();
                $('#datatable_Agrupamientos').dataTable().fnAddData(GetAgrupamientosJson(response));
            } else {
                ShowInfoBox("Información", "La búsqueda no obtuvo ningun resultado, intente con otros filtros.");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}
function GetAgrupamientosJson(Agrupamientos) {
    //var dataJson = Agrupamientos;
    $.each(Agrupamientos, function (key, value) {
        //value.Nivel = "<span class='label label-success'>" + value.Nivel + "</span>";
        //value.Estado = "<td><div class='progress progress-sm' data-progressbar-value='" + parseInt(value.Estado) * 25 + "'><div class='progress-bar'></div></div></td>";
        value.Evaluacion_html = '' + //"<td><div class='progress progress-sm' data-progressbar-value='80'><div class='progress-bar'></div></div></td>";
            '<td><span class="sparkline display-inline margin-bottom-10" data-sparkline-type="pie" data-sparkline-offset="90" data-sparkline-piesize="23px"> ' +
            '</span ></td>';

    });
    return Agrupamientos;
}
function VerDetalleGrupo(IdGrupo) {
    ViewAndHide("#div-DetalleAgrupamiento", "#div-RelacionAgrupamientos");
    $("#hidCodAgrupamiento").val(IdGrupo);
    ObtenerAgrupamiento(IdGrupo);
}
function AnularGrupo(IdGrupo) {
    $.SmartMessageBox({
        title: "<i class='fa fa-warning'></i> Atención...!!",
        content: "Esta acción anulará todas las solicitudes del Agrupamiento " + IdGrupo + ", ¿Desea continuar?",
        buttons: '[No][Yes]'
    }, function (ButtonPressed) {
        var formData = new FormData();
        formData.append("CodigoGrupo", IdGrupo);

        if (ButtonPressed === "Yes") {

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Grupo/AnularGrupo',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response === "Exito") {
                        ShowSuccessBox("Info!", "Proceso realizado con éxito");

                    } else {
                        ShowErrorBox("Error", response);
                    }
                },
                error: function () {
                    StopLoading();
                    ShowErrorBox("Error", "No se pudo completar la operación, por favor consultar con Sistemas");
                },
                complete: function () {
                    StopLoading();
                    $('#btnBuscar').trigger("click");
                }
            });


        }
    });
}

// Métoso Integrantes
function ListarIntegrantes(IdGrupo) {

    if (IdGrupo == '' || IdGrupo == null) {
        return;
    };

    var requestData = {
        CodAgrupamiento: IdGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ListarIntegrantesPorAgrupamiento',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Integrantes').DataTable().clear().draw();
                $('#datatable_Integrantes').dataTable().fnAddData(response);
            } else {
                //ShowInfoBox("Información", "Agrupamiento no cuenta con integrantes.");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}
function VerDetalleIntegrante(IdIntegrante) {
    ObtenerDatosIntegrante(IdIntegrante);

}

function GenerarPropuesta(solicitud) {
    var requestData = {
        CodSolicitud: solicitud
    };


    $.ajax({
        cache: false,
        type: "POST",
        url: '../Propuesta/Generar',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.code === '99') {
                //ShowSuccessBox("Mensaje Sistemas", response.message);
                $('.id-propuesta').text(response.message);
                showModal();
            } else {
                ShowErrorBox("Información", "No se pudo completar la solicitud");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
            ListarSolicitudes();
            ListarIntegrantes($("#hidCodAgrupamiento").val());
        }
    });
}
function VerPropuesta(NumeroSolicitud) {
    //window.location.href = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    var url = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    window.open(url, '_blank');
}

/*AGRO-REQ-023*/
function IniciarVinculacion(numeroSolicitud, numeroDocumento) {
    var date = new Date();
    var requestData = {
        idtoken: numeroSolicitud + '' + numeroDocumento + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds(),
    };
    var UrlBaseSGV = $("#UrlBaseSGV").val();
    var UrlBaseSGVAPI = $("#UrlBaseSGVAPI").val();

    $.ajax({
        cache: false,
        type: "POST",
        url: UrlBaseSGVAPI+'vinculacion/registrar/',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {

            if (response.exito == true) {
                var data = response.data;
                var IdToken = requestData.idtoken;
                var urlvisita = UrlBaseSGV+"vinculacion?IdToken=" + IdToken + "&NumeroDocumento=" + numeroDocumento + "&NumeroSolicitud=" + numeroSolicitud
                window.open(urlvisita, '_blank');
            }
            else {
                //ShowInfoBox("Información", response.Message);
                ShowErrorBox("Error", response.Message)
            }
        },
        error: function (e) {
            console.log(e);
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}
//AGRO-REQ-023


function Georeferenciar(NumeroSolicitud, NumeroDocumento, Nombre) {
    window.location.href = '../Georeferenciacion/Vincular?NroDocumento=' + NumeroDocumento + '&NroSolicitud=' + NumeroSolicitud + '&Nombre=' + Nombre;
}
function ConsultarGeoreferencia(NumeroSolicitud, NumeroDocumento, Nombre) {
    window.location.href = '../Georeferenciacion/ConsultaFotosSolicitud?NumeroSolicitud=' + NumeroSolicitud + '&NumeroDocumento=' + NumeroDocumento + '&Nombre=' + Nombre;
}

function DescargarDocumento(idLaserFiche) {
    var url = '../Grupo/DescargarDocumentoPorId?IdLaserFiche=' + idLaserFiche;
    window.open(url, '_blank');
}

function AnularSolicitud(Solicitud) {
    $.SmartMessageBox({
        title: "<i class='fa fa-warning'></i> Atención...!!",
        content: "Anular Solicitud " + Solicitud + ". Esta acción no se podrá deshacer, ¿Desea continuar?",
        buttons: '[No][Yes]'
    }, function (ButtonPressed) {
        var formData = new FormData();
        formData.append("NumeroSolicitud", Solicitud);

        if (ButtonPressed === "Yes") {

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Grupo/AnularSolicitud',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response === "100") {
                        ShowSuccessBox("Info!", "Proceso realizado con éxito");
                        ListarSolicitudes();
                        ListarIntegrantes($("#hidCodAgrupamiento").val());
                    } else {
                        ShowSuccessBox("Atención", "No se pudo completar su proceso... " + response);
                    }
                },
                error: function () {
                    StopLoading();
                    ShowSuccessBox("Atención", "No se pudo completar la operación, por favor consultar con Sistemas");
                },
                complete: function () {
                    StopLoading();
                }
            });


        }
    });
}

// Métodos para gráfico de estadísticas
function VerEstadisticasGrupo(CodGrupo, NombreGrupo) {
    //$.when($("#btnModal").trigger("click")).then($("#btnChart").trigger("click"));
    var requestData = {
        CodigoGrupo: CodGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ConsultarResumenCalificacionesPorAgrupamiento',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                ViewAndHide("#div-Grupo-CalificacionesSBS", "#div-RelacionAgrupamientos");
                $(".txt-grupo").html(NombreGrupo);
                Set_Grupo_ResumenCalficaciones(response);
            } else {
                ShowInfoBox("Información", "La consulta no tuvo resultados.");
            }
        },
        error: function () {
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, si el error persiste reportarlo con Sistemas");
        },
        complete: function () {
            StopLoading();
        }
    });
}
function Set_Grupo_ResumenCalficaciones(CalificacionesSBS) {
    var totReg = 0,
        maxAxisX = 0;

    $.each(CalificacionesSBS, function () { totReg += parseFloat(this.value) || 0; });


    if ($("#bar-chart-h").length) {
        //Display horizontal graph
        var d1_h = [], d2_h = [], d3_h = [], d4_h = [], d5_h = [], d6_h = [];

        $.each(CalificacionesSBS, function (key, value) {
            switch (value.key) {
                case "1": d1_h.push([parseFloat(value.value), 1]); setFila("Normal", value.value, value.value / totReg * 100); break;
                case "2": d2_h.push([parseFloat(value.value), 2]); setFila("CPP", value.value, value.value / totReg * 100); break;
                case "3": d3_h.push([parseFloat(value.value), 3]); setFila("Deficiente", value.value, value.value / totReg * 100); break;
                case "4": d4_h.push([parseFloat(value.value), 4]); setFila("Dudoso", value.value, value.value / totReg * 100); break;
                case "5": d5_h.push([parseFloat(value.value), 5]); setFila("Perdida", value.value, value.value / totReg * 100); break;
                case "6": d6_h.push([parseFloat(value.value), 6]); setFila("SinHistorial", value.value, value.value / totReg * 100); break;
            }
            maxAxisX = maxAxisX < parseFloat(value.value) ? parseFloat(value.value) : maxAxisX;
        });
        //$("#tdCantidadTotal").text(totReg);
        setFila("Total", totReg, 100);
        maxAxisX = maxAxisX < 10 ? maxAxisX * 1.2 :
            maxAxisX < 100 ? Math.floor(maxAxisX * 1.1) :
                maxAxisX < 1000 ? Math.floor(maxAxisX * 1.05) :
                    Math.floor(maxAxisX * 1.02);

        if (d1_h.length === 0) { d1_h.push([0, 1]); setFila("Normal", 0, 0); }
        if (d2_h.length === 0) { d2_h.push([0, 2]); setFila("CPP", 0, 0); }
        if (d3_h.length === 0) { d3_h.push([0, 3]); setFila("Deficiente", 0, 0); }
        if (d4_h.length === 0) { d4_h.push([0, 4]); setFila("Dudoso", 0, 0); }
        if (d5_h.length === 0) { d5_h.push([0, 5]); setFila("Perdida", 0, 0); }
        if (d6_h.length === 0) { d6_h.push([0, 6]); setFila("SinHistorial", 0, 0); }

        var ds_h = new Array();
        var widthBar = 10;
        ds_h.push({
            data: d6_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 5
            }
        });

        ds_h.push({
            data: d5_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 4
            }
        });

        ds_h.push({
            data: d4_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 3
            }
        });

        ds_h.push({
            data: d3_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 2
            }
        });

        ds_h.push({
            data: d2_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 1
            }
        });

        ds_h.push({
            data: d1_h,
            bars: {
                horizontal: true,
                align: "center",
                show: true,
                barWidth: widthBar,
                order: 0
            }
        });


        // display graph
        $.plot($("#bar-chart-h"), ds_h, {
            colors: ["#808080", "#ff0000", "#ff6a00", "#ffd800", "#b6ff00", "#4cff00"],
            lines: {
                show: false
            },
            xaxis: {
                max: maxAxisX
            },
            yaxis: {
                min: -37,
                max: 35,
                //ticks: []
                ticks: [[-29, "Normal"], [-18, "CPP"], [-7, "Deficiente"], [4, "Dudoso"], [15, "Pérdida"], [26, "S/Historial"]]
            },
            grid: {
                show: true,
                hoverable: true,
                clickable: true,
                tickColor: "#abebc6",
                borderWidth: 1,
                borderColor: "#abebc6"
            },
            legend: true,
            tooltip: true,
            tooltipOpts: {
                content: "Total: <span>%x</span>",
                defaultTheme: false
            }
        });

    }
}
function setFila(calif, cantidad, porcentaje) {
    $("#tdCantidad" + calif).text(cantidad);
    $("#tdPorcent" + calif).text(porcentaje.toFixed(0) + ' %');
}

$("#btnRegresarConsultaGrupos").click(function () {
    //ViewAndHide("#div-RelacionAgrupamientos", "#div-Grupo-CalificacionesSBS");
    $('#datatable_Integrantes').DataTable().clear().draw();
});



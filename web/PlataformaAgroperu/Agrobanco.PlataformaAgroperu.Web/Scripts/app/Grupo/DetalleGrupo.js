﻿var cboJunta = $("#ddlJuntaRegantes"),
    cboComis = $("#ddlComisionRegante"),
    cboTipDo = $("#ddlAsistTipoDoc"),
    cboNumDo = $("#TxtAsistNumDoc"),
    cboAsistNombre = $("#TxtAsistNombre"),
    cboBanco = $("#ddlBanco"),
    cboAbono = $("#ddlFormaAbono"),
    cboDesti = $("#ddlDestino"),
    cboTasa = $("#TxtTasa"),
    cboFPago = $("#ddlFormaPago"),
    TxtPlazo = $("#TxtPlazo"),
    cboProdu = $("#ddlProducto"),
    cboCIIU_ = $("#ddlCIIU"),
    cboCulti = $("#ddlCultivo"),
    cboDesgr = $("#ddlSeguroDesgravamen"),
    cboAgric = $("#ddlAgricola");
    
var CodComisionRegante;

$("#btnRegresarRelAgrup").click(function () {
    ViewAndHide("#div-RelacionAgrupamientos", "#div-DetalleAgrupamiento");
    $('#datatable_Integrantes').DataTable().clear().draw();
});

$("#ddlJuntaRegantes").on('change', function (e) {
    var CboComisionRegante = $("#ddlComisionRegante");
    var dataRequest = {
        CodJuntaRegante: this.value
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarComisionRegante',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            CboComisionRegante.find('option').remove();
            CboComisionRegante.append($('<option>', { value: "", text: "[Seleccione Comision]" }));

            $.each(response, function (key, value) {
                CboComisionRegante.append($('<option>', { value: value.code, text: value.value }));
            });
        },
        error: function () {
            StopLoading();
            CboComisionRegante.find('option').remove();
            CboComisionRegante.prepend($('<option></option>').html('Error al cargar Comisiones'));
        },
        complete: function () {
            StopLoading();
            cboComis.val(CodComisionRegante);
        }
    });

});

function ObtenerAgrupamiento(IdGrupo) {
    
    var requestData = {
        CodAgrupamiento: IdGrupo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Grupo/ObtenerDatosAgrupamiento',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            CodComisionRegante = response.DatosGrupo.CodComisionRegante;
            SetDatosAgrupamiento(response.DatosGrupo);
            $("#ddlJuntaRegantes").trigger("change");
            if (response.ExcelIntegrantes.length > 0) {
                $('#datatable_Integrantes').DataTable().clear().draw();
                $('#datatable_Integrantes').dataTable().fnAddData(response.ExcelIntegrantes);
            } else {
                //ShowInfoBox("Información", "Agrupamiento no cuenta con integrantes.");
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {           
            StopLoading();
        }
    });
}

function SetDatosAgrupamiento(agrupamiento) {
    $("#LblNomAgrup").text(agrupamiento.NombreGrupo);
    cboJunta.val(agrupamiento.CodJuntaRegante);
    
    cboTipDo.val(agrupamiento.TecnicoTipoDocumento);
    cboNumDo.val(agrupamiento.TecnicoNroDocumento);
    cboAsistNombre.val(agrupamiento.TecnicoNombre),
    cboBanco.val(agrupamiento.Banco);
    cboAbono.val(agrupamiento.FormaAbono);
    cboDesti.val(agrupamiento.Destino);
    cboTasa.val(agrupamiento.Tasa);
    cboFPago.val(agrupamiento.FormaPago);    
    cboCulti.val(agrupamiento.Cultivo);
    cboDesgr.val(agrupamiento.SeguroDesgravamen);    
}

function VerDetalleIntegrante(IdIntegrante) {
    ObtenerDatosIntegrante(IdIntegrante);

}

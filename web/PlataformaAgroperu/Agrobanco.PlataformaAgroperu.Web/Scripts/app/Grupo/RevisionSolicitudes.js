﻿$(document).ready(function () {    
    var responsiveHelper_datatable_Solcitudes = undefined;
    var responsiveHelper_datatable_Revision = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    // Modal Aprobacion por excepcion
    $('#myModal-AprobExcepcion').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data().id,
            nom = $(e.relatedTarget).data().nom,
            doc = $(e.relatedTarget).data().doc;

        $(e.currentTarget).find('#modal-txtCliente').val(nom);
        $(e.currentTarget).find('#modal-hidNroSolicitud').val(id);
        $(e.currentTarget).find('#modal-hidNroDocumento').val(doc);
        $(e.currentTarget).find('#modal-Respuesta').val('');
        $(e.currentTarget).find('#modal-txtObservacion').val('');

    });

    // Modal Tipo de Cronograma
    $('#myModal-TipoCronograma').on('show.bs.modal', function (e) {
        $('.div-ValidacionPlazo').hide();
        $(e.currentTarget).find('.help-block').remove();
        var Tipo = $(e.relatedTarget).data().tip,
            sol = $(e.relatedTarget).data().sol,
            nom = $(e.relatedTarget).data().nom,
            tcr = $(e.relatedTarget).data().tcr;
        var Cuotas = 0, MaxCuotas = 1;

        var DiasGracia = 0;
        var Permitidos = '';
        var FrecuenciaPago = '', UnidadFrecuencia = '', FechaGracia = '', Porcentajes = '', Fechas = '';

        switch (tcr) {
            case 1:
                FrecuenciaPago = $(e.relatedTarget).data().fpa;
                UnidadFrecuencia = $(e.relatedTarget).data().ufr;
                FechaGracia = $(e.relatedTarget).data().pgr;

                break;

            case 3:
                Porcentajes = $(e.relatedTarget).data().prc;
                Fechas = $(e.relatedTarget).data().fec;
                break;

            default:
                break;
        }

        Cuotas = $(e.relatedTarget).data().ncu;
        $('.lectura').val('');
        if (Tipo == '1') {

            Permitidos = tcr;
            $('.lectura').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').prop('disabled', true);
            $('#BtnGuardarTipoCronograma').hide();
            $('#Sec-DiasGracia').hide();
            $('#BtnCancelarTipoCronograma').text('Cerrar');
        }
        else {
            Permitidos = $(e.relatedTarget).data().cpe;
            MaxCuotas = $(e.relatedTarget).data().mxc;
            $(e.currentTarget).find('#ModalTC-hidMaxDiasGracia').val($(e.relatedTarget).data().mxg);
            $(e.currentTarget).find('#ModalTC-hidMaxPlazo').val($(e.relatedTarget).data().mxp);

            $('.lectura').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').prop('disabled', false);
            $('#BtnGuardarTipoCronograma').show();
            $('#Sec-DiasGracia').show();
            $('#BtnCancelarTipoCronograma').text('Cancelar');

            $('#ModalTC-NumeroCuotas').prop('disabled', false);
        }

        $(e.currentTarget).find('#ModalTC-NomCliente').text(sol + ' - ' + nom);
        $(e.currentTarget).find('#ModalTC-hidNroSolicitud').val(sol);
        $('#ModalTC-UnidadFrecuencia').val('M');

        //Asigna Tipos de Cuota
        $('#ModalTC-TipoCuota').find('option').remove();
        var Cadena = Permitidos.toString();

        for (var i = 0; i < Cadena.length; i++) {
            switch (Cadena.charAt(i)) {
                case '1': $('#ModalTC-TipoCuota').append($('<option>', { value: "1", text: "Cuotas Fijas" })); break;
                case '3': $('#ModalTC-TipoCuota').append($('<option>', { value: "3", text: "Cuotas Variables" })); break;
                case '9': $('#ModalTC-TipoCuota').append($('<option>', { value: "9", text: "Vencimiento" })); break;
            }
        }

        // Asigna Número de cuotas
        $('#ModalTC-NumeroCuotas').find('option').remove();
        $('#RegistroCuotas').empty();
        if (tcr == '1') {
            $('#ModalTC-FechaGracia').val(FechaGracia);
            $('#ModalTC-FrecuenciaPago').val(FrecuenciaPago);
        }

        var ListaPorcentajes = [];
        var ListaFechas = [];

        if (Tipo == '1') {
            ListaPorcentajes = Porcentajes.split('|');
            ListaFechas = Fechas.split('|');
            $('#ModalTC-NumeroCuotas').append($('<option>', { value: Cuotas, text: Cuotas }));
            if (tcr == '3') {
                for (var i = 1; i <= Cuotas; i++) {
                    var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                    $('#RegistroCuotas').append('' +
                        '<div class= "" >' +
                        '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                        '<section class="col col-4">' +
                        '<label class="input">' +
                        '<i class="icon-append">%</i>' +
                        '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" disabled="disabled" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                        '</label>' +
                        '</section>' +
                        '<section class="col col-5">' +
                        '<label class="input">' +
                        '<i class="icon-append fa fa-calendar"></i>' +
                        '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" disabled="disabled" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                        '</label>' +
                        '</section>' +
                        '</div>'
                    );
                }
            }
        }
        else {
            if (Cuotas == 0) {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                    if (tcr == "3") {
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }
            }
            else {
                for (var i = 1; i <= MaxCuotas; i++) {
                    $('#ModalTC-NumeroCuotas').append($('<option>', { value: i, text: i }));
                }

                if (tcr == '3') {
                    ListaPorcentajes = Porcentajes.split('|');
                    ListaFechas = Fechas.split('|');
                    for (var i = 1; i <= Cuotas; i++) {
                        var fechaF = ListaFechas[i].substr(0, 2) + '/' + ListaFechas[i].substr(2, 2) + '/' + ListaFechas[i].substr(4, 2)
                        $('#RegistroCuotas').append('' +
                            '<div class= "" >' +
                            '<label class="label col col-3">N° Cuota <span>' + i + '</span></label>' +
                            '<section class="col col-4">' +
                            '<label class="input">' +
                            '<i class="icon-append">%</i>' +
                            '<input type="number" class="input-porcentajeCuota" name="" placeholder="% Cuota" min="0" max="100" value = "' + ListaPorcentajes[i] + '">' +
                            '</label>' +
                            '</section>' +
                            '<section class="col col-5">' +
                            '<label class="input">' +
                            '<i class="icon-append fa fa-calendar"></i>' +
                            '<input type="text" class="cntrlFecha input-fechaCuota" name="" placeholder="Fecha Cuota" data-dateformat="dd/mm/yy" value="' + fechaF + '">' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );
                    }
                }

            }
        }

        $('.cntrlFecha').datepicker({
            dateFormat: 'dd/mm/yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
        });

        // Setea valores predeterminados
        $('#ModalTC-TipoCuota').val(tcr);
        $('#ModalTC-NumeroCuotas').val(Cuotas);
        switch (tcr) {
            case 9:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').hide();
                $('#ModalTC-NumeroCuotas').val(1);
                $('#ModalTC-NumeroCuotas').attr('disabled', true);
                break;
            case 1:
                $('#ModalTC-div-CuotasFijas').show();
                $('#ModalTC-div-CuotasVariables').hide();
                break;
            case 3:
                $('#ModalTC-div-CuotasFijas').hide();
                $('#ModalTC-div-CuotasVariables').show();
                break;
            default:
        }

    });

    /* Config. Tabla Solicitudes */
    var TableRevisionSolicitudes = $('#datatable_RevisionSolicitudes').DataTable({
        "data": [],
        //"oSearch": { "sSearch": "APROBADO" },
        "order": [1, "desc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Revision) {
                responsiveHelper_datatable_Revision = new ResponsiveDatatablesHelper($('#datatable_RevisionSolicitudes'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Revision.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Revision.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                'searchable': false,
                'orderable': false,
                'width': '3%',
                //'className': 'dt-body-center',
                "data": "TramaOpciones"
            },
            {
                "data": "NumeroSolicitud",
                'width': '5%'
            },
            //{ "data": "DatosCliente.TipoDocumento.value" },
            {
                "data": "DatosCliente.NroDocumento.value",
                'width': '5%'
            },
            { "data": "DatosCliente.NomPrimer.value" },
            {
                "data": "DatosSolicitud.Cultivo.value",
                "className": "text-center",
                //"width": "12%"
            },
            {
                "data": "DatosSolicitud.Moneda",
                "className": "text-center",
                "width": "5%"
            },
            {
                "data": "DatosSolicitud.MontoSolicitadoSoles.value",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                'searchable': false,
                'orderable': false,
                'width': '5%',
                'className': 'dt-body-center',
                "data": "Trama1" // Para descarga de Sustento
            }
            
        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    var TableSolicitudes = $('#datatable_Solicitudes').DataTable({
        "data": [],
        //"oSearch": { "sSearch": "APROBADO" },
        "order": [0, "desc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Solcitudes) {
                responsiveHelper_datatable_Solcitudes = new ResponsiveDatatablesHelper($('#datatable_Solicitudes'), breakpointDefinition);
            }
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.FlagExcepcion == '1') {
                $(row).find('td:eq(1)').addClass('text-warning');
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Solcitudes.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Solcitudes.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                "data": "NumeroSolicitud",
                'width': '5%'
            },
            //{ "data": "DatosCliente.TipoDocumento.value" },
            {
                "data": "DatosCliente.NroDocumento.value",
                'width': '5%'
            },
            { "data": "DatosCliente.NomPrimer.value" },
            {
                "data": "DatosSolicitud.MontoSolicitadoSoles.value",
                "render": $.fn.dataTable.render.number(",", ".", 2),
                "className": "text-right",
                "width": "7%"
            },
            {
                "data": "EstadoEvaluacion",
                "className": "text-center",
                "width": "5%"
            },
            {
                "data": "EstadoTipoCuota",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoGeoreferencia",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeVisita",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoFlujoCaja",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoInformeComercial",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoPropuesta",
                "className": "text-center",
                "width": "3%"
            },
            {
                "data": "EstadoSolicitud",
                "className": "text-center",
                'width': '3%'
            }

        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    ListarAgencias();
    $("#cboAgencia").on('change', function (e) {
        var CboAnalista = $("#cboFuncionario");
        var dataRequest = {
            CodAgencia: this.value
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarFuncionarios',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {
                CboAnalista.find('option').remove();
                CboAnalista.append($('<option>', { value: "", text: "[Seleccione]" }));
                $.each(response, function (key, value) {
                    CboAnalista.append($('<option>', { value: value.code, text: value.value }));
                });

            },
            error: function () {
                StopLoading();
                CboAnalista.find('option').remove();
                CboAnalista.prepend($('<option></option>').html('No se pudo obtener lista'));
            },
            complete: function () {
                StopLoading();
            }
        });
    });
    $('#btnBuscar').click(function () {
        ListarSolicitudesRevision();
        //$('a[rel]').popover();
    });

    $('#modal-BtnAprobar').click(function () {
        var nroSolicitud = $('#modal-hidNroSolicitud').val();
        var nroDocumento = $('#modal-hidNroDocumento').val();
        var Respuesta = $('#modal-Respuesta').val();
        var Observacion = $('#modal-txtObservacion').val();
        var valid = true;

        if (Observacion.length < 2) {
            ShowWarningBox("Info", "Ingrese comentario válido");
            return
        }

        if ($('#modal-Respuesta').val() === '' || $('#modal-Respuesta').val() === null) {
            ShowWarningBox("Info", "Seleccione respuesta...");
            valid = false;
            return
        }

        if (valid) {
            var formData = new FormData();
            formData.append("NumeroSolicitud", nroSolicitud);            
            formData.append("Observacion", Observacion);
            formData.append("Respuesta", Respuesta);

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Grupo/AprobarExcepcion',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response === "100") {
                        ShowSuccessBox("Info", "Proceso realizado con éxito");
                    } else {
                        ShowWarningBox("Info", "El proceso no puede ser completado");
                    }
                },
                error: function () {
                    StopLoading();
                    ShowErrorBox("Error", "No se pudo completar la operación");
                },
                complete: function () {
                    StopLoading();
                    ListarSolicitudesRevision();
                    $('#BtnCancelarAprobacion').trigger("click");
                }
            });
        }
    });

    // Metodos de Consultas Iniciales
    function ListarSolicitudesRevision() {
        if ($("#cboAgencia").val() === null || $("#cboAgencia").val() === '') {
            ShowWarningBox("Mensaje Sistemas", "Debe seleccionar la agencia");
            return;
        }
        if ($("#cboFuncionario").val() === null || $("#cboFuncionario").val() === '') {
            ShowWarningBox("Mensaje Sistemas", "Debe seleccionar el funcionario");
            return;
        }

        var requestData = {
            CodAgencia: $("#cboAgencia").val(),
            CodAnalista: $("#cboFuncionario").val()
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Grupo/ListarSolicitudesEnRevision',
            data: JSON.stringify(requestData),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                $('#datatable_Solicitudes').DataTable().clear().draw();
                $('#datatable_RevisionSolicitudes').DataTable().clear().draw();

                if (response.Lista1.length > 0) {
                    $('#datatable_Solicitudes').dataTable().fnAddData(response.Lista1);
                }

                if (response.Lista2.length > 0) {
                    $('#datatable_RevisionSolicitudes').dataTable().fnAddData(response.Lista2);

                }


            },
            error: function () {
                ShowWarningBox("Información", "No se pudo completar su solicitud");
                StopLoading();
            },
            complete: function () {
                StopLoading();
                $('a[rel]').popover();
            }
        });
    }
    function ListarAgencias() {
        //var dataRequest = {
        //    codOficinaRegional: 50
        //};
        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarAgencias',
            //data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {
                $("#cboAgencia").find('option').remove();
                $.each(response, function (key, value) {
                    $("#cboAgencia").append($('<option>', { value: value.code, text: value.value }));
                });
            },
            error: function () {
                StopLoading();
                $("#cboAgencia").find('option').remove();
                $("#cboAgencia").prepend($('<option></option>').html('No se pudo obtener lista'));
            },
            complete: function () {
                StopLoading();
                $("#cboAgencia").val($("#hidCodAgencia").val());                
                ListarFuncionarios();
            }
        });
    }
    function ListarFuncionarios() {
        var dataRequest = {
            CodAgencia: $("#cboAgencia").val()
        };
        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarFuncionarios',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {
                $("#cboFuncionario").find('option').remove();
                $.each(response, function (key, value) {
                    $("#cboFuncionario").append($('<option>', { value: value.code, text: value.value }));
                });
            },
            error: function () {
                StopLoading();
                $("#cboFuncionario").find('option').remove();
                $("#cboFuncionario").prepend($('<option></option>').html('No se pudo obtener lista'));
            },
            complete: function () {
                StopLoading();                
                //$("#btnBuscar").trigger("click");
            }
        });
    }


});

function VerPropuesta(NumeroSolicitud) {
    //window.location.href = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    var url = '../Propuesta/CreatePdf?cod_solicitud=' + NumeroSolicitud;
    window.open(url, '_blank');
}
function ConsultarGeoreferencia(NumeroSolicitud, NumeroDocumento, Nombre) {
    window.location.href = '../Georeferenciacion/ConsultaFotosSolicitud?NumeroSolicitud=' + NumeroSolicitud + '&NumeroDocumento=' + NumeroDocumento + '&Nombre=' + Nombre;
}
function DescargarDocumento(idLaserFiche) {
    var url = '../Grupo/DescargarDocumentoPorId?IdLaserFiche=' + idLaserFiche;
    window.open(url, '_blank');
}
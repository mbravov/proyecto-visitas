﻿$(document).ready(function () {
    var responsiveHelper_datatable_Integrantes = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    $('#myModal-NuevaComision').on('show.bs.modal', function (e) {

        var Organizacion = $("#CboOrganizacion option:selected").text();
        console.log($("#CboOrganizacion").val());

        if ($("#CboOrganizacion").val() === null) {
            ShowInfoBox("Info", "Seleccione una organización!");
            //$('#myModal-NuevaComision').modal('toggle');
            
        } else {
            $(e.currentTarget).find('#modal1-txtNomOrganizacion').val(Organizacion);    
        }
        
        
    });

    $('#myModal-EditarComision').on('show.bs.modal', function (e) {
        var IdCom = $(e.relatedTarget).data().com,
            NomCom = $(e.relatedTarget).data().nom

        $(e.currentTarget).find('#modal2-txtNomOrganizacion').val($("#CboOrganizacion option:selected").text());
        $(e.currentTarget).find('#modal2-txtComision').val(NomCom);
        $(e.currentTarget).find('#modal2-hidCodComision').val(IdCom);        
    });

    /* Config. Tabla Comisiones */
    var oComisiones = $('#datatable_Comisiones').DataTable({
        "data": [],
        //"order": [1, "asc"],
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth": true,
        "preDrawCallback": function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_Integrantes) {
                responsiveHelper_datatable_Integrantes = new ResponsiveDatatablesHelper($('#datatable_Comisiones'), breakpointDefinition);
            }
        },
        "rowCallback": function (nRow) {
            responsiveHelper_datatable_Integrantes.createExpandIcon(nRow);
        },
        "drawCallback": function (oSettings) {
            responsiveHelper_datatable_Integrantes.respond();
        },
        "bServerSide": false,
        "columns": [
            {
                'searchable': false,
                'orderable': false,
                //'align': 'center',
                'width': '6%',
                'className': 'text-center',
                "data": "trama"
            },
            {
                "data": "code",
                'className': 'text-center',
                'width': '12%'
            },
            
            { "data": "value" }            

        ],
        "error": function (xhr, error, thrown) {
            alert('Ocurrio un error al procesar');
        }
    });

    ListarOrganizaciones();
    // Metodos de botones
    $("#CboOrganizacion").on('change', function (e) {
        ListarComisiones();
    });

    $("#modal-BtnCrearComision").click(function () {
        var dataRequest = {
            CodigoOrganizacion: $("#CboOrganizacion").val(),
            NombreComision: $("#modal1-txtComision").val().toUpperCase()
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Mantenimiento/CrearComision',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                if (response.TipoError === "0") {
                    ShowInfoBox("Información", "Datos Guardado con éxito!");
                    ListarComisiones();
                } else {
                    ShowWarningBox("Atención", "Su proceso no pudo ser completado...");
                }
            },
            error: function () {
                StopLoading();
                ShowErrorBox("Atención", "Su proceso no pudo ser completado...");
            },
            complete: function () {
                StopLoading();
                $("#modal1-txtComision").val('');
                $("#BtnCancelarNuevaComision").trigger('click');
            }
        });
    });

    $("#modal-BtnActualizarComision").click(function () {
        var dataRequest = {
            CodigoOrganizacion: $("#CboOrganizacion").val(),
            CodigoComision: $("#modal2-hidCodComision").val(),
            NombreComision: $("#modal2-txtComision").val().toUpperCase()
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Mantenimiento/ActualizarComision',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () { StarLoading(); },
            success: function (response) {
                if (response.TipoError === "0") {
                    ShowInfoBox("Información", "Datos Guardado con éxito!");
                    ListarComisiones();
                } else {
                    ShowWarningBox("Atención", "Su proceso no pudo ser completado...");
                }
            },
            error: function () {
                StopLoading();
                ShowWarningBox("Atención", "Su proceso no pudo ser completado...");
            },
            complete: function () {
                StopLoading();
                $("#modal2-txtComision").val('');
                $("#BtnCancelarActualizarComision").trigger('click');
            }
        });
    })


    
});

//Funciones
function ListarOrganizaciones() {
    var requestData = {
        CodigoAgencia: $("#hidCodAgencia").val()
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarOrganizaciones',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $.each(response, function (key, value) {
                    $("#CboOrganizacion").append($('<option>', { value: value.code, text: value.value }));
                });
            }
        },
        error: function () {
            StopLoading();
        },
        complete: function () {
            StopLoading();
        }
    });
}

function ListarComisiones() {
    var dataRequest = {
        CodigoOrganizacion: $("#CboOrganizacion").val()
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarComisiones',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Comisiones').DataTable().clear().draw();
                $('#datatable_Comisiones').dataTable().fnAddData(response);
            } else {
                $('#datatable_Comisiones').DataTable().clear().draw();
                ShowInfoBox("Información", "La Organización no cuenta con Comisiones...");
            }
        },
        error: function () {
            StopLoading();
            $('#datatable_Comisiones').DataTable().clear().draw();
        },
        complete: function () {
            StopLoading();
        }
    });
}

function EliminarComision(IdOrg, IdComision) {
    $.SmartMessageBox({
        title: "<i class='fa fa-warning fw'></i>  Atención...!!",
        content: "Esta acción no se puede deshacer, ¿Desea continuar?",
        buttons: '[No][Yes]'
    }, function (ButtonPressed) {
        var formData = new FormData();
        formData.append("CodigoOrganizacion", IdOrg);
        formData.append("CodigoComision", IdComision);

        if (ButtonPressed === "Yes") {

            $.ajax({
                cache: false,
                type: "POST",
                url: '../Mantenimiento/EliminarComision',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () { StarLoading(); },
                success: function (response) {
                    if (response.TipoError === "0") {
                        ListarComisiones();
                        ShowInfoBox("Información", "Datos Guardado con éxito!");                                                
                    } else {
                        ShowWarningBox("Atención", "Su proceso no pudo ser completado...");
                    }
                },
                error: function () {
                    StopLoading();
                    ShowWarningBox("Error", "No se pudo completar la operación, por favor consultar con Sistemas");
                },
                complete: function () {
                    StopLoading();
                    $('#btnBuscar').trigger("click");
                }
            });


        }
    });
}
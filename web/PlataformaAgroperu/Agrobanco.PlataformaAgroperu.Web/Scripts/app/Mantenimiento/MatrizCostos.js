﻿var $commentForm = $("#form-MatrizCostos").validate({
    // Rules for form validation
    rules: {
        MC_PorcentajeFinanciamiento: {
            required: true,
            number: true
        },
        MC_AsistenciaTecnicaPorcentaje: {
            required: true,
            number: true,
            max: 3,
            min: 0
        },
        MC_InicioCampana: {
            required: true
        },
        MC_FinCampana: {
            required: true
        },
        MC_PeriodoFenologico: {
            required: true,
            digits: true
        },
        MC_PeriodoPostCosecha: {
            required: true,
            digits: true
        },
        MC_NumeroPagosAT: {
            required: true,
            digits: true,
            min: 0
        },
        MC_NumeroCuotas: {
            required: true,
            digits: true,
            min: 0
        },
        MC_DiasGracia: {
            required: true,
            digits: true,
            min: 0
        },
        //inputs de montos
        MC_Rendimiento: {
            required: true,
            Montos: true
        },
        MC_PrecioChacra: {
            required: true,
            Montos: true
        },
        MC_CostoProduccion: {
            required: true,
            Montos: true
        },
        MC_MontoFinanciamiento: {
            required: true,
            Montos: true
        },
        MC_AsistenciaTecnica: {
            required: true,
            Montos: true
        }

    },

    // Messages for form validation
    messages: {
        MC_PorcentajeFinanciamiento: {
            required: "Este dato es obligatorio",
            max: 'Porcentaje de Finacimiento supera el máximo permitido para el Sector',
        },
        MC_AsistenciaTecnicaPorcentaje: {
            required: "Este dato es obligatorio",
            max: 'Asistencia técnica no debe superar el 3%'
        },
        MC_PeriodoFenologico: {
            required: "Este dato es obligatorio",
            digits: "Solo se permite números enteros"
        },
        MC_PeriodoPostCosecha: {
            required: "Este dato es obligatorio",
            digits: "Solo se permite números enteros"
        },
        MC_PrecioChacra: {
            required: "Este dato es obligatorio"
        },
        MC_CostoProduccion: {
            required: "Este dato es obligatorio"
        },
        MC_MontoFinanciamiento: {
            required: "Este dato es obligatorio"
        },
        MC_AsistenciaTecnica: {
            required: "Este dato es obligatorio"
        }
    },
    // Do not change code below
    errorPlacement: function (error, element) {
        error.insertAfter(element.parent());
    }
});

const SectorAgricola = "1",
    SectorPecuario = "2",
    SectorForestal = "3";

// Evento de botones
$("#btnNuevoCosto").click(function () {
    //  i:Input, s:Select, 
    //key: campos que no se pueden modificar

    $('#BtnGuardarDatos').show();
    $('.frm-MC-i-key').prop("readonly", false);
    $('.frm-MC-s-key').prop("disabled", false);
    $('.frm-MC-i').prop("readonly", false);
    $('.frm-MC-s').prop("disabled", false);

    $("#div-TablaCostos").hide('slow');
    $("#div-DatosCostos").show('slow');
    
    $('.select-Siembra').trigger('change');
    $('.select-Cosecha').trigger('change');

    CntrlPorcentajeAsistenciaTecnica();

});
$('#btnBuscar').click(function () {
    ListarCostos($('#cboSector').val(), $('#cboPrograma').val(), $('#chkHistorico').is(':checked'));
});
$('#btnDescargar').click(function () {
    window.location.href = '../Mantenimiento/DescargarMatrizCostos';
});
$("#BtnCancelarDatosCostos").click(function () {
    $('.frm-MC-i-key').val('');
    $('.frm-MC-s-key').val('');
    $('.frm-MC-i').val('');
    $('.frm-MC-s').val('');

    $("#MC_Codigo").text('');
    $("#MC_Codigo_hd").val('0');
    $("#MC_CodigoCampana").val('0');
    $("#frmCboProvincia").find('option').remove();
    $("#frmCboDistrito").find('option').remove();

    $('#div-PagosAT').empty();
    $('#div-Desembolsos').empty();
    $('#div-InfoVisitas').empty();

    $("#div-TablaCostos").show('slow');
    $("#div-DatosCostos").hide('slow');
    //$commentForm.resetform();
});
$('.select-Siembra').on('change', function () {
    var InicioSiembra = parseInt($('#MC_InicioSiembra').val());
    var FinSiembra = parseInt($('#MC_FinSiembra').val());

    if (InicioSiembra === NaN || FinSiembra === NaN) {
        $("#MesesSiembra li").each(function () { // Modificar Etiquetas dentro de un elemento
            $(this).removeClass('active');
            $("a", this).removeClass('bg-color-blue');
        });
        return;
    } else {

        if (InicioSiembra <= FinSiembra) {            
            $("#MesesSiembra li").each(function () {
                if (parseInt($(this).attr('id')) >= InicioSiembra && parseInt($(this).attr('id')) <= FinSiembra) {
                    $(this).addClass('active');
                    $("a", this).addClass('bg-color-green');
                } else {
                    $(this).removeClass('active');
                    $("a", this).removeClass('bg-color-green');
                }
            });

        } else {
            $("#MesesSiembra li").each(function () {
                if (parseInt($(this).attr('id')) >= InicioSiembra || parseInt($(this).attr('id')) <= FinSiembra) {
                    $(this).addClass('active');
                    $("a", this).addClass('bg-color-green');
                } else {
                    $(this).removeClass('active');
                    $("a", this).removeClass('bg-color-green');
                }
            });
        }
    }    
});
$('.select-Cosecha').on('change', function () {
    var InicioCosecha = parseInt($('#MC_InicioCosecha').val());
    var FinCosecha = parseInt($('#MC_FinCosecha').val());

    if (InicioCosecha === NaN || FinCosecha === NaN) {
        $("#MesesCosecha li").each(function () {
            $(this).removeClass('active');
            $("a", this).removeClass('bg-color-red');
        });
        return;
    } else {

        if (InicioCosecha <= FinCosecha) {
            $("#MesesCosecha li").each(function () {
                if (parseInt($(this).attr('id')) >= InicioCosecha && parseInt($(this).attr('id')) <= FinCosecha) {
                    $(this).addClass('active');
                    $("a", this).addClass('bg-color-red');
                } else {
                    $(this).removeClass('active');
                    $("a", this).removeClass('bg-color-red');
                }
            });

        } else {
            $("#MesesCosecha li").each(function () {
                if (parseInt($(this).attr('id')) >= InicioCosecha || parseInt($(this).attr('id')) <= FinCosecha) {
                    $(this).addClass('active');
                    $("a", this).addClass('bg-color-red');
                } else {
                    $(this).removeClass('active');
                    $("a", this).removeClass('bg-color-red');
                }
            });
        }
    }
});
$("#MC_Producto").on('input', function () {
    var texto = $(this).val();
    var CodigoCultivo = $('#list').find('option[value="' + texto.trim() + '"]').first().text();
    
    if (CodigoCultivo.length === 6) {
        $('#MC_CodigoProducto').val(CodigoCultivo);
        var TipoProd = $('#list').find('option[value="' + texto.trim() + '"]').data().flag;
        $('#MC_TipoProducto').val(TipoProd);

        CntrlNumeroDesembolsos();
    } else {
        $('#MC_CodigoProducto').val('');
        $('#MC_TipoProducto').val('');
    }
})
$("#MC_NumeroDesembolsos").change(function () {
    var NumDesembosos = $(this).val();
    $('#div-Desembolsos').empty();
    for (var i = 1; i <= NumDesembosos; i++) {

        if ($('#MC_Sector').val() == '2') { // Sector Pecuario
            $('#div-Desembolsos').append('' +
                '<div class="">' +
                '<label class="label col col-4"> Desembolso N° <strong>' + i + '</strong></label>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append">%</i>' +
                '<input class="input-desembolso-porcentaje" type="number" step=".01" placeholder="% Desembolso">' +
                '</label>' +
                '</section>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append fa fa-calendar"></i>' +
                '<input id="' + i + '" class="input-desembolso-mes" type="number" min="1" placeholder="Mes Desembolso">' +
                '</label>' +
                '</section>' +
                '</div>'
            );
        } else {
            $('#div-Desembolsos').append('' +
                '<div class="">' +
                '<label class="label col col-8"> Desembolso N° <strong>' + i + '</strong></label>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append">%</i>' +
                '<input class="input-desembolso-porcentaje" type="number" step=".01" placeholder="% Desembolso">' +
                '</label>' +
                '</section>' +
                '</div>'
            );
        }
    }

    CntrlSlidersDesembolsos();
});
$('#div-Desembolsos').on('input', '.input-desembolso-mes', function () {
    CntrlSlidersDesembolsos();
});
$('#MC_NumeroPagosAT').on('input', function () {

    $('#div-PagosAT').empty();
    if ($(this).val() > 15) {
        return;
    }

    for (var i = 1; i <= $(this).val(); i++) {

        $('#div-PagosAT').append('' +
            '<div class="">' +
            '<label class="label col col-4"> Pago N° <strong>' + i + '</strong></label>' +
            '<section class="col col-4">' +
            '<label class="input">' +
            '<i class="icon-append">%</i>' +
            '<input class="input-pagoAT-porcentaje" type="number" step=".01" placeholder="% Pago">' +
            '</label>' +
            '</section>' +
            '<section class="col col-4">' +
            '<label class="input">' +
            '<i class="icon-append fa fa-calendar"></i>' +
            '<input class="input-pagoAT-dias" type="number" min="0" placeholder="Días Pago">' +
            '</label>' +
            '</section>' +
            '</div>'
        );
    }
});
$('#MC_NumeroInfoVisitas').on('input', function () {

    $('#div-InfoVisitas').empty();

    if ($(this).val() > 15) {
        return;
    }

    for (var i = 1; i <= $(this).val(); i++) {

        $('#div-InfoVisitas').append('' +
            '<div class="">' +
            '<label class="label col col-8"> Informe N° <strong>' + i + '</strong></label>' +
            '<section class="col col-4">' +
            '<label class="input">' +
            '<i class="icon-append fa fa-calendar"></i>' +
            '<input class="input-InfoVisita-mes" type="number" min="0" placeholder="Mes de Informe">' +
            '</label>' +
            '</section>' +
            '</div>'
        );
    }
});
$('#MC_TipoCuota').on('change', function () {    
    CntrlTipoCuota();
});

//Eventos con Peticiones
$("#frmCboDepartamento").on('change', function (e) {
    var CboProvincia = $("#frmCboProvincia");
    var dataRequest = {
        CodDepartamento: this.value
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarProvinciaPorDep',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            CboProvincia.find('option').remove();
            CboProvincia.prepend($('<option></option>').html('Cargando Provincias...'));
        },
        success: function (response) {
            CboProvincia.find('option').remove();
            if (response.length > 0) {
                CboProvincia.append($('<option>', { value: "", text: "[Seleccione]" }));
                $.each(response, function (key, value) {
                    CboProvincia.append($('<option>', { value: value.code, text: value.value }));
                });
                CboProvincia.append($('<option>', { value: dataRequest.CodDepartamento + "00", text: "<< Todos >>" }));
            }
            else {
                ShowInfoBox("Información", "No se encontraron Provincias para el Ubigeo");
            }



        },
        error: function () {
            //StopLoading();
            CboProvincia.find('option').remove();
            CboProvincia.prepend($('<option></option>').html('Error al cargar Provincias'));
        },
        complete: function () {
            //StopLoading();
        }
    });
});
$("#frmCboProvincia").on('change', function (e) {
    var CboDistrito = $("#frmCboDistrito");
    var dataRequest = {
        CodProvincia: this.value
    };


    if (this.value.substr(2, 2) === '00') {
        CboDistrito.find('option').remove();
        CboDistrito.append($('<option>', { value: dataRequest.CodProvincia + "00", text: "<< Todos >>" }));
        return;
    }

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarDistritoPorProv',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            CboDistrito.find('option').remove();
            CboDistrito.prepend($('<option></option>').html('Cargando Distritos...'));
        },
        success: function (response) {
            CboDistrito.find('option').remove();
            if (response.length > 0) {
                CboDistrito.append($('<option>', { value: "", text: "[Seleccione]" }));
                $.each(response, function (key, value) {
                    CboDistrito.append($('<option>', { value: value.code, text: value.value }));
                });
                CboDistrito.append($('<option>', { value: dataRequest.CodProvincia + "00", text: "<< Todos >>" }));
            }
            else {
                ShowInfoBox("Información", "No se encontraron Distritos para el Ubigeo");
            }
        },
        error: function () {
            //StopLoading();
            CboDistrito.find('option').remove();
            CboDistrito.prepend($('<option></option>').html('Error al cargar Distritos'));
        },
        complete: function () {
            //StopLoading();
        }
    });
});
$('.Sector-Programa').on('change', function (e) {

    if ($('#MC_Sector').val() != "" && $('#MC_Sector').val() != null && $('#MC_Programa').val() != "" && $('#MC_Programa').val() != null) {
        var dataRequest = {
            CodigoSector: $("#MC_Sector").val(),
            CodigoPrograma: $('#MC_Programa').val()
        };

        $.ajax({
            cache: false,
            type: "POST",
            url: '../Parametricas/ListarProductosPorSectorPrograma',
            data: JSON.stringify(dataRequest),
            contentType: 'application/json',
            processData: false,
            beforeSend: function () {
                $("#MC_Producto").val('');
                $("#MC_CodigoProducto").val('');
                $("#list").find('option').remove();
                $('#list').append($('<option>', { value: '', text: 'Cargando...' }));
            },
            success: function (response) {
                if (response.length > 0) {
                    $("#list").find('option').remove();
                    $.each(response, function (key, value) {
                        //$('#list').append($('<option>', { value: value.value, text: value.code }));
                        $('#list').append('<option value="' + value.value + '" data-flag="' + value.flag + '">' + value.code + '</option>');
                    });
                }
            },
            error: function () {
                $("#list").find('option').remove();
                $('#list').append($('<option>', { value: '', text: 'No se pudo obtener Productos' }));
            },
            complete: function () {
                //StopLoading();
            }
        });
    }
    CntrlNivelTecnologico();
    CntrlPorcentajeAsistenciaTecnica();
    CntrlNumeroDesembolsos();
    cntrlTipoProducto();
    $("#MC_NumeroDesembolsos").trigger('change');
    $('#MC_TipoProducto').val('');
});

//Calculos en formulario
$(".MC-calc1").on('input', function () {
    var calc = $("#MC_MontoFinanciamiento").val() / $("#MC_CostoProduccion").val() * 100;
    $("#MC_PorcentajeFinanciamiento").val(calc.toFixed(2));
});
$('.MC-calculo-plazo').on('input', function () {
    var calcPlazo = parseInt($("#MC_PeriodoFenologico").val()) + parseInt($("#MC_PeriodoPostCosecha").val());
    $('#MC_Plazo').val(calcPlazo);
});

// Funciones control de elementos vista
function cntrlTipoProducto() { // Pendiente preguntar por el valor en el cnofc
    if ($('#MC_Sector').val() == "1") { //Sector Agricola
        $('#div-MC_TipoProducto').show();
    } else {
        //$('#MC_NivelTecnologico').val('1');
        $('#div-MC_TipoProducto').hide();
    }
}
function CntrlPorcentajeAsistenciaTecnica() {
    if ($('#MC_Sector').val() == "3" && $('#MC_Programa').val() == "0001" || //Forestal Nacional
        $('#MC_Sector').val() == "1" && $('#MC_Programa').val() == "0002") { //Agricola Madre de Dios
        $('#MC_AsistenciaTecnicaPorcentaje').val('0');
        $('#div-MC_AsistenciaTecnicaPorcentaje').hide();
    } else {
        $('#div-MC_AsistenciaTecnicaPorcentaje').show();
    }
}
function CntrlNivelTecnologico() {
    if ($('#MC_Sector').val() == "2") { //Agricola Pecuario
        $('#div-MC_NivelTecnologico').show();
    } else {
        $('#MC_NivelTecnologico').val('1');
        $('#div-MC_NivelTecnologico').hide();
    }
}
function CntrlNumeroDesembolsos() {
    var Sect = $('#MC_Sector').val();
    var Prog = $('#MC_Programa').val();
    var TipP = $('#MC_TipoProducto').val();
    $('#MC_NumeroDesembolsos').find('option').remove();
    if (Sect == '1' && Prog == '0002' && TipP == '0004') { // Agricola & Madre Dios & Permanente
        //$("#MC_NumeroDesembolsos").append($('<option>', { value: "1", text: "01" }));
        //$("#MC_NumeroDesembolsos").append($('<option>', { value: "2", text: "02" }));
        $("#MC_NumeroDesembolsos").append($('<option>', { value: "3", text: "03" }));
    }
    else if (Sect == '1' && Prog == '0002' && $('#MC_CodigoProducto').val() == "010051" ) { // Agricola & Madre Dios & YUCA_010051        
        $("#MC_NumeroDesembolsos").append($('<option>', { value: "3", text: "03" }));
    }
    else {        
        $("#MC_NumeroDesembolsos").append($('<option>', { value: "2", text: "02" }));
    }
    $("#MC_NumeroDesembolsos").trigger('change');
}
function CntrlSlidersDesembolsos() {    
    var slidersArray = [];
    var diasInicio = [];
    var diasFin = [];
    var NumDesembosos = $('#MC_NumeroDesembolsos').val();    
    // Lógica de días de Desembolso
    switch ($('#MC_Sector').val()) {
        case '1': // Sector Agricola
            if ($('#MC_Programa').val() == "0002" && $('#MC_TipoProducto').val() == "0004" || // Madre de Dios y Permanente
                $('#MC_Programa').val() == "0002" && $('#MC_CodigoProducto').val() == "010051") { // Nacional, YUCA_010051
                diasInicio[1] = 0;
                diasFin[1] = 30;
                diasInicio[2] = 60; //121;
                diasFin[2] = 120; // 211;
                diasInicio[3] = 180; // 241;
                diasFin[3] = 270; // 331;

            } else if ($('#MC_Programa').val() == "0002" && $('#MC_TipoProducto').val() == "0001") { // Madre de Dios - Transitorio
                diasInicio[1] = 0;
                diasFin[1] = 29;
                diasInicio[2] = 30;
                diasFin[2] = 90;

            } else if ($('#MC_Programa').val() == "0001" && $('#MC_TipoProducto').val() == "0001") { // Nacional - Transitorio
                diasInicio[1] = 0;
                diasFin[1] = 29;
                diasInicio[2] = 30;
                diasFin[2] = 90;

            } else if ($('#MC_Programa').val() == "0001" && $('#MC_TipoProducto').val() == "0004") { // Nacional - Permanente - Semi-Permanente
                diasInicio[1] = 0;
                diasFin[1] = 30;
                diasInicio[2] = 60;
                diasFin[2] = 150;

            } else {
                diasInicio[1] = 0;
                diasFin[1] = 30;
                diasInicio[2] = 31;
                diasFin[2] = 121;
            }
            break;
        case '2': // Sector Pecuario
            var MesDesembolsosValidos = true;
            var j = 1;
            var mesD = [];
            $('.input-desembolso-mes').each(function (i, obj) {
                var mes = $(this).val();                
                if (isNaN(mes) || mes == '' || mes < 0 || (mes - Math.floor(mes)) != 0) {
                    MesDesembolsosValidos = false
                    return;
                }
                mesD[j] = parseInt(mes);
                j++;
            });
            if (MesDesembolsosValidos) {
                diasInicio[1] = 0;
                diasFin[1] = mesD[1] * 30;
                diasInicio[2] = mesD[2] * 30
                diasFin[2] = diasInicio[2] + 60;

            } else {
                diasInicio[1] = 0;
                diasFin[1] = 0;
                diasInicio[2] = 0;
                diasFin[2] = 0;
            }
            break;
        case '3': // Sector Forestal
            diasInicio[1] = 0;
            diasFin[1] = 30;
            diasInicio[2] = 31;
            diasFin[2] = 211;
            break;
        default:
    }

    // Instancia Sliders
    
    $('#div-slidersDesembolsos').empty();
    for (var i = 1; i <= NumDesembosos; i++) {
        $('#div-slidersDesembolsos').append('' +
            '<div class="col-sm-12 col-md-12">' +
            '<div class= "form-group" >' +
            '<input id="range-slider-' + i + '" type="text" name="range_' + i + '" value="">' +
            '</div>' +
            '</div >'
        );
        var nombreSlider = "#range-slider-" + i;
        $(nombreSlider).ionRangeSlider({
            type: "double",
            grid: i == NumDesembosos,
            min: diasInicio[1],
            max: diasFin[NumDesembosos],
            from: diasInicio[i],
            to: diasFin[i],
            from_fixed: true,
            to_fixed: true,
            hide_min_max: true,
            prefix: "día: "
        });
        slidersArray[i] = $(nombreSlider).data("ionRangeSlider");
    }
}
function CntrlTipoCuota() {
    if ($('#MC_TipoCuota').val() == "9") { // Vencimiento
        $('#MC_NumeroCuotas').val("0");
        $('#MC_DiasGracia').val("0");
        $('.div-MC_DatosCuotas').hide();
    }
    else {
        $('.div-MC_DatosCuotas').show();
    }
}
// Guardar datos
$('#BtnGuardarDatos').click(function () {

    var pCodigo = $("#MC_Codigo_hd").val();
    var pSector = $('#MC_Sector').val();
    var pPrograma = $('#MC_Programa').val();
    var pNivelTecnologico = $('#MC_NivelTecnologico').val();
    var pCodigoProducto = $('#MC_CodigoProducto').val();
    var pDescripcionProducto = $('#MC_DescripcionProducto').val();
    var pTipoProducto = $('#MC_TipoProducto').val();
    var pPriorizacion = $('#MC_Priorizacion').val();
    var pUnidadFinanciamiento = $('#MC_UnidadFinanciamiento').val();
    var pUbigeo = $('#frmCboDistrito').val();

    var pRendimiento = $('#MC_Rendimiento').val();
    var pUnidadRendimiento = $('#MC_UnidadRendimiento').val();
    var pPrecioVentaChacra = $('#MC_PrecioChacra').val();
    var pUnidadPrecioVenta = $('#MC_UnidadPrecio').val();
    var pCostoProduccion = $('#MC_CostoProduccion').val();
    var pUnidadCosto = $('#MC_UnidadCosto').val();
    var pPeriodoFenologico = $('#MC_PeriodoFenologico').val();
    var pPeriodoPostCosecha = $('#MC_PeriodoPostCosecha').val();
    var pMesInicioCampana = $('#MC_InicioCampana').val();
    var pMesFinCampana = $('#MC_FinCampana').val();
    var pCampana = $('#MC_CodigoCampana').val();
    var pMesInicioSiembra = $('#MC_InicioSiembra').val();
    var pMesFinSiembra = $('#MC_FinSiembra').val();
    var pMesInicioCosecha = $('#MC_InicioCosecha').val();
    var pMesFinCosecha = $('#MC_FinCosecha').val();

    var pMontoFinanciar = $('#MC_MontoFinanciamiento').val();
    var pPorcentajeFinanciar = $('#MC_PorcentajeFinanciamiento').val();
    var pAsistenciaTecnica = $('#MC_AsistenciaTecnica').val();
    var pAsistenciaTecnicaPorcentaje = $('#MC_AsistenciaTecnicaPorcentaje').val();    
    var pPlazo = $('#MC_Plazo').val();
    var pTipoCuota = $('#MC_TipoCuota').val();
    var pNumeroCuotas = $('#MC_NumeroCuotas').val();
    var pDiasGracia = $('#MC_DiasGracia').val();

    var pNumeroDesembolsos = $('#MC_NumeroDesembolsos').val();
    var pNumeroPagosAT = $('#MC_NumeroPagosAT').val();
    var pNumeroInfoVisitas = $('#MC_NumeroInfoVisitas').val();
    
    var PorcentajesDesembolso = "|",
        MesDesembolso = "|",
        MesDesembolsoFin = "|",
        PorcentajesAT = "|",
        DiasPagoAT = "|",
        MesesInformeVisita = "|";

    
    if ($("#form-MatrizCostos")[0].checkValidity() === false) {
        //$("#hidSubmit").trigger('click');
        $commentForm.form();
        return;
    }

    if ($('#MC_CodigoProducto').val() === "") {
        ShowWarningBox("Atención", "Ingrese un Cultivo válido...");
        return;
    }
    if ($('#MC_NivelTecnologico').val() === "" || $('#MC_NivelTecnologico').val() == null) {
        ShowWarningBox("Atención", "Seleccione un Nivel Tecnológico...");
        return;
    }

    // Validaciones de Cálculos
    var TopeFinanciamiento =
        $('#MC_Sector').val() == '1' ? 70 :
        $('#MC_Sector').val() == '2' ? 80 :
        $('#MC_Sector').val() == '3' ? 70 : -1;

    if ($("#MC_PorcentajeFinanciamiento").val() > TopeFinanciamiento) {
        var MensajeFinanciamiento =
            $('#MC_Sector').val() == SectorAgricola ? 'Para el sector Agricola el Porcentaje de Financiamiento es de máximo 70%, verifique' :
            $('#MC_Sector').val() == SectorPecuario ? 'Para el sector Pecuario el Porcentaje de Financiamiento es de máximo 80%, verifique' :
            $('#MC_Sector').val() == SectorForestal ? 'Para el sector Forestal el Porcentaje de Financiamiento es de máximo 70%, verifique' :
                                                      'Ingrese sector para poder validar el porcentaje de Financiamiento';

            
        ShowWarningBox("Atención", MensajeFinanciamiento);
        return;
    }
    var TopeAsitenciaTecnica = $('#MC_AsistenciaTecnica').val() / $('#MC_MontoFinanciamiento').val() * 100;
    var MontoAsist = parseFloat(TopeAsitenciaTecnica).toFixed(2);
    var TopePorcentaje = parseFloat($('#MC_AsistenciaTecnicaPorcentaje').val()).toFixed(2);
    console.log(MontoAsist, TopePorcentaje);

    if (MontoAsist > TopePorcentaje && $('#MC_AsistenciaTecnicaPorcentaje').val() != 0) {
        ShowWarningBox("Atención", "Monto financiamiento supera el pocentaje de Asistencia Técnica");
        return;
    }
    
    // Validación de fechas de Campaña
    var pFechaInicioCampana = 0, pFechaFinCampana = 0;
    var partesFecCampana = pMesInicioCampana.split('/');
    var anioCampana = partesFecCampana[2].toString();
    var mesCampana = partesFecCampana[1];
    var diaCampana = partesFecCampana[0];
    var dt = new Date(anioCampana, mesCampana - 1, diaCampana);
    if (dt == null || anioCampana.length != 4) {
        ShowWarningBox('Observación', 'Fecha de Inicio de Campaña inválida. Verifique');
        return;
    } else {
        pFechaInicioCampana = parseInt(anioCampana) * 10000 + parseInt(mesCampana) * 100 + parseInt(diaCampana)
    }

    partesFecCampana = pMesFinCampana.split('/');
    
    if (partesFecCampana.length < 3) {
        ShowWarningBox('Observación', 'Fecha de Inicio de Campaña inválida. Verifique');
        return;
    }
    anioCampana = partesFecCampana[2].toString();
    mesCampana = partesFecCampana[1];
    diaCampana = partesFecCampana[0];
    var dt = new Date(anioCampana, mesCampana - 1, diaCampana);
    if (dt == null || anioCampana.length != 4) {
        ShowWarningBox('Observación', 'Fecha de Inicio de Campaña inválida. Verifique');
        return;
    } else {
        pFechaFinCampana = parseInt(anioCampana) * 10000 + parseInt(mesCampana) * 100 + parseInt(diaCampana)
    }

    // Validaciones de Desembolsos
    var nPorcentDesemb = 0;
    $('.input-desembolso-porcentaje').each(function (i, obj) {
        PorcentajesDesembolso = PorcentajesDesembolso + $(this).val() + "|";
        var total = $(this).val();
        total = (total == null || total == undefined || total == "") ? 0 : total;
        nPorcentDesemb = nPorcentDesemb + parseFloat(total);
    });
    if (nPorcentDesemb != 100) {
        ShowWarningBox('Observación', 'Porcentajes de desembolsos deben sumar 100. Verifique');
        return;
    }

    if (pSector == '2') { // Pecuario
        var MesDesembolsosInvalidos = false;
        $('.input-desembolso-mes').each(function (i, obj) {
            var mes = $(this).val();
            if (isNaN(mes) || mes == '' || mes < 0 || (mes - Math.floor(mes)) != 0) {
                MesDesembolsosInvalidos = true
                return;
            }
            MesDesembolso = MesDesembolso + $(this).val() + "|";
        });
        if (MesDesembolsosInvalidos) {
            ShowWarningBox('Observación', 'Los meses de desembolso deben ser números enteros, verifique.');
            return;
        }
        MesDesembolsoFin = MesDesembolsoFin + "1|2|3|"
    } else {
        MesDesembolso = MesDesembolso + "0|0|0|";
        MesDesembolsoFin = MesDesembolsoFin + "0|0|0|"
    }


    //Validaciones de Pago Asistente tecnico
    var nPorcentAT = 0;
    $('.input-pagoAT-porcentaje').each(function (i, obj) {
        PorcentajesAT = PorcentajesAT + $(this).val() + "|";
        var total = $(this).val();
        total = (total == null || total == undefined || total == "") ? 0 : total;
        nPorcentAT = nPorcentAT + parseFloat(total);
    });
    if (nPorcentAT != 100) {
        ShowWarningBox('Observación', 'Porcentajes del Pago al Asistente técnico deben sumar 100. Verifique');
        return;
    }
    var PagosInvalidos = false;
    var DiasAnterior = 0;
    $('.input-pagoAT-dias').each(function (i, obj) {
        var dias = $(this).val();
        console.log("plazo: " + dias);
        console.log("anterior: " + DiasAnterior);

        if (isNaN(dias) || dias == '' || dias < 0 || (dias - Math.floor(dias)) != 0) {
            PagosInvalidos = true;
            return;
        }
        
        if (parseInt(dias) <= DiasAnterior) {
            PagosInvalidos = true;
            return;
        }
        DiasAnterior = parseInt(dias);
        DiasPagoAT = DiasPagoAT + $(this).val() + "|";
    });

    if (PagosInvalidos) {
        ShowWarningBox('Observación', 'Días de pago del Asistente Técnico deben ser números enteros y siempre mayor a los días de la cuota anterior. Verifique.');
        return;
    }

    // Validaciones de Meses informe de Visita
    var MesesInfoVisitaInvalidos = false;
    $('.input-InfoVisita-mes').each(function (i, obj) {
        var MesIV = $(this).val();
        if (isNaN(MesIV) || MesIV == '' || MesIV < 0 || (MesIV - Math.floor(MesIV)) != 0) {
            MesesInfoVisitaInvalidos = true;
            return;
        }
        MesesInformeVisita = MesesInformeVisita + $(this).val() + "|";
    });
    if (MesesInformeVisita.length <=2) { //valida ingreso de Meses de informe de visita
        ShowWarningBox('Observación', 'Ingrese meses de Informe de Vistas.');
        return;
    }
    if (MesesInfoVisitaInvalidos) {
        ShowWarningBox('Observación', 'Días de pago del Asistente Técnico deben ser números enteros, verifique.');
        return;
    }
    console.log("Descripcion: " + pDescripcionProducto);
    var MatrizCosto = {
        Codigo: pCodigo,
        Sector: pSector,
        Programa: pPrograma,
        NivelTecnologico: pSector == SectorPecuario ? pNivelTecnologico : "9", // Si es Pecuario se setea 9
        CodigoProducto: pCodigoProducto,
        DescripcionProducto: pDescripcionProducto == null || pDescripcionProducto == "" ? " " : pDescripcionProducto,
        TipoProducto: pTipoProducto,
        Priorizacion: pPriorizacion,
        UnidadFinanciamiento: pUnidadFinanciamiento,
        Ubigeo: pUbigeo,

        Rendimiento: pRendimiento,
        UnidadRendimiento: pUnidadRendimiento,
        PrecioVentaChacra: pPrecioVentaChacra,
        UnidadPrecioVenta: pUnidadPrecioVenta,
        CostoProduccion: pCostoProduccion,
        UnidadCosto: pUnidadCosto,
        PeriodoFenologico: pPeriodoFenologico,
        PeriodoPostCosecha: pPeriodoPostCosecha,
        FechaInicioCampana: pFechaInicioCampana,
        FechaFinCampana: pFechaFinCampana,
        Campana: pCampana,
        SiembraMesInicio: pMesInicioSiembra,
        SiembraMesFin: pMesFinSiembra,
        CosechaMesInicio: pMesInicioCosecha,
        CosechaMesFin: pMesFinCosecha,

        MontoFinanciar: pMontoFinanciar,
        PorcentajeFinanciar: pPorcentajeFinanciar,
        AsistenciaTecnica: pAsistenciaTecnica,
        AsistenciaTecnicaPorcentaje: pAsistenciaTecnicaPorcentaje,        
        Plazo: pPlazo,
        TipoCronograma: pTipoCuota,
        MaximoCuotas: pNumeroCuotas,
        MaximoDiasGracia: pDiasGracia,
        CronogramasPermitidos: pTipoCuota, // Temporalmente

        NumeroDesembolsos: pNumeroDesembolsos,
        TramaDesembolsoPorcentajes: PorcentajesDesembolso,
        TramaDesembolsoMesInicio: MesDesembolso,
        TramaDesembolsoMesFin: MesDesembolsoFin,
        NumeroPagosAT: pNumeroPagosAT,
        TramaPagosATPorcentajes: PorcentajesAT,
        TramaPagosATDias: DiasPagoAT,
        NumeroInfoVisitas: pNumeroInfoVisitas,
        TramaInfoVisitaMeses: MesesInformeVisita
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Mantenimiento/RegistrarMatrizCostos',
        data: JSON.stringify(MatrizCosto),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            
            if (response.TipoError == "100") {
                ShowInfoBox("Atención", response.Message);
                $('#datatable_Costos').DataTable().clear().draw();
                ListarCostos();
                $('#BtnCancelarDatosCostos').trigger('click');
            } else if (response.TipoError == "0") {
                ShowInfoBox("Atención", response.Message);
            } else {
                ShowWarningBox("Atención", response.Message);
            }
        },
        error: function () {
            StopLoading();
            
        },
        complete: function () {
            StopLoading();
        }
    });
})
// Funciones
function ListarCostos(CodigoSector, CodigoPrograma, pHistorico) {
    var dataRequest = {
        Sector: CodigoSector,
        Programa: CodigoPrograma,
        Historico: pHistorico
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Mantenimiento/ListarCostos',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Costos').DataTable().clear().draw();
                $('#datatable_Costos').dataTable().fnAddData(response);
            } else {
                $('#datatable_Costos').DataTable().clear().draw();
                ShowInfoBox("Información", "No se encontraron Resultados");
            }

        },
        error: function () {
            StopLoading();
            ShowInfoBox("Información", "Hubo problemas al obtener los costos");
        },
        complete: function () {
            StopLoading();
        }

    })
};
function ListarDepartamentos() {
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarDepartamentos',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            //StarLoading();
        },
        success: function (response) {

            $("#frmCboDepartamento").find('option').remove();
            $("#frmCboDepartamento").append($('<option>', { value: "", text: "[Seleccione Deparatamento]" }));

            if (response.length > 0) {
                $.each(response, function (key, value) {
                    $("#frmCboDepartamento").append($('<option>', { value: value.code, text: value.value }));
                });

            } else {
                $('#datatable_Costos').DataTable().clear().draw();
                ShowInfoBox("Información", "No se encontraron Resultados");
            }

        },
        error: function () {
            //StopLoading();
            ShowInfoBox("Información", "Hubo problemas al obtener Ubigeos");
        },
        complete: function () {
            //StopLoading();
        }

    });
};
function Consultar(Codigo) {

    CntrlPorcentajeAsistenciaTecnica();
    CntrlNumeroDesembolsos();
    CntrlNivelTecnologico();
    cntrlTipoProducto();
    CntrlTipoCuota();
    $('.select-Siembra').trigger('change');
    $('.select-Cosecha').trigger('change');
    var dataRequest = {
        CodigoCosto: Codigo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Mantenimiento/ListarCostoDetalle',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            if (response != null) {
                ListarDetalleCosto(response);
            }
            else {
                console.log(response);
            }
        },
        error: function () {
            StopLoading();
            ShowInfoBox("Información", "No se pudo obtener el detalle de desembolsos y Pagos Asistentes técnicos");
        },
        complete: function () {
            StopLoading();
            $('#BtnGuardarDatos').hide();
            $('.frm-MC-i-key').prop("readonly", true);
            $('.frm-MC-s-key').prop("disabled", true);
            $('.frm-MC-i').prop("readonly", true);
            $('.frm-MC-s').prop("disabled", true);
            $("#div-TablaCostos").hide('slow');
            $("#div-DatosCostos").show('slow');
            CntrlSlidersDesembolsos();
        }
    });



}
function Editar(Codigo) {
    CntrlPorcentajeAsistenciaTecnica();
    CntrlNumeroDesembolsos();
    CntrlNivelTecnologico();
    cntrlTipoProducto();
    CntrlTipoCuota();
    $('.select-Siembra').trigger('change');
    $('.select-Cosecha').trigger('change');

    var dataRequest = {
        CodigoCosto: Codigo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Mantenimiento/ListarCostoDetalle',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
        },
        success: function (response) {
            if (response != null) {
                ListarDetalleCosto(response);
            }
            else {
                console.log(response);
            }
        },
        error: function () {
            StopLoading();
            ShowInfoBox("Información", "No se pudo obtener el detalle de desembolsos y Pagos Asistentes técnicos");
        },
        complete: function () {
            StopLoading();
            $('#BtnGuardarDatos').show();
            $('.frm-MC-i-key').prop("readonly", true);
            $('.frm-MC-s-key').prop("disabled", true);
            $('.frm-MC-i').prop("readonly", false);
            $('.frm-MC-s').prop("disabled", false);

            $("#div-TablaCostos").hide('slow');
            $("#div-DatosCostos").show('slow');
            CntrlSlidersDesembolsos();
        }
    });
}
function Eliminar(Codigo) {
    $.SmartMessageBox({
        title: "<i class='fa fa-warning'></i> Atención...!!",
        content: "Deshabilitar matriz con codigo: " + Codigo + ". Esta acción no se podrá deshacer, ¿Desea continuar ? ",
        buttons: '[No][Yes]'
    }, function (ButtonPressed) {

        if (ButtonPressed === "Yes") {
            var requestData = {
                CodigoCosto: Codigo
            }
            $.ajax({
                cache: false,
                type: "POST",
                url: '../Mantenimiento/DeshabilitarMatrizCostos',
                data: JSON.stringify(requestData),
                contentType: 'application/json',
                processData: false,
                beforeSend: function () {
                    StarLoading();
                },
                success: function (response) {

                    if (response.TipoError == "100") {
                        ShowInfoBox("Atención", response.Message);
                        //$('#datatable_Costos').DataTable().clear().draw();
                        ListarCostos($('#cboSector').val(), $('#cboPrograma').val(), $('#chkHistorico').is(':checked'));
                    } else if (response.TipoError == "0") {
                        ShowInfoBox("Atención", response.Message);
                    } else {
                        ShowWarningBox("Atención", response.Message);
                    }
                },
                error: function () {
                    StopLoading();

                },
                complete: function () {
                    StopLoading();
                }
            });
        }        
    });    
}
function ListarDetalleCosto(lista) {

    $('#div-PagosAT').empty();
    $('#div-Desembolsos').empty();
    $('#div-InfoVisitas').empty();

    var NumAT = 0, NumDesemb = 0, NumInfoVisita = 0;
    for (var i = 0; i <= lista.length - 1; i++) {
        if (lista[i].key == '1') { // Desembolsos
            if ($('#MC_Sector').val() == '2') { // Sector Pecuario
                $('#div-Desembolsos').append('' +
                    '<div class="">' +
                    '<label class="label col col-4"> Desembolso N° <strong>' + lista[i].code + '</strong></label>' +
                    '<section class="col col-4">' +
                    '<label class="input">' +
                    '<i class="icon-append">%</i>' +
                    '<input class="input-desembolso-porcentaje" type="number" step=".01" placeholder="% Desembolso" value="' + lista[i].flag + '">' +
                    '</label>' +
                    '</section>' +
                    '<section class="col col-4">' +
                    '<label class="input">' +
                    '<i class="icon-append fa fa-calendar"></i>' +
                    '<input class="input-desembolso-mes" type="number" placeholder="Mes Desembolso" value="' + lista[i].value + '">' +
                    '</label>' +
                    '</section>' +
                    '</div>'
                );
            } else {
                $('#div-Desembolsos').append('' +
                    '<div class="">' +
                    '<label class="label col col-8"> Desembolso N° <strong>' + lista[i].code + '</strong></label>' +
                    '<section class="col col-4">' +
                    '<label class="input">' +
                    '<i class="icon-append">%</i>' +
                    '<input class="input-desembolso-porcentaje" type="number" step=".01" placeholder="% Desembolso" value="' + lista[i].flag + '">' +
                    '</label>' +
                    '</section>' +
                    '</div>'
                );
            };

            NumDesemb = NumDesemb + 1;
        }

        if (lista[i].key == '2') { // PAgo Asistente Tecnico
            $('#div-PagosAT').append('' +
                '<div class="">' +
                '<label class="label col col-4"> Pago N° <strong>' + lista[i].code + '</strong></label>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append">%</i>' +
                '<input class="input-pagoAT-porcentaje" type="number" step=".01" placeholder="% Pago" value="' + lista[i].flag + '">' +
                '</label>' +
                '</section>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append fa fa-calendar"></i>' +
                '<input class="input-pagoAT-dias" type="number" min="1" placeholder="Días Pago AT" value="' + lista[i].value + '">' +
                '</label>' +
                '</section>' +
                '</div>'
            );
            NumAT = NumAT + 1;
        }

        if (lista[i].key == '3') { // Informe Visita
            $('#div-InfoVisitas').append('' +
                '<div class="">' +
                '<label class="label col col-8"> Informe N° <strong>' + lista[i].code + '</strong></label>' +
                '<section class="col col-4">' +
                '<label class="input">' +
                '<i class="icon-append fa fa-calendar"></i>' +
                '<input class="input-InfoVisita-mes" type="number" min="0" placeholder="Mes de Informe" value="' + lista[i].value + '">' +
                '</label>' +
                '</section>' +
                '</div>'
            );
            NumInfoVisita = NumInfoVisita + 1;
        }
    }

    $('#MC_NumeroPagosAT').val(NumAT);
    $("#MC_NumeroDesembolsos").val(NumDesemb);
    $('#MC_NumeroInfoVisitas').val(NumInfoVisita);
}
function ParametricasMartriz() {

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarParametricasMatrizCostos',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            //StarLoading();
        },
        success: function (response) {

            if (response.length > 0) {
                $.each(response, function (key, value) {
                    switch (value.key) {
                        case "TECN": $("#MC_NivelTecnologico").append($('<option>', { value: value.code, text: value.value })); break;
                        case "TCUO": $("#MC_TipoCuota").append($('<option>', { value: value.code, text: value.value })); break;
                        case "REND": $("#MC_UnidadRendimiento").append($('<option>', { value: value.code, text: value.value })); break;
                        case "UFIN": $('#MC_UnidadFinanciamiento').append($('<option>', { value: value.code, text: value.value })); break;
                        case "SECT":
                            $("#MC_Sector").append($('<option>', { value: value.code, text: value.value }));
                            $("#cboSector").append($('<option>', { value: value.code, text: value.value }));
                            break;
                        case "PROG":
                            $("#MC_Programa").append($('<option>', { value: value.code, text: value.value }));
                            $("#cboPrograma").append($('<option>', { value: value.code, text: value.value }));
                            break;
                    }
                });
            }

        },
        error: function () {
            //StopLoading();
            ShowInfoBox("Información", "No se pudo obtener listado parámetros");
        },
        complete: function () {
            //StopLoading();
        }
    })
}
﻿var responsiveHelper_datatable_Padrones = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};



$('#btnLimpiar').click(function () {
    $('#datatable_Padrones').DataTable().clear().draw();
});


$("#btnBuscarPadrones").click(function () {
    ConsultarPadronxAgencia();
});

function ConsultarPadronxAgencia() {
    var _CodAgencia = $("#CboAgencia").val();

    var requestData = {
        CodAgencia: _CodAgencia
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Padron/ConsultarPadronPorAgencia',
        data: JSON.stringify(requestData),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () { StarLoading(); },
        success: function (response) {
            if (response.length > 0) {
                $('#datatable_Padrones').DataTable().clear().draw();
                $('#datatable_Padrones').dataTable().fnAddData(response);
            } else {
                ShowInfoBox("Información", "La búsqueda no obtuvo ningun resultado, intente con otros filtros.");
            }
        },
        error: function () {            
            StopLoading();
            ShowErrorBox("Atención", "Ocurrió un error, si el error persiste reportarlo con Sistemas");
        },
        complete: function () {
            StopLoading();
        }
    });
}

function DescargarPadron(CodigoPadron, NombrePadron) {
    window.location.href = '../Padron/DescargarPadron?CodigoPadron=' + CodigoPadron + '&NombrePadron=' + NombrePadron;
}



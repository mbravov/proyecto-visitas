﻿var $chrt_border_color = "#abebc6";
var responsiveHelper_datatable_tabletools = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};
$('#datatable_tableErrors').dataTable({
    "data": [],
    "autoWidth": true,
    "preDrawCallback": function () {
        if (!responsiveHelper_datatable_tabletools) {
            responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tableErrors'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_tabletools.respond();
    },
    "bServerSide": false,
    "columns": [
        { "data": "row", "width": "5%" },
        { "data": "LetterColumn", "width": "5%" },
        { "data": "value", "width": "15%" },
        { "data": "msg", "width": "80%" }
    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }
});
function ListarJuntaRegantes() {
    //var dataRequest = {
    //    codOficinaRegional: 50
    //};
    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarParametricasGrupo',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            StarLoading();
            //combo.find('option').remove();
            //combo.prepend($('<option></option>').html('Cargando...'));
        },
        success: function (response) {
            ListarParametricasGrupo(response);
        },
        error: function () {
            StopLoading();
            //combo.find('option').remove();
            //combo.prepend($('<option></option>').html('No se pudo obtener lista'));
        },
        complete: function () {
            StopLoading();
            //if (($("#CodOficina").val() === '0') && ($("#cmbagencia").val() === '0')) {
            //    return;
            //}
            //$("#btnBuscar").trigger("click");
        }
    });
}
function ListarParametricasGrupo(Parametricas) {
    var cboJunta = $("#listOrganizaciones");
    
    $.each(Parametricas, function (key, value) {
        switch (value.key) {
            case "JUNT": cboJunta.append($('<option>', { value: value.value, text: value.code })); break;          
        }
    });

}
$("#ddlJuntaRegante").on('input', function () {

    var texto = $(this).val();
    var CodigoOrganizacion = $('#listOrganizaciones').find('option[value="' + texto.trim() + '"]').text();
    if (CodigoOrganizacion != '') {
        $('#hidCodigOrganizacion').val(CodigoOrganizacion);
        ListarComisiones(CodigoOrganizacion)

    }
});
function ListarComisiones(Codigo) {
    var CboComisionRegante = $("#ddlComisionRegante");
    var dataRequest = {
        CodJuntaRegante: Codigo
    };

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Parametricas/ListarComisionRegante',
        data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            CboComisionRegante.find('option').remove();
            CboComisionRegante.prepend($('<option></option>').html('Cargando Comisiones...'));
        },
        success: function (response) {
            CboComisionRegante.find('option').remove();
            $('.div-AvisoComision').hide("swing");

            if (response.length > 0) {
                CboComisionRegante.append($('<option>', { value: "", text: "[Seleccione Comision]" }));
                $.each(response, function (key, value) {
                    $('#ddlComisionRegante').append('<option value="' + value.code + '" data-flag="' + value.flag + '">' + value.value + '</option>');
                });
            }
            else {
                CboComisionRegante.append($('<option>', { value: "0", text: "Sin Comisión" }));   
            }

            
        },
        error: function () {
            StopLoading();
            CboComisionRegante.find('option').remove();
            CboComisionRegante.prepend($('<option></option>').html('Error al cargar Comisiones'));
        },
        complete: function () {
            //StopLoading();
        }
    });

};
$("#ddlComisionRegante").on('change', function (e) {
    var flag = $(this).find(':selected').data().flag;

    if (flag !== 0) {
        $('.div-AvisoComision').hide("fast");
        $('.div-AvisoComision').show("linear");
    } else {
        $('.div-AvisoComision').hide("swing");
    }

});
$('#btnCrearGrupo').click(function () {
    
    grabar();
});

function grabar() {
    if ($("#TxtNombreJunta").val() === "") {
        ShowWarningBox("Mensaje Sistemas", "Debe registrar un nombre para el Padrón");
        return;
    }
    if ($("#ddlJuntaRegante").val() === '') {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar una Organización del listado");
        return;
    }
    if ($("#ddlComisionRegante").val() === "" || $("#ddlComisionRegante").val() === null) {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar una comisión válida");
        return;
    }

    console.log($("#ddlComisionRegante").val());
   
    var cadena = $("#fileExcel").val();
    if (cadena === "") {
        ShowWarningBox("Mensaje Sistemas", "Debe seleccionar el archivo excel correspondiente al padrón");
        return;
    }

    //if (ExistePadron() != "") {
    //    ShowWarningBox("Mensaje Sistemas", "La junta comision se ingreso anteriormente");
    //    return;
    //}

    var ddlJuntaRegante = $("#hidCodigOrganizacion").val();
    var ddlComision = $("#ddlComisionRegante").val();
    var dataRequest = {
        CodJunta: ddlJuntaRegante,
        Codcomision: ddlComision
    };
    //$.ajax({
    //    cache: false,
    //    type: "POST",
    //    url: '../Padron/ExistePadron',
    //    data: JSON.stringify(dataRequest),
    //    contentType: 'application/json',
    //    processData: false,
    //    beforeSend: function () {
    //    },
    //    success: function (response) {
    //        if (response != "") {
    //            ShowWarningBox("Mensaje Sistemas", "El Padron " + response+" ha sido ingresado anteriormente " );
    //            return;
    //        } else {
    //            ValidacionExcel();
    //        }
    //    },
    //    error: function () {
    //        //combo.find('option').remove();
    //        //combo.prepend($('<option></option>').html('No se pudo obtener lista'));
    //    },
    //    complete: function () {           
    //        //if (($("#CodOficina").val() === '0') && ($("#cmbagencia").val() === '0')) {
    //        //    return;
    //        //}
    //        //$("#btnBuscar").trigger("click");
    //    }
    //});

    ValidacionExcel();
   
}
function ValidacionExcel() {
    var njunta = $("#TxtNombreJunta").val();
    var ddlJuntaRegante = $("#hidCodigOrganizacion").val();
    var ddlComision = $("#ddlComisionRegante").val();

    // Tener en cuenta los número de linea
    var cab = njunta + "|" + 
        ddlJuntaRegante + "|" + 
        ddlComision + "|" +
        "|" +  
        "|" +
        "|" +
        "|" +
        "|" +
        "01|" +
        "|" +
        "0|" +
        "|" +
        "0|" +
        "0|" +
        "0|" +
        "0|" +
        "|*|";

    var tiempo = new Date();
    var anho = '00' + tiempo.getFullYear();
    var time = anho.substring(anho.length - 4, 8);
    var mes = '00' + (tiempo.getMonth() + 1);
    time = time + mes.substring(mes.length - 2, 4);
    var dia = '00' + tiempo.getDate();
    time = time + dia.substring(dia.length - 2, 4);
    var hora = '00' + tiempo.getHours();
    time = time + hora.substring(hora.length - 2, 4);
    var minuto = '00' + tiempo.getMinutes();
    time = time + minuto.substring(minuto.length - 2, 4);
    var segundo = '00' + tiempo.getSeconds();
    time = time + segundo.substring(segundo.length - 2, 4);


    //var dataRequest = {
    //    pcabecera: cab
    //}
    var formData = new FormData();
    var totalFiles = document.getElementById("fileExcel").files.length;
    if (totalFiles === 1) {
        var file = document.getElementById("fileExcel").files[0];
        formData.append("fileExcel", file);
        formData.append("pcabecera", cab);
        formData.append("FechaCliente", time);


        $.ajax({

            type: "POST",
            url: '../Padron/GrabarPadron',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function () {
                StarLoading();
            },
            success: function (response) {

                if (response.code === '99') {
                    $("#divNuevoGrupo").hide("slow");
                    $("#divResultados").show("slow");
                    SetResultados(response);
                    ShowSuccessBox("Mensaje Sistemas", "Registro exitoso");

                } else if (response.code === '03') {
                    ShowWarningBox("Mensaje Sistemas", "Se encontraron observaciones, favor revisar el listado");
                    $('#datatable_tableErrors').DataTable().clear().draw();
                    $('#datatable_tableErrors').dataTable().fnAddData(response.listErrors);

                } else if (response.code === '01') {
                    $("#fileExcel").val('');
                    $("#fileExcelText").val('');
                    ShowWarningBox("Mensaje Sistema", "Por favor vuelva adjuntar el archivo");

                } else {
                    ShowWarningBox("Mensaje Sistemas", response.message);                    
                }

                $("#fileExcel").val('');
                $("#fileExcelText").val('');
            },
            error: function () {
                StopLoading();
                ShowWarningBox("Mensaje Sistemas", "Su proceso no pudo ser completado, favor volver a intentar");
            },
            complete: function () {
                StopLoading();
                //if (($("#CodOficina").val() === '0') && ($("#cmbagencia").val() === '0')) {
                //    return;
                //}
                //$("#btnBuscar").trigger("click");
            }
        });
    }
}
function SetResultados(resultado) {
    $("#aNombreGrupo").text(resultado.idReg + ' - ' + $("#TxtNombreJunta").val());
    $("#aTotalRegistros").text("Total Registros: " + resultado.totReg);
    //SetChartCalifSBS(resultado.lstCalificacionesSBS);
}

function SetChartCalifSBS(CalificacionesSBS) {
    if ($("#bar-chart-h").length) {
        //Display horizontal graph
        var d1_h = [], d2_h = [], d3_h = [], d4_h = [], d5_h = [], d6_h = [];

        $.each(CalificacionesSBS, function (key, value) {
            switch (value.key) {
                case "1": d1_h.push([value.value, 1]); break;
                case "2": d2_h.push([value.value, 2]); break;
                case "3": d3_h.push([value.value, 3]); break;
                case "4": d4_h.push([value.value, 4]); break;
                case "5": d5_h.push([value.value, 5]); break;
                case "6": d6_h.push([value.value, 6]); break;
            }
        });

        if (d1_h.length === 0) { d1_h.push([0, 1]); }
        if (d2_h.length === 0) { d2_h.push([0, 2]); }
        if (d3_h.length === 0) { d3_h.push([0, 3]); }
        if (d4_h.length === 0) { d4_h.push([0, 4]); }
        if (d5_h.length === 0) { d5_h.push([0, 5]); }
        if (d6_h.length === 0) { d5_h.push([0, 6]); }

        var ds_h = new Array();
        ds_h.push({
            data: d6_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });

        ds_h.push({
            data: d5_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });

        ds_h.push({
            data: d4_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });

        ds_h.push({
            data: d3_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });

        ds_h.push({
            data: d2_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });

        ds_h.push({
            data: d1_h,
            bars: {
                horizontal: true,
                show: true,
                barWidth: 0.5,
                order: 1
            }
        });


        // display graph
        $.plot($("#bar-chart-h"), ds_h, {
            colors: ["#808080", "#ff0000", "#ff6a00", "#ffd800", "#b6ff00", "#4cff00"],
            grid: {
                show: true,
                hoverable: true,
                clickable: true,
                tickColor: $chrt_border_color,
                borderWidth: 0,
                borderColor: $chrt_border_color
            },
            legend: true,
            tooltip: true,
            tooltipOpts: {
                content: "<span>%x</span> = <span>%y</span>",
                defaultTheme: false
            }
        });

    }
}

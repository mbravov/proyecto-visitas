﻿var backgroundColor = {
    Rojo: 'rgba(255, 99, 132, 0.2)',
    Azul: 'rgba(54, 162, 235, 0.5)',
    Amarillo: 'rgba(255, 206, 86, 0.2)',
    Verde: 'rgba(30, 181, 101, 0.5)',
    Morado: 'rgba(153, 102, 255, 0.2)',
    Naranja: 'rgba(255, 159, 64, 0.2)'
};
var borderColor = {
    Rojo: 'rgba(255,99,132,1)',
    Azul: 'rgba(54, 162, 235, 1)',
    Amarillo: 'rgba(255, 206, 86, 1)',
    Verde: 'rgba(30, 181, 101, 1)',
    Morado: 'rgba(153, 102, 255, 1)',
    Naranja: 'rgba(255, 159, 64, 1)'
};
var Colors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)', // 'rgba(255, 159, 64, 0.2)', broder: 'rgba(255, 159, 64, 1)'
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)', // 'rgba(75, 192, 192, 0.2)', border: 'rgba(75, 192, 192, 1)'
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

var AgrColors = {
    VerdeOscuro: 'rgb(39, 99, 76)',
    VerdeFuerte: 'rgba(2, 124, 98, .7)',
    VerdeFuerteLogo: 'rgba(2, 101, 77, .7)',
    VerdeFuerteLogoBorde: 'rgb(2, 101, 77)',
    VerdeClaro: 'rgba(126, 180, 80, .8)',
    VerdeClaroBorde: 'rgb(126, 180, 80)',
    VerdeClaro2: 'rgb(129, 197, 94)',
    VerdeClaroLogo: 'rgb(151, 189, 30)',
    NaranjaFuerte: 'rgba(238, 120, 51, .7)',
    NaranjaClaro: 'rgba(237, 152, 53, .8)',
    NaranjaClaroBorde: 'rgb(237, 152, 53)',
    NaranjaClaro2: 'rgb()'
}

var APF_Colors = [AgrColors.VerdeClaro, AgrColors.NaranjaClaro, AgrColors.VerdeFuerteLogo];
var APF_ColorsBorde = [AgrColors.VerdeClaroBorde, AgrColors.NaranjaClaroBorde, AgrColors.VerdeFuerteLogoBorde];
const AnchoBorde = 2;
//var progress = document.getElementById('animationProgress');
$(document).ready(function () {
    //pageSetUp();
    Chart.plugins.unregister(ChartDataLabels);    
    ObtenerReporteSolicitudes();
});

function ObtenerReporteSolicitudes() {

    $.ajax({
        cache: false,
        type: "POST",
        url: '../Home/ObtenerReporteSolocitudes',
        //data: JSON.stringify(dataRequest),
        contentType: 'application/json',
        processData: false,
        beforeSend: function () {
            //StarLoading();
        },
        success: function (response) {

            if (response == null) {
                ShowInfoBox("Atención", "No se pudo obtener los datos Estadísticos");
            } else {
                InitGraficos(response);
            }

        },
        error: function () {
            //StopLoading();
            ShowInfoBox("Información", "No se pudo obtener listado parámetros");
        },
        complete: function () {
            //StopLoading();
        }
    })
}

function InitGraficos(DataResponse) {
    var Det = DataResponse.SetDatosDetalle;
    var Tot = DataResponse.SetDatosTotal;

    var APF_Saldo = [Tot.Agricola.Desembolsado.Saldo, Tot.Pecuario.Desembolsado.Saldo, Tot.Forestal.Desembolsado.Saldo];
    var APF_Desemb = [Tot.Agricola.Desembolsado.MontoSolicitado, Tot.Pecuario.Desembolsado.MontoSolicitado, Tot.Forestal.Desembolsado.MontoSolicitado];
    var APF_Creditos = [Tot.Agricola.Desembolsado.NumeroSolicitudes, Tot.Pecuario.Desembolsado.NumeroSolicitudes, Tot.Forestal.Desembolsado.NumeroSolicitudes];

    var APAD_AgricolaDesemb = [Tot.Agricola.Admision.MontoSolicitado, Tot.Agricola.Propuesta.MontoSolicitado, Tot.Agricola.Aprobada.MontoSolicitado]; // Tot.Agricola.Desembolsado.MontoSolicitado];
    var APAD_PecuarioDesemb = [Tot.Pecuario.Admision.MontoSolicitado, Tot.Pecuario.Propuesta.MontoSolicitado, Tot.Pecuario.Aprobada.MontoSolicitado]; //, Tot.Pecuario.Desembolsado.MontoSolicitado];
    var APAD_ForestalDesemb = [Tot.Forestal.Admision.MontoSolicitado, Tot.Forestal.Propuesta.MontoSolicitado, Tot.Forestal.Aprobada.MontoSolicitado]; //, Tot.Forestal.Desembolsado.MontoSolicitado];

    $('#spanDesembolso').text("S/ " + number_format(Tot.Desembolsado.Total.MontoSolicitado, 0));
    $('#spanSaldo').html("S/ " + number_format(Tot.Desembolsado.Total.Saldo, 0));
    $('#spanCreditos').html('<i class="fa fa-user" data-rel="bootstrap-tooltip" title="Increased"></i>  ' + number_format(Tot.Desembolsado.Total.NumeroSolicitudes, 0));

    var sparkOptions = {
        type: 'pie',
        height: '28px',
        //width: '30px',
        tooltipFormat: '{{offset:offset}} {{value}}',
        tooltipValueLookups: {
            'offset': {
                0: 'Agrícola',
                1: 'Pecuario',
                2: 'Forestal'
            }
        },
        //barWidth: 20,
        //barSpacing: 5                
        sliceColors: APF_Colors
    }
    $('#sparkCreditos').sparkline(APF_Creditos, sparkOptions);
    $('#sparkSaldo').sparkline(APF_Saldo, sparkOptions);
    $('#sparkDesembolso').sparkline(APF_Desemb, sparkOptions);
    $('.sparkDatos').addClass('sparkline');

    var dsDesembAPF = {
        label: "Desembolso",
        backgroundColor: backgroundColor.Verde,
        borderColor: borderColor.Verde,
        borderWidth: AnchoBorde,
        data: APF_Desemb
    };
    //Grafico Saldo por Sector
    var dsCreditosAPF = {
        label: "N° Creditos: ",
        backgroundColor: backgroundColor.Morado,
        borderColor: borderColor.Morado,
        borderWidth: AnchoBorde,
        data: APF_Creditos,
        yAxisID: "y-axis-Creditos",
        order: 2
    };
    var dsSaldoAPF = {
        label: "Saldo: S/",
        backgroundColor: backgroundColor.Azul,
        borderColor: borderColor.Azul,
        borderWidth: AnchoBorde,
        data: APF_Saldo,
        yAxisID: "y-axis-Saldo",
        order: 1
    };

    var DataAPF_dsSaldo_dsCreditos = {
        labels: ["Agricola", "Pecuario", "Forestal"],
        datasets: [dsCreditosAPF, dsSaldoAPF]
    };
    var optionsBar_APF_dsSaldo_dsCreditos = {
        responsive: true,
        legend: {
            position: 'bottom',
        },
        tooltips: {
            //mode: 'dataset',
            callbacks: {
                label: function (tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + number_format(tooltipItem.yLabel, 0);
                }
            }
        },
        scales: {
            xAxes: [{
                display: true
            }],
            yAxes: [{
                id: "y-axis-Creditos",
                    ticks: {
                        beginAtZero: true,
                        callback: function (valor, index, valores) {
                            return Number(valor).toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ',').toString();
                        }
                    }
            },
                {
                    id: "y-axis-Saldo",
                    ticks: {
                        beginAtZero: true,
                        callback: function (valor, index, valores) {
                            return Number(valor).toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ',').toString();
                        }
                    }
                }]
        }

    };
    var ctx = document.getElementById('chartBar-SaldoDesembPorSector').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: DataAPF_dsSaldo_dsCreditos,
        options: optionsBar_APF_dsSaldo_dsCreditos
    });

    //Pie's
    var dsDesembSector = {
        label: "Desemb",
        backgroundColor: APF_Colors,
        borderColor: APF_ColorsBorde,
        borderWidth: AnchoBorde,
        data: APF_Desemb
    };
    var dsSaldoSector = {
        label: "Saldo",
        backgroundColor: APF_Colors,
        borderColor: APF_ColorsBorde,
        borderWidth: AnchoBorde,
        data: APF_Saldo
    };
    var DataSaldoPorSector = {
        labels: ["Agricola", "Pecuario", "Forestal"],
        datasets: [dsSaldoSector]
    };
    var DataDesembPorSector = {
        labels: ["Agricola", "Pecuario", "Forestal"],
        datasets: [dsDesembSector]
    };
    var optionsPie = {
        responsive: true,
        legend: {
            //position: 'left',
            //align: 'start',
            labels: {
                //padding: 20,
                //boxWidth: 12
            }
        },
        //tooltips: {
        //    //mode: 'dataset',
        //    callbacks: {
        //        label: function (tooltipItem, chart) {
        //            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
        //            return datasetLabel + ': S/ ' + number_format(tooltipItem.xLabel, 0);
        //        }
        //    }
        //},
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value * 100 / sum).toFixed(2) + "%";
                    return percentage;
                },
                color: 'white',
                labels: {
                    title: {
                        font: {
                            size: '14'
                        }
                    }
                }
            }
        }
    };

    var ctx = document.getElementById('chartPie-SaldoPorSector').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'pie',
        data: DataSaldoPorSector,
        plugins: [ChartDataLabels],
        options: optionsPie
    });
    //var ctx = document.getElementById('chartPie-DesembPorSector').getContext('2d');
    //var chart = new Chart(ctx, {
    //    type: 'pie',
    //    data: DataDesembPorSector,
    //    plugins: [ChartDataLabels],
    //    options: optionsPie
    //});

    // Bar Tuberia
    var dsAgricolaDesembAPAD = {
        label: "Agrícola",
        backgroundColor: APF_Colors[0],
        borderColor: APF_ColorsBorde[0],
        borderWidth: AnchoBorde,
        data: APAD_AgricolaDesemb,
        order: 1
    };

    var dsPecuarioDesembAPAD = {
        label: "Pecuario",
        backgroundColor: APF_Colors[1],
        borderColor: APF_ColorsBorde[1],
        borderWidth: AnchoBorde,
        data: APAD_PecuarioDesemb,
        order: 2
    };
    var dsForestalDesembAPAD = {
        label: "Forestal",
        backgroundColor: APF_Colors[2],
        borderColor: APF_ColorsBorde[2],
        borderWidth: AnchoBorde,
        data: APAD_ForestalDesemb,
        order: 3
    };

    var DataDesembPorEstado = {
        labels: ["Admision", "Propuesta", "Aprobado"], //, "Desembolso"],
        datasets: [dsAgricolaDesembAPAD, dsPecuarioDesembAPAD, dsForestalDesembAPAD]
    };
    var chartOptionsAPAD = {
        responsive: true,
        legend: {
            //position: 'bottom',
        },
        tooltips: {
            //mode: 'dataset',
            callbacks: {
                label: function (tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': S/ ' + number_format(tooltipItem.yLabel, 0);
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                stacked: true                
            }],
            yAxes: [{
                display: true,
                stacked: true,                
                ticks: {
                    beginAtZero: true,
                    callback: function (valor, index, valores) {
                        return Number(valor).toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ',').toString();
                    }
                }
            }]
        }
    }
    var ctx = document.getElementById('chartBar-Tuberia').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: DataDesembPorEstado,
        options: chartOptionsAPAD
    });


    // Bar por Funcionario
    var dsAgricola = {
        label: "Agrícola",
        backgroundColor: APF_Colors[0],
        borderColor: APF_ColorsBorde[0],
        borderWidth: AnchoBorde,
        data: Det.Agricola.Total._MontoSolicitado
    };

    var dsPecuario = {
        label: "Pecuario",
        backgroundColor: APF_Colors[1],
        borderColor: APF_ColorsBorde[1],
        borderWidth: AnchoBorde,
        data: Det.Pecuario.Total._MontoSolicitado
        //order: 1
    };
    var dsForestal = {
        label: "Forestal",
        backgroundColor: APF_Colors[2],
        borderColor: APF_ColorsBorde[2],
        borderWidth: AnchoBorde,
        data: Det.Forestal.Total._MontoSolicitado
        //order: 1
    };

    var lineData = {
        labels: DataResponse.Labels,
        datasets: [dsAgricola, dsPecuario, dsForestal]
    };
    var chartOptions = {
        //plugins: {
        //    sort: {
        //        enable: true,
        //        mode: 'array',
        //        order: 'asc',
        //        reference: [],
        //        sortBy: 'backgroundColor'
        //    }
        //},
        responsive: true,
        legend: {
            position: 'bottom',
        },
        tooltips: {
            //mode: 'dataset',
            callbacks: {
                label: function (tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': S/ ' + number_format(tooltipItem.xLabel, 0);
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                stacked: true,
                //scaleLabel: {
                //    display: true,
                //    labelString: 'Miles S/'
                //},
                ticks: {
                    beginAtZero: true,
                    callback: function (valor, index, valores) {
                        return Number(valor).toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ',').toString();
                    }
                }
            }],
            yAxes: [{
                display: true,
                //scaleLabel: {
                //    display: true,
                //    labelString: 'Funcionarios de Negocio'
                //},
                stacked: true,
            }]
        },
        //title: {
        //    display: true,
        //    text: 'Desembolsos Agencia'
        //}
        //animation: {
        //    duration: 2000,
        //    onProgress: function (animation) {
        //        progress.value = animation.currentStep / animation.numSteps;
        //    },
        //    onComplete: function () {
        //        window.setTimeout(function () {
        //            progress.value = 0;
        //        }, 2000);
        //    }
        //}
    }

    // render chart
    var ctx = document.getElementById('lineChart').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: lineData,
        options: chartOptions
    });
}



function number_format(number, decimals, dec_point, thousands_sep) {
    // *     example: number_format(1234.56, 2, ',', ' ');
    // *     return: '1 234,56'
    number = (number + '').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

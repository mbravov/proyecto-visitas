﻿var ventana;
var responsiveHelper_datatable_tabletools = undefined;
var breakpointDefinition = {
    tablet: 1024,
    phone: 480
};
$('#datatable_tableErrors').dataTable({
    "data": [],
    "autoWidth": true,
    "preDrawCallback": function () {
        if (!responsiveHelper_datatable_tabletools) {
            responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tableErrors'), breakpointDefinition);
        }
    },
    "rowCallback": function (nRow) {
        responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
    },
    "drawCallback": function (oSettings) {
        responsiveHelper_datatable_tabletools.respond();
    },
    "bServerSide": false,
    "columns": [
        { "data": "Numero", "width": "8%" },
        { "data": "Mensaje" }
    ],
    "error": function (xhr, error, thrown) {
        alert('Ocurrio un error al procesar');
    }
});

function finalizar() {
    //window.open('Home/finalizar', '_SELF', 'status=yes,resizable=yes,toolbar=no,scrollbars=yes,top=0,left=0', false);
    window.location.href = 'Home/finalizar';
}

function cerrarVentana() {
    window.open('', '_self', '').close();
}

$(document).ready(function () {
    $("#btnCargarArchivo").click(function () {
        var formData = new FormData();
        var totalFiles = document.getElementById("fileExcel").files.length;
        if (totalFiles === 1) {
            var file = document.getElementById("fileExcel").files[0];
            formData.append("fileExcel", file);
            $("#btnCargarArchivo").attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                url: 'Export/SubirFormatoMEM',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.Codigo === 0) {
                        ShowSuccessBox("Exito", response.Mensaje);
                    } else {
                        ShowWarningBox("Atención", response.Mensaje);
                        $("#btnCargarArchivo").removeAttr("disabled");
                    }
                    $('#datatable_tableErrors').DataTable().clear().draw();
                    if (response.Codigo === 0) {
                        setTimeout("finalizar();", 2000);
                    } else if (response.Codigo === 1001) {
                        //$('#datatable_tableErrors').DataTable().clear().draw();
                        $('#datatable_tableErrors').dataTable().fnAddData(response.Errors);
                    }
                },
                error: function (error) {
                    ShowErrorBox("Error en el servidor al ejecutar el proceso", "Registre el error en la plataforma de incidencias");
                }
            });
        } else if (totalFiles === 0) {
            ShowWarningBox("Atención", "Seleccione archivo");
        } else {
            ShowWarningBox("Atención", "Seleccione archivo");
        }
    });
});
﻿using ServiceMEG.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceMEG.Controllers
{
    public class ApiGrupoController : ApiController
    {
        // GET: api/ApiGrupo
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ApiGrupo/5
        public int Get(int idgrupo)
        {
            //Task.Run(() =>
            //{
            //    Servicio oServicio = new Servicio();
            //    oServicio.ObtenerPendienteGrupo(codmodulo,idgrupo);
            //    //return "value";
            //});
            return -1;
        }

        //// GET: api/ApiGrupo/5
        public int Get(int codmodulo, int idgrupo)
        {
            //Task.Run(() =>
            //{
                
            //    //return "value";
            //});

            Servicio oServicio = new Servicio();
            oServicio.ObtenerPendienteGrupo(codmodulo, idgrupo);
            return 99;
        }

        // POST: api/ApiGrupo
        //public void Post([FromBody]string value)
        //{

        //}
        [HttpPost]
        public int Post(int codmodulo, int idgrupo)
        {
            Servicio oServicio = new Servicio();
            oServicio.ObtenerPendienteGrupo(codmodulo, idgrupo);

            //Task.Run(() =>
            //{
                
            //    //return "value";
            //});
            return idgrupo;
        }
        // PUT: api/ApiGrupo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiGrupo/5
        public void Delete(int id)
        {
        }
    }
}

﻿using Agrobanco.PlataformaAgroperu.Domain.Models.Common;
using Agrobanco.PlataformaAgroperu.Domain.ServiceImplementations;
using Agrobanco.PlataformaAgroperu.Infraestructure.Helpers;
using Agrobanco.PlataformaAgroperu.Infraestructure.Settings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net.Security;

namespace ServiceMEG.Models
{
    public class Servicio
    {
        public static void logFile(string mensaje)
        {
            try
            {
                //Open the File
                var appSettings = ConfigurationManager.AppSettings;
                StreamWriter sw = new StreamWriter(appSettings["ruta_log"], true, Encoding.ASCII);
                string fec = DateTime.Now.ToString();
                sw.WriteLine(fec + "::" + mensaje);

                //close the file
                sw.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "log FILE service");
            }
            finally
            {

            }
        }
        public void ObtenerPendienteGrupo(int codmodulo, int codgrupo)
        {

            var grupoDomain = new ApiGrupoDomain();
            logFile("Metodo:ObtenerPendienteGrupo:: codmodulo=" + codmodulo.ToString() + " codgrupo=" + codgrupo.ToString());
            List<IntegranteSentinel> lista = null;
            if (codmodulo == 3)
            {
                lista = grupoDomain.ObtenerIntegrantePadronSentinel(codmodulo, codgrupo);
                logFile("Metodo:ObtenerIntegrantePadronSentinel::lista.Count=" + lista.Count.ToString());
            }
            else if (codmodulo == 2)
            {
                lista = grupoDomain.ObtenerIntegranteGrupoSentinel(codmodulo, codgrupo);
                logFile("Metodo:ObtenerIntegranteGrupoSentinel::lista.Count=" + lista.Count.ToString());
            }

            string infoGrupo = "";
            int orden = 1;

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            foreach (var item in lista)
            {
                infoGrupo = infoGrupo + codgrupo.ToString() + '|' + ConsultaSentinel(orden.ToString(), codgrupo.ToString(),item.tipo_documento, item.nro_documento) + "|*|";
                orden++;
            }
            if (infoGrupo != "")
            {
                //grupoDomain.GuardarIntegranteSentinel(codmodulo, infoGrupo, codgrupo.ToString());
            }

        }

        //public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        //{
        //    return true;
        //}


        private string ConsultaSentinel(string n,string codgrupo,string tip_documento, string nro_documento)
        {
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            //System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AcceptAllCertifications);
            wsConsultaSentinel.sdt_respuesta_consulta_rapida r;
            r = null;
            string trama = "";


            


            try
            {
                Logger.LogInformation(codgrupo + "-" + n + " Inicio:: Consulta Sentinel ::tipo=" + tip_documento + "nro=" + nro_documento);
                wsConsultaSentinel.ws_senestlite03SoapPortClient cli = new wsConsultaSentinel.ws_senestlite03SoapPortClient();
                Logger.LogInformation("linea 76: Instancia cliente del web service");
                Logger.LogInformation("linea 79: Asigna ws_usuario");
                cli.ClientCredentials.UserName.UserName = ApplicationKeys.ws_usuario;
                Logger.LogInformation("linea 81: Asigna ws_clave");
                cli.ClientCredentials.UserName.Password = ApplicationKeys.ws_clave;
                Logger.LogInformation("linea 83: abre conexcion");
                cli.Open();
                Logger.LogInformation("linea 83: conexion abierta");

                Logger.LogInformation("Declara usuario, clave y key de WS");
                string Gx_usuenc = ApplicationKeys.ws_usuario;
                string Gx_pasenc = ApplicationKeys.ws_clave;
                string Gx_key = ApplicationKeys.ws_key;                
                long Servicio = long.Parse(ApplicationKeys.ws_servicio.ToString());
                Logger.LogInformation("linea 92: Ejecuta servicio para recibir respuesta");

                
                r = cli.Execute(Gx_usuenc, Gx_pasenc, Gx_key, Servicio, tip_documento, nro_documento);                

                Logger.LogInformation("codigo respuesta: " + r.CodigoWS);
                string v_sennot = string.IsNullOrEmpty(r.Nota) ? "0" : r.Nota;
                string v_sendeto = string.IsNullOrEmpty(r.DeudaTotal) ? "0" : r.DeudaTotal;
                string v_senvcdo = string.IsNullOrEmpty(r.VencidoBanco) ? "0" : r.VencidoBanco;
                string v_sendtrb = string.IsNullOrEmpty(r.DeudaTributaria) ? "0" : r.DeudaTributaria;
                string v_sendlab = string.IsNullOrEmpty(r.DeudaLaboral) ? "0" : r.DeudaLaboral;
                string v_sendipg = string.IsNullOrEmpty(r.DeudaImpaga) ? "0" : r.DeudaImpaga;
                string v_sendprt = string.IsNullOrEmpty(r.DeudaProtestos) ? "0" : r.DeudaProtestos;
                string v_sendsbs = string.IsNullOrEmpty(r.DeudaSBS) ? "0" : r.DeudaSBS;
                Logger.LogInformation("linea 103: Asignamos trama");
                trama = r.Documento + '|' + r.RazonSocial + '|' + r.FechaProceso + '|' + r.Semaforos + '|' + v_sennot + '|' + r.NroBancos + '|' + v_sendeto + '|' + v_senvcdo + '|' +
                    r.Calificativo + '|' + r.SemaActual + '|' + r.SemaPrevio + '|' + r.SemaPeorMejor + '|' + r.Documento2 + '|' + r.EstDomic + '|' + r.CondDomic + '|' + v_sendtrb + '|' +
                    v_sendlab + '|' + v_sendipg + '|' + v_sendprt + '|' + v_sendsbs + '|' + r.TarCtas + '|' + r.RepNeg + '|' + r.TipoActv + '|' + r.FechIniActv + '|' + r.DireccionFiscal + '|' + r.CodigoWS;
                cli.Close();
                Logger.LogInformation(codgrupo + "-" + n + " Fin:: Consulta Sentinel ::trama=" + trama);
            }
            catch (Exception ex)
            {
                
                Logger.LogException(ex,ex.Message);
                
                
                //  logFile("Metodo:ConsultaSentinel::ID:" + ID + ",tip_documento:" + tip_documento + ",nro_documento:" + nro_documento + " " + ex.Message + " " + ex.InnerException);
            }
            return trama;
        }
    }
}
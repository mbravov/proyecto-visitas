import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfesionalTecnicoComponent } from './componentes/profesional-tecnico/profesional-tecnico.component';
import { HomeComponent } from './componentes/home/home.component';
import { RedirectComponent } from './componentes/redirect/redirect.component';
import { IndexComponent } from './componentes/redirect/index/index.component';
import { AsignacionesComponent } from './componentes/asignaciones/asignaciones.component';
import { VinculacionComponent } from './componentes/vinculacion/vinculacion.component';
import { VisitasComponent } from './componentes/visitas/visitas.component';
import { AuthGuard } from './guards/auth.guard';
import { PrediosComponent } from './componentes/consulta/predios/predios.component';
import { AsignacionIndividualComponent } from './componentes/asignacion-individual/asignacion-individual.component';
import { ImeiComponent } from './componentes/imei/imei.component';

export const ROUTES: Routes = [
  { path: 'home', component: HomeComponent , canActivate: [ AuthGuard ] },
  { path: 'visitas/asignacion', component: AsignacionesComponent , canActivate: [ AuthGuard ] },
  { path: 'visitas/individual', component: AsignacionIndividualComponent , canActivate: [ AuthGuard ] },
  { path: 'vinculacion', component: VinculacionComponent, canActivate: [ AuthGuard ] },
  { path: 'visitas/consultas', component: VisitasComponent, canActivate: [ AuthGuard ] },
  { path: 'Consulta/Predios', component: PrediosComponent, canActivate: [ AuthGuard ] },
  { path: 'ResetearIMEI', component: ImeiComponent, canActivate: [ AuthGuard ] },
  // { path: 'tecnico', component: ProfesionalTecnicoComponent},
  //{ path: 'visitas', component: VisitasComponent},
  { path: 'tecnico', component: ProfesionalTecnicoComponent, canActivate: [ AuthGuard ] },
  { path: 'Redirect',
    children: [
    { path: 'Index', component: IndexComponent }], component: RedirectComponent },
{ path: '' , pathMatch: 'full', redirectTo: 'home'},
{ path: '**' , pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

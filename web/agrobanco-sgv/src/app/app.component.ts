import { Component, OnInit } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';

declare var $: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'agrobanco-sgv';

  constructor(public authGuard: AuthGuard) { }

  ngOnInit(): void { }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { Constantes } from 'src/app/comun/constantes';
import { AsociacionClienteProfesional } from 'src/app/models/AsociacionClienteProfesional.model';
import { Funcionario } from 'src/app/models/Funcionario.model';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { AsignacionService } from 'src/app/services/asignacion.service';
import { DomService } from 'src/app/services/dom.service';
import { ParametrosService } from 'src/app/services/parametros.service';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';
import { RedirectService } from 'src/app/services/redirect.service';
declare var M:any;

@Component({
  selector: 'app-asignacion-individual',
  templateUrl: './asignacion-individual.component.html',
  styleUrls: ['./asignacion-individual.component.css']
})
export class AsignacionIndividualComponent implements OnInit {

  msjSuccess = "Asociación realizada con éxito";
  asignacionForm = new FormGroup({});
  formSubmitted = false;
  loader = false;
  funcionarios: Funcionario[] = [];
  profesionales: ProfesionalTecnico[] = [];
  visitas: any = [{codigoInterno: "INICIAL"}];

  constructor(
    private domService: DomService,
    private parametroService: ParametrosService,
    private profesionalService: ProfesionalTecnicoService,
    private asignacionService: AsignacionService,
    private redirectService: RedirectService
  ) { }

  ngOnInit(): void {
    this.inicializarFormulario();
    this.obtenerParametros();
    this.domService.HideSideBar();
  }

  inicializarFormulario() {
    this.asignacionForm = new FormGroup({
      numerodocumento: new FormControl("", [Validators.required, Validators.minLength(8)]),
      nombre: new FormControl("", [Validators.required]),
      codigofuncionario: new FormControl("", [Validators.required]),
      codigoprofesional: new FormControl("", [Validators.required]),
      tipovisita: new FormControl(this.visitas[0].codigoInterno)
    });
  }

  inicializarCombos() {
    M.FormSelect.init(document.getElementById('select_funcionario'), {});
    M.FormSelect.init(document.getElementById('select_visita'), {});
    M.FormSelect.init(document.getElementById('select_profesional'), {});
  }

  obtenerParametros() {
    let agencia = Number(this.redirectService.leerAgencia());
    this.loader = true;
    forkJoin([
      this.parametroService.obtenerFuncionarios(agencia),
      this.parametroService.obtenerParametros(Constantes.parametroTipoVisita),
      this.profesionalService.buscarProfesional(new ProfesionalTecnicoModel())
    ]).subscribe((data: any) => {
      //console.log(data);
      
      this.funcionarios = data[0] as Funcionario[];
      this.visitas = data[1];
      this.profesionales = data[2].response.lista as ProfesionalTecnico[];

      setTimeout(() => {
        this.loader = false;
        this.inicializarFormulario();
        this.inicializarCombos();
      }, 1000);
    }, (err) => {
      this.loader = false;
      console.error(err);
    });
  }

  grabarAsignacion() {
    this.formSubmitted = true;

    if (this.asignacionForm.invalid) {
      return;
    }

    let params = this.asignacionForm.value;
    let profesional = this.profesionales[params.codigoprofesional];
    let usuario = this.redirectService.leeridPerfil();
    let primerNombre = "";
    let segundoNombre = "";
    let apellidoPaterno = "";
    let apellidoMaterno = ""; 

    if (params.nombre.length > 0 ){

      let nombreCompleto = params.nombre.split(" ");

      if (nombreCompleto.length == 4)
      {
        primerNombre = nombreCompleto[0].toString();
        segundoNombre = nombreCompleto[1].toString();
        apellidoPaterno = nombreCompleto[2].toString();
        apellidoMaterno = nombreCompleto[3].toString();      
      }else if (nombreCompleto.length == 3)
      {
        primerNombre = nombreCompleto[0].toString();    
        segundoNombre = ""; 
        apellidoPaterno = nombreCompleto[1].toString();
        apellidoMaterno = nombreCompleto[2].toString();
      }else if (nombreCompleto.length == 2)
      {
        primerNombre = nombreCompleto[0].toString();    
        segundoNombre = ""; 
        apellidoPaterno = nombreCompleto[1].toString();
        apellidoMaterno = "";
      }
   }

    let data = new AsociacionClienteProfesional();
    data.primernombre = primerNombre;
    data.segundonombre = segundoNombre ;
    data.apellidopaterno = apellidoPaterno;
    data.apellidomaterno = apellidoMaterno;  
    data.codigousuario = usuario;
    data.codigofuncionario = '0';
    data.tipodocumento = 1;
    data.estado = 1;
    data.usuarioweb = params.codigofuncionario;
    data.numerodocumento =  params.numerodocumento.toString();
    data.codigoprofesional = profesional.codigo;

    this.loader = true;
    // console.log(data);
    
    this.asignacionService.asignacionIndividual(data).subscribe((res: any) => {
      this.loader = false;
      if (res.exito) {
        this.openModalSuccess();
      } else {
        this.openModalError();
      }
    }, (err: any) => {
      this.loader = false;
      this.openModalError(false);
    });
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(reload: boolean = true){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        if (reload) {
          // location.reload();
        }
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { Constantes } from 'src/app/comun/constantes';
import { Agencia } from 'src/app/models/Agencia.interface';
import { AgenciaModel } from 'src/app/models/Agencia.model';
import { ClienteModel, Cliente } from 'src/app/models/Cliente.model';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { AsignacionService } from 'src/app/services/asignacion.service';
import { DomService } from 'src/app/services/dom.service';
import { ParametrosService } from 'src/app/services/parametros.service';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';
/***  ANGULAR MATERIAL ***/
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Asociacion, AsociacionModel } from 'src/app/models/Asociacion.model';
import { Funcionario, FuncionarioModel } from 'src/app/models/Funcionario.model';
import { IntegranteAsociacion, IntegranteAsociacionModel } from 'src/app/models/IntegranteAsociacion.model';
import { AsociacionClienteProfesional } from 'src/app/models/AsociacionClienteProfesional.model';
import { RedirectService } from 'src/app/services/redirect.service';
import { Console } from 'console';

declare var M: any;
declare var $: any;

@Component({
  selector: 'app-asignaciones',
  templateUrl: './asignaciones.component.html',
  styleUrls: ['./asignaciones.component.css']
})
export class AsignacionesComponent implements OnInit {

  public loader: boolean = false;
  
  asignacionForm: FormGroup;
  agenciaForm: FormGroup;
  agencias: any[] = [];
  asociaciones: Asociacion[] = [];
  funcionarios: any[] = [];
  profesionales: ProfesionalTecnico[] = [];
  tipovisitas: any = [{codigoInterno: "INICIAL"}];
  ELEMENT_DATA! : IntegranteAsociacion[];  
  displayedColumns: string[] = ["documento", "nombres", "ultimoFiltro", "resultado", "seleccionar"];
  listaClientes = new MatTableDataSource<IntegranteAsociacion>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  formSubmitted: boolean = false;
  msjSuccess = "Asignación realizada con éxito";
  CodAgenciaStorage:number;

  constructor(
      private domService: DomService, 
      private profesionalService: ProfesionalTecnicoService,
      private parametroService: ParametrosService,
      private asignacionService: AsignacionService,
      private redirectService: RedirectService
    ) { 
    this.asignacionForm = new FormGroup({});
    this.agenciaForm = new FormGroup({});
    this.CodAgenciaStorage = Number(redirectService.leerAgencia());
  }

  ngOnInit(): void {
    
    this.domService.HideSideBar();
    this.listaClientes.paginator = this.paginator;
    this.obtenerParametros();
    this.inicializarFormulario();
  }

  inicializarFormulario() {
    
    this.asignacionForm = new FormGroup({
      codigoprofesional : new FormControl("", [Validators.required]),
      tipovisita : new FormControl(this.tipovisitas[0].codigoInterno),
      codigofuncionario : new FormControl("", [Validators.required])
    });

    this.agenciaForm = new FormGroup({
      codigoagencia : new FormControl("", [Validators.required]),
      codigoasociacion : new FormControl("", [Validators.required])
    });

  }

  obtenerParametros() {
    this.loader = true;
    forkJoin([
      this.profesionalService.obtenerAgencias(), 
      this.parametroService.obtenerParametros(Constantes.parametroTipoVisita)
    ]).subscribe((data: any) => {

      data[0].response.forEach((item : AgenciaModel) => {
        this.agencias.push(item as Agencia);
      }); 
      
      this.tipovisitas = data[1];

      setTimeout(() => {
        this.cargarCombos();
        this.inicializarFormulario();
        this.agenciaForm.controls["codigoagencia"].setValue(this.CodAgenciaStorage);
        this.loader = false;
        this.agenciaSeleccionada(this.CodAgenciaStorage);
      }, 1500);
    });
  }

  cargarCombos() {
    M.FormSelect.init(document.getElementById('select_agencia'), {});
    M.FormSelect.init(document.getElementById('select_funcionario'), {});
    M.FormSelect.init(document.getElementById('select_asociacion'), {});
    M.FormSelect.init(document.getElementById('select_tipovisita'), {});
    M.FormSelect.init(document.getElementById('select_profesional'), {});
  }

  agenciaSeleccionada2(selector: any) {
    let agencia = selector.value.toString().split(" ")[1];
    
    let filter = new ProfesionalTecnicoModel();
    filter.codigoAgencia = Number(agencia);
    this.listaClientes.data = [];

    this.loader = true;
    forkJoin([
      this.parametroService.obtenerFuncionarios(Number(agencia)),
      this.parametroService.obtenerAsociaciones(Number(agencia)),
      this.profesionalService.buscarProfesional(filter)
    ]).subscribe((res: any) => {
      console.log(res);
      this.funcionarios = res[0] as Funcionario[];
      this.asociaciones = res[1] as Asociacion[];
      this.profesionales = res[2].response.lista as ProfesionalTecnico[];
      // console.log(this.profesionales);
      
    }, (err: any) => {
      this.loader = false;
      console.log(err); 
    }, () => {
      setTimeout(() => {
        this.cargarCombos();
        this.loader = false;
      }, 1500);
    });
  }

  agenciaSeleccionada(agencia: number) {
    // let agencia = selector.value.toString().split(" ")[1];
    
    let filter = new ProfesionalTecnicoModel();
    filter.codigoAgencia = Number(agencia);
    this.listaClientes.data = [];

    this.loader = true;
    forkJoin([
      this.parametroService.obtenerFuncionarios(Number(agencia)),
      this.parametroService.obtenerAsociaciones(Number(agencia)),
      this.profesionalService.buscarProfesional(filter)
    ]).subscribe((res: any) => {
      console.log(res);
      this.funcionarios = res[0] as Funcionario[];
      this.asociaciones = res[1] as Asociacion[];
      this.profesionales = res[2].response.lista as ProfesionalTecnico[];
      // console.log(this.profesionales);
      
    }, (err: any) => {
      this.loader = false;
      console.log(err); 
    }, () => {
      setTimeout(() => {
        this.cargarCombos();
        this.loader = false;
      }, 1500);
    });
  }

  asociacionSeleccionada() {
    this.listaClientes.data = [];
  }

  seleccionaCliente(index: number, selected: any) {
    // console.log(this.paginator);
    
    this.listaClientes.data[index + this.paginator.pageIndex * this.paginator.pageSize].seleccionar = selected.target.checked;
    console.log(this.listaClientes.data[index + this.paginator.pageIndex * this.paginator.pageSize]);
  }

  buscarAsignaciones() {  
    this.formSubmitted = true;

    if(this.agenciaForm.invalid)
    {      
      return;
    }
  
    let params = this.agenciaForm.value;
    console.log(params);

    this.loader = true;
    this.parametroService.obtenerIntegrantesAsociaciones(params.codigoasociacion).subscribe((res: IntegranteAsociacionModel[]) => {
      this.listaClientes.data = res as IntegranteAsociacion[];
      // console.log(this.listaClientes.data);
      
      this.loader = false;
    },(err: any)=>{
      console.log(err);   
      this.loader = false;             
    });
  }

  asignarClientes() {
    this.formSubmitted = true;

    if(this.asignacionForm.invalid || this.listaClientes.data.length === 0)
    {      
      return;
    }

    let params = this.asignacionForm.value;
    let lista: AsociacionClienteProfesional[] = [];
    let usuario = this.redirectService.leeridPerfil();
    let profesional = this.profesionales[params.codigoprofesional];

    this.listaClientes.data.forEach((item: IntegranteAsociacion) => {
      if (item.seleccionar) {
        let nombres = item.nombre.split(" ");
        let asItem = new AsociacionClienteProfesional();
        asItem.tipodocumento = 1;
        asItem.numerodocumento = item.documento;
        asItem.estado = 1;
        asItem.codigousuario = usuario;
        asItem.codigoprofesional = profesional.codigo;
        asItem.codigofuncionario = '0';
        asItem.usuarioweb = params.codigofuncionario;
        asItem.primernombre = nombres[0];
        asItem.apellidomaterno = nombres[nombres.length - 1];
        asItem.apellidopaterno = nombres[nombres.length - 2];
        if (nombres.length > 3) {
          asItem.segundonombre = nombres[1];
        }
        lista.push(asItem);
      }
    })

    console.log(this.listaClientes.data);
    

    this.loader = true;
    this.asignacionService.asignarClientesProfesional(lista).subscribe((res: any) => {
      this.loader = false;
      if (res.exito) {
        this.openModalSuccess();
      } else {
        this.openModalError();
      }
    }, (error) => {
      this.openModalError();
      console.error(error);
      this.loader = false;
    });
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        // location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

}

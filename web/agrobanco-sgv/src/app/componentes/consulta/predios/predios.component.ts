import { Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { Agencia } from 'src/app/models/Agencia.interface';
import { AgenciaModel } from 'src/app/models/Agencia.model';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { ParametrosService } from 'src/app/services/parametros.service';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';
import { PredioService } from '../../../services/predio.service';
import { ProfesionalTecnico } from '../../../models/ProfesionalTecnico.interface';
import { Funcionario, FuncionarioModel } from '../../../models/Funcionario.model';
import { Asociacion, AsociacionModel } from '../../../models/Asociacion.model';
import { environment } from 'src/environments/environment';
import { VisitaService } from 'src/app/services/visita.service';
import { Cultivo } from 'src/app/models/Cultivo.model';
import { DomService } from 'src/app/services/dom.service';
import { ThrowStmt } from '@angular/compiler';


declare var M: any;
declare var $: any;
declare var ol: any;
declare var mapaPuntosGeograficos:any;
declare var mapaAreaPerimetro:any;
declare var mapaAreaPerimetro2:any;

@Component({
  selector: 'app-predios',
  templateUrl: './predios.component.html',
  styleUrls: ['./predios.component.css']
})
export class PrediosComponent implements OnInit {

  busquedaFiltro: FormGroup;
  CodAgenciaStorage!:number;
  //funcionarios = [{id: 0, name: 'Todos'}];
  loader:boolean = true;
  peticionesTerminadas = true;

  agencias: Agencia[] = [];
  profesionales: ProfesionalTecnico[] = [];
  funcionarios: any[] = [];
  asociaciones: Asociacion[] = [];
  cultivos: Cultivo[] = [];
  campanas: any[] = [];
  creditos: string[] = [];
  map: any;
  noResultados:boolean  = false;

  constructor(
    private FormBuilder: FormBuilder,
    private elementRef:ElementRef,
    private profesionalService: ProfesionalTecnicoService,
    private parametroService: ParametrosService,
    private predioService: PredioService,
    private domService: DomService,
    private visitaService:VisitaService) {
    this.busquedaFiltro = new FormGroup({});
    this.CodAgenciaStorage = Number(localStorage.getItem('CodAgenciaAsignada'));
   }

  ngOnInit(): void {

    this.inicializarFormulario();
    this.cargaInicial();
    this.domService.HideSideBar();
  //  this.mapa1();

    //this.MostrarMapaAreaPerimetro();
  }

  private inicializarFormulario(): void {
    /*
    this.busquedaFiltro = new FormGroup({
                  codigoagencia : new FormControl(0),
                  codigofuncionario:  new FormControl(0),
    });
    */
    this.initFormBusquedaFiltro();
  }


  private cargaInicial(): void {
    this.loader = true;
    this.peticionesTerminadas = false;

    forkJoin([
      this.profesionalService.obtenerAgencias(),
      this.parametroService.obtenerCultivos(),
      this.parametroService.obtenerParametros(9),
      this.parametroService.obtenerCreditos(),
      this.parametroService.obtenerAsociaciones(this.CodAgenciaStorage),
      this.parametroService.obtenerFuncionarios(this.CodAgenciaStorage)
    ])
    .subscribe((data) => {
      // console.log(data[2]);
      console.log(data[0])
      this.loadAgencias(data[0]);
      this.cultivos = data[1] as Cultivo[];
      this.campanas = data[2] as any[];
      this.creditos = data[3] as string[];
      this.asociaciones = data[4] as Asociacion[];
      this.funcionarios = data[5] as Funcionario[];
      this.loader = false;

    }, (error) => {
      console.log(error);
    }, () => {

      setTimeout(() => {
        this.cargarCombos();
        this.peticionesTerminadas = true;
      }, 1000);

    });

  }

  private loadAgencias(result: any){
    this.agencias = result.response;

    this.busquedaFiltro.controls["codigoagencia"].setValue(this.CodAgenciaStorage);
    
    this.listarFuncionarios(this.CodAgenciaStorage);
  }

  initFormBusquedaFiltro() : void
  {
    this.busquedaFiltro = new FormGroup({
      codigoagencia: new FormControl(0) ,
      codigofuncionario: new FormControl('0') ,
      cultivo: new FormControl(0) ,
      campania: new FormControl(0) ,
      codigoAsociacion: new FormControl(0) ,
      codigoEstadoCredido: new FormControl(0),
      TipoMapa: new FormControl('',[Validators.required])   
    });
    // this.busquedaFiltro = this.FormBuilder.group({
    //   codigoagencia: [0],
    //   codigofuncionario: [0],
    //   cultivo: [0],
    //   campania: [0],
    //   codigoAsociacion: [0],
    //   codigoEstadoCredido:[0],
    //   TipoMapa:[]

    // });
  }

  private cargarCombos(): void {
    M.FormSelect.init(document.getElementById('select_agencia'), {});
    M.FormSelect.init(document.getElementById('select_funcionarios'), {});
    M.FormSelect.init(document.getElementById('select_cultivo'), {});
    M.FormSelect.init(document.getElementById('select_campana'), {});
    M.FormSelect.init(document.getElementById('select_asociacion'), {});
    M.FormSelect.init(document.getElementById('select_estadocredito'), {});
  }

  agenciaSeleccionada(selector: any) {
    var filtro = selector.value.toString().split(" ")[1]
    this.peticionesTerminadas = false;
    this.funcionarios  = [];
    this.asociaciones  = []
    this.listarFuncionarios(Number(filtro));
    this.listarAsociaciones(Number(filtro));

  }

  listarFuncionarios(agencia: number) {
    this.parametroService.obtenerFuncionarios(agencia).subscribe((res: FuncionarioModel[]) => {
      this.funcionarios = res as Funcionario[];
      // console.log("funcionarios",this.funcionarios);
    }, (error) => {
      console.error(error);
    },()=>{
      setTimeout(() => {
        this.cargarCombos()
      }, 1500);
      this.peticionesTerminadas = true;
    });
  }

  listarAsociaciones(agencia: number) {
    this.parametroService.obtenerAsociaciones(agencia).subscribe((res: AsociacionModel[]) => {
      // console.log("asociaciones",this.asociaciones);
      this.asociaciones = res as Asociacion[];
    }, (error) => {
      console.error(error);
    },()=>{
      setTimeout(() => {
        this.cargarCombos()
      }, 1500);
      this.peticionesTerminadas = true;
    });
  }


  BuscarPredio():void{
    console.log("busquedaFiltro",this.busquedaFiltro.getRawValue());
    if (this.busquedaFiltro.invalid) {
      return;
    }

    this.peticionesTerminadas = false;
    var d1 = this.elementRef.nativeElement.querySelector('#mapid');
    d1.insertAdjacentHTML('beforeend', "");
    
    if(this.map != undefined){
      this.map.remove();
      this.map = null;
    }
    
    this.predioService.obtenerPredio(this.busquedaFiltro.getRawValue()).subscribe((data:any)=>{
        let dataPredios = data;
        let ArrBase64:any = [];
        let ArrBase64Ordenada:any = [];
        let iteraciones = 0;
        let codLaserFicheanidado :string = "";
        console.log(dataPredios);
        if(dataPredios.length == 0){
          this.noResultados=true;
          this.peticionesTerminadas = true;
          return;
        }else{
          this.noResultados=false;
        }
        for(let index = 0 ; index < dataPredios.length; index++){
          codLaserFicheanidado = "";
          dataPredios[index].listaImagenes.forEach((element:any) => {
            codLaserFicheanidado = codLaserFicheanidado + "" +element.archivoByte + "|";
          });
          codLaserFicheanidado = codLaserFicheanidado.slice(0,-1);
          this.visitaService.obtenerimagenesListaMapa(codLaserFicheanidado, index).subscribe((data:any) => {
            // console.log(data);
            ArrBase64.push(data.response);
            iteraciones ++;
            if(iteraciones == dataPredios.length){
              // console.log(ArrBase64);

              for(let x=0;x<ArrBase64.length;x++){
                let dataBase64 = ArrBase64.filter((y:any)=> y.posicionLista == x);
                ArrBase64Ordenada.push(dataBase64[0].dataList);
              }
              // console.log(ArrBase64Ordenada)

              if(this.busquedaFiltro.controls['TipoMapa'].value==0){
                // this.MostrarMapaAreaPerimetro(data);
                
                this.map = mapaAreaPerimetro(dataPredios,ArrBase64Ordenada);
               }
               else{
                 this.map =  mapaPuntosGeograficos(dataPredios,ArrBase64Ordenada);
                 //this.MostrarMapaPuntosGeograficos(data)
               }
               this.peticionesTerminadas = true;
            }

            
          });
        }     

    }, (err: any) => {
      console.error(err);
      this.peticionesTerminadas = true;
      this.openModalError();
    });
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        // location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

}

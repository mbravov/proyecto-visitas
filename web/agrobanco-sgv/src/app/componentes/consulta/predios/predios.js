function mapaAreaPerimetro(ArrayVisitas,ArrBase64){
  var  mymap = null;
  if(ArrayVisitas.length==0){
    return;
  }
  var LogLat = [
    ArrayVisitas[0].listaCoordenadas[0].latitud,
    ArrayVisitas[0].listaCoordenadas[0].longitud
  ];

   mymap = L.map('mapid').setView(LogLat, 15);

  var arrayPuntos = [];
  var contar = 1;
  for (let index = 0; index < ArrayVisitas.length; index++) {
    const element = ArrayVisitas[index];
    try {
        var myIcon = L.icon({
          iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAFgUlEQVR4Aa1XA5BjWRTN2oW17d3YaZtr2962HUzbDNpjszW24mRt28p47v7zq/bXZtrp/lWnXr337j3nPCe85NcypgSFdugCpW5YoDAMRaIMqRi6aKq5E3YqDQO3qAwjVWrD8Ncq/RBpykd8oZUb/kaJutow8r1aP9II0WmLKLIsJyv1w/kqw9Ch2MYdB++12Onxee/QMwvf4/Dk/Lfp/i4nxTXtOoQ4pW5Aj7wpici1A9erdAN2OH64x8OSP9j3Ft3b7aWkTg/Fm91siTra0f9on5sQr9INejH6CUUUpavjFNq1B+Oadhxmnfa8RfEmN8VNAsQhPqF55xHkMzz3jSmChWU6f7/XZKNH+9+hBLOHYozuKQPxyMPUKkrX/K0uWnfFaJGS1QPRtZsOPtr3NsW0uyh6NNCOkU3Yz+bXbT3I8G3xE5EXLXtCXbbqwCO9zPQYPRTZ5vIDXD7U+w7rFDEoUUf7ibHIR4y6bLVPXrz8JVZEql13trxwue/uDivd3fkWRbS6/IA2bID4uk0UpF1N8qLlbBlXs4Ee7HLTfV1j54APvODnSfOWBqtKVvjgLKzF5YdEk5ewRkGlK0i33Eofffc7HT56jD7/6U+qH3Cx7SBLNntH5YIPvODnyfIXZYRVDPqgHtLs5ABHD3YzLuespb7t79FY34DjMwrVrcTuwlT55YMPvOBnRrJ4VXTdNnYug5ucHLBjEpt30701A3Ts+HEa73u6dT3FNWwflY86eMHPk+Yu+i6pzUpRrW7SNDg5JHR4KapmM5Wv2E8Tfcb1HoqqHMHU+uWDD7zg54mz5/2BSnizi9T1Dg4QQXLToGNCkb6tb1NU+QAlGr1++eADrzhn/u8Q2YZhQVlZ5+CAOtqfbhmaUCS1ezNFVm2imDbPmPng5wmz+gwh+oHDce0eUtQ6OGDIyR0uUhUsoO3vfDmmgOezH0mZN59x7MBi++WDL1g/eEiU3avlidO671bkLfwbw5XV2P8Pzo0ydy4t2/0eu33xYSOMOD8hTf4CrBtGMSoXfPLchX+J0ruSePw3LZeK0juPJbYzrhkH0io7B3k164hiGvawhOKMLkrQLyVpZg8rHFW7E2uHOL888IBPlNZ1FPzstSJM694fWr6RwpvcJK60+0HCILTBzZLFNdtAzJaohze60T8qBzyh5ZuOg5e7uwQppofEmf2++DYvmySqGBuKaicF1blQjhuHdvCIMvp8whTTfZzI7RldpwtSzL+F1+wkdZ2TBOW2gIF88PBTzD/gpeREAMEbxnJcaJHNHrpzji0gQCS6hdkEeYt9DF/2qPcEC8RM28Hwmr3sdNyht00byAut2k3gufWNtgtOEOFGUwcXWNDbdNbpgBGxEvKkOQsxivJx33iow0Vw5S6SVTrpVq11ysA2Rp7gTfPfktc6zhtXBBC+adRLshf6sG2RfHPZ5EAc4sVZ83yCN00Fk/4kggu40ZTvIEm5g24qtU4KjBrx/BTTH8ifVASAG7gKrnWxJDcU7x8X6Ecczhm3o6YicvsLXWfh3Ch1W0k8x0nXF+0fFxgt4phz8QvypiwCCFKMqXCnqXExjq10beH+UUA7+nG6mdG/Pu0f3LgFcGrl2s0kNNjpmoJ9o4B29CMO8dMT4Q5ox8uitF6fqsrJOr8qnwNbRzv6hSnG5wP+64C7h9lp30hKNtKdWjtdkbuPA19nJ7Tz3zR/ibgARbhb4AlhavcBebmTHcFl2fvYEnW0ox9xMxKBS8btJ+KiEbq9zA4RthQXDhPa0T9TEe69gWupwc6uBUphquXgf+/FrIjweHQS4/pduMe5ERUMHUd9xv8ZR98CxkS4F2n3EUrUZ10EYNw7BWm9x1GiPssi3GgiGRDKWRYZfXlON+dfNbM+GgIwYdwAAAAASUVORK5CYII='
        })
        for (var e = 0 in element.listaCoordenadas)
        {
          LogLat = [
            element.listaCoordenadas[e].latitud,
            element.listaCoordenadas[e].longitud
          ];
          // var marker = L.marker(LogLat);
          var marker = L.marker(LogLat,{icon : myIcon});

          marker.addTo(mymap);

          var img = '';
          for (var i = 0 in element.listaImagenes){
            img+= "<img  width='100%' class='imgToolTip' src='data:image/jpg;base64, "+  ArrBase64[index][i].archivoBytes+"'>";
            break;
          }
          //marker.bindPopup("<b>Visita "+contar+"</b><br>Predio visitado el dia "+element.fecha_visita+"<br>Todo conforme<br><br>").openPopup();
          marker.bindPopup(
            "<b>Visita "+contar+"</b><br>"+
            "Predio visitado el dia "+ formatDate(new Date(element.fecha_visita)) +"<br>"+
            "Cliente: " + element.primerNombre + " " + element.segundoNombre + " " + element.apellidoPaterno + " " + element.apellidoMaterno +"<br>" + 
            "Nombre de Predio: "+ element.nombrePredio +"<br>" + 
            "Dirección de Predio: "+ element.direccionPredio +"<br>" + 
            "Tipo de actividad: "+ actividad(element.actividad) +"<br>" + 
            "Comentario: "+ element.comentario +"<br><br>" + 
            img
            ).openPopup();
          arrayPuntos.push(LogLat);
        }

        L.polygon(arrayPuntos, {
          color: '#ff5050',
          fillColor: '#ff5050',
          fillOpacity: 0.7
        }).addTo(mymap);

        arrayPuntos = [];

    } catch (error) {
      console.log(error);
    }

    contar++;

  }
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZWJlbmFuY2lvIiwiYSI6ImNraHYzMjZncDE5aWoyem95cm51Z29iaHoifQ.sTpBM8Vv-XaHdM3r9Qc-fQ'
    }).addTo(mymap);

    return mymap;
}

function mapaPuntosGeograficos(ArrayVisitas,ArrBase64){
  var  mymap = null;
// debugger;
if(ArrayVisitas.length==0){
  return;
}
  var LogLat = [
    ArrayVisitas[0].listaCoordenadas[0].latitud,
    ArrayVisitas[0].listaCoordenadas[0].longitud
  ];

  mymap = L.map('mapid').setView(LogLat, 15);



  var contar = 1;
    for (let index = 0; index < ArrayVisitas.length; index++) {
      const element = ArrayVisitas[index];
      try {
          var slideshowContent = "";
          for (var i = 0 in element.listaCoordenadas)
          {
            var myIcon = L.icon({
              iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAFgUlEQVR4Aa1XA5BjWRTN2oW17d3YaZtr2962HUzbDNpjszW24mRt28p47v7zq/bXZtrp/lWnXr337j3nPCe85NcypgSFdugCpW5YoDAMRaIMqRi6aKq5E3YqDQO3qAwjVWrD8Ncq/RBpykd8oZUb/kaJutow8r1aP9II0WmLKLIsJyv1w/kqw9Ch2MYdB++12Onxee/QMwvf4/Dk/Lfp/i4nxTXtOoQ4pW5Aj7wpici1A9erdAN2OH64x8OSP9j3Ft3b7aWkTg/Fm91siTra0f9on5sQr9INejH6CUUUpavjFNq1B+Oadhxmnfa8RfEmN8VNAsQhPqF55xHkMzz3jSmChWU6f7/XZKNH+9+hBLOHYozuKQPxyMPUKkrX/K0uWnfFaJGS1QPRtZsOPtr3NsW0uyh6NNCOkU3Yz+bXbT3I8G3xE5EXLXtCXbbqwCO9zPQYPRTZ5vIDXD7U+w7rFDEoUUf7ibHIR4y6bLVPXrz8JVZEql13trxwue/uDivd3fkWRbS6/IA2bID4uk0UpF1N8qLlbBlXs4Ee7HLTfV1j54APvODnSfOWBqtKVvjgLKzF5YdEk5ewRkGlK0i33Eofffc7HT56jD7/6U+qH3Cx7SBLNntH5YIPvODnyfIXZYRVDPqgHtLs5ABHD3YzLuespb7t79FY34DjMwrVrcTuwlT55YMPvOBnRrJ4VXTdNnYug5ucHLBjEpt30701A3Ts+HEa73u6dT3FNWwflY86eMHPk+Yu+i6pzUpRrW7SNDg5JHR4KapmM5Wv2E8Tfcb1HoqqHMHU+uWDD7zg54mz5/2BSnizi9T1Dg4QQXLToGNCkb6tb1NU+QAlGr1++eADrzhn/u8Q2YZhQVlZ5+CAOtqfbhmaUCS1ezNFVm2imDbPmPng5wmz+gwh+oHDce0eUtQ6OGDIyR0uUhUsoO3vfDmmgOezH0mZN59x7MBi++WDL1g/eEiU3avlidO671bkLfwbw5XV2P8Pzo0ydy4t2/0eu33xYSOMOD8hTf4CrBtGMSoXfPLchX+J0ruSePw3LZeK0juPJbYzrhkH0io7B3k164hiGvawhOKMLkrQLyVpZg8rHFW7E2uHOL888IBPlNZ1FPzstSJM694fWr6RwpvcJK60+0HCILTBzZLFNdtAzJaohze60T8qBzyh5ZuOg5e7uwQppofEmf2++DYvmySqGBuKaicF1blQjhuHdvCIMvp8whTTfZzI7RldpwtSzL+F1+wkdZ2TBOW2gIF88PBTzD/gpeREAMEbxnJcaJHNHrpzji0gQCS6hdkEeYt9DF/2qPcEC8RM28Hwmr3sdNyht00byAut2k3gufWNtgtOEOFGUwcXWNDbdNbpgBGxEvKkOQsxivJx33iow0Vw5S6SVTrpVq11ysA2Rp7gTfPfktc6zhtXBBC+adRLshf6sG2RfHPZ5EAc4sVZ83yCN00Fk/4kggu40ZTvIEm5g24qtU4KjBrx/BTTH8ifVASAG7gKrnWxJDcU7x8X6Ecczhm3o6YicvsLXWfh3Ch1W0k8x0nXF+0fFxgt4phz8QvypiwCCFKMqXCnqXExjq10beH+UUA7+nG6mdG/Pu0f3LgFcGrl2s0kNNjpmoJ9o4B29CMO8dMT4Q5ox8uitF6fqsrJOr8qnwNbRzv6hSnG5wP+64C7h9lp30hKNtKdWjtdkbuPA19nJ7Tz3zR/ibgARbhb4AlhavcBebmTHcFl2fvYEnW0ox9xMxKBS8btJ+KiEbq9zA4RthQXDhPa0T9TEe69gWupwc6uBUphquXgf+/FrIjweHQS4/pduMe5ERUMHUd9xv8ZR98CxkS4F2n3EUrUZ10EYNw7BWm9x1GiPssi3GgiGRDKWRYZfXlON+dfNbM+GgIwYdwAAAAASUVORK5CYII='
            })

            LogLat = [
              element.listaCoordenadas[i].latitud,
              element.listaCoordenadas[i].longitud
            ];

            var marker = L.marker(LogLat,{icon : myIcon});
            marker.addTo(mymap);

            marker.on('popupopen', function(e) {
              $('.carousel.carousel-slider').carousel({
                fullWidth: true
              });
            });

           var popupContent = '';
           popupContent += '<div class="carousel carousel-slider" style="width:300px; height: 200px">';
           for (var e = 0 in element.listaImagenes){
              popupContent += '<a class="carousel-item" href="javascript:void(0);"'+e+'!"><img width="250px" src="data:image/jpg;base64, '+  ArrBase64[index][e].archivoBytes+'"></a>';
           }
           popupContent += '</div>';
           marker.bindPopup(
            "<b>Visita "+contar+"</b><br>"+
            "Predio visitado el dia "+ formatDate(new Date(element.fecha_visita)) +"<br>"+
            "Cliente: " + element.primerNombre + " " + element.segundoNombre + " " + element.apellidoPaterno + " " + element.apellidoMaterno +"<br>" + 
            "Nombre de Predio: "+ element.nombrePredio +"<br>" + 
            "Dirección de Predio: "+ element.direccionPredio +"<br>" + 
            "Tipo de actividad: "+ actividad(element.actividad) +"<br>" + 
            "Comentario: "+ element.comentario +"<br><br>" + 
            popupContent
            ).openPopup();
          //  marker.bindPopup("<b>Visita "+contar+"</b><br>Predio visitado el dia "+ formatDate(new Date(element.fecha_visita)) +"<br>Todo conforme<br><br>"+popupContent).openPopup();
         break;
          }

          L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiZWJlbmFuY2lvIiwiYSI6ImNraHYzMjZncDE5aWoyem95cm51Z29iaHoifQ.sTpBM8Vv-XaHdM3r9Qc-fQ'
        }).addTo(mymap);

      } catch (error) {
        console.log(error);
      }
      contar++;


    }


 return mymap;
}

function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

function actividad(codigo) {
  switch (codigo) {
    case 1:
      return "Agrícola";
    default:
      return "Pecuaria";
  }
}
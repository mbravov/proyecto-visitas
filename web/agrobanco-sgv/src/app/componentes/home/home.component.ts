import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { RedirectService } from 'src/app/services/redirect.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  ArrModulosTotal : any[] = [];
  ArrModulos : any [] = [];
  ArrOpciones : any [] = [];
  loginStatus: any;

  ArrColores : any = [
    "teal",
    "red",
    "orange",
    "blue"
  ]

  constructor( private servicio: RedirectService ) { }

  ngOnInit(): void {

  this.ArrModulosTotal = this.servicio.modulos;
  console.log(this.ArrModulosTotal)
  this.ArrModulos = this.servicio.modulos.filter(x => x.TipoOpcion == 1);
 
  }
  filtrarOpcionesPorModulo(IdOpcion: string) {
    this.ArrOpciones = this.ArrModulosTotal.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return this.ArrOpciones;
  }
  filtrarOpcionesPorModuloSinOpcion(IdOpcion: string) {
    this.ArrOpciones = [];
    let opciones = this.ArrModulosTotal.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    if(opciones.length == 0){
      this.ArrOpciones = this.ArrModulosTotal.filter(x => x.IdOpcion = IdOpcion && x.TipoOpcion == 1);
    }

    return this.ArrOpciones;
  }

  filtrarTieneOpciones(IdOpcion: string)
  {
    this.ArrOpciones = this.ArrModulosTotal.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return this.ArrOpciones.length;
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Imei } from 'src/app/models/Imei.model';
import { DomService } from 'src/app/services/dom.service';
import { ImeiService } from 'src/app/services/imei.service';

@Component({
  selector: 'app-imei',
  templateUrl: './imei.component.html',
  styleUrls: ['./imei.component.css']
})
export class ImeiComponent implements OnInit {

  loader: boolean = false;
  ELEMENT_DATA! : Imei[];  
  displayedColumns: string[] = ["usuario", "codigo", "acciones"];
  listaCodigos = new MatTableDataSource<Imei>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  msjSuccess = ""

  constructor(
    private domService: DomService,
    private imeiService: ImeiService
  ) { }

  ngOnInit(): void {
    this.domService.HideSideBar();
    this.listaCodigos.paginator = this.paginator;
    this.cargarData();
  }

  cargarData() {
    this.loader = true;
    this.imeiService.listarImei().subscribe((res:any) => {
      this.listaCodigos.data = res as Imei[];
      this.loader = false;
    }, (err: any) => {
      this.loader = false;
    });
  }

  resetear(usuario: string) {
    this.loader = true;
    
    this.imeiService.resetearImei(usuario).subscribe((res:any) => {
      this.loader = false;
      this.cargarData();
    }, (err: any) => {
      this.loader = false;
    });
  }

}

import { Input } from '@angular/core';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Agencia } from 'src/app/models/Agencia.interface';
import { AgenciaModel } from 'src/app/models/Agencia.model';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';
import { RedirectService } from 'src/app/services/redirect.service';

declare var M: any;

@Component({
  selector: 'app-profesional-tecnico-item',
  templateUrl: './profesional-tecnico-item.component.html',
  styleUrls: ['./profesional-tecnico-item.component.css']
})
export class ProfesionalTecnicoItemComponent implements OnInit {
  formProfesionalTecnicoItem! : FormGroup;
  tituloModo! : string;
  formSubmitted : boolean = false;
  profesional! : ProfesionalTecnico;
  codigoProfesionalTecnico!:number;
  agencias: Agencia[] = []
  @Output() transactionEvent = new EventEmitter<{isSubmit: boolean, transaction: boolean, hasError: boolean}>();

  constructor(private profesionalService: ProfesionalTecnicoService, private _redirectService: RedirectService) {
  }

  ngOnInit(): void {
    
    this.cargaInicial();

    this.profesionalService.obtenerAgencias().subscribe((res: any)=>{
      
      res.response.forEach((item : AgenciaModel) => {
        this.agencias.push(item as Agencia);
      });   

        },(err: any)=>{
          console.log(err);                
        },()=>{
          setTimeout(() => {
            this.enableComboAgencia();
          }, 2000);        
        }
        );
    

    this.profesionalService.codigoProfesionalTecnico$.subscribe((codigo: number)=>{
      console.log("subscribe iniciado");
      this.codigoProfesionalTecnico = codigo;
      //this.activarModo();

      setTimeout(() => {        
        this.enableCombos();        
      }, 2000);

      this.activarModo();
    });
  }

  private cargaInicial(){       
    this.inicializarFormulario();
    this.enableCombos();
  }

  private activarModo() {
    
    if (this.codigoProfesionalTecnico  == 0) {
      this.tituloModo = "Registrar";      
      this.inicializarFormulario();      
    }

    if (this.codigoProfesionalTecnico  > 0) {
      this.tituloModo = "Actualizar";
      
      let filtro = new ProfesionalTecnicoModel();
      filtro.codigo = this.codigoProfesionalTecnico;

      this.obtenerProfesional(filtro);      
    }    
  }

  private enableCombos(){
    this.enableComboTipoDocumento();
    this.enableComboAgencia();
    this.enableComboEstado();
  }

  private enableComboTipoDocumento() {
    var elem = document.getElementById('cbo-tipo-documento');
    var instance = M.FormSelect.init(elem, {});    
  }

  private enableComboAgencia() {
    var elem = document.getElementById('cbo-agencia');
    var instance = M.FormSelect.init(elem, {});    
  }

  private enableComboEstado() {
    var elem = document.getElementById('cbo-estado');
    var instance = M.FormSelect.init(elem, {});    
  }

  private inicializarFormulario()
  {
    this.formProfesionalTecnicoItem = new FormGroup({
      codigo: new FormControl(this.codigoProfesionalTecnico),
      tipoDocumento : new FormControl("", [Validators.required]),
      numeroDocumento : new FormControl("",  { validators: [Validators.required] }),
      primerNombre : new FormControl("", [Validators.required]),
      segundoNombre : new FormControl(""),
      apellidoPaterno : new FormControl("", [Validators.required]),
      apellidoMaterno : new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      celular: new FormControl("", [Validators.required]),
      codigoAgencia: new FormControl("", [Validators.required]),
      usuarioWeb: new FormControl("", [Validators.required]),
      estado: new FormControl(1)
    });

  }

  public keyPressOnlyNumber(e:any){
    
    let charCode = (e.which) ? e.which : e.keyCode    
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {      
      return false;
    }
    
    return true;       
  }
  
  public onSubmit() {    
    this.formSubmitted = true;
    debugger;

    if(this.formProfesionalTecnicoItem.invalid)
    {      
      return;
    }
    
    this.transactionEvent.emit({isSubmit: true, transaction: true, hasError: false });
    
    this.profesional = new ProfesionalTecnicoModel();
    let params = this.formProfesionalTecnicoItem.value;

    this.profesional.codigo = parseInt(params.codigo);
    this.profesional.tipoDocumento = parseInt(params.tipoDocumento);
    this.profesional.numeroDocumento = params.numeroDocumento;
    this.profesional.codigoAgencia = parseInt(params.codigoAgencia);
    this.profesional.primerNombre = params.primerNombre;
    this.profesional.segundoNombre = params.segundoNombre == null?"":params.segundoNombre;
    this.profesional.apellidoPaterno = params.apellidoPaterno;
    this.profesional.apellidoMaterno = params.apellidoMaterno;
    this.profesional.email = params.email;
    this.profesional.celular = params.celular;
    this.profesional.estado = parseInt(params.estado);
    this.profesional.usuarioWeb = params.usuarioWeb;
    this.profesional.usuarioCreacion = this._redirectService.leerUsuarioWeb();

    if (this.profesional.codigo > 0) {
      this.actualizar();
    }

    if (this.profesional.codigo == 0) {
      this.registrar();
    }
  }

  public registrar()
  {
    this.profesionalService.registrarProfesional(this.profesional).subscribe((res:any)=>{

      console.log(res);

    }, (error)=>{
      console.log(error);
      this.transactionEvent.emit({isSubmit: true, transaction: false, hasError : true });
    }, ()=>{
      this.transactionEvent.emit({isSubmit: true, transaction: false, hasError : false });
    });
  }

  public actualizar()
  {
    this.profesionalService.actualizarProfesional(this.profesional).subscribe((res:any)=>{

      console.log(res);

    }, (error)=>{
      console.log(error);
      this.transactionEvent.emit({isSubmit: true, transaction: false, hasError : true });
    }, ()=>{
      this.transactionEvent.emit({isSubmit: true, transaction: false, hasError : false });
    });
  }

  public obtenerProfesional(filtro : ProfesionalTecnicoModel){
    this.transactionEvent.emit({isSubmit: false, transaction: true, hasError: false });

    this.profesionalService.obtenerProfesional(filtro).subscribe((res:any)=>{
      console.log(res);      
            
      this.formProfesionalTecnicoItem.patchValue({
        codigo: res.response.codigo,
        tipoDocumento : res.response.tipoDocumento,
        numeroDocumento : res.response.numeroDocumento,
        primerNombre : res.response.primerNombre,
        segundoNombre : res.response.segundoNombre,
        apellidoPaterno : res.response.apellidoPaterno,
        apellidoMaterno : res.response.apellidoMaterno,
        email : res.response.email,
        celular : res.response.celular,
        codigoAgencia : res.response.codigoAgencia,
        usuarioWeb: res.response.usuarioWeb,
        estado : res.response.estado
      });
setTimeout(() => {
  this.enableCombos();
}, 1000);

    },(error)=>{
      console.log(error);
      this.transactionEvent.emit({isSubmit: false, transaction: false, hasError: true });
    },()=>{
      this.transactionEvent.emit({isSubmit: false, transaction: false, hasError: false });
    });
  }
  

}

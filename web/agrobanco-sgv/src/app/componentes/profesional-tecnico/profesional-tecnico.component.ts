import { CompileShallowModuleMetadata } from '@angular/compiler';
import { ViewChild, Component, OnInit } from '@angular/core';
import {ProfesionalTecnicoService} from '../../services/profesional-tecnico.service';

declare var $:any;
declare var M:any;

import { Agencia } from 'src/app/models/Agencia.interface';
import {AgenciaModel} from '../../models/Agencia.model';
import { DomService } from 'src/app/services/dom.service';
import { forkJoin, of } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { catchError, map } from 'rxjs/operators';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';

/***  ANGULAR MATERIAL ***/
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ParametrosService } from 'src/app/services/parametros.service';
import { Asociacion } from 'src/app/models/Asociacion.model';

/***  ANGULAR MATERIAL ***/

const tamanioPagina : number = 10;


@Component({
  selector: 'app-profesional-tecnico',
  templateUrl: './profesional-tecnico.component.html',
  styleUrls: ['./profesional-tecnico.component.css']
})
export class ProfesionalTecnicoComponent implements OnInit {
  
  busquedaFiltro : FormGroup;
  codigoProfesionalTecnicoActual : number = 0;
  //listaProfesionalTecnico : ProfesionalTecnicoModel[];
  asociaciones: Asociacion[] = [];
  agencias: Agencia[] = [];
  peticionesTerminadas: boolean = true;
  totalRegistrosBusqueda : number;
  listaPaginas : any[];
  paginaActual : number;
  ELEMENT_DATA! : ProfesionalTecnico[];  
  displayedColumns: string[] = ['numeroDocumento', "nombres", "agencia", "usuarioWeb", "estado", "editar"];
  listaProfesionalTecnico = new MatTableDataSource<ProfesionalTecnico>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  msjRegisterSuccess : string = "Se registró correctamente.";
  msjUpdateSuccess : string = "Se actualizó correctamente";
  msjSuccess: string = "";

  constructor(private parametroService: ParametrosService, private domService: DomService, private profesionalService : ProfesionalTecnicoService, private router: Router) {    
    this.busquedaFiltro = new FormGroup({});
    this.totalRegistrosBusqueda = 0;
    this.listaPaginas = [];
    this.paginaActual = 1;
    
  }

  ngOnInit(): void {   
    this.domService.HideSideBar();
    this.enableSelectAgencia();
    this.enableSelectAsociacion();
    this.inicializarFormulario();
    this.inicializarFormularioItem();  
    this.cargaInicial();
    
    this.listaProfesionalTecnico.paginator = this.paginator;
  }

  private enableSelectAgencia() {
    var elem = document.getElementById('select_agencia');
    var instance = M.FormSelect.init(elem, {});    
  }

  private enableSelectAsociacion() {
    var elem = document.getElementById('select_asociacion');
    var instance = M.FormSelect.init(elem, {});
  }

  public inicializarFormularioItem()
  {
    var elem = document.getElementById('profesional-tecnico-modal');
    M.Modal.init(elem, {});
  }

  public openModalItem(codigo: number)
  {        
    
    this.profesionalService.codigoProfesionalTecnico$.emit(codigo);
    
    if (codigo == 0) {
      this.msjSuccess = this.msjRegisterSuccess;
    }

    if (codigo > 0) {
      this.msjSuccess = this.msjUpdateSuccess;
    }
    

    var elem = document.getElementById('profesional-tecnico-modal');    
    var instance = M.Modal.getInstance(elem);
    
    instance.open();
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        // location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }


  private loadAgencias(result: any){        
    result.response.forEach((item : AgenciaModel) => {
      this.agencias.push(item as Agencia);
    });    
  }

  private loadAsociaciones(result:any){
    this.asociaciones = result[0];
   
  }

  private inicializarFormulario()
  {
    this.busquedaFiltro = new FormGroup({
      numeroDocumento : new FormControl(''),
      codigoAgencia : new FormControl(0),
      codigoAsociacion : new FormControl(0),
      pagina : new FormControl(0)      
    });
  }
  
  private cargaInicial(){
    this.peticionesTerminadas = false;
    let filters = this.busquedaFiltro.value;
    
    forkJoin([this.profesionalService.obtenerAgencias(), this.profesionalService.buscarProfesional(filters)])
      .subscribe((res)=>{
        let resAgencias = res[0];
        let reslista : any = res[1];

        this.loadAgencias(resAgencias);

        this.listaProfesionalTecnico.data = reslista.response.lista as ProfesionalTecnico[];

      },(error)=>{
        console.log(error);      
        this.peticionesTerminadas = true;
        this.openModalError();          
      },()=>{
        setTimeout(() => {
          this.enableSelectAgencia();
          this.enableSelectAsociacion();
          this.peticionesTerminadas = true;
        }, 1500);        
      }
      );    
  }

  agenciaSeleccionada2(selector: any) {
    let agencia = selector.target.value.split(" ")[1];;
    this.peticionesTerminadas = false;

    forkJoin([
      this.parametroService.obtenerAsociaciones(Number(agencia))
    ]).subscribe((res: any) => {
      this.loadAsociaciones(res);
      
    }, (err: any) => {
      console.log(err);      
      this.peticionesTerminadas = true;
      this.openModalError();  
    }, () => {
      setTimeout(() => {
        this.enableSelectAgencia();
        this.enableSelectAsociacion();
        this.peticionesTerminadas = true;
      }, 1500);
    });
  }

  buscarListaProfesional(){    
    this.peticionesTerminadas = false;
    let params = this.busquedaFiltro.value;    
    console.log(params);
    
    this.profesionalService.buscarProfesional(params).subscribe((res:any)=> {       
      
      this.listaProfesionalTecnico.data = res.response.lista as ProfesionalTecnico[];
      
    },(error)=>{
      console.log(error);
      this.peticionesTerminadas = true;
      this.openModalError();
    },()=>{
      this.peticionesTerminadas = true;
    });
  }

  onTransactionChild($event: any)
  {
    this.peticionesTerminadas = !$event.transaction;

    if ($event.hasError) {
      this.openModalError();
    }
    else{

      if($event.transaction == false && $event.isSubmit == true)
      {
        this.openModalSuccess();
      }
    }
    
    
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { EUsuarioModel } from 'src/app/models/EUsuario.model';
import { RedirectService } from '../../../services/redirect.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  private eusuario  = new EUsuarioModel();

  // tslint:disable-next-line: variable-name
  constructor( private servicio: RedirectService, private router: ActivatedRoute, private _router: Router ) { }

  token: any;

  ngOnInit(): void {

    this.router.queryParamMap
    .subscribe((result: ParamMap) => {
      console.log(result)
      // captura parametros y setea el objeto 'eusuario'
      const pToken: string | null = result.get('Token');
      const pIdPerfil: string | null = result.get('IdPerfil');
      
      this.eusuario.Token =  (pToken == null) ? '' : pToken;
      this.eusuario.idPerfil = (pIdPerfil == null) ? '' : pIdPerfil;

      // guarda los parametros en local storage
      this.servicio.guardarToken(this.eusuario);
      this.servicio.guardaridPerfil(this.eusuario);

    });

    this.servicio.obtenerUsuario( this.eusuario )
    .subscribe( data => {
      console.log(data);
      this.eusuario.UsuarioWeb = data.body.vUsuarioWeb;
      this.servicio.guardarUsuarioWeb(this.eusuario);
      this.servicio.guardaridPerfil(this.eusuario);
      this.servicio.guardarNombresUsuarioWeb(data.body.vNombre);
      this.servicio.guardarAgencia(data.body.vAgenciaAsignada);
      this.servicio.guardarFuncionario(data.body.vCodFuncionario);

      localStorage.setItem('CodAgenciaAsignada',data.body.vAgenciaAsignada)
      this.servicio.generarTokenSGV(this.eusuario).subscribe((resp:any) => {
        localStorage.setItem('TokenSGV',resp.headers.get('authorization'));
        this._router.navigate(['/home']);
      });
     
    });

    

  }

}

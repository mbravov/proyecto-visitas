import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Confirm } from 'src/app/models/Util.model';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmComponent implements OnInit {

  @Input() content: string = "";
  @Output() confirmEvent = new EventEmitter<Confirm>();

  constructor() { }

  ngOnInit(): void {
  }

  cancel() {
    this.confirmEvent.emit(Confirm.Cancel);
  }

  confirm() {
    this.confirmEvent.emit(Confirm.Agree);
  }

}

import { Component, OnInit } from '@angular/core';
import { DomService } from '../../../services/dom.service';
import { RedirectService } from '../../../services/redirect.service';
//declare var $:any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  nombreUsuario: any;
  
  constructor(private domService : DomService, public redirectService: RedirectService) {

    this.nombreUsuario = this.redirectService.leerNombresUsuarioWeb();

   } 

  ngOnInit(): void {

  }

  MenuOpenClick(): void{
    this.domService.ShowSideBar();
  }

}

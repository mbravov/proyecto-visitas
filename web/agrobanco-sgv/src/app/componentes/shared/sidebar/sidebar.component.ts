import { Component, OnInit } from '@angular/core';
import { ITS_JUST_ANGULAR } from '@angular/core/src/r3_symbols';
import { DomService } from '../../../services/dom.service';
import { RedirectService } from '../../../services/redirect.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  ListaOpciones: any[] = [];

  constructor(
    public domService: DomService,
    public redirectService: RedirectService
  ) {
    $('body').on('click', '.sidenav-overlay', this.domService.HideSideBar);
  }

  ngOnInit(): void {
    console.log(this.redirectService);
    this.inicializarControles();
    this.inicializarEventos();
  }

  inicializarControles(): void {
    // $('.sidenav').sidenav();
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});
    // $('.collapsible').collapsible();
  }

  sideNavOverlayClick(): void {
    this.domService.HideSideBar();
  }

  inicializarEventos() {
    //$("body").on("click", ".sidenav-overlay", this.sideNavOverlayClick);
  }

  clickSlideOut() {
    this.domService.ShowSideBar();
  }

  filtrarOpcionesPorModulo(IdOpcion: string) {
    this.ListaOpciones = this.redirectService.modulos.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return this.ListaOpciones;
  }

  filtrarTieneOpciones(IdOpcion: string)
  {
    this.ListaOpciones = this.redirectService.modulos.filter(
      (x) => x.IdRelacion === IdOpcion
    );
    return this.ListaOpciones.length;
  }

  obtenerRutaOpcion(Opcion:any){
    return Opcion.Controller +'/'+ Opcion.Action;
  }
}

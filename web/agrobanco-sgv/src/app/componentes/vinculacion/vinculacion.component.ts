import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { VinculacionVisita } from 'src/app/models/VinculacionVisita.interface';
import { AgroperuService } from 'src/app/services/agroperu.service';
import { DomService } from 'src/app/services/dom.service';
import { VisitaService } from 'src/app/services/visita.service';
import { PdfComponent } from '../visitas/pdf/pdf.component';
declare var $:any;
declare var M:any;
@Component({
  selector: 'app-vinculacion',
  templateUrl: './vinculacion.component.html',
  styleUrls: ['./vinculacion.component.css']
})
export class VinculacionComponent implements OnInit {
  ELEMENT_DATA!: VinculacionVisita[];
  displayedColumns = ['cultivo', 'solcredito', 'dni', 'nombre', 'fechaaprobacion', 'horaaprobacion', 'usuariovisita', 'verpdf', 'vincular'];
  listaVinculacionVisita = new MatTableDataSource<VinculacionVisita>(this.ELEMENT_DATA);
  TokenAgroPeru!: string;
  NroDocumento!: string;
  NroSolicititud!: string;
  msg!: String;
  msjSuccess="Vinculación realizada con éxito.";
  public loader: boolean = true;

  
  constructor(
    private _visitaService: VisitaService,
    private _agroPeruService: AgroperuService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private domService: DomService) { }

  ngOnInit(): void {
    this.msg = "";
    this.TokenAgroPeru = this.route.snapshot.queryParams.IdToken;
    this.NroDocumento = this.route.snapshot.queryParams.NumeroDocumento;
    this.NroSolicititud = this.route.snapshot.queryParams.NumeroSolicitud;

    this.domService.RemoveMenu();

    if (this.TokenAgroPeru != undefined) {
      this._agroPeruService.ValidarTokenAgroPeru(this.TokenAgroPeru).subscribe((data: any) => {

        if(data != null){
        if (data.estado == 1) {
          forkJoin([
            this.obtenerVinculacionVisitas(this.NroDocumento),
            this.desactivarTokenAgroPeru(this.TokenAgroPeru)
          ]).subscribe((data: any) => {
            console.log(data);
            this.listaVinculacionVisita = data[0];
            this.loader = false;
          });
        } else if (data.estado == 0) {
          this.obtenerVinculacionVisitas(this.NroDocumento).subscribe((data: any) => {
            console.log(data)
            this.listaVinculacionVisita = data;
            this.msg = "El token se encuentra inactivo o no es válido, no podrá realizar vinculaciones por favor vuelva a vincular desde la aplicación AGROPERU."
            this.loader = false;
          });
        }
      }else{
        this.obtenerVinculacionVisitas(this.NroDocumento).subscribe((data: any) => {
          this.listaVinculacionVisita = data;
          this.msg = "El token se encuentra inactivo o no es válido, no podrá realizar vinculaciones por favor vuelva a vincular desde la aplicación AGROPERU."
          this.loader = false;
        });
      }
      })
    }else{
      this.loader = false;
      this.msg = "No hay información por mostrar"
    }

  }

  obtenerVinculacionVisitas(documento: string ) {
    return this._visitaService.obtenerVisitasVinculacion(documento);
  }

  desactivarTokenAgroPeru(token: string) {
    return this._agroPeruService.DesactivarTokenAgroPeru(token);
  }

  AsignarSolicitudDeCredito(codigoVisita:number){
    this.loader=true;
    this._visitaService.asignacionSolicitudCredito(codigoVisita,this.NroSolicititud).subscribe((data:any)=> {
      console.log(data);
      this.obtenerVinculacionVisitas(this.NroDocumento).subscribe((data:any)=> {
        this.listaVinculacionVisita = data;
        this.loader = false;
        this.openModalSuccess();
      });
    }, (err: any) => {
      this.loader = false;
      this.openModalError();
      console.log(err)
   });
  }

  VerPDF(codigo:number){
    const dialogRef = this.dialog.open(PdfComponent);
    dialogRef.componentInstance.CodVisita = codigo;
    dialogRef.componentInstance.origen = 'Vinculacion';
  //  dialogRef.componentInstance.profesional = nombre1 + ' ' + nombre2 + ' ' + nombre3 + ' ' + nombre4;
    dialogRef.componentInstance.loading.subscribe((loading: boolean) => {
      this.loader = loading;
      if (!loading) {
        dialogRef.close();
      }
    });
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        

      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }
  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){     
       //   location.reload();   
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }
}


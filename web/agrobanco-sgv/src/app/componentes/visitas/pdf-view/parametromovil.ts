export abstract class ParametroMovil{

static RegimenConduccion:any = [
    "vacio",
    "Propio",
    "Posesionario",
    "Arrendado",
    "Mixto"
];

static UbicacionRiesgo:any =  [
      "vacio",
      "A orilla del rio",
      "Orilla del precipicio",
      "Suelo Salino/Sódico",
      "Huaycos",
      "Prob. Disp. de agua",
      "Sin exposición"
 ];
  
 static EstadoCampo:any =  [
    "vacio",
    "Otro cultivo",
    "En descanso",
    "En preparación",
    "Regado",
    "Sembrado"
  ];

  static  TipoRiego:any =  [
    "vacio",
    "Secano",
    "Gravedad",
    "Tecnificado"
  ];

  static DisponibilidadAgua1:any =  [
    "vacio",
    "Permanente",
    "Semanal / quincenal",
    "Mensual"
  ];

  static Pendiente:any =  [
    "vacio",
    "Plana",
    "Moderada",
    "Pronunciada"
  ];

  static TipoSuelo:any =  [
    "vacio",
    "Arenoso",
    "Franco",
    "Arcilloso"
  ];

  static AccesibilidadPredio:any =  [
    "vacio",
    "Fácil",
    "Regular",
    "Difícil"
  ];

  static Peso:any =  [
    "-Medida-",
    "kilogramo",
    "gramo",
    "tonelada"
  ];

  static TipoSemilla:any =  [
    "vacio",
    "Certificada",
    "Selección"
  ];

  static TipoAlimentacion:any =  [
    "vacio",
    "Pastos naturales",
    "Pastos manejados",
    "Alimento concentrado",
    "Mixtos"
  ];

  static Manejo:any =  [
    "vacio",
    "Extensiva",
    "Semi extensiva",
    "Intensiva"
  ];

  static FuenteAgua:any =  [
    "vacio",
    "Canal de riego",
    "Pozo",
    "Secano"
  ];

  static DisponibilidadAgua2:any =  [
    "vacio",
    "Permanente",
    "Semi permanente",
    "Estacional"
  ];

  static Crianza:any =  [
    "-Seleccione-",
    "Vacuno",
    "Camélido",
    "Porcino"
  ];

  static TipoTecnologia:any =  [
    "vacio",
    "Medio",
    "Alto",
    "Tradicional",
  ];

  static Manejo2:any =  [
    "vacio",
    "Fácil",
    "Regular",
    "Difícil"
  ];

  static TipoActividad:any =  [
    "Ninguno",
    "Agrícola",
    "Pecuaria"
  ];

  static PersonaPresente:any =  [
    "Ninguno",
    "Productor",
    "Familiar"
  ];

  static MercadoAcceso:any = [
    "Ninguno",
    "Local",
    "Regional",
    "Provincial",
    "Distrital"
];
}
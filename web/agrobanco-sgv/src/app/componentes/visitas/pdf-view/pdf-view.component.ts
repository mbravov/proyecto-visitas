import { Component, EventEmitter, Input, OnInit, Output, Pipe } from '@angular/core';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { VisitaService } from 'src/app/services/visita.service';
import { SafePipe } from 'src/app/pipes/safe.pipe';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { ParametroMovil } from './parametromovil';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { forkJoin } from 'rxjs';
declare var M: any;

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;
@Component({
  selector: 'app-pdf-view',
  templateUrl: './pdf-view.component.html',
  styleUrls: ['./pdf-view.component.css'],
  providers: [SafePipe]
})
export class PdfViewComponent implements OnInit {
  @Input() CodigoVisita!:number;
  @Output() Comentario = new EventEmitter<any>();

  peticionesTerminadas:boolean = false;
  producerPresented = true;
  activity = 0;
  loadImage = false;
  creating = false;
  dataVisorVisita: any;
  dataVisorMapa: any;
  dataVisorVisitaImagenesPredios:any;
  dataVisorVisitaImagenesPrediosComp:any;
  dataVisorVisitaImagenesFirmaConyuge:any;
  dataVisorVisitaImagenesFirmaTitular:any;
  listValues:any = ParametroMovil;
  codlaserficheAnidadoFotosPredio: string = "";
  codlaserficheAnidadoFotosPredioComp: string = "";

  constructor(private visitaService: VisitaService, public sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    //this.CodigoVisita = 142;
    this.visitaService.VisorVisita(this.CodigoVisita).subscribe((data: any) => {
      console.log("visita", data.response);
      if (data.response.comentario !== null) {
         this.Comentario.emit(data.response.comentario);
      }
      
      this.dataVisorVisita = data.response;
      console.log(this.dataVisorVisita);
      this.dataVisorVisita.listaImagenes.forEach((element:any) => {
         
         if (element.tipoImagen === 1) {
            this.codlaserficheAnidadoFotosPredio = this.codlaserficheAnidadoFotosPredio + "" + element.archivoByte +"|";
         } else {
            this.codlaserficheAnidadoFotosPredioComp = this.codlaserficheAnidadoFotosPredioComp + "" + element.archivoByte +"|";
         }
      });
      
      this.codlaserficheAnidadoFotosPredio = this.codlaserficheAnidadoFotosPredio.slice(0,-1);
      if (this.codlaserficheAnidadoFotosPredioComp.length > 0) {
         this.codlaserficheAnidadoFotosPredioComp = this.codlaserficheAnidadoFotosPredioComp.slice(0,-1);
      }

      console.log(this.codlaserficheAnidadoFotosPredio)
      forkJoin([this.ObtenerImagenes(this.codlaserficheAnidadoFotosPredio)]).subscribe((data:any) =>{
         console.log("Primer join", data);
         if (data[0].response !== null) {
            this.dataVisorVisitaImagenesPredios = data[0].response;
         }

         console.log(this.dataVisorVisita.capturaMapa)
         console.log(this.codlaserficheAnidadoFotosPredioComp)
         forkJoin([
            this.ObtenerImagenes(this.dataVisorVisita.capturaMapa),
            this.ObtenerImagenes(this.codlaserficheAnidadoFotosPredioComp)
         ]).subscribe((data:any) => {
            console.log("segundo join", data);
            
            // if(data[0].length > 0){
            //    if (data[0].response !== null) {
            //       this.dataVisorMapa = data[0].response[0].archivoBytes; 
            //    } 
            // }
            try {
               this.dataVisorMapa = data[0].response[0].archivoBytes; 
            } catch (error) {
               console.error("No hay mapa");
            }

            try {
               this.dataVisorVisitaImagenesPrediosComp = data[1].response;
            } catch (error) {
               console.error("No hay imágenes complementarias");
            }
            console.log(this.dataVisorVisita.firmaTitular)
            console.log(this.dataVisorVisita.firmaConyugue)

            forkJoin([  
               this.ObtenerImagenes(this.dataVisorVisita.firmaTitular),
               this.ObtenerImagenes(this.dataVisorVisita.firmaConyugue)]).subscribe((dataF:any) => {
                  console.log("Tercer join", dataF);
                  if (dataF[0].response.length != 0) {
                     this.dataVisorVisitaImagenesFirmaTitular = dataF[0].response[0].archivoBytes; 
                  }
                  // this.dataVisorVisitaImagenesFirmaTitular = dataF[0].response[0].archivoBytes;
                  if (dataF[1].response.length != 0) {
                     this.dataVisorVisitaImagenesFirmaConyuge = dataF[1].response[0].archivoBytes; 
                  } 
                  this.peticionesTerminadas = true;
                  this.creating=true;  
               },
               (err) => {
                  this.peticionesTerminadas = true;
                  this.openModalError();
                  console.log(err)
               });
         }, (err: any) => {
            this.peticionesTerminadas = true;
            this.openModalError();
            console.log(err)
         });
         
      }, (err: any) => {
         this.peticionesTerminadas = true;
         this.openModalError();
         console.log(err)
      });

      // forkJoin([
      //    this.ObtenerImagenes(this.dataVisorVisita.capturaMapa),
      //    this.ObtenerImagenes(this.codlaserficheAnidadoFotosPredio),
      //    this.ObtenerImagenes(this.codlaserficheAnidadoFotosPredioComp),
      // ]).subscribe((data:any) => {
      //    console.log(data);
         
      //    if (data[0].response !== null) {
      //       this.dataVisorMapa = data[0].response[0].archivoBytes; 
      //    } 
         
      //    if (data[2].response !== null) {
      //       this.dataVisorVisitaImagenesPrediosComp = data[2].response;
      //    }
      //    forkJoin([  
      //       this.ObtenerImagenes(this.dataVisorVisita.firmaTitular),
      //       this.ObtenerImagenes(this.dataVisorVisita.firmaConyugue)]).subscribe((dataF:any) => {
      //          if (dataF[0].response !== null) {
      //             this.dataVisorVisitaImagenesFirmaTitular = dataF[0].response[0].archivoBytes; 
      //          }
      //          // this.dataVisorVisitaImagenesFirmaTitular = dataF[0].response[0].archivoBytes;
      //          if (dataF[1].response !== null) {
      //             this.dataVisorVisitaImagenesFirmaConyuge = dataF[1].response[0].archivoBytes; 
      //          } 
      //          this.peticionesTerminadas = true;
      //          this.creating=true;  
      //       },
      //       (err) => {
      //          this.peticionesTerminadas = true;
      //          this.openModalError();
      //          console.log(err)
      //       });
      // },
      // (err) => {
      //    this.peticionesTerminadas = true;
      //    this.openModalError();
      //    console.log(err)});
    }, (err: any) => {
         this.peticionesTerminadas = true;
         this.openModalError();
         console.log(err)
      });
  }

  
  ObtenerImagenes(codigosLaserfiche : any){
     return this.visitaService.obtenerimagenesLista(codigosLaserfiche);
  }

  dowloadPdf(profesional: string) {
   
    //let url = UrlBase_SGVAPI+"/visita/obtenerimagenes?coddocumentolf=";
    let data = this.dataVisorVisita;
    this.creating = true;
    var doc = new jspdf.jsPDF();

   doc.setTextColor(9, 125, 29);
   doc.setFontSize(20);
   doc.setFont("courier", "bold");
   doc.text("Informe de Visita", 105, 10, {
      align: 'center'
   });

   doc.setDrawColor(180, 180, 180); // draw gray lines
   doc.line(2, 22, 206, 22); // divider

   doc.setFontSize(14);
   doc.setFont("courier", "bold");
   doc.text("Datos del Cliente", 2, 28);

   if(profesional != undefined){
      doc.setTextColor(0, 0, 0);
      doc.text("Profesional técnico:", 2, 18);
      doc.setFont("courier", "normal");
      doc.text(profesional, 66, 18);
   }


   doc.setFontSize(12);
   doc.setFont("courier", "bold");
   doc.text("Número de Documento:", 5, 36);
   doc.text("Nombres: ", 110, 36);
   doc.text("Apellidos: ", 5, 52);

   doc.setFont("courier", "normal");
   doc.text(data.numDocumentoCliente, 5, 42);
   let segundo = (data.segundoNombrecliente === null) ? "" : data.segundoNombrecliente;
   doc.text(data.primerNombrecliente + " " + segundo, 110, 42);
   doc.text(data.apellidoPaternoCliente + " " + data.apellidoMaternoCliente, 5, 58);

   doc.line(2, 66, 206, 66); // divider

   doc.setTextColor(9, 125, 29);
   doc.setFontSize(14);
   doc.setFont("courier", "bold");
   doc.text("Datos del Predio", 2, 72);

   doc.setTextColor(0, 0, 0);
   doc.setFontSize(12);
   doc.setFont("courier", "bold");
   doc.text("Nombre:", 5, 84);
   doc.text("Tipo de Actividad:", 110, 84);
   doc.text("Dirección:", 5, 100);
   // doc.text("Comentarios:", 5, 116)

   doc.setFont("courier", "normal");
   doc.text(data.nombrePredio, 5, 90);
   doc.text(ParametroMovil.TipoActividad[data.tipoActividad], 110, 90);
   doc.text(data.direccionPredio, 5, 106, {
      maxWidth: 200,
      align: 'justify'
   });
   // doc.text(data.comentPrediosColindantes, 47, 126, {
   //    maxWidth: 148,
   //    align: 'justify'
   // });

   doc.line(2, 114, 206, 114); // divider

   doc.setTextColor(9, 125, 29);
   doc.setFontSize(14);
   doc.setFont("courier", "bold");
   doc.text("Datos generales y del productor", 2, 120);

   doc.setTextColor(0, 0, 0);
   doc.setFontSize(12);
   doc.setFont("courier", "bold");
   doc.text("Fecha inicio visita:", 5, 132);
   doc.text("Fecha fin visita:", 110, 132);
   doc.text("Agencia:", 5, 146);
   doc.text("Persona presente:", 110, 146);

   if (data.personaPresente == 2) {
      doc.text("Vínculo familiar:", 5, 160);
      doc.text("Nombre familiar:", 110, 160);
      doc.text("Celular:", 5, 174);
      doc.text("Mercado de acceso y potenciales:", 110, 174);
      doc.text("Es área protegida:", 5, 192);
   } else {
      doc.text("Celular:", 5, 160);
      doc.text("Mercado de acceso y potenciales:", 110, 160);
      doc.text("Es área protegida:", 5, 174);
   }

   doc.setFont("courier", "normal");
   doc.text(data.fechaInicioVisita.replace('T', ' '), 5, 138);
   doc.text(data.fechaFinVisita.replace('T', ' '), 110, 138);
   doc.text(data.agenciaDescripcion, 5, 152);
   doc.text(ParametroMovil.PersonaPresente[data.personaPresente], 110, 152);
   if (data.personaPresente == 2) {
      doc.text(data.vinculoFamiliar, 5, 166);
      doc.text(data.nombreFamiliar, 110, 166);
      doc.text(data.numCelFamiliar.toString(), 5, 180);
      doc.text(ParametroMovil.MercadoAcceso[data.mercadoAcceso], 110, 180);
      doc.text(data.areaProtegida == 1 ?"Sí":"No", 5, 198);   
   } else {
      doc.text(data.numCelFamiliar.toString(), 5, 166);
      doc.text(ParametroMovil.MercadoAcceso[data.mercadoAcceso], 110, 166);
      doc.text(data.areaProtegida == 1 ?"Sí":"No", 5, 180);  
   }

   doc.line(2, 204, 206, 204); // divider
   
   doc.setTextColor(9, 125, 29);
   doc.setFontSize(14);
   doc.setFont("courier", "bold");
   doc.text("Detalle de la visita", 2, 210);

   if (data.tipoActividad == 1) {
      doc.setFont("courier", "normal");
      doc.text("Datos del terreno", 4, 220);

      doc.setTextColor(0, 0, 0);
      doc.setFontSize(12);
      doc.setFont("courier", "bold");
      doc.text("Número de parcelas:", 5, 228);
      doc.text("Hectáreas sembradas: ", 110, 228);
      doc.text("Unidad catastral: ", 5, 240);
      doc.text("Hectáreas a financiar: ", 110, 240);
      doc.text("Hectáreas totales: ", 5, 252);
      doc.text("Régimen de Tenencia: ", 110, 252);
      doc.text("Ubicación / Exp. al riesgo", 5, 264);
      doc.text("Estado del campo: ", 110, 264);

      doc.setFont("courier", "normal");
      doc.text(data.numParcelas.toString(), 5, 234);
      doc.text(data.numHectareasSembradas.toString(), 110, 234);
      doc.text(data.unidadCatastral, 5, 246);
      doc.text(data.numHectareasFinanciar.toString(), 110, 246);
      doc.text(data.numHectareasTotales.toString(), 5, 258);
      doc.text(ParametroMovil.RegimenConduccion[data.regimenTenencia], 110, 258);
      doc.text(ParametroMovil.UbicacionRiesgo[data.tipoUbicacion], 5, 270);
      doc.text(ParametroMovil.EstadoCampo[data.estadoCampo], 110, 270);

   } else if (data.tipoActividad == 2) {
      doc.setFont("courier", "normal");
      doc.text("Datos de la actividad", 4, 220);

      doc.setTextColor(0, 0, 0);
      doc.setFontSize(12);
      doc.setFont("courier", "bold");
      doc.text("Unidad a financiar:", 5, 234);
      doc.text("Unidades a financiar: ", 110, 234);
      doc.text("Unidades totales: ", 5, 250);
      doc.text("Unidades productivas: ", 110, 250);
      doc.text("Tipo de alimentación: ", 5, 266);
      doc.text("Tipo manejo: ", 110, 266);
      doc.text("Fuente de agua: ", 5, 282);
      doc.text("Disponibilidad de agua: ", 110, 282);

      doc.setFont("courier", "normal");
      doc.text(data.unidadFinanciar, 5, 240);
      doc.text(data.numUnidadesFinanciar.toString(), 110, 240);
      doc.text(data.numTotalUnidades.toString(), 5, 256);
      doc.text(data.unidadesProductivas.toString(), 110, 256);
      doc.text(ParametroMovil.TipoAlimentacion[data.tipoAlimentacion], 5, 272);
      doc.text(ParametroMovil.Manejo2[data.tipoManejo], 110, 272);
      doc.text(ParametroMovil.FuenteAgua[data.tipoFuenteAgua], 5, 288);
      doc.text(ParametroMovil.DisponibilidadAgua2[data.disponibilidadAguaPecuario], 110, 288);

   }

   doc.addPage();

   // doc.text("Detalle de la visita", 2, 8);

   if (data.tipoActividad == 1) {
      doc.line(2, 190, 206, 190); // divider

      doc.setTextColor(9, 125, 29);
      doc.setFontSize(14);
      doc.setFont("courier", "bold");
      doc.text("Firmas realizadas", 2, 196);

      if (data.comentario != null) {
         doc.text("Comentario de "+(this.dataVisorVisita.estadoVisita==2?"Aprobación":"Rechazó"), 2, 162);
      }

      doc.setFont("courier", "normal");
      // doc.text("Datos del terreno", 4, 18);
      doc.text("Datos del cultivo", 4, 60); 
      doc.text("Información predios colindantes", 4, 120); 
      doc.text("Comentario complementarios de la visita", 4, 136); 

      doc.setTextColor(0, 0, 0);
      doc.setFontSize(12);
      doc.setFont("courier", "bold");
      // doc.text("Número de parcelas:", 5, 28);
      // doc.text("Hectáreas sembradas: ", 110, 28);
      // doc.text("Unidad catastral: ", 5, 40);
      // doc.text("Hectáreas a financiar: ", 110, 40);
      // doc.text("Hectáreas totales: ", 5, 52);
      // doc.text("Régimen de Tenencia: ", 110, 52);
      // doc.text("Ubicación: / Exp. al riesgo", 5, 64);
      // doc.text("Estado del campo: ", 110, 64);
      doc.text("Tipo de riego:", 5, 20);
      doc.text("Disponibilidad de agua: ", 110, 20);
      doc.text("Pendiente: ", 5, 32);
      doc.text("Tipo de suelo: ", 110, 32);
      doc.text("Accesibilidad del predio: ", 5, 44);
      doc.text("Altitud del predio: ", 110, 44);

      doc.text("Cultivo:", 5, 68);
      doc.text("Variedad: ", 110, 68);
      doc.text("Fecha de siembra: ", 5, 80);
      doc.text("Experiencia en años: ", 110, 80);
      doc.text("Tipo de semilla: ", 5, 92);
      doc.text("Rendimiento campaña anterior: ", 110, 92);
      doc.text("Rendimiento esperado: ", 5, 104);

      doc.setFont("courier", "normal");
      // doc.text(data.numParcelas.toString(), 5, 33);
      // doc.text(data.numHectareasSembradas.toString(), 110, 33);
      // doc.text(data.unidadCatastral, 5, 45);
      // doc.text(data.numHectareasFinanciar.toString(), 110, 45);
      // doc.text(data.numHectareasTotales.toString(), 5, 57);
      // doc.text(ParametroMovil.RegimenConduccion[data.regimenTenencia], 110, 57);
      // doc.text(ParametroMovil.UbicacionRiesgo[data.tipoUbicacion], 5, 69);
      // doc.text(ParametroMovil.EstadoCampo[data.estadoCampo], 110, 69);
      doc.text(ParametroMovil.TipoRiego[data.tipoRiego], 5, 26);
      doc.text(ParametroMovil.DisponibilidadAgua1[data.disponibilidadAgua], 110, 26);
      doc.text(ParametroMovil.Pendiente[data.pendiente], 5, 38);
      doc.text(ParametroMovil.TipoSuelo[data.tipoSuelo], 110, 38);
      doc.text(ParametroMovil.AccesibilidadPredio[data.accesibilidadPredio], 5, 50);
      doc.text(data.altitudPredio.toString() + ' m', 110, 50);
      
      doc.text(data?.cultivoDescripcion, 5, 74);
      doc.text(data?.variedad, 110, 74);
      doc.text(data?.fechaSiembra.substring(0, 10), 5, 86);
      doc.text(data?.aniosExp.toString(), 110, 86);
      doc.text(ParametroMovil.TipoSemilla[data.tipoSiembra], 5, 98);
      doc.text(data?.rendimientoAnterior + " " + data?.unidadPesoDescripcion, 110, 98);
      doc.text(data?.rendimientoEsperado + " " + data?.unidadPesoDescripcion, 5, 110);

      doc.text(data.comentPrediosColindantes, 5, 126, {
         maxWidth: 200
      });

      doc.text(data.comentRecomendaciones, 5, 142, {
         maxWidth: 200
      });

      if (data.comentario != null) {
         doc.text(data.comentario, 5, 170, {
            maxWidth: 200 
         });
      } 

      doc.setFont("courier", "bold");
      doc.text("Firma de titular", 42, 204);

      doc.rect(14, 207, 82, 82); // borde
      doc.addImage(this.dataVisorVisitaImagenesFirmaTitular, "JPEG", 15, 208, 80, 80);
      if (this.dataVisorVisitaImagenesFirmaConyuge != undefined) {
         doc.text("Firma de cónyuge", 147, 204);
         doc.rect(119, 207, 82, 82); // borde
         doc.addImage(this.dataVisorVisitaImagenesFirmaConyuge, "JPEG", 120, 208, 80, 80);
      }

   } else if (data.tipoActividad == 2) {

      doc.line(2, 142, 206, 142); // divider
      doc.setTextColor(9, 125, 29);
      doc.setFontSize(14);
      doc.setFont("courier", "bold");
      doc.text("Firmas realizadas", 2, 148);

      if (data.comentario != null) {
         doc.text("Comentario", 2, 92);
      }

      doc.setFont("courier", "normal");
      // doc.text("Datos de la actividad", 4, 18);
      doc.text("Datos de la crianza", 4, 10);
      doc.text("Información predios colindantes", 4, 60); 
      doc.text("Comentario complementarios de la visita", 4, 76); 

      doc.setTextColor(0, 0, 0);
      doc.setFontSize(12);
      doc.setFont("courier", "bold");
      // doc.text("Unidad a financiar:", 5, 34);
      // doc.text("Unidades a financiar: ", 110, 34);
      // doc.text("Unidades totales: ", 5, 50);
      // doc.text("Unidades productivas: ", 110, 50);
      // doc.text("Tipo de alimentación: ", 5, 66);
      // doc.text("Tipo manejo: ", 110, 66);
      // doc.text("Fuente de agua: ", 5, 82);
      // doc.text("Disponibilidad de agua: ", 110, 82);
      doc.text("Crianza:", 5, 16);
      doc.text("Raza: ", 110, 16);
      doc.text("Años experiencia: ", 5, 28);
      doc.text("Tipo de tecnología: ", 110, 28);
      doc.text("Manejo: ", 5, 40);

      doc.setFont("courier", "normal");
      // doc.text(data.unidadFinanciar, 5, 40);
      // doc.text(data.numUnidadesFinanciar.toString(), 110, 40);
      // doc.text(data.numTotalUnidades.toString(), 5, 56);
      // doc.text(data.unidadesProductivas.toString(), 110, 56);
      // doc.text(ParametroMovil.TipoAlimentacion[data.tipoAlimentacion], 5, 72);
      // doc.text(ParametroMovil.Manejo2[data.tipoManejo], 110, 72);
      // doc.text(ParametroMovil.FuenteAgua[data.tipoFuenteAgua], 5, 88);
      // doc.text(ParametroMovil.DisponibilidadAgua2[data.disponibilidadAguaPecuario], 110, 88);
      doc.text(data.crianzaDescripcion, 5, 22);
      doc.text(data.raza, 110, 22);
      doc.text(data.aniosExperienciaPecuario.toString(), 5, 34);
      doc.text(ParametroMovil.TipoTecnologia[Number(data.tipoTecnologia)], 110, 34);
      doc.text(ParametroMovil.Manejo2[data.tipoManejoPecuario], 5, 46);

      doc.text(data.comentPrediosColindantes, 5, 66, {
         maxWidth: 200
      });

      doc.text(data.comentRecomendaciones, 5, 82, {
         maxWidth: 200
      });

      if (data.comentario != null) {
         doc.text(data.comentario, 5, 98, {
            maxWidth: 200 
         });
      }      

      doc.setFont("courier", "bold");
      doc.text("Firma de titular", 42, 160);

      doc.rect(14, 169, 82, 82); // borde
      doc.addImage(this.dataVisorVisitaImagenesFirmaTitular, "JPG", 15, 170, 80, 80);
      if (this.dataVisorVisitaImagenesFirmaConyuge != undefined) {

         doc.text("Firma de cónyuge", 147, 160);
         doc.rect(119, 169, 82, 82); // borde
         doc.addImage(this.dataVisorVisitaImagenesFirmaConyuge, "JPG", 120, 170, 80, 80);
      }
   }

   doc.addPage();
   
   doc.setTextColor(9, 125, 29);
   doc.setFontSize(14);
   doc.setFont("courier", "bold");
   doc.text("Captura del terreno", 12, 18);
   doc.text("Fotografías capturadas del predio", 12, 132);

   doc.addImage(this.dataVisorMapa, "JPG", 65, 24, 80, 100);

   if  (this.dataVisorVisitaImagenesPredios.length < 3) {
      for (let index = 0; index < this.dataVisorVisitaImagenesPredios.slice(0, 2).length; index++) {
         let image = this.dataVisorVisitaImagenesPredios[index];
         let x = index == 0 ? 20 : 125;
         let y = 140;
         doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
      }

      if (this.dataVisorVisitaImagenesPrediosComp.length > 0) {
         doc.text("Fotografías complementarias del predio", 12, 206);
         for (let index = 0; index < this.dataVisorVisitaImagenesPrediosComp.slice(0, 2).length; index++) {
            let image = this.dataVisorVisitaImagenesPrediosComp[index];
            let x = index == 0 ? 20 : 125;
            let y = 214;
            doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
         }

         if (this.dataVisorVisitaImagenesPrediosComp.length > 2) {
            doc.addPage();
            doc.text("Fotografías complementarias del predio", 12, 16);

            for (let index = 0; index < this.dataVisorVisitaImagenesPrediosComp.slice(4, this.dataVisorVisitaImagenesPrediosComp.length).length; index++) {
               let image = this.dataVisorVisitaImagenesPrediosComp.slice(4, this.dataVisorVisitaImagenesPrediosComp.length)[index];
               let x = index % 2 == 1 ? 125 : 20;
               let y = 24;
               if (index > 1 && index < 4) {
                  y = 96
               }
               if (index >= 4) {
                  y = 170
               }
               doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
            }
         }
      }


   } else {
      for (let index = 0; index < this.dataVisorVisitaImagenesPredios.slice(0, 4).length; index++) {
         let image = this.dataVisorVisitaImagenesPredios.slice(0, 4)[index];
         let x = index % 2 == 1 ? 125 : 20;
         let y = index < 2 ? 140 : 206;
         doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
      }

      if (this.dataVisorVisitaImagenesPrediosComp.length > 0) {
         doc.addPage();
         doc.text("Fotografías complementarias del predio", 12, 16);

         for (let index = 0; index < this.dataVisorVisitaImagenesPrediosComp.length; index++) {
            let image = this.dataVisorVisitaImagenesPrediosComp[index];
            let x = index % 2 == 1 ? 125 : 20;
            let y = 24;
            if (index > 1 && index < 4) {
               y = 96
            }
            if (index >= 4) {
               y = 170
            }
            doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
         }
      }

   }


   if (this.dataVisorVisitaImagenesPredios.length > 4) {
      doc.addPage();
      let lastList = this.dataVisorVisitaImagenesPredios.slice(4, this.dataVisorVisitaImagenesPredios.length);
      for (let index = 0; index < lastList.length; index++) {
         let image = lastList[index];
         let x = index % 2 == 1 ? 125 : 20;
         let y = 24;
         if (index > 1 && index < 4) {
            y = 96
         }
         if (index >= 4) {
            y = 170
         }
         doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
      }

      if (this.dataVisorVisitaImagenesPrediosComp.length > 0) {
         doc.text("Fotografías complementarias del predio", 12, 96);

         for (let index = 0; index < this.dataVisorVisitaImagenesPrediosComp.length; index++) {
            let image = this.dataVisorVisitaImagenesPrediosComp[index];
            let x = index % 2 == 1 ? 125 : 20;
            let y = 102;
            if (index > 1 && index < 4) {
               y = 174
            }
            if (index >= 4) {
               y = 246
            }
            doc.addImage(image.archivoBytes, "JPG", x, y, 60, 60);
         }
      }
   }
   
   doc.save("Resumen_visita_"+data.primerNombrecliente+"_"+data.apellidoPaternoCliente);

  }

  public openModalError(){
   var elem = document.getElementById('ptv-alert-error');    
   var opts = { 
     onCloseEnd: function(){     
      //   location.reload();   
       // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
       //   me.router.navigate(['ProfesionalTecnicoComponent']);
       // }); 
     }
   }
   var instance = M.Modal.init(elem, opts);
   instance.open();
 }

}
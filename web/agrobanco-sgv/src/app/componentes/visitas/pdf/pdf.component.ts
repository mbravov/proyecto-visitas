import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VisitaService } from 'src/app/services/visita.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { SafePipe } from 'src/app/pipes/safe.pipe';
import { PdfViewComponent } from '../pdf-view/pdf-view.component';
@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {

  formVisitaPdfItem! : FormGroup;
  formSubmitted : boolean = false;
  public CodVisita!: number;
  public profesional!: string;
  origen: any;
  public loading = new EventEmitter<boolean>();
  public mensaje = new EventEmitter<string>();
  public comentarioVisita: any = '';

  constructor(private visitaService: VisitaService) { }

  ngOnInit(): void {
    // console.log(this.CodVisita);
    this.initForm();
  }

  initForm():void {
    this.formVisitaPdfItem = new FormGroup({
      comentario : new FormControl("", [Validators.required])
    });
  }

  onComment(comment: any) {
    if (comment !== null) {
      this.comentarioVisita = comment;
      this.formVisitaPdfItem.controls.comentario.disable();
      this.formVisitaPdfItem.controls.comentario.setValue(comment);
    }
  }

  onSubmit(): void {

    var active = document.activeElement?.id;

    this.formSubmitted = true;
    debugger;

    if(this.formVisitaPdfItem.invalid) {      
      return;
    }

    let estado = 1;

    if (active == "btnAprobe") {
      estado = 2;
    }
    if (active == "btnReject") {
      estado = 3;
    }

    let params = this.formVisitaPdfItem.value;
    let comentario = params.comentario;

    this.loading.emit(true);
    this.visitaService.aprobacionVisita(this.CodVisita, comentario, estado).subscribe((res: any) => {
      this.loading.emit(false);
      if (res.response.exito) {
        this.mensaje.emit(estado == 2 ? "Aprobación realizada con éxito" : "Rechazo realizado con éxito");
      } else {
        this.mensaje.emit("");
        console.log(res);
      }
    },(error) => {
      console.log(error);
      this.loading.emit(false);
      this.mensaje.emit("");
    });
    
  }
}

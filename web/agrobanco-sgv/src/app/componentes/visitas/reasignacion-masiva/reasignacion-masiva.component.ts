import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { VisitaService } from 'src/app/services/visita.service';
declare var M: any;

@Component({
  selector: 'app-reasignacion-masiva',
  templateUrl: './reasignacion-masiva.component.html',
  styleUrls: ['./reasignacion-masiva.component.css']
})
export class ReasignacionMasivaComponent implements OnInit {

  
  FormReasignacionPT : FormGroup;
  listaProfesionales : ProfesionalTecnico[] = [];
  @Input() profesionalesEvent = new EventEmitter<ProfesionalTecnico[]>();
  msjUpdateSuccessReasignacion : string = "Se actualizó correctamente";
  peticionesTerminadas = true;

  constructor(private _formBuilder: FormBuilder, private visitaService: VisitaService) { 
    this.FormReasignacionPT = new FormGroup({
      selectOldPT: new FormControl("", [Validators.required]),
      selectNewPT: new FormControl("", [Validators.required])
    });

  }

  ngOnInit(): void {
    this.profesionalesEvent.subscribe( (list: ProfesionalTecnico[]) => {
      this.listaProfesionales = list;
      this.peticionesTerminadas = false;
      setTimeout(() => {
        this.CargarCombo();
      }, 1500);
    });
    this.inicializarFormulario();
  }

  private inicializarFormulario(): void {
    this.FormReasignacionPT =  this._formBuilder.group({
      selectOldPT: [[], Validators.required],
      selectNewPT: [[], Validators.required],
    });;
  }
  
  CargarCombo(){
    M.FormSelect.init(document.getElementById('select_old_pt'), {});
    M.FormSelect.init(document.getElementById('select_new_pt'), {});
    this.peticionesTerminadas = true;
  }

  onSubmit() {
    if (this.FormReasignacionPT.invalid) {
      return
    }

    let params = this.FormReasignacionPT.value;
    let antiguoCodigo = parseInt(params.selectOldPT);
    let nuevoCodigo = parseInt(params.selectNewPT);

    this.reasignarProfesional(antiguoCodigo, nuevoCodigo);
  }

  reasignarProfesional(old: number, replace: number) {
    this.peticionesTerminadas = false;
    this.visitaService.reasignarMasivaProfesional(old, replace).subscribe((res: any) => {
      
      this.peticionesTerminadas = true;

      if (res.response.exito) {
        this.msjUpdateSuccessReasignacion = "Se actualizó correctamente";
        this.openModalSuccess();
      } else {
        this.openModalError();
      }
    }, (error) => {
      this.peticionesTerminadas = true;
      this.openModalError();
    });
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

}

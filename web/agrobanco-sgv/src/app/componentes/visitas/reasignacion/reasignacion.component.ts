import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';
import { VisitaService } from 'src/app/services/visita.service';
declare var M: any;
declare var $: any;

@Component({
  selector: 'app-reasignacion',
  templateUrl: './reasignacion.component.html',
  styleUrls: ['./reasignacion.component.css']
})

export class ReasignacionComponent implements OnInit {
  
  @Input() event$ = new EventEmitter<number>();

  FormReasignacionPT : FormGroup;
  @Input() ArrProfesionalesTecnicos : ProfesionalTecnico[] = [];
  listaProfesionales : ProfesionalTecnico[] = [];
  @Input() codigoVisita = 0;
  msjUpdateSuccess : string = "Se actualizó correctamente";
  peticionesTerminadas = true;

  constructor(private _formBuilder: FormBuilder,
              private profesionalService: ProfesionalTecnicoService,
              private visitaService: VisitaService) { 
    this.FormReasignacionPT = new FormGroup({
      selectPT: new FormControl("", [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.inicializarFormulario();

    this.event$.subscribe((codigo: number) => {
      
      this.listaProfesionales = this.ArrProfesionalesTecnicos.filter(item => item.codigo != codigo);
      console.log(this.listaProfesionales);
      this.peticionesTerminadas = false;
      setTimeout(() => {
        this.peticionesTerminadas = true;
        this.CargarCombo();
      }, 1500);
      
    });
  }

  private inicializarFormulario(): void {
    this.FormReasignacionPT =  this._formBuilder.group({
      selectPT: [[], Validators.required],
    });;
  }
  
  CargarCombo(){
    M.FormSelect.init(document.getElementById('select_pt'), {});
  }

  onSubmit() {
    if(this.FormReasignacionPT.invalid)
    {      
      return;
    }

    let params = this.FormReasignacionPT.value;
    let nuevoCodigo = parseInt(params.selectPT);
    this.reasignarProfesional(nuevoCodigo);
  }

  reasignarProfesional(codigoProfesional: number) {
    this.peticionesTerminadas = false;
    this.visitaService.reasignarProfesionalVisita(this.codigoVisita, codigoProfesional).subscribe((res: any) => {
      
      this.peticionesTerminadas = true;

      if (res.response.exito) {
        this.openModalSuccess();
      } else {
        this.openModalError();
      }
    }, (error) => {
      this.peticionesTerminadas = true;
      this.openModalError();
    });
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
        
        // me.router.navigateByUrl('/tecnico', { skipLocationChange: true }).then(() => {
        //   me.router.navigate(['ProfesionalTecnicoComponent']);
        // }); 
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

}

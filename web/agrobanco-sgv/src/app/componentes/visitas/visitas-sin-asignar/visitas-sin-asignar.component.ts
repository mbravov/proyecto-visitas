import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { AsociacionClienteProfesional } from 'src/app/models/AsociacionClienteProfesional.model';
import { SinAsignar } from 'src/app/models/SinAsignar.model';
import { AsignacionService } from 'src/app/services/asignacion.service';
import { RedirectService } from 'src/app/services/redirect.service';

@Component({
  selector: 'app-visitas-sin-asignar',
  templateUrl: './visitas-sin-asignar.component.html',
  styleUrls: ['./visitas-sin-asignar.component.css']
})
export class VisitasSinAsignarComponent implements OnInit {

  FormVisita: FormGroup = new FormGroup({});
  public loadingObserver = new EventEmitter<boolean>();
  public mensaje = new EventEmitter<string>();
  displayedColumns: string[] = [
    "numerodocumento",
    "nombrecliente",
    "fechavisita",
    "horavisita",
    "visitador",
    "cultivo"
  ];
  FormSubmitted = false;
  ELEMENT_DATA!: SinAsignar[];
  listaVisitas = new MatTableDataSource<SinAsignar>(this.ELEMENT_DATA);

  constructor(
    private asignacionService: AsignacionService,
    public dialogRef: MatDialogRef<VisitasSinAsignarComponent>,
    private redirectService: RedirectService
  ) { }

  ngOnInit(): void {
    this.FormVisita = new FormGroup({
      nrodoc: new FormControl("", [
        Validators.required, 
        Validators.maxLength(8),
        Validators.minLength(8)
      ])
    });
  }

  onSubmit() {
    this.FormSubmitted = true;
    if (this.FormVisita.invalid) {
      return;
    }

    this.loadingObserver.emit(true);
    this.asignacionService.listarSinAsignacion(this.FormVisita.value).subscribe((res: any) => {
      this.loadingObserver.emit(false);
      
      this.listaVisitas.data = res as SinAsignar[];
    }, (err: any) => {
      console.error(err);
      this.loadingObserver.emit(false);
    });
  }

  asignacion() {
    if (this.listaVisitas.data.length === 0) {
      return;
    }

    let lista: AsociacionClienteProfesional[] = [];

    this.listaVisitas.data.forEach((item: SinAsignar) => {
      let asItem = new AsociacionClienteProfesional();
      asItem.tipodocumento = 1;
      asItem.numerodocumento = item.numeroDocumento;
      asItem.estado = 1;
      asItem.codigofuncionario = "1";
      asItem.codigousuario = this.redirectService.leeridPerfil();
      asItem.usuarioweb = this.redirectService.leerUsuarioWeb();
      asItem.codigoprofesional = -1;
      lista.push(asItem);
    });

    console.log(lista);

    this.loadingObserver.emit(true);
    this.asignacionService.asignacionGrupal(lista).subscribe((res: any) => {
      this.loadingObserver.emit(false);
      if (res.exito) {
        this.mensaje.emit("Asociacion realizada con éxito")
      } else {
        this.mensaje.emit("");
      }
    }, (error: any) => {
      console.error(error);
      this.loadingObserver.emit(false);
      this.mensaje.emit("");
    });

  }

}

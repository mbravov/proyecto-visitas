import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin } from 'rxjs';

import { Agencia } from 'src/app/models/Agencia.interface';
import { AgenciaModel } from 'src/app/models/Agencia.model';
import { VisitaModel } from 'src/app/models/Visita.model';
import { DomService } from 'src/app/services/dom.service';
import { ProfesionalTecnicoService } from 'src/app/services/profesional-tecnico.service';

import { Constantes } from 'src/app/comun/constantes';
import { ParametrosService } from 'src/app/services/parametros.service';

import { VisitaService } from '../../services/visita.service';
import { ProfesionalTecnico } from 'src/app/models/ProfesionalTecnico.interface';
import { PdfComponent } from './pdf/pdf.component';
import { MatDialog } from '@angular/material/dialog';
import { Confirm } from 'src/app/models/Util.model';
import { MsgSuccessComponent } from '../shared/msg-success/msg-success.component';
import { Funcionario, FuncionarioModel } from 'src/app/models/Funcionario.model';
import { ProfesionalTecnicoModel } from 'src/app/models/ProfesionalTecnico.model';
import { VisitasSinAsignarComponent } from './visitas-sin-asignar/visitas-sin-asignar.component';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

declare var M: any;
declare var $: any;

@Component({
  selector: 'app-visitas',
  templateUrl: './visitas.component.html',
  styleUrls: ['./visitas.component.css']
})
export class VisitasComponent implements OnInit {

  busquedaFiltro: FormGroup;
  displayedColumns: string[] = ['codigoSolicitudCredito', 'numdocumentocliente', 'clientenombres','cultivo','visitador', 'tipovisita', 'fechaasignacion', 'fechavisita', 'estadovisita','estadoinforme', 'revisar'];
  ELEMENT_DATA!: VisitaModel[];
  listaVisita = new MatTableDataSource<VisitaModel>(this.ELEMENT_DATA);
  tipovisitas : any;
  estadoinformes : any;
  loader : boolean = true;
  //estadoinformes = [{id: 0, name: 'Todos'}, {id: 1, name: 'Pendiente'}, {id: 2, name: 'Aprobado'}, {id: 3, name: 'Rechazado'}];
  agencias: Agencia[] = [];
  funcionarios: any[] = [];
  CodAgenciaStorage:number;
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  codigoProfesional = new EventEmitter<number>();
  codigoVisita: number = 0;
  
  constructor(public dialog: MatDialog,private domService: DomService, private visitaService: VisitaService, private parametroService: ParametrosService, private profesionalService: ProfesionalTecnicoService  ) {
    this.busquedaFiltro = new FormGroup({});
    this.CodAgenciaStorage = Number(localStorage.getItem('CodAgenciaAsignada'));
  }

  ngOnInit(): void {
    this.domService.HideSideBar();
    this.cargaInicial();
    this.inicializarFormulario();
    this.cargarCombos();
    this.CargarParametros();
    this.listaVisita.paginator = this.paginator;
  }

  private inicializarFormulario(): void {
    this.busquedaFiltro = new FormGroup({
                  codigoagencia : new FormControl(0),
                  codigofuncionario : new FormControl(""),
                  tipovisita : new FormControl(0),
                  estadoinforme : new FormControl(0),
                  pagina : new FormControl(0),
                  vinculado :  new FormControl(0)
    });

    this.busquedaFiltro.controls["vinculado"].setValue('1');
  }

  private cargarCombos(): void {
    M.FormSelect.init(document.getElementById('select_agencia'), {});
    M.FormSelect.init(document.getElementById('select_funcionario'), {});
    M.FormSelect.init(document.getElementById('select_tipovisita'), {});
    M.FormSelect.init(document.getElementById('select_estadoinforme'), {});
    this.busquedaFiltro.controls["codigofuncionario"].setValue("TODOS");
  }

  openModalVisita(codigo: number) {  
    //var elem = document.getElementById('visita-pdf-modal');   
    //var elem = document.getElementById('pdf-modal');  
    //M.Modal.init(elem, {}); 
    //var instance = M.Modal.getInstance(elem);
    //instance.open();
    // this.visitaService.codigoVisita$.emit(codigo);
    //this.visitaService.createPdf$.emit(true);
    var elem = document.getElementById('visita-pdf-modal');  
    M.Modal.init(elem, {}); 
    var instance = M.Modal.getInstance(elem);
    instance.open();
  }

  openModalAprobar(codigo: number, nombre1: string, nombre2: string, nombre3: string, nombre4: string) { 
    const dialogRef = this.dialog.open(PdfComponent);
    dialogRef.componentInstance.CodVisita = codigo;
    dialogRef.componentInstance.profesional = nombre1 + ' ' + nombre2 + ' ' + nombre3 + ' ' + nombre4;
    dialogRef.componentInstance.loading.subscribe((loading: boolean) => {
      this.loader = loading;
      // if (!loading) {
      //   dialogRef.close();
      // }
    });
    dialogRef.componentInstance.mensaje.subscribe((msg: string) => {
      if (msg === "") {
        this.openModalError();
      } else {
        this.msjUpdateSuccess = msg;
        this.openModalSuccess();
      }
    });
    /*dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });*/
  }

  openModalReasignacion(codigo: number, visita: number) {  
    // console.log('Codigo profesional' + codigo);
    
    this.codigoProfesional.emit(codigo);
    this.codigoVisita = visita;
    
    var elem = document.getElementById('reasignacion-pt');  
    M.Modal.init(elem, {}); 
    var instance = M.Modal.getInstance(elem);
    instance.open();
  }

  openModalReasignacionProfesionales() {  

    this.profesionalesEvent.emit(this.listaProfesionales);
    
    var elem = document.getElementById('reasignacion-masiva-pt');  
    M.Modal.init(elem, {}); 
    var instance = M.Modal.getInstance(elem);
    instance.open();
  }

  private cargaInicial(): void {
    this.loader = true;
    const filters = new VisitaModel();
    this.loader = true;

    forkJoin([this.visitaService.obtenerVisitas( filters ),
      this.parametroService.obtenerParametros(Constantes.parametroTipoVisita),
      this.parametroService.obtenerParametros(Constantes.parametroEstadoInformes), 
      this.profesionalService.obtenerAgencias()])
    .subscribe((data) => {
      
      const reslista: any = data[0];
      this.tipovisitas = data[1];
      this.estadoinformes = data[2];
      this.loadAgencias(data[3]);
      // console.log(data[4])
      this.listaVisita.data = reslista as VisitaModel[];
      this.loader = false;
      
    }, (error) => {
      console.log(error);
      this.loader = false;
      this.openModalError();
    }, () => {

      setTimeout(() => {
        this.cargarCombos();
        this.loader = false;
      }, 1000);

    });

  }

  private loadAgencias(result: any){       
    result.response.forEach((item : AgenciaModel) => {
      this.agencias.push(item as Agencia);
    });    
    this.busquedaFiltro.controls["codigoagencia"].setValue(this.CodAgenciaStorage);
    
    this.listarFuncionarios(this.CodAgenciaStorage);
  }

  public buscarListaVisitas(): void {

    this.loader = true;
    const params = this.busquedaFiltro.value;
    params.vinculado = parseInt(this.busquedaFiltro.controls["vinculado"].value);
    
    this.visitaService.obtenerVisitas(params).subscribe((res: any) => {

      this.listaVisita.data = res as VisitaModel[];
      console.log(this.listaVisita.data);
      
    }, (error) => {
      console.log(error);
      this.loader = false;
      this.openModalError();
    }, () => {
      this.loader = false;
    });

  }

  listaProfesionales: ProfesionalTecnico[] = [];
  profesionalesEvent = new EventEmitter<ProfesionalTecnico[]>()

  CargarParametros(){
    let busquedaFiltro:any = {
      numeroDocumento :'',
      codigoAgencia : 0,
      codigoAsociacion : 0,
      pagina : 0
    }

    this.profesionalService.buscarProfesional(busquedaFiltro).subscribe((res:any)=> { 
      this.listaProfesionales = res.response.lista as ProfesionalTecnico[];
    },(error)=>{
      console.log(error);
      this.openModalError();
    },()=>{
    });
  }

  mostrarAnulacionDialogo(codigo: number) {
    this.codigoAnulacion = codigo;

    var elem = document.getElementById('ptv-dialog-confirm');  
    M.Modal.init(elem, {}); 
    var instance = M.Modal.getInstance(elem);
    instance.open();
  }

  msgAnularAsignacion = "¿Está seguro que desea anular la asignación de visita?";
  msjUpdateSuccess = "Operación realizada con éxito.";
  codigoAnulacion: number = 0;

  confirmAction($event: Confirm) {
    this.msjUpdateSuccess = "Operación realizada con éxito.";
    if ($event == Confirm.Agree) {
      this.loader = true;
      this.visitaService.anularAsignacion(this.codigoAnulacion).subscribe((res: any) => {
        this.loader = false;
        if (res.response.exito) {
          this.openModalSuccess();
        } else {
          this.openModalError();
        }
      }, (error) => {
        this.loader = false;
        this.openModalError();
      });
    }
  }

  public openModalSuccess()
  {
    var elem = document.getElementById('ptv-alert-ok');    
    var opts = { 
      onCloseEnd: function(){        
        location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  public openModalError(){
    var elem = document.getElementById('ptv-alert-error');    
    var opts = { 
      onCloseEnd: function(){        
        // location.reload();
      }
    }
    var instance = M.Modal.init(elem, opts);
    instance.open();
  }

  listarFuncionarios(agencia: number) {
    this.parametroService.obtenerFuncionarios(agencia).subscribe((res: FuncionarioModel[]) => {
      this.funcionarios = res as Funcionario[];
      this.loader = false;
    }, (error) => {
      console.error(error);
      this.openModalError();
    },()=>{
      setTimeout(() => {
        M.FormSelect.init(document.getElementById('select_funcionario'), {});
      }, 1000);       
    });
  }


  agenciaSeleccionada(selector: any) {
    let agencia = selector.value.toString().split(" ")[1];
    
    let filter = new ProfesionalTecnicoModel();
    filter.codigoAgencia = Number(agencia);
    // console.log(filter);
    
    this.loader = true;

    this.listarFuncionarios(Number(agencia));

    this.busquedaFiltro.controls["codigofuncionario"].setValue("TODOS");
    
  }

  openModalCargarCliente() {
    const dialogRef = this.dialog.open(VisitasSinAsignarComponent, {
      width: '900px'
    });
    dialogRef.componentInstance.loadingObserver.subscribe((load: boolean) => {
      this.loader = load;
    });
    dialogRef.componentInstance.mensaje.subscribe((msg: string) => {
      if (msg === "") {
        this.openModalError();
      } else {
        this.msjUpdateSuccess = msg;
        this.openModalSuccess();
      }
    });
  }

  DescripcionEstadoInforme(codigo:any){
    let texto = "";
    switch(codigo){
      case 1: texto = "Pendiente"; break;
      case 2: texto ="Aprobado"; break;
      case 3: texto ="Rechazado"; break;
    } 
    return texto;
  }

  DescripcionTipoVisita(codigo:any){
    let texto = "";
    switch(codigo){
      case 1: texto ="Inicial"; break;
      case 2: texto ="Seguimiento"; break;
      case 3: texto ="Final"; break;
    } 
    return texto;
  }
  

  Exportar(){

    let PestañaConsultaVisitas:any = [];

    this.listaVisita.data.forEach((element:any) => {
      let ConsultaVisita = {
        solicitud: element.codigoSolicitudCredito,
        dni: element.eAsignacion.eCliente.numdocumento,
        nombres: element.eAsignacion.eCliente.primerNombre + ' ' + element.eAsignacion.eCliente.primerNombre + ' ' + element.eAsignacion.eCliente.primerNombre + ' ' + element.eAsignacion.eCliente.primerNombre,
        cultivo: element.cultivo,
        visitador: element.eAsignacion.eProfesionalTecnico.primerNombre + ' ' + element.eAsignacion.eProfesionalTecnico.segundoNombre + ' ' + element.eAsignacion.eProfesionalTecnico.apellidoPaterno + ' ' + element.eAsignacion.eProfesionalTecnico.apellidoMaterno + '/' + element.nombreFuncionario,
        tipovisita : this.DescripcionTipoVisita(element.tipoVisita),
        fechaasignacion: element.fechaRegistro.replace('T',' '),
        fechavisita: element.eAsignacion.fechaRegistro.replace('T',' '),
        estadovisita: element.codigo>0?'Si':'No' ,
        estadoinforme: this.DescripcionEstadoInforme(element.estado)
      };
      PestañaConsultaVisitas.push(Object.values(ConsultaVisita));
    })


    const title = 'Consulta Visitas';
    const header = ["Nro. Solicitud Crédito", "DNI", "Nombres", "Cultivo", "Visitador / Funcionario", "Tipo visita",	"Fecha Asignación",	"Fecha Visita", 	"Estado Visita" ,	"Estado Informe"];
    const dataConsultaVisitas = PestañaConsultaVisitas;
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Consulta Visitas');

    let titleRow = worksheet.addRow([title]);
    titleRow.font = { name: 'Calibri', family: 4, size: 16, underline: 'single', bold: true }
    worksheet.addRow([]);
    worksheet.mergeCells('A1:D2');

    worksheet.addRow([]);

    let headerRow = worksheet.addRow(header);

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '39B679' },
        bgColor: { argb: '39B679' }
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })

    dataConsultaVisitas.forEach((d:any) => {
      let row = worksheet.addRow(d);
    }
    )

    
    worksheet.getColumn(1).width = 10;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 30;
    worksheet.getColumn(5).width = 50;
    worksheet.getColumn(3).alignment = { wrapText: true };
    worksheet.getColumn(4).alignment = { wrapText: true };
    worksheet.getColumn(5).alignment = { wrapText: true };
    worksheet.getColumn(7).width = 30;
    worksheet.getColumn(8).width = 30;
    worksheet.getColumn(9).width = 20;
    worksheet.getColumn(10).width = 20;
    worksheet.addRow([]);

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'ConsultaVisitas.xlsx');
    })
  }

}

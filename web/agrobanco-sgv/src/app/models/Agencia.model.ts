export class AgenciaModel{
    
    codigo: number;
    oficina: string;
    oficinaRegional: string;

    constructor() {
        this.codigo = 0;
        this.oficina = '';
        this.oficinaRegional = '';
    }
}
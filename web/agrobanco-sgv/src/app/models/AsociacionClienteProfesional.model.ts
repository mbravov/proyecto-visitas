export class AsociacionClienteProfesional {
    tipodocumento: number = 0;
    numerodocumento: string = "";
    codigofuncionario: any;
    codigoprofesional: any = null;
    estado: number = 0;
    codigousuario: string = "";
    primernombre: string = "";
    segundonombre: string = "";
    apellidopaterno: string = "";
    apellidomaterno: string = "";
    usuarioweb: string = "";
}
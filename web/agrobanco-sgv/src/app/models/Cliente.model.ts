
export class ClienteModel{

    codigo: number;
    tipodocumento: number;
    numdocumento: string;
    primerNombre: string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    nombreCompleto: string;
    codigoAsosiacion: number;
    ultimoFiltro: string;
    estado: number;
    usuarioCreacion: string;
    usuarioActualizacion: string;
    seleccionar?: boolean;

    constructor() {
        this.codigo = 0;
        this.tipodocumento = 0;
        this.numdocumento = '';
        this.primerNombre = '';
        this.segundoNombre = '';
        this.apellidoPaterno = '';
        this.apellidoMaterno = '';
        this.nombreCompleto = '';
        this.codigoAsosiacion = 0;
        this.ultimoFiltro = "";
        this.estado = 0;
        this.usuarioCreacion = '';
        this.usuarioActualizacion = '';
        this.seleccionar = false;
    }
}

export interface Cliente {
    codigo: number;
    tipodocumento: number;
    numdocumento: string;
    primerNombre: string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    nombreCompleto: string;
    codigoAsosiacion: number;
    ultimoFiltro: string;
    estado: number;
    usuarioCreacion: string;
    usuarioActualizacion: string;
    seleccionar?: boolean;
}

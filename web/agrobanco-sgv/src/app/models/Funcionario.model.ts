export interface Funcionario {
    usuario: string,
    nombre: string
}

export class FuncionarioModel {
    usuario: string = "";
    nombre: string = "";
}
export class ImeiModel {
    identificadorMovil: string = "";
    usuario: string = "";
}

export interface Imei {
    identificadorMovil: string,
    usuario: string
}
export interface IntegranteAsociacion {
    documento: string,
    nombre: string,
    resultado: number,
    fecha: string,
    hora: string,
    seleccionar: boolean,
    pagina: number,
    pagesize: number
}

export class IntegranteAsociacionModel {
    documento: string = "";
    nombre: string = "";
    resultado: number = 0;
    fecha: string = "";
    hora: string = "";
    seleccionar: boolean = false;
    pagina: number = 0;
    pagesize: number = 0;
}
export interface ProfesionalTecnico{
    
    codigo: number;
    tipoDocumento: number;
    numeroDocumento: string;
    codigoAgencia: number;
    codigoAsociacion: number;
    primerNombre : string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    email: string;
    celular: string;
    estado: number;
    usuarioCreacion: string;
    usuarioActualizacion: string;
    nombreAgencia: string;
    usuarioWeb: string;
    nombres : string;
    editar : string;
    pagina : number;
    pageSize : number;
  }
export class ProfesionalTecnicoModel{
    
    codigo: number = 0;
    tipoDocumento: number;
    numeroDocumento: string;    
    codigoAgencia: number;
    codigoAsociacion: number;
    primerNombre : string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    email: string;
    celular: string;
    estado: number;
    pagina : number;
    pageSize : number;
    nombres: string;
    editar: string;
    usuarioCreacion: string = '';
    usuarioActualizacion: string = '';
    nombreAgencia: string = '';
    usuarioWeb: string = '';

    constructor() {
        this.tipoDocumento = 0;
        this.numeroDocumento = '';
        this.codigoAgencia = 0;
        this.codigoAsociacion = 0;
        this.primerNombre = '';
        this.segundoNombre = '';
        this.apellidoPaterno = '';
        this.apellidoMaterno = '';
        this.email = '';
        this.celular = '';
        this.estado = 0;
        this.pagina = 0;
        this.pageSize = 0;
        this.nombres = '';
        this.editar = '';
        this.nombreAgencia = '';
        this.usuarioWeb = '';
    }
}
export class SinAsignarModel {
    codigoVisita: number = 0;
    numeroDocumento: string = "";
    nombreCompleto: string = "";
    primerNombre: string = "";
    segundoNombre: string = "";
    apellidoPaterno: string = "";
    apellidoMaterno: string = "";
    fecha: string = "";
    hora: string = "";
    usuarioCreacion: string = "";
    cultivo: string = "";
}

export interface SinAsignar {
    codigoVisita: number,
    numeroDocumento: string ,
    nombreCompleto: string ,
    primerNombre: string ,
    segundoNombre: string ,
    apellidoPaterno: string ,
    apellidoMaterno: string ,
    fecha: string ,
    hora: string ,
    usuarioCreacion: string ,
    cultivo: string
}
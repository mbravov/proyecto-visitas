export interface VinculacionVisita{
    id:string;
    cultivo:string;
    solicitudcredito:string;
    nrodocumentocliente:string;
    primernombrecliente:string;
    segundonombrecliente:string;
    apellidopaternocliente:string;
    apellidomaternocliente:string;
    fechaaprobacion:Date;
    nombrefuncionario:string;
    primernombre_pt:String;
    segundonombre_pt:string;
    apellidopaterno_pt:string;
    apellidomaterno_pt:string;
    
}
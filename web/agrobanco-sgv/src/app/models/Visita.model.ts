import { AsignacionModel } from './Asignacion.model';

export class VisitaModel{

    codigo: number;
    codigosolicitudcredito: string;
    numdocumentocliente: string;
    fechaultimofiltro: Date | null;
    fechaultimofiltroobtenido: Date | null;
    codigoagencia: number;
    codigoAsignacion: number;
    fechaasignacion: Date | null;
    codigofuncionario: string;
    codigoProfesionalTecnico: number;
    tipovisita: number;
    estadoinforme: number;
    fecharegistro: Date | null;
    usuariocreacion: string;
    usuarioactualizacion: string;
    codigotecnico: number;
    pagina: number;
    pagesize: number;
    vinculado: number;
    clientePrimerNombre: string;
    clienteSegundoNombre: string;
    clienteApellidoPaterno: string;
    clienteApellidoMaterno: string;
    tecnicoPrimerNombre: string;
    tecnicoSegundoNombre: string;
    tecnicoApellidoPaterno: string;
    tecnicoApellidoMaterno: string;
    nombreFuncionaro: string;

    eAsignacion: AsignacionModel;
  cultivo: any;

    constructor() {
        this.codigo = 0;
        this.codigosolicitudcredito = '';
        this.numdocumentocliente = '';
        this.fechaultimofiltro = null;
        this.fechaultimofiltroobtenido = null;
        this.codigoagencia = 0;
        this.codigoAsignacion = 0;
        this.fechaasignacion = null;
        this.codigofuncionario = '';
        this.codigoProfesionalTecnico = 0;
        this.tipovisita = 0;
        this.estadoinforme = 0;
        this.fecharegistro = null;
        this.usuariocreacion = '';
        this.usuarioactualizacion = '';
        this.codigotecnico = 0;
        this.tipovisita = 0;
        this.pagina = 0;
        this.pagesize = 0;
        this.vinculado = 1;
        this.clientePrimerNombre = '';
        this.clienteSegundoNombre = '';
        this.clienteApellidoPaterno = '';
        this.clienteApellidoMaterno = '';
        this.tecnicoPrimerNombre = '';
        this.tecnicoSegundoNombre = '';
        this.tecnicoApellidoPaterno = '';
        this.tecnicoApellidoMaterno = '';
        this.nombreFuncionaro = '';

        this.eAsignacion = new AsignacionModel();
    }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'generalEstado'
})
export class GeneralEstadoPipe implements PipeTransform {

  transform( valor: number ): string {

    let descestado = '';

    if ( valor === 1 ) {
      descestado = 'SI';
    }
    else {
      descestado = 'NO';
    }

    return descestado;
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'informeEstado'
})
export class InformeEstadoPipe implements PipeTransform {

  transform(codigo: number): string {
    let descripcion = '';

    if ( codigo === 1 ) {
      descripcion = 'Pendiente';
    }

    else if ( codigo === 2 ) {
      descripcion = 'Aprobado';
    }

    else if ( codigo === 3 ) {
      descripcion = 'Rechazado';
    }

    return descripcion;
  }

}

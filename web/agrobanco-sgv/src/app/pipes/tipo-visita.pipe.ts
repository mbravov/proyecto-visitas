import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tipoVisita'
})
export class TipoVisitaPipe implements PipeTransform {

  transform(codigo: number): string {

    let descripcion = '';

    if ( codigo === 1 ) {
      descripcion = 'Inicial';
    }

    else if ( codigo === 2 ) {
      descripcion = 'Seguimiento';
    }

    else if ( codigo === 3 ) {
      descripcion = 'Final';
    }

    return descripcion;
  }

}

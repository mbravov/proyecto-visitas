import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ultimoFiltro'
})
export class UltimoFiltroPipe implements PipeTransform {

  transform(value: string, type: string): string {
    let resultado = "";

    if (type == "Fecha") {
      let day = value.substring(6, 8);
      let month = value.substring(4, 6);
      let year = value.substring(0, 4);
      resultado = day+"/"+month+"/"+year;
    } else if (type == "Hora") {
      if (value.length == 5) {
        value = "0"+value;
      }
      let hour = value.substring(0, 2);
      let minute = value.substring(2, 4);
      resultado = hour+":"+minute;
    }

    return resultado;
  }

}

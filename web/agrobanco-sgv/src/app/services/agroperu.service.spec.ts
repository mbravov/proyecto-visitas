import { TestBed } from '@angular/core/testing';

import { AgroperuService } from './agroperu.service';

describe('AgroperuService', () => {
  let service: AgroperuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgroperuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

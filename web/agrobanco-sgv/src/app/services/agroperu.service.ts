import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;

@Injectable({
  providedIn: 'root'
})
export class AgroperuService {
  
  TokenSGV = localStorage.getItem('TokenSGV');

  constructor(private http: HttpClient) { }

  public ValidarTokenAgroPeru(token: string): Observable<any>
   {

    let request = {
      idtoken : token,
      estado : 0,
      aplicacion : environment.AppKey
     
    };

   return this.http.post(`${UrlBase_SGVAPI}/vinculacion/obtener`, request );

  }

  public DesactivarTokenAgroPeru(token: string): Observable<any>
  {

   let request = {
     idtoken : token,
     estado : 0,
     aplicacion : environment.AppKey
    
   };

  return this.http.post(`${UrlBase_SGVAPI}/vinculacion/actualizar`, request );

 }
}


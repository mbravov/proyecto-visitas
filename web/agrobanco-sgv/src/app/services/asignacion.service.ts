import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AsociacionClienteProfesional } from '../models/AsociacionClienteProfesional.model';

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;
@Injectable({
  providedIn: 'root'
})
export class AsignacionService {

  TokenSGV = localStorage.getItem('TokenSGV');

  constructor(private http: HttpClient) { }

  public listarClientes(codAsociacion: number): any {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': this.TokenSGV 
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/asignacion/clientes`, null, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
    
  }

  // Buscar una visita que se encuentre sin asgnacion para ese dni y vincularlo con la asignacion que se está generando
  public asignacionIndividual(data: AsociacionClienteProfesional) {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': this.TokenSGV 
    }

    const req = ajax.post (`${UrlBase_SGVAPI}/asignacion/asignacionIndividual`, data, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });


    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }


  public asignacionGrupal(data: AsociacionClienteProfesional[]) {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': this.TokenSGV 
    }

    let parametro = {
      lista : data
    };

    const req = ajax.post (`${UrlBase_SGVAPI}/asignacion/asignacionGrupal`, parametro, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });


    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }

  public asignarClientesProfesional(data: AsociacionClienteProfesional[]) {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': this.TokenSGV 
    }

    let parametro = {
      lista : data
    };

    const req = ajax.post (`${UrlBase_SGVAPI}/asignacion/asignacionProfesional`, parametro, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });


    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }

  public listarSinAsignacion(params: any) {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': this.TokenSGV 
    }

    let data = {
      nrodoc: Number(params.nrodoc)
    };

    const req = ajax.post (`${UrlBase_SGVAPI}/asignacion/sinasignar`, data, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });


    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }
  
}

import { Injectable } from '@angular/core';
declare var $:any;

@Injectable({
  providedIn: 'root'
})
export class DomService {
  vezMenu : boolean = true; 
  constructor() { }

  ShowSideBar() {    
    $("body").addClass("menu-open");
    $('.sidenav-overlay').attr('style', 'display: block; opacity: 1;');

    if(this.vezMenu)
    {
      $(".collapsible-body.active").parent().addClass("active").find(".collapsible-body").css("display", "block");
      this.vezMenu = false;
    }
  }

  RemoveMenu() {
    $("#menu-open").hide();
    $("#slide-out").addClass("validacion");
    $(".container-body").attr('style', 'left:0px; width: calc( 100% );');
  }

  HideSideBar(){
    $("body").removeClass('menu-open');
    $('.sidenav-overlay').removeAttr('style');
    $(".collapsible-body").css("display", "none");
    $(".menu-li").removeClass("active");
  }

  ShowLoading(){
    $("#loading-site").show();
  }

  HideLoading()
  {
    $("#loading-site").hide();
  }
  
}

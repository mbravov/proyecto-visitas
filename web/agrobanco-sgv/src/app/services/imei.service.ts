import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;

@Injectable({
  providedIn: 'root'
})
export class ImeiService {

  TokenSGV = localStorage.getItem('TokenSGV');

  constructor() { }

  public listarImei(): Observable<any>
  {
    // let Parametro = {
    //   codgrupo: 0,
    //   codagencia: CodAgencia
    // };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/listaIMEI`, null, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }

  public resetearImei(codUsuario : string): Observable<any>
  {
    let Parametro = {
      usuario: codUsuario
    };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/ResetearIMEI`, Parametro, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ajax } from 'rxjs/ajax';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;

@Injectable({
  providedIn: 'root'
})

export class ParametrosService {

  TokenSGV = localStorage.getItem('TokenSGV');

  constructor() { }

  public obtenerParametros(CodGrupo : Number): Observable<any>
  {
    let Parametro = {
      codgrupo : CodGrupo
    };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/ObtenerParametros`, Parametro, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }
  

  public obtenerFuncionarios(CodAgencia : Number): Observable<any>
  {
    let Parametro = {
      codgrupo: 0,
      codagencia: CodAgencia
    };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/funcionarios`, Parametro, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }

  public obtenerAsociaciones(CodAgencia : Number): Observable<any>
  {
    let Parametro = {
      codigo : CodAgencia
    };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/asociaciones`, Parametro, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }


  public obtenerIntegrantesAsociaciones(CodAsociacion : Number): Observable<any>
  {
    let Parametro = {
      codigo : CodAsociacion
    };

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/integrantesAsociaciones`, Parametro, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }


  public obtenerCultivos(): Observable<any>
  {

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/listarCultivos`, null, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }
  
  public obtenerCreditos(): Observable<any>
  {

    let headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    
    let req = ajax.post(`${UrlBase_SGVAPI}/parametro/listarCreditos`, null, headers);

    let data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response;
      })
    );
  }

}

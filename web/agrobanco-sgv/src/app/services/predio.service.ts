import { Injectable } from '@angular/core';


import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';


import { ajax } from 'rxjs/ajax';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;

@Injectable({
  providedIn: 'root'
})
export class PredioService {

  TokenSGV = localStorage.getItem('TokenSGV');

  constructor(private http: HttpClient) { }

  public obtenerPredio(filtros: any): Observable<any>
  {

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };


    const Predio = {
      "codigoagencia": filtros.codigoagencia,
      "codigofuncionario": filtros.codigofuncionario,
      "cultivo":   filtros.cultivo.toString(),
      "campania": Number(filtros.campania),
      "codigoAsociacion": filtros.codigoAsociacion,
      "codigoEstadoCredido": filtros.codigoEstadoCredido.toString()
    }

    const URL = `${UrlBase_SGVAPI}/predio/lista`;

    // console.log(Predio);

    const req = ajax.post(URL, Predio, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          console.log(res);
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });


    return data$.pipe(
      map( (resp: any) => {
        return resp.response.lista;
      })
    );
  }
}

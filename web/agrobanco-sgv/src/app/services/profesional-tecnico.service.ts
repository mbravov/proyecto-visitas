import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { environment } from 'src/environments/environment';
import { ProfesionalTecnico } from '../models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from '../models/ProfesionalTecnico.model';


const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;
@Injectable({
  providedIn: 'root'
})
export class ProfesionalTecnicoService {

  codigoProfesionalTecnico$ = new EventEmitter<number>();

  TokenSGV = localStorage.getItem('TokenSGV');

  constructor(private http: HttpClient) {

  }

  public obtenerAgencias():any{
    // const httpOptions = {
    //   headers: new HttpHeaders(
    //     {
    //       'Content-Type': 'application/json'
    //       // 'Authorization': `Bearer ${ usuario.Token }`
    //     }),
    //   observe: 'response' as 'response'
    // };

    // return this.http.get("https://reqres.in/api/unknown");

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }

    const req = ajax.post (`${UrlBase_SGVAPI}/parametro/agencias`, null, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    // console.log(data$);
    return data$;


  }

  public obtenerFuncionarios(CodAgencia : number):any{
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }
    let Parametro = {
      codagencia : CodAgencia
    };
    const req = ajax.post (`${UrlBase_SGVAPI}/parametro/funcionarios`, Parametro, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;


  }

  public obtenerAsociaciones():any{
    const req = ajax.getJSON("https://reqres.in/api/unknown");

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public buscarProfesional(filtros: ProfesionalTecnicoModel){

    // const httpOptions = {
    //   headers: new HttpHeaders(
    //     {
    //       'Content-Type': 'application/json',
    //       // 'Authorization': `Bearer ${ usuario.Token }`
    //       "Token":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJDb2RpZ28iOiJyZ3VlcmUiLCJDb3JyZW9FbGVjdHJvbmljbyI6InJndWVyZSIsIk5vbWJyZSI6InJndWVyZSIsIk51bWVyb0RvY3VtZW50byI6InJndWVyZSIsIklkZW50aWZpY2Fkb3JVbmljbyI6ImQwZjg5ZmQyLTNiOWUtNDdkNi04NTY3LTRlYzAyNTZiZTU2ZiIsIm5iZiI6MTYxMjM3OTU1NSwiZXhwIjoxNjEyNjc5NTU1LCJpYXQiOjE2MTIzNzk1NTV9.t6_I_EWUBe6NXAD4cUDvlTulkdlgDEusw-nb8GroSe4"
    //     }),
    //   observe: 'response' as 'response'
    // };

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/profesionaltecnico/listabusqueda`, filtros, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;

  }

  public registrarProfesional(profesional: ProfesionalTecnico){

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/profesionaltecnico/registrar`, profesional, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public obtenerProfesional(filtros: ProfesionalTecnicoModel)
  {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/profesionaltecnico/obtener`, filtros, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public actualizarProfesional(profesional: ProfesionalTecnico){

    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/profesionaltecnico/actualizar`, profesional, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }


}

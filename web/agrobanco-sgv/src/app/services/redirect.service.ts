import { Injectable } from '@angular/core';
import {  HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';


import { EUsuarioModel } from '../models/EUsuario.model';
import { environment } from 'src/environments/environment';


const UrlBase_SGSAPI = environment.UrlBase_SGSAPI;
const UrlBase_SSA = environment.UrlBase_SSA;
const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;
const AppKey = environment.AppKey;
const AppCode = environment.AppCode;
@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  public modulos: any[] = [];

  constructor( private http: HttpClient ) {  }


  obtenerUsuario( usuario: EUsuarioModel ): Observable<any> {

    usuario.UsuarioWeb = '';

    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${ usuario.Token }` }
        ),
      observe: 'response' as 'response'
    };
    
    return this.http.post(`${UrlBase_SGSAPI}/usuario/obtenerusuario`, usuario, httpOptions)
    .pipe(
      map( resp => {
        return resp;
      })
    );
   }



   obtenerModulos( usuario: EUsuarioModel ): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${ usuario.Token }` }
        ),
      observe: 'response' as 'response'
    };
    
    return this.http.get(`${UrlBase_SGSAPI}/modulo/obtenermodulos/${ usuario.idPerfil }/${ usuario.UsuarioWeb }`, httpOptions);
   }



   public guardarToken( usuario: EUsuarioModel ): void {
    localStorage.setItem('token', usuario.Token.toString());
  }



  public leerToken(): any {

    let Token: any;

    if ( localStorage.getItem('token') ) {
      Token = localStorage.getItem('token');
    } else {
      Token = '';
    }

    return Token;
  }

  public guardarFuncionario(codFunc: string): void {
    localStorage.setItem('usFunc', codFunc);
  }

  public leerFuncionario(): any {
    let funcionario: any;

    if ( localStorage.getItem('usFunc') ) {
      funcionario = localStorage.getItem('usFunc');
    } else {
      funcionario = '';
    }

    return funcionario;
  }

  public guardarUsuarioWeb( usuario: EUsuarioModel ): void {
    localStorage.setItem('usuarioWeb', usuario.UsuarioWeb);
  }

  public guardarAgencia( agencia: string): void {
    localStorage.setItem('usAge', agencia);
  }

  public leerAgencia(): any {
    let agencia: any;

    if ( localStorage.getItem('usAge') ) {
      agencia = localStorage.getItem('usAge');
    } else {
      agencia = '';
    }

    return agencia;
  }

  public leerUsuarioWeb(): any {

    let usuarioWeb: any;

    if ( localStorage.getItem('usuarioWeb') ) {
      usuarioWeb = localStorage.getItem('usuarioWeb');
    } else {
      usuarioWeb = '';
    }

    return usuarioWeb;
  }



  public guardaridPerfil( usuario: EUsuarioModel ): void {
    localStorage.setItem('idperfil', usuario.idPerfil);
  }



  public leeridPerfil(): any {

    let idperfil: any;

    if ( localStorage.getItem('idperfil') ) {
      idperfil = localStorage.getItem('idperfil');
    } else {
      idperfil = '';
    }

    return idperfil;
  }



  public guardarNombresUsuarioWeb( nombreUsuario: string ): void {
    localStorage.setItem('nombreUsuario', nombreUsuario);
  }



  public leerNombresUsuarioWeb(): any {

    let nombreUsuario: any;

    if ( localStorage.getItem('nombreUsuario') ) {
      nombreUsuario = localStorage.getItem('nombreUsuario');
    } else {
      nombreUsuario = '';
    }

    return nombreUsuario;
  }



public redirectLogin(): void {
  document.location.href = `${UrlBase_SSA}`;
}

validarToken(): Observable<any> {
  const token = this.leerToken();

  const httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${ token }` }
      ),
    observe: 'response' as 'response'
  };

  return this.http.get(`${UrlBase_SGSAPI}/validate/token`, httpOptions).pipe(tap(data => data));
 }


 generarTokenSGV(eusuario : EUsuarioModel): Observable<any> {

  const httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'X-AppCode': AppCode,
        'X-AppKey' : AppKey
      }
      ),
    observe: 'response' as 'response'
  };


  const model = {
    correoElectronico : eusuario.UsuarioWeb,
    nombreUsuario : eusuario.UsuarioWeb,
    codigoUsuario : eusuario.UsuarioWeb,
    numeroDocumento : eusuario.UsuarioWeb
  }

  
  return this.http.post(`${UrlBase_SGVAPI}/acceso`,model, httpOptions);

 }

}


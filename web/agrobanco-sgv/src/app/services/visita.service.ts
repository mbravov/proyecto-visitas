import { EventEmitter, Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { VisitaModel } from '../models/Visita.model';
import { ajax } from 'rxjs/ajax';
import { environment } from 'src/environments/environment';

const UrlBase_SGVAPI = environment.UrlBase_SGVAPI;
@Injectable({
  providedIn: 'root'
})
export class VisitaService {

  TokenSGV = localStorage.getItem('TokenSGV');
  codigoVisita$ = new EventEmitter<number>();
  createPdf$ = new EventEmitter<boolean>();

  constructor( private http: HttpClient ) {
    
   }

  public obtenerVisitas(filtros: VisitaModel): Observable<any>
  {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/lista`, filtros, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response.lista;
      })
    );
  }

  public obtenerDetalleVisita(codigo: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/detalle`, null, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response.lista;
      })
    );
  }

  public reasignarProfesionalVisita(codvisita: number, codprofesional: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codigovisita : codvisita,
      codigoprofesional: codprofesional
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/reasignar`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public reasignarMasivaProfesional(anterior: number, nuevo: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codigoanterior : anterior,
      codigonuevo: nuevo
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/reasignar_masivo`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }


  public anularAsignacion(codigo: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codigoasignacion : codigo
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/anulacion`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public aprobacionVisita (codigo: number, comment: string, value: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codigovisita : codigo,
      comentario: comment,
      estadovisita: value
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/aprobacion`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public VisorVisita(codigoVisita: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codvisita : codigoVisita
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/visorvisita`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }



  public obtenerVisitasVinculacion(dni: string): Observable<any>
  {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      documento: dni
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/listavincularvisita`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response.lista;
      })
    );
  }



  public asignacionSolicitudCredito(codigo: number, solicitudcredito: string) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };

    let params = {
      codigovisita : codigo,
      solicitudcredito: solicitudcredito
    };

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/vincularvisitasolicitud`, params, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }

  async getBase64ImageFromUrl(imageUrl: string) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }
  
  public obtenerimagenesLista(coddocumentolf: string) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    let param = {
      coddocumentolf : coddocumentolf
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/obtenerimagenesLista`, param, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }


  public obtenerimagenesListaMapa(coddocumentolf: string,posicionLista: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Token': this.TokenSGV
    };
    let param = {
      coddocumentolf : coddocumentolf,
      posicionlista : posicionLista
    }

    const req = ajax.post(`${UrlBase_SGVAPI}/visita/obtenerimagenesListaMapa`, param, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$;
  }

}

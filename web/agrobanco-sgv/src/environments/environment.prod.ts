export const environment = {
  production: true,
  UrlBase_SGSAPI:  (window as any)["env"]["UrlBase_SGSAPI"],
  UrlBase_SSA: (window as any)["env"]["UrlBase_SSA"],
  UrlBase_SGVAPI:  (window as any)["env"]["UrlBase_SGVAPI"],

  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"],
};

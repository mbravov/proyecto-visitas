// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  UrlBase_SGSAPI:  (window as any)["env"]["UrlBase_SGSAPI"],
  UrlBase_SSA: (window as any)["env"]["UrlBase_SSA"],
  UrlBase_SGVAPI:  (window as any)["env"]["UrlBase_SGVAPI"],

  // AppKey: 'RGlzdHJpbHV6X0ludmVyc2lvbmVzX0FwbGljYWNpb24=',
  // AppCode : '5C2CE4D4-8749-4EDA-88E1-7F9E110E639D',

  // RegimenConduccion: [
  //   "vacio",
  //   "Propio",
  //   "Posesionario",
  //   "Arrendado",
  //   "Mixto"
  // ],

  // UbicacionRiesgo: [
  //   "vacio",
  //   "A orilla del rio",
  //   "Orilla del precipicio",
  //   "Suelo Salino/Sódico",
  //   "Huaycos",
  //   "Prob. Disp. de agua",
  //   "Sin exposición"
  // ],

  // EstadoCampo: [
  //   "vacio",
  //   "Otro cultivo",
  //   "En descanso",
  //   "En preparación",
  //   "Regado",
  //   "Sembrado"
  // ],

  // TipoRiego: [
  //   "vacio",
  //   "Secano",
  //   "Gravedad",
  //   "Tecnificado"
  // ],

  // DisponibilidadAgua1: [
  //   "vacio",
  //   "Permanente",
  //   "Semanal / quincenal",
  //   "Mensual"
  // ],

  // Pendiente: [
  //   "vacio",
  //   "Plana",
  //   "Moderada",
  //   "Pronunciada"
  // ],

  // TipoSuelo: [
  //   "vacio",
  //   "Tipo de suelo",
  //   "Arenoso",
  //   "Franco",
  //   "Arcilloso"
  // ],

  // AccesibilidadPredio: [
  //   "vacio",
  //   "Fácil",
  //   "Regular",
  //   "Difícil"
  // ],

  // Peso: [
  //   "-Medida-",
  //   "kilogramo",
  //   "gramo",
  //   "tonelada"
  // ],

  // TipoSemilla: [
  //   "vacio",
  //   "Certificada",
  //   "Selección"
  // ],

  // TipoAlimentacion: [
  //   "vacio",
  //   "Pastos naturales",
  //   "Pastos manejados",
  //   "Alimento concentrado",
  //   "Mixtos"
  // ],

  // Manejo: [
  //   "vacio",
  //   "Extensiva",
  //   "Semi extensiva",
  //   "Intensiva"
  // ],

  // FuenteAgua: [
  //   "vacio",
  //   "Canal de riego",
  //   "Pozo",
  //   "Secano"
  // ],

  // DisponibilidadAgua2: [
  //   "vacio",
  //   "Permanente",
  //   "Semi permanente",
  //   "Estacional"
  // ],

  // Crianza: [
  //   "-Seleccione-",
  //   "Vacuno",
  //   "Camélido",
  //   "Porcino"
  // ],

  // TipoTecnologia: [
  //   "vacio",
  //   "Medio",
  //   "Alto",
  //   "Tradicional",
  // ],

  // Manejo2: [
  //   "vacio",
  //   "Fácil",
  //   "Regular",
  //   "Difícil"
  // ],

  // TipoActividad: [
  //   "Ninguno",
  //   "Agrícola",
  //   "Pecuaria"
  // ],

  // PersonaPresente: [
  //   "Ninguno",
  //   "Productor",
  //   "Familiar"
  // ],

  // MercadoAcceso: [
  //   "Ninguno",
  //   "Local",
  //   "Regional",
  //   "Provincial",
  //   "Distrital"
  // ]

  AppKey:  (window as any)["env"]["AppKey"],
  AppCode :  (window as any)["env"]["AppCode"],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
